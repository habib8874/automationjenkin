﻿namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents an order average report line
    /// </summary>
    public partial class OrderAverageReportLine
    {
        /// <summary>
        /// Gets or sets the count
        /// </summary>
        public int CountOrders { get; set; }

        /// <summary>
        /// Gets or sets the shipping summary (excluding tax)
        /// </summary>
        public decimal SumShippingExclTax { get; set; }

        /// <summary>
        /// Gets or sets the payment fee summary (excluding tax)
        /// </summary>
        public decimal OrderPaymentFeeExclTaxSum { get; set; }

        /// <summary>
        /// Gets or sets the tax summary
        /// </summary>
        public decimal SumTax { get; set; }

        /// <summary>
        /// Gets or sets the order total summary
        /// </summary>
        public decimal SumOrders { get; set; }

        /// <summary>
        /// Gets or sets the refunded amount summary
        /// </summary>
        public decimal SumRefundedAmount { get; set; }

        /// <summary>
        /// Gets or sets the SubTotal amount summary
        /// </summary>
        public decimal OrderSubTotalSum { get; set; }

        /// <summary>
        /// Gets or sets the Discount amount summary
        /// </summary>
        public decimal OrderDiscountTotalSum { get; set; }

        /// <summary>
        /// Gets or sets the Payable amount summary
        /// </summary>
        public decimal OrderPayableTotalSum { get; set; }

        /// <summary>
        /// Gets or sets the Paid amount summary
        /// </summary>
        public decimal OrderPaidTotalSum { get; set; }

        /// <summary>
        /// Gets or sets the Pending amount summary
        /// </summary>
        public decimal OrderPendingTotalSum { get; set; }
    }
}
