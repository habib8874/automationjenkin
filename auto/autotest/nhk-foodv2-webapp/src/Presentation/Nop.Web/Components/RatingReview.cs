﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.RatingReview;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Web.Models.RatingReview;
using Nop.Web.Models.ShoppingCart;
using System.Linq;

namespace Nop.Web.Components
{
    public class RatingReviewViewComponent : NopViewComponent
    {
        private readonly IPermissionService _permissionService;
        private readonly IOrderService _orderService;
        private readonly IRatingReviewFactory _ratingReviewFactory;
        private readonly ISettingService _settingService;
        private readonly IWorkContext _workContext;
        private readonly IPictureService _pictureService;
        private readonly IStoreContext _storeContext;


        public RatingReviewViewComponent(IPermissionService permissionService, IOrderService orderService,
            IRatingReviewFactory ratingReviewFactory, ISettingService settingService,
            IWorkContext workContext, IPictureService pictureService, IStoreContext storeContext)
        {
            this._permissionService = permissionService;
            this._orderService = orderService;
            this._ratingReviewFactory = ratingReviewFactory;
            this._settingService = settingService;
            this._workContext = workContext;
            this._pictureService = pictureService;
            this._storeContext = storeContext;
        }

        public IViewComponentResult Invoke(bool showEmpty = false, int orderId = 0, bool skipNotInterestedCheck = false, int directItemId = 0)
        {
            var isEnabled = _settingService.GetSettingByKey<bool>("ratingreviewsettings.isenabled", true, _workContext.GetCurrentStoreId, true);
            if (!isEnabled)
                return Content("");

            if (showEmpty)
                return View(new RatingReviewModel());

            Order lastOrder = null;
            int storeId = _workContext.GetCurrentStoreId;

            if (orderId > 0)
            {
                lastOrder = _orderService.GetOrderById(orderId);
            }
            else
            {
                var orders = _orderService.SearchOrders(storeId: storeId,
                    customerId: _workContext.CurrentCustomer.Id);

                if (orders.Count == 0)
                    return Content("");

                lastOrder = orders.OrderByDescending(x => x.CreatedOnUtc).FirstOrDefault();
            }

            if (lastOrder == null || lastOrder.OrderStatus != OrderStatus.OrderDelivered)
                return Content("");

            //direct item review ?
            int reviewType = (int)ReviewType.Agent;
            if (directItemId == 0)
            {

                reviewType = (int)ReviewType.Agent;
                int review_status = _workContext.CurrentCustomer.CheckReviewForCustomer(reviewType, lastOrder.Id, storeId, skipNotInterestedCheck);

                if (review_status == 0 || review_status == 3)
                    return Content("");

                if (review_status == 2 || (!skipNotInterestedCheck && review_status == 4))
                {
                    reviewType = (int)ReviewType.Merchant;
                    review_status = _workContext.CurrentCustomer.CheckReviewForCustomer(reviewType, lastOrder.Id, storeId, skipNotInterestedCheck);

                    if (review_status == 0 || review_status == 3)
                        return Content("");

                    if (review_status == 2 || review_status == 4)
                    {
                        reviewType = (int)ReviewType.Item;
                        review_status = _workContext.CurrentCustomer.CheckReviewForCustomer(reviewType, lastOrder.Id, storeId, skipNotInterestedCheck);

                        if (review_status != 1)
                            return Content("");
                    }
                }
            }
            else
            {
                reviewType = (int)ReviewType.Item;
            }

            var model = _ratingReviewFactory.PrepareRatingReviewModel(reviewType, lastOrder, directItemId);
            if (model == null)
                return Content("");

            return View(model);
        }
    }
}
