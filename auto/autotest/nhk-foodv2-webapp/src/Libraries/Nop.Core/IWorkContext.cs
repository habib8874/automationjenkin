﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Vendors;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain;
using Nop.Core.Domain.NB;

namespace Nop.Core
{
    /// <summary>
    /// Represents work context
    /// </summary>
    public interface IWorkContext
    {
        /// <summary>
        /// Gets or sets the current customer
        /// </summary>
        Customer CurrentCustomer { get; set; }

        /// <summary>
        /// Gets the original customer (in case the current one is impersonated)
        /// </summary>
        Customer OriginalCustomerIfImpersonated { get; }

        /// <summary>
        /// Gets the current vendor (logged-in manager)
        /// </summary>
        Vendor CurrentVendor { get; }

        /// <summary>
        /// Gets or sets current user working language
        /// </summary>
        Language WorkingLanguage { get; set; }

        /// <summary>
        /// Gets or sets current user working currency
        /// </summary>
        Currency WorkingCurrency { get; set; }

        /// <summary>
        /// Gets or sets current tax display type
        /// </summary>
        TaxDisplayType TaxDisplayType { get; set; }

        /// <summary>
        /// Gets or sets value indicating whether we're in admin area
        /// </summary>
        bool IsAdmin { get; set; }

        #region Custom code from v4.0
        int GetCurrentStoreId { get; }
        Vendor CurrentMerchant { get; set; }
        string EnteredDeliveryAddress { get; set; }
        string CurrentMerchantAddress { get; set; }
        string CurrentMerchantPhone { get; set; }
        string CurrentStoreISDCode { get; }
        OrderDetails CurrentOrderDetails { get; set; }
        int CurrentMerchantId { get; }
        bool IsVendorRole { get; }
        bool IsAdminRole { get; }
        bool IsStoreOwnerRole { get; }
        StoreInformationSettings StoreInformationSettings { get; }
        #endregion

        #region Generate OTP
        /// <summary>
        /// Generate OTP
        /// </summary>
        string GenerateOTP { get; }
        #endregion

        #region User sub admin

        /// <summary>
        /// Get User Sub Admin details.
        /// </summary>
        UserSubAdmin SubAdmin { get; }

        #endregion
    }
}
