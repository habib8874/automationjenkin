﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB;

namespace Nop.Data.Mapping.NB
{
    /// <summary>
    /// Represents a DeliverySlot configuration
    /// </summary>
    public partial class DeliverySlotMap : NopEntityTypeConfiguration<DeliverySlot>
    {
        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<DeliverySlot> builder)
        {
            builder.ToTable(NopMappingDefaults.DeliverySlotTable);
            builder.HasKey(pc => pc.Id);
            base.Configure(builder);
        }
    }
}
