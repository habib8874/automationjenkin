﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB.Package
{
    /// <summary>
    /// Represents a package list model
    /// </summary>
    public partial class PackageListModel : BasePagedListModel<PackageModel>
    {
    }
}