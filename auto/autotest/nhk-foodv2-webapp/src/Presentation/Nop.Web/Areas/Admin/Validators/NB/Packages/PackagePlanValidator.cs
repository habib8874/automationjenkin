﻿using FluentValidation;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Areas.Admin.Models.NB.Package;
using Nop.Web.Framework.Validators;

namespace Nop.Web.Areas.Admin.Validators.Packages
{
    public partial class PackagePlanValidator : BaseNopValidator<PackagePlanModel>
    {
        public PackagePlanValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            //RuleFor(x => x.PlanName).NotEmpty().WithMessage(localizationService.GetResource("Admin.Subscription.PackagePlan.Fields.PlanName.Required"));
        }
    }
}