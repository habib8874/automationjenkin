﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
//using Nop.Web.Areas.Admin.Validators.UserPushIdDetails;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using System;

namespace Nop.Web.Areas.Admin.Models.UserPushIdDetails
{
    //[Validator(typeof(UserPushIdDetailValidator))]
    public partial class UserPushIdDetailModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.Configuration.UserPushIdDetails.Fields.UserId")]
        public int UserId { get; set; }

        [NopResourceDisplayName("Admin.Configuration.UserPushIdDetails.Fields.Token")]
        public string Token { get; set; }

        [NopResourceDisplayName("Admin.Configuration.UserPushIdDetails.Fields.CrDt")]
        public DateTime CrDt { get; set; }
    }
}
