﻿using System;

namespace Nop.Core.Domain.NB.Agent
{
    /// <summary>
    /// Represents a Agent order geolocation entity class
    /// </summary>
    public partial class AgentOrderGeoLocation : BaseEntity
    {
        public int AgentId { get; set; }
        public int OderId { get; set; }
        public string AgentLat { get; set; }
        public string AgentLong { get; set; }
        public string DeviceUpDt { get; set; }
        public DateTime? SysUpDt { get; set; }
    }
}
