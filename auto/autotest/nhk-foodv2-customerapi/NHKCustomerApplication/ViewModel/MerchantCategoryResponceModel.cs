﻿using NHKCustomerApplication.ViewModels;
using System.Collections.Generic;

namespace NHKCustomerApplication.ViewModel
{
    public class MerchantCategoryResponceModel : CustomerResponseModel
    {
        public List<CatList> catLists { get; set; }
    }

    public class CatList
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
        public int ParentCategoryId { get; set; }
        public bool IsPublished { get; set; }
        public bool IsDeleted { get; set; }
        public int TotalRecords { get; set; }
        public int PictureId { get; set; }
        public string CategoryPictureUrl { get; set; }
        public int DisplayOrder { get; set; }
    }
    public class MerchantCategoryListResponceModelV1 : CustomerResponseModel
    {
        public List<MerchantCategoryListResponceModel> Categoriess { get; set; }
    }
    public class MerchantCategoryListResponceModel
    {
        public int MerchantCategoryId { get; set; }
        public string MerchantCatName { get; set; }
        public string Description { get; set; }
        public int PictureId { get; set; }
        public int DisplayOrder { get; set; }
        public int StoreId { get; set; }
        public bool IsPublished { get; set; }
        public bool IsDeleted { get; set; }
        public string MerchantCategoryPictureUrl { get; set; }
        public string StoreName { get; set; }
    }
}
