﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Schedule;

namespace Nop.Data.Mapping.ScheduleMap
{
    public partial class ScheduleMap : NopEntityTypeConfiguration<Schedules>
    {
        public override void Configure(EntityTypeBuilder<Schedules> builder)
        {
            builder.ToTable("Schedules");
            builder.HasKey(p => p.Id);
            base.Configure(builder);
        }
    }
}
