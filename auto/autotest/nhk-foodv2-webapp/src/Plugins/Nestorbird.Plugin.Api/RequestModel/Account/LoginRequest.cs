﻿namespace Nestorbird.Plugin.Api.RequestModel.Account
{
    public class LoginRequest
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string DeviceToken { get; set; }
    }
}
