﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;
using Nop.Web.Areas.Admin.Models.Orders;
using Nop.Core.Domain.Tax;

namespace Nop.Web.Areas.Admin.Models.NB.Common
{
    public class OrderNotificationModel : BaseNopEntityModel
    {
        public OrderNotificationModel()
        {
            Items = new List<OrderItemModel>();
        }

        public string OrderNumber { get; set; }
        public string CreatedOn { get; set; }
        public string CustomerName { get; set; }
        public string OrderTotal { get; set; }
        public bool AllowCustomersToSelectTaxDisplayType { get; set; }
        public TaxDisplayType TaxDisplayType { get; set; }

        public IList<OrderItemModel> Items { get; set; }
    }
}
