﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Services.Localization;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Tax;
using Nop.Web.Framework.Models.DataTables;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the tax model factory implementation
    /// </summary>
    public partial class TaxModelFactory : ITaxModelFactory
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly ITaxCategoryService _taxCategoryService;
        private readonly ITaxPluginManager _taxPluginManager;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly IWorkContext _workContext;
        private readonly IStoreService _storeService;

        #endregion

        #region Ctor

        public TaxModelFactory(ILocalizationService localizationService, 
            ITaxCategoryService taxCategoryService,
            ITaxPluginManager taxPluginManager,
            IBaseAdminModelFactory baseAdminModelFactory,
            IWorkContext workContext,
            IStoreService storeService)
        {
            _localizationService = localizationService;
            _taxCategoryService = taxCategoryService;
            _taxPluginManager = taxPluginManager;
            _baseAdminModelFactory = baseAdminModelFactory;
            _workContext = workContext;
            _storeService = storeService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare tax configuration model
        /// </summary>
        /// <param name="taxConfigurationModel">Tax configuration model</param>
        /// <returns>Tax configuration model</returns>
        public virtual TaxConfigurationModel PrepareTaxConfigurationModel(TaxConfigurationModel taxConfigurationModel)
        {
            if (taxConfigurationModel == null)
                throw new ArgumentNullException(nameof(taxConfigurationModel));

            //prepare nested search models
            PrepareTaxProviderSearchModel(taxConfigurationModel.TaxProviders);
            PrepareTaxCategorySearchModel(taxConfigurationModel.TaxCategories);

            return taxConfigurationModel;
        }

        /// <summary>
        /// Prepare tax provider search model
        /// </summary>
        /// <param name="searchModel">Tax provider search model</param>
        /// <returns>Tax provider search model</returns>
        public virtual TaxProviderSearchModel PrepareTaxProviderSearchModel(TaxProviderSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged tax provider list model
        /// </summary>
        /// <param name="searchModel">Tax provider search model</param>
        /// <returns>Tax provider list model</returns>
        public virtual TaxProviderListModel PrepareTaxProviderListModel(TaxProviderSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get tax providers
            var taxProviders = _taxPluginManager.LoadAllPlugins().ToPagedList(searchModel);

            //prepare grid model
            var model = new TaxProviderListModel().PrepareToGrid(searchModel, taxProviders, () =>
            {
                return taxProviders.Select(provider =>
                {
                    //fill in model values from the entity
                    var taxProviderModel = provider.ToPluginModel<TaxProviderModel>();

                    //fill in additional values (not existing in the entity)
                    taxProviderModel.ConfigurationUrl = provider.GetConfigurationPageUrl();
                    taxProviderModel.IsPrimaryTaxProvider = _taxPluginManager.IsPluginActive(provider);

                    return taxProviderModel;
                });
            });
            return model;
        }

        /// <summary>
        /// Prepare tax category search model
        /// </summary>
        /// <param name="searchModel">Tax category search model</param>
        /// <returns>Tax category search model</returns>
        public virtual TaxCategorySearchModel PrepareTaxCategorySearchModel(TaxCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            //prepare available stores
            _baseAdminModelFactory.PrepareStores(searchModel.AddTaxCategory.AvailableStores);

            return searchModel;
        }

        /// <summary>
        /// Prepare paged tax category list model
        /// </summary>
        /// <param name="searchModel">Tax category search model</param>
        /// <returns>Tax category list model</returns>
        public virtual TaxCategoryListModel PrepareTaxCategoryListModel(TaxCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get tax categories
            var allTaxCategories = _taxCategoryService.GetAllTaxCategories();
            if (_workContext.IsStoreOwnerRole)
            {
                allTaxCategories = allTaxCategories.Where(x => x.StoreId == 0 || x.StoreId == _workContext.GetCurrentStoreId).ToList();
            }
            var taxCategories = allTaxCategories.ToPagedList(searchModel);

            //prepare grid model
            var model = new TaxCategoryListModel().PrepareToGrid(searchModel, taxCategories, () =>
            {
                //fill in model values from the entity
                return taxCategories.Select(taxCategory =>
                { 
                    var m = taxCategory.ToModel<TaxCategoryModel>();
                    //fill in additional values (not existing in the entity)
                    m.StoreName = taxCategory.StoreId > 0
                        ? _storeService.GetStoreById(taxCategory.StoreId)?.Name ?? "Deleted"
                        : _localizationService.GetResource("Admin.Configuration.Settings.AllSettings.Fields.StoreName.AllStores");

                    return m;
                });
            });

            return model;
        }

        #endregion
    }
}