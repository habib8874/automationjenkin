﻿using Nop.Core.Domain.Catalog;

namespace Nop.Core.Domain.NB.Suppliers
{
    public partial class ProductSupplier:BaseEntity
    {
        /// <summary>
        /// Gets or sets the product identifier
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the supplier identifier
        /// </summary>
        public int SupplierId { get; set; }

       // public string ProductName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the product is featured
        /// </summary>
        public bool IsFeaturedProduct { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets the Supplier
        /// </summary>
        public virtual Supplier Supplier { get; set; }

        /// <summary>
        /// Gets the product
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        /// Gets the Price
        /// </summary>
        public decimal Price { get; set; }
    }
}
