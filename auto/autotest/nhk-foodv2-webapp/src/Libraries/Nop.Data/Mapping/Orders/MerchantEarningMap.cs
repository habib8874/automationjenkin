﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Orders;

namespace Nop.Data.Mapping.Orders
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class MerchantEarningMap : NopEntityTypeConfiguration<MerchantEarning>
    {
        public override void Configure(EntityTypeBuilder<MerchantEarning> builder)
        {
            builder.ToTable(NopMappingDefaults.MerchantEarningTable);
            builder.HasKey(sci => sci.Id);

            base.Configure(builder);
        }
    }
}