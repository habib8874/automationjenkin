﻿using NHKCustomerApplication.ViewModels;
using System.Collections.Generic;

namespace NHKCustomerApplication.Repository
{
    /// <summary>
    /// UserRepository.
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// RegisterCustomer.
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="saltKey"></param>
        /// <param name="password"></param>
        /// <param name="otp"></param>
        /// <returns></returns>
        CustomerResponseModel RegisterCustomer(CustomerModel customer, string saltKey, string password, string otp, int languageId = 0);

        
        /// <summary>
        /// ExternalAuth.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerResponseModel ExternalAuth(ExternalAuthModel customer);

        /// <summary>
        /// Insert Guest User.
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="otp"></param>
        /// <returns></returns>
        CustomerResponseModel InsertGuestUser(GuestUserModel customer, string otp);

        /// <summary>
        /// RegistaionVerificationCustomer.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerResponseModel RegistaionVerificationCustomerV3(RegistationverificationCustomer customer);

        /// <summary>
        /// CustomerLogin
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerLoginResponseModel CustomerLoginV3(CustomerLoginV3 customer);
        CustomerLoginResponseModel CustomerLoginV2_2(CustomerLogin customer);

        /// <summary>
        /// RegistaionVerificationCustomer.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerResponseModel RegistaionVerificationCustomer(RegistationverificationCustomer customer);

        /// <summary>
        /// CustomerLogin
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerLoginResponseModel CustomerLogin(CustomerLogin customer);

        /// <summary>
        /// CustomerLogin
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerLoginResponseModel CustomerLoginV2_1(CustomerLoginV2_1 customer);

        /// <summary>
        /// GetCooknRestItemsByRestnCookId.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerRestProductResponseModel GetCooknRestItemsByRestnCookId(CooknRestProductByID customer);
       

        List<ProductAttributesV2> GetProductProductAttributes(List<int> productIds);


        /// <summary>
        /// Get List of Products (Categories or Sub-Categories).
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        CustomerRestProductResponseModels GetCooknRestItemsByCategoryId(CooknRestProductByCategoryIds requestModel);
        CustomerRestProductResponseModels GetCooknRestItemsByCategoryId__V2(CooknRestProductByCategoryIdsV2 requestModel);

        /// <summary>
        /// Get Filtered and sorted List of Products 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        CustomerRestProductFilterResponseModel GetProductItemsFilternSort(CustomerProductFilter customer);
    }
}
