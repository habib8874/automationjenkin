﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB;

namespace Nop.Data.Mapping.NB
{
    /// <summary>
    /// Represents a ProductDelivery Slot mapping configuration
    /// </summary>
    public partial class ProductDeliverySlotMappingMap : NopEntityTypeConfiguration<ProductDeliverySlotMapping>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<ProductDeliverySlotMapping> builder)
        {
            builder.ToTable(NopMappingDefaults.ProductDeliverySlotMappingTable);
            builder.HasKey(productDeliverySlot => productDeliverySlot.Id);

            builder.HasOne(productDeliverySlot => productDeliverySlot.Product)
                .WithMany()
                .HasForeignKey(productDeliverySlot => productDeliverySlot.ProductId)
                .IsRequired();

            builder.HasOne(productDeliverySlot => productDeliverySlot.DeliverySlot)
                .WithMany()
                .HasForeignKey(productDeliverySlot => productDeliverySlot.DeliverySlotId)
                .IsRequired();

            base.Configure(builder);
        }

        #endregion
    }
}