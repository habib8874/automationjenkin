﻿using System.Collections.Generic;
using Nop.Core.Domain.Catalog;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Product tag service interface
    /// </summary>
    public partial interface IProductTagImageMappingService
    {
        /// <summary>
        /// Delete a product tag Image Maping
        /// </summary>
        /// <param name="productTagImageMaping">Product tag image mapping</param>
        void DeleteProductTagImageMapping(ProductImageTag productTagImageMapping);

        /// <summary>
        /// Gets all product tags image mapping
        /// </summary>
        /// <returns>Product tags image mapping</returns>
        IList<ProductImageTag> GetAllProductTagsImageMapping();

        /// <summary>
        /// Gets product tag Image Mapping
        /// </summary>
        /// <param name="productTagId">Product tag identifier</param>
        /// <returns>Product tag image mapping</returns>
        ProductImageTag GetProductTagImageMappingById(int productTagImageMappingId);
        
        /// <summary>
        /// Gets product tag Image Mapping
        /// </summary>
        /// <param name="productTagId">Product tag identifier</param>
        /// <returns>Product tag image mapping</returns>
        ProductImageTag GetProductTagImageMappingByTagId(int productTagImageMappingTagId);

        ///// <summary>
        ///// Gets product tag by name
        ///// </summary>
        ///// <param name="name">Product tag name</param>
        ///// <returns>Product tag</returns>
        //ProductTag GetProductTagByName(string name);

        /// <summary>
        /// Inserts a product tag Image Mapping
        /// </summary>
        /// <param name="productTagImageMapping">Product tag image mapping</param>
        void InsertProductTagImageMapping(ProductImageTag productTagImageMapping);

        /// <summary>
        /// Updates the product tag Image Mapping
        /// </summary>
        /// <param name="productTagImageMapping">Product tag Image Mapping</param>
        void UpdateProductTagImageMapping(ProductImageTag productTagImageMapping);

        /// <summary>
        /// Get number of products
        /// </summary>
        /// <param name="productTagId">Product tag identifier</param>
        /// <param name="storeId">Store identifier</param>
        /// <returns>Number of products</returns>
        int GetProductCount(int productTagId, int storeId);

        /// <summary>
        /// Update product tags Image Mapping
        /// </summary>
        /// <param name="product">Product for update</param>
        /// <param name="productTagsImage Mapping">Product tags Image Mapping</param>
        //void UpdateProductTagsImageMapping(Product product, string[] productTagsImageMapping);
    }
}
