﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.BookingTable;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Stores;
using Nop.Data;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.BookingTable
{
    /// <summary>
    /// Product service
    /// </summary>
    public partial class BookingService : IBookingService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : product ID
        /// </remarks>
        private const string PRODUCTS_BY_ID_KEY = "Nop.BookingTableTransaction.id-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PRODUCTS_PATTERN_KEY = "Nop.BookingTableTransaction.";

        #endregion

        #region Fields
        private readonly IRepository<BookingTableMaster> _BookingMasterRepository;
        private readonly IRepository<BookingTableTransaction> _BookingTransactionRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<RelatedProduct> _relatedProductRepository;
        private readonly IRepository<CrossSellProduct> _crossSellProductRepository;
        private readonly IRepository<TierPrice> _tierPriceRepository;
        private readonly IRepository<LocalizedProperty> _localizedPropertyRepository;
        private readonly IRepository<AclRecord> _aclRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly IRepository<ProductPicture> _productPictureRepository;
        private readonly IRepository<ProductSpecificationAttribute> _productSpecificationAttributeRepository;
        private readonly IRepository<ProductReview> _productReviewRepository;
        private readonly IRepository<ProductWarehouseInventory> _productWarehouseInventoryRepository;
        private readonly IRepository<SpecificationAttributeOption> _specificationAttributeOptionRepository;
        private readonly IRepository<StockQuantityHistory> _stockQuantityHistoryRepository;
        private readonly ILanguageService _languageService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IDataProvider _dataProvider;
        private readonly IDbContext _dbContext;
        private readonly ICacheManager _cacheManager;
        private readonly IWorkContext _workContext;
        private readonly LocalizationSettings _localizationSettings;
        private readonly CommonSettings _commonSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly IEventPublisher _eventPublisher;
        private readonly IAclService _aclService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IRepository<BookingTableMasterDetail> _BookingMasterDetailsRepository;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="productRepository">Product repository</param>
        /// <param name="relatedProductRepository">Related product repository</param>
        /// <param name="crossSellProductRepository">Cross-sell product repository</param>
        /// <param name="tierPriceRepository">Tier price repository</param>
        /// <param name="localizedPropertyRepository">Localized property repository</param>
        /// <param name="aclRepository">ACL record repository</param>
        /// <param name="storeMappingRepository">Store mapping repository</param>
        /// <param name="productPictureRepository">Product picture repository</param>
        /// <param name="productSpecificationAttributeRepository">Product specification attribute repository</param>
        /// <param name="productReviewRepository">Product review repository</param>
        /// <param name="productWarehouseInventoryRepository">Product warehouse inventory repository</param>
        /// <param name="specificationAttributeOptionRepository">Specification attribute option repository</param>
        /// <param name="stockQuantityHistoryRepository">Stock quantity history repository</param>
        /// <param name="productAttributeService">Product attribute service</param>
        /// <param name="productAttributeParser">Product attribute parser service</param>
        /// <param name="languageService">Language service</param>
        /// <param name="workflowMessageService">Workflow message service</param>
        /// <param name="dataProvider">Data provider</param>
        /// <param name="dbContext">Database Context</param>
        /// <param name="workContext">Work context</param>
        /// <param name="localizationSettings">Localization settings</param>
        /// <param name="commonSettings">Common settings</param>
        /// <param name="catalogSettings">Catalog settings</param>
        /// <param name="eventPublisher">Event published</param>
        /// <param name="aclService">ACL service</param>
        /// <param name="storeMappingService">Store mapping service</param>
        public BookingService(
            IRepository<BookingTableTransaction> BookingTransactionRepository,
             IRepository<BookingTableMaster> bookingMasterRepository,
            ICacheManager cacheManager,
            IRepository<Product> productRepository,
            IRepository<RelatedProduct> relatedProductRepository,
            IRepository<CrossSellProduct> crossSellProductRepository,
            IRepository<TierPrice> tierPriceRepository,
            IRepository<ProductPicture> productPictureRepository,
            IRepository<LocalizedProperty> localizedPropertyRepository,
            IRepository<AclRecord> aclRepository,
            IRepository<StoreMapping> storeMappingRepository,
            IRepository<ProductSpecificationAttribute> productSpecificationAttributeRepository,
            IRepository<ProductReview> productReviewRepository,
            IRepository<ProductWarehouseInventory> productWarehouseInventoryRepository,
            IRepository<SpecificationAttributeOption> specificationAttributeOptionRepository,
            IRepository<StockQuantityHistory> stockQuantityHistoryRepository,
            ILanguageService languageService,
            IWorkflowMessageService workflowMessageService,
            IDataProvider dataProvider,
            IDbContext dbContext,
            IWorkContext workContext,
            LocalizationSettings localizationSettings,
            CommonSettings commonSettings,
            CatalogSettings catalogSettings,
            IEventPublisher eventPublisher,
            IAclService aclService,
            IStoreMappingService storeMappingService,
            IRepository<BookingTableMasterDetail> BookingMasterDetailsRepository
            )
        {
            _BookingTransactionRepository = BookingTransactionRepository;
            _BookingMasterRepository = bookingMasterRepository;
            _cacheManager = cacheManager;
            _productRepository = productRepository;
            _relatedProductRepository = relatedProductRepository;
            _crossSellProductRepository = crossSellProductRepository;
            _tierPriceRepository = tierPriceRepository;
            _productPictureRepository = productPictureRepository;
            _localizedPropertyRepository = localizedPropertyRepository;
            _aclRepository = aclRepository;
            _storeMappingRepository = storeMappingRepository;
            _productSpecificationAttributeRepository = productSpecificationAttributeRepository;
            _productReviewRepository = productReviewRepository;
            _productWarehouseInventoryRepository = productWarehouseInventoryRepository;
            _specificationAttributeOptionRepository = specificationAttributeOptionRepository;
            _stockQuantityHistoryRepository = stockQuantityHistoryRepository;
            _languageService = languageService;
            _workflowMessageService = workflowMessageService;
            _dataProvider = dataProvider;
            _dbContext = dbContext;
            _workContext = workContext;
            _localizationSettings = localizationSettings;
            _commonSettings = commonSettings;
            _catalogSettings = catalogSettings;
            _eventPublisher = eventPublisher;
            _aclService = aclService;
            _storeMappingService = storeMappingService;
            _BookingMasterDetailsRepository = BookingMasterDetailsRepository;
        }

        #endregion



        #region Products

        /// <summary>
        /// Gets product
        /// </summary>
        /// <param name="productId">Product identifier</param>
        /// <returns>Product</returns>
        public virtual BookingTableTransaction GetBookingById(int bookingId)
        {
            if (bookingId == 0)
                return null;

            var key = string.Format(PRODUCTS_BY_ID_KEY, bookingId);
            return _cacheManager.Get(key, () => _BookingTransactionRepository.GetById(bookingId));
        }
        /// <summary>
        /// <summary>
        /// CancelBooking
        /// </summary>
        /// <param name="products">Products</param>
        public virtual void CancelBooking(BookingTableTransaction bookingTableTransaction,bool value)
        {
            if (bookingTableTransaction == null)
                throw new ArgumentNullException(nameof(bookingTableTransaction));
           
                _dbContext.ExecuteSqlCommand("Update  BookingTableTransaction set Status="+3 +" where id=" + bookingTableTransaction.Id);
                _dbContext.SaveChanges();
        }
        /// <summary>
        /// UpdateBooking
        /// </summary>
        /// <param name="products">Products</param>
        public virtual void UpdateBooking(BookingTableTransaction bookingTableTransaction)
        {
            if (bookingTableTransaction == null)
                throw new ArgumentNullException(nameof(bookingTableTransaction));

            _dbContext.ExecuteSqlCommand("Update  BookingTableTransaction set Status=" + bookingTableTransaction.Status + " where id=" + bookingTableTransaction.Id);
            _dbContext.SaveChanges();
        }
       
     
        /// <summary>
        /// Search products
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="categoryIds">Category identifiers</param>
        /// <param name="manufacturerId">Manufacturer identifier; 0 to load all records</param>
        /// <param name="storeId">Store identifier; 0 to load all records</param>
        /// <param name="vendorId">Vendor identifier; 0 to load all records</param>
        /// <param name="warehouseId">Warehouse identifier; 0 to load all records</param>
        /// <param name="productType">Product type; 0 to load all records</param>
        /// <param name="visibleIndividuallyOnly">A values indicating whether to load only products marked as "visible individually"; "false" to load all records; "true" to load "visible individually" only</param>
        /// <param name="markedAsNewOnly">A values indicating whether to load only products marked as "new"; "false" to load all records; "true" to load "marked as new" only</param>
        /// <param name="featuredProducts">A value indicating whether loaded products are marked as featured (relates only to categories and manufacturers). 0 to load featured products only, 1 to load not featured products only, null to load all products</param>
        /// <param name="priceMin">Minimum price; null to load all records</param>
        /// <param name="priceMax">Maximum price; null to load all records</param>
        /// <param name="productTagId">Product tag identifier; 0 to load all records</param>
        /// <param name="keywords">Keywords</param>
        /// <param name="searchDescriptions">A value indicating whether to search by a specified "keyword" in product descriptions</param>
        /// <param name="searchManufacturerPartNumber">A value indicating whether to search by a specified "keyword" in manufacturer part number</param>
        /// <param name="searchSku">A value indicating whether to search by a specified "keyword" in product SKU</param>
        /// <param name="searchProductTags">A value indicating whether to search by a specified "keyword" in product tags</param>
        /// <param name="languageId">Language identifier (search for text searching)</param>
        /// <param name="filteredSpecs">Filtered product specification identifiers</param>
        /// <param name="orderBy">Order by</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <param name="overridePublished">
        /// null - process "Published" property according to "showHidden" parameter
        /// true - load only "Published" products
        /// false - load only "Unpublished" products
        /// </param>
        /// <returns>Products</returns>
        public virtual IPagedList<BookingTableMaster> SearchBooking(
            int pageIndex = 0,
             int pageSize = int.MaxValue,
             int TableNumber = 0,
             int storeId = 0,
             int vendorId = 0,
             bool showHidden = true)
        {
            var query = _BookingMasterRepository.Table.ToList();
            if (storeId != 0 && storeId>0)
            {
                query = query.Where(x => x.StoreId == storeId).ToList();
            }
            if (vendorId != 0 && vendorId > 0)
            {
                query = query.Where(x => x.VendorId == vendorId).ToList();
            }
            if (TableNumber != 0 && TableNumber > 0)
            {
                query = query.Where(x => x.TableNo == TableNumber).ToList();
            }
            var smss = new PagedList<BookingTableMaster>(query, pageIndex, pageSize);
            return smss;
        }

        /// <summary>
        /// Search products
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="categoryIds">Category identifiers</param>
        /// <param name="manufacturerId">Manufacturer identifier; 0 to load all records</param>
        /// <param name="storeId">Store identifier; 0 to load all records</param>
        /// <param name="vendorId">Vendor identifier; 0 to load all records</param>
        /// <param name="warehouseId">Warehouse identifier; 0 to load all records</param>
        /// <param name="productType">Product type; 0 to load all records</param>
        /// <param name="visibleIndividuallyOnly">A values indicating whether to load only products marked as "visible individually"; "false" to load all records; "true" to load "visible individually" only</param>
        /// <param name="markedAsNewOnly">A values indicating whether to load only products marked as "new"; "false" to load all records; "true" to load "marked as new" only</param>
        /// <param name="featuredProducts">A value indicating whether loaded products are marked as featured (relates only to categories and manufacturers). 0 to load featured products only, 1 to load not featured products only, null to load all products</param>
        /// <param name="priceMin">Minimum price; null to load all records</param>
        /// <param name="priceMax">Maximum price; null to load all records</param>
        /// <param name="productTagId">Product tag identifier; 0 to load all records</param>
        /// <param name="keywords">Keywords</param>
        /// <param name="searchDescriptions">A value indicating whether to search by a specified "keyword" in product descriptions</param>
        /// <param name="searchManufacturerPartNumber">A value indicating whether to search by a specified "keyword" in manufacturer part number</param>
        /// <param name="searchSku">A value indicating whether to search by a specified "keyword" in product SKU</param>
        /// <param name="searchProductTags">A value indicating whether to search by a specified "keyword" in product tags</param>
        /// <param name="languageId">Language identifier (search for text searching)</param>
        /// <param name="filteredSpecs">Filtered product specification identifiers</param>
        /// <param name="orderBy">Order by</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <param name="overridePublished">
        /// null - process "Published" property according to "showHidden" parameter
        /// true - load only "Published" products
        /// false - load only "Unpublished" products
        /// </param>
        /// <returns>Products</returns>
        public virtual IPagedList<BookingTableTransaction> SearchBookingTables(
            int pageIndex = 0,
             int pageSize = int.MaxValue,
             DateTime? BookingSearchDate = null,
             string From = "",
             string To = "",
             int storeId = 0,
             int vendorId = 0,
              string SearchCustomerName = "",
              int SearchTableNumber = 0,
             string SearchContactNumber = "",
             int SearchBookingStatusId = 0,
             bool showHidden = true)
        {
            var query = _BookingTransactionRepository.Table.ToList();
            if (storeId != 0)
            {
                query = query.Where(x => x.StoreId == storeId).ToList();
            }
            if (vendorId != 0)
            {
                query = query.Where(x => x.VendorId == vendorId).ToList();
            }
            if (SearchTableNumber != 0)
            {
                query = query.Where(x => x.TableNumber == SearchTableNumber).ToList();
            }
            if (SearchBookingStatusId != 0)
            {
                query = query.Where(x => x.Status == SearchBookingStatusId).ToList();
            }
            if (BookingSearchDate!=null)
            {
                query = query.Where(x => x.BookingDate == BookingSearchDate).ToList();
            }
            if (SearchContactNumber != "" && SearchContactNumber != null)
            {
                query = query.Where(x => x.MobileNumber.Contains(SearchContactNumber)).ToList();
            }
            if (From.ToString()!="0")
            {
                DateTime ConvertFromTiming = DateTime.ParseExact(From,
                                    "h:mm tt", CultureInfo.InvariantCulture);
                TimeSpan span = ConvertFromTiming.TimeOfDay;
                query = query.Where(x => x.BookingTimeFrom == span).ToList();
            }
            if (SearchCustomerName != "" && SearchCustomerName!=null)
            {
                query = query.Where(x => x.CustomerName.ToLower().ToString().Contains(SearchCustomerName.ToLower().ToString())).ToList();
            }
            if (To.ToString()!="0")
            {
                DateTime ConvertToTiming = DateTime.ParseExact(To,
                                    "h:mm tt", CultureInfo.InvariantCulture);
                TimeSpan ToSpan = ConvertToTiming.TimeOfDay;
                query = query.Where(x => x.BookingTimeTo == ToSpan).ToList();
            }
            query = query.OrderByDescending(x => x.Id).ToList();
            var smss = new PagedList<BookingTableTransaction>(query, pageIndex, pageSize);
            return smss;
        }
        #endregion

        #region Booking Table Master

        /// <summary>
        /// GetMasterBookingById
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public BookingTableMaster GetMasterBookingById(int bookingId)
        {
            var booking = _BookingMasterRepository.Table.Where(x => x.Id == bookingId).FirstOrDefault();
            return booking;
        }
        /// <summary>
        /// DeleteMasterBookingById
        /// </summary>
        /// <param name="bookingTableMaster"></param>
        /// <returns></returns>
        public bool DeleteMasterBookingById(BookingTableMaster bookingTableMaster)
        {

            var bookingDetailsCount = _BookingMasterRepository.Table.Where(x => x.BookingDetailsId == bookingTableMaster.BookingDetailsId).Count();
            if (bookingDetailsCount == 1)
            {
                _BookingMasterDetailsRepository.Delete(_BookingMasterDetailsRepository.Table.Where(x => x.Id == bookingTableMaster.BookingDetailsId).FirstOrDefault());
            }
            _BookingMasterRepository.Delete(bookingTableMaster);
            return true;
        }
        /// <summary>
        /// CreateBookingMaster
        /// </summary>
        /// <param name="bookingTableMaster"></param>
        /// <returns></returns>
        public bool CreateBookingMaster(BookingTableMaster bookingTableMaster)
        {
            if (_BookingMasterRepository.Table.Where(x => x.StoreId == bookingTableMaster.StoreId && x.VendorId == bookingTableMaster.VendorId && x.TableNo == bookingTableMaster.TableNo).Any())
            {
                return false;
            }

            var BookingMasterExist = _BookingMasterRepository.Table.Where(x => x.StoreId == bookingTableMaster.StoreId && x.VendorId == bookingTableMaster.VendorId).FirstOrDefault();
            if (BookingMasterExist == null)
            {
                BookingTableMasterDetail bookingTableMasterDetails = new BookingTableMasterDetail();
                bookingTableMasterDetails.CreatedAt = DateTime.UtcNow;
                bookingTableMasterDetails.ImageId = 1;
                //bookingTableMasterDetails.ImageURL = "";
                bookingTableMasterDetails.BookingMasterId = 1;
                _BookingMasterDetailsRepository.Insert(bookingTableMasterDetails);

                bookingTableMaster.BookingDetailsId = bookingTableMasterDetails.Id;
                _BookingMasterRepository.Insert(bookingTableMaster);
            }
            else
            {
                bookingTableMaster.TableAvailableFrom = BookingMasterExist.TableAvailableFrom;
                bookingTableMaster.TableAvailableTo = BookingMasterExist.TableAvailableTo;
                bookingTableMaster.BookingDetailsId = BookingMasterExist.Id;
                _BookingMasterRepository.Insert(bookingTableMaster);
            }



            return true;
        }
        #endregion
    }
}
