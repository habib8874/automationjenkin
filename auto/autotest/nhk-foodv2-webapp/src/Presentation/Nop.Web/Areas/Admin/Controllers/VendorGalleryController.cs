﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Vendors;
using Nop.Core.Domain.VendorsGallery;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Services.VendorsGallery;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Models.VendorGallery;
using Nop.Web.Areas.Admin.Models.Vendors;
using Nop.Web.Framework.Models.Extensions;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class VendorGalleryController : BaseAdminController
    {
        #region Fields

        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly ILocalizationService _localizationService;
        private readonly IVendorService _vendorService;
        private readonly IPermissionService _permissionService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly IPictureService _pictureService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly VendorSettings _vendorSettings;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IAddressService _addressService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IVendorGalleryService _vendorGalleryService;
        private readonly IVendorModelFactory _vendorModelFactory;
        private readonly INotificationService _notificationService;

        #endregion

        #region Ctor

        public VendorGalleryController(ICustomerService customerService, 
            ILocalizationService localizationService,
            IVendorService vendorService, 
            IPermissionService permissionService,
            IUrlRecordService urlRecordService,
            ILanguageService languageService,
            ILocalizedEntityService localizedEntityService,
            IPictureService pictureService,
            IDateTimeHelper dateTimeHelper,
            VendorSettings vendorSettings,
            ICustomerActivityService customerActivityService,
            IAddressService addressService,
            ICountryService countryService,
            IStateProvinceService stateProvinceService,
            IWorkContext workContext,
            IStoreService storeService,
            IVendorGalleryService vendorGalleryService,
            IVendorModelFactory vendorModelFactory,
            INotificationService notificationService
            )
        {
            _customerService = customerService;
            _localizationService = localizationService;
            _vendorService = vendorService;
            _permissionService = permissionService;
            _urlRecordService = urlRecordService;
            _languageService = languageService;
            _localizedEntityService = localizedEntityService;
            _pictureService = pictureService;
            _dateTimeHelper = dateTimeHelper;
            _vendorSettings = vendorSettings;
            _customerActivityService = customerActivityService;
            _addressService = addressService;
            _countryService = countryService;
            _stateProvinceService = stateProvinceService;
            _workContext = workContext;
            _storeService = storeService;
            _vendorGalleryService = vendorGalleryService;
            _vendorModelFactory = vendorModelFactory;
            _notificationService = notificationService;
        }

        #endregion
            
        #region Utilities

        protected virtual void UpdatePictureSeoNames(Vendor vendor)
        {
            var picture = _pictureService.GetPictureById(vendor.PictureId);
            if (picture != null)
                _pictureService.SetSeoFilename(picture.Id, _pictureService.GetPictureSeName(vendor.Name));
        }

        protected virtual void UpdateStores(Vendor vendor, VendorModel model)
        {
                
        }
        protected virtual void UpdateLocales(Vendor vendor, VendorModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(vendor,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(vendor,
                    x => x.Description,
                    localized.Description,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(vendor,
                    x => x.MetaKeywords,
                    localized.MetaKeywords,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(vendor,
                    x => x.MetaDescription,
                    localized.MetaDescription,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(vendor,
                    x => x.MetaTitle,
                    localized.MetaTitle,
                    localized.LanguageId);

                //search engine name
                var seName = _urlRecordService.ValidateSeName(vendor, localized.SeName, localized.Name, false);
                _urlRecordService.SaveSlug(vendor, seName, localized.LanguageId);
            }
        }

        

        #endregion

        [HttpPost]
        public virtual IActionResult BulkEditUpdate(IEnumerable<VendorGalleryDataModel> vendorGalleryListModels)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendorGallery))
                return AccessDeniedView();

            if (vendorGalleryListModels != null)
            {
                foreach (var pModel in vendorGalleryListModels)
                {
                    //update
                    var vendorGallerytoUpdate = _vendorGalleryService.GetVendorGalleryById(pModel.Id);
                   
                    if (vendorGallerytoUpdate != null)
                    {
                        //a vendor should have access only to his products
                        vendorGallerytoUpdate.Active = pModel.Active;
                        _vendorGalleryService.UpdateVendorGallery(vendorGallerytoUpdate);
                    }
                }
            }

            return new NullJsonResult();
        }


        [HttpPost]
        public virtual IActionResult DeleteSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendorGallery))
                return AccessDeniedView();

            foreach (var item in selectedIds)
            {
                var vendorGallery = _vendorGalleryService.GetVendorGalleryById(item);
                if (vendorGallery != null)
                {
                    _vendorGalleryService.DeleteVendorGallery(vendorGallery);
                }
                else
                {
                    _notificationService.ErrorNotification(_localizationService.GetResource("Gallery Not found"));
                }

            }

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.VendorGallery.Gallery.Deleted"));

            return Json(new { Result = true });
        }
        #region Vendors

        //list
        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendorGallery))
                return AccessDeniedView();

            var model = new VendorGalleryListSearchModel();
            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
            {
                var customerId = _workContext.CurrentCustomer.Id;
                var customer = _customerService.GetAllCustomers().Where(x => x.Id == customerId).FirstOrDefault();

                //vendor
                model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                var vandorslist = _vendorService.GetAllVendors();
                if (vandorslist != null && vandorslist.Any())
                {
                    foreach (var c in vandorslist)
                    {
                        model.AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
                model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                foreach (var s in _storeService.GetAllStores())
                {
                    model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });
                }

            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                var customer = _customerService.GetAllCustomers().Where(x => x.RegisteredInStoreId == _workContext.CurrentCustomer.RegisteredInStoreId).ToList();
                model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                var vandorslist = _vendorService.GetAllVendors();
                if (vandorslist != null && vandorslist.Any())
                {
                    foreach (var c in vandorslist)
                    {
                        if (c.StoreId == stores.Id)
                            model.AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
            }
            var VideoGalleyList = from VendorGalleryEnum d in Enum.GetValues(typeof(VendorGalleryEnum))
                                  select new { ID = (int)d, Name = d.ToString() };
            model.AvailableGalleryType.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
            foreach (var item in VideoGalleyList)
            {
                model.AvailableGalleryType.Add(new SelectListItem { Text = item.Name, Value = item.ID.ToString() });
            }
            model.SetGridPageSize();
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(VendorGalleryListSearchModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendorGallery))
                return AccessDeniedDataTablesJson();

            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
            {
                if (model.SearchStoreId <= 0)
                    model.SearchStoreId = 0;
                if (model.SearchVendorId <= 0)
                    model.SearchVendorId = 0;
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                if (model.SearchStoreId == 0)
                {
                    var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                    model.SearchStoreId = stores.Id;
                }
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
            {
                if (model.SearchVendorId == 0)
                {
                    var vendorId = _customerService.GetAllCustomers().FirstOrDefault(x => x.Id == _workContext.CurrentCustomer.Id).VendorId;
                    model.SearchVendorId = vendorId;
                }
            }
            var vendorGallery = _vendorGalleryService.GetAllVendorGallery(model.SearchName,model.SearchStoreId,model.SearchVendorId,model.GalleryTypeId, model.Page - 1, model.PageSize, true);
            var modelList = new VendorGalleryListModel().PrepareToGrid(model, vendorGallery, () =>
            {
                //fill in model values from the entity
                return vendorGallery.Select(x =>
                {
                    var Vendortable = new VendorGalleryDataModel
                    {
                        Id = x.Id,
                        Active = x.Active,
                        StoreId = x.StoreId,
                        VendorId = x.VendorId,
                        GalleryType = x.GalleryType.ToString(),
                        PictureId = x.PictureId == null ? 0 : x.PictureId.Value,
                        GalleryCaption = x.GalleryCaption,
                        //Seats = x.Seats
                        DisplayOrder = x.DisplayOrder
                    };
                    var values = Enum.GetValues(typeof(VendorGalleryEnum));
                    foreach (var s in values)
                    {
                        if ((int)s == (int)x.GalleryType)
                        {
                            Vendortable.GalleryType = (Enum.GetName(typeof(VendorGalleryEnum), s));
                        }
                    }
                    Vendortable.Store = _storeService.GetStoreById(x.StoreId) == null ? "Na" : _storeService.GetStoreById(x.StoreId).Name;
                    Vendortable.Vendor = _vendorService.GetVendorById(x.VendorId) == null ? "Na" : _vendorService.GetVendorById(x.VendorId).Name;
                    if (x.PictureId != null)
                    {
                        var defaultProductPicture = _pictureService.GetPictureById(x.PictureId.Value);
                        Vendortable.PictureThumbnailUrl = _pictureService.GetPictureUrl(defaultProductPicture, 0, true);
                    }
                    else
                    {
                        Vendortable.VideoThumbnailUrl = x.GalleryURL;
                    }

                    return Vendortable;
                });
            });
            return Json(modelList);

        }
        public virtual IList<SelectListItem> GetAvailableVendors(string Store)
        {
            string select = "0";
            IList<SelectListItem> AvailableVendors = new List<SelectListItem>();
            if (Store == select)
            {

                AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
            }
            else
            {
                int storeId = Convert.ToInt32(Store);
                AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "" });
                var vandorslist = _vendorService.GetAllVendors();
                if (vandorslist != null && vandorslist.Any())
                {
                    foreach (var c in vandorslist)
                    {
                        if (c.StoreId == storeId)
                            AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
            }
            //var storeId = _workContext.CurrentCustomer.RegisteredInStoreId;

            return AvailableVendors;
        }
        //create
        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendorGallery))
                return AccessDeniedView();


            var model = new VendorGalleryModel();
            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
            {
                var customerId = _workContext.CurrentCustomer.Id;
                var customer = _customerService.GetAllCustomers().Where(x => x.Id == customerId).FirstOrDefault();

                //vendor

                model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                foreach (var s in _storeService.GetAllStores())
                {
                    model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });
                }

            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                var customer = _customerService.GetAllCustomers().Where(x => x.RegisteredInStoreId == _workContext.CurrentCustomer.RegisteredInStoreId).ToList();
                model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                var vandorslist = _vendorService.GetAllVendors();
                if (vandorslist != null && vandorslist.Any())
                {
                    foreach (var c in vandorslist)
                    {
                        if (c.StoreId == stores.Id)
                            model.AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
                model.StoreId = stores.Id;
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
            {
                var vendor = _customerService.GetAllCustomers().FirstOrDefault(x => x.Id == _workContext.CurrentCustomer.Id);
                ///*bookingTableTransaction.StoreId = vendor.Re*/gisteredInStoreId;
                model.VendorId = vendor.VendorId;
                model.StoreId = vendor.RegisteredInStoreId;
            }
            var VideoGalleyList = from VendorGalleryEnum d in Enum.GetValues(typeof(VendorGalleryEnum))
              select new { ID = (int)d, Name = d.ToString() };
            foreach (var item in VideoGalleyList)
            {
                model.AvailableGalleryType.Add(new SelectListItem { Text = item.Name, Value = item.ID.ToString() });
            }
            return View(model);
        }
        [HttpPost]
        public virtual IActionResult Create(VendorGalleryModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendorGallery))
                return AccessDeniedView();
            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
            {
                var customerId = _workContext.CurrentCustomer.Id;
                var customer = _customerService.GetAllCustomers().Where(x => x.Id == customerId).FirstOrDefault();

                //vendor

                model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                foreach (var s in _storeService.GetAllStores())
                {
                    model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });
                }
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                var customer = _customerService.GetAllCustomers().Where(x => x.RegisteredInStoreId == _workContext.CurrentCustomer.RegisteredInStoreId).ToList();
                model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                var vandorslist = _vendorService.GetAllVendors();
                if (vandorslist != null && vandorslist.Any())
                {
                    foreach (var c in vandorslist)
                    {
                        if (c.StoreId == stores.Id)
                            model.AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
                model.StoreId = stores.Id;
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
            {
                var vendor = _customerService.GetAllCustomers().FirstOrDefault(x => x.Id == _workContext.CurrentCustomer.Id);
                model.StoreId = vendor.RegisteredInStoreId;
                model.VendorId = vendor.VendorId;
            }
            var VideoGalleyList = from VendorGalleryEnum d in Enum.GetValues(typeof(VendorGalleryEnum))
                                  select new { ID = (int)d, Name = d.ToString() };
            foreach (var item in VideoGalleyList)
            {
                model.AvailableGalleryType.Add(new SelectListItem { Text = item.Name, Value = item.ID.ToString() });
            }

            if (model.StoreId == 0)
            {

                _notificationService.ErrorNotification("Please Select Store");
                return View(model);
            }
            if (model.VendorId == 0)
            {

                _notificationService.ErrorNotification("Please Select Merchant");
                return View(model);
            }
            if (model.GalleryType == (int)VendorGalleryEnum.Image)
            {
                if(model.PictureId==0)
                {
                    _notificationService.ErrorNotification("Please Upload Image");
                    return View(model);
                }
                
            }
            if (model.GalleryType == (int)VendorGalleryEnum.Video)
            {
                if (string.IsNullOrEmpty(model.GalleryURL))
                {
                    _notificationService.ErrorNotification("Please Provide URL");
                    return View(model);
                }

            }
            if (ModelState.IsValid)
            {

                VendorGallery vendorGallary = new VendorGallery();
                vendorGallary.StoreId = model.StoreId;
                vendorGallary.VendorId = model.VendorId;
                vendorGallary.GalleryCaption = model.GalleryCaption;
                vendorGallary.GalleryType = model.GalleryType;
                if(vendorGallary.GalleryType==(int)VendorGalleryEnum.Image)
                {
                    vendorGallary.PictureId = model.PictureId;
                }
                else
                {
                    vendorGallary.GalleryURL = model.GalleryURL;
                }
                vendorGallary.Active = model.Active;
                vendorGallary.Deleted = false;
                vendorGallary.CreatedDt = DateTime.Now;
                _vendorGalleryService.InsertVendorGallery(vendorGallary);
                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.VendorsGallery.Added"));

                
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
           // PrepareVendorModel(model, null, true, true);
            return View(model);
        }

      
        
        public virtual bool GetGalleryType(string GalleryTypeId)
        {
            int _GalleryTypeId = Convert.ToInt32(GalleryTypeId);
            if (_GalleryTypeId ==(int)VendorGalleryEnum.Image)
                return true;
            else
            {
                return false;
            }
        }


        #endregion


    }
    public enum VendorGalleryEnum
    {
        Image = 1,
        Video = 2
    }
}