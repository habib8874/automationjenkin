﻿using Dapper;
using Microsoft.Extensions.Configuration;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Repository;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModel;
using NHKCustomerApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;

namespace NHKCustomerApplication.Services
{
	public class CommonService : ICommonService
	{
		#region Fields
		private readonly ecuadordevContext _dbcontext;
		public string CurrencySymbol { get; set; }
		private string connectionString = string.Empty;
		private readonly IConfiguration _configuration;
		private readonly IOrderServices _orderServices;
		private readonly IUserRepository _userRepository;
		private const string FAVORITEMERCHANTS = "NB_GetFavoriteMerchantsByCustomerId";
		private const string ENTITYREVIEWS = "NB_GetEntityReviews";

		#endregion

		#region Constructor
		public CommonService(IUserRepository userRepository, ecuadordevContext dbcontext, IConfiguration configuration,
							 IOrderServices orderServices)
		{
			_dbcontext = dbcontext;
			_configuration = configuration;
			_orderServices = orderServices;
			this._userRepository = userRepository;
			connectionString = _configuration.GetSection("ConnectionStrings").GetSection("ecuadordatabase").Value;

		}

		#endregion

		#region Methods

		/// <summary>
		/// Get vendor slider pictures
		/// </summary>
		/// <param name="vendorId">Vendor identifier</param>
		/// <param name="storeId">Store identifier</param>
		/// <returnsList of PictureModel></returns>
		public List<PictureModel> GetVendorSliderPictures(int vendorId, int storeId)
		{
			var lstPictureModel = new List<PictureModel>();
			var vendorSliderPictures = _dbcontext.NB_Vendor_SliderPicture_Mapping.Where(x => x.VendorId == vendorId).ToList();
			var pictureSize = Convert.ToInt32(_dbcontext.Setting.FirstOrDefault(x => x.Name.Contains("mediasettings.vendorSliderPictureSize") && x.StoreId == storeId)?.Value ?? "150");
			string storeUrl = _dbcontext.Store.FirstOrDefault(x => x.Id == storeId)?.Url ?? "";

			foreach (var item in vendorSliderPictures)
			{
				if (item.PictureId > 0)
				{
					var pictureAltText = _dbcontext.Picture.Where(x => x.Id == item.PictureId)?.FirstOrDefault().AltAttribute;
					var pictureUrl = Helper.GetPictureUrl(Convert.ToInt32(item.PictureId), storeUrl, pictureSize);

					var t = new PictureModel()
					{
						PictureId = Convert.ToInt32(item.PictureId),
						PictureUrl = pictureUrl,
						AltText = pictureAltText
					};
					lstPictureModel.Add(t);
				}
			}
			return lstPictureModel;
		}

		/// <summary>
		/// Get store slider pictures
		/// </summary>
		/// <param name="storeId">Store identifier</param>
		/// <returnsList of PictureModel></returns>
		public List<PictureModel> GetStoreSliderPictures(int storeId)
		{
			var lstPictureModel = new List<PictureModel>();
			var PictureId = _dbcontext.Setting.Where(x => x.Name.ToLower().Contains("nivoslidersettings.picture") && x.StoreId == storeId).ToList();
			string storeUrl = _dbcontext.Store.FirstOrDefault(x => x.Id == storeId)?.Url ?? "";
			int pictureSize = 100;
			var sliderPictureSize = _dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productbannerpicturesize")).FirstOrDefault();
			if (sliderPictureSize != null)
				pictureSize = Convert.ToInt32(sliderPictureSize.Value);
			foreach (var item in PictureId)
			{
				if (!string.IsNullOrEmpty(item.Value))
				{
					var t = new PictureModel()
					{
						PictureId = Convert.ToInt32(item.Value),
						PictureUrl = Helper.GetPictureUrl(Convert.ToInt32(item.Value), storeUrl, pictureSize)
					};
					lstPictureModel.Add(t);
				}
			}
			return lstPictureModel;
		}

		/// <summary>
		/// Get slider pictures by geo location
		/// </summary>
		/// <param name="latitude"></param>
		/// <param name="langitute"></param>
		/// <param name="storeId"></param>
		/// <returns>List of pictures</returns>
		public IList<PictureModel> GetSliderPicturesByGeoLocation(string latitude, string langitute, int storeId)
		{
			var listPictureModel = new List<PictureModel>();
			var geofaciningSettingIds = new List<int>();
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				var result = db.Query<int>("GetSliderPicturesByLongLat", new
				{
					latlong = latitude + " " + langitute,
					StoreId = storeId
				}, commandType: CommandType.StoredProcedure);
				//return result;
				geofaciningSettingIds = result?.ToList() ?? null;
			}
			if (geofaciningSettingIds != null && geofaciningSettingIds.Any())
			{
				var geofancingPictureIds = new List<int>();
				var geoFancingSettings = _dbcontext.Setting.Where(s => geofaciningSettingIds.Contains(s.Id));
				if (geoFancingSettings != null && geofaciningSettingIds.Any())
				{
					foreach (var setting in geoFancingSettings)
					{
						//get pictureid setting
						var resultString = Regex.Match(setting.Name, @"\d+").Value;
						var pictureNumber = Int32.Parse(resultString);
						var pictureIdStr = _dbcontext.Setting.FirstOrDefault(s => s.Name == "nivoslidersettings.picture" + pictureNumber + "id" && s.StoreId == storeId)?.Value ?? string.Empty;
						int.TryParse(pictureIdStr, out int pictureId);
						if (pictureId > 0)
							geofancingPictureIds.Add(pictureId);
					}
				}

				//if we get pictureids, than bind model
				if (geofancingPictureIds != null && geofancingPictureIds.Any())
				{
					string storeUrl = _dbcontext.Store.FirstOrDefault(x => x.Id == storeId)?.Url ?? "";
					int pictureSize = 100;
					var sliderPictureSize = _dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productbannerpicturesize")).FirstOrDefault();
					if (sliderPictureSize != null)
						pictureSize = Convert.ToInt32(sliderPictureSize.Value);
					foreach (var pictureId in geofancingPictureIds)
					{
						var pictureModel = new PictureModel()
						{
							PictureId = pictureId,
							PictureUrl = Helper.GetPictureUrl(pictureId, storeUrl, pictureSize)
						};
						listPictureModel.Add(pictureModel);
					}
				}
			}

			return listPictureModel;
		}

		/// <summary>
		/// Db of All Top Rated Rest List [TakeAway + Delivery]
		/// </summary>
		/// <param name="requestModel">Store identifier</param>
		/// <returnsList of PictureModel></returns>
		public string GetTopRatedCooknRestDbResult(TopRatedRestnChefModel requestModel)
		{
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				string result = db.Query<string>("GetTopRatedCooknRest_Validation", new
				{
					requestModel.ApiKey,
					requestModel.StoreId,
					requestModel.RestType,
					latLongPos = requestModel.LatPos + " " + requestModel.LongPos,
					requestModel.SkipLocationSearch,
					merchantCategoryIds = string.IsNullOrWhiteSpace(requestModel.MerchantCategoryIds) ? "" : requestModel.MerchantCategoryIds,
					requestModel.SearchValue
				}, commandType: CommandType.StoredProcedure).FirstOrDefault();
				return result;
			}
		}

		/// <summary>
		/// Top Rated Rest List [TakeAway + Delivery]
		/// </summary>
		/// <param name="requestModel">Store identifier</param>
		/// <returnsList of PictureModel></returns>
		public List<CooknRestV2> GetTopRatedCooknRest(TopRatedRestnChefModel requestModel, List<int> listmodel)
		{
			var TopRatedChefnRestaurantlist = new List<CooknRestV2>();
			List<CooknRestV2> TopRatedChefnRestaurant = new List<CooknRestV2>();
			var isApproved = _dbcontext.Setting.Where(x => x.Name.Contains("ratingreviewsettings.merchant.isapprovalrequired") && x.StoreId == requestModel.StoreId).Any() ? Convert.ToBoolean(_dbcontext.Setting.Where(x => x.Name.Contains("ratingreviewsettings.merchant.isapprovalrequired") && x.StoreId == requestModel.StoreId).FirstOrDefault().Value) : false;
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				SqlCommand command = new SqlCommand("getvendorList", connection);
				command.Parameters.Add(new SqlParameter("@StoreId", requestModel.StoreId));
				command.CommandType = CommandType.StoredProcedure;
				connection.Open();
				SqlDataReader reader = command.ExecuteReader();
				try
				{
					while (reader.Read())
					{
						CooknRestV2 cooknRestV2 = new CooknRestV2()
						{
							RestnCookId = Convert.ToInt32(reader["RestnCookId"]),
							CooknRestName = Convert.ToString(reader["CooknRestName"]),
							PictureId = Convert.ToInt32(reader["PictureId"]),
							IsOpen = Convert.ToBoolean(reader["IsOpen"]),
							Geolocation = Convert.ToString(reader["Geolocation"]),
							RestnCookAddress = Convert.ToString(reader["RestnCookAddress"]),
							Distance = Convert.ToString(reader["Distance"]),
							OpenTime = Convert.ToString(reader["Opentime"]),
							CloseTime = Convert.ToString(reader["Closetime"]),
							CooknRestImageURL = Convert.ToString(reader["CooknRestImageURL"]),
							AvailableType = Convert.ToInt32(reader["AvailableType"]),
							PhoneNumber = Convert.ToString(reader["PhoneNumber"]),
							Description = Convert.ToString(reader["Description"]),
							Latitude = Convert.ToDouble(reader["Latitude"]),
							Longitude = Convert.ToDouble(reader["Longitude"])
						};
						TopRatedChefnRestaurant.Add(cooknRestV2);
					}
				}
				finally
				{
					// Always call Close when done reading.
					connection.Close();
				}
			}
			if (TopRatedChefnRestaurant.Any())
			{
				if (listmodel.Any())
				{
					foreach (var item in TopRatedChefnRestaurant)
					{
                        item.MerchantOffer = Helper.GetMerchantOffers(requestModel.StoreId, item.RestnCookId, _dbcontext);
                        if (item.MerchantOffer != null || requestModel.GetMerchantOffer == false)
						{
							if (listmodel.Contains(item.RestnCookId))
							{
								string[] venLtLngStrArr = item.Geolocation.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
								double latRest = Convert.ToDouble(venLtLngStrArr[0]);
								double longRest = Convert.ToDouble(venLtLngStrArr[1]);
								var distancetype = _dbcontext.Setting.Where(x => x.Name.Contains("setting.customer.distancetype") && x.StoreId == requestModel.StoreId).FirstOrDefault()?.Value ?? "";
								item.Distance = String.Format("{0:0.00}" + " " + distancetype, Helper.distance(requestModel.LatPos, requestModel.LongPos, latRest, longRest, "K"));

								item.IsRestAvailable = false;

								var isVendorSchedule = item.AvailableType;
								if (isVendorSchedule != null && isVendorSchedule != 0)
								{
									if (isVendorSchedule == (int)AvailableTypeEnum.AllDay)
									{
										var AllDaySchedulesTimimgs = _dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.AllDay).ToList();
										if (AllDaySchedulesTimimgs.Any())
										{
											//var CurrentTime = DateTime.Now.TOS;
											var CurrentTime = Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, _dbcontext, requestModel.CustomerId).TimeOfDay;
											foreach (var __item in AllDaySchedulesTimimgs)
											{
												if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
												{
													item.IsRestAvailable = true;
												}
												DateTime fromTime = DateTime.Today.Add(__item.ScheduleFromTime);
												string displayFromTime = fromTime.ToString("hh:mm tt");
												DateTime toTime = DateTime.Today.Add(__item.ScheduleToTime);
												string displayToTime = toTime.ToString("hh:mm tt");
												string fromToTime = displayFromTime + " - " + displayToTime;

												item.OpenCloseTime.Add(fromToTime);
											}
										}
										else
										{
											item.IsRestAvailable = true;
										}
									}
									else if (isVendorSchedule == (int)AvailableTypeEnum.Custom)
									{
										var dayofWeek = (int)Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, _dbcontext, requestModel.CustomerId).DayOfWeek;
										var AllDaySchedulesTimimgs = _dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.Custom && x.AvailableOn == dayofWeek).ToList();
										if (AllDaySchedulesTimimgs.Any())
										{
											//var CurrentTime = DateTime.Now.TOS;
											var CurrentTime = Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, _dbcontext, requestModel.CustomerId).TimeOfDay;
											foreach (var __item in AllDaySchedulesTimimgs)
											{
												if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
												{
													item.IsRestAvailable = true;
												}
												DateTime fromTime = DateTime.Today.Add(__item.ScheduleFromTime);
												string displayFromTime = fromTime.ToString("hh:mm tt");
												DateTime toTime = DateTime.Today.Add(__item.ScheduleToTime);
												string displayToTime = toTime.ToString("hh:mm tt");
												string fromToTime = displayFromTime + " - " + displayToTime;
												item.OpenCloseTime.Add(fromToTime);

											}
										}
										else
										{
											item.IsRestAvailable = true;
										}
									}
									else
									{
										item.IsRestAvailable = true;
									}
								}
								else
								{
									item.IsRestAvailable = true;
								}



								//check merchat is in favorite list for current customer
								item.IsInFavorite = Helper.IsMerchantInFavorite(item.RestnCookId, requestModel.CustomerId, _dbcontext);
								TopRatedChefnRestaurantlist.Add(item);
							}

							//get merchant average rating and count (removed old logic and applied new one)
							var avgMerchantRating = _orderServices.GetAverageRating(requestModel.StoreId, item.RestnCookId, (int)NHKCustomerApplication.Utilities.ReviewType.Merchant, out int merchantReviewCount);
							item.Rating = avgMerchantRating;
							item.RatingCount = merchantReviewCount;
						}

					}
				}
			}
			return TopRatedChefnRestaurantlist;
		}

		private List<CooknChefCatProductsV2> GetSubCategories(int catId, List<RestProductResponseModel> resultproducts, List<ProductAttributesV2> attributes)
		{
			List<CooknRestProductProductAttributesResponse> res = new List<CooknRestProductProductAttributesResponse>();
			var nullattr = res.Where(x => x.IsRequired).ToList();
			return resultproducts.Where(x => x.CategoryId == catId)?.
			  Select(x => new CooknChefCatProductsV2
			  {
				  Available = x.Available,
				  Cuisine = !string.IsNullOrEmpty(x.Cuisine) ? x.Cuisine : "",
				  CustomerImages = GetCustomerImages(x.CustomeURL),
				  Description = !string.IsNullOrEmpty(x.ShortDescription) ? x.ShortDescription : "",
				  ImageUrl = GetProductImages(x.ProductPics, x.ChefBaseUrl, x.PictureSize),
				  IsProductAttributesExist = x.IsProductAttributesExist,
				  ItemId = x.ItemId,
				  ItemName = x.itemName,
				  Preferences = GetPreferences(x.ProductPreferences, x.ChefBaseUrl, x.PictureSize),
				  Price = x.ProductPrice.HasValue ? x.ProductPrice.Value.ToString() : "0:00",
				  OldPrice = x.OldPrice,
				  ProductProductAttributes = x.IsProductAttributesExist ? GetProductProductAttributes(x.ItemId, attributes) : nullattr,
				  Quantity = x.Quantity.HasValue ? x.Quantity.Value : 0,
				  IsWishList = x.IsWishList,
				  WishListId = x.WishListId.HasValue ? x.WishListId.Value : 0,
				  Rating = GetRating(x.Rating),
				  RatingCount = GetRatingCount(x.Rating),
				  ReviewCount = GetReviewCount(x.Rating),

			  })?.
			  OrderBy(x => x.ItemId)?.ToList();
		}

		private List<PreferencesV2> GetPreferences(string URL, string ChefBaseUrl, int? PictureSize)
		{
			if (string.IsNullOrEmpty(URL))
			{
				return new List<PreferencesV2>().Where(x => x.Image == "1").ToList();
			}
			else
			{
				var arrurl = URL.Split("::::");
				if (arrurl.Length > 0)
				{
					int pictureSize = PictureSize.HasValue ? PictureSize.Value : 75;
					List<PreferencesV2> listUrl = new List<PreferencesV2>();
					foreach (string url in arrurl)
					{
						if (!string.IsNullOrEmpty(url))
						{
							var arrurl2 = url.Split(";;;;");
							if (arrurl2.Length > 0 && arrurl2.Length >= 2)
							{
								var pref = new PreferencesV2
								{
									Name = arrurl2[3],
									ImageType = arrurl2[1],
									Image = arrurl2[0]
								};
								int pictureId = Convert.ToInt32(pref.Image);
								string lastPart = Helper.GetFileExtensionFromMimeType(pref.ImageType);
								var prefdetail = arrurl2[2];
								var fileName = !string.IsNullOrEmpty(prefdetail)
											? $"{pictureId:0000000}_{prefdetail}_{pictureSize}.{lastPart}" + "? " + DateTime.Now
											: $"{pictureId:0000000}_{pictureSize}.{lastPart}" + "? " + DateTime.Now;
								pref.Image = ChefBaseUrl + fileName;
								listUrl.Add(pref);
							}
						}
					}
					return listUrl;
				}
				else
				{
					return new List<PreferencesV2>().Where(x => x.Image == "1").ToList();
				}
			}
		}

		private List<CooknChefProductsImagesV2> GetProductImages(string URL, string ChefBaseUrl, int? PictureSize, int storeId = 0)
		{
			int pictureSize = PictureSize.HasValue ? PictureSize.Value : 75;
			if (string.IsNullOrEmpty(URL))
			{
				return new List<CooknChefProductsImagesV2>() { new CooknChefProductsImagesV2 { ProductImageURL = ChefBaseUrl + "default-image_" + pictureSize + ".png?" + DateTime.Now } };
			}
			else
			{
				var arrurl = URL.Split("::::");
				if (arrurl.Length > 0)
				{
					List<CooknChefProductsImagesV2> listUrl = new List<CooknChefProductsImagesV2>();
					foreach (string url in arrurl)
					{
						if (!string.IsNullOrEmpty(url))
						{
							var arrurl2 = url.Split(";;;;");
							if (arrurl2.Length > 0 && arrurl2.Length >= 2)
							{
								var pref = new CooknChefProductsImagesV2
								{
									FileSeo = arrurl2[2],
									MimeType = arrurl2[1],
									ProductImageURL = arrurl2[0]
								};
								if (!string.IsNullOrEmpty(pref.ProductImageURL))
								{
									int pictureId = Convert.ToInt32(pref.ProductImageURL);
									if (pictureId != 0)
									{
										if (storeId == 0)
										{
											string lastPart = Helper.GetFileExtensionFromMimeType(pref.MimeType);
											string thumbFileName = !string.IsNullOrEmpty(pref.FileSeo)
											? $"{pictureId:0000000}_{pref.FileSeo}.{lastPart}" + "? " + DateTime.Now
											: $"{pictureId:0000000}_{pictureSize}.{lastPart}" + "? " + DateTime.Now;
											pref.ProductImageURL = ChefBaseUrl + thumbFileName + "?" + DateTime.Now;
										}
										else
										{
											string storeUrl = _dbcontext.Store.FirstOrDefault(x => x.Id == storeId)?.Url ?? "";
											pref.ProductImageURL = Helper.GetPictureUrl(pictureId, storeUrl, pictureSize);
										}
									}
									else
									{
										pref.ProductImageURL = ChefBaseUrl + "default-image_" + pictureSize + ".png?" + DateTime.Now;
									}
								}
								else
								{
									pref.ProductImageURL = ChefBaseUrl + "default-image_" + pictureSize + ".png?" + DateTime.Now;
								}
								listUrl.Add(pref);
							}
						}
					}
					return listUrl;
				}
				else
				{
					return new List<CooknChefProductsImagesV2>() { new CooknChefProductsImagesV2 { ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now } };
				}
			}
		}

		private List<CustomerImage> GetCustomerImages(string customerURL)
		{
			if (string.IsNullOrEmpty(customerURL))
			{
				return new List<CustomerImage>().Where(x => x.Image == "1").ToList();
			}
			else
			{
				var arrurl = customerURL.Split("::::");
				if (arrurl.Length > 0)
				{
					List<CustomerImage> listUrl = new List<CustomerImage>();
					foreach (string url in arrurl)
					{
						if (!string.IsNullOrEmpty(url))
						{
							listUrl.Add(new CustomerImage { Image = url });
						}
					}
					return listUrl;
				}
				else
				{
					return null;
				}
			}

		}

		private List<CooknRestProductProductAttributesResponse> GetProductProductAttributes(int pid, List<ProductAttributesV2> attributes)
		{
			List<CooknRestProductProductAttributesResponse> res = new List<CooknRestProductProductAttributesResponse>();
			var prodAttributes = attributes.Where(x => x.ProductId == pid).ToList();
			var result = prodAttributes.Select(x => new CooknRestProductProductAttributesResponse
			{
				AttributeName = x.AttributeName,
				IsRequired = x.IsRequired,
				AttributeTypeId = x.AttributeTypeId,
				ProductId = x.ProductId,
				ProductAttributeId = x.ProductAttributeId,
				ProductAttributeMappingId = x.ProductAttributeMappingId
			}).Distinct();
			res = result.Select(x => new CooknRestProductProductAttributesResponse
			{
				AttributeName = x.AttributeName,
				IsRequired = x.IsRequired,
				AttributeTypeId = x.AttributeTypeId,
				ProductId = x.ProductId,
				ProductAttributeId = x.ProductAttributeId,
				ProductAttributeMappingId = x.ProductAttributeMappingId,
				ProductAttribute = prodAttributes.Where(y => y.ProductAttributeId == x.ProductAttributeId && y.ProductAttributeMappingId == x.ProductAttributeMappingId).Select(y => new ProductAttributes
				{
					Name = y.Name,
					Price = y.Price,
					ProductAttributeValueId = y.ProductAttributeValueId,
					IsPreSelected = y.IsPreSelected,
					UsePercentage = y.UsePercentage,
					Currency = this.CurrencySymbol
				}).ToList()
			}).ToList();
			return res;
		}

		private List<CooknChefCatProductsV2> GetProductItems(List<RestProductResponseModel> resultproducts, List<ProductAttributesV2> attributes, int storeId = 0)
		{
			List<CooknRestProductProductAttributesResponse> res = new List<CooknRestProductProductAttributesResponse>();
			var nullattr = res.Where(x => x.IsRequired).ToList();
			return resultproducts?.
			  Select(x => new CooknChefCatProductsV2
			  {
				  Available = x.Available,
				  Cuisine = !string.IsNullOrEmpty(x.Cuisine) ? x.Cuisine : "",
				  CustomerImages = GetCustomerImages(x.CustomeURL),
				  Description = !string.IsNullOrEmpty(x.ShortDescription) ? x.ShortDescription : "",
				  ImageUrl = GetProductImages(x.ProductPics, x.ChefBaseUrl, x.PictureSize, storeId),
				  IsProductAttributesExist = x.IsProductAttributesExist,
				  ItemId = x.ItemId,
				  ItemName = x.itemName,
				  CategoryId = x.CategoryId,
				  CategoryName = x.CategoryName,
				  Preferences = GetPreferences(x.ProductPreferences, x.ChefBaseUrl, x.PictureSize),
				  Price = x.ProductPrice.HasValue ? x.ProductPrice.Value.ToString() : "0:00",
				  OldPrice = x.OldPrice,
				  ParentCategoryId = x.ParentCategoryId,
				  ProductProductAttributes = x.IsProductAttributesExist ? GetProductProductAttributes(x.ItemId, attributes) : nullattr,
				  Quantity = x.Quantity.HasValue ? x.Quantity.Value : 0,
				  IsWishList = x.IsWishList,
				  WishListId = x.WishListId.HasValue ? x.WishListId.Value : 0,
				  Rating = GetRating(x.Rating),
				  RatingCount = GetRatingCount(x.Rating),
				  ReviewCount = GetReviewCount(x.Rating),
				  BrandName = x.BrandName,
				  ProductOffer =Helper.GetProductOffer(storeId,x.ItemId,_dbcontext),
				  TotalRecords = Convert.ToInt32(x.TotalRecords)

			  })?.
			  OrderBy(x => x.ItemId)?.ToList();
		}

		private List<ProductsFilternSorting> GetProductFilterItems(List<RestProductFilterModel> resultproducts, List<ProductAttributesV2> attributes,int storeId)
		{
			List<CooknRestProductProductAttributesResponse> res = new List<CooknRestProductProductAttributesResponse>();
			var nullattr = res.Where(x => x.IsRequired).ToList();
			return resultproducts?.
			  Select(x => new ProductsFilternSorting
			  {
				  itemId = x.Id,
				  Available = x.Available,
				  Cuisine = !string.IsNullOrEmpty(x.Cuisine) ? x.Cuisine : "",
				  description = !string.IsNullOrEmpty(x.ShortDescription) ? x.ShortDescription : "",
				  OldPrice = x.OldPrice,
				  Price = x.Price,
				  itemName = x.Name,
				  ImageUrl = GetProductImages(x.ProductPics, x.ChefBaseUrl, x.PictureSize),
				  CustomerImages = GetCustomerImages(x.CustomeURL),
				  ProductTypeId = x.ProductTypeId,
				  CallPrice = x.CallForPrice,
				  ProductProductAttributes = GetProductProductAttributes(x.Id, attributes),
				  isDeleted = x.Deleted,
				  isPublished = x.Published,
				  IsWishList = x.IsWishList,
				  WishListId = x.WishListId.HasValue ? x.WishListId.Value : 0,
				  ProductPics = x.ProductPics,
				  Rating = GetRating(x.Rating),
				  RatingCount = GetRatingCount(x.Rating),
				  ReviewCount = GetReviewCount(x.Rating),
				  ProductOffer=Helper.GetProductOffer(storeId,x.Id,_dbcontext),
				  Quantity = x.Quantity.HasValue ? x.Quantity.Value : 0,

			  })?.ToList();

		}

		private double GetRating(string rating)
		{
			if (string.IsNullOrEmpty(rating))
			{
				return 0.00;
			}
			else
			{
				var arrurl = rating.Split("::::");
				if (arrurl.Length > 0 && arrurl.Length > 1)
				{
					return Convert.ToDouble(arrurl[1]);
				}
				else
				{
					return 0.00;
				}
			}

		}

		private Int16 GetRatingCount(string rating)
		{
			if (string.IsNullOrEmpty(rating))
			{
				return 0;
			}
			else
			{
				var arrurl = rating.Split("::::");
				if (arrurl.Length > 0 && arrurl.Length > 1)
				{
					return Convert.ToInt16(arrurl[2]);
				}
				else
				{
					return 0;
				}
			}

		}

		private int GetReviewCount(string rating)
		{
			if (string.IsNullOrEmpty(rating))
			{
				return 0;
			}
			else
			{
				var arrurl = rating.Split("::::");
				if (arrurl.Length > 0 && arrurl.Length > 1)
				{
					return Convert.ToInt16(arrurl[0]);
				}
				else
				{
					return 0;
				}
			}
		}

		/// <summary>
		/// Product List
		/// </summary>
		/// <param name="requestModel"></param>
		/// <returns>List of ALl Rated products</returns>
		public List<CooknChefCatProductsV2> GetCooknRestItemsByCategoryIdV1(CooknRestProductByCategoryIds requestModel)
		{
			try
			{
				var pictureSize = _dbcontext.Setting.Where(x => x.Name.Contains("setting.customer.productpictureSize") && x.StoreId == requestModel.StoreId).FirstOrDefault()?.Value ?? "100";
				var result = _userRepository.GetCooknRestItemsByCategoryId(requestModel);
				string currencyCode = result.Products.FirstOrDefault()?.CurrencyCode ?? "";
				if (string.IsNullOrEmpty(currencyCode))
				{
					currencyCode = "USD";
				}
				this.CurrencySymbol = Helper.GetCurrencySymbolFromCode(currencyCode);

				var ProductProductAttributes = new List<ProductAttributesV2>();
				if (result.Products.Any(x => x.IsProductAttributesExist))
				{
					ProductProductAttributes = _userRepository.GetProductProductAttributes(result.Products.Where(x => x.IsProductAttributesExist).Select(x => x.ItemId).ToList());
				}
				if (result.ValidData && result.Products.Count > 0)
				{
					return GetProductItems(result.Products, ProductProductAttributes,requestModel.StoreId);
				}
				return null;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				return null;
			}
		}

		/// <summary>
		/// Product List
		/// </summary>
		/// <param name="requestModel"></param>
		/// <returns>List of ALl Rated products</returns>
		public List<CooknChefCatProductsV2> GetCooknRestItemsByCategoryIdV2(CooknRestProductByCategoryIdsV2 requestModel)
		{
			try
			{
				var pictureSize = _dbcontext.Setting.Where(x => x.Name.Contains("setting.customer.productpictureSize") && x.StoreId == requestModel.StoreId).FirstOrDefault()?.Value ?? "100";
				var result = _userRepository.GetCooknRestItemsByCategoryId__V2(requestModel);
				string currencyCode = result.Products.FirstOrDefault()?.CurrencyCode ?? "";
				if (string.IsNullOrEmpty(currencyCode))
				{
					currencyCode = "USD";
				}
				this.CurrencySymbol = Helper.GetCurrencySymbolFromCode(currencyCode);

				var ProductProductAttributes = new List<ProductAttributesV2>();
				if (result.Products.Any(x => x.IsProductAttributesExist))
				{
					ProductProductAttributes = _userRepository.GetProductProductAttributes(result.Products.Where(x => x.IsProductAttributesExist).Select(x => x.ItemId).ToList());
				}
				if (result.ValidData && result.Products.Count > 0)
				{
					return GetProductItems(result.Products, ProductProductAttributes, requestModel.StoreId);

				}
				return null;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				return null;
			}
		}

		/// <summary>
		/// Filtered ans sorted product list
		/// </summary>
		/// <param name="requestModel"></param>
		/// <returns>List of ALl Rated products</returns>
		public List<ProductsFilternSorting> GetproductsItemByFilternSorting(CustomerProductFilter requestModel)
		{
			try
			{
				var result = _userRepository.GetProductItemsFilternSort(requestModel);
				var ProductProductAttributes = new List<ProductAttributesV2>();
				if (result.Products.Any())
				{
					ProductProductAttributes = _userRepository.GetProductProductAttributes(result.Products.Select(x => x.Id).ToList());
				}
				if (result.ValidData && result.Products.Count > 0)
				{
					return GetProductFilterItems(result.Products, ProductProductAttributes,requestModel.StoreId);
				}
				return null;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				return null;
			}
		}

		#region Favorite merchant

		/// <summary>
		/// Get favorite merchant by customer using SP
		/// </summary>
		/// <param name="requestModel"></param>
		/// <returns>CustomerFavoriteMerchantResponce</returns>
		public CustomerFavoriteMerchantResponce GetFavoriteMerchantsByCustomerId(CustomerFavoriteMerchant requestModel)
		{
			var vendors = new List<Vendor>();
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					var result = db.Query<Vendor>(FAVORITEMERCHANTS, new
					{
						requestModel.CustomerId,
						requestModel.PageIndex,
						requestModel.PageSize,
						requestModel.StoreId,
					}, commandType: CommandType.StoredProcedure);

					vendors = result?.ToList();

				}
				catch (SqlException e)
				{
					return new CustomerFavoriteMerchantResponce { ValidData = false, ResultData = e.Message };
				}
			}

			if (vendors != null && vendors.Count() > 0)
			{
				var vendorIds = vendors.Select(v => v.Id).ToList();
				var model = new TopRatedRestnChefModel()
				{
					CustomerId = requestModel.CustomerId,
					StoreId = requestModel.StoreId
				};
				var restData = GetTopRatedCooknRest(model, vendorIds);
				if (restData.Any())
				{
					return new CustomerFavoriteMerchantResponce
					{
						ValidData = true,
						ResultData = string.Empty,
						CooknRests = restData,
						TotalRecords = _dbcontext.FavoriteMerchant.Where(f => f.CustomerId == requestModel.CustomerId).Count()
					};
				}

			}
			return new CustomerFavoriteMerchantResponce { ValidData = false, ResultData = "" };
		}

		#endregion

		#region Reviews 

		/// <summary>
		/// Get review list for agnet/merchant/product based on entity id and store.
		/// </summary>
		/// <param name="requestModel"></param>
		/// <returns>ReviewOverviewModel</returns>
		public ReviewOverviewModel GetReviewsByEntityId(ReviewRequestModel requestModel, int languageId, ecuadordevContext dbContext)
		{
			var ratingReviews = new List<RatingReviews>();
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					var result = db.Query<RatingReviews>(ENTITYREVIEWS, new
					{
						requestModel.ReviewType,
						requestModel.EntityId,
						requestModel.PageIndex,
						requestModel.PageSize,
						requestModel.StoreId,
					}, commandType: CommandType.StoredProcedure);

					ratingReviews = result?.ToList();

				}
				catch (SqlException e)
				{
					return new ReviewOverviewModel { ValidData = false, ResultData = e.Message };
				}
			}

			var response = new ReviewOverviewModel();
			try
			{
				if (ratingReviews != null && ratingReviews.Count() > 0)
				{
					foreach (var review in ratingReviews)
					{
						var entityName = string.Empty;
						if (review.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item)
						{
							var reviewProduct = _dbcontext.Product.FirstOrDefault(p => p.Id == review.EntityId);
							entityName = LanguageHelper.GetLocalizedValueProduct(review.EntityId, "Name", languageId, reviewProduct.Name, dbContext);
						}
						else if (review.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Agent || review.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant)
						{
							if (review.EntityId > 0)
								//get customer name
								entityName = Helper.GetCustomerFullName(review.EntityId, dbContext);
						}

						var reviewModel = new ReviewEntityModel
						{
							ReviewType = review.ReviewType,
							EntityId = review.EntityId,
							CustomerName = Helper.GetCustomerFullName(review.CustomerId, dbContext),
							Name = entityName,
							Title = review.Title,
							ReviewText = review.ReviewText ?? string.Empty,
							ReplyText = review.ReplyText ?? string.Empty,
							Rating = review.Rating,
							ReviewDate = review.CreatedOnUtc.ToString("dd/MM/yyyy"),
							ApprovalStatus = review.IsApproved ? "Approved" : "Pending",
						};
						response.Reviews.Add(reviewModel);
					}
				}

				response.PageIndex = requestModel.PageIndex;
				response.PageSize = requestModel.PageSize;
				response.ReviewType = requestModel.ReviewType;
				response.EntityId = requestModel.EntityId;
				response.TotalReviews = ratingReviews.Count();
				response.ResultData = string.Empty;
				response.ValidData = true;

				return response;
			}
			catch (Exception e)
			{
				return new ReviewOverviewModel { ValidData = false, ResultData = e.Message };
			}
		}















		#endregion

		#endregion

		public void  AddressToFar(AddRemoveProductCart addProductToCart, ProductDetailRootModel productDetailRoot,  int VendorId, int languageId)
        {
			var addressTobeDelivered = _dbcontext.Address.Where(x => x.Id == addProductToCart.deliveryAddressId).FirstOrDefault();
			string strsql = "select id from vendor where Id=" + VendorId + " and storeid=" + addProductToCart.StoreId + " and geofancing is not null and geofancing like '%poly%' and isdelivery=1";
			strsql += " and cast(geometry::STGeomFromText(cast((replace(replace(replace(replace(replace(replace(replace(geofancing,";
			strsql += "char(34) + 'coordinates' + char(34),''),";
			strsql += "char(34) + 'type'+ char(34) +':',''),'' + char(34) +',:[{'+ char(34) +'lat'+ char(34) +':','((') ,'{'+ char(34) +'P','P'),','+ char(34) +'lng'+ char(34) +':',' '),'},{'+ char(34) +'lat'+ char(34) +':',', '),'}]}','))') ) AS varchar(max)) ,0) as geometry)";
			strsql += ".STContains(cast(geometry::STGeomFromText('POINT(" + addressTobeDelivered.Latitude + " " + addressTobeDelivered.Longitude + ")', 0) As geometry)) = 1";
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				SqlCommand command = new SqlCommand(strsql, connection);
				connection.Open();
				SqlDataReader reader = command.ExecuteReader();
				try
				{
					if (!reader.HasRows)
					{
						connection.Close();
						productDetailRoot.MerchantAddress = true;
						productDetailRoot.MerchantMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.AddressToFar", languageId, _dbcontext);
					}
					else
					{
						productDetailRoot.MerchantAddress = false;
						productDetailRoot.MerchantMessage = string.Empty;

					}
				}
				finally
				{
					// Always call Close when done reading.
					connection.Close();
				}
			}
			


		}
	}
}
