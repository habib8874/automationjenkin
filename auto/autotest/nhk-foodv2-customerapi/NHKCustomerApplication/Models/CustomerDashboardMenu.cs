﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class CustomerDashboardMenu
    {
        public int Id { get; set; }
        public int MenuId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string ActivityUrl { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDt { get; set; }
        public int StoreId { get; set; }
    }
}
