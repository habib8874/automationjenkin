﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.ShoppingCart
{
    public partial class OrderTotalsModel : BaseNopModel
    {
        public OrderTotalsModel()
        {
            TaxRates = new List<TaxRate>();
            GiftCards = new List<GiftCard>();
        }
        public bool IsEditable { get; set; }
        public bool IsMiniCart { get; set; }
        public string SubTotal { get; set; }
        public decimal SubTotalAmount { get; set; }
        public string SubTotalDiscount { get; set; }
        public decimal SubTotalDiscountAmount { get; set; }
        public string Shipping { get; set; }
        public bool RequiresShipping { get; set; }
        public string SelectedShippingMethod { get; set; }
        public bool HideShippingTotal { get; set; }

        public string PaymentMethodAdditionalFee { get; set; }

        public string Tax { get; set; }
        public decimal TaxAmount { get; set; }
        public IList<TaxRate> TaxRates { get; set; }
        public bool DisplayTax { get; set; }
        public string AppliedTaxRates { get; set; }
        public bool DisplayTaxRates { get; set; }

        public IList<GiftCard> GiftCards { get; set; }

        public string OrderTotalDiscount { get; set; }
        public decimal OrderTotalDiscountAmount { get; set; }
        public int RedeemedRewardPoints { get; set; }
        public string RedeemedRewardPointsAmount { get; set; }

        public int WillEarnRewardPoints { get; set; }

        public string OrderTotal { get; set; }
        public decimal OrderTotalAmount { get; set; }
        public decimal DeliveryAmount { get; set; }
        public decimal ServiceChargeAmount { get; set; }

        public decimal TipAmount { get; set; }
        public decimal TotalDiscount { get; set; }
        public string TotalDiscountAmount { get; set; }
        public decimal ItemTotal { get; set; }
        public string ItemTotaltAmount { get; set; }

        #region Nested classes

        public partial class TaxRate: BaseNopModel
        {
            public string Rate { get; set; }
            public string Value { get; set; }
        }

        public partial class GiftCard : BaseNopEntityModel
        {
            public string CouponCode { get; set; }
            public string Amount { get; set; }
            public string Remaining { get; set; }
        }

        #endregion
    }
}