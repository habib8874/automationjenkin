using System.Collections.Generic;
using Nop.Core.Domain.Localization;

namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents a product tag
    /// </summary>
    public partial class ProductCuisine  : BaseEntity
    {
   
        public string Name { get; set; }
        public int StoreId { get; set; }

        //private ICollection<Product> _Products;

        //public virtual ICollection<Product> Products
        //{
        //    get { return _Products ?? (_Products = new List<Product>()); }
        //    protected set { _Products = value; }
        //}
    }
}
