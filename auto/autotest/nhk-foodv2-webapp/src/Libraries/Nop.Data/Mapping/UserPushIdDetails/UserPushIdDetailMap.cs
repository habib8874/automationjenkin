﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.UserPushIdDetails;

namespace Nop.Data.Mapping.UserPushIdDetails
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class UserPushIdDetailMap : NopEntityTypeConfiguration<UserPushIdDetail>
    {
        /// <summary>
        /// Ctor
        /// </summary>
        public override void Configure(EntityTypeBuilder<UserPushIdDetail> builder)
        {
            builder.ToTable("UserPushIdDetails");
            builder.HasKey(s => s.Id);
            builder.Property(s => s.UserId).IsRequired();
            builder.Property(s => s.Token).IsRequired().HasMaxLength(2000);
            builder.Ignore(s => s.CrDt);
            base.Configure(builder);
        }
    }
}
