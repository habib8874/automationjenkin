﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Common
{
    public class AgentResultModel
    {
        public bool Result { get; set; }
        public string ResultMsg { get; set; }
    }
}
