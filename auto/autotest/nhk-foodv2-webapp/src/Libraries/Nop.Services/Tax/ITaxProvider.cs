﻿using System.Collections.Generic;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Services.Plugins;

namespace Nop.Services.Tax
{
    /// <summary>
    /// Provides an interface for creating tax providers
    /// </summary>
    public partial interface ITaxProvider : IPlugin
    {
        /// <summary>
        /// Gets tax rate
        /// </summary>
        /// <param name="calculateTaxRequest">Tax calculation request</param>
        /// <returns>Tax</returns>
        CalculateTaxResult GetTaxRate(CalculateTaxRequest calculateTaxRequest);

        /// <summary>
        /// Get tax category Ids
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="stateId"></param>
        /// <returns></returns>
        IList<int> GetTaxCategoryIds(int storeId,int stateId);

        /// <summary>
        /// get applied taxes
        /// </summary>
        /// <param name="products"></param>
        /// <param name="address"></param>
        /// <param name="storeId"></param>
        /// <returns></returns>
        string GetAppliedTaxRates(List<Product> products, Address address, int storeId);
    }
}
