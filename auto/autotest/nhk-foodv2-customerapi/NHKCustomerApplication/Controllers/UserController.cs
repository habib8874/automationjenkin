﻿using Microsoft.AspNetCore.Mvc;
using NHKCustomerApplication.Caching;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Services;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModel;
using NHKCustomerApplication.ViewModels;
using System;
using System.Net;

namespace NHKCustomerApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        #region "fields"

        private readonly IUserService _userService;
        private ecuadordevContext dbcontext;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Constructor

        public UserController(IUserService userService, ecuadordevContext context, ICacheManager cacheManager)
        {
            this._userService = userService;
            dbcontext = context;
            this._cacheManager = cacheManager;

        }

        #endregion

        /// <summary>
        /// RegisterCustomer is used to register a custome.
        /// </summary>
        /// <param name="customer">customer.</param>
        /// <returns>registered customer details.</returns>
        [Route("~/api/v2/Signup")]
        [HttpPost]
        public CustomerAPIResponses RegisterCustomer([FromBody]CustomerModel customer)
        {
            return _userService.RegisterCustomer(customer);
        }

        /// <summary>
        /// ExternalAuthenticationSignUp is used for external authentication.
        /// </summary>
        /// <param name="customer">ExternalAuthModel.</param>
        /// <returns>registered customer details.</returns>
        [Route("~/api/v2/ExternalAuthenticationSignUp")]
        [HttpPost]
        public CustomerAPIResponses ExternalAuthenticationSignUp([FromBody] ExternalAuthModel customer)
        {
            return _userService.ExternalAuth(customer);
        }

        /// <summary>
        /// CreateGuestUser is used to create a guest user.
        /// </summary>
        /// <param name="customer">GuestUserModel object.</param>
        /// <returns>CustomerAPIResponses.</returns>
        [Route("~/api/v2/CreateGuestUser")]
        [HttpPost]
        public CustomerAPIResponses CreateGuestUser([FromBody] GuestUserModel customer)
        {
            return _userService.CreateGuestUser(customer);
        }

        /// <summary>
        /// RegistaionverificationCustomer is used to verify otp.
        /// <param name="customer">RegistationverificationCustomer.</param>
        /// <returns>Success message if otp matched.</returns>
        /// </summary>
        [Route("~/api/v2/RegistrationVerification")]
        [HttpPost]
        public CustomerAPIResponses RegistaionverificationCustomer([FromBody]RegistationverificationCustomer RegistationverificationCustomer)
        {
            return _userService.RegistaionVerificationCustomer(RegistationverificationCustomer);
        }

        /// <summary>
        /// RegistaionverificationCustomer is used to verify otp.
        /// <param name="customer">RegistationverificationCustomer.</param>
        /// <returns>Success message if otp matched.</returns>
        /// </summary>
        [Route("~/api/v3/RegistrationVerification")]
        [HttpPost]
        public CustomerAPIResponses RegistaionverificationCustomerV3([FromBody] RegistationverificationCustomer RegistationverificationCustomer)
        {
            return _userService.RegistaionVerificationCustomerV3(RegistationverificationCustomer);
        }


        ///<summary>
        /// Customer Login
        /// <param name="customer"></param>
        /// <returns></returns>
        /// </summary>
        [Route("~/api/v2/Login")]
        [HttpPost]
        public CustomerAPIResponses Login(CustomerLogin customer)
        {
            return _userService.CustomerLogin(customer);
        }

        /// <summary>
        /// Customer LoginV2.2
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        [Route("~/api/v2.2/Login for Vonzu")]
        [HttpPost]
        public CustomerAPIResponses Login_V2(CustomerLogin customer)
        {
            return _userService.CustomerLoginV2_2(customer);
        }

        ///<summary>
        /// Customer Login
        /// <param name="customer"></param>
        /// <returns></returns>
        /// </summary>
        [Route("~/api/v2.1/Login")]
        [HttpPost]
        public CustomerAPIResponses LoginV2_1(CustomerLoginV2_1 customer)
        {
            return _userService.CustomerLoginV2_1(customer);
        }

        /// <summary>
        /// ExternalAuthenticationSignUp is used for external authentication.
        /// </summary>
        /// <param name="customer">ExternalAuthModel.</param>
        /// <returns>registered customer details.</returns>
        [Route("~/api/v3/Login")]
        [HttpPost]
        public BaseAPIResponseModel LoginV3(CustomerLoginV3 requestModel)
        {
            int languageId = LanguageHelper.GetIdByLangCode(requestModel.StoreId, requestModel.UniqueSeoCode, dbcontext);
            try
            {
                if (requestModel == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMessage.ModelNotDefined", languageId, dbcontext));
                if (!string.IsNullOrEmpty(requestModel.ApiKey) || !string.IsNullOrWhiteSpace(requestModel.ApiKey))
                {
                    var keyExist = Helper.AuthenticateKey(requestModel.ApiKey, requestModel.StoreId, dbcontext,_cacheManager);
                    if (keyExist == null)
                        return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMessage.InvalidAuthentication", languageId, dbcontext));
                }
                var data = _userService.CustomerLoginV3(requestModel);
                if (data == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.Register", languageId, dbcontext));
                return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.LoginSuccessfully", languageId, dbcontext), data);
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                return BaseMethodsHelper.ErrorResponse(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        [Route("~/api/v2/GetCooknRestProductsByRestnCookId")]
        [HttpPost]
        public CustomerAPIResponses GetCooknRestItemsByRestnCookIdv4([FromBody]CooknRestProductByID customer)
        {
            return _userService.GetCooknRestItemsByRestnCookId(customer);
        }
    }
}