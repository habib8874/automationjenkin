﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nop.Web.Areas.Admin.Models.Orders;

namespace Nop.Web.Areas.Admin.Models.NB.DispatchManagement
{
    public class DispatchModel
    {
        public DispatchModel()
        {
            PendingOrderIds = new List<int>();
            OnGoingOrderIds = new List<int>();
            OrderAgentModel = new OrderModel();
        }
        public IList<int> PendingOrderIds { get; set; }
        public IList<int> OnGoingOrderIds { get; set; }
        public OrderModel OrderAgentModel { get; set; }
    }
}
