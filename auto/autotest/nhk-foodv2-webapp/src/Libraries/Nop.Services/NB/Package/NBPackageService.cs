﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.NB.Package;
using Nop.Services.Catalog;
using Nop.Services.Events;

namespace Nop.Services.NB.Package
{
    public partial class NBPackageService : INBPackageService
    {
        #region Fields

        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<NBPackage> _packageRepository;
        private readonly IRepository<NBPackagePlan> _packagePlanRepository;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        public NBPackageService(
            IEventPublisher eventPublisher,
            IRepository<NBPackage> packageRepository,
            IRepository<NBPackagePlan> packagePlanRepository,
            ICacheManager cacheManager)
        {
            _eventPublisher = eventPublisher;
            _packageRepository = packageRepository;
            _packagePlanRepository = packagePlanRepository;
            _cacheManager = cacheManager;
        }

        #endregion

        #region Methods

        #region Packages

        /// <summary>
        /// Deletes a package
        /// </summary>
        /// <param name="package">Package</param>
        public virtual void DeletePackage(NBPackage package)
        {
            if (package == null)
                throw new ArgumentNullException(nameof(package));

            package.Deleted = true;
            _packageRepository.Update(package);

            //event notification
            _eventPublisher.EntityUpdated(package);
        }

        /// <summary>
        /// Gets a package
        /// </summary>
        /// <param name="packageId">The package identifier</param>
        /// <returns>Package</returns>
        public virtual NBPackage GetPackageById(int packageId)
        {
            if (packageId == 0)
                return null;


            var key = string.Format(NopNBDefaults.PackagesByIdCacheKey, packageId);

            return _cacheManager.Get(key, () => _packageRepository.GetById(packageId));
        }

        /// <summary>
        /// Gets all packages
        /// </summary>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <returns>Packages</returns>
        public virtual IList<NBPackage> GetAllPackages(int storeId, string name)
        {

            var query = _packageRepository.Table;
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(x => x.Name.Contains(name));
            }
            if (storeId > 0)
            {
                query = query.Where(x => x.StoreId == storeId);
            }
            query = query.OrderBy(t => t.Name);
            return query.ToList();
        }

        /// <summary>
        /// Inserts a package
        /// </summary>
        /// <param name="package">Package</param>
        public virtual void InsertPackage(NBPackage package)
        {
            if (package == null)
                throw new ArgumentNullException(nameof(package));

            _packageRepository.Insert(package);

            //event notification
            _eventPublisher.EntityInserted(package);
        }

        /// <summary>
        /// Updates the package
        /// </summary>
        /// <param name="package">Package</param>
        public virtual void UpdatePackage(NBPackage package)
        {
            if (package == null)
                throw new ArgumentNullException(nameof(package));

            _packageRepository.Update(package);

            //event notification
            _eventPublisher.EntityUpdated(package);
        }
        #endregion

        #region Package plans
        /// <summary>
        /// Deletes a packagePlan
        /// </summary>
        /// <param name="packagePlan">PackagePlan</param>
        public virtual void DeletePackagePlan(NBPackagePlan packagePlan)
        {
            if (packagePlan == null)
                throw new ArgumentNullException(nameof(packagePlan));

            _packagePlanRepository.Delete(packagePlan);

            //event notification
            _eventPublisher.EntityDeleted(packagePlan);
        }

        /// <summary>
        /// Gets a packagePlan
        /// </summary>
        /// <param name="packagePlanId">The packagePlan identifier</param>
        /// <returns>PackagePlan</returns>
        public virtual NBPackagePlan GetPackagePlanById(int packagePlanId)
        {
            if (packagePlanId == 0)
                return null;

            //return _packagePlanRepository.Table.FirstOrDefault(x => x.Id == packagePlanId);


            var key = string.Format(NopNBDefaults.PackagePlanByIdCacheKey, packagePlanId);

            return _cacheManager.Get(key, () => _packagePlanRepository.GetById(packagePlanId));
        }

        /// <summary>
        /// Gets packagePlans
        /// </summary>
        /// <param name="packageId">get records by packageId</param>
        /// <param name="productId">get records by productId</param>
        /// <returns>PackagePlans</returns>
        public virtual IList<NBPackagePlan> GetPackagePlans(int packageId = 0, int productId = 0)
        {

            var query = _packagePlanRepository.Table;

            if (packageId > 0)
            {
                query = query.Where(x => x.PackageId == packageId);
            }
            if (productId > 0)
            {
                query = query.Where(x => x.ProductId == productId);
            }
            query = query.OrderBy(t => t.Name);
            return query.ToList();
        }

        /// <summary>
        /// Inserts a packagePlan
        /// </summary>
        /// <param name="packagePlan">PackagePlan</param>
        public virtual void InsertPackagePlan(NBPackagePlan packagePlan)
        {
            if (packagePlan == null)
                throw new ArgumentNullException(nameof(packagePlan));

            _packagePlanRepository.Insert(packagePlan);

            //event notification
            _eventPublisher.EntityInserted(packagePlan);
        }

        /// <summary>
        /// Updates the packagePlan
        /// </summary>
        /// <param name="packagePlan">PackagePlan</param>
        public virtual void UpdatePackagePlan(NBPackagePlan packagePlan)
        {
            if (packagePlan == null)
                throw new ArgumentNullException(nameof(packagePlan));

            _packagePlanRepository.Update(packagePlan);

            //event notification
            _eventPublisher.EntityUpdated(packagePlan);
        }

        /// <summary>
        /// Gets a packagePlan
        /// </summary>
        /// <param name="productId">The productId identifier</param>
        /// <returns>PackagePlan</returns>
        public virtual NBPackagePlan GetPackagePlanByProductId(int productId)
        {
            if (productId == 0)
                return null;

            return _packagePlanRepository.Table
                .Where(x => x.ProductId == productId)
                .FirstOrDefault();
        }

        #endregion

        #endregion
    }
}
