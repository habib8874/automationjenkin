﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace NHKCustomerApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeliverySlotController : ControllerBase
    {
        private string connectionString = string.Empty;
        private ecuadordevContext dbcontext;
        private readonly IConfiguration _configuration;
        public DeliverySlotController(ecuadordevContext context, IConfiguration configuration)
        {
            dbcontext = context;
            _configuration = configuration;
            connectionString = _configuration.GetSection("ConnectionStrings").GetSection("ecuadordatabase").Value;
        }

        [Route("~/api/v1/getdeliveryslots")]
        [HttpPost]
        public CustomerAPIResponses GetDeliverySlots(RequestDeliverySlotModel deliverySlotObj)
        {
            int languageId = LanguageHelper.GetIdByLangCode(deliverySlotObj.StoreId, deliverySlotObj.UniqueSeoCode, dbcontext);
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            try
            {
                if (deliverySlotObj == null)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else if (string.IsNullOrEmpty(deliverySlotObj.APIKey) || string.IsNullOrWhiteSpace(deliverySlotObj.APIKey))
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else if (deliverySlotObj.CustomerId == 0)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else if (deliverySlotObj.StoreId == 0)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else if (deliverySlotObj.RestnCookId == 0)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.RestaurantIdNotDefined", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }

                var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == deliverySlotObj.APIKey && x.StoreId == deliverySlotObj.StoreId).Any();
                if (!keyExist)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }

                //check any delivery slot active
                var genricdeliveryScheduleActive = dbcontext.Setting.Where(s => s.StoreId == deliverySlotObj.StoreId && s.Name == "NB.Setting.DeliveryScheduleActive").Select(s => s.Value).FirstOrDefault();
                var deliveryScheduleActive = string.IsNullOrEmpty(genricdeliveryScheduleActive) ? false : Convert.ToBoolean(genricdeliveryScheduleActive);
                if (deliveryScheduleActive)
                {
                    deliveryScheduleActive = (from p in dbcontext.DeliverySlot
                                              where p.StoreId == deliverySlotObj.StoreId && (p.MerchantId == deliverySlotObj.RestnCookId || p.MerchantId == 0) && !p.Deleted && p.Published
                                              select p).Any();
                }

                if (deliveryScheduleActive)
                {
                    var genrictotalDeliveryWeeks = dbcontext.Setting.Where(s => s.StoreId == deliverySlotObj.StoreId && s.Name == "NB.Setting.DeliveryScheduleWeeks").Select(s => s.Value).FirstOrDefault();
                    var totalDeliveryWeeks = string.IsNullOrEmpty(genrictotalDeliveryWeeks) ? 4 : Convert.ToInt32(genrictotalDeliveryWeeks);

                    IList<DeliverySlotModel> deliverySlotlist = new List<DeliverySlotModel>();
                    var dateTimeNow = DateTime.Now;
                    var deliveryDateTime = dateTimeNow.Date;
                    var dateTimeWeeks = dateTimeNow.AddDays(totalDeliveryWeeks * 7).Date;
                    bool getDeliverySlot = true;

                    while (getDeliverySlot && deliveryDateTime <= dateTimeWeeks)
                    {
                        List<DeliverySlot> deliverySlots = new List<DeliverySlot>();
                        string productIds = "";
                        using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
                        {
                            deliverySlots = db.Query<DeliverySlot>("NB_Web_GetDeliverySlotIds", new
                            {
                                deliveryDateTime,
                                productIds,
                                merchantId = deliverySlotObj.RestnCookId,
                                storeId = deliverySlotObj.StoreId
                            }, commandType: CommandType.StoredProcedure).ToList();
                        }

                        if (deliverySlots.Count > 0)
                        {
                            var bookedDeliverySlots = (from eq in dbcontext.DeliverySlotBooking
                                                       where eq.StoreId == deliverySlotObj.StoreId && eq.DeliveryDateUtc == deliveryDateTime.Date && !eq.Deleted
                                                       select eq).ToList();

                            foreach (var deliverySlot in deliverySlots)
                            {
                                var startTimeEnd = deliveryDateTime.Add(deliverySlot.StartTime);
                                var endTime = deliveryDateTime.Add(deliverySlot.EndTime);

                                //bypass today date deliveryslot if todaydatetime is greater then to start time
                                if (startTimeEnd.Date == dateTimeNow.Date && dateTimeNow > startTimeEnd)
                                {
                                    while (startTimeEnd < dateTimeNow)
                                    {
                                        double minuts = +deliverySlot.Duration;
                                        startTimeEnd = startTimeEnd.AddMinutes(minuts);
                                    }
                                }

                                var startTimeStart = startTimeEnd;

                                try
                                {
                                    var vendor = dbcontext.Vendor.FirstOrDefault(x => x.Id == deliverySlotObj.RestnCookId);
                                    // check Merchant start and close time 
                                    TimeSpan openTimeSpan = DateTime.Parse(vendor.Opentime).TimeOfDay;
                                    TimeSpan closetimeSpan = DateTime.Parse(vendor.Closetime).TimeOfDay;

                                    var openTime = deliveryDateTime.Date.Add(openTimeSpan);
                                    var closetime = deliveryDateTime.Date.Add(closetimeSpan);

                                    if (startTimeEnd < openTime)
                                    {
                                        while (startTimeEnd < openTime)
                                        {
                                            double minuts = +deliverySlot.Duration;
                                            startTimeEnd = startTimeEnd.AddMinutes(minuts);
                                        }
                                        startTimeStart = startTimeEnd;
                                    }

                                    if (endTime > closetime)
                                    {
                                        endTime = closetime;
                                    }

                                    //check cut off time
                                    if (deliverySlot.CutOffTime > 0 && startTimeEnd.Date == dateTimeNow.Date)
                                    {
                                        var cutOffDateTime = dateTimeNow.AddHours(deliverySlot.CutOffTime);

                                        while (startTimeEnd < cutOffDateTime)
                                        {
                                            double minuts = +deliverySlot.Duration;
                                            startTimeEnd = startTimeEnd.AddMinutes(minuts);
                                        }
                                        startTimeStart = startTimeEnd;
                                    }

                                }
                                catch { }

                                while (startTimeEnd != endTime && startTimeEnd <= endTime)
                                {
                                    double minuts = +deliverySlot.Duration;
                                    startTimeEnd = startTimeEnd.AddMinutes(minuts);
                                    if (bookedDeliverySlots.Where(x => x.DeliveryStartTime == startTimeStart && x.DeliveryEndTime == startTimeEnd && x.OrderId > 0).ToList().Count < deliverySlot.NumberOfOrders)
                                    {

                                        var deliverySlotmodel = new DeliverySlotModel
                                        {
                                            Id = deliverySlot.Id,
                                            StartDateUtc = startTimeStart,
                                            StartDateString = startTimeStart.ToString("MM/dd/yyyy"),
                                            StartTime = startTimeStart.ToString("hh:mm tt"),
                                            EndTime = startTimeEnd.ToString("hh:mm tt"),
                                            SurChargeFee = deliverySlot.SurChargeFee
                                        };

                                        deliverySlotlist.Add(deliverySlotmodel);
                                    }
                                    startTimeStart = startTimeStart.AddMinutes(minuts);
                                }
                            }
                            deliverySlotlist = deliverySlotlist.OrderBy(x => x.StartDateUtc).ToList();

                            if (deliverySlotlist.Count > 0)
                            {
                                getDeliverySlot = false;
                            }

                        }
                        deliveryDateTime = deliveryDateTime.AddDays(1);
                    }

                    if (deliverySlotlist.Count > 0)
                    {
                        IList<DeliveryDaysModel> deliveryDayslist = new List<DeliveryDaysModel>();

                        var deliverydaysDate = dateTimeNow;

                        while (deliverydaysDate <= dateTimeWeeks)
                        {
                            var deliveryDays = new DeliveryDaysModel
                            {
                                WeekDay = deliverydaysDate.ToString("ddd").ToUpper(),
                                WeekDateValue = string.Format("{0} {1}", deliverydaysDate.ToString("dd"), deliverydaysDate.ToString("MMM")),
                                WeekDate = deliverydaysDate.Date
                            };

                            deliveryDayslist.Add(deliveryDays);
                            deliverydaysDate = deliverydaysDate.AddDays(1);
                        }

                        var deliveryModel = new DeliveryModel
                        {
                            DeliverySlotList = deliverySlotlist,
                            DeliveryDaysList = deliveryDayslist,
                            ActiveSlotDate = deliveryDateTime.AddDays(-1).Date
                        };

                        customerAPIResponses.ErrorMessageTitle = "Success!!";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.SuccessMesaage.DeliveryList", languageId, dbcontext);
                        customerAPIResponses.Status = true;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                        customerAPIResponses.ResponseObj = deliveryModel;
                        return customerAPIResponses;
                    }
                }

                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.SuccessMesaage.NoDeliverySlotListFound", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
        }

        [Route("~/api/v1/getdeliveryslotsbydate")]
        [HttpPost]
        public CustomerAPIResponses GetDeliverySlotsByDate(RequestDeliverySlotModel deliverySlotObj)
        {
            int languageId = LanguageHelper.GetIdByLangCode(deliverySlotObj.StoreId, deliverySlotObj.UniqueSeoCode, dbcontext);
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            try
            {
                if (deliverySlotObj == null)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else if (string.IsNullOrEmpty(deliverySlotObj.APIKey) || string.IsNullOrWhiteSpace(deliverySlotObj.APIKey))
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else if (deliverySlotObj.CustomerId == 0)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else if (deliverySlotObj.StoreId == 0)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else if (deliverySlotObj.RestnCookId == 0)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.RestaurantIdNotDefined", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else if (string.IsNullOrEmpty(deliverySlotObj.DeliveryDate))
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.SendDeliveryDate", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }


                var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == deliverySlotObj.APIKey && x.StoreId == deliverySlotObj.StoreId).Any();
                if (!keyExist)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }

                //get user current date
                var deliveryDateTime = DateTime.ParseExact(deliverySlotObj.DeliveryDate, "MM-dd-yyyy", CultureInfo.InvariantCulture);

                IList<DeliverySlotModel> deliverySlotlist = new List<DeliverySlotModel>();
                List<DeliverySlot> deliverySlots = new List<DeliverySlot>();
                string productIds = "";

                using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
                {
                    deliverySlots = db.Query<DeliverySlot>("NB_Web_GetDeliverySlotIds", new
                    {
                        deliveryDateTime,
                        productIds,
                        merchantId = deliverySlotObj.RestnCookId,
                        storeId = deliverySlotObj.StoreId
                    }, commandType: CommandType.StoredProcedure).ToList();
                }

                if (deliverySlots.Count > 0)
                {
                    var bookedDeliverySlots = (from eq in dbcontext.DeliverySlotBooking
                                               where eq.StoreId == deliverySlotObj.StoreId && eq.DeliveryDateUtc == deliveryDateTime.Date && !eq.Deleted
                                               select eq).ToList();

                    foreach (var deliverySlot in deliverySlots)
                    {
                        var startTimeEnd = deliveryDateTime.Add(deliverySlot.StartTime);
                        var endTime = deliveryDateTime.Add(deliverySlot.EndTime);

                        //bypass today date deliveryslot if todaydatetime is greater then to start time
                        var dateTimeNow = DateTime.Now;
                        if (startTimeEnd.Date == dateTimeNow.Date && dateTimeNow > startTimeEnd)
                        {
                            while (startTimeEnd < dateTimeNow)
                            {
                                double minuts = +deliverySlot.Duration;
                                startTimeEnd = startTimeEnd.AddMinutes(minuts);
                            }
                        }

                        var startTimeStart = startTimeEnd;

                        try
                        {
                            var vendor = dbcontext.Vendor.FirstOrDefault(x => x.Id == deliverySlotObj.RestnCookId);
                            // check Merchant start and close time 
                            TimeSpan openTimeSpan = DateTime.Parse(vendor.Opentime).TimeOfDay;
                            TimeSpan closetimeSpan = DateTime.Parse(vendor.Closetime).TimeOfDay;

                            var openTime = deliveryDateTime.Date.Add(openTimeSpan);
                            var closetime = deliveryDateTime.Date.Add(closetimeSpan);

                            if (startTimeEnd < openTime)
                            {
                                while (startTimeEnd < openTime)
                                {
                                    double minuts = +deliverySlot.Duration;
                                    startTimeEnd = startTimeEnd.AddMinutes(minuts);
                                }
                                startTimeStart = startTimeEnd;
                            }

                            if (endTime > closetime)
                            {
                                endTime = closetime;
                            }

                            //check cut off time
                            if (deliverySlot.CutOffTime > 0 && startTimeEnd.Date == dateTimeNow.Date)
                            {
                                var cutOffDateTime = dateTimeNow.AddHours(deliverySlot.CutOffTime);

                                while (startTimeEnd < cutOffDateTime)
                                {
                                    double minuts = +deliverySlot.Duration;
                                    startTimeEnd = startTimeEnd.AddMinutes(minuts);
                                }
                                startTimeStart = startTimeEnd;
                            }

                        }
                        catch { }

                        while (startTimeEnd != endTime && startTimeEnd <= endTime)
                        {
                            double minuts = +deliverySlot.Duration;
                            startTimeEnd = startTimeEnd.AddMinutes(minuts);
                            if (bookedDeliverySlots.Where(x => x.DeliveryStartTime == startTimeStart && x.DeliveryEndTime == startTimeEnd && x.OrderId > 0).ToList().Count < deliverySlot.NumberOfOrders)
                            {

                                var deliverySlotmodel = new DeliverySlotModel
                                {
                                    Id = deliverySlot.Id,
                                    StartDateUtc = startTimeStart,
                                    StartDateString = startTimeStart.ToString("MM/dd/yyyy"),
                                    StartTime = startTimeStart.ToString("hh:mm tt"),
                                    EndTime = startTimeEnd.ToString("hh:mm tt"),
                                    SurChargeFee = deliverySlot.SurChargeFee
                                };

                                deliverySlotlist.Add(deliverySlotmodel);
                            }
                            startTimeStart = startTimeStart.AddMinutes(minuts);
                        }
                    }

                    deliverySlotlist = deliverySlotlist.OrderBy(x => x.StartDateUtc).ToList();

                    customerAPIResponses.ErrorMessageTitle = "Success!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.SuccessMesaage.DeliverySlotList", languageId, dbcontext);
                    customerAPIResponses.Status = true;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                    customerAPIResponses.ResponseObj = deliverySlotlist;
                    return customerAPIResponses;
                }

                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.NoDeliverySlotListFound", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
        }

        [Route("~/api/v1/savedeliveryslot")]
        [HttpPost]
        public CustomerAPIResponses SaveDeliverySlot(SaveDeliverySlotModel deliverySlotObj)
        {
            int languageId = LanguageHelper.GetIdByLangCode(deliverySlotObj.StoreId, deliverySlotObj.UniqueSeoCode, dbcontext);
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            try
            {
                if (deliverySlotObj == null)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else if (string.IsNullOrEmpty(deliverySlotObj.APIKey) || string.IsNullOrWhiteSpace(deliverySlotObj.APIKey))
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else if (deliverySlotObj.CustomerId == 0)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else if (deliverySlotObj.StoreId == 0)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else if (deliverySlotObj.RestnCookId == 0)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.RestaurantIdNotDefined", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }

                var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == deliverySlotObj.APIKey && x.StoreId == deliverySlotObj.StoreId).Any();
                if (!keyExist)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }

                // remove all old entitys
                var deliveryGenericAttributes = dbcontext.GenericAttribute.Where(x => x.EntityId == deliverySlotObj.CustomerId && x.StoreId == deliverySlotObj.StoreId && x.Key == "SelectedDeliverySlotBookingId" && x.KeyGroup == "Customer").ToList();

                foreach (var deliveryGenericAttribute in deliveryGenericAttributes)
                {
                    dbcontext.GenericAttribute.Remove(deliveryGenericAttribute);
                    dbcontext.SaveChanges();
                }

                if (deliverySlotObj.SlotId > 0)
                {
                    //get user current date
                    var deliveryDate = DateTime.ParseExact(deliverySlotObj.SlotDate, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                    var deliverySlotBooking = new DeliverySlotBooking
                    {
                        SlotId = deliverySlotObj.SlotId,
                        StoreId = deliverySlotObj.StoreId,
                        CustomerId = deliverySlotObj.CustomerId,
                        DeliveryDateUtc = deliveryDate.Date,
                        DeliveryStartTime = deliveryDate.Date.Add(DateTime.Parse(deliverySlotObj.StartTime).TimeOfDay),
                        DeliveryEndTime = deliveryDate.Date.Add(DateTime.Parse(deliverySlotObj.EndTime).TimeOfDay),
                        CreatedOnUtc = DateTime.UtcNow
                    };

                    dbcontext.DeliverySlotBooking.Add(deliverySlotBooking);
                    dbcontext.SaveChanges();

                    GenericAttribute genericAttributeGender = new GenericAttribute
                    {
                        Key = "SelectedDeliverySlotBookingId",
                        EntityId = deliverySlotObj.CustomerId,
                        Value = deliverySlotBooking.Id.ToString(),
                        KeyGroup = "Customer",
                        StoreId = deliverySlotObj.StoreId
                    };
                    dbcontext.GenericAttribute.Add(genericAttributeGender);
                    dbcontext.SaveChanges();
                }
                else
                {
                    GenericAttribute genericAttributeGender = new GenericAttribute
                    {
                        Key = "SelectedDeliverySlotBookingId",
                        EntityId = deliverySlotObj.CustomerId,
                        Value = "0",
                        KeyGroup = "Customer",
                        StoreId = deliverySlotObj.StoreId
                    };

                    dbcontext.GenericAttribute.Add(genericAttributeGender);
                    dbcontext.SaveChanges();
                }

                customerAPIResponses.ErrorMessageTitle = "Success!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(deliverySlotObj.StoreId, "API.ErrorMesaage.DeliverySlotSaved", languageId, dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                customerAPIResponses.ResponseObj = true;
                return customerAPIResponses;
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
        }
    }
}
