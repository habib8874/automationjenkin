﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Services.Orders;
using Nop.Web.Areas.Admin.Models.BookingTable;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;

namespace Nop.Web.Components
{
    public class BookTableViewComponent : NopViewComponent
    {
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;

        public BookTableViewComponent(IShoppingCartModelFactory shoppingCartModelFactory,
            IStoreContext storeContext,
            IWorkContext workContext)
        {
            this._shoppingCartModelFactory = shoppingCartModelFactory;
            this._storeContext = storeContext;
            this._workContext = workContext;
        }

        //public IViewComponentResult Invoke(string merchantid)
        public IViewComponentResult Invoke()
        {
            //New Logic Added By Mah 22-03-2019
           
            string merchantid = _workContext.CurrentMerchant.Id.ToString(); 
            var model = new BookingTableTransactionListSearchModel();
            if(!string.IsNullOrEmpty(merchantid))
                model.VendorId = Convert.ToInt32( merchantid);
            return View(model);            
        }
    }
}
