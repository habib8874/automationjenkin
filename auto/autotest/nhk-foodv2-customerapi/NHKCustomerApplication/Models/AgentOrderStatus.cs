﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class AgentOrderStatus
    {
        public int Id { get; set; }
        public int? AgentId { get; set; }
        public int OrderId { get; set; }
        public DateTime? OrderDt { get; set; }
        public int OrderStatus { get; set; }
        public DateTime? AgentStatusDt { get; set; }
        public string AgentLat { get; set; }
        public string AgentLong { get; set; }
        public DateTime? SysDt { get; set; }
    }
}
