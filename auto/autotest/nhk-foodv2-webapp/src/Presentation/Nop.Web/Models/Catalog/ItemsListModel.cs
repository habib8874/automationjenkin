﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Catalog
{
    /// <summary>
    /// ItemsListModel model for catalog
    /// </summary>
    public partial class ItemsListModel : BaseNopModel
    {
        #region Ctor

        /// <summary>
        /// Constructor
        /// </summary>
        public ItemsListModel()
        {
            this.SelectedImageTags = new List<SelectListItem>();
            this.Items = new List<ProductOverviewModelMerchant>();
            PagingFilteringContext = new CatalogPagingFilteringModel();
            SortingList = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        public string Keywords { get; set; }
        public List<SelectListItem> SelectedImageTags { get; set; }
        public List<ProductOverviewModelMerchant> Items { get; set; }
        public string CategoryName { get; set; }
        public CatalogPagingFilteringModel PagingFilteringContext { get; set; }
        public IList<SelectListItem> SortingList { get; set; }
        #endregion

    }
}