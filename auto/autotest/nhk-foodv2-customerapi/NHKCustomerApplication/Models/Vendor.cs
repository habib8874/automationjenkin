﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class Vendor
    {
        public Vendor()
        {
            VendorNote = new HashSet<VendorNote>();
            VendorScheduleMapping = new HashSet<VendorScheduleMapping>();
            DiscountAppliedToVendor = new HashSet<DiscountAppliedToVendor>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public int PictureId { get; set; }
        public int AddressId { get; set; }
        public string AdminComment { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
        public int DisplayOrder { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        public int PageSize { get; set; }
        public bool AllowCustomersToSelectPageSize { get; set; }
        public string PageSizeOptions { get; set; }
        public int? StoreId { get; set; }
        public string Geolocation { get; set; }
        public string Geofancing { get; set; }
        public string WeeklyOff { get; set; }
        public string Opentime { get; set; }
        public string Closetime { get; set; }
        public bool? IsOpen { get; set; }
        public bool? IsTakeAway { get; set; }
        public bool? IsDelivery { get; set; }
        public bool? IsDining { get; set; }
        public string ExpDeliveryTime { get; set; }
        public string CostFor { get; set; }
        public int? TakeAwayTime { get; set; }
        public int? DeliveryTime { get; set; }
        public decimal? MinimumOrderValue { get; set; }
        public TimeSpan? LunchFrom { get; set; }
        public TimeSpan? LunchTo { get; set; }
        public TimeSpan? DinnerFrom { get; set; }
        public TimeSpan? DinnerTo { get; set; }
        public bool? IsBookTableEnable { get; set; }
        public bool? RatingPercent { get; set; }
        public bool? IsDeliveryTimeEnable { get; set; }
        public bool? IsCostForEnable { get; set; }
        public int? AvailableType { get; set; }
        public bool? IsAvailableSchedulesEnable { get; set; }
        public decimal DeliveryCharge { get; set; }
        public string FixedOrDynamic { get; set; }

        public string KmOrMilesForDynamic { get; set; }

        public decimal PriceForDynamic { get; set; }

        public decimal DistanceForDynamic { get; set; }

        public decimal PricePerUnitDistanceForDynamic { get; set; }

        public virtual ICollection<VendorNote> VendorNote { get; set; }
        public virtual ICollection<VendorScheduleMapping> VendorScheduleMapping { get; set; }
        public virtual ICollection<DiscountAppliedToVendor> DiscountAppliedToVendor { get; set; }
        
    }
}
