﻿namespace Nop.Core.Domain.NB
{
    /// <summary>
    /// Represents default values related to nb data
    /// </summary>
    public static partial class NopNbDomainDefaults
    {
        #region SubAdmin attributes

        /// <summary>
        /// Gets a name of generic attribute to store the value of Subadmin's Associated 'MerchantId' for MerchantType subadmin.
        /// </summary>
        public static string SubAdminMerchantIdAttribute => "SubAdminMerchantId";

        #endregion

        #region Rider/Agent attributes

        /// <summary>
        /// Gets a name of generic attribute to store the value of 'AgentDocument1Id'
        /// </summary>
        public static string AgentIdProofDocIdAttribute => "AgentIdProofDocId";

        /// <summary>
        /// Gets a name of generic attribute to store the value of 'AgentDocument2Id'
        /// </summary>
        public static string AgentRCBookDocIdAttribute => "AgentRCBookDocId";

        /// <summary>
        /// Gets a name of generic attribute to store the value of 'AgentAcountActivated'
        /// </summary>
        public static string AgentAcountActivatedAttribute => "AgentAcountActivated";

        #endregion

        #region Customer attributes

        /// <summary>
        /// Gets a name of generic attribute to store the value of 'SearchedLocationLatLong'
        /// </summary>
        public static string SearchedLocationLatLongAttribute => "SearchedLocationLatLong";

        #endregion
    }
}
