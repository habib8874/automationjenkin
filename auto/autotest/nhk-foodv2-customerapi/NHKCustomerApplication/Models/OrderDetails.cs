﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class OrderDetails
    {
        public int Id { get; set; }
        public int? StoreId { get; set; }
        public int? CustomerId { get; set; }
        public int? OrderType { get; set; }
        public string OrderNote { get; set; }
        public string EnteredLocation { get; set; }
        public bool IsOrderComplete { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public int? OrderTime { get; set; }
        public DateTime? ScheduleDate { get; set; }
        public TimeSpan? ScheduleTime { get; set; }
        public decimal? Tip { get; set; }
        public decimal? DeliveryCharges { get; set; }
    }
}
