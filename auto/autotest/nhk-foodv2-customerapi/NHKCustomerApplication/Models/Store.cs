﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class Store
    {
        public Store()
        {
            BlogComment = new HashSet<BlogComment>();
            NewsComment = new HashSet<NewsComment>();
            ProductReview = new HashSet<ProductReview>();
            StoreMapping = new HashSet<StoreMapping>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool SslEnabled { get; set; }
        public string Hosts { get; set; }
        public int DefaultLanguageId { get; set; }
        public int DisplayOrder { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhoneNumber { get; set; }
        public string CompanyVat { get; set; }
        public int? StoreOwnerId { get; set; }
        public bool? IsDisplayDeliveryOrTakeaway { get; set; }
        public bool? IsAggrigator { get; set; }
        public bool? IsDelivery { get; set; }
        public bool? IsTakeaway { get; set; }
        public bool? IsDinning { get; set; }
        public int? CountryId { get; set; }

        public virtual ICollection<BlogComment> BlogComment { get; set; }
        public virtual ICollection<NewsComment> NewsComment { get; set; }
        public virtual ICollection<ProductReview> ProductReview { get; set; }
        public virtual ICollection<StoreMapping> StoreMapping { get; set; }
    }
}
