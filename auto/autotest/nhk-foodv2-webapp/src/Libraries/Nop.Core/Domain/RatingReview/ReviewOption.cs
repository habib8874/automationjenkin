﻿using Nop.Core.Domain.Localization;

namespace Nop.Core.Domain.RatingReview
{
    /// <summary>
    /// Represents a product review
    /// </summary>
    public partial class ReviewOption : BaseEntity, ILocalizedEntity
    {
        /// <summary>
        /// Gets or sets the Setting Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Entity type identifier (Agent/Merchant/Item)
        /// </summary>
        public int EntityType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the setting is active
        /// </summary>
        public bool Active { get; set; }

    }
}
