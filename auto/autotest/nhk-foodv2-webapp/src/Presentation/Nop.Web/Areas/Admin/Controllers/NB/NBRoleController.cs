﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.NB.Merchant;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.NB.Merchant;
using Nop.Services.Security;
using Nop.Services.Vendors;
using Nop.Web.Areas.Admin.Factories.NB;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Customers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers.NB
{
    public partial class NBRoleController : BaseAdminController
    {

        #region Fields

        private readonly INbRoleModelFactory _nbRoleModelFactory;
        private readonly IPermissionService _permissionService;
        private readonly ICustomerService _customerService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IVendorCustomerRoleServices _vendorCustomerRoleServices;
        private readonly IVendorService _vendorService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public NBRoleController(INbRoleModelFactory nbRoleModelFactory,
            IPermissionService permissionService,
            ICustomerService customerService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            INotificationService notificationService,
            IVendorCustomerRoleServices vendorCustomerRoleServices,
            IVendorService vendorService,
            IWorkContext workContext)
        {
            _nbRoleModelFactory = nbRoleModelFactory;
            _permissionService = permissionService;
            _customerService = customerService;
            _customerActivityService = customerActivityService;
            _localizationService = localizationService;
            _notificationService = notificationService;
            _vendorCustomerRoleServices = vendorCustomerRoleServices;
            _vendorService = vendorService;
            _workContext = workContext;
        }

        #endregion

        #region Methods

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBRoles))
                return AccessDeniedView();

            if (_workContext.SubAdmin.IsSubAdminRole)
                return AccessDeniedView();

            //prepare model
            var model = _nbRoleModelFactory.PrepareNBRoleSearchModel(new CustomerRoleSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(CustomerRoleSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBRoles))
                return AccessDeniedDataTablesJson();

            if (_workContext.SubAdmin.IsSubAdminRole)
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _nbRoleModelFactory.PrepareNBRoleListModel(searchModel);

            return Json(model);
        }

        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBRoles) || !_permissionService.Authorize(StandardPermissionProvider.ManageAcl))
                return AccessDeniedView();

            if (_workContext.SubAdmin.IsSubAdminRole)
                return AccessDeniedView();

            //prepare model
            var model = _nbRoleModelFactory.PrepareNBRoleModel(new CustomerRoleModel(), null);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Create(CustomerRoleModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBRoles) || !_permissionService.Authorize(StandardPermissionProvider.ManageAcl))
                return AccessDeniedView();

            //validations
            if (model.UserGroupId == 0)
                ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.Configuration.Roles.Error.UserGroup"));
            if (string.IsNullOrEmpty(model.SystemName))
                ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.Configuration.Roles.Error.RoleSystemName"));
            if (string.IsNullOrEmpty(model.PhoneNumber))
                ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.Configuration.Roles.Error.PhoneNumber"));
            if (model.UserGroupId == (int)UserGroups.Business)
            {
                if (model.SelectedStoreId == 0)
                    ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.Configuration.Roles.Error.Store"));

                if (model.SelctedMerchantIds.Count == 0)
                    ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.Configuration.Roles.Error.Merchant"));
            }

            if (ModelState.IsValid)
            {
                var customerRole = model.ToEntity<CustomerRole>();
                customerRole.CreatedByNB = true;
                _customerService.InsertCustomerRole(customerRole);

                #region mappings

                //vendor customer role mapping
                var existingMappings = _vendorCustomerRoleServices.GetAllMappingsByCustomerRoleId(customerRole.Id);

                //if user group is Merchant than there'll be no Merachnt to be mapped. So delete if any existance. 
                //For that lets remove selected merchantIds if any.
                if (model.UserGroupId == (int)UserGroups.Merchant)
                    model.SelctedMerchantIds = new List<int>();

                if (existingMappings != null && existingMappings.Count > 0)
                {
                    var toBeDeleteMerchantIds = existingMappings.Where(sel => model.SelctedMerchantIds.All(ex => ex != sel.VendorId));
                    foreach (var merchantId in toBeDeleteMerchantIds.Select(x => x.VendorId))
                    {
                        //delete
                        _vendorCustomerRoleServices.DeleteVendorCustomerRoleMapping(existingMappings.FirstOrDefault(m => m.VendorId == merchantId));
                    }
                }

                foreach (var merchantId in model.SelctedMerchantIds)
                {
                    //mapping not found, than insert
                    if (existingMappings == null || !existingMappings.Select(x => x.VendorId).Contains(merchantId))
                    {
                        var mapping = new VendorCustomerRoleMapping
                        {
                            VendorId = merchantId,
                            CustomerRoleId = customerRole.Id
                        };
                        _vendorCustomerRoleServices.InsertVendorCustomerRoleMapping(mapping);
                    }
                }

                #endregion

                //activity log
                _customerActivityService.InsertActivity("AddNewCustomerRole",
                    string.Format(_localizationService.GetResource("ActivityLog.AddNewCustomerRole"), customerRole.Name), customerRole);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Customers.CustomerRoles.Added"));

                return continueEditing ? RedirectToAction("Edit", new { id = customerRole.Id }) : RedirectToAction("List");
            }

            //prepare model
            var previouseSelectedMerchantIds = model.SelctedMerchantIds; //for T6048
            model = _nbRoleModelFactory.PrepareNBRoleModel(model, null, true);
            model.SelctedMerchantIds = previouseSelectedMerchantIds; //for T6048

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBRoles) || !_permissionService.Authorize(StandardPermissionProvider.ManageAcl))
                return AccessDeniedView();

            if (_workContext.SubAdmin.IsSubAdminRole)
                return AccessDeniedView();

            //try to get a customer role with the specified id
            var customerRole = _customerService.GetCustomerRoleById(id);
            if (customerRole == null)
                return RedirectToAction("List");

            //prepare model
            var model = _nbRoleModelFactory.PrepareNBRoleModel(null, customerRole);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(CustomerRoleModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBRoles) || !_permissionService.Authorize(StandardPermissionProvider.ManageAcl))
                return AccessDeniedView();

            if (_workContext.SubAdmin.IsSubAdminRole)
                return AccessDeniedView();

            //try to get a customer role with the specified id
            var customerRole = _customerService.GetCustomerRoleById(model.Id);
            if (customerRole == null)
                return RedirectToAction("List");

            try
            {
                //validations
                if (model.UserGroupId == 0)
                    ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.Configuration.Roles.Error.UserGroup"));
                if (string.IsNullOrEmpty(model.SystemName))
                    ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.Configuration.Roles.Error.RoleSystemName"));
                if (string.IsNullOrEmpty(model.PhoneNumber))
                    ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.Configuration.Roles.Error.PhoneNumber"));
                if (model.UserGroupId == (int)UserGroups.Business)
                {
                    if (model.SelectedStoreId == 0)
                        ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.Configuration.Roles.Error.Store"));

                    if (model.SelctedMerchantIds.Count == 0)
                        ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.Configuration.Roles.Error.Merchant"));
                }

                if (ModelState.IsValid)
                {
                    if (customerRole.IsSystemRole && !model.Active)
                        throw new NopException(_localizationService.GetResource("Admin.Customers.CustomerRoles.Fields.Active.CantEditSystem"));

                    if (customerRole.IsSystemRole && !customerRole.SystemName.Equals(model.SystemName, StringComparison.InvariantCultureIgnoreCase))
                        throw new NopException(_localizationService.GetResource("Admin.Customers.CustomerRoles.Fields.SystemName.CantEditSystem"));

                    if (NopCustomerDefaults.RegisteredRoleName.Equals(customerRole.SystemName, StringComparison.InvariantCultureIgnoreCase) &&
                        model.PurchasedWithProductId > 0)
                        throw new NopException(_localizationService.GetResource("Admin.Customers.CustomerRoles.Fields.PurchasedWithProduct.Registered"));

                    customerRole = model.ToEntity(customerRole);
                    customerRole.CreatedByNB = true;
                    _customerService.UpdateCustomerRole(customerRole);

                    #region mappings

                    //vendor customer role mapping
                    var existingMappings = _vendorCustomerRoleServices.GetAllMappingsByCustomerRoleId(customerRole.Id);

                    //if user group is Merchant than there'll be no Merachnt to be mapped. So delete if any existance. 
                    //For that lets remove selected merchantIds if any.
                    if (model.UserGroupId == (int)UserGroups.Merchant)
                        model.SelctedMerchantIds = new List<int>();

                    if (existingMappings != null && existingMappings.Count > 0)
                    {
                        var toBeDeleteMerchantIds = existingMappings.Where(sel => model.SelctedMerchantIds.All(ex => ex != sel.VendorId));
                        foreach (var merchantId in toBeDeleteMerchantIds.Select(x => x.VendorId))
                        {
                            //delete
                            _vendorCustomerRoleServices.DeleteVendorCustomerRoleMapping(existingMappings.FirstOrDefault(m => m.VendorId == merchantId));
                        }
                    }

                    foreach (var merchantId in model.SelctedMerchantIds)
                    {
                        //mapping not found, than insert
                        if (existingMappings == null || !existingMappings.Select(x => x.VendorId).Contains(merchantId))
                        {
                            var mapping = new VendorCustomerRoleMapping
                            {
                                VendorId = merchantId,
                                CustomerRoleId = customerRole.Id
                            };
                            _vendorCustomerRoleServices.InsertVendorCustomerRoleMapping(mapping);
                        }
                    }

                    #endregion

                    //activity log
                    _customerActivityService.InsertActivity("EditCustomerRole",
                        string.Format(_localizationService.GetResource("ActivityLog.EditCustomerRole"), customerRole.Name), customerRole);

                    _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Customers.CustomerRoles.Updated"));

                    return continueEditing ? RedirectToAction("Edit", new { id = customerRole.Id }) : RedirectToAction("List");
                }

                //prepare model
                var previouseSelectedMerchantIds = model.SelctedMerchantIds; //for T6048
                model = _nbRoleModelFactory.PrepareNBRoleModel(model, customerRole, true);
                model.SelctedMerchantIds = previouseSelectedMerchantIds; //for T6048

                //if we got this far, something failed, redisplay form
                return View(model);
            }
            catch (Exception exc)
            {
                _notificationService.ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = customerRole.Id });
            }
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBRoles) || !_permissionService.Authorize(StandardPermissionProvider.ManageAcl))
                return AccessDeniedView();

            if (_workContext.SubAdmin.IsSubAdminRole)
                return AccessDeniedView();

            //try to get a customer role with the specified id
            var customerRole = _customerService.GetCustomerRoleById(id);
            if (customerRole == null)
                return RedirectToAction("List");

            try
            {
                _customerService.DeleteCustomerRole(customerRole);

                //activity log
                _customerActivityService.InsertActivity("DeleteCustomerRole",
                    string.Format(_localizationService.GetResource("ActivityLog.DeleteCustomerRole"), customerRole.Name), customerRole);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Customers.CustomerRoles.Deleted"));

                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                _notificationService.ErrorNotification(exc.Message);
                return RedirectToAction("Edit", new { id = customerRole.Id });
            }
        }

        public virtual IActionResult LoadStoreVendors(int storeId, int customerRoleId)
        {
            var vendorsSelectedList = new List<SelectListItem>();
            if (storeId > 0)
            {
                var vendors = _vendorService.GetAllVendors(showHidden: false)?.Where(x => x.StoreId == storeId);
                vendorsSelectedList = vendors?.Select(v => new SelectListItem
                {
                    Text = v.Name,
                    Value = v.Id.ToString()
                }).ToList();
            }

            var model = new CustomerRoleModel();
            model.AvailableMerchants = vendorsSelectedList;
            model.SelctedMerchantIds = _vendorCustomerRoleServices.GetAllMappingsByCustomerRoleId(customerRoleId)?.Select(x => x.VendorId)?.ToList() ?? new List<int>();

            var merchantSelectedList = RenderPartialViewToString("~/Areas/Admin/Views/NBRole/_StoreAndMerchants.cshtml", model);

            return Json(new
            {
                success = true,
                merchantSelectedList = merchantSelectedList
            });
            //return Json(vendorsSelectedList);
        }

        #endregion
    }
}
