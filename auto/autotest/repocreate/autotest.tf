#define the Provider
terraform {
    required_providers {
        gitlab = {
            source = "gitlabhq/gitlab"
        }
    }
}


# Configure the GitLab Provider
provider "gitlab" {
    token = "-MkUgtqWKcAAEBxiPzyG"
}

variable "reponame" {
    type = list
    default = ["nhk-food-autotest-web", "nhk-food-autotest-customerAPI"]
}


resource "gitlab_project" "test" {
  name = var.reponame[count.index]
  count = 2
  #description = "My awesome codebase"
  default_branch = "deployment-development"
  visibility_level = "private"
  
}

