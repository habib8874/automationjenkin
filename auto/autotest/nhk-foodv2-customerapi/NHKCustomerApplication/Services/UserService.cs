﻿using Microsoft.EntityFrameworkCore;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Repository;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using CustomerDetails = NHKCustomerApplication.Models.CustomerDetails;


namespace NHKCustomerApplication.Services
{
    /// <summary>
    /// UserService.
    /// </summary>
    public class UserService : IUserService
    {
        #region "Fields"

        private const string PASSWORDFORMAT = "SHA512";
        private readonly IUserRepository _userRepository;
        private readonly ecuadordevContext _dbcontext;

        public string CurrencySymbol { get; set; }
        #endregion

        #region "Constructor"

        public UserService(IUserRepository userRepository, ecuadordevContext dbcontext)
        {
            _userRepository = userRepository;
            _dbcontext = dbcontext;
        }

        #endregion

        #region "private methods"

        /// <summary>
        /// ValidateRestCustomer.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        private CustomerAPIResponses ValidateRestCustomer(CooknRestProductByID customer, int languageId)
        {
            if (customer != null && !string.IsNullOrEmpty(customer.APIKey) && customer.CustId > 0 && customer.RestnCookId > 0 && customer.StoreId > 0)
            {
                return null;
            }
            else
            {
                if (customer == null)
                {
                    return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, _dbcontext));
                }
                if (string.IsNullOrEmpty(customer.APIKey))
                {
                    return Helper.GetCustomerAPIErrorResponses("Key Not Defined");
                }
                if (customer.CustId == 0)
                {
                    return Helper.GetCustomerAPIErrorResponses("Customer Id Not Defined");
                }
                if (customer.RestnCookId == 0)
                {
                    return Helper.GetCustomerAPIErrorResponses("Restaurant  Id Not Defined");
                }
                if (customer.StoreId == 0)
                {
                    return Helper.GetCustomerAPIErrorResponses("Store is required");
                }
            }
            return null;
        }

        /// <summary>
        /// ValidateUserlogin.
        /// </summary>
        /// <param name="customer">CustomerLogin object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        private CustomerAPIResponses ValidateUserloginV3(CustomerLoginV3 customer, int languageId)
        {
            if (customer == null)
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.ApiKey))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.Email) && string.IsNullOrEmpty(customer.Phone))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EnterMobileOrEmail", languageId, _dbcontext));
            }
            if (customer.StoreId == 0)
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.PasswordRequired", languageId, _dbcontext));
            }
            return null;
        }

        /// <summary>
        /// ValidateUser.
        /// </summary>
        /// <param name="customer">CustomerModel object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        private CustomerAPIResponses ValidateUser(CustomerModel customer, int languageId)
        {
            if (customer == null)
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.ApiKey))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.DeviceToken))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.DeviceTokenMissing", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.Email))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EmailRequired", languageId, _dbcontext));
            }
            if (!string.IsNullOrEmpty(customer.Email))
            {
                try
                {
                    customer.Email = customer.Email.ToLower().ToString();
                    var addr = Regex.IsMatch(customer.Email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
                    //addr.Address == customer.Email;
                    if (!addr)
                    {
                        return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ProvideValidEmail", languageId, _dbcontext));
                    }
                }
                catch (Exception ex)
                {
                    return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ProvideValidEmail", languageId, _dbcontext));
                }

            }
            if (string.IsNullOrEmpty(customer.MobileNo))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.MobilenumberRequired", languageId, _dbcontext));
            }
            if (!string.IsNullOrEmpty(customer.MobileNo))
            {
                try
                {
                    string Number = customer.MobileNo.Split(' ')[1];
                    if (!string.IsNullOrEmpty(Number))
                    {
                        bool invalidNo = false;
                        foreach (char c in Number)
                        {
                            if (c < '0' || c > '9')
                            {
                                invalidNo = true;
                                break;
                            }
                        }
                        if (invalidNo)
                        {
                            return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ProvideValidPhoneNumber", languageId, _dbcontext));
                        }

                    }
                }
                catch
                {
                    return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ProvideValidPhoneNumber", languageId, _dbcontext));
                }
            }
            if (string.IsNullOrEmpty(customer.Pswd))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.PasswordRequired", languageId, _dbcontext));
            }
            if (customer.StoreId == 0)
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.StoreRequired", languageId, _dbcontext));
            }
            return null;
        }

        /// <summary>
        /// ValidateUser.
        /// </summary>
        /// <param name="customer">ExternalAuthModel object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        private CustomerAPIResponses ValidateUser(ExternalAuthModel customer, int languageId)
        {
            if (customer == null)
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.ApiKey))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.DeviceToken))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.DeviceTokenMissing", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.Email) && string.IsNullOrEmpty(customer.MobileNo))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EnterMobileOrEmail", languageId, _dbcontext));
            }
            if (!string.IsNullOrEmpty(customer.Email))
            {
                try
                {
                    customer.Email = customer.Email.ToLower().ToString();
                    var addr = Regex.IsMatch(customer.Email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
                    //addr.Address == customer.Email;
                    if (!addr)
                    {
                        return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ProvideValidEmail", languageId, _dbcontext));
                    }
                }
                catch (Exception ex)
                {
                    return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ProvideValidEmail", languageId, _dbcontext));
                }

            }
            if (!string.IsNullOrEmpty(customer.MobileNo))
            {
                try
                {
                    string Number = customer.MobileNo.Split(' ')[1];
                    if (!string.IsNullOrEmpty(Number))
                    {
                        bool invalidNo = false;
                        foreach (char c in Number)
                        {
                            if (c < '0' || c > '9')
                            {
                                invalidNo = true;
                                break;
                            }
                        }
                        if (invalidNo)
                        {
                            return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ProvideValidPhoneNumber", languageId, _dbcontext));
                        }

                    }
                }
                catch
                {
                    return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ProvideValidPhoneNumber", languageId, _dbcontext));
                }
            }
            if (customer.StoreId == 0)
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.StoreRequired", languageId, _dbcontext));
            }
            return null;
        }

        /// <summary>
        /// ValidateGuestUser.
        /// </summary>
        /// <param name="customer">GuestUserModel object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        private CustomerAPIResponses ValidateGuestUser(GuestUserModel customer, int languageId)
        {
            if (customer == null)
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.ApiKey))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.DeviceToken))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.DeviceTokenMissing", languageId, _dbcontext));
            }
            if (customer.StoreId == 0)
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.StoreRequired", languageId, _dbcontext));
            }
            return null;
        }
        /// <summary>
        /// ValidateRegistrationVerificationUser.
        /// </summary>
        /// <param name="customer">RegistationverificationCustomer object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        private CustomerAPIResponses ValidateRegistrationVerificationUser(RegistationverificationCustomer customer, int languageId)
        {
            if (customer != null && !string.IsNullOrEmpty(customer.ApiKey) && customer.CustomerId > 0 && !string.IsNullOrEmpty(customer.OTP))
            {
                return null;
            }
            else
            {
                if (customer == null)
                {
                    return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, _dbcontext));
                }
                if (string.IsNullOrEmpty(customer.ApiKey))
                {
                    return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, _dbcontext));
                }
                if (customer.CustomerId == 0)
                {
                    return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, _dbcontext));
                }
                if (string.IsNullOrEmpty(customer.OTP))
                {
                    return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.OtpMismatch", languageId, _dbcontext));
                }
            }
            return null;
        }

        /// <summary>
        /// ValidateUserlogin.
        /// </summary>
        /// <param name="customer">CustomerLogin object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        private CustomerAPIResponses ValidateUserlogin(CustomerLogin customer, int languageId)
        {
            if (customer == null)
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.ApiKey))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.DeviceToken))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.DeviceTokenMissing", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.Email) && string.IsNullOrEmpty(customer.Phone))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EnterMobileOrEmail", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.Pswd))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EnterMobileOrEmail", languageId, _dbcontext));
            }
            if (customer.StoreId == 0)
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.PasswordRequired", languageId, _dbcontext));
            }
            return null;
        }

        /// <summary>
        /// ValidateUserlogin.
        /// </summary>
        /// <param name="customer">CustomerLogin object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        private CustomerAPIResponses ValidateUserloginV2_1(CustomerLoginV2_1 customer, int languageId)
        {
            if (customer == null)
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.ApiKey))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.DeviceToken))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.DeviceTokenMissing", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.Email) && string.IsNullOrEmpty(customer.Phone))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EnterMobileOrEmail", languageId, _dbcontext));
            }
            if (string.IsNullOrEmpty(customer.Pswd))
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EnterMobileOrEmail", languageId, _dbcontext));
            }
            if (customer.StoreId == 0)
            {
                return Helper.GetCustomerAPIErrorResponses(LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.PasswordRequired", languageId, _dbcontext));
            }
            return null;
        }

        #endregion

        /// <summary>
        /// RegisterCustomer.
        /// </summary>
        /// <param name="customer">CustomerModel object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        public CustomerAPIResponses RegisterCustomer(CustomerModel customer)
        {
            int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, _dbcontext);
            CustomerAPIResponses inValidUser = ValidateUser(customer, languageId);
            if (inValidUser == null)
            {
                CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
                string saltKey = Helper.CreateSaltKey(5);
                CustomerSignUpResponseModel customerSignUpResponse = new CustomerSignUpResponseModel();
                const int _min = 1000;
                const int _max = 9999;
                Random _rdm = new Random();
                string otp = _rdm.Next(_min, _max).ToString();
                string password = Helper.CreatePasswordHash(customer.Pswd, saltKey, PASSWORDFORMAT);
                Customer customerObj = new Customer();
                var result = _userRepository.RegisterCustomer(customer, saltKey, password, otp, languageId);
                try
                {
                    if (result.ValidData)
                    {
                        int userid = Convert.ToInt32(result.ResultData);
                        customerSignUpResponse.UserID = userid;
                    }
                    else
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = result.ResultData;
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Ambiguous;
                        customerAPIResponses.ResponseObj = null;
                        if (result.ResultData == "Invalid Authentication key")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        }
                        if (result.ResultData == "Email is already registered with us!!Please login with same email.")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.Emailisalreadyregisteredwithus!!Pleaseloginwithsameemail", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        }
                        if (result.ResultData == "Phone number is already registered with us!!Please login with same phone number.")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.Phonenumberisalreadyregisteredwithus!!Pleaseloginwithsamephonenumber", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        }
                        return customerAPIResponses;
                    }
                }
                catch (Exception ex)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.InternalServerError", languageId, _dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }

                customerSignUpResponse.Email = customer.Email;
                customerSignUpResponse.MobileNo = customer.MobileNo;
                customerSignUpResponse.FirstName = customer.FirstName;
                customerSignUpResponse.LastName = customer.LastName;
                customerSignUpResponse.UserStatus = customerObj.Active == true ? "Active" : "InActive";
                customerSignUpResponse.DeviceToken = customer.DeviceToken;
                customerSignUpResponse.OTP = otp;
                customerSignUpResponse.IsFCMToken = (customer.DeviceToken != "" && customer.DeviceToken != null) ? true : false;
                customerAPIResponses.ErrorMessageTitle = "Success!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerRegisteredSuccessfully", languageId, _dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = 200;
                customerAPIResponses.ResponseObj = customerSignUpResponse;
                return customerAPIResponses;
            }
            else
            {
                return inValidUser;
            }
        }

        /// <summary>
        /// ExternalAuthModel.
        /// </summary>
        /// <param name="customer">ExternalAuthModel object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        public CustomerAPIResponses ExternalAuth(ExternalAuthModel customer)
        {
            int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, _dbcontext);
            CustomerAPIResponses inValidUser = ValidateUser(customer, languageId);
            if (inValidUser == null)
            {
                CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
                CustomerSignUpResponseModel customerSignUpResponse = new CustomerSignUpResponseModel();
                Customer customerObj = new Customer();
                
                customer.Email = string.IsNullOrEmpty(customer.Email) ? Helper.GetCustomEmail(customer.StoreId, customer.MobileNo, _dbcontext) : customer.Email;
                var result = _userRepository.ExternalAuth(customer);
                try
                {
                    if (result.ValidData && Helper.IsNumeric(result.ResultData))
                    {
                        int userid = Convert.ToInt32(result.ResultData);
                        customerSignUpResponse.UserID = userid;
                    }
                    else
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = result.ResultData;
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Ambiguous;
                        customerAPIResponses.ResponseObj = null;
                        if (result.ResultData == "Invalid Authentication key")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        }
                        else
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.InternalServerError", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                        }
                        return customerAPIResponses;
                    }
                }
                catch (Exception ex)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.InternalServerError", languageId, _dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                var store = _dbcontext.Store.AsEnumerable().Where(x => x.Id == customer.StoreId).FirstOrDefault();
                var CustomerAddressDetails = _dbcontext.GenericAttribute.Where(x => x.EntityId == customerObj.Id).ToList();
                CustomerDetails customerDetail = new CustomerDetails();
                customerDetail.CrDt = DateTime.UtcNow;
                customerDetail.CustomerId = Convert.ToInt32(result.ResultData);
                customerDetail.DeviceNo = customer.DeviceNo;
                customerDetail.DeviceToken = customer.DeviceToken;
                customerDetail.Imei1 = customer.Imei1;
                customerDetail.Imei2 = customer.Imei2;
                customerDetail.LatPos = customer.LatPos;
                customerDetail.LongPos = customer.LongPos;
                int _min = 1000;
                int _max = 9999;
                Random _rdm = new Random();
                customerDetail.Otp = _rdm.Next(_min, _max).ToString();
                customerDetail.Otpfailed = 0;
                customerDetail.OtpresendCount = 0;
                customerDetail.CrDt = DateTime.UtcNow;
                customerDetail.DeviceType = string.IsNullOrEmpty(customer.DeviceType) ? "A" : customer.DeviceType;
                _dbcontext.CustomerDetails.Add(customerDetail);
                _dbcontext.SaveChanges();

                customerSignUpResponse.Email = customer.Email;
                customerSignUpResponse.MobileNo = customer.MobileNo;
                customerSignUpResponse.FirstName = customer.FirstName;
                customerSignUpResponse.LastName = customer.LastName;
                customerSignUpResponse.UserStatus = "Active";
                customerSignUpResponse.DeviceToken = customer.DeviceToken;
                customerSignUpResponse.OTP = customerDetail.Otp;
                customerSignUpResponse.IsFCMToken = (customer.DeviceToken != "" && customer.DeviceToken != null) ? true : false;
                customerAPIResponses.ErrorMessageTitle = "Success!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerRegisteredSuccessfully", languageId, _dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = 200;
                customerAPIResponses.ResponseObj = customerSignUpResponse;
                return customerAPIResponses;
            }
            else
            {
                return inValidUser;
            }
        }

        /// <summary>
        /// Create guest user.
        /// </summary>
        /// <param name="customer">GuestUserModel object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        public CustomerAPIResponses CreateGuestUser(GuestUserModel customer)
        {
            int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, _dbcontext);
            CustomerAPIResponses inValidUser = ValidateGuestUser(customer, languageId);
            if (inValidUser == null)
            {
                CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
                string saltKey = Helper.CreateSaltKey(5);
                CustomerSignUpResponseModel customerSignUpResponse = new CustomerSignUpResponseModel();
                const int _min = 1000;
                const int _max = 9999;
                Random _rdm = new Random();
                string otp = _rdm.Next(_min, _max).ToString();
                Customer customerObj = new Customer();
                var result = _userRepository.InsertGuestUser(customer, otp);
                try
                {
                    if (result.ValidData)
                    {
                        int userid = Convert.ToInt32(result.ResultData);
                        customerSignUpResponse.UserID = userid;
                    }
                    else
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = result.ResultData;
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Ambiguous;
                        customerAPIResponses.ResponseObj = null;
                        if (result.ResultData == "Invalid Authentication key")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        }
                        return customerAPIResponses;
                    }
                }
                catch (Exception ex)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.InternalServerError", languageId, _dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }

                customerAPIResponses.ErrorMessageTitle = "Success!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerRegisteredSuccessfully", languageId, _dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = 200;
                customerAPIResponses.ResponseObj = customerSignUpResponse;
                return customerAPIResponses;
            }
            else
            {
                return inValidUser;
            }
        }

        /// <summary>
        /// RegistaionVerificationCustomer.
        /// </summary>
        /// <param name="customer">RegistationverificationCustomer object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        public CustomerAPIResponses RegistaionVerificationCustomer(RegistationverificationCustomer customer)
        {
            int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, _dbcontext);
            CustomerAPIResponses inValidUser = ValidateRegistrationVerificationUser(customer, languageId);
            if (inValidUser == null)
            {
                CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
                var result = _userRepository.RegistaionVerificationCustomer(customer);
                try
                {
                    if (result.ValidData)
                    {
                        bool validOTP = Convert.ToBoolean(result.ValidData);
                        customerAPIResponses.ErrorMessageTitle = "Success!!";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.OtpVerifiedSuccessfully", languageId, _dbcontext);
                        customerAPIResponses.Status = true;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                        customerAPIResponses.ResponseObj = customer.CustomerId;
                        return customerAPIResponses;
                    }
                    else
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = result.ResultData;
                        customerAPIResponses.Status = false;
                        customerAPIResponses.ResponseObj = null;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        return customerAPIResponses;
                    }
                }
                catch (Exception ex)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.InternalServerError", languageId, _dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.ResponseObj = null;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
                    return customerAPIResponses;
                }
            }
            else
            {
                return inValidUser;
            }
        }

        /// <summary>
        /// RegistaionVerificationCustomer.
        /// </summary>
        /// <param name="customer">RegistationverificationCustomer object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        public CustomerAPIResponses RegistaionVerificationCustomerV3(RegistationverificationCustomer customer)
        {
            int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, _dbcontext);
            CustomerAPIResponses inValidUser = ValidateRegistrationVerificationUser(customer, languageId);
            if (inValidUser == null)
            {
                CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
                var response = new LoginOtpTimerSecond();
                var result = _userRepository.RegistaionVerificationCustomerV3(customer);
                try
                {
                    if (result.ValidData)
                    {
                        var seconds = _dbcontext.Setting.Where(x => x.Name.Contains("adminareasettings.loginotptimer") && x.StoreId == customer.StoreId).FirstOrDefault()?.Value ?? "60";
                        response.CustomerID = customer.CustomerId;
                        response.Second = seconds;
                        bool validOTP = Convert.ToBoolean(result.ValidData);
                        customerAPIResponses.ErrorMessageTitle = "Success!!";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.OtpVerifiedSuccessfully", languageId, _dbcontext);
                        customerAPIResponses.Status = true;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                        customerAPIResponses.ResponseObj = response;
                        return customerAPIResponses;
                    }
                    else
                    {
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaageChefNotProvidingAnyProductForSellYet", languageId, _dbcontext);
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = result.ResultData;
                        customerAPIResponses.Status = false;
                        customerAPIResponses.ResponseObj = null;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        return customerAPIResponses;
                    }
                }
                catch (Exception ex)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.InternalServerError", languageId, _dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.ResponseObj = null;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
                    return customerAPIResponses;
                }
            }
            else
            {
                return inValidUser;
            }
        }

        /// <summary>
        /// CustomerLogin.
        /// </summary>
        /// <param name="customer">CustomerLogin obect.</param>
        /// <returns></returns>
        public CustomerAPIResponses CustomerLogin(CustomerLogin customer)
        {
            int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, _dbcontext);
            CustomerAPIResponses inValidUser = ValidateUserlogin(customer, languageId);
            if (inValidUser == null)
            {
                CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
                var result = _userRepository.CustomerLogin(customer);
                if (result.ValidData)
                {
                    UpdateDeviceToken(result.UserID, customer.DeviceToken,customer.DeviceType);
                    CustomerSignUpResponseModel customerSignUpResponse = new CustomerSignUpResponseModel();
                    customerSignUpResponse.DeviceToken = customer.DeviceToken;
                    customerSignUpResponse.Email = result.Email;
                    customerSignUpResponse.FirstName = result.FirstName;
                    customerSignUpResponse.IsFCMToken = (customer.DeviceToken != "" || customer.DeviceToken != null) ? true : false;
                    customerSignUpResponse.LastName = result.LastName;
                    customerSignUpResponse.MobileNo = result.MobileNo;
                    customerSignUpResponse.UserID = result.UserID;
                    customerSignUpResponse.UserStatus = result.UserStatus;
                    customerAPIResponses.Status = true;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                    customerAPIResponses.ResponseObj = customerSignUpResponse;
                    return customerAPIResponses;
                }
                else
                {
                    if (string.IsNullOrEmpty(result.ResultData))
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.InternalServerError", languageId, _dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.ResponseObj = null;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
                        return customerAPIResponses;
                    }
                    else
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = result.ResultData;
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Ambiguous;
                        customerAPIResponses.ResponseObj = null;
                        if (result.ResultData == "Invalid Authentication key")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        }
                        if (result.ResultData == "Invalid Username.")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.InvalidUsername", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                        }
                        if (result.ResultData == "Invalid Phone.")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.UserWithMobileNotRegisteredYet.", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                        }
                        if (result.ResultData == "Password is incorrect")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.PasswordIsIncorrect", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                        }
                        return customerAPIResponses;
                    }
                }
            }
            else
            {
                return inValidUser;
            }
        }

        /// <summary>
        /// CustomerLoginV2_2
        /// </summary>
        /// <param name="customer">CustomerLogin obect.</param>
        /// <returns></returns>
        public CustomerAPIResponses CustomerLoginV2_2(CustomerLogin customer)
        {
            int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, _dbcontext);
            CustomerAPIResponses inValidUser = ValidateUserlogin(customer, languageId);
            if (inValidUser == null)
            {
                CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
                var result = _userRepository.CustomerLoginV2_2(customer);
                if (result.ValidData)
                {
                    UpdateDeviceToken(result.UserID, customer.DeviceToken, customer.DeviceType);
                    CustomerSignUpResponseModel customerSignUpResponse = new CustomerSignUpResponseModel();
                    customerSignUpResponse.DeviceToken = customer.DeviceToken;
                    customerSignUpResponse.Email = result.Email;
                    customerSignUpResponse.FirstName = result.FirstName;
                    customerSignUpResponse.IsFCMToken = (customer.DeviceToken != "" || customer.DeviceToken != null) ? true : false;
                    customerSignUpResponse.LastName = result.LastName;
                    customerSignUpResponse.MobileNo = result.MobileNo;
                    customerSignUpResponse.UserID = result.UserID;
                    customerSignUpResponse.UserStatus = result.UserStatus;
                    customerAPIResponses.Status = true;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                    customerAPIResponses.ResponseObj = customerSignUpResponse;
                    return customerAPIResponses;
                }
                else
                {
                    if (string.IsNullOrEmpty(result.ResultData))
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.InternalServerError", languageId, _dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.ResponseObj = null;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
                        return customerAPIResponses;
                    }
                    else
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = result.ResultData;
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Ambiguous;
                        customerAPIResponses.ResponseObj = null;
                        if (result.ResultData == "Invalid Authentication key")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        }
                        if (result.ResultData == "Invalid Username.")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.InvalidUsername", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                        }
                        if (result.ResultData == "Invalid Phone.")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.UserWithMobileNotRegisteredYet.", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                        }
                        if (result.ResultData == "Password is incorrect")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.PasswordIsIncorrect", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                        }
                        return customerAPIResponses;
                    }
                }
            }
            else
            {
                return inValidUser;
            }
        }

        /// <summary>
        /// CustomerLogin.
        /// </summary>
        /// <param name="customer">CustomerLogin obect.</param>
        /// <returns></returns>
        public CustomerAPIResponses CustomerLoginV2_1(CustomerLoginV2_1 customer)
        {
            int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, _dbcontext);
            CustomerAPIResponses inValidUser = ValidateUserloginV2_1(customer, languageId);
            if (inValidUser == null)
            {
                CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
                var result = _userRepository.CustomerLoginV2_1(customer);
                if (result.ValidData)
                {
                    UpdateDeviceToken(result.UserID, customer.DeviceToken,customer.DeviceType);
                    CustomerSignUpResponseModel customerSignUpResponse = new CustomerSignUpResponseModel();
                    customerSignUpResponse.DeviceToken = customer.DeviceToken;
                    customerSignUpResponse.Email = result.Email;
                    customerSignUpResponse.FirstName = result.FirstName;
                    customerSignUpResponse.IsFCMToken = (customer.DeviceToken != "" || customer.DeviceToken != null) ? true : false;
                    customerSignUpResponse.LastName = result.LastName;
                    customerSignUpResponse.MobileNo = result.MobileNo;
                    customerSignUpResponse.UserID = result.UserID;
                    customerSignUpResponse.UserStatus = result.UserStatus;
                    customerAPIResponses.Status = true;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                    customerAPIResponses.ResponseObj = customerSignUpResponse;
                    return customerAPIResponses;
                }
                else
                {
                    if (string.IsNullOrEmpty(result.ResultData))
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.InternalServerError", languageId, _dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.ResponseObj = null;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
                        return customerAPIResponses;
                    }
                    else
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = result.ResultData;
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Ambiguous;
                        customerAPIResponses.ResponseObj = null;
                        if (result.ResultData == "Invalid Authentication key")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        }
                        if (result.ResultData == "Invalid Username.")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.InvalidUsername", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                        }
                        if (result.ResultData == "Invalid Phone.")
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.UserWithMobileNotRegisteredYet.", languageId, _dbcontext);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                        }
                        return customerAPIResponses;
                    }
                }
            }
            else
            {
                return inValidUser;
            }
        }


        /// <summary>
        /// CustomerLogin.
        /// </summary>
        /// <param name="customer">CustomerLogin obect.</param>
        /// <returns></returns>
        public CustomerLoginResponseModelV3 CustomerLoginV3(CustomerLoginV3 customer)
        {
            int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, _dbcontext);
            CustomerAPIResponses inValidUser = ValidateUserloginV3(customer, languageId);
            if (inValidUser == null)
            {                   
                CustomerLoginResponseModelV3 customerSignUpResponse = new CustomerLoginResponseModelV3();
                Customer customerObj = new Customer();
                var custEmail = customer.Email;
                var otp = string.Empty;
                customer.Email = string.IsNullOrEmpty(customer.Email) ? Helper.GetCustomEmail(customer.StoreId, customer.Phone, _dbcontext) : customer.Email;
                var result = _userRepository.CustomerLoginV3(customer);
                var store = _dbcontext.Store.AsEnumerable().Where(x => x.Id == customer.StoreId).FirstOrDefault();
              //  bool isGuestId = false;
                if (result.ValidData)
                {
                    int userid = Convert.ToInt32(result.ResultData);
                    UpdateDeviceToken(userid, customer.DeviceToken, customer.DeviceType);
                    customerSignUpResponse.UserID = userid;
                    var seconds = _dbcontext.Setting.Where(x => x.Name.Contains("adminareasettings.loginotptimer") && x.StoreId == customer.StoreId).FirstOrDefault()?.Value ?? "60";
                    if (!string.IsNullOrEmpty(custEmail))
                    {
                        otp = Helper.SendEmailOtp(userid, customer.StoreId, customer.Email,seconds, languageId, _dbcontext);
                    }
                    if (string.IsNullOrEmpty(custEmail))
                    {
                        otp = Helper.SendPhoneOtp(userid, customer.StoreId, customer.Phone, seconds,languageId, _dbcontext);
                    }
                }
                customerSignUpResponse.Email = customer.Email;
                customerSignUpResponse.MobileNo = customer.Phone;
                customerSignUpResponse.UserStatus = true;
                customerSignUpResponse.DeviceToken = customer.DeviceToken;
                customerSignUpResponse.OTP = otp;
                customerSignUpResponse.IsFCMToken = (customer.DeviceToken != "" && customer.DeviceToken != null) ? true : false;

                return customerSignUpResponse;
            }
            else
            {
                return null;
            }
        }


        private void UpdateDeviceToken(int customerId, string deviceToken, string deviceType)
        {
            if (!string.IsNullOrEmpty(deviceToken))
            {
                var customerDetails = _dbcontext.CustomerDetails.Where(x => x.CustomerId == customerId).OrderByDescending(x=>x.Id).FirstOrDefault();
                if (customerDetails != null)
                {
                    customerDetails.DeviceToken = deviceToken;
                    _dbcontext.Entry(customerDetails).State = EntityState.Modified;
                    _dbcontext.SaveChanges();
                }
                else 
                {
                    var customerDetail = new Models.CustomerDetails();
                    customerDetail.CrDt = DateTime.UtcNow;
                    customerDetail.CustomerId = customerId;
                    customerDetail.DeviceToken = deviceToken;
                    customerDetail.Otpfailed = 0;
                    customerDetail.OtpresendCount = 0;
                    int _min = 1000;
                    int _max = 9999;
                    Random _rdm = new Random();
                    customerDetail.Otp = _rdm.Next(_min, _max).ToString();
                    customerDetail.CrDt = DateTime.UtcNow;
                    customerDetail.DeviceType = string.IsNullOrEmpty(deviceType) ? "A" : deviceType;
                    _dbcontext.CustomerDetails.Add(customerDetail);
                    _dbcontext.SaveChanges();
                }
            }
        }

        private double GetRating(string rating)
        {
            if (string.IsNullOrEmpty(rating))
            {
                return 0.00;
            }
            else
            {
                var arrurl = rating.Split("::::");
                if (arrurl.Length > 0 && arrurl.Length > 1)
                {
                    return Convert.ToDouble(arrurl[1]);
                }
                else
                {
                    return 0.00;
                }
            }

        }
        private Int16 GetRatingCount(string rating)
        {
            if (string.IsNullOrEmpty(rating))
            {
                return 0;
            }
            else
            {
                var arrurl = rating.Split("::::");
                if (arrurl.Length > 0 && arrurl.Length > 1)
                {
                    return Convert.ToInt16(arrurl[2]);
                }
                else
                {
                    return 0;
                }
            }

        }
        private int GetReviewCount(string rating)
        {
            if (string.IsNullOrEmpty(rating))
            {
                return 0;
            }
            else
            {
                var arrurl = rating.Split("::::");
                if (arrurl.Length > 0 && arrurl.Length > 1)
                {
                    return Convert.ToInt16(arrurl[0]);
                }
                else
                {
                    return 0;
                }
            }

        }
        private List<CustomerImage> GetCustomerImages(string customerURL)
        {
            if (string.IsNullOrEmpty(customerURL))
            {
                return new List<CustomerImage>().Where(x => x.Image == "1").ToList();
            }
            else
            {
                var arrurl = customerURL.Split("::::");
                if (arrurl.Length > 0)
                {
                    List<CustomerImage> listUrl = new List<CustomerImage>();
                    foreach (string url in arrurl)
                    {
                        if (!string.IsNullOrEmpty(url))
                        {
                            listUrl.Add(new CustomerImage { Image = url });
                        }
                    }
                    return listUrl;
                }
                else
                {
                    return null;
                }
            }

        }
        private List<CooknChefProductsImagesV2> GetProductImages(string URL, string ChefBaseUrl, int? PictureSize)
        {
            int pictureSize = PictureSize.HasValue ? PictureSize.Value : 75;
            if (string.IsNullOrEmpty(URL))
            {
                return new List<CooknChefProductsImagesV2>() { new CooknChefProductsImagesV2 { ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now } };
            }
            else
            {
                var arrurl = URL.Split("::::");
                if (arrurl.Length > 0)
                {
                    List<CooknChefProductsImagesV2> listUrl = new List<CooknChefProductsImagesV2>();
                    foreach (string url in arrurl)
                    {
                        if (!string.IsNullOrEmpty(url))
                        {
                            var arrurl2 = url.Split(";;;;");
                            if (arrurl2.Length > 0 && arrurl2.Length >= 2)
                            {
                                var pref = new CooknChefProductsImagesV2
                                {
                                    FileSeo = arrurl2[2],
                                    MimeType = arrurl2[1],
                                    ProductImageURL = arrurl2[0]
                                };
                                if (!string.IsNullOrEmpty(pref.ProductImageURL))
                                {
                                    int pictureId = Convert.ToInt32(pref.ProductImageURL);
                                    if (pictureId != 0)
                                    {

                                        string lastPart = Helper.GetFileExtensionFromMimeType(pref.MimeType);
                                        string thumbFileName = !string.IsNullOrEmpty(pref.FileSeo)
                                        ? $"{pictureId:0000000}_{pref.FileSeo}.{lastPart}" + "? " + DateTime.Now
                                        : $"{pictureId:0000000}_{pictureSize}.{lastPart}" + "? " + DateTime.Now;

                                        pref.ProductImageURL = ChefBaseUrl + thumbFileName + "?" + DateTime.Now;
                                    }
                                    else
                                    {
                                        pref.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                                    }
                                }
                                else
                                {
                                    pref.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                                }
                                listUrl.Add(pref);
                            }

                        }
                    }
                    return listUrl;
                }
                else
                {
                    return new List<CooknChefProductsImagesV2>() { new CooknChefProductsImagesV2 { ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now } };
                }
            }

        }
        private List<PreferencesV2> GetPreferences(string URL, string ChefBaseUrl, int? PictureSize)
        {
            if (string.IsNullOrEmpty(URL))
            {
                return new List<PreferencesV2>().Where(x => x.Image == "1").ToList();
            }
            else
            {
                var arrurl = URL.Split("::::");
                if (arrurl.Length > 0)
                {
                    int pictureSize = PictureSize.HasValue ? PictureSize.Value : 75;
                    List<PreferencesV2> listUrl = new List<PreferencesV2>();
                    foreach (string url in arrurl)
                    {
                        if (!string.IsNullOrEmpty(url))
                        {
                            var arrurl2 = url.Split(";;;;");
                            if (arrurl2.Length > 0 && arrurl2.Length >= 2)
                            {
                                var pref = new PreferencesV2
                                {

                                    Name = arrurl2[3],
                                    ImageType = arrurl2[1],
                                    Image = arrurl2[0]
                                };
                                int pictureId = Convert.ToInt32(pref.Image);
                                string lastPart = Helper.GetFileExtensionFromMimeType(pref.ImageType);
                                var prefdetail = arrurl2[2];
                                var fileName = !string.IsNullOrEmpty(prefdetail)
                                            ? $"{pictureId:0000000}_{prefdetail}_{pictureSize}.{lastPart}" + "? " + DateTime.Now
                                            : $"{pictureId:0000000}_{pictureSize}.{lastPart}" + "? " + DateTime.Now;
                                pref.Image = ChefBaseUrl + fileName;
                                listUrl.Add(pref);
                            }

                        }
                    }
                    return listUrl;
                }
                else
                {
                    return new List<PreferencesV2>().Where(x => x.Image == "1").ToList();
                }
            }

        }
        private List<CooknRestProductsV3> GetSubCategories(int catId, List<RestProductModel> resultproducts, List<ProductAttributesV2> attributes)
        {
            List<CooknRestProductsV2> products = new List<CooknRestProductsV2>();
            var res = resultproducts.Where(x => x.ParentCategoryId == catId)?.
                 Select(x => new CooknRestProductsV3 { CategoryId = x.CategoryId, CategoryName = x.CategoryName, IsMagicMenu = x.IsMagicMenu, CategoryDescription=x.CategoryDescription, CategoryImage=x.CategoryPictureUrl,  CategoryItems = GetCategoryItems(x.CategoryId, resultproducts, attributes) })?.ToList();
            return res;
        }
        private List<CooknRestProductProductAttributesResponse> GetProductProductAttributes(int pid, List<ProductAttributesV2> attributes)
        {
            List<CooknRestProductProductAttributesResponse> res = new List<CooknRestProductProductAttributesResponse>();
            var prodAttributes = attributes.Where(x => x.ProductId == pid).ToList();
            var result = prodAttributes.Select(x => new CooknRestProductProductAttributesResponse
            {
                AttributeName = x.AttributeName,
                IsRequired = x.IsRequired,
                AttributeTypeId = x.AttributeTypeId,
                ProductId = x.ProductId,
                ProductAttributeId = x.ProductAttributeId,
                ProductAttributeMappingId = x.ProductAttributeMappingId
            }).Distinct();
            res = result.Select(x => new CooknRestProductProductAttributesResponse
            {
                AttributeName = x.AttributeName,
                IsRequired = x.IsRequired,
                AttributeTypeId = x.AttributeTypeId,
                ProductId = x.ProductId,
                ProductAttributeId = x.ProductAttributeId,
                ProductAttributeMappingId = x.ProductAttributeMappingId,
                ProductAttribute = prodAttributes.Where(y => y.ProductAttributeId == x.ProductAttributeId && y.ProductAttributeMappingId == x.ProductAttributeMappingId).Select(y => new ProductAttributes
                {
                    Name = y.Name,
                    Price = y.Price,
                    ProductAttributeValueId = y.ProductAttributeValueId,
                    IsPreSelected = y.IsPreSelected,
                    UsePercentage = y.UsePercentage,
                    Currency = this.CurrencySymbol
                }).ToList()
            }).ToList();
            return res;
        }
        private List<CooknChefCatProductsV2> GetCategoryItems(int catId, List<RestProductModel> resultproducts, List<ProductAttributesV2> attributes)
        {
            List<CooknRestProductProductAttributesResponse> res = new List<CooknRestProductProductAttributesResponse>();
            var nullattr = res.Where(x => x.IsRequired).ToList();
            return resultproducts.Where(x => x.CategoryId == catId)?.
              Select(x => new CooknChefCatProductsV2
              {
                  Available = x.Available,
                  Cuisine = !string.IsNullOrEmpty(x.Cuisine) ? x.Cuisine : "",
                  CustomerImages = GetCustomerImages(x.CustomeURL),
                  Description = !string.IsNullOrEmpty(x.ShortDescription) ? x.ShortDescription : "",
                  ImageUrl = GetProductImages(x.ProductPics, x.ChefBaseUrl, x.PictureSize),
                  IsProductAttributesExist = x.IsProductAttributesExist,
                  ItemId = x.ItemId,
                  ItemName = x.itemName,
                  Preferences = GetPreferences(x.ProductPreferences, x.ChefBaseUrl, x.PictureSize),
                  Price = x.ProductPrice.HasValue ? x.ProductPrice.Value.ToString() : "0:00",
                  OldPrice=x.OldPrice,
                  ProductProductAttributes = x.IsProductAttributesExist ? GetProductProductAttributes(x.ItemId, attributes) : nullattr,
                  Quantity = x.Quantity.HasValue ? x.Quantity.Value : 0,
                  IsWishList = x.IsWishList,
                  WishListId = x.WishListId.HasValue ? x.WishListId.Value : 0,
                  Rating = GetRating(x.Rating),
                  RatingCount = GetRatingCount(x.Rating),
                  ReviewCount = GetReviewCount(x.Rating),
              })?.
              OrderBy(x => x.ItemId)?.ToList();
            //return products.Select(x=>x.ParentCategoryId==catId);
        }
        /// <summary>
        /// GetCooknRestItemsByRestnCookId.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public CustomerAPIResponses GetCooknRestItemsByRestnCookId(CooknRestProductByID customer)
        {
            int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, _dbcontext);
            CustomerAPIResponses inValidUser = ValidateRestCustomer(customer, languageId);
            if (inValidUser == null)
            {
                CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
                var result = _userRepository.GetCooknRestItemsByRestnCookId(customer);
                if (result.ValidData && result.Products.Count > 0)
                {
                    string currencyCode = result.Products.FirstOrDefault().CurrencyCode;
                    if (string.IsNullOrEmpty(currencyCode))
                    {
                        currencyCode = "USD";
                    }
                    this.CurrencySymbol = Helper.GetCurrencySymbolFromCode(currencyCode);
                    List<CooknRestProductsV2> products = new List<CooknRestProductsV2>();
                    List<ProductAttributesV2> ProductProductAttributes = new List<ProductAttributesV2>();
                    if (result.Products.Any(x => x.IsProductAttributesExist))
                    {
                        ProductProductAttributes = _userRepository.GetProductProductAttributes(result.Products.Where(x => x.IsProductAttributesExist).Select(x => x.ItemId).ToList());
                    }
                   
                    products = result.Products.Where(x => x.ParentCategoryId == 0).
                        Select(x => new CooknRestProductsV2 { CategoryId = x.CategoryId, CategoryName = x.CategoryName, IsMagicMenu = x.IsMagicMenu }).
                        OrderBy(x => x.CategoryId).
                        Distinct().ToList().Select(x => new CooknRestProductsV2 { CategoryId = x.CategoryId, CategoryName = x.CategoryName, IsMagicMenu = x.IsMagicMenu, Subcategories = GetSubCategories(x.CategoryId, result.Products, ProductProductAttributes), CategoryItems = GetCategoryItems(x.CategoryId, result.Products, ProductProductAttributes) }).ToList();
                   
                    customerAPIResponses.Status = true;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                    customerAPIResponses.ResponseObj = products;
                }
                else
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = result.ResultData;
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    if (result.ResultData == "Invalid Authentication key")
                    {
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, _dbcontext);
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                    }
                    if (result.ValidData)
                    {
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaageChefNotProvidingAnyProductForSellYet", languageId, _dbcontext);
                    }
                }
                return customerAPIResponses;
            }
            else
            {
                return inValidUser;
            }
        }
    }
}
