﻿namespace Nestorbird.Plugin.Api.RequestModel.Account
{
    public class ProfileUpdateRequest
    {
        public ProfileUpdateRequest()
        {
            RemoveImage = false;
        }

        public int CustomerId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string ImageUrl { get; set; }

        public string ImageBase64 { get; set; }

        public string ImageName { get; set; }

        public bool RemoveImage { get; set; }
    }
}
