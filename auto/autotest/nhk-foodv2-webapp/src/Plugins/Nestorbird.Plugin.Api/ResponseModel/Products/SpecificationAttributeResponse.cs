﻿using System.Collections.Generic;

namespace Nestorbird.Plugin.Api.ResponseModel.Products
{
    public class SpecificationAttributeResponse
    {
        public SpecificationAttributeResponse()
        {
            SpecificationAttributeValues = new List<SpecificationAttributeValueResponse>();
        }


        public int Id { get; set; }

        public string Name { get; set; }

        public int SpecificationAttributeId { get; set; }

        public IList<SpecificationAttributeValueResponse> SpecificationAttributeValues { get; set; }
    }

    public class SpecificationAttributeValueResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
