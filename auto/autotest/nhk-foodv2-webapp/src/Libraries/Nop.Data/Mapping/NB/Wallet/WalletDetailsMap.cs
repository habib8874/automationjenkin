﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB;
using Nop.Core.Domain.Orders;

namespace Nop.Data.Mapping.NB
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class WalletDetailsMap : NopEntityTypeConfiguration<WalletDetails>
    {
        public override void Configure(EntityTypeBuilder<WalletDetails> builder)
        {
            builder.ToTable("CustomerWallet");
            builder.HasKey(sci => sci.Id);
            builder.Property(w => w.Amount).HasColumnType("decimal(18, 4)");
            builder.Property(w => w.CustomerId).IsRequired();
            base.Configure(builder);
        }
    }
}
