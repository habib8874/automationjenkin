﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.NB.Catalog.QRCode;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.NB.Catalog;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Models.NB;
using Nop.Web.Framework.Models.Extensions;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Services.Common;
using Nop.Services.Logging;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class NBQRCodeController : BaseAdminController
    {

        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly INotificationService _notificationService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly IWorkContext _workContext;
        private readonly IQRCodeService _qRCodeService;
        private readonly IPictureService _pictureService;
        private readonly IPdfService _pdfService;
        private readonly ILogger _logger;

        #endregion

        #region Ctor

        public NBQRCodeController(ILocalizationService localizationService,
            IPermissionService permissionService,
            INotificationService notificationService,
            IBaseAdminModelFactory baseAdminModelFactory,
            IWorkContext workContext,
            IQRCodeService qRCodeService,
            IPictureService pictureService,
            IPdfService pdfService,
            ILogger logger)
        {
            _localizationService = localizationService;
            _permissionService = permissionService;
            _notificationService = notificationService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _workContext = workContext;
            _qRCodeService = qRCodeService;
            _pictureService = pictureService;
            _pdfService = pdfService;
            _logger = logger;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Functionality to prepare QR code model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual QRCodeModel PrepareQRCodeModel(QRCodeModel model)
        {
            //prepare available vendors
            _baseAdminModelFactory.PrepareVendors(model.AvailableVendor, false);

            return model;
        }

        private static Byte[] BitmapToBytes(Bitmap img)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        public virtual int GetQRCodePictureId(string text)
        {
            try
            {
                if (string.IsNullOrEmpty(text))
                    return 0;

                QRCoder.QRCodeGenerator qrGenerator = new QRCoder.QRCodeGenerator();
                QRCoder.QRCodeData qrCodeData = qrGenerator.CreateQrCode(text,
                QRCoder.QRCodeGenerator.ECCLevel.Q);
                QRCoder.QRCode qrCode = new QRCoder.QRCode(qrCodeData);
                Bitmap qrCodeImage = qrCode.GetGraphic(20);
                var qrCodeBitMap = BitmapToBytes(qrCodeImage);

                var picture = _pictureService.InsertPicture(qrCodeBitMap, MimeTypes.ImagePng, "qrcode");
                return picture == null ? 0 : picture.Id;
            }
            catch (Exception exc)
            {
                _logger.Error(exc.Message, exc);
                return 0;
            }
        }

        #endregion Utilities

        #region Methods

        #region QRCode list / create / edit / delete

        /// <summary>
        /// Functionality to prepare QR code list search model
        /// </summary>
        /// <returns></returns>
        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageQRCode))
                return AccessDeniedView();

            var searchModel = new QRCodeSearchModel();

            //prepare available vendors
            _baseAdminModelFactory.PrepareVendors(searchModel.AvailableVendors);

            //prepare grid
            searchModel.SetGridPageSize();

            return View(searchModel);
        }

        /// <summary>
        /// Functionality to prepare QR code list model
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual IActionResult QRCodeList(QRCodeSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageQRCode))
                return AccessDeniedDataTablesJson();

            //get parameters to filter QR Code
            if (_workContext.CurrentVendor != null)
                searchModel.SearchVendorId = _workContext.CurrentVendor.Id;

            //get QRCode
            var qRCodes = _qRCodeService.GetAllQRCode(vendorId: searchModel.SearchVendorId,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare list model
            var model = new QRCodeListModel().PrepareToGrid(searchModel, qRCodes, () =>
            {
                return qRCodes.Select(qRCode =>
                {
                    //fill in model values from the entity
                    var qRCodeModel = new QRCodeModel
                    {
                        Id = qRCode.Id,
                        VendorId = qRCode.MerchantId,
                        VendorName = qRCode.Vendor?.Name
                    };

                    return qRCodeModel;
                });
            });

            return Json(model);
        }

        /// <summary>
        /// Functionality to create QR code
        /// </summary>
        /// <returns></returns>
        public virtual IActionResult CreateQRCode()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageQRCode))
                return AccessDeniedView();

            //prepare model
            var model = PrepareQRCodeModel(new QRCodeModel());

            return View(model);
        }

        /// <summary>
        /// Functionality to create QR code
        /// </summary>
        /// <param name="model"></param>
        /// <param name="continueEditing"></param>
        /// <returns></returns>
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult CreateQRCode(QRCodeModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageQRCode))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var qRCode = new QRCode
                {
                    MerchantId = model.VendorId,
                    LogoId = model.QRCodeLogoId,
                    ResturantContent = model.ResturantContent,
                    WebsiteURL = model.WebsiteURL
                };
                qRCode.QRCodePictureId = GetQRCodePictureId(qRCode.WebsiteURL);

                _qRCodeService.InsertQRCode(qRCode);

                _notificationService.SuccessNotification(_localizationService.GetResource("NB.Admin.QRCode.Added.Successfully"));
                if (!continueEditing)
                {
                    return RedirectToAction("List");
                }

                return RedirectToAction("EditQRCode", new { id = qRCode.Id });
            }

            //prepare model
            model = PrepareQRCodeModel(model);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        /// <summary>
        /// Functionality to edit QR code
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual IActionResult EditQRCode(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageQRCode))
                return AccessDeniedView();

            //try to get a product with the specified id
            var qRCode = _qRCodeService.GetQRCodeById(id);
            if (qRCode == null)
                return RedirectToAction("List");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && qRCode.MerchantId != _workContext.CurrentVendor.Id)
                return RedirectToAction("List");

            var model = new QRCodeModel
            {
                Id = qRCode.Id,
                VendorId = qRCode.MerchantId,
                QRCodeLogoId = qRCode.LogoId,
                ResturantContent = qRCode.ResturantContent,
                WebsiteURL = qRCode.WebsiteURL
            };

            //prepare model
            model = PrepareQRCodeModel(model);

            return View(model);
        }

        /// <summary>
        /// Functionality to edit QR code
        /// </summary>
        /// <param name="model"></param>
        /// <param name="continueEditing"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult EditQRCode(QRCodeModel model, bool continueEditing, IFormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageQRCode))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var qRCode = _qRCodeService.GetQRCodeById(model.Id);

                if (qRCode == null)
                {
                    throw new Exception("No qRCode found with the specified id");
                }

                qRCode.LogoId = model.QRCodeLogoId;
                qRCode.MerchantId = model.VendorId;
                qRCode.ResturantContent = model.ResturantContent;
                qRCode.WebsiteURL = model.WebsiteURL;
                qRCode.QRCodePictureId = GetQRCodePictureId(model.WebsiteURL);
                _qRCodeService.UpdateQRCode(qRCode);

                _notificationService.SuccessNotification(_localizationService.GetResource("NB.Admin.QRCode.Updated.Successfully"));
                if (!continueEditing)
                {
                    return RedirectToAction("List");
                }
                return RedirectToAction("EditQRCode", new { id = model.Id });
            }

            //prepare model
            model = PrepareQRCodeModel(model);

            return RedirectToAction("EditQRCode", new { id = model.Id });
        }

        /// <summary>
        /// Functionality to delete the QRCode
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageQRCode))
                return AccessDeniedView();

            var qRCode = _qRCodeService.GetQRCodeById(id);
            if (qRCode.Id != 0)
                _qRCodeService.DeleteQRCode(qRCode);

            _notificationService.SuccessNotification(_localizationService.GetResource("NB.Admin.QRCode.Deleted.Successfully"));

            return RedirectToAction("List");
        }

        /// <summary>
        /// Functionality to delete the selected QRCodes
        /// </summary>
        /// <param name="selectedIds"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual IActionResult DeleteSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageQRCode))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                var qRCodes = _qRCodeService.GetQRCodeByIds(selectedIds.ToArray());
                foreach (var qRCode in qRCodes)
                {
                    _qRCodeService.DeleteQRCode(qRCode);
                }
            }

            return Json(new { Result = true });
        }

        /// <summary>
        /// Functionality to create PDF File of QR code
        /// </summary>
        /// <param name="qrCodeId"></param>
        /// <returns></returns>
        public virtual IActionResult CreatePDFFile(int qrCodeId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageQRCode))
                return AccessDeniedView();

            var qrCode = _qRCodeService.GetQRCodeById(qrCodeId)
                ?? throw new ArgumentException("No QR Code found with the specified id");

            var qRcodePicture = _pictureService.GetPictureById(qrCode.QRCodePictureId);
            var qRCodeLogo = _pictureService.GetPictureById(qrCode.LogoId);
            var model = new QRCodeModel
            {
                VendorName = qrCode.Vendor.Name,
                WebsiteURL = qrCode.WebsiteURL,
                ResturantContent = qrCode.ResturantContent,
                QRCodeURL = _pictureService.GetPictureUrl(qRcodePicture, 0, true),
                LogoURL = _pictureService.GetPictureUrl(qRCodeLogo, 0, true)
            };

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintQRCodePdf(RenderPartialViewToString("QRCodePDF", model), stream);
                bytes = stream.ToArray();
                stream.Close();
            }
            var fileName = $"{_localizationService.GetResource("NB.Admin.QRCode.Pdf")}_{DateTime.Now:yyyy-MM-dd-HH-mm-ss}.pdf";
            return File(bytes, MimeTypes.ApplicationPdf, fileName);
        }

        #endregion

        #endregion

    }
}