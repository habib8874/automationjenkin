﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB
{
    /// <summary>
    /// Represents a DeliverySlotSearc search model
    /// </summary>
    public partial class DeliverySlotSearchModel : BaseSearchModel
    {
        #region Properties
        public int Id { get; set; }
        public string Name { get; set; }
        public int Duration { get; set; }
        public int StoreId { get; set; }
        #endregion
    }
}