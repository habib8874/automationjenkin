﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NHKCustomerApplication.Caching;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Services;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModel;
using NHKCustomerApplication.ViewModels;
using System;
using System.Data;
using System.Linq;
using System.Net;

namespace NHKCustomerApplication.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class BannerController : ControllerBase
	{
		#region "Fields"

		private ecuadordevContext dbcontext;
		private readonly IConfiguration _configuration;
		private readonly ICommonService _commonService;
		private readonly ICacheManager _cacheManager;

		#endregion

		#region 

		/// <summary>
		/// Get vendor slider pictures
		/// </summary>
		/// <param name="vendorId">Vendor identifier</param>
		/// <param name="storeId">Store identifier</param>
		/// <returns></returns>
		public BannerController(ICommonService commonService, ICacheManager cacheManager, ecuadordevContext context, IConfiguration configuration)
		{
			dbcontext = context;
			_configuration = configuration;
			this._commonService = commonService;
			this._cacheManager = cacheManager;
		}
		#endregion

		/// <summary>
		/// Get vendor slider pictures
		/// </summary>
		/// <param name="requestModel"></param>
		/// <returns></returns>
		[Route("~/api/v1/GetVendorSliderPictures")]
		[HttpPost]

		public BaseAPIResponseModel GetVendorSliderPictures([FromBody] VendorSliderPictureReqModel requestModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(requestModel.StoreId, requestModel.UniqueSeoCode, dbcontext);
			try
			{
				if (requestModel == null)
					return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext));
				if (string.IsNullOrEmpty(requestModel.ApiKey))
					return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext));
				if (requestModel.StoreId == 0)
					return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext));
				if (requestModel.RestncookId == 0)
					return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext));
				if (!string.IsNullOrEmpty(requestModel.ApiKey) || !string.IsNullOrWhiteSpace(requestModel.ApiKey))
				{
					var keyExist = Helper.AuthenticateKey(requestModel.ApiKey, requestModel.StoreId, dbcontext, _cacheManager);
					if (keyExist == null)
						return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext));
				}
				var key = string.Format(CacheKeys.VendorBannerListKey, requestModel.StoreId,requestModel.RestncookId,  languageId);
				var data = _cacheManager.Get(key, () => {
					
					return _commonService.GetVendorSliderPictures(requestModel.RestncookId, requestModel.StoreId);
				});
				if(data.Count<=0)
					return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMessage.NoBannerFound", languageId, dbcontext));
				return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMessage.BannerFound", languageId, dbcontext), data);


			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				return BaseMethodsHelper.ErrorResponse(ex.Message, HttpStatusCode.BadRequest);
			}

		}
	}
}
