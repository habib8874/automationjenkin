﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Schedule
{
    public partial class Product_Schedule_Mapping : BaseEntity
    {
        public int ProductId { get; set; }
        public int ScheduleId { get; set; }
        public bool IsFeaturedProduct { get; set; }
        public int DisplayOrder { get; set; }
       
    }
}
