﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Vendors;

namespace Nestorbird.Plugin.Api.EventConsumer
{
    public class OrderPlaceEventConsumer : IConsumer<OrderPlacedEvent>
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IVendorService _vendorService;
        private readonly ICustomerService _customerService;
        private readonly MerchantAPISettings _merchantAPISettings;
        private readonly ILogger _logger;

        #endregion

        #region Ctor

        public OrderPlaceEventConsumer(ILocalizationService localizationService,
            IGenericAttributeService genericAttributeService,
            IVendorService vendorService,
            ICustomerService customerService,
            MerchantAPISettings merchantAPISettings,
            ILogger logger)
        {
            _localizationService = localizationService;
            _genericAttributeService = genericAttributeService;
            _vendorService = vendorService;
            _customerService = customerService;
            _merchantAPISettings = merchantAPISettings;
            _logger = logger;
        }

        #endregion

        public void HandleEvent(OrderPlacedEvent eventMessage)
        {
            try
            {
                var order = eventMessage.Order;
                _logger.Information("Order Place Event : " + order.Id);
                var vendorId = order.OrderItems.Select(x => x.Product.VendorId).FirstOrDefault();
                if (vendorId > 0)
                {
                    var vendor = _vendorService.GetVendorById(vendorId);
                    var customer = _customerService.GetCustomerByVendorId(vendor.Id);
                    var deviceToken = _genericAttributeService.GetAttribute<string>(customer, NestorbirdAPIPlugin.DeviceTokenAttribute, vendor.StoreId);
                    if (!string.IsNullOrEmpty(deviceToken))
                    {
                        var vendorLanguageId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.LanguageIdAttribute, vendor.StoreId);
                        if (vendorLanguageId <= 0)
                            vendorLanguageId = 1;

                        var webKey = _merchantAPISettings.MerchantAppWebKey;
                        var webAddr = "https://fcm.googleapis.com/fcm/send";
                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, webKey);
                        httpWebRequest.Method = "POST";

                        var orderStatus = _localizationService.GetLocalizedEnum(order.OrderStatus, vendorLanguageId);
                        var deliveredTime = string.Empty;
                        var orderId = order.Id;
                        var orderNumber = !string.IsNullOrEmpty(order.CustomOrderNumber) ? order.CustomOrderNumber : order.Id.ToString();
                        var heading = _localizationService.GetResource("NB.Merchant.OrderPlaced.Title");
                        var text = _localizationService.GetResource("NB.Merchant.OrderPlaced.Text");

                        var orderType = "";
                        var orderDetails = order.GetOrderDetails();
                        if (orderDetails != null && orderDetails.OrderType != null)
                        {
                            orderType = (orderDetails.OrderType == 1) ? _localizationService.GetResource("Delivery", vendorLanguageId) : _localizationService.GetResource("Takeaway", vendorLanguageId);
                        }

                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            var strNJson = @"{
                            ""to"": """ + deviceToken + @""",
                            ""data"": {
                                ""Status"": """ + orderStatus + @""",
                                ""DeliveryTime"": """ + deliveredTime + @""",
                                ""OrderId"": """ + orderId + @""",
                                ""OrderNumber"": """ + orderNumber + @""",
                                ""RestType"": """ + orderType + @""",
                            },
                            ""notification"": {
                                ""title"": """ + heading + @""",
                                ""text"": """ + text + @""",
                            }}";

                            streamWriter.Write(strNJson);
                            streamWriter.Flush();
                        }

                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            var result = streamReader.ReadToEnd();
                            _logger.Information("Push notification : " + result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Push notification error", ex);
            }
        }
    }
}
