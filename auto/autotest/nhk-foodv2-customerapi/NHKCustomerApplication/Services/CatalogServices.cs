﻿using NHKCustomerApplication.Repository;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NHKCustomerApplication.Services
{
	public class CatalogServices : ICatalogServices
	{
		private readonly ICatalogRespository _catalogRepository;

		#region "Constructor"
		public CatalogServices(ICatalogRespository catalogRepository/*, ecuadordevContext dbcontext, IConfiguration configuration*/)
		{
			_catalogRepository = catalogRepository;
		}
		#endregion
		#region
		/// <summary>
		/// business logic of getting Business category 
		/// </summary>
		/// <param name="merchantCategorysModelV2,languageId"></param>
		/// <returns></returns>
		public List<MerchantCategoryModel> GetMerchantCategory(MerchantCategorysModelV2 merchantCategorysModelV2, int languageId)
		{
			try
			{
				//Responce list
				var merchantCategoryList = new List<MerchantCategoryModel>();
				//calling sp of Business category listing
				var result2 = _catalogRepository.GetMerchntCategoriesListById(merchantCategorysModelV2);
				if (result2.ValidData && result2.Categoriess.Count > 0)
				{
					merchantCategoryList = result2.Categoriess.Select(x => new MerchantCategoryModel { Id = x.MerchantCategoryId, Name = x.MerchantCatName, ShortDescription = x.Description, PictureId = x.PictureId, DisplayOrder = x.DisplayOrder, StoreId = x.StoreId, Published = x.IsDeleted, Deleted = x.IsDeleted, PictureThumbnailUrl = x.MerchantCategoryPictureUrl, StoreName = x.StoreName })
				  .Distinct().ToList().Select(x => new MerchantCategoryModel { Id = x.Id, Name = x.Name, ShortDescription = x.ShortDescription, PictureId = x.PictureId, StoreId = x.StoreId, Published = x.Published, DisplayOrder = x.DisplayOrder, Deleted = x.Deleted, PictureThumbnailUrl = x.PictureThumbnailUrl, StoreName = x.StoreName }).ToList();
					return merchantCategoryList;
				}
				return null;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				return null;
			}
		}


		/// <summary>
		/// business logic of getting category listing
		/// </summary>
		/// <param name="categorys,languageId"></param>
		/// <returns></returns>
		public List<CooknCategory> GetCategorys(Categorys categorys, int languageId)
		{
			try
			{
				//calling sp of Category listing
				var result = _catalogRepository.GetCategoryByVendor(categorys, languageId);
				//Response list
				var products = new List<CooknCategory>();
				if (result.ValidData && result.catLists.Count > 0)
				{
					products = result.catLists.
					   Select(x => new CooknCategory { CategoryId = x.CategoryId, Name = x.CategoryName, Description = x.CategoryDescription, Published = x.IsPublished, Deleted = x.IsDeleted, ParentCategoryId = x.ParentCategoryId, DisplayOrder = x.DisplayOrder, PictureId = x.PictureId, PictureThumbnailUrl = x.CategoryPictureUrl, TotalRecords = x.TotalRecords }).
					   OrderBy(x => x.CategoryId).
					   Distinct().ToList();
				}
				return products;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				return null;
			}
		}
	}
}
#endregion
