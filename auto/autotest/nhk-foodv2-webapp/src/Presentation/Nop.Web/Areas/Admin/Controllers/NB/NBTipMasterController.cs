﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.NB.Tip;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.NB.TipMaster;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Models.NB;
using Nop.Web.Framework.Models.Extensions;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers.NB
{
    public partial class NBTipMasterController : BaseAdminController
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IProductService _productService;
        private readonly IWorkContext _workContext;
        private readonly ITipMasterService _tipMasterService;
        private readonly IPictureService _pictureService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly IStoreService _storeService;

        #endregion

        #region Ctor

        public NBTipMasterController(ILocalizationService localizationService,
            INotificationService notificationService,
            IPermissionService permissionService,
            IProductService productService,
            IWorkContext workContext,
            ITipMasterService tipMasterService,
            IPictureService pictureService,
            IBaseAdminModelFactory baseAdminModelFactory,
            IStoreService storeService)
        {
            _localizationService = localizationService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _productService = productService;
            _workContext = workContext;
            _tipMasterService = tipMasterService;
            _pictureService = pictureService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _storeService = storeService;
        }

        #endregion

        #region Methods
        #region Tip Master

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgents))
                return AccessDeniedView();

            return View(new TipSearchModel());
        }

        /// <summary>
        /// Functionality to get Tips list
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual IActionResult TipsList(TipSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgents))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = PrepareTipListModel(searchModel);

            return Json(model);
        }

        /// <summary>
        /// Functionality to create a new Tip
        /// </summary>
        /// <returns></returns>
        public virtual IActionResult CreateTip()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgents))
                return AccessDeniedView();

            //prepare model
            var model = new TipModel();
            //prepare available stores
            _baseAdminModelFactory.PrepareStores(model.AvailableStores, false);

            return View(model);
        }

        /// <summary>
        /// Functionality to save a newly created tip
        /// </summary>
        /// <param name="model"></param>
        /// <param name="continueEditing"></param>
        /// <returns></returns>
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult CreateTip(TipModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgents))
                return AccessDeniedView();


            if (!ModelState.IsValid)
            {
                _notificationService.ErrorNotification(_localizationService.GetResource("NB.Admin.TipMaster.Empty.Error"));
                return RedirectToAction("CreateTip");
            }
            var tip = new Tip
            {
                TipAmount = model.TipAmount,
                Deleted = model.Deleted,
                DisplayOrder = model.DisplayOrder,
                StoreId = model.StoreId
            };
            _tipMasterService.InsertTip(tip);


            _notificationService.SuccessNotification(_localizationService.GetResource("NB.Admin.TipMaster.Added.Succesfully"));
            if (!continueEditing)
            {
                return RedirectToAction("List");
            }

            return RedirectToAction("EditTip", new { id = tip.Id });
        }

        /// <summary>
        /// Functionality to edit a tip
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual IActionResult EditTip(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgents))
                return AccessDeniedView();

            var tip = _tipMasterService.GetTipById(id);
            if (tip == null)
            {
                throw new Exception("No tip found with the specified id");
            }

            var model = new TipModel
            {
                Id = tip.Id,
                TipAmount = tip.TipAmount,
                DisplayOrder = tip.DisplayOrder,
                Deleted = tip.Deleted,
                StoreId = tip.StoreId
            };

            //prepare available stores
            _baseAdminModelFactory.PrepareStores(model.AvailableStores, false);

            return View(model);
        }

        /// <summary>
        /// Functionality to edit a tip
        /// </summary>
        /// <param name="model"></param>
        /// <param name="continueEditing"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult EditTip(TipModel model, bool continueEditing, IFormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgents))
                return AccessDeniedView();

            var tip = _tipMasterService.GetTipById(model.Id);

            if (tip == null)
            {
                throw new Exception("No tip found with the specified id");
            }

            if (!ModelState.IsValid)
            {
                _notificationService.ErrorNotification(_localizationService.GetResource("NB.Admin.TipMaster.Empty.Error"));
                return RedirectToAction("EditQuestion", new { id = model.Id });
            }

            tip.TipAmount = model.TipAmount;
            tip.Deleted = model.Deleted;
            tip.DisplayOrder = model.DisplayOrder;
            tip.StoreId = model.StoreId;

            _tipMasterService.UpdateTip(tip);

            _notificationService.SuccessNotification(_localizationService.GetResource("NB.Admin.TipMaster.Updated.Succesfully"));
            if (!continueEditing)
            {
                return RedirectToAction("List");
            }

            return RedirectToAction("EditTip", new { id = model.Id });
        }

        /// <summary>
        /// Functionality to delete the tip
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual IActionResult DeleteTip(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgents))
                return AccessDeniedView();

            var tip = _tipMasterService.GetTipById(id);
            tip.Deleted = true;
            _tipMasterService.UpdateTip(tip);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult DeleteSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgents))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                var tips = _tipMasterService.GetTipsByIds(selectedIds.ToArray());
                foreach (var tip in tips)
                {
                    tip.Deleted = true;
                    _tipMasterService.UpdateTip(tip);
                }
            }

            return Json(new { Result = true });
        }

        /// <summary>
        /// Functionality to prepare tip list model
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        public virtual TipListModel PrepareTipListModel(TipSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get tips
            var tips = _tipMasterService.GetAllTips(_workContext.IsAdminRole ? 0 : _workContext.GetCurrentStoreId).ToPagedList(searchModel);

            var allStores = _storeService.GetAllStores();

            //prepare grid model
            var model = new TipListModel().PrepareToGrid(searchModel, tips, () =>
            {
                return tips.Select(tip =>
                {
                    //fill in model values from the entity
                    var tipsModel = new TipModel
                    {
                        Id = tip.Id,
                        TipAmount = tip.TipAmount,
                        Deleted = tip.Deleted,
                        DisplayOrder = tip.DisplayOrder
                    };

                    var store = allStores.Where(x => x.Id == tip.StoreId).FirstOrDefault();
                    tipsModel.StoreName = store == null ? string.Empty : store.Name;

                    return tipsModel;
                });
            });

            return model;
        }

        #endregion
        #endregion
    }
}