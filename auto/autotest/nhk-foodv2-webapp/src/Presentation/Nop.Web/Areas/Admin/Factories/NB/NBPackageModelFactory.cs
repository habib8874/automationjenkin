﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.NB.Package;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.NB.Package;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.NB.Package;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Web.Areas.Admin.Factories.NB
{
    public partial class NBPackageModelFactory : INBPackageModelFactory
    {

        #region Fields

        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly IProductService _productService;
        private readonly IWorkContext _workContext;
        private readonly INBPackageService _nBPackageService;
        private readonly ISettingService _settingService;

        #endregion

        #region Ctor

        public NBPackageModelFactory(
            IBaseAdminModelFactory baseAdminModelFactory,
            IProductService productService,
            IWorkContext workContext,
            INBPackageService nBPackageService,
            ISettingService settingService)
        {
            _baseAdminModelFactory = baseAdminModelFactory;
            _productService = productService;
            _workContext = workContext;
            _nBPackageService = nBPackageService;
            _settingService = settingService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare Package search model
        /// </summary>
        /// <param name="searchModel">Package search model</param>
        /// <returns>Package search model</returns>
        public virtual PackageSearchModel PreparePackageSearchModel(PackageSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged Package list model
        /// </summary>
        /// <param name="searchModel">Package search model</param>
        /// <returns>Package list model</returns>
        public virtual PackageListModel PreparePackageListModel(PackageSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));
            if (_workContext.IsStoreOwnerRole)
            {
                searchModel.StoreId = _workContext.GetCurrentStoreId;
            }
            else if (_workContext.SubAdmin.IsSubAdminRole)
            {
                searchModel.StoreId = _workContext.GetCurrentStoreId;
            }

            //get packages
            var packages = _nBPackageService.GetAllPackages(storeId: searchModel.StoreId, "").ToPagedList(searchModel);

            //prepare grid model
            var model = new PackageListModel().PrepareToGrid(searchModel, packages, () =>
            {
                return packages.Select(p =>
                {
                    //fill in model values from the entity
                    var packageModel = new PackageModel();
                    packageModel.Id = p.Id;
                    packageModel.Name = p.Name;
                    packageModel.UnitLabel = p.UnitLabel;
                    return packageModel;
                });
            });

            return model;
        }

        /// <summary>
        /// Prepare package model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="package"></param>
        /// <returns></returns>
        public virtual PackageModel PreparePackageModel(PackageModel model, NBPackage package)
        {
            if (package != null)
            {
                //fill in model values from the entity
                if (model == null)
                {
                    model = package.ToModel<PackageModel>();
                }

                model.PackagePlanModel.PackagePlanSearchModel.SetGridPageSize();
                model.PackagePlanModel.PackageId = model.Id;
                var items = _settingService.GetSettingByKey<string>("PackagePlan.BillingInterval", "Daily,Custom", _workContext.GetCurrentStoreId, true);
                model.PackagePlanModel.AvailableBillingInterval = items.Split(',').Select(x => new SelectListItem { Text = x, Value = x }).ToList();
                //model.SubscriptionCourseSearchModel.SetGridPageSize();
                //model.SubscriptionCourseSearchModel.PackageId = package.Id;
            }

            //prepare model store
            _baseAdminModelFactory.PrepareStores(model.AvailableStores);

            return model;
        }

        /// <summary>
        /// get package plans list
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        public virtual PackagePlanListModel PreparePackagePlanListModel(PackagePlanSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get packages
            var packages = _nBPackageService.GetPackagePlans(searchModel.PackageId).ToPagedList(searchModel);

            //prepare grid model
            var model = new PackagePlanListModel().PrepareToGrid(searchModel, packages, () =>
            {
                return packages.Select(p =>
                {
                    //fill in model values from the entity
                    var packageModel = new PackagePlanModel();
                    packageModel.Id = p.Id;
                    packageModel.PlanName = p.Name;
                    packageModel.Price = p.Price;
                    packageModel.BillingInterval = p.BillingInterval;
                    packageModel.StartDate = p.StartDate;
                    packageModel.EndDate = p.EndDate;
                    packageModel.TrailPeriod = p.TrailPeriod;
                    return packageModel;
                });
            });

            return model;
        }

        #endregion
    }
}
