﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.RatingReview;

namespace Nop.Services.RatingReview
{
    /// <summary>
    /// RatingReviews service interface
    /// </summary>
    public partial interface IRatingReviewService
    {
        /// <summary>
        /// Deletes a ratingReview
        /// </summary>
        /// <param name="ratingReview">RatingReviews</param>
        void DeleteRatingReviews(RatingReviews ratingReview);

        /// <summary>
        /// Gets a ratingReview
        /// </summary>
        /// <param name="ratingReviewId">The ratingReview identifier</param>
        /// <returns>RatingReviews</returns>
        RatingReviews GetRatingReviewsById(int ratingReviewId);

        /// <summary>
        /// Gets all ratingReviews
        /// </summary>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <returns>RatingReviewss</returns>
        IList<RatingReviews> GetAllRatingReviews(int storeId);

        /// <summary>
        /// Inserts a ratingReview
        /// </summary>
        /// <param name="ratingReview">RatingReviews</param>
        void InsertRatingReviews(RatingReviews ratingReview);

        /// <summary>
        /// Updates the ratingReview
        /// </summary>
        /// <param name="ratingReview">RatingReviews</param>
        void UpdateRatingReviews(RatingReviews ratingReview);

        /// <summary>
        /// check whether customer 
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="CustomerId"></param>
        /// <param name="reviewType"></param>
        /// <param name="orderId"></param>
        /// <param name="skipNotInterestedCheck"></param>
        /// <returns></returns>
        int CheckReviewForCustomer(int storeId, int customerId, int reviewType, int orderId, bool skipNotInterestedCheck = false);



        /// <summary>
        /// Gets all rating reviews
        /// </summary>
        /// <param name="customerId">Customer identifier (who wrote a review); 0 to load all records</param>
        /// <param name="approved">A value indicating whether to content is approved; null to load all records</param> 
        /// <param name="fromUtc">Item creation from; null to load all records</param>
        /// <param name="toUtc">Item item creation to; null to load all records</param>
        /// <param name="message">Search title or review text; null to load all records</param>
        /// <param name="storeId">The store identifier; pass 0 to load all records</param>
        /// <param name="productId">The product identifier; pass 0 to load all records</param>
        /// <param name="vendorId">The vendor identifier (limit to products of this vendor); pass 0 to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Reviews</returns>
        IPagedList<RatingReviews> GetAllRatingReviewsWithPaging(int customerId,
            bool? approved,
            DateTime? fromUtc = null,
            DateTime? toUtc = null,
            string message = null,
            int storeId = 0,
            int reviewType = 0,
            int itemId = 0,
            int vendorsId = 0,//Vendor DropDown List
            int vendorId = 0,
            int pageIndex = 0,
            int pageSize = int.MaxValue,
            IList<int> vendorIds = null);

        /// <summary>
        /// Gets products by identifier
        /// </summary>
        /// <param name="productIds">Product identifiers</param>
        /// <returns>Products</returns>
        IList<RatingReviews> GetRatingReviewsByIds(int[] ratingReviewsIds);



        /// <summary>
        /// Delete ratings
        /// </summary>
        /// <param name="ratings">Ratings</param>
        void DeleteRatingReviews(IList<RatingReviews> ratingReviews);

        /// <summary>
        /// Get rating values
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="entityId"></param>
        /// <param name="reviewType"></param>
        /// <returns></returns>
        decimal GetAverageRating(int storeId, int entityId, int reviewType, out int reviewCount);

        /// <summary>
        /// get all ratings of items
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        IList<RatingReviews> GetItemRatings(int storeId, int productId);

        /// <summary>
        /// Get value idicates wether all reviews for this order is done, including Agent, Merchant and for all order items.
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        bool AllReviewFound(int orderId);

        /// <summary>
        /// Get value idicates is rating review enabled and rating found for merchant based on order
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        bool IsMerchantReviewEnabledAndFound(int orderId);

    }
}
