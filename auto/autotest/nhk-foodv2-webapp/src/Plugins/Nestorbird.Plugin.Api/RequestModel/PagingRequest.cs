﻿namespace Nestorbird.Plugin.Api.RequestModel
{
    public class PagingRequest
    {
        public virtual int PageIndex { get; set; } = 0;

        public virtual int PageSize { get; set; } = int.MaxValue;
    }
}
