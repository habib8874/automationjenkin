﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class FilterCategoryValues
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int FilterCategoryId { get; set; }
        public int StoreId { get; set; }

        public virtual FilterCategory FilterCategory { get; set; }
    }
}
