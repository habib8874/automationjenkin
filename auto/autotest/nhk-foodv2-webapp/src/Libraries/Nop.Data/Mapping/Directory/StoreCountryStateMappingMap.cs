﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Directory;

namespace Nop.Data.Mapping.Directory
{
    /// <summary>
    /// Represents a country, store and state mapping configuration
    /// </summary>
    public partial class StoreCountryStateMappingMap : NopEntityTypeConfiguration<StoreCountryStateMapping>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<StoreCountryStateMapping> builder)
        {
            builder.ToTable(nameof(StoreCountryStateMapping));
            builder.HasKey(state => state.Id);

            builder.Property(state => state.CountryName).HasMaxLength(100);
            builder.Property(state => state.StateName).HasMaxLength(100);

            base.Configure(builder);
        }

        #endregion
    }
}