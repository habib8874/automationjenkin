﻿namespace Nop.Services.NB.API
{
    /// <summary>
    /// VersionInfo service interface
    /// </summary>
    public interface IVersionInfoServices
    {
        /// <summary>
        /// value idicates whether provided API key is valid or not
        /// </summary>
        /// <param name="key"></param>
        /// <returns>True/False</returns>
        bool IsValidAPIkey(string key);
    }
}
