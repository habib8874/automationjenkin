﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Areas.Admin.Models.Common;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.Suppliers
{
    public partial class SupplierModel : BaseNopEntityModel, ILocalizedModel<SupplierLocalizedModel>,IAclSupportedModel
    {
        #region Ctor

        public SupplierModel()
        {
            if (PageSize < 1)
                PageSize = 5;
            Address = new AddressModel();
            Locales = new List<SupplierLocalizedModel>();
            SelectedCustomerRoleIds = new List<int>();
            AvailableCustomerRoles = new List<SelectListItem>();
            AvailableStore = new List<SelectListItem>();
            SupplierProductSearchModel = new SupplierProductSearchModel();
        }

        #endregion

        #region Properties
        public bool IsLoggedInAsVendor { get; set; }


        [NopResourceDisplayName("Admin.Supplier.Fields.SupplierName")]
        public string SupplierName { get; set; }
        [NopResourceDisplayName("Admin.Supplier.Fields.Description")]
        public string Description { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Fields.Active")]
        public bool Active { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Fields.AddressId")]
        public int AddressId { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Fields.Deleted")]
        public bool Deleted { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Fields.LocationMap")]
        public string LocationMap { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Fields.Geofancing")]
        public string Geofancing { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Fields.DeliveryFee")]
        public decimal DeliveryFee { get; set; }
       
        [NopResourceDisplayName("Admin.Supplier.Fields.CreatedOnUtc")]
        public string CreatedOnUtc { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Fields.FixedOrDynamic")]
        public string FixedOrDynamic { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }


        [NopResourceDisplayName("Admin.Supplier.Fields.KmOrMilesForDynamic")]
        public string KmOrMilesForDynamic { get; set; }


        [NopResourceDisplayName("Admin.Supplier.Fields.PriceForDynamic")]
        public decimal PriceForDynamic { get; set; }


        [NopResourceDisplayName("Admin.Supplier.Fields.DistanceForDynamic")]
        public decimal DistanceForDynamic { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Fields.PricePerUnitDistanceForDynamic")]
        public decimal PricePerUnitDistanceForDynamic { get; set; }

        public AddressModel Address { get; set; }

        public IList<SupplierLocalizedModel> Locales { get; set; }


        public IList<int> SelectedCustomerRoleIds { get; set; }
        public IList<SelectListItem> AvailableCustomerRoles { get; set; }

        public IList<SelectListItem> AvailableStore { get; set; }
        public int PageSize { get; }

        [NopResourceDisplayName("Admin.Supplier.Fields.IsSupplier")]
        public bool IsSupplier { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Fields.StoreId")]
        public int StoreId { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Fields.Parent")]
        public int ParentSupplierId { get; set; }


        [NopResourceDisplayName("Admin.Supplier.Fields.Published")]
        public bool Published { get; set; }

        public SupplierProductSearchModel SupplierProductSearchModel { get; set; }
    }


    public partial class SupplierLocalizedModel : ILocalizedLocaleModel
    {
        [NopResourceDisplayName("Admin.Supplier.Fields.LanguageId")]
        public int LanguageId { get; set; }
        [NopResourceDisplayName("Admin.Supplier.Fields.SupplierName")]
        public string SupplierName { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Fields.Description")]
        public string Description { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Fields.SupplierID")]
        public int SupplierID { get; set; }
    }
}
#endregion