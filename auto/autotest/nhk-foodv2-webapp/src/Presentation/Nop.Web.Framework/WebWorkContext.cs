﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Vendors;
using Nop.Core.Http;
using Nop.Services.Authentication;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Stores;
using Nop.Services.Tasks;
using Nop.Services.Vendors;
using Nop.Core.Domain.Orders;
using Nop.Data;
using Nop.Services.Orders;
using Nop.Core.Infrastructure;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Data.Extensions;
using Nop.Core.Domain.CustomEntities;
using Nop.Services.Configuration;
using Nop.Core.Domain;
using Nop.Core.Domain.NB;
using Nop.Core.Domain.NB.Merchant;
using Nop.Services.NB.Merchant;
using System.Collections.Generic;

namespace Nop.Web.Framework
{
    /// <summary>
    /// Represents work context for web application
    /// </summary>
    public partial class WebWorkContext : IWorkContext
    {
        #region Fields

        private readonly CurrencySettings _currencySettings;
        private readonly IAuthenticationService _authenticationService;
        private readonly ICurrencyService _currencyService;
        private readonly ICustomerService _customerService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILanguageService _languageService;
        private readonly IStoreContext _storeContext;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IUserAgentHelper _userAgentHelper;
        private readonly IVendorService _vendorService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly TaxSettings _taxSettings;

        //Custom code from v4.0
        private readonly IStoreService _storeService;
        private readonly IDbContext _dbContext;
        private readonly IVendorCustomerRoleServices _vendorCustomerRoleServices;
        private Customer _cachedCustomer;
        private Customer _originalCustomerIfImpersonated;
        private Vendor _cachedVendor;
        private Language _cachedLanguage;
        private Currency _cachedCurrency;
        private TaxDisplayType? _cachedTaxDisplayType;

        //Custom code from v4.0
        private Vendor _cachedMerchant;
        private string _enteredDeliveryAddress;

        private string _cachedMerchantAddress;
        private string _cachedMerchantPhone;


        #endregion

        #region Ctor

        public WebWorkContext(CurrencySettings currencySettings,
            IAuthenticationService authenticationService,
            ICurrencyService currencyService,
            ICustomerService customerService,
            IGenericAttributeService genericAttributeService,
            IHttpContextAccessor httpContextAccessor,
            ILanguageService languageService,
            IStoreContext storeContext,
            IStoreMappingService storeMappingService,
            IUserAgentHelper userAgentHelper,
            IVendorService vendorService,
            LocalizationSettings localizationSettings,
            TaxSettings taxSettings, IStoreService storeService,
            IDbContext dbContext,
            IVendorCustomerRoleServices vendorCustomerRoleServices)
        {
            _currencySettings = currencySettings;
            _authenticationService = authenticationService;
            _currencyService = currencyService;
            _customerService = customerService;
            _genericAttributeService = genericAttributeService;
            _httpContextAccessor = httpContextAccessor;
            _languageService = languageService;
            _storeContext = storeContext;
            _storeMappingService = storeMappingService;
            _userAgentHelper = userAgentHelper;
            _vendorService = vendorService;
            _localizationSettings = localizationSettings;
            _taxSettings = taxSettings;
            _storeService = storeService;
            _dbContext = dbContext;
            _vendorCustomerRoleServices = vendorCustomerRoleServices;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Get nop customer cookie
        /// </summary>
        /// <returns>String value of cookie</returns>
        protected virtual string GetCustomerCookie()
        {
            var cookieName = $"{NopCookieDefaults.Prefix}{NopCookieDefaults.CustomerCookie}";
            return _httpContextAccessor.HttpContext?.Request?.Cookies[cookieName];
        }

        /// <summary>
        /// Set nop customer cookie
        /// </summary>
        /// <param name="customerGuid">Guid of the customer</param>
        protected virtual void SetCustomerCookie(Guid customerGuid)
        {
            if (_httpContextAccessor.HttpContext?.Response == null)
                return;

            //delete current cookie value
            var cookieName = $"{NopCookieDefaults.Prefix}{NopCookieDefaults.CustomerCookie}";
            _httpContextAccessor.HttpContext.Response.Cookies.Delete(cookieName);

            //get date of cookie expiration
            var cookieExpires = 24 * 365; //TODO make configurable
            var cookieExpiresDate = DateTime.Now.AddHours(cookieExpires);

            //if passed guid is empty set cookie as expired
            if (customerGuid == Guid.Empty)
                cookieExpiresDate = DateTime.Now.AddMonths(-1);

            //set new cookie value
            var options = new CookieOptions
            {
                HttpOnly = true,
                Expires = cookieExpiresDate
            };
            _httpContextAccessor.HttpContext.Response.Cookies.Append(cookieName, customerGuid.ToString(), options);
        }

        /// <summary>
        /// Get language from the requested page URL
        /// </summary>
        /// <returns>The found language</returns>
        protected virtual Language GetLanguageFromUrl()
        {
            if (_httpContextAccessor.HttpContext?.Request == null)
                return null;

            //whether the requsted URL is localized
            var path = _httpContextAccessor.HttpContext.Request.Path.Value;
            if (!path.IsLocalizedUrl(_httpContextAccessor.HttpContext.Request.PathBase, false, out Language language))
                return null;

            //check language availability
            if (!_storeMappingService.Authorize(language))
                return null;

            return language;
        }

        /// <summary>
        /// Get language from the request
        /// </summary>
        /// <returns>The found language</returns>
        protected virtual Language GetLanguageFromRequest()
        {
            if (_httpContextAccessor.HttpContext?.Request == null)
                return null;

            //get request culture
            var requestCulture = _httpContextAccessor.HttpContext.Features.Get<IRequestCultureFeature>()?.RequestCulture;
            if (requestCulture == null)
                return null;

            //try to get language by culture name
            var requestLanguage = _languageService.GetAllLanguages().FirstOrDefault(language =>
                language.LanguageCulture.Equals(requestCulture.Culture.Name, StringComparison.InvariantCultureIgnoreCase));

            //check language availability
            if (requestLanguage == null || !requestLanguage.Published || !_storeMappingService.Authorize(requestLanguage))
                return null;

            return requestLanguage;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the current customer
        /// </summary>
        public virtual Customer CurrentCustomer
        {
            get
            {
                //whether there is a cached value
                if (_cachedCustomer != null)
                    return _cachedCustomer;

                Customer customer = null;

                //check whether request is made by a background (schedule) task
                if (_httpContextAccessor.HttpContext == null ||
                    _httpContextAccessor.HttpContext.Request.Path.Equals(new PathString($"/{NopTaskDefaults.ScheduleTaskPath}"), StringComparison.InvariantCultureIgnoreCase))
                {
                    //in this case return built-in customer record for background task
                    customer = _customerService.GetCustomerBySystemName(NopCustomerDefaults.BackgroundTaskCustomerName);
                }

                if (customer == null || customer.Deleted || !customer.Active || customer.RequireReLogin)
                {
                    //check whether request is made by a search engine, in this case return built-in customer record for search engines
                    if (_userAgentHelper.IsSearchEngine())
                        customer = _customerService.GetCustomerBySystemName(NopCustomerDefaults.SearchEngineCustomerName);
                }

                if ((customer == null || customer.Deleted || !customer.Active || customer.RequireReLogin) && _httpContextAccessor.HttpContext != null)
                {
                    //try to get registered user
                    customer = _authenticationService.GetAuthenticatedCustomer();
                }

                if (customer != null && !customer.Deleted && customer.Active && !customer.RequireReLogin)
                {
                    //get impersonate user if required
                    var impersonatedCustomerId = _genericAttributeService
                        .GetAttribute<int?>(customer, NopCustomerDefaults.ImpersonatedCustomerIdAttribute);
                    if (impersonatedCustomerId.HasValue && impersonatedCustomerId.Value > 0)
                    {
                        var impersonatedCustomer = _customerService.GetCustomerById(impersonatedCustomerId.Value);
                        if (impersonatedCustomer != null && !impersonatedCustomer.Deleted && impersonatedCustomer.Active && !impersonatedCustomer.RequireReLogin)
                        {
                            //set impersonated customer
                            _originalCustomerIfImpersonated = customer;
                            customer = impersonatedCustomer;
                        }
                    }
                }

                if (customer == null || customer.Deleted || !customer.Active || customer.RequireReLogin)
                {
                    //get guest customer
                    var customerCookie = GetCustomerCookie();
                    if (!string.IsNullOrEmpty(customerCookie))
                    {
                        if (Guid.TryParse(customerCookie, out Guid customerGuid))
                        {
                            //get customer from cookie (should not be registered)
                            var customerByCookie = _customerService.GetCustomerByGuid(customerGuid);
                            if (customerByCookie != null && !customerByCookie.IsRegistered())
                                customer = customerByCookie;
                        }
                    }
                }

                if (customer == null || customer.Deleted || !customer.Active || customer.RequireReLogin)
                {
                    //create guest if not exists
                    customer = _customerService.InsertGuestCustomer();
                }

                if (!customer.Deleted && customer.Active && !customer.RequireReLogin)
                {
                    //set customer cookie
                    SetCustomerCookie(customer.CustomerGuid);

                    //cache the found customer
                    _cachedCustomer = customer;
                }

                return _cachedCustomer;
            }
            set
            {
                SetCustomerCookie(value.CustomerGuid);
                _cachedCustomer = value;
            }
        }

        /// <summary>
        /// Gets the original customer (in case the current one is impersonated)
        /// </summary>
        public virtual Customer OriginalCustomerIfImpersonated
        {
            get { return _originalCustomerIfImpersonated; }
        }

        /// <summary>
        /// Gets the current vendor (logged-in manager)
        /// </summary>
        public virtual Vendor CurrentVendor
        {
            get
            {
                //whether there is a cached value
                if (_cachedVendor != null)
                    return _cachedVendor;

                if (CurrentCustomer == null)
                    return null;

                //try to get vendor
                var vendor = _vendorService.GetVendorById(CurrentCustomer.VendorId);

                //check vendor availability
                if (vendor == null || vendor.Deleted || !vendor.Active)
                    return null;

                //cache the found vendor
                _cachedVendor = vendor;

                return _cachedVendor;
            }
        }

        /// <summary>
        /// Gets or sets current user working language
        /// </summary>
        public virtual Language WorkingLanguage
        {
            get
            {
                //whether there is a cached value
                if (_cachedLanguage != null)
                    return _cachedLanguage;

                Language detectedLanguage = null;

                //localized URLs are enabled, so try to get language from the requested page URL
                if (_localizationSettings.SeoFriendlyUrlsForLanguagesEnabled)
                    detectedLanguage = GetLanguageFromUrl();

                //whether we should detect the language from the request
                if (detectedLanguage == null && _localizationSettings.AutomaticallyDetectLanguage)
                {
                    //whether language already detected by this way
                    var alreadyDetected = _genericAttributeService.GetAttribute<bool>(CurrentCustomer,
                        NopCustomerDefaults.LanguageAutomaticallyDetectedAttribute, _storeContext.CurrentStore.Id);

                    //if not, try to get language from the request
                    if (!alreadyDetected)
                    {
                        detectedLanguage = GetLanguageFromRequest();
                        if (detectedLanguage != null)
                        {
                            //language already detected
                            _genericAttributeService.SaveAttribute(CurrentCustomer,
                                NopCustomerDefaults.LanguageAutomaticallyDetectedAttribute, true, _storeContext.CurrentStore.Id);
                        }
                    }
                }

                //if the language is detected we need to save it
                if (detectedLanguage != null)
                {
                    //get current saved language identifier
                    var currentLanguageId = _genericAttributeService.GetAttribute<int>(CurrentCustomer,
                        NopCustomerDefaults.LanguageIdAttribute, _storeContext.CurrentStore.Id);

                    //save the detected language identifier if it differs from the current one
                    if (detectedLanguage.Id != currentLanguageId)
                    {
                        _genericAttributeService.SaveAttribute(CurrentCustomer,
                            NopCustomerDefaults.LanguageIdAttribute, detectedLanguage.Id, _storeContext.CurrentStore.Id);
                    }
                }

                //get current customer language identifier
                var customerLanguageId = _genericAttributeService.GetAttribute<int>(CurrentCustomer,
                    NopCustomerDefaults.LanguageIdAttribute, _storeContext.CurrentStore.Id);

                var allStoreLanguages = _languageService.GetAllLanguages(storeId: _storeContext.CurrentStore.Id);

                //check customer language availability
                var customerLanguage = allStoreLanguages.FirstOrDefault(language => language.Id == customerLanguageId);
                if (customerLanguage == null)
                {
                    //it not found, then try to get the default language for the current store (if specified)
                    customerLanguage = allStoreLanguages.FirstOrDefault(language => language.Id == _storeContext.CurrentStore.DefaultLanguageId);
                }

                //if the default language for the current store not found, then try to get the first one
                if (customerLanguage == null)
                    customerLanguage = allStoreLanguages.FirstOrDefault();

                //if there are no languages for the current store try to get the first one regardless of the store
                if (customerLanguage == null)
                    customerLanguage = _languageService.GetAllLanguages().FirstOrDefault();

                //cache the found language
                _cachedLanguage = customerLanguage;

                return _cachedLanguage;
            }
            set
            {
                //get passed language identifier
                var languageId = value?.Id ?? 0;

                //and save it
                _genericAttributeService.SaveAttribute(CurrentCustomer,
                    NopCustomerDefaults.LanguageIdAttribute, languageId, _storeContext.CurrentStore.Id);

                //then reset the cached value
                _cachedLanguage = null;
            }
        }

        /// <summary>
        /// Gets or sets current user working currency
        /// </summary>
        public virtual Currency WorkingCurrency
        {
            get
            {
                //whether there is a cached value
                if (_cachedCurrency != null)
                    return _cachedCurrency;

                //return primary store currency when we're in admin area/mode
                var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
                if (primaryStoreCurrency != null)
                {
                    _cachedCurrency = primaryStoreCurrency;
                    return primaryStoreCurrency;
                }

                //find a currency previously selected by a customer
                var customerCurrencyId = _genericAttributeService.GetAttribute<int>(CurrentCustomer,
                    NopCustomerDefaults.CurrencyIdAttribute, _storeContext.CurrentStore.Id);

                var allStoreCurrencies = _currencyService.GetAllCurrencies(storeId: _storeContext.CurrentStore.Id);

                //check customer currency availability
                var customerCurrency = allStoreCurrencies.FirstOrDefault(currency => currency.Id == customerCurrencyId);
                if (customerCurrency == null)
                {
                    //it not found, then try to get the default currency for the current language (if specified)
                    customerCurrency = allStoreCurrencies.FirstOrDefault(currency => currency.Id == WorkingLanguage.DefaultCurrencyId);
                }

                //if the default currency for the current store not found, then try to get the first one
                if (customerCurrency == null)
                    customerCurrency = allStoreCurrencies.FirstOrDefault();

                //if there are no currencies for the current store try to get the first one regardless of the store
                if (customerCurrency == null)
                    customerCurrency = _currencyService.GetAllCurrencies().FirstOrDefault();

                //cache the found currency
                _cachedCurrency = customerCurrency;

                return _cachedCurrency;
            }
            set
            {
                //get passed currency identifier
                var currencyId = value?.Id ?? 0;

                //and save it
                _genericAttributeService.SaveAttribute(CurrentCustomer,
                    NopCustomerDefaults.CurrencyIdAttribute, currencyId, _storeContext.CurrentStore.Id);

                //then reset the cached value
                _cachedCurrency = null;
            }
        }

        /// <summary>
        /// Gets or sets current tax display type
        /// </summary>
        public virtual TaxDisplayType TaxDisplayType
        {
            get
            {
                //whether there is a cached value
                if (_cachedTaxDisplayType.HasValue)
                    return _cachedTaxDisplayType.Value;

                var taxDisplayType = TaxDisplayType.IncludingTax;

                //whether customers are allowed to select tax display type
                if (_taxSettings.AllowCustomersToSelectTaxDisplayType && CurrentCustomer != null)
                {
                    //try to get previously saved tax display type
                    var taxDisplayTypeId = _genericAttributeService.GetAttribute<int?>(CurrentCustomer,
                        NopCustomerDefaults.TaxDisplayTypeIdAttribute, _storeContext.CurrentStore.Id);
                    if (taxDisplayTypeId.HasValue)
                    {
                        taxDisplayType = (TaxDisplayType)taxDisplayTypeId.Value;
                    }
                    else
                    {
                        //default tax type by customer roles
                        var defaultRoleTaxDisplayType = _customerService.GetCustomerDefaultTaxDisplayType(CurrentCustomer);
                        if (defaultRoleTaxDisplayType != null)
                        {
                            taxDisplayType = defaultRoleTaxDisplayType.Value;
                        }
                    }
                }
                else
                {
                    //default tax type by customer roles
                    var defaultRoleTaxDisplayType = _customerService.GetCustomerDefaultTaxDisplayType(CurrentCustomer);
                    if (defaultRoleTaxDisplayType != null)
                    {
                        taxDisplayType = defaultRoleTaxDisplayType.Value;
                    }
                    else
                    {
                        //or get the default tax display type
                        taxDisplayType = _taxSettings.TaxDisplayType;
                    }
                }

                //cache the value
                _cachedTaxDisplayType = taxDisplayType;

                return _cachedTaxDisplayType.Value;

            }
            set
            {
                //whether customers are allowed to select tax display type
                if (!_taxSettings.AllowCustomersToSelectTaxDisplayType)
                    return;

                //save passed value
                _genericAttributeService.SaveAttribute(CurrentCustomer,
                    NopCustomerDefaults.TaxDisplayTypeIdAttribute, (int)value, _storeContext.CurrentStore.Id);

                //then reset the cached value
                _cachedTaxDisplayType = null;
            }
        }

        /// <summary>
        /// Gets or sets value indicating whether we're in admin area
        /// </summary>
        public virtual bool IsAdmin { get; set; }

        #endregion

        #region Custom code from v4.0
        public virtual int GetCurrentStoreId
        {
            get
            {
                var webHelper = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Core.IWebHelper>();
                var storeUrl = webHelper.GetStoreLocation();

                int currStoreId = _storeService.GetAllStores().Where(x => x.Hosts != null && x.Hosts.Contains(storeUrl)).Select(x => x.Id).FirstOrDefault();

                return currStoreId;
            }
        }

        //Get Current Vendor
        #region Custom code
        public virtual Vendor CurrentMerchant
        {
            get
            {
                //whether there is a cached value
                if (_cachedMerchant != null)
                    return _cachedMerchant;

                //find a currency previously selected by a customer
                var customerCurrentMerchantId = _genericAttributeService.GetAttribute<int>(CurrentCustomer, "CustomerCurrentMerchantId", GetCurrentStoreId, 0);

                if (customerCurrentMerchantId == 0)
                    return null;

                //try to get vendor
                var vendor = _vendorService.GetVendorById(customerCurrentMerchantId);

                //check vendor availability
                if (vendor == null || vendor.Deleted || !vendor.Active)
                    return null;

                //cache the found merchant
                _cachedMerchant = vendor;

                return _cachedMerchant;
            }
            set
            {
                //get passed currency identifier
                var merchantId = value?.Id ?? 0;

                //and save it
                _genericAttributeService.SaveAttribute(CurrentCustomer,
                    "CustomerCurrentMerchantId", merchantId, GetCurrentStoreId);

                //then reset the cached value
                _cachedMerchant = null;
            }
        }

        public virtual int CurrentMerchantId
        {
            get
            {
                return CurrentMerchant?.Id ?? 0;
            }
        }
        #endregion

        #region Custom code 
        public virtual string EnteredDeliveryAddress
        {
            get
            {
                //whether there is a cached value
                if (_enteredDeliveryAddress != null)
                    return _enteredDeliveryAddress;

                var enteredDeliveryAddress = _genericAttributeService.GetAttribute<string>(CurrentCustomer, "EnteredDeliveryAddress",
                    storeId: GetCurrentStoreId);

                //cache the found address
                _enteredDeliveryAddress = enteredDeliveryAddress;

                return _enteredDeliveryAddress;
            }
            set
            {
                //save it
                _genericAttributeService.SaveAttribute(CurrentCustomer,
                    "EnteredDeliveryAddress", value, GetCurrentStoreId);

                //then reset the cached value
                _enteredDeliveryAddress = null;
            }
        }
        #endregion

        #region Custom Code For Display Merchant Addess And PhoneNo 
        #region Custom code 
        public virtual string CurrentMerchantAddress
        {
            get
            {
                if (CurrentMerchant != null && CurrentMerchant.AddressId > 0)
                {
                    var _addressService = EngineContext.Current.Resolve<IAddressService>();
                    var address = _addressService.GetAddressById(CurrentMerchant.AddressId);
                    if (address != null)
                    {
                        var addressLine = "";
                        if (!string.IsNullOrEmpty(address.Address1))
                        {
                            addressLine += address.Address1;
                        }
                        if (!string.IsNullOrEmpty(address.Address2))
                        {
                            addressLine += ", " + address.Address2;
                        }
                        if (!string.IsNullOrEmpty(address.City))
                        {
                            addressLine += ", " + address.City;
                        }
                        if (address.StateProvince != null && !string.IsNullOrEmpty(address.StateProvince.Name))
                        {
                            addressLine += ", " + address.StateProvince.Name;
                        }
                        return addressLine;
                    }
                }



                return null;
            }
            set
            {
                //get passed currency identifier
                var address = !string.IsNullOrEmpty(value) ? value : "";

                //and save it
                _genericAttributeService.SaveAttribute(CurrentCustomer,
                    "CustomerCurrentMerchantAddress", address, GetCurrentStoreId);

                //then reset the cached value
                _cachedMerchantAddress = null;
            }
        }

        public virtual string CurrentMerchantPhone
        {
            get
            {
                if (CurrentMerchant != null || CurrentMerchant.AddressId > 0)
                {
                    var _addressService = EngineContext.Current.Resolve<IAddressService>();
                    var address = _addressService.GetAddressById(CurrentMerchant.AddressId);
                    if (address != null)
                    {
                        return address.PhoneNumber;
                    }
                }

                return null;
            }
            set
            {
                //get passed currency identifier
                var phone = !string.IsNullOrEmpty(value) ? value : "";

                //and save it
                _genericAttributeService.SaveAttribute(CurrentCustomer,
                    "CustomerCurrentMerchantPhone", phone, GetCurrentStoreId);

                //then reset the cached value
                _cachedMerchantPhone = null;
            }
        }
        #endregion
        #endregion


        #region Custom code 
        public virtual string CurrentStoreISDCode
        {
            get
            {
                try
                {
                    var _commonService = EngineContext.Current.Resolve<ICommonService>();
                    var data = _commonService.GetISDCode(GetCurrentStoreId).First();
                    return data?.ISDCode;
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
            }
        }
        #endregion

        #region Custom code 
        public virtual OrderDetails CurrentOrderDetails
        {
            get
            {
                var _orderService = EngineContext.Current.Resolve<IOrderService>();
                var ordersDetails = _orderService.GetOrderDetails(customerId: CurrentCustomer.Id, storeId: GetCurrentStoreId, isOrderComplete: false);
                if (ordersDetails == null)
                {
                    //Delivery , Takeaway , Dining
                    var _settingService = EngineContext.Current.Resolve<ISettingService>();
                    string defaultOrderTypeString = _settingService.GetSettingByKey<string>("setting.order.defaultordertype", "", GetCurrentStoreId);
                    var defaultOrderType = defaultOrderTypeString.Equals("Delivery") ? 1 : (defaultOrderTypeString.Equals("Takeaway") ? 2 : (defaultOrderTypeString.Equals("Dining") ? 2 : 1));
                    var storeDetails = _storeContext.CurrentStoreById(GetCurrentStoreId);
                    ordersDetails = new OrderDetails();
                    ordersDetails.StoreId = GetCurrentStoreId;
                    ordersDetails.CustomerId = CurrentCustomer.Id;
                    ordersDetails.IsOrderComplete = false;
                    ordersDetails.CreatedOnUtc = DateTime.UtcNow;
                    if (storeDetails.IsDelivery == true && storeDetails.IsTakeaway == false && storeDetails.IsDinning == false)
                        ordersDetails.OrderType = 1;
                    else if (storeDetails.IsDelivery == false && storeDetails.IsTakeaway == true && storeDetails.IsDinning == false)
                        ordersDetails.OrderType = 2;
                    else if (storeDetails.IsDelivery == false && storeDetails.IsTakeaway == false && storeDetails.IsDinning == true)
                        ordersDetails.OrderType = 3;
                    else
                        ordersDetails.OrderType = defaultOrderType;
                    _orderService.InsertOrderDetails(ordersDetails);
                }
                return ordersDetails;
            }
            set
            {
                var ordersDetails = value;
                if (ordersDetails != null)
                {
                    var _orderService = EngineContext.Current.Resolve<IOrderService>();
                    _orderService.UpdateOrderDetails(ordersDetails);
                }
            }
        }
        public virtual bool IsVendorRole
        {
            get
            {
                var _cacheManager = EngineContext.Current.Resolve<ICacheManager>();
                string cacheKey = "Admin.IsVendor.Key-" + GetCurrentStoreId + "-" + CurrentCustomer.Id;
                bool VendorFlag = _cacheManager.Get(cacheKey, () =>
                {
                    return CurrentCustomer.CustomerRoles.Any(x => x.Name == "Vendors");
                });
                return VendorFlag;
            }
        }
        public virtual bool IsAdminRole
        {
            get
            {
                var _cacheManager = EngineContext.Current.Resolve<ICacheManager>();
                string cacheKey = "Admin.Administrators.Key-" + GetCurrentStoreId + "-" + CurrentCustomer.Id;
                bool VendorFlag = _cacheManager.Get(cacheKey, () =>
                {
                    return CurrentCustomer.CustomerRoles.Any(x => x.Name == "Administrators");
                });
                return VendorFlag;
            }
        }
        public virtual bool IsStoreOwnerRole
        {
            get
            {
                var _cacheManager = EngineContext.Current.Resolve<ICacheManager>();
                string cacheKey = "Admin.StoreOwner.Key-" + GetCurrentStoreId + "-" + CurrentCustomer.Id;
                bool VendorFlag = _cacheManager.Get(cacheKey, () =>
                {
                    return CurrentCustomer.CustomerRoles.Any(x => x.Name == "Store Owner");
                });
                return VendorFlag;
            }
        }

        public virtual StoreInformationSettings StoreInformationSettings
        {
            get
            {
                var settingService = EngineContext.Current.Resolve<ISettingService>();
                return settingService.LoadSetting<StoreInformationSettings>(GetCurrentStoreId);
            }
        }
        #endregion
        #endregion

        #region Generate OTP
        /// <summary>
        /// Generate OTP
        /// </summary>
        public virtual string GenerateOTP
        {
            get
            {
                string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
                string sRandomOTP = GenerateRandomOTP(6, saAllowedCharacters);
                return sRandomOTP;
            }
        }
        private string GenerateRandomOTP(int iOTPLength, string[] saAllowedCharacters)
        {
            string sOTP = String.Empty;
            string sTempChars = String.Empty;
            Random rand = new Random();

            for (int i = 0; i < iOTPLength; i++)
            {
                int p = rand.Next(0, saAllowedCharacters.Length);
                sTempChars = saAllowedCharacters[rand.Next(0, saAllowedCharacters.Length)];
                sOTP += sTempChars;
            }

            return sOTP;
        }
        #endregion

        #region User sub admin


        /// <summary>
        /// Get User Sub Admin details.
        /// </summary>
        public virtual UserSubAdmin SubAdmin
        {
            get
            {
                var cacheManager = EngineContext.Current.Resolve<ICacheManager>();
                string cacheKey = string.Format(Nop.Services.Catalog.NopNBDefaults.UserSubAdminCacheKey, CurrentCustomer.Id);

                return cacheManager.Get(cacheKey, () =>
                {
                    var subAdmin = new UserSubAdmin()
                    {
                        IsSubAdminRole = false
                    };
                    var subAdminRole = CurrentCustomer.CustomerRoles.FirstOrDefault(x => x.CreatedByNB && x.UserGroupId > 0);
                    if (subAdminRole != null)
                    {
                        var vendorIds = new List<int>();
                        if (subAdminRole.UserGroupId == (int)UserGroups.Business)
                        {
                            //if BusinessType than assign vendorIds from our mapping table (one/multiple merchants)
                            vendorIds = _vendorCustomerRoleServices.GetAllMappingsByCustomerRoleId(subAdminRole.Id)?.Select(m => m.VendorId)?.ToList();
                        }
                        else
                        {
                            //if MerchantType than assign vendorId from our generic attribute (one merchant only)
                            var merchantId = _genericAttributeService.GetAttribute<int>(CurrentCustomer, NopNbDomainDefaults.SubAdminMerchantIdAttribute, CurrentCustomer.RegisteredInStoreId);
                            if (merchantId > 0)
                                vendorIds.Add(merchantId);
                        }
                        subAdmin.AssociatedVenorIds = vendorIds;
                        subAdmin.IsSubAdminRole = true;
                        subAdmin.UserGroupId = subAdminRole.UserGroupId;
                    }

                    return subAdmin;
                });
            }
        }

        #endregion
    }
}