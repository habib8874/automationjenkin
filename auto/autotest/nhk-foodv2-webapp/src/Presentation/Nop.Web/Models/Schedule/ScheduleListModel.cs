﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.Schedule
{
    public partial class ScheduleDataListModel : BasePagedListModel<ScheduleListModel>
    {
    }
    //[Validator(typeof(ScheduleValidatorModel))]
    public class ScheduleListSearchModel : BaseSearchModel
    {
        public ScheduleListSearchModel()
        {
            AvailableStores = new List<SelectListItem>();
            AvailableVendors = new List<SelectListItem>();
            AvailableFromTiming = new List<SelectListItem>();
            AvailableToTiming = new List<SelectListItem>();
            AvailableDays = new List<SelectListItem>();
        }
        public SchedulesDay? SchedulesDay { get; set; }
        public bool IsLoggedInAsVendor { get; set; }
        public bool IsCurrentVendor { get; set; }
        public int Id { get; set; }
        public int ScheduleId { get; set; }
        [NopResourceDisplayName("Admin.Schedule.Name")]
        public string SearchScheduleName { get; set; }
        public int StoreId { get; set; }
        [NopResourceDisplayName("Admin.Booking.List.Store")]
        public int SearchStoreId { get; set; }
        [NopResourceDisplayName("Admin.Booking.List.Vendor")]
        public int SearchVendorId { get; set; }

        public string Store { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
        public int VendorId { get; set; }
        public string Vendor { get; set; }
        public IList<SelectListItem> AvailableVendors { get; set; }
        public IList<SelectListItem> AvailableFromTiming { get; set; }
        public IList<SelectListItem> AvailableToTiming { get; set; }
        public IList<SelectListItem> AvailableDays { get; set; }
        [NopResourceDisplayName("Admin.Booking.List.AllDay")]
        public bool IsAllDay { get; set; }
        [NopResourceDisplayName("Admin.Booking.List.CustomDay")]
        public bool IsCustomDay { get; set; }
        public bool IsMonday { get; set; }
        public bool IsTuesday { get; set; }
        public bool IsWednesday { get; set; }
        public bool IsThursday { get; set; }
        public bool IsFriday { get; set; }
        public bool IsSaturday { get; set; }
        public bool IsSunday { get; set; }
        public string Name { get; set; }
        public TimeSpan FromTime { get; set; }
        public TimeSpan ToTime { get; set; }
        [NopResourceDisplayName("Admin.Schedule.FromStringTime")]
        public string FromStringTime { get; set; }
        [NopResourceDisplayName("Admin.Schedule.ToStringTime")]
        public string ToStringTime { get; set; }
        [NopResourceDisplayName("Admin.Schedule.IsActive")]
        public bool IsActive { get; set; }
    }
    public class ScheduleListModel : BaseNopModel
    {
        public ScheduleListModel()
        {
            AvailableStores = new List<SelectListItem>();
            AvailableVendors = new List<SelectListItem>();
            AvailableFromTiming = new List<SelectListItem>();
            AvailableToTiming = new List<SelectListItem>();
            AvailableDays = new List<SelectListItem>();
        }
        public SchedulesDay? SchedulesDay { get; set; }
        public bool IsLoggedInAsVendor { get; set; }
        public bool IsCurrentVendor { get; set; }
        public int Id { get; set; }
        [NopResourceDisplayName("Admin.Schedule.Name")]
        public string SearchScheduleName { get; set; }
        public int StoreId { get; set; }
        [NopResourceDisplayName("Admin.Booking.List.Store")]
        public int SearchStoreId { get; set; }
        [NopResourceDisplayName("Admin.Booking.List.Vendor")]
        public int SearchVendorId { get; set; }

        public string Store { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
        public int VendorId { get; set; }
        public string Vendor { get; set; }
        public IList<SelectListItem> AvailableVendors { get; set; }
        public IList<SelectListItem> AvailableFromTiming { get; set; }
        public IList<SelectListItem> AvailableToTiming { get; set; }
        public IList<SelectListItem> AvailableDays { get; set; }
        [NopResourceDisplayName("Admin.Booking.List.AllDay")]
        public bool IsAllDay { get; set; }
        [NopResourceDisplayName("Admin.Booking.List.CustomDay")]
        public bool IsCustomDay { get; set; }
        public bool IsMonday { get; set; }
        public bool IsTuesday { get; set; }
        public bool IsWednesday { get; set; }
        public bool IsThursday { get; set; }
        public bool IsFriday { get; set; }
        public bool IsSaturday { get; set; }
        public bool IsSunday { get; set; }
        public string Name { get; set; }
        public TimeSpan FromTime { get; set; }
        public TimeSpan ToTime { get; set; }
        [NopResourceDisplayName("Admin.Schedule.FromStringTime")]
        public string FromStringTime { get; set; }
        [NopResourceDisplayName("Admin.Schedule.ToStringTime")]
        public string ToStringTime { get; set; }
        [NopResourceDisplayName("Admin.Schedule.IsActive")]
        public bool IsActive { get; set; }
    }
    public enum SchedulesDay
    {
        /// <summary>
        /// Sunday
        /// </summary>
        Sunday = 1,

        /// <summary>
        /// Sunday
        /// </summary>
        Monday = 2,
        /// <summary>
        /// Sunday
        /// </summary>
        Tuesday = 3,
        /// <summary>
        /// Sunday
        /// </summary>
        Wednesday = 4,
        /// <summary>
        /// Sunday
        /// </summary>
        Thursday = 5,
        /// <summary>
        /// Sunday
        /// </summary>
        Friday = 6,
        /// <summary>
        /// Sunday
        /// </summary>
        Saturday = 7,
        /// <summary>
        /// Sunday
        /// </summary>
        [Display(Name = "All Days")]
        All = 8,
    }
}