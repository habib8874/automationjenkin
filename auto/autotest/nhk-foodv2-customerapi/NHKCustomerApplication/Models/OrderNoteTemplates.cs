﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class OrderNoteTemplates
    {
        public int OrderStatus { get; set; }
        public int TemplateId { get; set; }
    }
}
