﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Services.Localization;
using Nop.Services.NB.Package;
using Nop.Web.Models.NB;

namespace Nop.Web.Factories.NB
{
    public partial class NBCustomerPackageModelFactory : INBCustomerPackageModelFactory
    {

        #region Fields

        private readonly INBPackageService _nBPackageService;
        private readonly ILocalizationService _localizationService;

        #endregion

        #region Ctor

        public NBCustomerPackageModelFactory(
            INBPackageService nBPackageService,
            ILocalizationService localizationService)
        {
            _nBPackageService = nBPackageService;
            _localizationService = localizationService;
        }

        #endregion

        #region Methods

        // <summary>
        /// Get PackagePlan by Package identifier
        /// </summary>
        /// <param name="packageId">Package identifier</param>
        /// <param name="addSelectPackagePlan">Whether to add "Select Package" item to list of packages</param>
        /// <returns>List of identifiers and names of PackagePlan</returns>
       
        public virtual IList<PackagePlanModel> GetPackagesPlanBypackageId(string packageId, bool addSelectPacagePlanItem)
        {
            if (string.IsNullOrEmpty(packageId))
                throw new ArgumentNullException(nameof(packageId));

                var package = _nBPackageService.GetPackageById(Convert.ToInt32(packageId));
                var packagePlans = _nBPackageService.GetPackagePlans(package != null ? package.Id : 0).ToList();
                var result = new List<PackagePlanModel>();
                foreach (var packagePlan in packagePlans)
                    result.Add(new PackagePlanModel
                    {
                        Id = packagePlan.Id,
                        Name =  packagePlan.Name
                    });

                if (package == null)
                {
                    //package is not selected ("choose package" item)
                    if (addSelectPacagePlanItem)
                    {
                        result.Insert(0, new PackagePlanModel
                        {
                            Id = 0,
                            Name = _localizationService.GetResource("NB.Account.Fields.SelectPackagePlan")
                        });
                    }
                }

                return result;
        }
        #endregion
    }
}
