﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Orders;
using Nop.Core;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Web.Models.ShoppingCart;
using System;
using System.Linq;

namespace Nop.Web.Components
{
    public class FlyoutShoppingCartViewComponent : NopViewComponent
    {
        private readonly IPermissionService _permissionService;
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;
        private readonly ShoppingCartSettings _shoppingCartSettings;
        //Custom code from v4.0
        private readonly IWorkContext _workContext; 
        private readonly IStoreContext _storeContext; 
        private readonly ICatalogModelFactory _catalogModelFactory;

        public FlyoutShoppingCartViewComponent(IPermissionService permissionService,
            IShoppingCartModelFactory shoppingCartModelFactory,
            ShoppingCartSettings shoppingCartSettings, IWorkContext workContext, IStoreContext storeContext,
            ICatalogModelFactory catalogModelFactory)
        {
            _permissionService = permissionService;
            _shoppingCartModelFactory = shoppingCartModelFactory;
            _shoppingCartSettings = shoppingCartSettings;
            _workContext = workContext;
            _storeContext = storeContext;
            _catalogModelFactory = catalogModelFactory;
        }

        public IViewComponentResult Invoke(bool isForCheckout = false)
        {
            if (!_shoppingCartSettings.MiniShoppingCartEnabled)
                return Content("");

            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return Content("");

            var model = _shoppingCartModelFactory.PrepareMiniShoppingCartModel();

            //Custom code from v4.0
            model.IsForCheckout = isForCheckout;
            var currentTime = DateTime.Now.TimeOfDay;
            string merchantid = (_workContext.CurrentMerchant?.Id??0).ToString();
            //if (merchantid == "0")
            //    return Content("");
            model.IsDisplayCheckoutButton = false;
            var merchantModel = _catalogModelFactory.PrepareMerchantAllModels().Where(x => x.Id.ToString() == merchantid).ToList();
            if(merchantModel.Count > 0)
            foreach (var item in merchantModel.FirstOrDefault().AvailableSchedules)
            {
                if (item.ScheduleFromTimeTS <= currentTime && item.ScheduleToTimeTS >= currentTime)
                {
                    model.IsDisplayCheckoutButton = true;
                }
            }
            return View(model);
        }
    }
}
