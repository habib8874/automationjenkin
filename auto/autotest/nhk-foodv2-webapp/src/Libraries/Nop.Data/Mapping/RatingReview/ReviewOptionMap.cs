﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.RatingReview;

namespace Nop.Data.Mapping.RatingReview
{
    /// <summary>
    /// Represents a review option configuration
    /// </summary>
    public partial class ReviewOptionMap : NopEntityTypeConfiguration<ReviewOption>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<ReviewOption> builder)
        {
            builder.ToTable(NopMappingDefaults.ReviewOptionTable);
            builder.HasKey(r => r.Id);

            base.Configure(builder);
        }

        #endregion
    }
}
