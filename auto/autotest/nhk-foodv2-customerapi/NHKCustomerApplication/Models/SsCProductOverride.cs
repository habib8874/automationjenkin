﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsCProductOverride
    {
        public int Id { get; set; }
        public int ConditionId { get; set; }
        public int ProductId { get; set; }
        public int ProductState { get; set; }

        public virtual SsCCondition Condition { get; set; }
    }
}
