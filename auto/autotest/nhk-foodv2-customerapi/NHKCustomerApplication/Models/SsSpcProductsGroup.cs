﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsSpcProductsGroup
    {
        public SsSpcProductsGroup()
        {
            SsSpcProductsGroupItem = new HashSet<SsSpcProductsGroupItem>();
        }

        public int Id { get; set; }
        public bool Published { get; set; }
        public string Title { get; set; }
        public string WidgetZone { get; set; }
        public int Store { get; set; }
        public int NumberOfProductsPerItem { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<SsSpcProductsGroupItem> SsSpcProductsGroupItem { get; set; }
    }
}
