﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Models.Media;

namespace Nop.Web.Models.NB.MerchantCategorys
{
    /// <summary>
    /// Represents a MerchantCategory model
    /// </summary>
    public partial class MerchantCategoryModel : BaseNopEntityModel
    {
        public MerchantCategoryModel() 
        {
            PictureModel = new PictureModel();
        }

        public string Name { get; set; }
        
        public string ShortDescription { get; set; }
        
        public int PictureId { get; set; }

        public PictureModel PictureModel { get; set; }

        public int DisplayOrder { get; set; }
        
        public int StoreId { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the entity is published
        /// </summary>
        
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }
        
        public string StoreName { get; set; }

        public string StoreIds { get; set; }
    }
}