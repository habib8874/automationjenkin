﻿using Nop.Core.Domain.Localization;
using System;

namespace Nop.Core.Domain.UserPushIdDetails
{
    public partial class UserPushIdDetail : BaseEntity
    {
        /// <summary>
        /// Gets or sets the user id
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the token value
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Gets or sets the Token datetime value
        /// </summary>
        public DateTime  CrDt { get; set; }
    }
}
