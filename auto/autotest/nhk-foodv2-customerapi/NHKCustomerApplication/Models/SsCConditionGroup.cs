﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsCConditionGroup
    {
        public SsCConditionGroup()
        {
            SsCConditionStatement = new HashSet<SsCConditionStatement>();
        }

        public int Id { get; set; }
        public int ConditionId { get; set; }

        public virtual SsCCondition Condition { get; set; }
        public virtual ICollection<SsCConditionStatement> SsCConditionStatement { get; set; }
    }
}
