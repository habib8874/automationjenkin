﻿using System;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Nestorbird.Plugin.Api.ResponseModel;
using Nop.Core;
using Nop.Core.Domain.Localization;
using Nop.Core.Infrastructure;
using Nop.Services.Localization;
using Nop.Services.Media;

namespace Nestorbird.Plugin.Api.Controllers
{
    [ApiController]
    public abstract class BaseAPIController : Controller
    {
        public readonly ILanguageService _languageService;
        private IWorkContext _workContext;

        public BaseAPIController(ILanguageService languageService)
        {
            _languageService = languageService;
        }

        public int CurrentLanguageId = 1;
        public Language CurrentLanguage = null;

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            _workContext = EngineContext.Current.Resolve<IWorkContext>();
            var langId = context.HttpContext.Request.Headers["LanguageId"].ToString();
            if (!string.IsNullOrEmpty(langId))
            {
                CurrentLanguageId = Convert.ToInt32(langId);
                if (CurrentLanguageId <= 0)
                {
                    CurrentLanguageId = 1;
                }
            }
            else
            {
                if (context.HttpContext.Request.Query.ContainsKey("LanguageId"))
                {
                    langId = context.HttpContext.Request.Query["LanguageId"];
                    if (!string.IsNullOrEmpty(langId))
                    {
                        CurrentLanguageId = Convert.ToInt32(langId);
                        if (CurrentLanguageId <= 0)
                        {
                            CurrentLanguageId = 1;
                        }
                    }
                }
            }

            CurrentLanguage = _languageService.GetLanguageById(CurrentLanguageId);
            _workContext.WorkingLanguage = CurrentLanguage;

            base.OnActionExecuting(context);
        }
                
        protected virtual int SavePicture(string imageBase64, string imageName)
        {
            try
            {
                if (!string.IsNullOrEmpty(imageBase64) && !string.IsNullOrEmpty(imageName))
                {
                    var imageBytes = Convert.FromBase64String(imageBase64);
                    using (var stream = new MemoryStream(imageBytes))
                    {
                        var formFile = new FormFile(stream, 0, stream.Length, null, imageName)
                        {
                            Headers = new HeaderDictionary(),
                            ContentType = string.Empty
                        };

                        var _pictureService = EngineContext.Current.Resolve<IPictureService>();
                        var picture = _pictureService.InsertPicture(formFile, imageName);
                        return picture.Id;
                    }
                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                return 0;
            }
        }
                
        protected virtual IActionResult Error(string message)
        {
            var response = new BaseResponse<string>
            {
                Success = false,
                Message = message
            };

            return Ok(response);
        }

        protected virtual IActionResult Success<T>(T data, string message = "")
        {
            var response = new BaseResponse<T>
            {
                Success = true,
                Message = message,
                Data = data
            };

            return Ok(response);
        }
    }
}
