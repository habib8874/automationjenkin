﻿using FluentValidation;
using Nop.Core.Domain;
using Nop.Core.Domain.Customers;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.Customer;

namespace Nop.Web.Validators.Customer
{
    public partial class LoginValidator : BaseNopValidator<LoginModel>
    {
        public LoginValidator(ILocalizationService localizationService, CustomerSettings customerSettings, StoreInformationSettings storeSettings)
        {
            if (!customerSettings.UsernamesEnabled && !storeSettings.EnableOTPAuthentication)
            {
                //login by email
                //RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Account.Login.Fields.Email.Required"));
                //RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Common.WrongEmail"));

                //login by Email/phone
            }

            //T6966 Changes
            RuleFor(x => x.EmailOrPhone).NotEmpty().WithMessage(localizationService.GetResource("Account.Login.Fields.EmailOrPhone.Required"));
            RuleFor(x => x.Password).NotEmpty().WithMessage(localizationService.GetResource("Account.Login.Fields.Password.Required"));//T7873
        }
    }
}