﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Areas.Admin.Models.RatingReview
{
    public partial class MerchantRatingModel: BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Store")]
        public string StoreName { get; set; }
        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Product")]
        public int ProductId { get; set; }
        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Product")]
        public string ProductName { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Customer")]
        public int CustomerId { get; set; }
        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Customer")]
        public string CustomerInfo { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Title")]
        public string Title { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.ReviewText")]
        public string ReviewText { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.ReplyText")]
        public string ReplyText { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Rating")]
        public int Rating { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.IsApproved")]
        public bool IsApproved { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }

        //vendor
        public bool IsLoggedInAsVendor { get; set; }



        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Merchant")]
        public string Merchant { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.ReviewOptions")]
        public string ReviewOptions { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.DislikedReviewOptions")]
        public string DislikedReviewOptions { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Merchant")]
        public int MerchantId { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Like/Dislike")]
        public bool LikeDislike { get; set; }

        public FeedbackModel FeedbackModel { get; set; }
    }
}
