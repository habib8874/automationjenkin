﻿using System;

namespace Nop.Core.Domain.NB.Package
{
    /// <summary>
    /// PackagePlan
    /// </summary>
    public partial class NBPackagePlan : BaseEntity
    {
        /// <summary>
        /// Gets or sets the PackageId 
        /// </summary>
        public int PackageId { get; set; }

        /// <summary>
        /// Gets or sets the Name 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Price 
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the BillingInterval 
        /// </summary>
        public string BillingInterval { get; set; }

        /// <summary>
        /// Gets or sets the StartDate 
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the EndDate 
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the TrailPeriod 
        /// </summary>
        public string TrailPeriod { get; set; }

        /// <summary>
        /// Gets or sets the ProductId 
        /// </summary>
        public int? ProductId { get; set; }

    }
}
