﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Factories.NB;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Controllers.NB
{
    public partial class NBCustomerPackageController : BasePublicController
    {
        #region Fields

        private readonly INBCustomerPackageModelFactory _nbCustomerPackageModelFactory;

        #endregion

        #region Ctor

        public NBCustomerPackageController(INBCustomerPackageModelFactory nbCustomerPackageModelFactory)

        {
            _nbCustomerPackageModelFactory = nbCustomerPackageModelFactory;
        }

        #endregion

        #region package Plans

        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult GetPackagePlanByPackageId(string packageId, bool addSelectPacagePlanItem)
        {
            var model = _nbCustomerPackageModelFactory.GetPackagesPlanBypackageId(packageId, addSelectPacagePlanItem);
            return Json(model);
        }

        #endregion

    }
}
