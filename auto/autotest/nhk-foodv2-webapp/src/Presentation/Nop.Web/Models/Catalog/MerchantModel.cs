﻿using System;
using System.Collections.Generic;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Media;

namespace Nop.Web.Models.Catalog
{
    public partial class MerchantModel : BaseNopEntityModel
    {
        public MerchantModel()
        {
            PictureModel = new PictureModel();
            Products = new List<ProductOverviewModel>();
            PagingFilteringContext = new CatalogPagingFilteringModel();
            AvailableSchedules = new List<VendorScheduleModel>();
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        public string SeName { get; set; }
        public bool AllowCustomersToContactVendors { get; set; }

        public PictureModel PictureModel { get; set; }

        public CatalogPagingFilteringModel PagingFilteringContext { get; set; }

        public IList<ProductOverviewModel> Products { get; set; }
        public bool Active { get; set; }
        public int StoreId { get; set; }
        public string Geolocation { get; set; }
        public string Geofancing { get; set; }
        public string WeeklyOff { get; set; }
        public string Opentime { get; set; }
        public string Closetime { get; set; }
        public bool IsOpen { get; set; }
        public bool IsTakeAway { get; set; }
        public bool IsDelivery { get; set; }
        public bool IsDining { get; set; }
        public string ExpDeliveryTime { get; set; }


        //New
        public bool IsBookTableEnable { get; set; }        
        public bool RatingPercent { get; set; }        
        public bool IsAvailableSchedulesEnable { get; set; }        
        public bool IsCostForEnable { get; set; }        
        public bool IsDeliveryTimeEnable { get; set; }
        //New


        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Zip { get; set; }
        public string PhoneNo { get; set; }
        public string FaxNo { get; set; }
        public int AvailableType { get; set; }
        public IList<VendorScheduleModel> AvailableSchedules { get; set; }

        public bool MerchantIsAvailable { get; set; }
        public string Latvalue { get; set; }
        public string LongValue { get; set; }

        //indicates whether merchant available in customers favoritelist
        public bool InFavoritelist { get; set; }
        public bool IsMyaccountPage { get; set; }

        //overall reviews
        public decimal ReviewPercentage { get; set; }
        public int ReviewCount { get; set; }
    }
    public class VendorScheduleModel : BaseNopEntityModel
    {
        public VendorScheduleModel()
        {
        }
        public string ScheduleName { get; set; }
        public int ScheduleDay { get; set; }
        public string ScheduleDayS { get; set; }
        public string ScheduleFromTime { get; set; }
        public TimeSpan ScheduleFromTimeTS { get; set; }
        public TimeSpan ScheduleToTimeTS { get; set; }
        public string ScheduleToTime { get; set; }
        public int DisplayOrder { get; set; }
        public int VendorId { get; set; }
    }
}
