﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.RatingReview;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Orders;
using Nop.Services.RatingReview;
using Nop.Services.Security;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Web.Models.RatingReview;
using Nop.Web.Models.ShoppingCart;
using System.Linq;

namespace Nop.Web.Components
{
    public class DisplayRatingsViewComponent : NopViewComponent
    {
        private readonly IRatingReviewService _ratingReviewService;
        private readonly IOrderService _orderService;
        private readonly IRatingReviewFactory _ratingReviewFactory;
        private readonly ISettingService _settingService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;

        public DisplayRatingsViewComponent(IRatingReviewService ratingReviewService, IOrderService orderService,
            IRatingReviewFactory ratingReviewFactory, ISettingService settingService,
            IWorkContext workContext, IStoreContext storeContext)
        {
            this._ratingReviewService = ratingReviewService;
            this._orderService = orderService;
            this._ratingReviewFactory = ratingReviewFactory;
            this._settingService = settingService;
            this._workContext = workContext;
            this._storeContext = storeContext;
        }

        public IViewComponentResult Invoke(int reviewType, int productId = 0, int merchantId=0)
        {
            int storeId = _workContext.GetCurrentStoreId;
            decimal ratingPercent = 0;
            if (reviewType == (int)ReviewType.Item)
            {
                ratingPercent = _ratingReviewService.GetAverageRating(storeId, productId, reviewType, out int reviewCount);
            }
            else
            {
                int entityId = (merchantId>0)? merchantId: (_workContext.CurrentMerchant?.Id??0);
                ratingPercent = _ratingReviewService.GetAverageRating(storeId, entityId, reviewType, out int reviewCount);
            }
            var model = new DisplayRatingsModel();
            model.ReviewType = reviewType;
            model.RatingPercent = ratingPercent;
            return View(model);
        }
    }
}
