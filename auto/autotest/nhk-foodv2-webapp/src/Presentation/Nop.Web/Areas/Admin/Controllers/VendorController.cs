﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Vendors;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Vendors;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Vendors;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Core.Infrastructure;
using Nop.Core;
using Nop.Services.Stores;
using System.Globalization;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models.Extensions;
using Nop.Web.Areas.Admin.Models.Schedule;
using Nop.Services.Configuration;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Discounts;
using Nop.Services.Discounts;
using Nop.Core.Domain.NB.Discount;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class VendorController : BaseAdminController
    {
        #region Fields

        private readonly IAddressService _addressService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ICustomerService _customerService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IPictureService _pictureService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IVendorAttributeParser _vendorAttributeParser;
        private readonly IVendorAttributeService _vendorAttributeService;
        private readonly IVendorModelFactory _vendorModelFactory;
        private readonly IVendorService _vendorService;
        private readonly ISettingService _settingService;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IDiscountService _discountService;

        #endregion

        #region Ctor
        public VendorController(IAddressService addressService,
            ICustomerActivityService customerActivityService,
            ICustomerService customerService,
            IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService,
            INotificationService notificationService,
            IPermissionService permissionService,
            IPictureService pictureService,
            IUrlRecordService urlRecordService,
            IVendorAttributeParser vendorAttributeParser,
            IVendorAttributeService vendorAttributeService,
            IVendorModelFactory vendorModelFactory,
            IVendorService vendorService,
             ISettingService settingService,
            IWorkContext workContext,
            IStoreContext storeContext,
            IStoreService storeService,
            IDiscountService discountService)
        {
            _storeContext = storeContext;
            _addressService = addressService;
            _customerActivityService = customerActivityService;
            _customerService = customerService;
            _genericAttributeService = genericAttributeService;
            _localizationService = localizationService;
            _localizedEntityService = localizedEntityService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _pictureService = pictureService;
            _urlRecordService = urlRecordService;
            _vendorAttributeParser = vendorAttributeParser;
            _vendorAttributeService = vendorAttributeService;
            _vendorModelFactory = vendorModelFactory;
            _vendorService = vendorService;
            _workContext = workContext;
            _storeService = storeService;
            _settingService = settingService;
            _discountService = discountService;
        }

        #endregion

        #region Utilities

        protected virtual void UpdatePictureSeoNames(Vendor vendor)
        {
            var picture = _pictureService.GetPictureById(vendor.PictureId);
            if (picture != null)
                _pictureService.SetSeoFilename(picture.Id, _pictureService.GetPictureSeName(vendor.Name));
        }
        protected virtual void UpdateLocales(Vendor vendor, VendorModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(vendor,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(vendor,
                    x => x.Description,
                    localized.Description,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(vendor,
                    x => x.MetaKeywords,
                    localized.MetaKeywords,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(vendor,
                    x => x.MetaDescription,
                    localized.MetaDescription,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(vendor,
                    x => x.MetaTitle,
                    localized.MetaTitle,
                    localized.LanguageId);

                //search engine name
                var seName = _urlRecordService.ValidateSeName(vendor, localized.SeName, localized.Name, false);
                _urlRecordService.SaveSlug(vendor, seName, localized.LanguageId);
            }
        }
        protected virtual string ParseVendorAttributes(IFormCollection form)
        {
            if (form == null)
                throw new ArgumentNullException(nameof(form));

            var attributesXml = string.Empty;
            var vendorAttributes = _vendorAttributeService.GetAllVendorAttributes();
            foreach (var attribute in vendorAttributes)
            {
                var controlId = $"{NopAttributePrefixDefaults.Vendor}{attribute.Id}";
                StringValues ctrlAttributes;
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                        ctrlAttributes = form[controlId];
                        if (!StringValues.IsNullOrEmpty(ctrlAttributes))
                        {
                            var selectedAttributeId = int.Parse(ctrlAttributes);
                            if (selectedAttributeId > 0)
                                attributesXml = _vendorAttributeParser.AddVendorAttribute(attributesXml,
                                    attribute, selectedAttributeId.ToString());
                        }

                        break;
                    case AttributeControlType.Checkboxes:
                        var cblAttributes = form[controlId];
                        if (!StringValues.IsNullOrEmpty(cblAttributes))
                        {
                            foreach (var item in cblAttributes.ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                var selectedAttributeId = int.Parse(item);
                                if (selectedAttributeId > 0)
                                    attributesXml = _vendorAttributeParser.AddVendorAttribute(attributesXml,
                                        attribute, selectedAttributeId.ToString());
                            }
                        }

                        break;
                    case AttributeControlType.ReadonlyCheckboxes:
                        //load read-only (already server-side selected) values
                        var attributeValues = _vendorAttributeService.GetVendorAttributeValues(attribute.Id);
                        foreach (var selectedAttributeId in attributeValues
                            .Where(v => v.IsPreSelected)
                            .Select(v => v.Id)
                            .ToList())
                        {
                            attributesXml = _vendorAttributeParser.AddVendorAttribute(attributesXml,
                                attribute, selectedAttributeId.ToString());
                        }

                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        ctrlAttributes = form[controlId];
                        if (!StringValues.IsNullOrEmpty(ctrlAttributes))
                        {
                            var enteredText = ctrlAttributes.ToString().Trim();
                            attributesXml = _vendorAttributeParser.AddVendorAttribute(attributesXml,
                                attribute, enteredText);
                        }

                        break;
                    case AttributeControlType.Datepicker:
                    case AttributeControlType.ColorSquares:
                    case AttributeControlType.ImageSquares:
                    case AttributeControlType.FileUpload:
                    //not supported vendor attributes
                    default:
                        break;
                }
            }

            return attributesXml;
        }
        protected virtual void SaveDiscountMappings(Vendor vendor, VendorModel model)
        {
            var allDiscounts = _discountService.GetAllDiscounts(DiscountType.AssignedToVendor, showHidden: true);

            foreach (var discount in allDiscounts)
            {
                if (model.SelectedDiscountIds != null && model.SelectedDiscountIds.Contains(discount.Id))
                {
                    //new discount
                    if (vendor.DiscountVendorMapping.Count(mapping => mapping.DiscountId == discount.Id) == 0)
                        vendor.DiscountVendorMapping.Add(new DiscountVendorMapping { Discount = discount });
                }
                else
                {
                    //remove discount
                    if (vendor.DiscountVendorMapping.Count(mapping => mapping.DiscountId == discount.Id) > 0)
                    {
                        vendor.DiscountVendorMapping
                            .Remove(vendor.DiscountVendorMapping.FirstOrDefault(mapping => mapping.DiscountId == discount.Id));
                    }
                }
            }
             _vendorService.UpdateVendor(vendor);
            //_productService.UpdateHasDiscountsApplied(product);
        }

        #endregion

        #region Vendors

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            //prepare model
            var model = _vendorModelFactory.PrepareVendorSearchModel(new VendorSearchModel());
            model.IsVendor = _workContext.IsVendorRole;
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(VendorSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _vendorModelFactory.PrepareVendorListModel(searchModel);

            return Json(model);
        }

        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            if (_workContext.IsVendorRole)
                return AccessDeniedView();

            //prepare model
            var model = _vendorModelFactory.PrepareVendorModel(new VendorModel(), null);

            //StoreId  
            int storeId = 0;
            if (_workContext.IsAdminRole)
            {
                storeId = _storeContext.CurrentStore.Id;
            }
            else if (_workContext.IsStoreOwnerRole)
            {
                storeId = _storeService.GetAllStores().FirstOrDefault(s => s.StoreOwnerId == _workContext.CurrentCustomer.Id).Id;
            }
            else if (_workContext.IsVendorRole)
            {
                storeId = _storeContext.CurrentStore.Id;
            }
            else if (_workContext.SubAdmin.IsSubAdminRole)
                storeId = _workContext.GetCurrentStoreId;

            PrepareStoreIdModel(model);
            model.TimeZoneId = _settingService.GetSetting("datetimesettings.storetimezoneid", storeId)?.Value;
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public virtual IActionResult Create(VendorModel model, bool continueEditing, IFormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            //parse vendor attributes
            var vendorAttributesXml = ParseVendorAttributes(form);
            _vendorAttributeParser.GetAttributeWarnings(vendorAttributesXml).ToList()
                .ForEach(warning => ModelState.AddModelError(string.Empty, warning));
            //StoreId            
            PrepareStoreIdModel(model);

            if (ModelState.IsValid)
            {
                var vendor = model.ToEntity<Vendor>();
                if (vendor != null)
                {
                    if (model.IsAllDay)
                        vendor.AvailableType = (int)AvailableTypeEnum.AllDay;
                    if (model.IsCustom)
                        vendor.AvailableType = (int)AvailableTypeEnum.Custom;
                    if (vendor.ExpDeliveryTime == null)
                        vendor.ExpDeliveryTime = "";
                    if (vendor.CostFor == null)
                        vendor.CostFor = "";
                }
                _vendorService.InsertVendor(vendor);

                //activity log
                _customerActivityService.InsertActivity("AddNewVendor",
                    string.Format(_localizationService.GetResource("ActivityLog.AddNewVendor"), vendor.Id), vendor);

                //search engine name
                model.SeName = _urlRecordService.ValidateSeName(vendor, model.SeName, vendor.Name, true);
                _urlRecordService.SaveSlug(vendor, model.SeName, 0);

                //address
                var address = model.Address.ToEntity<Address>();
                address.CreatedOnUtc = DateTime.UtcNow;

                //some validation
                if (address.CountryId == 0)
                    address.CountryId = null;
                if (address.StateProvinceId == 0)
                    address.StateProvinceId = null;
                _addressService.InsertAddress(address);
                vendor.AddressId = address.Id;
                _vendorService.UpdateVendor(vendor);

                //vendor attributes
                _genericAttributeService.SaveAttribute(vendor, NopVendorDefaults.VendorAttributes, vendorAttributesXml);
                _genericAttributeService.SaveAttribute(vendor, NopVendorDefaults.TimeZoneId, model.TimeZoneId);
                // vendor.Email
                // _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.TimeZoneIdAttribute, model.TimeZoneId, customer.RegisteredInStoreId);

                //locales
                UpdateLocales(vendor, model);

                //update picture seo file name
                UpdatePictureSeoNames(vendor);
                
                //update discount
                if (model.DiscountId > 0)
                {
                    model.SelectedDiscountIds.Add(model.DiscountId);
                }

                SaveDiscountMappings(vendor, model);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Vendors.Added"));

                if (!continueEditing)
                    return RedirectToAction("List");

                return RedirectToAction("Edit", new { id = vendor.Id });
            }

            //prepare model
            model = _vendorModelFactory.PrepareVendorModel(model, null, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            //try to get a vendor with the specified id
            var vendor = _vendorService.GetVendorById(id);
            if (vendor == null || vendor.Deleted)
                return RedirectToAction("List");

            //prepare model
            var model = _vendorModelFactory.PrepareVendorModel(null, vendor);
            //StoreId            
            PrepareStoreIdModel(model);
            model.TimeZoneId = _genericAttributeService.GetAttribute<string>(vendor, NopVendorDefaults.TimeZoneId);
            if (string.IsNullOrEmpty(model.TimeZoneId))
            {
                model.TimeZoneId = _settingService.GetSetting("datetimesettings.storetimezoneid", model.StoreId)?.Value;
            }
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(VendorModel model, bool continueEditing, IFormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            //try to get a vendor with the specified id
            var vendor = _vendorService.GetVendorById(model.Id);
            if (vendor == null || vendor.Deleted)
                return RedirectToAction("List");

            //parse vendor attributes
            var vendorAttributesXml = ParseVendorAttributes(form);
            _vendorAttributeParser.GetAttributeWarnings(vendorAttributesXml).ToList()
                .ForEach(warning => ModelState.AddModelError(string.Empty, warning));

            //StoreId            
            PrepareStoreIdModel(model);

            if (ModelState.IsValid)
            {
                var prevPictureId = vendor.PictureId;
                vendor = model.ToEntity(vendor);
                if (model.IsAllDay)
                    vendor.AvailableType = (int)AvailableTypeEnum.AllDay;
                if (model.IsCustom)
                    vendor.AvailableType = (int)AvailableTypeEnum.Custom;
                if (vendor != null)
                {
                    if (vendor.ExpDeliveryTime == null)
                        vendor.ExpDeliveryTime = "";
                    if (vendor.CostFor == null)
                        vendor.CostFor = "";
                }
                _vendorService.UpdateVendor(vendor);

                //vendor attributes
                _genericAttributeService.SaveAttribute(vendor, NopVendorDefaults.VendorAttributes, vendorAttributesXml);
                _genericAttributeService.SaveAttribute(vendor, NopVendorDefaults.TimeZoneId, model.TimeZoneId);
                //activity log
                _customerActivityService.InsertActivity("EditVendor",
                    string.Format(_localizationService.GetResource("ActivityLog.EditVendor"), vendor.Id), vendor);

                //search engine name
                model.SeName = _urlRecordService.ValidateSeName(vendor, model.SeName, vendor.Name, true);
                _urlRecordService.SaveSlug(vendor, model.SeName, 0);

                //address
                var address = _addressService.GetAddressById(vendor.AddressId);
                if (address == null)
                {
                    address = model.Address.ToEntity<Address>();
                    address.CreatedOnUtc = DateTime.UtcNow;

                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;

                    _addressService.InsertAddress(address);
                    vendor.AddressId = address.Id;
                    _vendorService.UpdateVendor(vendor);
                }
                else
                {
                    address = model.Address.ToEntity(address);

                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;

                    _addressService.UpdateAddress(address);
                }

                //locales
                UpdateLocales(vendor, model);

                //delete an old picture (if deleted or updated)
                if (prevPictureId > 0 && prevPictureId != vendor.PictureId)
                {
                    var prevPicture = _pictureService.GetPictureById(prevPictureId);
                    if (prevPicture != null)
                        _pictureService.DeletePicture(prevPicture);
                }
                //update picture seo file name
                UpdatePictureSeoNames(vendor);

                //update discount
                if (model.DiscountId>0)
                {
                    model.SelectedDiscountIds.Add(model.DiscountId);
                }
                SaveDiscountMappings(vendor, model);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Vendors.Updated"));

                if (!continueEditing)
                    return RedirectToAction("List");

                return RedirectToAction("Edit", new { id = vendor.Id });
            }

            //prepare model
            model = _vendorModelFactory.PrepareVendorModel(model, vendor, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            //try to get a vendor with the specified id
            var vendor = _vendorService.GetVendorById(id);
            if (vendor == null)
                return RedirectToAction("List");

            //clear associated customer references
            var associatedCustomers = _customerService.GetAllCustomers(vendorId: vendor.Id);
            foreach (var customer in associatedCustomers)
            {
                customer.VendorId = 0;
                _customerService.UpdateCustomer(customer);
            }

            //delete a vendor
            _vendorService.DeleteVendor(vendor);

            //activity log
            _customerActivityService.InsertActivity("DeleteVendor",
                string.Format(_localizationService.GetResource("ActivityLog.DeleteVendor"), vendor.Id), vendor);

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Vendors.Deleted"));

            return RedirectToAction("List");
        }

        #endregion

        #region Vendor notes

        [HttpPost]
        public virtual IActionResult VendorNotesSelect(VendorNoteSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedDataTablesJson();

            //try to get a vendor with the specified id
            var vendor = _vendorService.GetVendorById(searchModel.VendorId)
                ?? throw new ArgumentException("No vendor found with the specified id");

            //prepare model
            var model = _vendorModelFactory.PrepareVendorNoteListModel(searchModel, vendor);

            return Json(model);
        }

        public virtual IActionResult VendorNoteAdd(int vendorId, string message)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            //try to get a vendor with the specified id
            var vendor = _vendorService.GetVendorById(vendorId);
            if (vendor == null)
                return ErrorJson("Vendor cannot be loaded");

            if (string.IsNullOrEmpty(message))
                return ErrorJson(_localizationService.GetResource("Admin.Vendors.VendorNotes.Fields.Note.Validation"));

            var vendorNote = new VendorNote
            {
                Note = message,
                CreatedOnUtc = DateTime.UtcNow
            };
            vendor.VendorNotes.Add(vendorNote);
            _vendorService.UpdateVendor(vendor);

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult VendorNoteDelete(int id, int vendorId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            //try to get a vendor with the specified id
            var vendor = _vendorService.GetVendorById(vendorId)
                ?? throw new ArgumentException("No vendor found with the specified id", nameof(vendorId));

            //try to get a vendor note with the specified id
            var vendorNote = vendor.VendorNotes.FirstOrDefault(vn => vn.Id == id)
                ?? throw new ArgumentException("No vendor note found with the specified id", nameof(id));

            _vendorService.DeleteVendorNote(vendorNote);

            return new NullJsonResult();
        }

        #endregion

        #region Custom code from v4.0
        //For StoreIds
        protected virtual void PrepareStoreIdModel(VendorModel model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            //model.AvailabeStoreId.Add(new SelectListItem
            //{
            //    Text = "---",
            //    Value = "0"
            //});

            int customerId = _workContext.CurrentCustomer.Id;
            List<int> rollIds = _workContext.CurrentCustomer.CustomerRoles.Select(x => x.Id).ToList();


            //var Cust = Nop.Web.Areas.Admin.Models.Customers.CustomerListModel ;

            //var customer = EngineContext.Current.Resolve<IWorkContext>.CurrentCustomer;
            //var customerId = customer.Id;

            //int id = NopContext.Current.User != null ? NopContext.Current.User.CustomerId : 0;

            var allRoles = _customerService.GetAllCustomerRoles(true).Where(x => rollIds.Contains(x.Id));

            //List<int> rollIds = new List<int>();
            //rollIds.Add(999999);
            bool AdmFlag = false;
            bool StoreFlag = false;
            foreach (var role in allRoles)
            {
                if (role.Name == "Store Owner")
                {
                    StoreFlag = true;
                }
                if (role.Name == "Administrators")
                {
                    AdmFlag = true;
                }
            }

            if (StoreFlag)
            {
                var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == customerId);
                foreach (var store in stores)
                {
                    model.AvailabeStoreId.Add(new SelectListItem
                    {
                        Text = store.Name + " (" + store.Url + ")",
                        Value = store.Id.ToString()
                    });
                }
            }

            if (_workContext.SubAdmin.IsSubAdminRole)
            {
                var stores = _storeService.GetAllStores().Where(x => x.Id == _workContext.GetCurrentStoreId);
                foreach (var store in stores)
                {
                    model.AvailabeStoreId.Add(new SelectListItem
                    {
                        Text = store.Name + " (" + store.Url + ")",
                        Value = store.Id.ToString()
                    });
                }
            }

            if (AdmFlag)
            {

                model.AvailabeStoreId.Add(new SelectListItem
                {
                    Text = "---",
                    Value = "0"
                });

                var stores = _storeService.GetAllStores();
                foreach (var store in stores)
                {
                    model.AvailabeStoreId.Add(new SelectListItem
                    {
                        Text = store.Name + " (" + store.Url + ")",
                        Value = store.Id.ToString()
                    });
                }
            }

            //Add Weekdays
            Dictionary<string, int> Weekdays = new Dictionary<string, int>()
            {
                { "None", 0 }, { "Monday", 1 }, { "Tuesday", 2 }, { "Wednesday", 3 }
                , { "Thursday", 4 }, { "Friday", 5 }, { "Saturday", 6 }, { "Sunday", 7 }
            };

            foreach (KeyValuePair<string, int> Weekday in Weekdays)
            {
                model.AvailabeWeekDays.Add(new SelectListItem
                {
                    Text = Weekday.Key.ToString(),
                    Value = Weekday.Value.ToString()
                });
            }


            //int[] searchOwnerRoleIds = rollIds.ToArray(); // new int[] { 1, 5, 6 };

            //var storeOwnerIds = _storeService.GetAllStores().Select(x => x.StoreOwnerId).Distinct().ToList();

            //var customersIds = _customerService.GetAllCustomers()
            //    .Where(e => !storeOwnerIds.Contains(e.Id))
            //    .Select(x => x.Id).ToList();


            //var customers = _customerService.GetAllCustomers(
            //    customerRoleIds: searchOwnerRoleIds
            //    ).Where(e => customersIds.Contains(e.Id));


        }

        public virtual IActionResult VendorCustomScheduleList(int vendorId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            var vendor = _vendorService.GetVendorById(vendorId);
            if (vendor == null)
                throw new ArgumentException("No vendor found with the specified id");

            var data = _vendorService.GetVendorScheduleByScheduleId(vendor.Id, (int)AvailableTypeEnum.Custom);
            var vendorSchedules = new PagedList<VendorSchedule>(data.AsQueryable(), 0, 100000);

            var searchModel = new VendorScheduleListSearchModel();
            searchModel.SetGridPageSize();

            var model = new VendorScheduleListModel().PrepareToGrid(searchModel, vendorSchedules, () =>
            {
                return vendorSchedules.Select(x =>
                {
                    var m = new VendorModel.VendorScheduleModel
                    {
                        Id = x.Id,
                        ScheduleName = x.ScheduleName,
                        ScheduleFromTimeTS = x.ScheduleFromTime,
                        ScheduleToTimeTS = x.ScheduleToTime,
                        DisplayOrder = x.DisplayOrder,
                        ScheduleDay = x.AvailableOn
                    };
                    DateTime time = DateTime.Today.Add(m.ScheduleFromTimeTS);
                    m.ScheduleFromTime = time.ToString("hh:mm tt");
                    DateTime time2 = DateTime.Today.Add(m.ScheduleToTimeTS);
                    m.ScheduleToTime = time2.ToString("hh:mm tt");
                    m.ScheduleDayS = Enum.GetName(typeof(AvailableDaysEnum), m.ScheduleDay);
                    return m;
                });
            });

            return Json(model);
        }
        public virtual IActionResult VendorAllDayScheduleList(int vendorId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            var vendor = _vendorService.GetVendorById(vendorId);
            if (vendor == null)
                throw new ArgumentException("No vendor found with the specified id");

            var data = _vendorService.GetVendorScheduleByScheduleId(vendor.Id, (int)AvailableTypeEnum.AllDay);
            var vendorSchedules = new PagedList<VendorSchedule>(data.AsQueryable(), 0, 100000);

            var searchModel = new VendorScheduleListSearchModel();
            searchModel.SetGridPageSize();

            var model = new VendorScheduleListModel().PrepareToGrid(searchModel, vendorSchedules, () =>
            {
                return vendorSchedules.Select(x =>
                {
                    var m = new VendorModel.VendorScheduleModel
                    {
                        Id = x.Id,
                        ScheduleName = x.ScheduleName,
                        ScheduleFromTimeTS = x.ScheduleFromTime,
                        ScheduleToTimeTS = x.ScheduleToTime,
                        DisplayOrder = x.DisplayOrder,
                        ScheduleDay = x.AvailableOn
                    };
                    DateTime time = DateTime.Today.Add(m.ScheduleFromTimeTS);
                    m.ScheduleFromTime = time.ToString("hh:mm tt");
                    DateTime time2 = DateTime.Today.Add(m.ScheduleToTimeTS);
                    m.ScheduleToTime = time2.ToString("hh:mm tt");
                    m.ScheduleDayS = Enum.GetName(typeof(AvailableDaysEnum), m.ScheduleDay);

                    return m;
                });
            });


            return Json(model);
        }
        public virtual IActionResult VendorCustomScheduleDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            var VendorSchedule = _vendorService.GetVendorScheduleById(id);
            if (VendorSchedule == null)
                throw new ArgumentException("No Vendor Schedule mapping found with the specified id");

            //var categoryId = productCategory.CategoryId;
            _vendorService.DeleteVendorScheduleById(VendorSchedule);

            return new NullJsonResult();
        }
        public virtual IActionResult VendorAllDayScheduleDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            var VendorSchedule = _vendorService.GetVendorScheduleById(id);
            if (VendorSchedule == null)
                throw new ArgumentException("No Vendor Schedule mapping found with the specified id");

            //var categoryId = productCategory.CategoryId;
            _vendorService.DeleteVendorScheduleById(VendorSchedule);

            return new NullJsonResult();
        }
        public virtual IActionResult CustomDayPopup(int vendorId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            var model = new VendorModel.VendorScheduleModel();
            model.AvailableDays = Enum.GetValues(typeof(AvailableDaysEnum)).Cast<AvailableDaysEnum>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            foreach (var s in _storeService.GetAllAvailableTiming())
            {
                model.AvailableFromTime.Add(new SelectListItem { Text = s.ToString(), Value = s.ToString() });
            }
            foreach (var s in _storeService.GetAllAvailableTiming())
            {
                model.AvailableToTime.Add(new SelectListItem { Text = s.ToString(), Value = s.ToString() });
            }
            model.VendorId = vendorId;
            return View(model);
        }
        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult CustomDayPopup(VendorModel.VendorScheduleModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();
            DateTime dateTimetoConvert = DateTime.ParseExact(model.ScheduleFromTime,
                                   "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanFrom = dateTimetoConvert.TimeOfDay;
            DateTime dateTimetoConvertTo = DateTime.ParseExact(model.ScheduleToTime,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            var result = _vendorService.InsertVendorSchedule(
                                new VendorSchedule
                                {
                                    VendorId = model.VendorId,
                                    Deleted = false,
                                    AvailableType = (int)AvailableTypeEnum.Custom,
                                    DisplayOrder = 1,
                                    AvailableOn = model.ScheduleDay,
                                    ScheduleName = model.ScheduleName,
                                    ScheduleFromTime = dateTimetoConvert.TimeOfDay,
                                    ScheduleToTime = dateTimetoConvertTo.TimeOfDay
                                });
            //ScheduleProductList(model.CategoryId);
            ViewBag.RefreshPage = true;


            return View(model);
        }
        public virtual IActionResult AllDayAddPopup(int vendorId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            var model = new VendorModel.VendorScheduleModel();
            model.AvailableDays = Enum.GetValues(typeof(AvailableDaysEnum)).Cast<AvailableDaysEnum>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            foreach (var s in _storeService.GetAllAvailableTiming())
            {
                model.AvailableFromTime.Add(new SelectListItem { Text = s.ToString(), Value = s.ToString() });
            }
            foreach (var s in _storeService.GetAllAvailableTiming())
            {
                model.AvailableToTime.Add(new SelectListItem { Text = s.ToString(), Value = s.ToString() });
            }
            model.VendorId = vendorId;
            return View(model);
        }
        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult AllDayAddPopup(VendorModel.VendorScheduleModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();
            DateTime dateTimetoConvert = DateTime.ParseExact(model.ScheduleFromTime,
                                   "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanFrom = dateTimetoConvert.TimeOfDay;
            DateTime dateTimetoConvertTo = DateTime.ParseExact(model.ScheduleToTime,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            var result = _vendorService.InsertVendorSchedule(
                                new VendorSchedule
                                {
                                    VendorId = model.VendorId,
                                    Deleted = false,
                                    AvailableType = (int)AvailableTypeEnum.AllDay,
                                    DisplayOrder = 1,
                                    //  AvailableOn = (int)AvailableDaysEnum.AllDay,
                                    ScheduleName = model.ScheduleName,
                                    ScheduleFromTime = dateTimetoConvert.TimeOfDay,
                                    ScheduleToTime = dateTimetoConvertTo.TimeOfDay
                                });
            //ScheduleProductList(model.CategoryId);
            ViewBag.RefreshPage = true;


            return View(model);
        }
        public virtual IActionResult EditAllDayVendorScheduleMapping(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();
            var VendorSchedule = _vendorService.GetVendorScheduleById(id);
            if (VendorSchedule == null)
                throw new ArgumentException("No Vendor Schedule mapping found with the specified id");
            var model = new VendorModel.VendorScheduleModel();
            model.ScheduleName = VendorSchedule.ScheduleName;
            model.ScheduleDay = VendorSchedule.AvailableOn;
            DateTime time = DateTime.Today.Add(VendorSchedule.ScheduleFromTime);
            model.ScheduleFromTime = time.ToString("h:mm tt");

            DateTime time2 = DateTime.Today.Add(VendorSchedule.ScheduleToTime);
            model.ScheduleToTime = time2.ToString("h:mm tt");
            model.AvailableDays = Enum.GetValues(typeof(AvailableDaysEnum)).Cast<AvailableDaysEnum>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            foreach (var s in _storeService.GetAllAvailableTiming())
            {
                model.AvailableFromTime.Add(new SelectListItem { Text = s.ToString(), Value = s.ToString() });
            }
            foreach (var s in _storeService.GetAllAvailableTiming())
            {
                model.AvailableToTime.Add(new SelectListItem { Text = s.ToString(), Value = s.ToString() });
            }
            return View(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult EditAllDayVendorScheduleMapping(VendorModel.VendorScheduleModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();
            var VendorSchedule = _vendorService.GetVendorScheduleById(model.Id);
            if (VendorSchedule == null)
                throw new ArgumentException("No Vendor Schedule mapping found with the specified id");
            DateTime dateTimetoConvert = DateTime.ParseExact(model.ScheduleFromTime,
                                   "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanFrom = dateTimetoConvert.TimeOfDay;
            DateTime dateTimetoConvertTo = DateTime.ParseExact(model.ScheduleToTime,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            VendorSchedule.AvailableType = (int)AvailableTypeEnum.AllDay;
            VendorSchedule.DisplayOrder = 1;
            VendorSchedule.ScheduleName = model.ScheduleName;
            VendorSchedule.ScheduleFromTime = dateTimetoConvert.TimeOfDay;
            VendorSchedule.ScheduleToTime = dateTimetoConvertTo.TimeOfDay;
            var result = _vendorService.UpdateVendorSchedule(VendorSchedule

                                );
            //ScheduleProductList(model.CategoryId);
            ViewBag.RefreshPage = true;


            return View(model);
        }
        public virtual IActionResult EditCustomVendorScheduleMapping(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();
            var VendorSchedule = _vendorService.GetVendorScheduleById(id);
            if (VendorSchedule == null)
                throw new ArgumentException("No Vendor Schedule mapping found with the specified id");
            var model = new VendorModel.VendorScheduleModel();
            model.ScheduleName = VendorSchedule.ScheduleName;
            model.ScheduleDay = VendorSchedule.AvailableOn;
            DateTime time = DateTime.Today.Add(VendorSchedule.ScheduleFromTime);
            model.ScheduleFromTime = time.ToString("h:mm tt");

            DateTime time2 = DateTime.Today.Add(VendorSchedule.ScheduleToTime);
            model.ScheduleToTime = time2.ToString("h:mm tt");
            model.AvailableDays = Enum.GetValues(typeof(AvailableDaysEnum)).Cast<AvailableDaysEnum>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            foreach (var s in _storeService.GetAllAvailableTiming())
            {
                model.AvailableFromTime.Add(new SelectListItem { Text = s.ToString(), Value = s.ToString() });
            }
            foreach (var s in _storeService.GetAllAvailableTiming())
            {
                model.AvailableToTime.Add(new SelectListItem { Text = s.ToString(), Value = s.ToString() });
            }
            return View(model);
        }
        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult EditCustomVendorScheduleMapping(VendorModel.VendorScheduleModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();
            var VendorSchedule = _vendorService.GetVendorScheduleById(model.Id);
            if (VendorSchedule == null)
                throw new ArgumentException("No Vendor Schedule mapping found with the specified id");
            DateTime dateTimetoConvert = DateTime.ParseExact(model.ScheduleFromTime,
                                   "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanFrom = dateTimetoConvert.TimeOfDay;
            DateTime dateTimetoConvertTo = DateTime.ParseExact(model.ScheduleToTime,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            VendorSchedule.AvailableType = (int)AvailableTypeEnum.Custom;
            VendorSchedule.DisplayOrder = 1;
            VendorSchedule.AvailableOn = model.ScheduleDay;
            VendorSchedule.ScheduleName = model.ScheduleName;
            VendorSchedule.ScheduleFromTime = dateTimetoConvert.TimeOfDay;
            VendorSchedule.ScheduleToTime = dateTimetoConvertTo.TimeOfDay;
            var result = _vendorService.UpdateVendorSchedule(VendorSchedule

                                );
            //ScheduleProductList(model.CategoryId);
            ViewBag.RefreshPage = true;


            return View(model);
        }
        #endregion

        #region Vendor slider pictures

        public virtual IActionResult VendorSliderPictureAdd(int pictureId, int displayOrder,
            string overrideAltAttribute, string overrideTitleAttribute, int vendorId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            if (pictureId == 0)
                throw new ArgumentException();

            //try to get a vendor with the specified id
            var vendor = _vendorService.GetVendorById(vendorId)
                ?? throw new ArgumentException("No vendor found with the specified id");

            if (_vendorService.GetVendorSliderPicturesByVendorId(vendorId).Any(p => p.PictureId == pictureId))
                return Json(new { Result = false });

            //try to get a picture with the specified id
            var picture = _pictureService.GetPictureById(pictureId)
                ?? throw new ArgumentException("No picture found with the specified id");

            _pictureService.UpdatePicture(picture.Id,
                _pictureService.LoadPictureBinary(picture),
                picture.MimeType,
                picture.SeoFilename,
                overrideAltAttribute,
                overrideTitleAttribute);

            _pictureService.SetSeoFilename(pictureId, _pictureService.GetPictureSeName(vendor.Name));

            _vendorService.InsertVendorSliderPicture(new VendorSliderPicture
            {
                PictureId = pictureId,
                VendorId = vendorId,
                DisplayOrder = displayOrder
            });

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult VendorSliderPictureList(VendorSliderPictureSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedDataTablesJson();

            //try to get a vendor with the specified id
            var vendor = _vendorService.GetVendorById(searchModel.VendorId)
                ?? throw new ArgumentException("No vendor found with the specified id");

            //prepare model
            var model = _vendorModelFactory.PrepareVendorSliderPictureListModel(searchModel, vendor);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult VendorSliderPictureUpdate(VendorSliderPictureModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            //try to get a vendor selider picture with the specified id
            var vendorSliderPicture = _vendorService.GetVendorSliderPictureById(model.Id)
                ?? throw new ArgumentException("No vendor selider picture found with the specified id");

            //try to get a picture with the specified id
            var picture = _pictureService.GetPictureById(vendorSliderPicture.PictureId)
                ?? throw new ArgumentException("No picture found with the specified id");

            _pictureService.UpdatePicture(picture.Id,
                _pictureService.LoadPictureBinary(picture),
                picture.MimeType,
                picture.SeoFilename,
                model.OverrideAltAttribute,
                model.OverrideTitleAttribute);

            vendorSliderPicture.DisplayOrder = model.DisplayOrder;
            _vendorService.UpdateVendorSliderPicture(vendorSliderPicture);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult VendorSliderPictureDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            //try to get a vendor slider picture with the specified id
            var vendorSliderPicture = _vendorService.GetVendorSliderPictureById(id)
                ?? throw new ArgumentException("No vendor slider picture found with the specified id");

            var pictureId = vendorSliderPicture.PictureId;
            _vendorService.DeleteVendorSliderPicture(vendorSliderPicture);

            //try to get a picture with the specified id
            var picture = _pictureService.GetPictureById(pictureId)
                ?? throw new ArgumentException("No picture found with the specified id");

            _pictureService.DeletePicture(picture);

            return new NullJsonResult();
        }

        #endregion
    }
}