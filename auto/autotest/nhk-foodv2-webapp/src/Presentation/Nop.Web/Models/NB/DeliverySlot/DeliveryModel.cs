﻿using System;
using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.NB
{
    /// <summary>
    /// Represents a Delivery model
    /// </summary>
    public partial class DeliveryModel : BaseNopEntityModel
    {
        public DeliveryModel() 
        {
            DeliverySlotList = new List<DeliverySlotModel>();
            DeliveryDaysList = new List<DeliveryDaysModel>();
        }

        public IList<DeliverySlotModel> DeliverySlotList { get; set; }
        public IList<DeliveryDaysModel> DeliveryDaysList { get; set; }

        public DateTime ActiveSlotDate { get; set; }

        public int SlotId { get; set; }

        public string SlotDate { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }
    }
}