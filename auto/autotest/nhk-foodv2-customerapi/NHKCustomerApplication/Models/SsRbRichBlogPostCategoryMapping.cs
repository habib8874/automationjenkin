﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsRbRichBlogPostCategoryMapping
    {
        public int Id { get; set; }
        public int BlogPostId { get; set; }
        public int CategoryId { get; set; }
        public bool LimitedToStores { get; set; }
    }
}
