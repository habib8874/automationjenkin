using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class ProductCuisineMap : NopEntityTypeConfiguration<ProductCuisine>
    {
        public override void Configure(EntityTypeBuilder<ProductCuisine> builder)
        {
            builder.ToTable(nameof(ProductCuisine));
            builder.HasKey(pt => pt.Id);
            builder.Property(pt => pt.Name).IsRequired().HasMaxLength(400);
            builder.Property(pt => pt.StoreId).IsRequired();

            base.Configure(builder);
        }
    }
}