﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB.Package
{
    /// <summary>
    /// Represents a package plan list model
    /// </summary>
    public partial class PackagePlanListModel : BasePagedListModel<PackagePlanModel>
    {
    }
}