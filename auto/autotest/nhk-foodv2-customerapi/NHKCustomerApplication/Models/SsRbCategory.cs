﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsRbCategory
    {
        public int Id { get; set; }
        public int LanguageId { get; set; }
        public string Name { get; set; }
        public int DisplayOrder { get; set; }
        public bool Published { get; set; }
        public string Seotitle { get; set; }
        public string Seodescription { get; set; }
        public string Seokeywords { get; set; }
        public string Sename { get; set; }
        public bool LimitedToStores { get; set; }
    }
}
