﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsMmMenu
    {
        public SsMmMenu()
        {
            SsMmMenuItem = new HashSet<SsMmMenuItem>();
        }

        public int Id { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }
        public string CssClass { get; set; }
        public bool ShowDropdownsOnClick { get; set; }
        public bool LimitedToStores { get; set; }

        public virtual ICollection<SsMmMenuItem> SsMmMenuItem { get; set; }
    }
}
