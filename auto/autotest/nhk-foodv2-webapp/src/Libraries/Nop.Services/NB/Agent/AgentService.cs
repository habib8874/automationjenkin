﻿using System;
using System.Linq;
using Nop.Core.Data;
using Nop.Core.Domain.NB.Agent;

namespace Nop.Services.NB.Agent
{
    /// <summary>
    /// Agent service class
    /// </summary>
    public class AgentService : IAgentService
    {
        #region Fields

        private readonly IRepository<AgentOrderGeoLocation> _agentGeoLocationRepository;

        #endregion

        #region Ctor

        public AgentService(IRepository<AgentOrderGeoLocation> agentGeoLocationRepository)
        {
            _agentGeoLocationRepository = agentGeoLocationRepository;
        }

        #endregion

        public AgentOrderGeoLocation GetAgentOrderGeoLocation(int agentId, int orderId)
        {
            return _agentGeoLocationRepository.Table.FirstOrDefault(a => a.AgentId == agentId && a.OderId == orderId);
        }
    }
}
