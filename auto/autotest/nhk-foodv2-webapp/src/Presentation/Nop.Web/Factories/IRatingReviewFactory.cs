﻿using Nop.Core.Domain.Orders;
using Nop.Web.Models.RatingReview;

namespace Nop.Web.Factories
{
    /// <summary>
    /// Represents the interface of the rating review model factory
    /// </summary>
    public partial interface IRatingReviewFactory
    {
        /// <summary>
        /// get the rating review model by review type
        /// </summary>
        /// <param name="reviewType">reviewType name</param>
        /// <returns>ratingReview model</returns>
        RatingReviewModel PrepareRatingReviewModel(int reviewType, Order order, int directItemId = 0);


        /// <summary>
        /// prepare product rating details model
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        ProductRatingsDetailsModel PrepareProductRatingsDetailsModel(int productId);
    }
}
