﻿using Microsoft.AspNetCore.SignalR;
using Nop.Core.Domain.Orders;
using Nop.Services.Events;
using Nop.Services.NB.SignalR;
using Nop.Services.Orders;

namespace Nop.Services.NB.Events
{
    /// <summary>
    /// Represents NB event consumer
    /// </summary>
    public class NBEventConsumer :
        IConsumer<OrderPlacedEvent>
    {
        #region Fields

        private readonly IHubContext<SignalRHub> _signalRHubContext;
        private readonly IOrderService _orderService;

        #endregion

        #region Ctor

        public NBEventConsumer(IHubContext<SignalRHub> signalRHubContext,
            IOrderService orderService)
        {
            _signalRHubContext = signalRHubContext;
            _orderService = orderService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Handle the order placed event
        /// </summary>
        /// <param name="eventMessage">The event message.</param>
        public void HandleEvent(OrderPlacedEvent eventMessage)
        {
            //invoke order placed notify signalR
            _signalRHubContext.Clients.All.SendAsync("orderPlacedNotifyRequest", eventMessage.Order.Id);

            //order place firebase notification
            _orderService.OrderPlacedNotificationtoMerchant(eventMessage.Order);
        }

        #endregion
    }
}
