﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsPrProductRibbon
    {
        public SsPrProductRibbon()
        {
            SsPrCategoryPageRibbon = new HashSet<SsPrCategoryPageRibbon>();
            SsPrProductPageRibbon = new HashSet<SsPrProductPageRibbon>();
        }

        public int Id { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }
        public bool StopAddingRibbonsAftherThisOneIsAdded { get; set; }
        public int Priority { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool LimitedToStores { get; set; }

        public virtual ICollection<SsPrCategoryPageRibbon> SsPrCategoryPageRibbon { get; set; }
        public virtual ICollection<SsPrProductPageRibbon> SsPrProductPageRibbon { get; set; }
    }
}
