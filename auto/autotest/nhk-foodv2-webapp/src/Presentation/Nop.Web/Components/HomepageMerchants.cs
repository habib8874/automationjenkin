﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Services.Common;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;

namespace Nop.Web.Components
{
    public class HomepageMerchantsViewComponent : NopViewComponent
    {
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly IWorkContext _workContext;
        public HomepageMerchantsViewComponent(ICatalogModelFactory catalogModelFactory,
            IWorkContext workContext)
        {
            _catalogModelFactory = catalogModelFactory;
            _workContext = workContext;
        }

        public IViewComponentResult Invoke(string latlong, int ordertype, bool getOfferMerchant= false)
        {
            var lstAllMerchants = _catalogModelFactory.PrepareMerchantAllModels(getOfferMerchant: getOfferMerchant);
            lstAllMerchants = lstAllMerchants.Where(x => 
                (ordertype == 0 || (ordertype == 1 && x.IsDelivery) || (ordertype == 2 && x.IsTakeAway) || (ordertype == 3 && x.IsDining)) &&
                x.StoreId == _workContext.GetCurrentStoreId).ToList();

            if (!string.IsNullOrEmpty(latlong))
            {
                var commonService = EngineContext.Current.Resolve<ICommonService>();
                var listven = commonService.GetAllVendorByLongLat(latlong, ordertype);
                lstAllMerchants = lstAllMerchants.Where(x => listven.Contains(x.Id)).ToList();
            }

            if (ordertype == 0 && (_workContext.CurrentOrderDetails != null))
            {
                var ordType = _workContext.CurrentOrderDetails.OrderType ?? 1;
                ordertype = ordType;
            }

            if (ordertype > 0)
            {
                TempData["orderdeltype"] = (ordertype == 1);
                TempData["ordertaketype"] = (ordertype == 2);
                TempData["orderdine"] = (ordertype == 3);
                TempData.Keep("orderdeltype");
                TempData.Keep("ordertaketype");
                TempData.Keep("orderdine");
            }

            if (!lstAllMerchants.Any())
                return Content("");

            return View(lstAllMerchants);
        }
    }
}