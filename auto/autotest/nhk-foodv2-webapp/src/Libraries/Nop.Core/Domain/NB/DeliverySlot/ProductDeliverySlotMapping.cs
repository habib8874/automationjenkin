﻿using Nop.Core.Domain.Catalog;

namespace Nop.Core.Domain.NB
{
    /// <summary>
    /// Represents a Product DeliverySlot mapping class
    /// </summary>
    public partial class ProductDeliverySlotMapping : BaseEntity
    {
        /// <summary>
        /// Gets or sets the Product identifier
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the DeliverySlot tag identifier
        /// </summary>
        public int DeliverySlotId { get; set; }

        /// <summary>
        /// Gets or sets the Product
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        /// Gets or sets the DeliverySlot tag
        /// </summary>
        public virtual DeliverySlot DeliverySlot { get; set; }
    }
}