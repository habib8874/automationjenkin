﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Customers
{
    /// <summary>
    /// Represents a customer role search model
    /// </summary>
    public partial class CustomerRoleSearchModel : BaseSearchModel
    {
        public CustomerRoleSearchModel()
        {
            AvailableNBRoles = new List<SelectListItem>();
        }

        #region custom

        [NopResourceDisplayName("NB.Admin.Configuration.Roles.Fields.RoleName")]
        public int SearchNBRoleId { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Roles.Fields.RoleSystemName")]
        public string SearchNBRole { get; set; }

        public IList<SelectListItem> AvailableNBRoles { get; set; }

        #endregion
    }
}