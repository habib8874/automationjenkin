﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Common;

namespace Nop.Data.Mapping.Common
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class AddressTypeMap : NopEntityTypeConfiguration<AddressType>
    {
        public override void Configure(EntityTypeBuilder<AddressType> builder)
        {
            builder.ToTable(nameof(AddressType));
            builder.HasKey(address => address.Id);

            base.Configure(builder);
        }
    }
}
