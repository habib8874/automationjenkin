﻿using NHKCustomerApplication.Models;
using NHKCustomerApplication.Repository;
using NHKCustomerApplication.ViewModel;
using NHKCustomerApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NHKCustomerApplication.Services
{
    public class WalletService : IWalletService
    {
        #region Fields
        private readonly IWalletRepository _walletRepository;
        #endregion


        #region Constructor
        public WalletService(IWalletRepository walletRepo)
        {
            _walletRepository = walletRepo;


        }


        #endregion
        #region Methods
        /// <summary>
        /// GetCurrentBalance
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public decimal GetCurrentBalance(int customerId)
        {


            var transactions = _walletRepository.GetWalletTransactions(customerId);
            return transactions.Where(x => x.Credited).Sum(x => x.Amount) - transactions.Where(x => x.Credited == false).Sum(x => x.Amount);

        }
        /// <summary>
        /// GetHistory
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<WalletRequestModel> GetHistory(int customerId)
        {
            return _walletRepository.GetWalletTransactions(customerId);
        }
        /// <summary>
        /// AddMoney
        /// </summary>
        /// <param name="requestModel"></param>
        public void AddMoney(WalletRequestModel requestModel)
        {
            CustomerWallet wallet = new CustomerWallet();
            wallet.Amount = requestModel.Amount;
            wallet.Credited = true;
            wallet.CustomerId = wallet.TransactionBy = requestModel.CustomerId;
            wallet.OrderId = requestModel.OrderId;
            wallet.FromWeb = false;
            wallet.TransactionOnUtc = DateTime.UtcNow;
            wallet.Note = $"Amount added in wallet on {DateTime.UtcNow}";
            _walletRepository.AddMoneyToWallet(wallet);
        } 
        #endregion


    }
}
