﻿using Nop.Core.Configuration;

namespace Nestorbird.Plugin.Api
{
    public class MerchantAPISettings : ISettings
    {
        public string MerchantAppWebKey { get; set; }
    }
}
