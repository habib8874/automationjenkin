﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Infrastructure;
using Nop.Services.Localization;
using Nop.Services.Media;

namespace Nop.Web.Controllers.NB
{
    public partial class PictureController : BasePublicController
    {
        #region Fields
        private readonly IPictureService _pictureService;
        private readonly ILocalizationService _localizationService;
        private readonly INopFileProvider _fileProvider;
        #endregion

        #region Ctor
        public PictureController(IPictureService pictureService,
            ILocalizationService localizationService,
            INopFileProvider fileProvider)
        {
            _pictureService = pictureService;
            _localizationService = localizationService;
            _fileProvider = fileProvider;
        }

        #endregion

        #region Method
        public virtual ActionResult GetPictureUrl(int pictureId, int size)
        {
            try
            {
                //get picture
                var picture = _pictureService.GetPictureById(pictureId);
                if (picture == null)
                    return Content("");

                //get picture url
                var pictureUrl = _pictureService.GetPictureUrl(picture, size);
                if (string.IsNullOrEmpty(pictureUrl))
                    return Content("");

                //build local path
                var filePath = _fileProvider.GetAbsolutePath("images\\thumbs", pictureUrl.Split('/').LastOrDefault());
                var bytes = _fileProvider.ReadAllBytes(filePath);

                //return file
                return File(bytes, picture.MimeType, string.Format("{0}.{1}", picture.SeoFilename, _pictureService.GetFileExtensionFromMimeType(picture.MimeType)));

            }
            catch (Exception ex)
            {
                return Content("");
            }
        }

        #endregion
    }
}
