﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class VersionLangs
    {
        public int Id { get; set; }
        public string DeviceOstype { get; set; }
        public string Apkversion { get; set; }
        public string VerLang { get; set; }
    }
}
