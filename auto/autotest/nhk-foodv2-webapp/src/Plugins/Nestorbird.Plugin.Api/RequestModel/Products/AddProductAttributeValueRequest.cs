﻿namespace Nestorbird.Plugin.Api.RequestModel.Products
{
    public class AddProductAttributeRequest
    {
        public int ProductAttributeId { get; set; }

        public int ProductId { get; set; }

        public string Name { get; set; }

        public int ControlType { get; set; }

        public bool IsRequired { get; set; }
    }

    public class AddProductAttributeValueRequest
    {
        public int ProductAttributeId { get; set; }

        public int ProductId { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public bool IsPreSelected { get; set; }
    }
}
