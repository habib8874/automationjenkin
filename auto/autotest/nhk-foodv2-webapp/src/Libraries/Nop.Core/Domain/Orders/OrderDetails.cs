﻿using System;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;

namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents a OrderDetails
    /// </summary>
    public partial class OrderDetails : BaseEntity
    {
        public int? StoreId { get; set; }
        public int? CustomerId { get; set; }
        public int? OrderType { get; set; }
        public string OrderNote { get; set; }
        public string EnteredLocation { get; set; }
        public bool IsOrderComplete { get; set; }
        public DateTime CreatedOnUtc { get; set; }

        //Added By Mah 25-05-2019
        public int? OrderTime { get; set; }
        public DateTime? ScheduleDate { get; set; }
        public TimeSpan? ScheduleTime { get; set; }
        public decimal? DeliveryCharges { get; set; }
        public decimal? Tip { get; set; }

    }
}
