﻿using System;
using System.Collections.Generic;
using Nestorbird.Plugin.Api.ResponseModel.Account;
using Nop.Core.Domain.Orders;

namespace Nestorbird.Plugin.Api.ResponseModel.Order
{
    public class OrderDetailResponse
    {
        public OrderDetailResponse()
        {
            OrderItems = new List<OrderItemResponse>();
        }

        public int OrderId { get; set; }

        public string CustomOrderId { get; set; }

        public string OrderGuid { get; set; }

        public string OrderTotal { get; set; }

        public DateTime CreatedOn { get; set; }

        public string CustomerName { get; set; }

        public string OrderType { get; set; }

        public int? OrderTypeId { get; set; }

        public string OrderStatus { get; set; }

        public string PaymentStatus { get; set; }

        public string ShippingStatus { get; set; }

        public OrderStatus OrderStatusEnum { get; set; }

        public string SubTotal { get; set; }

        public string Tax { get; set; }

        public string Discount { get; set; }

        public string ShippingMethod { get; set; }

        public string CurrencyCode { get; set; }

        public decimal OrderTotalAmount { get; set; }

        public string DeliveryCharges { get; set; }

        public string PaymentMethod { get; set; }

        public bool IsAgentAssigned { get; set; }

        public int AgentId { get; set; }

        public string AgentName { get; set; }

        public string AgentStatus { get; set; }

        public int AgentCityId { get; set; }

        public string AgentCityName { get; set; }

        public string ShippingCharges { get; set; }

        public string ServiceCharges { get; set; }

        public AgentStatus AgentStatusEnum { get; set; }

        public IList<OrderItemResponse> OrderItems { get; set; }

        public AddressResponse ShippingAddress { get; set; }

        public AddressResponse BillingAddress { get; set; }
    }
}
