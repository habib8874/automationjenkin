﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.MerchantCategorys
{
    /// <summary>
    /// Represents a MerchantCategory model
    /// </summary>
    public partial class MerchantCategoryModel : BaseNopEntityModel, ILocalizedModel<MerchantCategoryLocalizedModel>
    {
        public MerchantCategoryModel() 
        {
            AvailableStores = new List<SelectListItem>();
            Locales = new List<MerchantCategoryLocalizedModel>();
            MerchantCategoryMerchantSearchModel = new MerchantCategoryMerchantSearchModel();
        }

        [NopResourceDisplayName("NB.Admin.MerchantCategory.Fields.Name")]
        public string Name { get; set; }
        [NopResourceDisplayName("NB.Admin.MerchantCategory.Fields.ShortDescription")]
        public string ShortDescription { get; set; }
        [UIHint("Picture")]
        [NopResourceDisplayName("NB.Admin.MerchantCategory.Fields.PictureId")]
        public int PictureId { get; set; }
        [NopResourceDisplayName("Admin.Catalog.MerchantCategory.Fields.PictureId")]
        public string PictureThumbnailUrl { get; set; }
        [NopResourceDisplayName("NB.Admin.MerchantCategory.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }
        [NopResourceDisplayName("NB.Admin.MerchantCategory.Fields.StoreId")]
        public int StoreId { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the entity is published
        /// </summary>
        [NopResourceDisplayName("NB.Admin.MerchantCategory.Fields.Published")]
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }
        [NopResourceDisplayName("NB.Admin.MerchantCategory.Fields.StoreName")]
        public string StoreName { get; set; }
        [NopResourceDisplayName("NB.Admin.MerchantCategory.Fields.StoreList")]
        public IList<SelectListItem> AvailableStores { get; set; }

        public IList<MerchantCategoryLocalizedModel> Locales { get; set; }

        public MerchantCategoryMerchantSearchModel MerchantCategoryMerchantSearchModel { get; set; }
    }

    public partial class MerchantCategoryLocalizedModel : ILocalizedLocaleModel
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("NB.Admin.MerchantCategory.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("NB.Admin.MerchantCategory.Fields.ShortDescription")]
        public string ShortDescription { get; set; }
    }
}