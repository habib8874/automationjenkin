﻿using Nop.Core.Configuration;

namespace Nop.Core.Domain.NB.Common
{
    public partial class NBWebFCMNotificationSetting : ISettings
    {

        /// <summary>
        /// Gets or sets a value of WebAddress
        /// </summary>
        public string WebAddress { get; set; }

        /// <summary>
        /// Gets or sets a value of WebKey
        /// </summary>
        public string WebKey { get; set; }
    }
}
