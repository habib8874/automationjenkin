﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Tax;
using Nop.Web.Areas.Admin.Models.Common;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Reports
{
    /// <summary>
    /// Represents an AgentEarning model
    /// </summary>
    public partial class AgentEarningModel : BaseNopEntityModel
    {
        #region Ctor

        public AgentEarningModel()
        {
            
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.AgentEarning.Fields.CustomOrderNumber")]
        public string CustomOrderNumber { get; set; }

        //Code for agents start
        [NopResourceDisplayName("Admin.AgentEarning.Fields.AssignedAgentId")]
        public int AssignedAgentId { get; set; }

        [NopResourceDisplayName("Admin.AgentEarning.Fields.AgentName")]
        public string AgentName { get; set; }

        public int CustomerId { get; set; }
        [NopResourceDisplayName("Admin.AgentEarning.Fields.CustomerEmail")]
        public string CustomerEmail { get; set; }

        [NopResourceDisplayName("Admin.AgentEarning.Fields.CustomerName")]
        public string CustomerName { get; set; }

        [NopResourceDisplayName("Admin.AgentEarning.Fields.OrderStatus")]
        public string OrderStatus { get; set; }

        [NopResourceDisplayName("Admin.AgentEarning.Fields.PaymentStatus")]
        public string PaymentStatus { get; set; }

        [NopResourceDisplayName("Admin.AgentEarning.Fields.ItemCost")]
        public string ItemCost { get; set; }

        [NopResourceDisplayName("Admin.AgentEarning.Fields.DeliveryFee")]
        public string DeliveryFee { get; set; }

        [NopResourceDisplayName("Admin.AgentEarning.Fields.Tip")]
        public string Tip { get; set; }
        public decimal TipAmount { get; set; }

        [NopResourceDisplayName("Admin.AgentEarning.Fields.PayableAmount")]
        public string PayableAmount { get; set; }
        public decimal PayableAmountValue { get; set; }


        [NopResourceDisplayName("Admin.AgentEarning.Fields.PaidAmount")]
        public decimal PaidAmount { get; set; }
        public string PaidAmmountStr { get; set; }

        #endregion


    }

}