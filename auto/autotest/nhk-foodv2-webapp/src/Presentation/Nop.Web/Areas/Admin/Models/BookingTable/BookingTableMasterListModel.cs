﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.BookingTable
{
    public partial class BookingTableMasterListSearchModel : BaseSearchModel
    {
        public BookingTableMasterListSearchModel()
        {
            AvailableStores = new List<SelectListItem>();
            AvailableVendors = new List<SelectListItem>();
            AvailableFromTiming = new List<SelectListItem>();
            AvailableToTiming = new List<SelectListItem>();
        }
        public bool IsCurrentVendor { get; set; }
        public int Id { get; set; }
        public int StoreId { get; set; }
        [NopResourceDisplayName("Admin.Booking.List.Store")]
        public int SearchStoreId { get; set; }
        [NopResourceDisplayName("Admin.Booking.List.Vendor")]
        public int SearchVendorId { get; set; }
        [NopResourceDisplayName("Admin.Booking.TableNumber")]
        public int SearchTableNo { get; set; }
        public string Store { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
        public int VendorId { get; set; }
        public string Vendor { get; set; }
        public IList<SelectListItem> AvailableVendors { get; set; }
        public IList<SelectListItem> AvailableFromTiming { get; set; }
        public IList<SelectListItem> AvailableToTiming { get; set; }
        public int TableNo { get; set; }
        public int Seats { get; set; }
        public bool Status { get; set; }
        public decimal Availability { get; set; }
        public DateTime CreatedAt { get; set; }
        public TimeSpan TableAvailableFrom { get; set; }
        public TimeSpan TableAvailableTo { get; set; }
        [NopResourceDisplayName("Admin.Booking.BookingTimeFrom")]
        public string AvailableFrom { get; set; }
        [NopResourceDisplayName("Admin.Booking.BookingTimeTo")]
        public string AvailableTo { get; set; }
        public int PictureId { get; set; }
    }
    public partial class BookingTableMasterGridListModel : BasePagedListModel<BookingTableMasterListModel>
    {
    }
    public partial class BookingTableMasterListModel: BaseNopModel
    {
        public BookingTableMasterListModel()
        {
            AvailableStores = new List<SelectListItem>();
            AvailableVendors = new List<SelectListItem>();
            AvailableFromTiming = new List<SelectListItem>();
            AvailableToTiming = new List<SelectListItem>();
        }
        public bool IsCurrentVendor { get; set; }
        public int Id { get; set; }
        public int StoreId { get; set; }
        [NopResourceDisplayName("Admin.Booking.List.Store")]
        public int SearchStoreId { get; set; }
        [NopResourceDisplayName("Admin.Booking.List.Vendor")]
        public int SearchVendorId { get; set; }
        [NopResourceDisplayName("Admin.Booking.TableNumber")]
        public int SearchTableNo { get; set; }
        public string Store { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
        public int VendorId { get; set; }
        public string Vendor { get; set; }
        public IList<SelectListItem> AvailableVendors { get; set; }
        public IList<SelectListItem> AvailableFromTiming { get; set; }
        public IList<SelectListItem> AvailableToTiming { get; set; }
        public int TableNo { get; set; }
        public int Seats { get; set; }
        public bool Status { get; set; }
        public decimal Availability { get; set; }
        public DateTime CreatedAt { get; set; }
        public TimeSpan TableAvailableFrom { get; set; }
        public TimeSpan TableAvailableTo { get; set; }
        [NopResourceDisplayName("Admin.Booking.BookingTimeFrom")]
        public string AvailableFrom { get; set; }
        [NopResourceDisplayName("Admin.Booking.BookingTimeTo")]
        public string AvailableTo { get; set; }
        [UIHint("Picture")]
        [NopResourceDisplayName("Admin.Vendors.Fields.Picture")]
        public int PictureId { get; set; }
    }
}
