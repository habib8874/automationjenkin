﻿using System.Collections.Generic;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Security;

namespace Nop.Services.Security
{
    /// <summary>
    /// Standard permission provider
    /// </summary>
    public partial class StandardPermissionProvider : IPermissionProvider
    {
        //admin area permissions
        public static readonly PermissionRecord AccessAdminPanel = new PermissionRecord { Name = "Access admin area", SystemName = "AccessAdminPanel", Category = "Standard" };
        public static readonly PermissionRecord AllowCustomerImpersonation = new PermissionRecord { Name = "Admin area. Allow Customer Impersonation", SystemName = "AllowCustomerImpersonation", Category = "Customers" };
        public static readonly PermissionRecord ManageProducts = new PermissionRecord { Name = "Admin area. Manage Products", SystemName = "ManageProducts", Category = "Catalog" };
        public static readonly PermissionRecord ManageCategories = new PermissionRecord { Name = "Admin area. Manage Categories", SystemName = "ManageCategories", Category = "Catalog" };
        public static readonly PermissionRecord ManageManufacturers = new PermissionRecord { Name = "Admin area. Manage Manufacturers", SystemName = "ManageManufacturers", Category = "Catalog" };
        public static readonly PermissionRecord ManageProductReviews = new PermissionRecord { Name = "Admin area. Manage Product Reviews", SystemName = "ManageProductReviews", Category = "Catalog" };
        public static readonly PermissionRecord ManageProductTags = new PermissionRecord { Name = "Admin area. Manage Product Tags", SystemName = "ManageProductTags", Category = "Catalog" };
        public static readonly PermissionRecord ManageAttributes = new PermissionRecord { Name = "Admin area. Manage Attributes", SystemName = "ManageAttributes", Category = "Catalog" };
        public static readonly PermissionRecord ManageCustomers = new PermissionRecord { Name = "Admin area. Manage Customers", SystemName = "ManageCustomers", Category = "Customers" };
        public static readonly PermissionRecord ManageVendors = new PermissionRecord { Name = "Admin area. Manage Vendors", SystemName = "ManageVendors", Category = "Customers" };
        public static readonly PermissionRecord ManageCurrentCarts = new PermissionRecord { Name = "Admin area. Manage Current Carts", SystemName = "ManageCurrentCarts", Category = "Orders" };
        public static readonly PermissionRecord ManageOrders = new PermissionRecord { Name = "Admin area. Manage Orders", SystemName = "ManageOrders", Category = "Orders" };
        public static readonly PermissionRecord ManageRecurringPayments = new PermissionRecord { Name = "Admin area. Manage Recurring Payments", SystemName = "ManageRecurringPayments", Category = "Orders" };
        public static readonly PermissionRecord ManageGiftCards = new PermissionRecord { Name = "Admin area. Manage Gift Cards", SystemName = "ManageGiftCards", Category = "Orders" };
        public static readonly PermissionRecord ManageReturnRequests = new PermissionRecord { Name = "Admin area. Manage Return Requests", SystemName = "ManageReturnRequests", Category = "Orders" };
        public static readonly PermissionRecord OrderCountryReport = new PermissionRecord { Name = "Admin area. Access order country report", SystemName = "OrderCountryReport", Category = "Orders" };
        public static readonly PermissionRecord ManageAffiliates = new PermissionRecord { Name = "Admin area. Manage Affiliates", SystemName = "ManageAffiliates", Category = "Promo" };
        public static readonly PermissionRecord ManageCampaigns = new PermissionRecord { Name = "Admin area. Manage Campaigns", SystemName = "ManageCampaigns", Category = "Promo" };
        public static readonly PermissionRecord ManageDiscounts = new PermissionRecord { Name = "Admin area. Manage Discounts", SystemName = "ManageDiscounts", Category = "Promo" };
        public static readonly PermissionRecord ManageNewsletterSubscribers = new PermissionRecord { Name = "Admin area. Manage Newsletter Subscribers", SystemName = "ManageNewsletterSubscribers", Category = "Promo" };
        public static readonly PermissionRecord ManagePolls = new PermissionRecord { Name = "Admin area. Manage Polls", SystemName = "ManagePolls", Category = "Content Management" };
        public static readonly PermissionRecord ManageNews = new PermissionRecord { Name = "Admin area. Manage News", SystemName = "ManageNews", Category = "Content Management" };
        public static readonly PermissionRecord ManageBlog = new PermissionRecord { Name = "Admin area. Manage Blog", SystemName = "ManageBlog", Category = "Content Management" };
        public static readonly PermissionRecord ManageWidgets = new PermissionRecord { Name = "Admin area. Manage Widgets", SystemName = "ManageWidgets", Category = "Content Management" };
        public static readonly PermissionRecord ManageTopics = new PermissionRecord { Name = "Admin area. Manage Topics", SystemName = "ManageTopics", Category = "Content Management" };
        public static readonly PermissionRecord ManageForums = new PermissionRecord { Name = "Admin area. Manage Forums", SystemName = "ManageForums", Category = "Content Management" };
        public static readonly PermissionRecord ManageMessageTemplates = new PermissionRecord { Name = "Admin area. Manage Message Templates", SystemName = "ManageMessageTemplates", Category = "Content Management" };
        public static readonly PermissionRecord ManageCountries = new PermissionRecord { Name = "Admin area. Manage Countries", SystemName = "ManageCountries", Category = "Configuration" };
        public static readonly PermissionRecord ManageLanguages = new PermissionRecord { Name = "Admin area. Manage Languages", SystemName = "ManageLanguages", Category = "Configuration" };
        public static readonly PermissionRecord ManageSettings = new PermissionRecord { Name = "Admin area. Manage Settings", SystemName = "ManageSettings", Category = "Configuration" };
        public static readonly PermissionRecord ManagePaymentMethods = new PermissionRecord { Name = "Admin area. Manage Payment Methods", SystemName = "ManagePaymentMethods", Category = "Configuration" };
        public static readonly PermissionRecord ManageExternalAuthenticationMethods = new PermissionRecord { Name = "Admin area. Manage External Authentication Methods", SystemName = "ManageExternalAuthenticationMethods", Category = "Configuration" };
        public static readonly PermissionRecord ManageTaxSettings = new PermissionRecord { Name = "Admin area. Manage Tax Settings", SystemName = "ManageTaxSettings", Category = "Configuration" };
        public static readonly PermissionRecord ManageShippingSettings = new PermissionRecord { Name = "Admin area. Manage Shipping Settings", SystemName = "ManageShippingSettings", Category = "Configuration" };
        public static readonly PermissionRecord ManageCurrencies = new PermissionRecord { Name = "Admin area. Manage Currencies", SystemName = "ManageCurrencies", Category = "Configuration" };
        public static readonly PermissionRecord ManageActivityLog = new PermissionRecord { Name = "Admin area. Manage Activity Log", SystemName = "ManageActivityLog", Category = "Configuration" };
        public static readonly PermissionRecord ManageAcl = new PermissionRecord { Name = "Admin area. Manage ACL", SystemName = "ManageACL", Category = "Configuration" };
        public static readonly PermissionRecord ManageEmailAccounts = new PermissionRecord { Name = "Admin area. Manage Email Accounts", SystemName = "ManageEmailAccounts", Category = "Configuration" };
        public static readonly PermissionRecord ManageStores = new PermissionRecord { Name = "Admin area. Manage Stores", SystemName = "ManageStores", Category = "Configuration" };
        public static readonly PermissionRecord ManagePlugins = new PermissionRecord { Name = "Admin area. Manage Plugins", SystemName = "ManagePlugins", Category = "Configuration" };
        public static readonly PermissionRecord ManageSystemLog = new PermissionRecord { Name = "Admin area. Manage System Log", SystemName = "ManageSystemLog", Category = "Configuration" };
        public static readonly PermissionRecord ManageMessageQueue = new PermissionRecord { Name = "Admin area. Manage Message Queue", SystemName = "ManageMessageQueue", Category = "Configuration" };
        public static readonly PermissionRecord ManageMaintenance = new PermissionRecord { Name = "Admin area. Manage Maintenance", SystemName = "ManageMaintenance", Category = "Configuration" };
        public static readonly PermissionRecord HtmlEditorManagePictures = new PermissionRecord { Name = "Admin area. HTML Editor. Manage pictures", SystemName = "HtmlEditor.ManagePictures", Category = "Configuration" };
        public static readonly PermissionRecord ManageScheduleTasks = new PermissionRecord { Name = "Admin area. Manage Schedule Tasks", SystemName = "ManageScheduleTasks", Category = "Configuration" };
        public static readonly PermissionRecord ManageSuppliers = new PermissionRecord { Name = "Admin area. Manage Suppliers", SystemName = "ManageSuppliers", Category = "Supplier" };
        //public store permissions
        public static readonly PermissionRecord DisplayPrices = new PermissionRecord { Name = "Public store. Display Prices", SystemName = "DisplayPrices", Category = "PublicStore" };
        public static readonly PermissionRecord EnableShoppingCart = new PermissionRecord { Name = "Public store. Enable shopping cart", SystemName = "EnableShoppingCart", Category = "PublicStore" };
        public static readonly PermissionRecord EnableWishlist = new PermissionRecord { Name = "Public store. Enable wishlist", SystemName = "EnableWishlist", Category = "PublicStore" };
        public static readonly PermissionRecord PublicStoreAllowNavigation = new PermissionRecord { Name = "Public store. Allow navigation", SystemName = "PublicStoreAllowNavigation", Category = "PublicStore" };
        public static readonly PermissionRecord AccessClosedStore = new PermissionRecord { Name = "Public store. Access a closed store", SystemName = "AccessClosedStore", Category = "PublicStore" };

        public static readonly PermissionRecord ManageStates = new PermissionRecord { Name = "Admin area. Manage State", SystemName = "ManageStates", Category = "Configuration" };

        #region Custom code from v4.0
        public static readonly PermissionRecord ManageItemReviews = new PermissionRecord { Name = "Admin.area.admin.RatingReview.ItemReview", SystemName = "ManageItemReview", Category = "RatingReview" };
        public static readonly PermissionRecord ManageMerchant = new PermissionRecord { Name = "Admin.area.admin.RatingReview.MerchantReview", SystemName = "ManageMerchant", Category = "RatingReview" };
        public static readonly PermissionRecord ManageAgent = new PermissionRecord { Name = "Admin.area.admin.RatingReview.AgentReview", SystemName = "ManageAgent", Category = "RatingReview" };
        public static readonly PermissionRecord ManageSchedules = new PermissionRecord { Name = "Manage Schedules", SystemName = "ManageSchedules", Category = "Catalog" };
        public static readonly PermissionRecord ManageVendorUser = new PermissionRecord { Name = "Admin area. Manage Vendors user", SystemName = "ManageVendorsUser", Category = "Customers" };
        public static readonly PermissionRecord ManageAgents = new PermissionRecord { Name = "Admin area. Manage Agents", SystemName = "ManageAgents", Category = "Customers" };
        public static readonly PermissionRecord ManageBookingMaster = new PermissionRecord { Name = "Manage Booking Master", SystemName = "ManageBookingMaster", Category = "Booking" };
        public static readonly PermissionRecord ManageBookingTransaction = new PermissionRecord { Name = "Manage Booking Transaction", SystemName = "ManageBookingTransaction", Category = "Booking" };
        public static readonly PermissionRecord ManageProductTagImageMapping = new PermissionRecord { Name = "Admin Area.ProductTagImagemapping", SystemName = "ManageProductTagImageMapping", Category = "Catalog" };
        public static readonly PermissionRecord AgentAllocation = new PermissionRecord { Name = "Admin area.Setting.Agent Allocation", SystemName = "AgentAllocation", Category = "Setting" };
        public static readonly PermissionRecord StoreSetup = new PermissionRecord { Name = "Admin area.Setting.StoreSetup", SystemName = "StoreSetup", Category = "Setting" };
        public static readonly PermissionRecord ManageProductCuisine = new PermissionRecord { Name = "Admin Area.ProductCuisine", SystemName = "ManageProductCuisine", Category = "ProductCuisine" };// by mohini
        public static readonly PermissionRecord ManageProductBulk = new PermissionRecord { Name = "Admin Area.ManageProductBulkEdit", SystemName = "ManageProductBulkEdit", Category = "Catalog" };
        public static readonly PermissionRecord ManageAgentEarning = new PermissionRecord { Name = "Admin Area.ManageAgentEarning", SystemName = "ManageAgentEarning", Category = "Orders" };
        public static readonly PermissionRecord ManageMerchantEarning = new PermissionRecord { Name = "Admin Area.ManageMerchantEarning", SystemName = "ManageMerchantEarning", Category = "Orders" };
        public static readonly PermissionRecord ManageMerchantCategorys = new PermissionRecord { Name = "NB.Admin Area.ManageMerchantCategorys", SystemName = "ManageMerchantCategorys", Category = "Catalog" };

        //For Vendor Gallary
        public static readonly PermissionRecord ManageVendorGallery = new PermissionRecord { Name = "Admin area. Manage Vendors Gallery", SystemName = "ManageVendorGallery", Category = "Customers" };

        //Configuration: Roles & permissions
        public static readonly PermissionRecord ManageNBRoles = new PermissionRecord { Name = "NB.Admin Area.ManageRoles", SystemName = "ManageNBRoles", Category = "Customers" };
        //User->Admins
        public static readonly PermissionRecord ManageUserAdmin = new PermissionRecord { Name = "NB.Admin Area.UserAdmin", SystemName = "ManageNBUserAdmin", Category = "Customers" };
        //For DeliverySLot
        public static readonly PermissionRecord ManageDeliverySlots = new PermissionRecord { Name = "NB.Admin Area.DeliverySlots", SystemName = "ManageDeliverySlots", Category = "DeliverySlots" };

        //Configuration: Packages
        public static readonly PermissionRecord ManageNBPackages = new PermissionRecord { Name = "NB.Admin Area.ManagePackages", SystemName = "ManageNBPackages", Category = "Packages" };

        //Configuration : Accounts and billings
        public static readonly PermissionRecord ManageNBAccountsBillings = new PermissionRecord { Name = "NB.Admin Area.ManageNBAccountsBillings", SystemName = "ManageNBAccountsBillings", Category = "AcountsBillings" };
        public static readonly PermissionRecord ManageQRCode = new PermissionRecord { Name = "NB.Admin Area.ManageQRCode", SystemName = "ManageQRCode", Category = "Catalog" };

        //Dispach management
        public static readonly PermissionRecord ManageDispatchManagement = new PermissionRecord { Name = "NB.Admin Area.DispatchManagement", SystemName = "ManageDispatchManagement", Category = "DispatchModule" };

        #endregion

        /// <summary>
        /// Get permissions
        /// </summary>
        /// <returns>Permissions</returns>
        public virtual IEnumerable<PermissionRecord> GetPermissions()
        {
            return new[]
            {
                AccessAdminPanel,
                AllowCustomerImpersonation,
                ManageProducts,
                ManageCategories,
                ManageManufacturers,
                ManageProductReviews,
                ManageProductTags,
                ManageAttributes,
                ManageCustomers,
                ManageVendors,
                ManageCurrentCarts,
                ManageOrders,
                ManageRecurringPayments,
                ManageGiftCards,
                ManageReturnRequests,
                OrderCountryReport,
                ManageAffiliates,
                ManageCampaigns,
                ManageDiscounts,
                ManageNewsletterSubscribers,
                ManagePolls,
                ManageNews,
                ManageBlog,
                ManageWidgets,
                ManageTopics,
                ManageForums,
                ManageMessageTemplates,
                ManageCountries,
                ManageLanguages,
                ManageSettings,
                ManagePaymentMethods,
                ManageExternalAuthenticationMethods,
                ManageTaxSettings,
                ManageShippingSettings,
                ManageCurrencies,
                ManageActivityLog,
                ManageAcl,
                ManageEmailAccounts,
                ManageStores,
                ManagePlugins,
                ManageSuppliers,
                ManageSystemLog,
                ManageMessageQueue,
                ManageMaintenance,
                HtmlEditorManagePictures,
                ManageScheduleTasks,
                DisplayPrices,
                EnableShoppingCart,
                EnableWishlist,
                PublicStoreAllowNavigation,
                AccessClosedStore,
                //Custom code from v4.0
                ManageSchedules,
                AccessClosedStore,
                ManageBookingMaster,
                ManageBookingTransaction,
                ManageProductBulk,
                ManageAgentEarning,
                ManageMerchantEarning,
                ManageVendorGallery,
                ManageNBRoles,
                ManageUserAdmin,
                ManageNBPackages,
                ManageNBAccountsBillings,
                ManageQRCode
            };
        }

        /// <summary>
        /// Get default permissions
        /// </summary>
        /// <returns>Permissions</returns>
        public virtual IEnumerable<DefaultPermissionRecord> GetDefaultPermissions()
        {
            return new[]
            {
                new DefaultPermissionRecord
                {
                    CustomerRoleSystemName = NopCustomerDefaults.AdministratorsRoleName,
                    PermissionRecords = new[]
                    {
                        AccessAdminPanel,
                        AllowCustomerImpersonation,
                        ManageProducts,
                        ManageCategories,
                        ManageManufacturers,
                        ManageProductReviews,
                        ManageProductTags,
                        ManageAttributes,
                        ManageCustomers,
                        ManageVendors,
                        ManageCurrentCarts,
                        ManageOrders,
                        ManageRecurringPayments,
                        ManageGiftCards,
                        ManageReturnRequests,
                        OrderCountryReport,
                        ManageAffiliates,
                        ManageCampaigns,
                        ManageSuppliers,
                        ManageDiscounts,
                        ManageNewsletterSubscribers,
                        ManagePolls,
                        ManageNews,
                        ManageBlog,
                        ManageWidgets,
                        ManageTopics,
                        ManageForums,
                        ManageMessageTemplates,
                        ManageCountries,
                        ManageLanguages,
                        ManageSettings,
                        ManagePaymentMethods,
                        ManageExternalAuthenticationMethods,
                        ManageTaxSettings,
                        ManageShippingSettings,
                        ManageCurrencies,
                        ManageActivityLog,
                        ManageAcl,
                        ManageEmailAccounts,
                        ManageStores,
                        ManagePlugins,
                        ManageSystemLog,
                        ManageMessageQueue,
                        ManageMaintenance,
                        HtmlEditorManagePictures,
                        ManageScheduleTasks,
                        DisplayPrices,
                        EnableShoppingCart,
                        EnableWishlist,
                        PublicStoreAllowNavigation,
                        AccessClosedStore,
                        //Custom code from v4.0
                        ManageSchedules,
                        AccessClosedStore,
                        ManageBookingMaster,
                        ManageBookingTransaction,
                        ManageProductBulk,
                        ManageAgentEarning,
                        ManageMerchantEarning,
                        ManageVendorGallery,
                        ManageNBRoles,
                        ManageUserAdmin,
                        ManageNBPackages,
                        ManageNBAccountsBillings,
                        ManageQRCode
                    }
                },
                new DefaultPermissionRecord
                {
                    CustomerRoleSystemName = NopCustomerDefaults.ForumModeratorsRoleName,
                    PermissionRecords = new[]
                    {
                        DisplayPrices,
                        EnableShoppingCart,
                        EnableWishlist,
                        PublicStoreAllowNavigation
                    }
                },
                new DefaultPermissionRecord
                {
                    CustomerRoleSystemName = NopCustomerDefaults.GuestsRoleName,
                    PermissionRecords = new[]
                    {
                        DisplayPrices,
                        EnableShoppingCart,
                        EnableWishlist,
                        PublicStoreAllowNavigation
                    }
                },
                new DefaultPermissionRecord
                {
                    CustomerRoleSystemName = NopCustomerDefaults.RegisteredRoleName,
                    PermissionRecords = new[]
                    {
                        DisplayPrices,
                        EnableShoppingCart,
                        EnableWishlist,
                        PublicStoreAllowNavigation
                    }
                },
                new DefaultPermissionRecord
                {
                    CustomerRoleSystemName = NopCustomerDefaults.VendorsRoleName,
                    PermissionRecords = new[]
                    {
                        AccessAdminPanel,
                        ManageProducts,
                        ManageProductReviews,
                        ManageOrders,
                        ManageProductBulk

                    }
                }
            };
        }
    }
}