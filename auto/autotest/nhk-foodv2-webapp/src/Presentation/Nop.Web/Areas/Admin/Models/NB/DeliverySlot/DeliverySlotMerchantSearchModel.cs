﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB
{
    /// <summary>
    /// Represents a DeliverySlot Merchant search model
    /// </summary>
    public partial class DeliverySlotMerchantSearchModel : BaseSearchModel
    {
        #region Properties

        public int DeliverySlotId { get; set; }

        public int StoreId { get; set; }

        #endregion
    }
}