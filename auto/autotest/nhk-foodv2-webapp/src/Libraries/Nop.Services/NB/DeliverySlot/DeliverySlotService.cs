﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.NB;
using Nop.Core.Domain.Vendors;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.NB
{
    /// <summary>
    /// DeliverySlot property service
    /// </summary>
    public partial class DeliverySlotService : IDeliverySlotService
    {

        #region Fields

        private readonly IRepository<DeliverySlot> _deliverySlotRepository;
        private readonly IRepository<DeliverySlotBooking> _deliverySlotBookingRepository;
        private readonly IRepository<ProductDeliverySlotMapping> _productDeliverySlotMappingRepository;
        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="workContext">Work context</param>
        /// <param name="storeContext">Store context</param>
        /// <param name="eventPublisher">Event publisher</param>
        public DeliverySlotService(IRepository<DeliverySlot> deliverySlotRepository,
            IRepository<DeliverySlotBooking> deliverySlotBookingRepository,
            IRepository<ProductDeliverySlotMapping> productDeliverySlotMappingRepository,
            IRepository<Vendor> vendorRepository,
            IWorkContext workContext,
            IStoreContext storeContext,
            IEventPublisher eventPublisher,
            ICacheManager cacheManager
)
        {
            _deliverySlotRepository = deliverySlotRepository;
            _productDeliverySlotMappingRepository = productDeliverySlotMappingRepository;
            _deliverySlotBookingRepository = deliverySlotBookingRepository;
            _vendorRepository = vendorRepository;
            _workContext = workContext;
            _storeContext = storeContext;
            _eventPublisher = eventPublisher;
            _cacheManager = cacheManager;
        }

        #endregion

        #region DeliverySlot

        /// <summary>
        /// Gets a DeliverySlot  
        /// </summary>
        /// <param name="DeliverySlotId">DeliverySlot  identifier</param>
        /// <returns>MerchantCategory </returns>
        public virtual DeliverySlot GetDeliverySlotById(int deliverySlotId)
        {
            if (deliverySlotId == 0)
                return null;

            return _deliverySlotRepository.GetById(deliverySlotId);
        }

        /// <summary>
        /// Gets a DeliverySlot 
        /// </summary>
        /// <param name="storeId">storeId  identifier</param>
        /// <param name="searchName">searchName  identifier</param>
        /// <param name="duration">duration  identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="merchantId">merchant identifier</param>
        /// <param name="isDefaultSlot">isDefaultSlot identifier</param>
        /// <returns>DeliverySlots </returns>
        public virtual IPagedList<DeliverySlot> GetAllDeliverySlot(int storeId = 0, string searchName = null, int duration = 0, int pageIndex = 0, int pageSize = int.MaxValue, int merchantId = 0, bool isDefaultSlot = true)
        {
            var query = from eq in _deliverySlotRepository.Table
                        where !eq.Deleted
                        select eq;

            if (storeId > 0)
                query = query.Where(x => x.StoreId == storeId);

            if (!string.IsNullOrWhiteSpace(searchName))
                query = query.Where(x => x.Name.Contains(searchName));

            if (duration > 0)
                query = query.Where(x => x.Duration == duration);

            if (merchantId > 0)
                query = query.Where(x => x.MerchantId == merchantId);

            if (!isDefaultSlot)
                query = query.Where(x => x.IsDefaultSlot == isDefaultSlot);

            return new PagedList<DeliverySlot>(query, pageIndex, pageSize);
        }

        /// <summary>
        /// Inserts a DeliverySlot
        /// </summary>
        /// <param name="DeliverySlot">DeliverySlot </param>
        public virtual void InsertDeliverySlot(DeliverySlot deliverySlot)
        {
            if (deliverySlot == null)
                throw new ArgumentNullException("deliverySlot");

            _deliverySlotRepository.Insert(deliverySlot);

            //event notification
            _eventPublisher.EntityInserted(deliverySlot);
        }

        /// <summary>
        /// Updates the DeliverySlot 
        /// </summary>
        /// <param name="DeliverySlot">DeliverySlot </param>
        public virtual void UpdateDeliverySlot(DeliverySlot deliverySlot)
        {
            if (deliverySlot == null)
                throw new ArgumentNullException("deliverySlot");

            _deliverySlotRepository.Update(deliverySlot);


            //event notification
            _eventPublisher.EntityUpdated(deliverySlot);
        }

        /// <summary>
        /// Delete a DeliverySlot
        /// </summary>
        /// <param name="DeliverySlot">DeliverySlot</param>
        public virtual void DeleteDeliverySlot(DeliverySlot deliverySlot)
        {
            if (deliverySlot == null)
                throw new ArgumentNullException("deliverySlot");

            _deliverySlotRepository.Delete(deliverySlot);


            //event notification
            _eventPublisher.EntityDeleted(deliverySlot);
        }

        /// <summary>
        /// Gets a DeliverySlot  
        /// </summary>
        /// <param name="DeliverySlotIds">DeliverySlot  identifier</param>
        /// <returns>DeliverySlot list</returns>
        public virtual IList<DeliverySlot> GetDeliverySlotByIds(int[] deliverySlotIds)
        {
            if (deliverySlotIds == null || deliverySlotIds.Length == 0)
                return new List<DeliverySlot>();

            var query = from p in _deliverySlotRepository.Table
                        where deliverySlotIds.Contains(p.Id) && !p.Deleted && p.Published
                        select p;
            var deliverySlots = query.ToList();
            //sort by passed identifiers
            var sortedDeliverySlots = new List<DeliverySlot>();
            foreach (var id in deliverySlotIds)
            {
                var deliverySlot = deliverySlots.Find(x => x.Id == id);
                if (deliverySlot != null)
                    sortedDeliverySlots.Add(deliverySlot);
            }

            return sortedDeliverySlots;
        }

        /// <summary>
        /// Check Same Slot Exists  
        /// </summary>
        /// <param name="startDateUtc">startDateUtc  identifier</param>
        /// <param name="endDateUtc">endDateUtc  identifier</param>
        /// <param name="storeId">storeId  identifier</param>
        /// <param name="merchantId">merchantId  identifier</param>
        /// <param name="id">id  identifier</param>
        /// <returns>DeliverySlot list</returns>
        public virtual bool CheckSameSlotExists(DateTime startDateUtc, DateTime endDateUtc, int storeId, int merchantId, int id)
        {
            var query = from eq in _deliverySlotRepository.Table
                        where !eq.Deleted &&
                           (
                           (eq.StartDateUtc.Add(eq.StartTime) <= startDateUtc && eq.EndDateUtc.Add(eq.EndTime) > startDateUtc)
                            || (eq.StartDateUtc.Add(eq.StartTime) < endDateUtc && eq.EndDateUtc.Add(eq.EndTime) >= endDateUtc)
                            || (eq.StartDateUtc.Add(eq.StartTime) > startDateUtc && eq.EndDateUtc.Add(eq.EndTime) < endDateUtc)
                            )
                        select eq;

            if (storeId > 0)
                query = query.Where(x => x.StoreId == storeId);

            if (merchantId > 0)
                query = query.Where(x => x.MerchantId == merchantId);

            if (id > 0)
                query = query.Where(x => x.Id != id);

            return query.Any();
        }

        /// <summary>
        /// Gets a DeliverySlot  
        /// </summary>
        /// <param name="deliverySlotBookingId">deliverySlotBookingId  identifier</param>
        /// <returns>DeliverySlot </returns>
        public virtual DeliverySlot GetDeliverySlotByDeliverySlotBookingId(int deliverySlotBookingId)
        {
            if (deliverySlotBookingId == 0)
                return null;

            var query = from db in _deliverySlotBookingRepository.Table
                        join d in _deliverySlotRepository.Table on db.SlotId equals d.Id
                        where db.Id == deliverySlotBookingId &&
                              !d.Deleted
                        orderby db.Id
                        select d;

            return query.FirstOrDefault();
        }

        /// <summary>
        /// Gets a DeliverySlotBooking  
        /// </summary>
        /// <param name="DeliverySlotBookingId">DeliverySlotBooking  identifier</param>
        /// <returns>DeliverySlotBooking </returns>
        public virtual DeliverySlotBooking GetDeliverySlotBookingById(int deliverySlotBookingId)
        {
            if (deliverySlotBookingId == 0)
                return null;

            return _deliverySlotBookingRepository.GetById(deliverySlotBookingId);
        }

        /// <summary>
        /// Gets a DeliverySlotBooking  
        /// </summary>
        /// <param name="orderId">orderId  identifier</param>
        /// <returns>DeliverySlotBooking </returns>
        public virtual DeliverySlotBooking GetDeliverySlotBookingByOrderId(int orderId)
        {
            if (orderId == 0)
                return null;

            return _deliverySlotBookingRepository.Table.Where(x => x.OrderId == orderId).FirstOrDefault();
        }

        /// <summary>
        /// Gets a DeliverySlotBooking 
        /// </summary>
        /// <param name="slotId">slotId  identifier</param>
        /// <param name="orderId">orderId  identifier</param>
        /// <param name="storeId">storeId  identifier</param>
        /// <param name="customerId">customerId  identifier</param>
        /// <param name="deliveryDate">deliveryDate  identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>DeliverySlotBooking </returns>
        public virtual IPagedList<DeliverySlotBooking> GetAllDeliverySlotsBooking(int slotId = 0, int orderId = 0, int storeId = 0, int customerId = 0, DateTime? deliveryDate = null, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from eq in _deliverySlotBookingRepository.Table
                        where !eq.Deleted
                        select eq;

            if (slotId > 0)
                query = query.Where(x => x.SlotId == slotId);

            if (orderId > 0)
                query = query.Where(x => x.OrderId == orderId);

            if (storeId > 0)
                query = query.Where(x => x.StoreId == storeId);

            if (customerId > 0)
                query = query.Where(x => x.CustomerId == customerId);

            if (deliveryDate != null)
                query = query.Where(x => x.DeliveryDateUtc == deliveryDate.Value.Date);

            if (customerId > 0)
                query = query.Where(x => x.CustomerId == customerId);

            return new PagedList<DeliverySlotBooking>(query, pageIndex, pageSize);
        }

        /// <summary>
        /// Inserts a DeliverySlotBooking
        /// </summary>
        /// <param name="DeliverySlotBooking">DeliverySlotBooking </param>
        public virtual void InsertDeliverySlotBooking(DeliverySlotBooking deliverySlotBooking)
        {
            if (deliverySlotBooking == null)
                throw new ArgumentNullException(nameof(deliverySlotBooking));

            _deliverySlotBookingRepository.Insert(deliverySlotBooking);

            //event notification
            _eventPublisher.EntityInserted(deliverySlotBooking);
        }

        /// <summary>
        /// Updates the DeliverySlotBooking 
        /// </summary>
        /// <param name="DeliverySlotBooking">DeliverySlotBooking </param>
        public virtual void UpdateDeliverySlotBooking(DeliverySlotBooking deliverySlotBooking)
        {
            if (deliverySlotBooking == null)
                throw new ArgumentNullException("deliverySlotBooking");

            _deliverySlotBookingRepository.Update(deliverySlotBooking);


            //event notification
            _eventPublisher.EntityUpdated(deliverySlotBooking);
        }

        /// <summary>
        /// Inserts a product DeliverySlot mapping
        /// </summary>
        /// <param name="productDeliverySlot">>Product DeliverySlot mapping</param>
        public virtual void InsertProductDeliverySlot(ProductDeliverySlotMapping productDeliverySlotMapping)
        {
            if (productDeliverySlotMapping == null)
                throw new ArgumentNullException(nameof(productDeliverySlotMapping));

            _productDeliverySlotMappingRepository.Insert(productDeliverySlotMapping);

            //event notification
            _eventPublisher.EntityInserted(productDeliverySlotMapping);
        }

        /// <summary>
        /// Deletes a product DeliverySlot mapping
        /// </summary>
        /// <param name="productDeliverySlot">Product deliverySlot</param>
        public virtual void DeleteProductDeliverySlot(ProductDeliverySlotMapping productDeliverySlotMapping)
        {
            if (productDeliverySlotMapping == null)
                throw new ArgumentNullException(nameof(productDeliverySlotMapping));

            _productDeliverySlotMappingRepository.Delete(productDeliverySlotMapping);

            //event notification
            _eventPublisher.EntityDeleted(productDeliverySlotMapping);
        }

        /// <summary>
        /// Gets a product DeliverySlot mapping collection
        /// </summary>
        /// <param name="productId">Product identifier</param>
        /// <returns>Product DeliverySlot mapping collection</returns>
        public virtual IList<ProductDeliverySlotMapping> GetProductDeliverySlotsByProductId(int productId)
        {
            if (productId == 0)
                return new List<ProductDeliverySlotMapping>();

            var query = from pd in _productDeliverySlotMappingRepository.Table
                        join d in _deliverySlotRepository.Table on pd.DeliverySlotId equals d.Id
                        where pd.ProductId == productId &&
                              !d.Deleted
                        orderby pd.Id
                        select pd;

            return query.ToList();
        }

        /// <summary>
        /// Returns a ProductDeliverySlotMapping that has the specified values
        /// </summary>
        /// <param name="source">Source</param>
        /// <param name="productId">Product identifier</param>
        /// <param name="deliverySlotId">deliverySlot identifier</param>
        /// <returns>A ProductDeliverySlotMapping that has the specified values; otherwise null</returns>
        public virtual ProductDeliverySlotMapping FindProductDeliverySlot(IList<ProductDeliverySlotMapping> source, int productId, int deliverySlotId)
        {
            foreach (var productDeliverySlot in source)
                if (productDeliverySlot.ProductId == productId && productDeliverySlot.DeliverySlotId == deliverySlotId)
                    return productDeliverySlot;

            return null;
        }

        /// <summary>
        /// Gets a DeliverySlot mapping collection
        /// </summary>
        /// <param name="productId">deliverySlot identifier</param>
        /// <returns>Product DeliverySlot mapping collection</returns>
        public virtual IList<ProductDeliverySlotMapping> GetProductDeliverySlotsByDeliverySlotId(int deliverySlotId)
        {
            if (deliverySlotId == 0)
                return new List<ProductDeliverySlotMapping>();

            var query = from pd in _productDeliverySlotMappingRepository.Table
                        join d in _deliverySlotRepository.Table on pd.DeliverySlotId equals d.Id
                        where pd.DeliverySlotId == deliverySlotId &&
                              !d.Deleted
                        orderby pd.Id
                        select pd;

            return query.ToList();
        }

        /// <summary>
        /// Any Slot Availble To Merchant 
        /// </summary>
        /// <param name="storeId">storeId  identifier</param>
        /// <param name="merchantId">merchant identifier</param>
        /// <returns>status </returns>
        public virtual bool AnySlotAvailbleToMerchant(int storeId = 0, int merchantId = 0)
        {
            return (from p in _deliverySlotRepository.Table
                        where p.StoreId == storeId && (p.MerchantId == merchantId || p.MerchantId == 0) && !p.Deleted && p.Published
                        select p).Any();
        }
        #endregion
    }
}
