﻿using Nop.Core.Domain.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.WebsiteExtension
{
    public partial class WebsiteExtension: BaseEntity
    {
        /// <summary>
        /// Gets or sets the id
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// Gets or sets the extension name
        /// </summary>
        public string extension { get; set; }
    }
   
        
}
