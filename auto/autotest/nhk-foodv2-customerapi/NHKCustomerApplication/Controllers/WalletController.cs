﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NHKCustomerApplication.Caching;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Services;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModel;
using NHKCustomerApplication.ViewModels;

namespace NHKCustomerApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WalletController : ControllerBase
    {
        #region Fields
        private readonly ecuadordevContext _dbContext;
        private readonly IWalletService _walletService;
        private readonly ICacheManager _cacheManager;
        #endregion

        #region Constructor
        public WalletController(ecuadordevContext context, IWalletService walletService, ICacheManager cacheManager)
        {
            _dbContext = context;
            _walletService = walletService;
            this._cacheManager = cacheManager;
        } 
        #endregion
        /// <summary>
        /// Gets the current wallet balance for a customer
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        [HttpPost("~/api/v1/GetWalletBalance")]
        public BaseAPIResponseModel WalletBalance(WalletMiniModel requestModel)
        {
            BaseAPIResponseModel response = new BaseAPIResponseModel();
            int languageId = LanguageHelper.GetIdByLangCode(requestModel.StoreId, requestModel.UniqueSeoCode, _dbContext);
            if (requestModel == null)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMessage.ModelNotDefined", languageId, _dbContext));
            if (requestModel.CustomerId != 0)
            {
                var customerExist = Helper.GetCustomer(requestModel.CustomerId, requestModel.StoreId, _dbContext);
                if (customerExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.CustomerNotFound", languageId, _dbContext));
            }
            if (!string.IsNullOrEmpty(requestModel.ApiKey) || !string.IsNullOrWhiteSpace(requestModel.ApiKey))
            {
                var keyExist = Helper.AuthenticateKey(requestModel.ApiKey, requestModel.StoreId, _dbContext,_cacheManager);
                if (keyExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.GetAllStoreLanguages.InvalidAuthentication", languageId, _dbContext));
            }
            if (requestModel.StoreId == 0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.StoreIdMissing", languageId, _dbContext));

            try
            {
               double balance = Convert.ToDouble(_walletService.GetCurrentBalance(requestModel.CustomerId));
                string currency = Helper.GetStoreCurrency(requestModel.StoreId, _dbContext);
                var data = new
                {
                    Balance=balance,
                    Symbol=currency,
                };
                if (data == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMessage.WalletBalance", languageId, _dbContext));
                return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.SuccessMessage.WalletBalance", languageId, _dbContext), data);
            }
            catch (Exception ex)
            {

                Helper.SentryLogs(ex);
                response.ErrorMessage = ex.Message;
                response.Status = false;
                response.StatusCode = 400;
                response.ResponseObj = null;
                return response;
            }
            
            
        }
        /// <summary>
        /// Gets all transactions done by wallet for a customer
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        [HttpPost("~/api/v1/GetWalletHistory")]
        public BaseAPIResponseModel WalletHistory(WalletMiniModel requestModel)
        {
            BaseAPIResponseModel response = new BaseAPIResponseModel();
            int languageId = LanguageHelper.GetIdByLangCode(requestModel.StoreId, requestModel.UniqueSeoCode, _dbContext);
            if (requestModel == null)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMessage.ModelNotDefined", languageId, _dbContext));
            if (requestModel.CustomerId != 0)
            {
                var customerExist = Helper.GetCustomer(requestModel.CustomerId, requestModel.StoreId, _dbContext);
                if (customerExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.CustomerNotFound", languageId, _dbContext));
            }
            if (!string.IsNullOrEmpty(requestModel.ApiKey) || !string.IsNullOrWhiteSpace(requestModel.ApiKey))
            {
                var keyExist = Helper.AuthenticateKey(requestModel.ApiKey, requestModel.StoreId, _dbContext, _cacheManager);
                if (keyExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.GetAllStoreLanguages.InvalidAuthentication", languageId, _dbContext));
            }
            if (requestModel.StoreId == 0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.StoreIdMissing", languageId, _dbContext));

            try
            {
                var data=_walletService.GetHistory(requestModel.CustomerId);
                if(data==null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMessage.WalletHistory", languageId, _dbContext));
                return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.SuccessMessage.WalletHistory", languageId, _dbContext), data);
            }
            catch (Exception ex)
            {

                Helper.SentryLogs(ex);
                response.ErrorMessage = ex.Message;
                response.Status = false;
                response.StatusCode = 400;
                response.ResponseObj = null;
                return response;
            }


        }
        /// <summary>
        /// Gets all payment methods excluding COD
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        [HttpPost("~/api/v1/GetPaymentMethodsForWallet")]
        public BaseAPIResponseModel WalletPaymentMethods(WalletMiniModel requestModel)
        {
            BaseAPIResponseModel response = new BaseAPIResponseModel();
            int languageId = LanguageHelper.GetIdByLangCode(requestModel.StoreId, requestModel.UniqueSeoCode, _dbContext);
            if (requestModel == null)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMessage.ModelNotDefined", languageId, _dbContext));
            if (requestModel.CustomerId != 0)
            {
                var customerExist = Helper.GetCustomer(requestModel.CustomerId, requestModel.StoreId, _dbContext);
                if (customerExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.CustomerNotFound", languageId, _dbContext));
            }
            if (!string.IsNullOrEmpty(requestModel.ApiKey) || !string.IsNullOrWhiteSpace(requestModel.ApiKey))
            {
                var keyExist = Helper.AuthenticateKey(requestModel.ApiKey, requestModel.StoreId, _dbContext, _cacheManager);
                if (keyExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.GetAllStoreLanguages.InvalidAuthentication", languageId, _dbContext));
            }
            if (requestModel.StoreId == 0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.StoreIdMissing", languageId, _dbContext));

            try
            {
                List<PaymentMethodModel> paymentMethods = new List<PaymentMethodModel>();

                PaymentMethodModel paymentMethod = new PaymentMethodModel();
                paymentMethod.PaymentMethodId = (int)PaymentMethodEnum.PayPal;
                paymentMethod.PaymentMethodName = LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.GetAllPaymentMethods.Paypal", languageId, _dbContext);
                paymentMethod.isShow = false;
                paymentMethods.Add(paymentMethod);

                paymentMethod = new PaymentMethodModel();
                paymentMethod.PaymentMethodId = (int)PaymentMethodEnum.Card;
                paymentMethod.PaymentMethodName = LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.GetAllPaymentMethods.CreditDebitCart", languageId, _dbContext);
                paymentMethod.isShow = false;
                paymentMethods.Add(paymentMethod);

                paymentMethod = new PaymentMethodModel();
                paymentMethod.PaymentMethodId = (int)PaymentMethodEnum.Square;
                paymentMethod.PaymentMethodName = LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.GetAllPaymentMethods.Square", languageId, _dbContext);
                paymentMethod.isShow = false;
                paymentMethods.Add(paymentMethod);

        

                var data = paymentMethods;
                if (data == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMessage.WalletPaymentMethods", languageId, _dbContext));
                return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.SuccessMessage.WalletPaymentMethods", languageId, _dbContext), data);
            }
            catch (Exception ex)
            {

                Helper.SentryLogs(ex);
                response.ErrorMessage = ex.Message;
                response.Status = false;
                response.StatusCode = 400;
                response.ResponseObj = null;
                return response;
            }
        }
        /// <summary>
        /// Generates order on the basis of subscription
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns>orderid</returns>
        [HttpPost("~/api/v1/AddMoney")]

        public BaseAPIResponseModel AddMoney(WalletRequestModel requestModel)
        {
            BaseAPIResponseModel response = new BaseAPIResponseModel();
            int languageId = LanguageHelper.GetIdByLangCode(requestModel.StoreId, requestModel.UniqueSeoCode, _dbContext);
            if (requestModel == null)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMessage.ModelNotDefined", languageId, _dbContext));
            if (requestModel.CustomerId != 0)
            {
                var customerExist = Helper.GetCustomer(requestModel.CustomerId, requestModel.StoreId, _dbContext);
                if (customerExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.CustomerNotFound", languageId, _dbContext));
            }
            if (!string.IsNullOrEmpty(requestModel.ApiKey) || !string.IsNullOrWhiteSpace(requestModel.ApiKey))
            {
                var keyExist = Helper.AuthenticateKey(requestModel.ApiKey, requestModel.StoreId, _dbContext, _cacheManager);
                if (keyExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.GetAllStoreLanguages.InvalidAuthentication", languageId, _dbContext));
            }
            if (requestModel.StoreId == 0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.StoreIdMissing", languageId, _dbContext));

            try
            {
                var customer=Helper.GetCustomer(requestModel.CustomerId, requestModel.StoreId, _dbContext);
                if (requestModel.Amount > 0) 
                {
                    bool storeCreditCard = false;
                    //Get product by SKU
                    var product=_dbContext.Product.Where(x => x.Sku.ToLower().Trim() == "subscriptionfeature" && !x.Deleted).OrderBy(x => x.Id).FirstOrDefault();
                    var cart = new
                    {
                        CreatedOnUtc = DateTime.UtcNow,
                        Id = 1,
                        CustomerEnteredPrice = requestModel.Amount,
                        CustomerId = requestModel.CustomerId,
                        ProductId = product.Id,
                        Quantity = 1,
                        ShoppingCartType = ShoppingCartType.ShoppingCart,
                        StoreId = requestModel.StoreId
                    };
                    //  Prevent anonymous checkout
                   
                    bool isAnonymousCheckoutAllowed=Convert.ToBoolean(_dbContext.Setting.Where(x => x.Name.Equals("ordersettings.anonymouscheckoutallowed") && x.StoreId == requestModel.StoreId).FirstOrDefault()?.Value);
                    if(!isAnonymousCheckoutAllowed)
                    {
                        ProcessPaymentRequest paymentRequest = new ProcessPaymentRequest();
                        
                        //If payment is done by card
                        if(requestModel.PaymentMethodId==(int)PaymentMethodEnum.Card)
                        {
                            //Encrypt card details
                            paymentRequest.PaymentMethodSystemName = "Payments.Stripe";
                            paymentRequest.CustomerId = requestModel.CustomerId;
                            paymentRequest.StoreCreditCard= storeCreditCard = true;
                            
                        }
                        #region Order
                        //Generate order GUID
                        if (paymentRequest != null)
                        {
                            Helper.GenerateOrderGUID(paymentRequest);
                        }
                        
                        
                        Order order = new Order();
                        order.OrderGuid = paymentRequest.OrderGuid;
                        if(order.OrderGuid==Guid.Empty)
                        {
                            response.ErrorMessage = "Order GUID not generated";
                            response.StatusCode = 400;
                            return response;
                        }
                        order.StoreId = requestModel.StoreId;
                        order.CustomerId = requestModel.CustomerId;
                        var billingAddress = Helper.GetCustomerBillingAddress(requestModel.CustomerId,_dbContext);
                        order.BillingAddressId = billingAddress.AddressId;
                        order.PickupInStore = false;//since order doesn't require shipping
                        order.OrderStatusId = (int)OrderStatus.Received;
                        order.ShippingStatusId = (int)ShippingStatus.ShippingNotRequired;
                        //order.PaymentStatusId to be updated after checkout
                        order.PaymentMethodSystemName = paymentRequest.PaymentMethodSystemName;
                        var currencysetting=_dbContext.GenericAttribute.Where(x => x.EntityId == requestModel.CustomerId && x.KeyGroup == "Customer" && x.StoreId == requestModel.StoreId && x.Key == "CurrencyId").FirstOrDefault();
                        string currencyCode = "";
                        if(currencysetting==null)
                        {
                            currencyCode= Helper.GetCurrentCurrencyCode(requestModel.StoreId, _dbContext);
                        }
                        else
                        {
                            int currencyId = Convert.ToInt32(currencysetting.Value);
                            currencyCode = _dbContext.Currency.Where(x => x.Id == currencyId).FirstOrDefault().CurrencyCode;
                        }
                        decimal currencyRate = 0;
                       

                        order.CustomerCurrencyCode = currencyCode;
                        //order.CurrencyRate
                        //order.CustomerTaxDisplayTypeId
                        order.OrderSubtotalInclTax= decimal.Zero;
                        order.OrderSubtotalExclTax= decimal.Zero;
                        order.OrderSubTotalDiscountInclTax= decimal.Zero;
                        order.OrderSubTotalDiscountExclTax= decimal.Zero;
                        order.OrderShippingInclTax = decimal.Zero;
                        order.OrderShippingExclTax = decimal.Zero;
                        order.PaymentMethodAdditionalFeeInclTax = decimal.Zero;
                        order.PaymentMethodAdditionalFeeExclTax=decimal.Zero;
                        //order.TaxRates don't know how to calculate
                        order.OrderTax= decimal.Zero;
                        order.OrderDiscount= decimal.Zero;
                        order.OrderTotal = requestModel.Amount;
                        order.RefundedAmount= decimal.Zero;
                        //order.RewardPointsHistoryEntryId
                        //order.CheckoutAttributeDescription
                        order.CustomerLanguageId = languageId;
                        order.AffiliateId = customer.AffiliateId;

                        var ipAddress = Request.HttpContext.Connection.LocalIpAddress.ToString();
                        if (ipAddress == "::1")
                            order.CustomerIp = "127.0.0.1";
                        else
                            order.CustomerIp = ipAddress;
                       
                        ////order.PaidDateUtc=                 
                        order.Deleted = false;
                        order.CreatedOnUtc = DateTime.UtcNow;
                        
                        //order.OrderDetailId
                        order.Tip = decimal.Zero;
                        order.DeliveryAmount = decimal.Zero;
                        order.ServiceChargeAmount = decimal.Zero;
                        order.CustomOrderNumber = "0";
                        _dbContext.Order.Add(order);
                        _dbContext.SaveChanges();


                        
                        #endregion

                        #region OrderDetails
                        OrderDetails orderDetails = new OrderDetails();
                        orderDetails.OrderType = 1;
                        orderDetails.IsOrderComplete = true;
                        orderDetails.CreatedOnUtc = DateTime.UtcNow;
                        _dbContext.OrderDetails.Add(orderDetails);
                        _dbContext.SaveChanges();
                        #endregion

                        #region OrderItem
                        var orderItem = new OrderItem()
                        {
                            OrderItemGuid = Guid.NewGuid(),
                            Order = order,
                            ProductId = cart.ProductId,
                            UnitPriceInclTax = 0,
                            UnitPriceExclTax = 0,
                            PriceInclTax = 0,
                            PriceExclTax = 0,
                            OriginalProductCost = decimal.Zero,
                            AttributeDescription = " ",
                            AttributesXml = null,
                            Quantity = cart.Quantity,
                            DiscountAmountInclTax = 0,
                            DiscountAmountExclTax = 0,
                            DownloadCount = 0,
                            IsDownloadActivated = false,
                            LicenseDownloadId = 0,
                            ItemWeight = decimal.Zero,
                            RentalStartDateUtc = null,
                            RentalEndDateUtc = null
                        };
                        order.OrderItem.Add(orderItem);

                        #endregion
                        order.OrderDetailId = orderDetails.Id;
                       
                        _dbContext.SaveChanges();
                        var resp = new
                        {
                            OrderId = order.Id,
                            WalletAmount=requestModel.Amount,
                        };

                        if (resp == null)
                            return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.NoProductForThisCategory", languageId, _dbContext));
                        return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "Your order placed", languageId, _dbContext), resp);
                    }
                    else
                    {
                        response.ErrorMessage = "Anonymous checkout is not allowed";
                        response.StatusCode = 400;
                        return response;
                    }
                }

                response.ErrorMessage = "Invalid wallet amount";
                response.StatusCode = 400;
                return response;
                object data = null;
                if (data == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.NoProductForThisCategory", languageId, _dbContext));
                return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.ProductFoundForThisCategory", languageId, _dbContext), data);
            }
            catch (Exception ex)
            {

                Helper.SentryLogs(ex);
                response.ErrorMessage = ex.Message;
                response.Status = false;
                response.StatusCode = 400;
                response.ResponseObj = null;
                return response;
            }
        }

       
       



    }

}
