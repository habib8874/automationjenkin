﻿using System;

namespace Nestorbird.Plugin.Api.ResponseModel.Order
{
    public class NotificationResponse
    {
        public int OrderId { get; set; }

        public DateTime OrderDate { get; set; }

        public string CustomerName { get; set; }

        public string Price { get; set; }
    }
}
