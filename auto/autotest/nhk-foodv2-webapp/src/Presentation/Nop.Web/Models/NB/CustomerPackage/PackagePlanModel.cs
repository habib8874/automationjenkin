﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Models.NB
{
    public partial class PackagePlanModel : BaseNopModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}