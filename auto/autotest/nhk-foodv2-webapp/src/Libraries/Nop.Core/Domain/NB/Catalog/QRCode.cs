﻿using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Vendors;

namespace Nop.Core.Domain.NB.Catalog.QRCode
{
    /// <summary>
    /// Represents a QRCode
    /// </summary>
    public partial class QRCode : BaseEntity, ILocalizedEntity
    {
        public int MerchantId { get; set; }
        public int LogoId { get; set; }
        public string ResturantContent { get; set; }
        public string WebsiteURL { get; set; }
        public int QRCodePictureId { get; set; }

        public virtual Vendor Vendor { get; set; }
    }
}