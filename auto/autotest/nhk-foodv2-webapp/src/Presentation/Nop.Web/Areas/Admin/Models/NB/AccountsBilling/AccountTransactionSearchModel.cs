﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB.AccountsBilling
{
    /// <summary>
    /// Represents a customer activity log search model
    /// </summary>
    public partial class AccountTransactionSearchModel : BaseSearchModel
    {
    }
}