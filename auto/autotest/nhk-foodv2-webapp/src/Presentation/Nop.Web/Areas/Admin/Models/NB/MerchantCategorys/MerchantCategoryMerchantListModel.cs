﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB.MerchantCategorys
{
    /// <summary>
    /// Represents a Merchant Category Merchant list model
    /// </summary>
    public partial class MerchantCategoryMerchantListModel : BasePagedListModel<MerchantCategoryMerchantModel>
    {
    }
}