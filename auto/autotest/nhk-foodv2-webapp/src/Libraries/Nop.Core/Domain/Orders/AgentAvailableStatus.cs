﻿using System;
namespace Nop.Core.Domain.Orders
{
    public partial class AgentAvailableStatus : BaseEntity
    {
        public int AgentId { get; set; }
        public bool Status { get; set; }
        public string UpDt { get; set; }
        public DateTime SysDt { get; set; }
    }
}