﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Services.Stores;
using Nop.Core;
using System;

namespace Nop.Web.Components
{
    public class MerchantOnCheckOutByIdViewComponent : NopViewComponent
    {
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;

        public MerchantOnCheckOutByIdViewComponent(ICatalogModelFactory catalogModelFactory, IStoreService storeService, IWorkContext workContext)
        {
            this._catalogModelFactory = catalogModelFactory;
            this._storeService = storeService;
            this._workContext = workContext;
        }

        public IViewComponentResult Invoke()
        {
            int merchantId = _workContext.CurrentMerchant.Id;

            if (merchantId == 0)
                return Content("");

            var model = _catalogModelFactory.PrepareMerchantById(merchantId);

            if (model == null)
                return Content("");

            return View(model);
        }
    }
}
