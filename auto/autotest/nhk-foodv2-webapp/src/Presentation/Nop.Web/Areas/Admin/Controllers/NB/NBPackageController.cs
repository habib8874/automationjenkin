﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.NB.Package;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.NB.Package;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Factories.NB;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.NB.Package;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers.NB
{
    public partial class NBPackageController : BaseAdminController
    {
        #region Fields

        private readonly INBPackageModelFactory _nbPackageModelFactory;
        private readonly ICategoryService _categoryService;
        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IPictureService _pictureService;
        private readonly IProductService _productService;
        private readonly IWorkContext _workContext;
        private readonly INBPackageService _nBPackageService;

        #endregion

        #region Ctor

        public NBPackageController(
            INBPackageModelFactory nbPackageModelFactory,
            ICategoryService categoryService,
            ILocalizationService localizationService,
            INotificationService notificationService,
            IPermissionService permissionService,
            IPictureService pictureService,
            IProductService productService,
            IWorkContext workContext,
            INBPackageService nBPackageService)
        {
            _nbPackageModelFactory = nbPackageModelFactory;
            _categoryService = categoryService;
            _localizationService = localizationService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _pictureService = pictureService;
            _productService = productService;
            _workContext = workContext;
            _nBPackageService = nBPackageService;
        }

        #endregion

        #region Methods

        #region Package List

        public virtual IActionResult PackageList()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBPackages))
                return AccessDeniedView();

            //prepare model
            var model = _nbPackageModelFactory.PreparePackageSearchModel(new PackageSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult PackageList(PackageSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBPackages))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _nbPackageModelFactory.PreparePackageListModel(searchModel);

            return Json(model);
        }
        #endregion

        #region Create / Edit / Delete

        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBPackages))
                return AccessDeniedView();

            //prepare model
            var model = _nbPackageModelFactory.PreparePackageModel(new PackageModel(), null);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Create(PackageModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBPackages))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                //add product 
                var product = new Product();
                product.Name = model.Name;
                product.ShortDescription = model.ShortDescription;
                product.FullDescription = model.FullDescription;
                product.Name = model.Name;
                product.CreatedOnUtc = DateTime.UtcNow;
                product.UpdatedOnUtc = DateTime.UtcNow;
                product.ProductTypeId = (int)(ProductType.GroupedProduct);
                product.StockQuantity = 1000000;
                product.OrderMinimumQuantity = 1;
                product.OrderMaximumQuantity = 1;
                product.Published = true;
                _productService.InsertProduct(product);

                //category mapping
                var subscriptionCategory = _categoryService.GetAllCategories("Subscription").FirstOrDefault();

                if (subscriptionCategory != null)
                {
                    _categoryService.InsertProductCategory(new ProductCategory
                    {
                        ProductId = product.Id,
                        CategoryId = subscriptionCategory.Id,
                        DisplayOrder = 0
                    });
                }

                //add package
                var package = model.ToEntity<NBPackage>();
                package.Name = model.Name;
                package.PictureId = model.PictureId;
                package.ShortDescription = model.ShortDescription;
                package.FullDescription = model.FullDescription;
                package.ProductId = product.Id;
                package.StoreId = model.StoreId;
                if (_workContext.IsStoreOwnerRole || _workContext.SubAdmin.IsSubAdminRole)
                    package.StoreId = _workContext.GetCurrentStoreId;

                package.UnitLabel = model.UnitLabel;
                package.CreatedOnUtc = DateTime.UtcNow;
                package.ModifiedOnUtc = DateTime.UtcNow;

                _nBPackageService.InsertPackage(package);


                _notificationService.SuccessNotification(_localizationService.GetResource("NB.Admin.Package.Added"));

                if (!continueEditing)
                    return RedirectToAction("PackageList");

                return RedirectToAction("Edit", new { id = package.Id });
            }

            //prepare model
            model = _nbPackageModelFactory.PreparePackageModel(model, null);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBPackages))
                return AccessDeniedView();

            //try to get a subscription with the specified id
            var package = _nBPackageService.GetPackageById(id);
            if (package == null || package.Deleted)
                return RedirectToAction("PackageList");

            //prepare model
            var model = _nbPackageModelFactory.PreparePackageModel(null, package);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(PackageModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBPackages))
                return AccessDeniedView();

            //try to get a subscription with the specified id
            var package = _nBPackageService.GetPackageById(model.Id);
            if (package == null || package.Deleted)
                return RedirectToAction("PackageList");

            if (ModelState.IsValid)
            {
                //add package
                package.Name = model.Name;
                package.PictureId = model.PictureId;
                package.ShortDescription = model.ShortDescription;
                package.FullDescription = model.FullDescription;
                package.StoreId = model.StoreId;
                if (_workContext.IsStoreOwnerRole)
                    package.StoreId = _workContext.GetCurrentStoreId;

                package.UnitLabel = model.UnitLabel;
                package.ModifiedOnUtc = DateTime.UtcNow;
                _nBPackageService.UpdatePackage(package);


                _notificationService.SuccessNotification(_localizationService.GetResource("NB.Admin.Package.Updated"));

                if (!continueEditing)
                    return RedirectToAction("PackageList");

                return RedirectToAction("Edit", new { id = package.Id });
            }

            //prepare model
            model = _nbPackageModelFactory.PreparePackageModel(model, package);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBPackages))
                return AccessDeniedView();

            //try to get a subscription with the specified id
            var package = _nBPackageService.GetPackageById(id);
            if (package == null || package.Deleted)
                return RedirectToAction("PackageList");

            _nBPackageService.DeletePackage(package);

            _notificationService.SuccessNotification(_localizationService.GetResource("NB.Admin.Package.Deleted"));

            return RedirectToAction("PackageList");
        }

        #endregion

        #region Package plans

        public virtual IActionResult PackagePlanAdd(int packageId, string planName,
            decimal price, string billingInterval, DateTime? startDate, DateTime? endDate, string trailPeriod)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBPackages))
                return AccessDeniedView();

            if (packageId == 0)
                throw new ArgumentException();

            int productId = 0;
            var package = _nBPackageService.GetPackageById(packageId);
            if (package != null && (package.ProductId ?? 0) > 0)
            {

                //add product 
                var product = new Product();
                product.Name = planName;
                product.ShortDescription = planName;
                product.CreatedOnUtc = DateTime.UtcNow;
                product.UpdatedOnUtc = DateTime.UtcNow;
                product.ProductTypeId = (int)(ProductType.SimpleProduct);
                product.StockQuantity = 1000000;
                product.OrderMinimumQuantity = 1;
                product.OrderMaximumQuantity = 1;
                product.ParentGroupedProductId = package.ProductId ?? 0;
                product.VisibleIndividually = true;
                product.Price = price;
                product.Published = true;
                _productService.InsertProduct(product);

                productId = product.Id;
                //category mapping
                var subscriptionCategory = _categoryService.GetAllCategories("Subscription").FirstOrDefault();

                if (subscriptionCategory != null)
                {
                    _categoryService.InsertProductCategory(new ProductCategory
                    {
                        ProductId = productId,
                        CategoryId = subscriptionCategory.Id,
                        DisplayOrder = 0
                    });
                }
            }

            _nBPackageService.InsertPackagePlan(new NBPackagePlan
            {
                PackageId = packageId,
                Name = planName,
                Price = price,
                BillingInterval = billingInterval,
                StartDate = startDate,
                EndDate = endDate,
                TrailPeriod = trailPeriod,
                ProductId = productId
            });

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult PackagePlanList(PackagePlanSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBPackages))
                return AccessDeniedDataTablesJson();

            //try to get a subscription with the specified id
            var package = _nBPackageService.GetPackageById(searchModel.PackageId);
            if (package == null || package.Deleted)
                throw new ArgumentException("No package found with the specified id");
            searchModel.PackageId = package.Id;

            //prepare model
            var model = _nbPackageModelFactory.PreparePackagePlanListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult PackagePlanUpdate(PackagePlanModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBPackages))
                return AccessDeniedView();

            //try to get a package plan with the specified id
            var packagePlan = _nBPackageService.GetPackagePlanById(model.Id)
                ?? throw new ArgumentException("No package plan found with the specified id");

            packagePlan.Name = model.PlanName;
            packagePlan.Price = model.Price;
            packagePlan.BillingInterval = model.BillingInterval;
            packagePlan.TrailPeriod = model.TrailPeriod;
            _nBPackageService.UpdatePackagePlan(packagePlan);

            var product = _productService.GetProductById(packagePlan.ProductId ?? 0);
            if (product != null)
            {
                product.Price = model.Price;
                _productService.UpdateProduct(product);
            }

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult PackagePlanDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBPackages))
                return AccessDeniedView();


            //try to get a package plan with the specified id
            var packagePlan = _nBPackageService.GetPackagePlanById(id)
                ?? throw new ArgumentException("No package plan found with the specified id");

            _nBPackageService.DeletePackagePlan(packagePlan);

            return new NullJsonResult();
        }

        #endregion

        #endregion
    }
}