﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Primitives;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.Gdpr;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.NB;
using Nop.Core.Domain.NB.Package;
using Nop.Core.Domain.NB.VendorTokens;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Stores;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Vendors;
using Nop.Core.Http;
using Nop.Core.Infrastructure;
using Nop.Services.Authentication;
using Nop.Services.Authentication.External;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Events;
using Nop.Services.ExportImport;
using Nop.Services.Gdpr;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.NB.Package;
using Nop.Services.NB.VendorTokens;
using Nop.Services.Orders;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Extensions;
using Nop.Web.Factories;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.Customer;
using Nop.Web.Models.NB.CustomerPackage;
using Nop.Web.Models.ShoppingCart;
using Nop.Web.Validators.NB.CustomerPackage;
using stripe = Stripe;

namespace Nop.Web.Controllers
{
    public partial class CustomerController : BasePublicController
    {
        #region Fields

        private readonly AddressSettings _addressSettings;
        private readonly CaptchaSettings _captchaSettings;
        private readonly CustomerSettings _customerSettings;
        private readonly DateTimeSettings _dateTimeSettings;
        private readonly IDownloadService _downloadService;
        private readonly ForumSettings _forumSettings;
        private readonly GdprSettings _gdprSettings;
        private readonly IAddressAttributeParser _addressAttributeParser;
        private readonly IAddressModelFactory _addressModelFactory;
        private readonly IAddressService _addressService;
        private readonly IAuthenticationService _authenticationService;
        private readonly ICountryService _countryService;
        private readonly ICurrencyService _currencyService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly ICustomerAttributeService _customerAttributeService;
        private readonly ICustomerModelFactory _customerModelFactory;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly ICustomerService _customerService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICommonService _commonService;
        private readonly IExportManager _exportManager;
        private readonly IExternalAuthenticationService _externalAuthenticationService;
        private readonly IGdprService _gdprService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IGiftCardService _giftCardService;
        private readonly ILocalizationService _localizationService;
        private readonly ILogger _logger;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly IOrderService _orderService;
        private readonly IPictureService _pictureService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IStoreContext _storeContext;
        private readonly ITaxService _taxService;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly TaxSettings _taxSettings;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly IVendorService _vendorService;
        private readonly VendorSettings _vendorSettings;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ICheckoutAttributeService _checkoutAttributeService;
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;
        private readonly ISettingService _settingService;
        private readonly IRepository<CustomerRole> _customerRoleRepository;
        private readonly INBVendorTokenService _nBVendorTokenService;
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly IStoreService _storeService;
        private readonly INBCustomerPackageService _nBCustomerPackageService;
        private readonly INBPackageService _nbPackageService;
        private readonly ICityService _cityService;
        #endregion

        #region Ctor

        public CustomerController(AddressSettings addressSettings,
            CaptchaSettings captchaSettings,
            CustomerSettings customerSettings,
            DateTimeSettings dateTimeSettings,
            IDownloadService downloadService,
            ForumSettings forumSettings,
            GdprSettings gdprSettings,
            IAddressAttributeParser addressAttributeParser,
            IAddressModelFactory addressModelFactory,
            IAddressService addressService,
            IAuthenticationService authenticationService,
            ICountryService countryService,
            ICurrencyService currencyService,
            ICustomerActivityService customerActivityService,
            ICustomerAttributeParser customerAttributeParser,
            ICustomerAttributeService customerAttributeService,
            ICustomerModelFactory customerModelFactory,
            ICustomerRegistrationService customerRegistrationService,
            ICustomerService customerService,
            IEventPublisher eventPublisher,
            IExportManager exportManager,
            IExternalAuthenticationService externalAuthenticationService,
            IGdprService gdprService,
            IGenericAttributeService genericAttributeService,
            IGiftCardService giftCardService,
            ILocalizationService localizationService,
            ILogger logger,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            IOrderService orderService,
            IPictureService pictureService,
            IPriceFormatter priceFormatter,
            IShoppingCartService shoppingCartService,
            IStateProvinceService stateProvinceService,
            IStoreContext storeContext,
            ITaxService taxService,
            IWebHelper webHelper,
            IWorkContext workContext,
            IWorkflowMessageService workflowMessageService,
            LocalizationSettings localizationSettings,
            MediaSettings mediaSettings,
            StoreInformationSettings storeInformationSettings,
            TaxSettings taxSettings,
            IStaticCacheManager staticCacheManager,
            IRepository<CustomerRole> customerRoleRepository,
            IVendorService vendorService,
            VendorSettings vendorSettings,
            IUrlRecordService urlRecordService,
            ICheckoutAttributeService checkoutAttributeService,
            IShoppingCartModelFactory shoppingCartModelFactory,
            ISettingService settingService,
            INBVendorTokenService nBVendorTokenService,
            ICatalogModelFactory catalogModelFactory,
            IStoreService storeService,
            INBCustomerPackageService nBCustomerPackageService,
            INBPackageService nbPackageService,
            ICityService cityService)
        {
            _addressSettings = addressSettings;
            _captchaSettings = captchaSettings;
            _customerSettings = customerSettings;
            _dateTimeSettings = dateTimeSettings;
            _downloadService = downloadService;
            _forumSettings = forumSettings;
            _gdprSettings = gdprSettings;
            _addressAttributeParser = addressAttributeParser;
            _addressModelFactory = addressModelFactory;
            _addressService = addressService;
            _authenticationService = authenticationService;
            _countryService = countryService;
            _currencyService = currencyService;
            _customerActivityService = customerActivityService;
            _customerAttributeParser = customerAttributeParser;
            _customerAttributeService = customerAttributeService;
            _customerModelFactory = customerModelFactory;
            _customerRegistrationService = customerRegistrationService;
            _customerService = customerService;
            _eventPublisher = eventPublisher;
            _exportManager = exportManager;
            _externalAuthenticationService = externalAuthenticationService;
            _gdprService = gdprService;
            _genericAttributeService = genericAttributeService;
            _giftCardService = giftCardService;
            _localizationService = localizationService;
            _logger = logger;
            _newsLetterSubscriptionService = newsLetterSubscriptionService;
            _orderService = orderService;
            _pictureService = pictureService;
            _priceFormatter = priceFormatter;
            _shoppingCartService = shoppingCartService;
            _stateProvinceService = stateProvinceService;
            _storeContext = storeContext;
            _taxService = taxService;
            _webHelper = webHelper;
            _workContext = workContext;
            _workflowMessageService = workflowMessageService;
            _localizationSettings = localizationSettings;
            _mediaSettings = mediaSettings;
            _storeInformationSettings = storeInformationSettings;
            _taxSettings = taxSettings;
            _staticCacheManager = staticCacheManager;
            _vendorService = vendorService;
            _vendorSettings = vendorSettings;
            _urlRecordService = urlRecordService;
            _checkoutAttributeService = checkoutAttributeService;
            _shoppingCartModelFactory = shoppingCartModelFactory;
            _settingService = settingService;
            _customerRoleRepository = customerRoleRepository;
            _nBVendorTokenService = nBVendorTokenService;
            _catalogModelFactory = catalogModelFactory;
            _storeService = storeService;
            _commonService = EngineContext.Current.Resolve<ICommonService>();

            _nBCustomerPackageService = nBCustomerPackageService;
            _nbPackageService = nbPackageService;
            _cityService = cityService;
        }

        #endregion

        #region Utilities
        protected void RegistrationOTP(Customer customer)
        {
            var phone = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute, customer.RegisteredInStoreId, "");
            if (!phone.Contains(" "))
                phone = _workContext.CurrentStoreISDCode + " " + phone;


            string sRandomOTP = _workContext.GenerateOTP;

            _genericAttributeService.SaveAttribute(customer, "RegistrationOTP", sRandomOTP, _workContext.GetCurrentStoreId);
            _genericAttributeService.SaveAttribute(customer, "OTPSendDateTime", DateTime.UtcNow, _workContext.GetCurrentStoreId);


            _workflowMessageService.SendCustomerOTPOnPhone(customer, phone, _workContext.WorkingLanguage.Id, sRandomOTP);
            _workflowMessageService.SendCustomerOTPOnEmail(customer, customer.Email, _workContext.WorkingLanguage.Id, sRandomOTP);
        }

        public void ForgotOTP(string emailOrPhone)
        {
            var customerEmailExists = _customerService.GetCustomerByEmail(emailOrPhone);
            if (customerEmailExists == null)
                emailOrPhone = _workContext.CurrentStoreISDCode + " " + emailOrPhone;
            var customerExists = _customerService.GetCustomerByPhone(emailOrPhone);
            if (customerExists != null)
            {
                string sRandomOTP = _workContext.GenerateOTP;

                _genericAttributeService.SaveAttribute(customerExists,
                "ForgotOTP", sRandomOTP, _workContext.GetCurrentStoreId);

                _workflowMessageService.SendCustomerForgotOTPOnPhone(customerExists, emailOrPhone, _workContext.WorkingLanguage.Id, sRandomOTP);

            }
            else if (customerEmailExists != null)
            {
                string sRandomOTP = _workContext.GenerateOTP;

                _genericAttributeService.SaveAttribute(customerEmailExists,
                "ForgotOTP", sRandomOTP, _workContext.GetCurrentStoreId);
                _workflowMessageService.SendCustomerForgotOTP(customerEmailExists, emailOrPhone, _workContext.WorkingLanguage.Id, sRandomOTP);

            }
        }

        protected virtual string ParseCustomCustomerAttributes(IFormCollection form)
        {
            if (form == null)
                throw new ArgumentNullException(nameof(form));

            var attributesXml = "";
            var attributes = _customerAttributeService.GetAllCustomerAttributes();
            foreach (var attribute in attributes)
            {
                var controlId = $"{NopAttributePrefixDefaults.Customer}{attribute.Id}";
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!StringValues.IsNullOrEmpty(ctrlAttributes))
                            {
                                var selectedAttributeId = int.Parse(ctrlAttributes);
                                if (selectedAttributeId > 0)
                                    attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
                                        attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.Checkboxes:
                        {
                            var cblAttributes = form[controlId];
                            if (!StringValues.IsNullOrEmpty(cblAttributes))
                            {
                                foreach (var item in cblAttributes.ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    var selectedAttributeId = int.Parse(item);
                                    if (selectedAttributeId > 0)
                                        attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
                                            attribute, selectedAttributeId.ToString());
                                }
                            }
                        }
                        break;
                    case AttributeControlType.ReadonlyCheckboxes:
                        {
                            //load read-only (already server-side selected) values
                            var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                            foreach (var selectedAttributeId in attributeValues
                                .Where(v => v.IsPreSelected)
                                .Select(v => v.Id)
                                .ToList())
                            {
                                attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
                                    attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!StringValues.IsNullOrEmpty(ctrlAttributes))
                            {
                                var enteredText = ctrlAttributes.ToString().Trim();
                                attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
                                    attribute, enteredText);
                            }
                        }
                        break;
                    case AttributeControlType.Datepicker:
                    case AttributeControlType.ColorSquares:
                    case AttributeControlType.ImageSquares:
                    case AttributeControlType.FileUpload:
                    //not supported customer attributes
                    default:
                        break;
                }
            }

            return attributesXml;
        }

        protected virtual void LogGdpr(Customer customer, CustomerInfoModel oldCustomerInfoModel,
            CustomerInfoModel newCustomerInfoModel, IFormCollection form)
        {
            try
            {
                //consents
                var consents = _gdprService.GetAllConsents().Where(consent => consent.DisplayOnCustomerInfoPage).ToList();
                foreach (var consent in consents)
                {
                    var previousConsentValue = _gdprService.IsConsentAccepted(consent.Id, _workContext.CurrentCustomer.Id);
                    var controlId = $"consent{consent.Id}";
                    var cbConsent = form[controlId];
                    if (!StringValues.IsNullOrEmpty(cbConsent) && cbConsent.ToString().Equals("on"))
                    {
                        //agree
                        if (!previousConsentValue.HasValue || !previousConsentValue.Value)
                        {
                            _gdprService.InsertLog(customer, consent.Id, GdprRequestType.ConsentAgree, consent.Message);
                        }
                    }
                    else
                    {
                        //disagree
                        if (!previousConsentValue.HasValue || previousConsentValue.Value)
                        {
                            _gdprService.InsertLog(customer, consent.Id, GdprRequestType.ConsentDisagree, consent.Message);
                        }
                    }
                }

                //newsletter subscriptions
                if (_gdprSettings.LogNewsletterConsent)
                {
                    if (oldCustomerInfoModel.Newsletter && !newCustomerInfoModel.Newsletter)
                        _gdprService.InsertLog(customer, 0, GdprRequestType.ConsentDisagree, _localizationService.GetResource("Gdpr.Consent.Newsletter"));
                    if (!oldCustomerInfoModel.Newsletter && newCustomerInfoModel.Newsletter)
                        _gdprService.InsertLog(customer, 0, GdprRequestType.ConsentAgree, _localizationService.GetResource("Gdpr.Consent.Newsletter"));
                }

                //user profile changes
                if (!_gdprSettings.LogUserProfileChanges)
                    return;

                if (oldCustomerInfoModel.Gender != newCustomerInfoModel.Gender)
                    _gdprService.InsertLog(customer, 0, GdprRequestType.ProfileChanged, $"{_localizationService.GetResource("Account.Fields.Gender")} = {newCustomerInfoModel.Gender}");

                if (oldCustomerInfoModel.FirstName != newCustomerInfoModel.FirstName)
                    _gdprService.InsertLog(customer, 0, GdprRequestType.ProfileChanged, $"{_localizationService.GetResource("Account.Fields.FirstName")} = {newCustomerInfoModel.FirstName}");

                if (oldCustomerInfoModel.LastName != newCustomerInfoModel.LastName)
                    _gdprService.InsertLog(customer, 0, GdprRequestType.ProfileChanged, $"{_localizationService.GetResource("Account.Fields.LastName")} = {newCustomerInfoModel.LastName}");

                if (oldCustomerInfoModel.ParseDateOfBirth() != newCustomerInfoModel.ParseDateOfBirth())
                    _gdprService.InsertLog(customer, 0, GdprRequestType.ProfileChanged, $"{_localizationService.GetResource("Account.Fields.DateOfBirth")} = {newCustomerInfoModel.ParseDateOfBirth().ToString()}");

                if (oldCustomerInfoModel.Email != newCustomerInfoModel.Email)
                    _gdprService.InsertLog(customer, 0, GdprRequestType.ProfileChanged, $"{_localizationService.GetResource("Account.Fields.Email")} = {newCustomerInfoModel.Email}");

                if (oldCustomerInfoModel.Company != newCustomerInfoModel.Company)
                    _gdprService.InsertLog(customer, 0, GdprRequestType.ProfileChanged, $"{_localizationService.GetResource("Account.Fields.Company")} = {newCustomerInfoModel.Company}");

                if (oldCustomerInfoModel.StreetAddress != newCustomerInfoModel.StreetAddress)
                    _gdprService.InsertLog(customer, 0, GdprRequestType.ProfileChanged, $"{_localizationService.GetResource("Account.Fields.StreetAddress")} = {newCustomerInfoModel.StreetAddress}");

                if (oldCustomerInfoModel.StreetAddress2 != newCustomerInfoModel.StreetAddress2)
                    _gdprService.InsertLog(customer, 0, GdprRequestType.ProfileChanged, $"{_localizationService.GetResource("Account.Fields.StreetAddress2")} = {newCustomerInfoModel.StreetAddress2}");

                if (oldCustomerInfoModel.ZipPostalCode != newCustomerInfoModel.ZipPostalCode)
                    _gdprService.InsertLog(customer, 0, GdprRequestType.ProfileChanged, $"{_localizationService.GetResource("Account.Fields.ZipPostalCode")} = {newCustomerInfoModel.ZipPostalCode}");

                if (oldCustomerInfoModel.City != newCustomerInfoModel.City)
                    _gdprService.InsertLog(customer, 0, GdprRequestType.ProfileChanged, $"{_localizationService.GetResource("Account.Fields.City")} = {newCustomerInfoModel.City}");

                if (oldCustomerInfoModel.County != newCustomerInfoModel.County)
                    _gdprService.InsertLog(customer, 0, GdprRequestType.ProfileChanged, $"{_localizationService.GetResource("Account.Fields.County")} = {newCustomerInfoModel.County}");

                if (oldCustomerInfoModel.CountryId != newCustomerInfoModel.CountryId)
                {
                    var countryName = _countryService.GetCountryById(newCustomerInfoModel.CountryId)?.Name;
                    _gdprService.InsertLog(customer, 0, GdprRequestType.ProfileChanged, $"{_localizationService.GetResource("Account.Fields.Country")} = {countryName}");
                }

                if (oldCustomerInfoModel.StateProvinceId != newCustomerInfoModel.StateProvinceId)
                {
                    var stateProvinceName = _stateProvinceService.GetStateProvinceById(newCustomerInfoModel.StateProvinceId)?.Name;
                    _gdprService.InsertLog(customer, 0, GdprRequestType.ProfileChanged, $"{_localizationService.GetResource("Account.Fields.StateProvince")} = {stateProvinceName}");
                }
            }
            catch (Exception exception)
            {
                _logger.Error(exception.Message, exception, customer);
            }
        }

        #endregion

        #region Methods

        #region Login / logout

        [HttpsRequirement(SslRequirement.Yes)]
        //available even when a store is closed
        [CheckAccessClosedStore(true)]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult Login(bool? checkoutAsGuest)
        {
            var model = _customerModelFactory.PrepareLoginModel(checkoutAsGuest);
            return View(model);
        }

        [HttpPost]
        [ValidateCaptcha]
        //available even when a store is closed
        [CheckAccessClosedStore(true)]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        [PublicAntiForgery]
        public virtual IActionResult Login(LoginModel model, string returnUrl, bool captchaValid)
        {
            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnLoginPage && !captchaValid)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptchaMessage"));
            }

            if (ModelState.IsValid)
            {
                if (_customerSettings.UsernamesEnabled && model.Username != null)
                {
                    model.Username = model.Username.Trim();
                }

                //check enable otp option
                var emailOrPhoneStr = model.Email;
                if (_storeInformationSettings.EnableOTPAuthentication)
                {
                    emailOrPhoneStr = model.EmailOrPhone;
                    var emails = string.Empty;

                    if (emailOrPhoneStr.Contains("@"))
                    {
                        emails = emailOrPhoneStr;
                    }
                    else
                    {
                        var currentStoreUrl = _storeContext.CurrentStore.Url;
                        var isdCode = _commonService.GetISDCode(_workContext.GetCurrentStoreId, false).FirstOrDefault();
                        string url = currentStoreUrl.Split("/")[2];
                        string number = emailOrPhoneStr.Split(' ')[1];
                        emails = number + "@" + url;
                        _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, NopCustomerDefaults.PhoneAttribute, number, _workContext.GetCurrentStoreId);

                    }

                    var customer = _customerService.GetCustomerByEmail(emails);

                    if (customer == null)
                    {
                        customer = new Customer
                        {
                            CustomerGuid = Guid.NewGuid(),
                            Email = emails,
                            Active = true,
                            CreatedOnUtc = DateTime.UtcNow,
                            LastActivityDateUtc = DateTime.UtcNow,
                            RegisteredInStoreId = _workContext.GetCurrentStoreId // _storeContext.CurrentStore.Id
                        };
                        _customerService.InsertCustomer(customer);
                    }
                    var customerRegister = customer.IsRegistered();
                    if (!customerRegister)
                    {
                        var register = _customerService.GetCustomerRoleBySystemName("Registered");
                        if (register != null)
                        {
                            customer.CustomerCustomerRoleMappings.Add(new CustomerCustomerRoleMapping { CustomerRole = register });
                            _customerService.UpdateCustomer(customer);
                        }
                    }
                    RegistrationOTP(customer);
                    return RedirectToAction("OTPVerification", new { id = customer.Id });
                }

                //assign emailOrPhone as we are now using email/phone login functionality T6966
                emailOrPhoneStr = model.EmailOrPhone;
                var loginResult = _customerRegistrationService.ValidateCustomer(_customerSettings.UsernamesEnabled ? model.Username : emailOrPhoneStr, model.Password);

                switch (loginResult)
                {
                    case CustomerLoginResults.Successful:
                        {
                            var customer = _customerSettings.UsernamesEnabled
                                ? _customerService.GetCustomerByUsername(model.Username)
                                : _customerService.GetCustomerByEmailOrPhone(emailOrPhoneStr);

                            //migrate shopping cart
                            _shoppingCartService.MigrateShoppingCart(_workContext.CurrentCustomer, customer, true);
                            //Custom code from v4.0
                            var orderDetails = _workContext.CurrentOrderDetails;
                            orderDetails.CustomerId = customer.Id;
                            _workContext.CurrentOrderDetails = orderDetails;

                            //sign in new customer
                            _authenticationService.SignIn(customer, model.RememberMe);
                            _staticCacheManager.Clear();

                            //raise event       
                            _eventPublisher.Publish(new CustomerLoggedinEvent(customer));

                            //activity log
                            _customerActivityService.InsertActivity(customer, "PublicStore.Login",
                                _localizationService.GetResource("ActivityLog.PublicStore.Login"), customer);

                            //firebase - vendor tokens
                            if (customer.IsVendor() && !string.IsNullOrEmpty(model.FCMToken))
                            {
                                var vendorToken = new NBVendorToken();
                                vendorToken.CustomerId = customer.Id;
                                vendorToken.Token = model.FCMToken;
                                vendorToken.Deleted = false;
                                var tokenExist = _nBVendorTokenService.GetNBVendorTokenByToken(model.FCMToken);
                                if (tokenExist == null)
                                    _nBVendorTokenService.InsertNBVendorToken(vendorToken);
                            }

                            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                                return Redirect("/");

                            return Redirect(returnUrl);
                        }
                    case CustomerLoginResults.CustomerNotExist:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));
                        break;
                    case CustomerLoginResults.Deleted:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.Deleted"));
                        break;
                    case CustomerLoginResults.NotActive:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotActive"));
                        break;
                    case CustomerLoginResults.NotRegistered:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered"));
                        break;
                    case CustomerLoginResults.LockedOut:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.LockedOut"));
                        break;
                    case CustomerLoginResults.WrongPassword:
                    default:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials"));
                        break;
                }
            }

            //If we got this far, something failed, redisplay form
            model = _customerModelFactory.PrepareLoginModel(model.CheckoutAsGuest);
            return View(model);
        }
        //available even when a store is closed
        [CheckAccessClosedStore(true)]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult Logout()
        {
            if (_workContext.OriginalCustomerIfImpersonated != null)
            {
                //activity log
                _customerActivityService.InsertActivity(_workContext.OriginalCustomerIfImpersonated, "Impersonation.Finished",
                    string.Format(_localizationService.GetResource("ActivityLog.Impersonation.Finished.StoreOwner"),
                        _workContext.CurrentCustomer.Email, _workContext.CurrentCustomer.Id),
                    _workContext.CurrentCustomer);

                _customerActivityService.InsertActivity("Impersonation.Finished",
                    string.Format(_localizationService.GetResource("ActivityLog.Impersonation.Finished.Customer"),
                        _workContext.OriginalCustomerIfImpersonated.Email, _workContext.OriginalCustomerIfImpersonated.Id),
                    _workContext.OriginalCustomerIfImpersonated);

                //logout impersonated customer
                _genericAttributeService
                    .SaveAttribute<int?>(_workContext.OriginalCustomerIfImpersonated, NopCustomerDefaults.ImpersonatedCustomerIdAttribute, null);

                //redirect back to customer details page (admin area)
                return RedirectToAction("Edit", "Customer", new { id = _workContext.CurrentCustomer.Id, area = AreaNames.Admin });
            }

            //activity log
            _customerActivityService.InsertActivity(_workContext.CurrentCustomer, "PublicStore.Logout",
                _localizationService.GetResource("ActivityLog.PublicStore.Logout"), _workContext.CurrentCustomer);

            //standard logout 
            _authenticationService.SignOut();

            //raise logged out event       
            _eventPublisher.Publish(new CustomerLoggedOutEvent(_workContext.CurrentCustomer));

            //EU Cookie
            if (_storeInformationSettings.DisplayEuCookieLawWarning)
            {
                //the cookie law message should not pop up immediately after logout.
                //otherwise, the user will have to click it again...
                //and thus next visitor will not click it... so violation for that cookie law..
                //the only good solution in this case is to store a temporary variable
                //indicating that the EU cookie popup window should not be displayed on the next page open (after logout redirection to homepage)
                //but it'll be displayed for further page loads
                TempData[$"{NopCookieDefaults.Prefix}{NopCookieDefaults.IgnoreEuCookieLawWarning}"] = true;
            }

            return RedirectToRoute("Homepage");
        }

        #endregion

        #region Password recovery

        [HttpsRequirement(SslRequirement.Yes)]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult PasswordRecovery()
        {
            var model = _customerModelFactory.PreparePasswordRecoveryModel();
            return View(model);
        }

        [ValidateCaptcha]
        [HttpPost, ActionName("PasswordRecovery")]
        [PublicAntiForgery]
        [FormValueRequired("send-email")]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult PasswordRecoverySend(PasswordRecoveryModel model, bool captchaValid)
        {
            // validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnForgotPasswordPage && !captchaValid)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptchaMessage"));
            }

            if (ModelState.IsValid)
            {
                //check enable otp option
                var emailOrPassword = model.Email;
                if (_storeInformationSettings.EnableOTPAuthentication)
                {
                    emailOrPassword = model.EmailOrPhone;
                }
                var customer = _customerService.GetCustomerByEmail(emailOrPassword);
                if (_storeInformationSettings.EnableOTPAuthentication)
                {
                    if (customer == null)
                        customer = _customerService.GetCustomerByPhone(_workContext.CurrentStoreISDCode + " " + emailOrPassword);
                    if (customer != null)
                    {
                        ForgotOTP(emailOrPassword);
                        return RedirectToAction("PasswordRcoveryOTPVerification", new { id = customer.Id });
                    }
                }
                if (customer != null && customer.Active && !customer.Deleted)
                {
                    //save token and current date
                    var passwordRecoveryToken = Guid.NewGuid();
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PasswordRecoveryTokenAttribute,
                        passwordRecoveryToken.ToString());
                    DateTime? generatedDateTime = DateTime.UtcNow;
                    _genericAttributeService.SaveAttribute(customer,
                        NopCustomerDefaults.PasswordRecoveryTokenDateGeneratedAttribute, generatedDateTime);

                    //send email
                    _workflowMessageService.SendCustomerPasswordRecoveryMessage(customer,
                        _workContext.WorkingLanguage.Id);

                    model.Result = _localizationService.GetResource("Account.PasswordRecovery.EmailHasBeenSent");
                }
                else
                {
                    model.Result = _localizationService.GetResource("Account.PasswordRecovery.EmailNotFound");
                    model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnForgotPasswordPage;
                    model.OTPVerificationEnabled = _storeInformationSettings.EnableOTPAuthentication;
                }

                return View(model);
            }
            else
            {
                model.Result = _localizationService.GetResource("NB.Customer.Account.PasswordRecovery.ModelState.Error");
                model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnForgotPasswordPage;
                model.OTPVerificationEnabled = _storeInformationSettings.EnableOTPAuthentication;
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult PasswordRcoveryOTPVerification(int id)
        {
            var customer = _customerService.GetCustomerById(id);
            var model = new OTPVerificationModel();
            model.CustomerId = id;
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult PasswordRcoveryOTPVerification(OTPVerificationModel model)
        {
            var customer = _customerService.GetCustomerById(model.CustomerId);
            if (customer == null)
                return RedirectToRoute("Homepage");

            var phone = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute, customer.RegisteredInStoreId, "");
            if (!phone.Contains(" "))
                phone = _workContext.CurrentStoreISDCode + " " + phone;
            var otpPhone = _genericAttributeService.GetAttribute<string>(customer, "ForgotOTP", _workContext.GetCurrentStoreId);
            if (string.IsNullOrEmpty(model.OTP))
            {
                ModelState.AddModelError("", _localizationService.GetResource("Registration.LoginSignup.EnterOTP"));
            }
            else if (otpPhone != model.OTP)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Checkout.LoginSignup.InvalidOTP.Validate"));
            }

            //return model if errors
            if (ModelState.ErrorCount > 0)
            {
                return View(model);
            }

            if (customer != null && customer.Active && !customer.Deleted)
            {
                //save token and current date
                var passwordRecoveryToken = Guid.NewGuid();
                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PasswordRecoveryTokenAttribute,
                    passwordRecoveryToken.ToString());
                DateTime? generatedDateTime = DateTime.UtcNow;
                _genericAttributeService.SaveAttribute(customer,
                    NopCustomerDefaults.PasswordRecoveryTokenDateGeneratedAttribute, generatedDateTime);

                //send email
                _workflowMessageService.SendCustomerPasswordRecoveryMessage(customer,
                    _workContext.WorkingLanguage.Id);

                //send sms
                _workflowMessageService.SendCustomerPasswordRecoveryMessageOnPhone(customer, phone, _workContext.WorkingLanguage.Id);

                model.Result = _localizationService.GetResource("Account.PasswordRecovery.EmailHasBeenSent");
            }
            else
            {
                ModelState.AddModelError("", _localizationService.GetResource("Account.PasswordRecovery.UserNotFound"));
            }
            return View(model);
        }

        [HttpsRequirement(SslRequirement.Yes)]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult PasswordRecoveryConfirm(string token, string email)
        {
            var customer = _customerService.GetCustomerByEmail(email);
            if (customer == null)
                return RedirectToRoute("Homepage");

            if (string.IsNullOrEmpty(_genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PasswordRecoveryTokenAttribute)))
            {
                return View(new PasswordRecoveryConfirmModel
                {
                    DisablePasswordChanging = true,
                    Result = _localizationService.GetResource("Account.PasswordRecovery.PasswordAlreadyHasBeenChanged")
                });
            }

            var model = _customerModelFactory.PreparePasswordRecoveryConfirmModel();

            //validate token
            if (!_customerService.IsPasswordRecoveryTokenValid(customer, token))
            {
                model.DisablePasswordChanging = true;
                model.Result = _localizationService.GetResource("Account.PasswordRecovery.WrongToken");
            }

            //validate token expiration date
            if (_customerService.IsPasswordRecoveryLinkExpired(customer))
            {
                model.DisablePasswordChanging = true;
                model.Result = _localizationService.GetResource("Account.PasswordRecovery.LinkExpired");
            }

            return View(model);
        }

        [HttpPost, ActionName("PasswordRecoveryConfirm")]
        [PublicAntiForgery]
        [FormValueRequired("set-password")]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult PasswordRecoveryConfirmPOST(string token, string email, PasswordRecoveryConfirmModel model)
        {
            var customer = _customerService.GetCustomerByEmail(email);
            if (customer == null)
                return RedirectToRoute("Homepage");

            //validate token
            if (!_customerService.IsPasswordRecoveryTokenValid(customer, token))
            {
                model.DisablePasswordChanging = true;
                model.Result = _localizationService.GetResource("Account.PasswordRecovery.WrongToken");
                return View(model);
            }

            //validate token expiration date
            if (_customerService.IsPasswordRecoveryLinkExpired(customer))
            {
                model.DisablePasswordChanging = true;
                model.Result = _localizationService.GetResource("Account.PasswordRecovery.LinkExpired");
                return View(model);
            }

            if (ModelState.IsValid)
            {
                var response = _customerRegistrationService.ChangePassword(new ChangePasswordRequest(email,
                    false, _customerSettings.DefaultPasswordFormat, model.NewPassword));
                if (response.Success)
                {
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PasswordRecoveryTokenAttribute, "");

                    model.DisablePasswordChanging = true;
                    model.Result = _localizationService.GetResource("Account.PasswordRecovery.PasswordHasBeenChanged");
                }
                else
                {
                    model.Result = response.Errors.FirstOrDefault();
                }

                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult ResendOTP(int customerId)
        {
            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                return Json(new { success = false });

            RegistrationOTP(customer);
            return Json(new { success = true });
        }

        #endregion     

        #region Register

        #region Register Customer

        [HttpsRequirement(SslRequirement.Yes)]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult Register()
        {
            //check whether registration is allowed
            if (_customerSettings.UserRegistrationType == UserRegistrationType.Disabled)
                return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.Disabled });

            var model = new RegisterModel();
            model = _customerModelFactory.PrepareRegisterModel(model, false, setDefaultValues: true);
            model.RegisterationType = RegistrationCustomerType.Customer;
            model.CompanySale = "Food";
            model.RestaurantRegisterAllow = _settingService.GetSettingByKey<bool>("setting.storesetup.RestaurantRegisterAllow", false, _storeContext.CurrentStore.Id);
            model.StoreRegisterAllow = _settingService.GetSettingByKey<bool>("Setting.StoreSetup.StoreRegisterAllow", false, _storeContext.CurrentStore.Id);
            return View(model);
        }

        [HttpPost]
        [ValidateCaptcha]
        [ValidateHoneypot]
        [PublicAntiForgery]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult Register(RegisterModel model, string returnUrl, bool captchaValid, IFormCollection form)
        {
            //check whether registration is allowed
            if (_customerSettings.UserRegistrationType == UserRegistrationType.Disabled)
                return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.Disabled });

            if (_workContext.CurrentCustomer.IsRegistered())
            {
                //Already registered customer. 
                _authenticationService.SignOut();

                //raise logged out event       
                _eventPublisher.Publish(new CustomerLoggedOutEvent(_workContext.CurrentCustomer));

                //Save a new record
                _workContext.CurrentCustomer = _customerService.InsertGuestCustomer();
            }
            var customer = _workContext.CurrentCustomer;
            customer.RegisteredInStoreId = _storeContext.CurrentStore.Id;

            //custom customer attributes
            var customerAttributesXml = ParseCustomCustomerAttributes(form);
            var customerAttributeWarnings = _customerAttributeParser.GetAttributeWarnings(customerAttributesXml);
            foreach (var error in customerAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnRegistrationPage && !captchaValid)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptchaMessage"));
            }

            if (ModelState.IsValid)
            {
                if (_customerSettings.UsernamesEnabled && model.Username != null)
                {
                    model.Username = model.Username.Trim();
                }

                var isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard && (model.RegisterationType == RegistrationCustomerType.Customer);
                if (_storeInformationSettings.EnableOTPAuthentication)
                    isApproved = false;
                var registrationRequest = new CustomerRegistrationRequest(customer,
                    model.Email,
                    _customerSettings.UsernamesEnabled ? model.Username : model.Email,
                    model.Password,
                    _customerSettings.DefaultPasswordFormat,
                    _storeContext.CurrentStore.Id,
                    isApproved);
                //For checking Phone No 16-05-2019
                if (!string.IsNullOrEmpty(model.Phone))
                {
                    var attributes = _genericAttributeService.GetAttributesForPhoneEntity(model.ISDId + " " + model.Phone, customer.RegisteredInStoreId);
                    if (attributes.Count > 0)

                    {
                        var result = new CustomerRegistrationResult();
                        result.AddError("Mobile number is already exist!!");
                        if (!result.Success)
                        {
                            foreach (var error in result.Errors)
                                ModelState.AddModelError("", error);
                            model = _customerModelFactory.PrepareRegisterModel(model, true, customerAttributesXml);
                            return View(model);
                        }

                    }
                }

                //validate venue name and venue url
                if (model.RegisterationType == RegistrationCustomerType.StoreOwner)
                {
                    var venueNameExist = VenueNameExist(model.VenueName);
                    var venueUrlHostExist = VenueUrlHostExist(model.VenueURL);

                    var result = new CustomerRegistrationResult();

                    if (venueNameExist)
                        result.AddError(_localizationService.GetResource("NB.Account.Fields.Store.VenueNameAlreadyExist"));

                    if (venueUrlHostExist)
                        result.AddError(_localizationService.GetResource("NB.Account.Fields.Store.VenueUrlAlreadyExist"));


                    if (!result.Success)
                    {
                        foreach (var error in result.Errors)
                            ModelState.AddModelError("", error);
                        model = _customerModelFactory.PrepareRegisterModel(model, true, customerAttributesXml);
                        return View(model);
                    }
                }

                //End For checking Phone No 16-05-2019
                var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);
                if (registrationResult.Success)
                {

                    //properties
                    if (_dateTimeSettings.AllowCustomersToSetTimeZone)
                    {
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.TimeZoneIdAttribute, model.TimeZoneId, customer.RegisteredInStoreId);
                    }
                    //VAT number
                    if (_taxSettings.EuVatEnabled)
                    {
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.VatNumberAttribute, model.VatNumber, customer.RegisteredInStoreId);

                        var vatNumberStatus = _taxService.GetVatNumberStatus(model.VatNumber, out string _, out string vatAddress);
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.VatNumberStatusIdAttribute, (int)vatNumberStatus, customer.RegisteredInStoreId);
                        //send VAT number admin notification
                        if (!string.IsNullOrEmpty(model.VatNumber) && _taxSettings.EuVatEmailAdminWhenNewVatSubmitted)
                            _workflowMessageService.SendNewVatSubmittedStoreOwnerNotification(customer, model.VatNumber, vatAddress, _localizationSettings.DefaultAdminLanguageId);
                    }

                    //form fields
                    if (_customerSettings.GenderEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.GenderAttribute, model.Gender, customer.RegisteredInStoreId);
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FirstNameAttribute, model.FirstName, customer.RegisteredInStoreId);
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.LastNameAttribute, model.LastName, customer.RegisteredInStoreId);
                    if (_customerSettings.DateOfBirthEnabled)
                    {
                        var dateOfBirth = model.ParseDateOfBirth();
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.DateOfBirthAttribute, dateOfBirth, customer.RegisteredInStoreId);
                    }
                    if (_customerSettings.CompanyEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CompanyAttribute, model.Company, customer.RegisteredInStoreId);
                    if (_customerSettings.StreetAddressEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StreetAddressAttribute, model.StreetAddress, customer.RegisteredInStoreId);
                    if (_customerSettings.StreetAddress2Enabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StreetAddress2Attribute, model.StreetAddress2, customer.RegisteredInStoreId);
                    if (_customerSettings.ZipPostalCodeEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.ZipPostalCodeAttribute, model.ZipPostalCode, customer.RegisteredInStoreId);
                    if (_customerSettings.CityEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CityAttribute, model.City, customer.RegisteredInStoreId);
                    if (_customerSettings.CountyEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CountyAttribute, model.County, customer.RegisteredInStoreId);
                    if (_customerSettings.CountryEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CountryIdAttribute, model.CountryId, customer.RegisteredInStoreId);
                    if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StateProvinceIdAttribute,
                            model.StateProvinceId);
                    if (_customerSettings.PhoneEnabled)
                    {
                        if (model.ISDId != null && model.ISDId > 0)
                        {
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, (model.ISDId.ToString() + " " + model.Phone).Trim(), customer.RegisteredInStoreId); //added customer.RegisteredInStoreId
                        }
                        else
                        {
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, model.Phone, customer.RegisteredInStoreId);
                        }
                    }
                    if (_customerSettings.FaxEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FaxAttribute, model.Fax, customer.RegisteredInStoreId);

                    //newsletter
                    if (_customerSettings.NewsletterEnabled)
                    {
                        //save newsletter value
                        var newsletter = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(model.Email, customer.RegisteredInStoreId);
                        if (newsletter != null)
                        {
                            if (model.Newsletter)
                            {
                                newsletter.Active = true;
                                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(newsletter);

                                //GDPR
                                if (_gdprSettings.GdprEnabled && _gdprSettings.LogNewsletterConsent)
                                {
                                    _gdprService.InsertLog(customer, 0, GdprRequestType.ConsentAgree, _localizationService.GetResource("Gdpr.Consent.Newsletter"));
                                }
                            }
                            //else
                            //{
                            //When registering, not checking the newsletter check box should not take an existing email address off of the subscription list.
                            //_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletter);
                            //}
                        }
                        else
                        {
                            if (model.Newsletter)
                            {
                                _newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
                                {
                                    NewsLetterSubscriptionGuid = Guid.NewGuid(),
                                    Email = model.Email,
                                    Active = true,
                                    StoreId = customer.RegisteredInStoreId,
                                    CreatedOnUtc = DateTime.UtcNow
                                });

                                //GDPR
                                if (_gdprSettings.GdprEnabled && _gdprSettings.LogNewsletterConsent)
                                {
                                    _gdprService.InsertLog(customer, 0, GdprRequestType.ConsentAgree, _localizationService.GetResource("Gdpr.Consent.Newsletter"));
                                }
                            }
                        }
                    }

                    if (_customerSettings.AcceptPrivacyPolicyEnabled)
                    {
                        //privacy policy is required
                        //GDPR
                        if (_gdprSettings.GdprEnabled && _gdprSettings.LogPrivacyPolicyConsent)
                        {
                            _gdprService.InsertLog(customer, 0, GdprRequestType.ConsentAgree, _localizationService.GetResource("Gdpr.Consent.PrivacyPolicy"));
                        }
                    }

                    //GDPR
                    if (_gdprSettings.GdprEnabled)
                    {
                        var consents = _gdprService.GetAllConsents().Where(consent => consent.DisplayDuringRegistration).ToList();
                        foreach (var consent in consents)
                        {
                            var controlId = $"consent{consent.Id}";
                            var cbConsent = form[controlId];
                            if (!StringValues.IsNullOrEmpty(cbConsent) && cbConsent.ToString().Equals("on"))
                            {
                                //agree
                                _gdprService.InsertLog(customer, consent.Id, GdprRequestType.ConsentAgree, consent.Message);
                            }
                            else
                            {
                                //disagree
                                _gdprService.InsertLog(customer, consent.Id, GdprRequestType.ConsentDisagree, consent.Message);
                            }
                        }
                    }

                    //save customer attributes
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CustomCustomerAttributes, customerAttributesXml, customer.RegisteredInStoreId);

                    //login customer now
                    if (isApproved)
                        _authenticationService.SignIn(customer, true);

                    //insert default address (if possible)
                    var defaultAddress = new Address
                    {
                        FirstName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute, customer.RegisteredInStoreId),
                        LastName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute, customer.RegisteredInStoreId),
                        Email = customer.Email,
                        Company = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CompanyAttribute, customer.RegisteredInStoreId),
                        County = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CountyAttribute, customer.RegisteredInStoreId),
                        Address1 = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.StreetAddressAttribute, customer.RegisteredInStoreId),
                        Address2 = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.StreetAddress2Attribute, customer.RegisteredInStoreId),
                        ZipPostalCode = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.ZipPostalCodeAttribute, customer.RegisteredInStoreId),
                        PhoneNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute, customer.RegisteredInStoreId),
                        FaxNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FaxAttribute, customer.RegisteredInStoreId),
                        CreatedOnUtc = customer.CreatedOnUtc
                    };

                    if (model.RegisterationType == RegistrationCustomerType.Agent)
                    {
                        defaultAddress.CountryId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderCountryIdAttribute, customer.RegisteredInStoreId) > 0
                            ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderCountryIdAttribute, customer.RegisteredInStoreId)
                            : null;
                        defaultAddress.StateProvinceId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderStateProvinceIdAttribute, customer.RegisteredInStoreId) > 0
                            ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderStateProvinceIdAttribute, customer.RegisteredInStoreId)
                            : null;
                        defaultAddress.City = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.AgentRiderCityAttribute, customer.RegisteredInStoreId);
                    }
                    else
                    {
                        defaultAddress.CountryId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderCountryIdAttribute) > 0
                            ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderCountryIdAttribute)
                            : null;
                        defaultAddress.StateProvinceId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderStateProvinceIdAttribute) > 0
                            ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderStateProvinceIdAttribute)
                            : null;
                        defaultAddress.City = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.AgentRiderCityAttribute, customer.RegisteredInStoreId);
                    }

                    //raise event       
                    _eventPublisher.Publish(new CustomerRegisteredEvent(customer));

                    //notifications
                    if (_customerSettings.NotifyNewCustomerRegistration)
                        _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer,
                            _localizationSettings.DefaultAdminLanguageId);

                    if (_storeInformationSettings.EnableOTPAuthentication)
                    {
                        RegistrationOTP(customer);
                        return RedirectToAction("OTPVerification", new { id = customer.Id });
                    }

                    //notifications
                    if (_customerSettings.NotifyNewCustomerRegistration)
                        _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer,
                            _localizationSettings.DefaultAdminLanguageId);

                    switch (_customerSettings.UserRegistrationType)
                    {
                        case UserRegistrationType.EmailValidation:
                            {
                                //email validation message
                                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenAttribute, Guid.NewGuid().ToString(), customer.RegisteredInStoreId);
                                _workflowMessageService.SendCustomerEmailValidationMessage(customer, _workContext.WorkingLanguage.Id);

                                //result
                                return RedirectToRoute("RegisterResult",
                                    new { resultId = (int)UserRegistrationType.EmailValidation });
                            }
                        case UserRegistrationType.AdminApproval:
                            {
                                return RedirectToRoute("RegisterResult",
                                    new { resultId = (int)UserRegistrationType.AdminApproval });
                            }
                        case UserRegistrationType.Standard:
                            {
                                //send customer welcome message
                                _workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

                                var redirectUrl = Url.RouteUrl("RegisterResult",
                                    new { resultId = (int)UserRegistrationType.Standard, returnUrl }, _webHelper.CurrentRequestProtocol);
                                return Redirect(redirectUrl);
                            }
                        default:
                            {
                                return RedirectToRoute("Homepage");
                            }
                    }
                }

                //errors
                foreach (var error in registrationResult.Errors)
                    ModelState.AddModelError("", error);
            }

            //If we got this far, something failed, redisplay form
            model = _customerModelFactory.PrepareRegisterModel(model, true, customerAttributesXml);
            return View(model);
        }

        #endregion

        #region Register Agent

        [HttpsRequirement(SslRequirement.Yes)]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult RegisterAgent()
        {
            //check whether registration is allowed
            if (_customerSettings.UserRegistrationType == UserRegistrationType.Disabled)
                return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.Disabled });

            var model = new RegisterModel();
            model = _customerModelFactory.PrepareRegisterModel(model, false, setDefaultValues: true);
            model.RegisterationType = RegistrationCustomerType.Agent;
            model.CompanySale = "Food";
            model.RestaurantRegisterAllow = _settingService.GetSettingByKey<bool>("setting.storesetup.RestaurantRegisterAllow", false, _storeContext.CurrentStore.Id);
            model.StoreRegisterAllow = _settingService.GetSettingByKey<bool>("Setting.StoreSetup.StoreRegisterAllow", false, _storeContext.CurrentStore.Id);
            return View(model);
        }

        [HttpPost]
        [ValidateCaptcha]
        [ValidateHoneypot]
        [PublicAntiForgery]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult RegisterAgent(RegisterModel model, string returnUrl, bool captchaValid, IFormCollection form)
        {
            //check whether registration is allowed
            if (_customerSettings.UserRegistrationType == UserRegistrationType.Disabled)
                return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.Disabled });

            if (_workContext.CurrentCustomer.IsRegistered())
            {
                //Already registered customer. 
                _authenticationService.SignOut();

                //raise logged out event       
                _eventPublisher.Publish(new CustomerLoggedOutEvent(_workContext.CurrentCustomer));

                //Save a new record
                _workContext.CurrentCustomer = _customerService.InsertGuestCustomer();
            }
            var customer = _workContext.CurrentCustomer;
            customer.RegisteredInStoreId = _storeContext.CurrentStore.Id;

            //custom customer attributes
            var customerAttributesXml = ParseCustomCustomerAttributes(form);
            var customerAttributeWarnings = _customerAttributeParser.GetAttributeWarnings(customerAttributesXml);
            foreach (var error in customerAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnRegistrationPage && !captchaValid)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptchaMessage"));
            }

            if (ModelState.IsValid)
            {
                if (_customerSettings.UsernamesEnabled && model.Username != null)
                {
                    model.Username = model.Username.Trim();
                }

                var isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard && (model.RegisterationType == RegistrationCustomerType.Customer);
                if (_storeInformationSettings.EnableOTPAuthentication)
                    isApproved = false;
                var registrationRequest = new CustomerRegistrationRequest(customer,
                    model.Email,
                    _customerSettings.UsernamesEnabled ? model.Username : model.Email,
                    model.Password,
                    _customerSettings.DefaultPasswordFormat,
                    _storeContext.CurrentStore.Id,
                    isApproved);
                //For checking Phone No 16-05-2019
                if (!string.IsNullOrEmpty(model.Phone))
                {
                    var attributes = _genericAttributeService.GetAttributesForPhoneEntity(model.ISDId + " " + model.Phone, customer.RegisteredInStoreId);
                    if (attributes.Count > 0)

                    {
                        var result = new CustomerRegistrationResult();
                        result.AddError("Mobile number is already exist!!");
                        if (!result.Success)
                        {
                            foreach (var error in result.Errors)
                                ModelState.AddModelError("", error);
                            model = _customerModelFactory.PrepareRegisterModel(model, true, customerAttributesXml);
                            return View(model);
                        }

                    }
                }

                //validate documents for Agent/Rider signup
                if (model.RegisterationType == RegistrationCustomerType.Agent)
                {
                    var result = new CustomerRegistrationResult();

                    if (model.IdProofDocId == 0)
                        result.AddError(_localizationService.GetResource("NB.Register.Rider.Document.IdProof.Error.NotUploaded"));

                    if (model.RcBookDocId == 0)
                        result.AddError(_localizationService.GetResource("NB.Register.Rider.Document.RCBook.Error.NotUploaded"));

                    if (!result.Success)
                    {
                        foreach (var error in result.Errors)
                            ModelState.AddModelError("", error);
                        model = _customerModelFactory.PrepareRegisterModel(model, true, customerAttributesXml);
                        return View(model);
                    }
                }

                //End For checking Phone No 16-05-2019
                var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);
                if (registrationResult.Success)
                {

                    //properties
                    if (_dateTimeSettings.AllowCustomersToSetTimeZone)
                    {
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.TimeZoneIdAttribute, model.TimeZoneId, customer.RegisteredInStoreId);
                    }
                    //VAT number
                    if (_taxSettings.EuVatEnabled)
                    {
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.VatNumberAttribute, model.VatNumber, customer.RegisteredInStoreId);

                        var vatNumberStatus = _taxService.GetVatNumberStatus(model.VatNumber, out string _, out string vatAddress);
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.VatNumberStatusIdAttribute, (int)vatNumberStatus, customer.RegisteredInStoreId);
                        //send VAT number admin notification
                        if (!string.IsNullOrEmpty(model.VatNumber) && _taxSettings.EuVatEmailAdminWhenNewVatSubmitted)
                            _workflowMessageService.SendNewVatSubmittedStoreOwnerNotification(customer, model.VatNumber, vatAddress, _localizationSettings.DefaultAdminLanguageId);
                    }

                    //form fields
                    if (_customerSettings.GenderEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.GenderAttribute, model.Gender, customer.RegisteredInStoreId);
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FirstNameAttribute, model.FirstName, customer.RegisteredInStoreId);
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.LastNameAttribute, model.LastName, customer.RegisteredInStoreId);
                    if (_customerSettings.DateOfBirthEnabled)
                    {
                        var dateOfBirth = model.ParseDateOfBirth();
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.DateOfBirthAttribute, dateOfBirth, customer.RegisteredInStoreId);
                    }
                    if (_customerSettings.CompanyEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CompanyAttribute, model.Company, customer.RegisteredInStoreId);
                    if (_customerSettings.StreetAddressEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StreetAddressAttribute, model.StreetAddress, customer.RegisteredInStoreId);
                    if (_customerSettings.StreetAddress2Enabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StreetAddress2Attribute, model.StreetAddress2, customer.RegisteredInStoreId);
                    if (_customerSettings.ZipPostalCodeEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.ZipPostalCodeAttribute, model.ZipPostalCode, customer.RegisteredInStoreId);
                    if (_customerSettings.CityEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CityAttribute, model.City, customer.RegisteredInStoreId);
                    if (_customerSettings.CountyEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CountyAttribute, model.County, customer.RegisteredInStoreId);
                    if (_customerSettings.CountryEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CountryIdAttribute, model.CountryId, customer.RegisteredInStoreId);
                    if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StateProvinceIdAttribute,
                            model.StateProvinceId);
                    if (_customerSettings.PhoneEnabled)
                    {
                        if (model.ISDId != null && model.ISDId > 0)
                        {
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, (model.ISDId.ToString() + " " + model.Phone).Trim(), customer.RegisteredInStoreId); //added customer.RegisteredInStoreId
                        }
                        else
                        {
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, model.Phone, customer.RegisteredInStoreId);
                        }
                    }
                    if (_customerSettings.FaxEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FaxAttribute, model.Fax, customer.RegisteredInStoreId);

                    //newsletter
                    if (_customerSettings.NewsletterEnabled)
                    {
                        //save newsletter value
                        var newsletter = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(model.Email, customer.RegisteredInStoreId);
                        if (newsletter != null)
                        {
                            if (model.Newsletter)
                            {
                                newsletter.Active = true;
                                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(newsletter);

                                //GDPR
                                if (_gdprSettings.GdprEnabled && _gdprSettings.LogNewsletterConsent)
                                {
                                    _gdprService.InsertLog(customer, 0, GdprRequestType.ConsentAgree, _localizationService.GetResource("Gdpr.Consent.Newsletter"));
                                }
                            }
                            //else
                            //{
                            //When registering, not checking the newsletter check box should not take an existing email address off of the subscription list.
                            //_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletter);
                            //}
                        }
                        else
                        {
                            if (model.Newsletter)
                            {
                                _newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
                                {
                                    NewsLetterSubscriptionGuid = Guid.NewGuid(),
                                    Email = model.Email,
                                    Active = true,
                                    StoreId = customer.RegisteredInStoreId,
                                    CreatedOnUtc = DateTime.UtcNow
                                });

                                //GDPR
                                if (_gdprSettings.GdprEnabled && _gdprSettings.LogNewsletterConsent)
                                {
                                    _gdprService.InsertLog(customer, 0, GdprRequestType.ConsentAgree, _localizationService.GetResource("Gdpr.Consent.Newsletter"));
                                }
                            }
                        }
                    }

                    if (_customerSettings.AcceptPrivacyPolicyEnabled)
                    {
                        //privacy policy is required
                        //GDPR
                        if (_gdprSettings.GdprEnabled && _gdprSettings.LogPrivacyPolicyConsent)
                        {
                            _gdprService.InsertLog(customer, 0, GdprRequestType.ConsentAgree, _localizationService.GetResource("Gdpr.Consent.PrivacyPolicy"));
                        }
                    }

                    //GDPR
                    if (_gdprSettings.GdprEnabled)
                    {
                        var consents = _gdprService.GetAllConsents().Where(consent => consent.DisplayDuringRegistration).ToList();
                        foreach (var consent in consents)
                        {
                            var controlId = $"consent{consent.Id}";
                            var cbConsent = form[controlId];
                            if (!StringValues.IsNullOrEmpty(cbConsent) && cbConsent.ToString().Equals("on"))
                            {
                                //agree
                                _gdprService.InsertLog(customer, consent.Id, GdprRequestType.ConsentAgree, consent.Message);
                            }
                            else
                            {
                                //disagree
                                _gdprService.InsertLog(customer, consent.Id, GdprRequestType.ConsentDisagree, consent.Message);
                            }
                        }
                    }

                    //save customer attributes
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CustomCustomerAttributes, customerAttributesXml, customer.RegisteredInStoreId);

                    if (model.RegisterationType == RegistrationCustomerType.Agent)
                    {
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.RestaurantNameAttribute, model.RestaurantName, customer.RegisteredInStoreId);
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.VehicleNumberAttribute, model.VehicleNumber, customer.RegisteredInStoreId);
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AgentRiderCityIdAttribute, model.RiderCityId, customer.RegisteredInStoreId);
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AgentRiderCountryIdAttribute, model.RiderCountryId, customer.RegisteredInStoreId);
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AgentRiderStateProvinceIdAttribute, model.RiderStateProvinceId, customer.RegisteredInStoreId);
                        if (model.RiderCityId > 0)
                        {
                            var cityName = _cityService.GetCityById(model.RiderCityId).Name;
                            if (cityName != null)
                                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AgentRiderCityAttribute, cityName, customer.RegisteredInStoreId);
                        }
                    }
                    //login customer now
                    if (isApproved)
                        _authenticationService.SignIn(customer, true);

                    //insert default address (if possible)
                    var defaultAddress = new Address
                    {
                        FirstName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute, customer.RegisteredInStoreId),
                        LastName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute, customer.RegisteredInStoreId),
                        Email = customer.Email,
                        Company = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CompanyAttribute, customer.RegisteredInStoreId),
                        County = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CountyAttribute, customer.RegisteredInStoreId),
                        Address1 = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.StreetAddressAttribute, customer.RegisteredInStoreId),
                        Address2 = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.StreetAddress2Attribute, customer.RegisteredInStoreId),
                        ZipPostalCode = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.ZipPostalCodeAttribute, customer.RegisteredInStoreId),
                        PhoneNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute, customer.RegisteredInStoreId),
                        FaxNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FaxAttribute, customer.RegisteredInStoreId),
                        CreatedOnUtc = customer.CreatedOnUtc
                    };
                    if (model.RegisterationType == RegistrationCustomerType.Agent)
                    {
                        defaultAddress.CountryId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderCountryIdAttribute, customer.RegisteredInStoreId) > 0
                            ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderCountryIdAttribute, customer.RegisteredInStoreId)
                            : null;
                        defaultAddress.StateProvinceId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderStateProvinceIdAttribute, customer.RegisteredInStoreId) > 0
                            ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderStateProvinceIdAttribute, customer.RegisteredInStoreId)
                            : null;
                        defaultAddress.City = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.AgentRiderCityAttribute, customer.RegisteredInStoreId);
                    }
                    else
                    {
                        defaultAddress.CountryId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderCountryIdAttribute) > 0
                            ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderCountryIdAttribute)
                            : null;
                        defaultAddress.StateProvinceId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderStateProvinceIdAttribute) > 0
                            ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderStateProvinceIdAttribute)
                            : null;
                        defaultAddress.City = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.AgentRiderCityAttribute, customer.RegisteredInStoreId);
                    }

                    //raise event       
                    _eventPublisher.Publish(new CustomerRegisteredEvent(customer));

                    //notifications
                    if (_customerSettings.NotifyNewCustomerRegistration)
                        _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer,
                            _localizationSettings.DefaultAdminLanguageId);

                    if (_storeInformationSettings.EnableOTPAuthentication)
                    {
                        RegistrationOTP(customer);
                        return RedirectToAction("OTPVerification", new { id = customer.Id });
                    }

                    //notifications
                    if (_customerSettings.NotifyNewCustomerRegistration)
                        _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer,
                            _localizationSettings.DefaultAdminLanguageId);

                    switch (_customerSettings.UserRegistrationType)
                    {
                        case UserRegistrationType.EmailValidation:
                            {
                                //email validation message
                                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenAttribute, Guid.NewGuid().ToString(), customer.RegisteredInStoreId);
                                _workflowMessageService.SendCustomerEmailValidationMessage(customer, _workContext.WorkingLanguage.Id);

                                //result
                                return RedirectToRoute("RegisterResult",
                                    new { resultId = (int)UserRegistrationType.EmailValidation });
                            }
                        case UserRegistrationType.AdminApproval:
                            {
                                return RedirectToRoute("RegisterResult",
                                    new { resultId = (int)UserRegistrationType.AdminApproval });
                            }
                        case UserRegistrationType.Standard:
                            {
                                if (model.RegisterationType == RegistrationCustomerType.Agent)
                                {
                                    var agentRole = _customerService.GetCustomerRoleBySystemName("Agents");
                                    if (agentRole != null)
                                    {
                                        customer.CustomerCustomerRoleMappings.Add(new CustomerCustomerRoleMapping { CustomerRole = agentRole });
                                        _customerService.UpdateCustomer(customer);
                                    }

                                    //save document ids
                                    if (model.IdProofDocId > 0)
                                        _genericAttributeService.SaveAttribute(customer, NopNbDomainDefaults.AgentIdProofDocIdAttribute, model.IdProofDocId, customer.RegisteredInStoreId);
                                    if (model.RcBookDocId > 0)
                                        _genericAttributeService.SaveAttribute(customer, NopNbDomainDefaults.AgentRCBookDocIdAttribute, model.RcBookDocId, customer.RegisteredInStoreId);

                                    //send meassage to store admin to verfiy agent
                                    _workflowMessageService.SendAgentRiderVerifyMessage(customer, _workContext.WorkingLanguage.Id);

                                    return RedirectToRoute("RegisterResult",
                                       new { resultId = (int)UserRegistrationType.AdminApproval });
                                }
                                //send customer welcome message
                                _workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

                                var redirectUrl = Url.RouteUrl("RegisterResult",
                                    new { resultId = (int)UserRegistrationType.Standard, returnUrl }, _webHelper.CurrentRequestProtocol);
                                return Redirect(redirectUrl);
                            }
                        default:
                            {
                                return RedirectToRoute("Homepage");
                            }
                    }
                }

                //errors
                foreach (var error in registrationResult.Errors)
                    ModelState.AddModelError("", error);
            }

            //If we got this far, something failed, redisplay form
            model = _customerModelFactory.PrepareRegisterModel(model, true, customerAttributesXml);
            return View(model);
        }

        #endregion

        #region Register Seller/RestaurantOwner

        [HttpsRequirement(SslRequirement.Yes)]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult RegisterSeller()
        {
            //check whether registration is allowed
            if (_customerSettings.UserRegistrationType == UserRegistrationType.Disabled)
                return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.Disabled });

            var model = new RegisterModel();
            model = _customerModelFactory.PrepareRegisterModel(model, false, setDefaultValues: true);
            model.RegisterationType = RegistrationCustomerType.RestaurantOwner;
            model.CompanySale = "Food";
            model.RestaurantRegisterAllow = _settingService.GetSettingByKey<bool>("setting.storesetup.RestaurantRegisterAllow", false, _storeContext.CurrentStore.Id);
            model.StoreRegisterAllow = _settingService.GetSettingByKey<bool>("Setting.StoreSetup.StoreRegisterAllow", false, _storeContext.CurrentStore.Id);
            return View(model);
        }

        [HttpPost]
        [ValidateCaptcha]
        [ValidateHoneypot]
        [PublicAntiForgery]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult RegisterSeller(RegisterModel model, string returnUrl, bool captchaValid, IFormCollection form)
        {
            //check whether registration is allowed
            if (_customerSettings.UserRegistrationType == UserRegistrationType.Disabled)
                return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.Disabled });

            if (_workContext.CurrentCustomer.IsRegistered())
            {
                //Already registered customer. 
                _authenticationService.SignOut();

                //raise logged out event       
                _eventPublisher.Publish(new CustomerLoggedOutEvent(_workContext.CurrentCustomer));

                //Save a new record
                _workContext.CurrentCustomer = _customerService.InsertGuestCustomer();
            }
            var customer = _workContext.CurrentCustomer;
            customer.RegisteredInStoreId = _storeContext.CurrentStore.Id;

            //custom customer attributes
            var customerAttributesXml = ParseCustomCustomerAttributes(form);
            var customerAttributeWarnings = _customerAttributeParser.GetAttributeWarnings(customerAttributesXml);
            foreach (var error in customerAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnRegistrationPage && !captchaValid)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptchaMessage"));
            }

            if (ModelState.IsValid)
            {
                if (_customerSettings.UsernamesEnabled && model.Username != null)
                {
                    model.Username = model.Username.Trim();
                }

                var isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard && (model.RegisterationType == RegistrationCustomerType.Customer);
                if (_storeInformationSettings.EnableOTPAuthentication)
                    isApproved = false;
                var registrationRequest = new CustomerRegistrationRequest(customer,
                    model.Email,
                    _customerSettings.UsernamesEnabled ? model.Username : model.Email,
                    model.Password,
                    _customerSettings.DefaultPasswordFormat,
                    _storeContext.CurrentStore.Id,
                    isApproved);
                //For checking Phone No 16-05-2019
                if (!string.IsNullOrEmpty(model.Phone))
                {
                    var attributes = _genericAttributeService.GetAttributesForPhoneEntity(model.ISDId + " " + model.Phone, customer.RegisteredInStoreId);
                    if (attributes.Count > 0)

                    {
                        var result = new CustomerRegistrationResult();
                        result.AddError("Mobile number is already exist!!");
                        if (!result.Success)
                        {
                            foreach (var error in result.Errors)
                                ModelState.AddModelError("", error);
                            model = _customerModelFactory.PrepareRegisterModel(model, true, customerAttributesXml);
                            return View(model);
                        }

                    }
                }

                //validate venue name and venue url
                if (model.RegisterationType == RegistrationCustomerType.StoreOwner)
                {
                    var venueNameExist = VenueNameExist(model.VenueName);
                    var venueUrlHostExist = VenueUrlHostExist(model.VenueURL);

                    var result = new CustomerRegistrationResult();

                    if (venueNameExist)
                        result.AddError(_localizationService.GetResource("NB.Account.Fields.Store.VenueNameAlreadyExist"));

                    if (venueUrlHostExist)
                        result.AddError(_localizationService.GetResource("NB.Account.Fields.Store.VenueUrlAlreadyExist"));


                    if (!result.Success)
                    {
                        foreach (var error in result.Errors)
                            ModelState.AddModelError("", error);
                        model = _customerModelFactory.PrepareRegisterModel(model, true, customerAttributesXml);
                        return View(model);
                    }
                }

                // validate package and package plan required for resturant owner 
                if (model.RegisterationType == RegistrationCustomerType.RestaurantOwner && model.IsStoreRegisterAllowPackage)
                {
                    var result = new CustomerRegistrationResult();

                    if (model.PackageId == 0)
                        result.AddError(_localizationService.GetResource("NB.Account.Fields.SelectPackage.Required"));

                    if (model.PackagePlanId == 0)
                        result.AddError(_localizationService.GetResource("NB.Account.Fields.SelectPackagePlan.Required"));


                    if (!result.Success)
                    {
                        foreach (var error in result.Errors)
                            ModelState.AddModelError("", error);
                        model = _customerModelFactory.PrepareRegisterModel(model, true, customerAttributesXml);
                        return View(model);
                    }
                }

                //End For checking Phone No 16-05-2019
                var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);
                if (registrationResult.Success)
                {

                    //properties
                    if (_dateTimeSettings.AllowCustomersToSetTimeZone)
                    {
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.TimeZoneIdAttribute, model.TimeZoneId, customer.RegisteredInStoreId);
                    }
                    //VAT number
                    if (_taxSettings.EuVatEnabled)
                    {
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.VatNumberAttribute, model.VatNumber, customer.RegisteredInStoreId);

                        var vatNumberStatus = _taxService.GetVatNumberStatus(model.VatNumber, out string _, out string vatAddress);
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.VatNumberStatusIdAttribute, (int)vatNumberStatus, customer.RegisteredInStoreId);
                        //send VAT number admin notification
                        if (!string.IsNullOrEmpty(model.VatNumber) && _taxSettings.EuVatEmailAdminWhenNewVatSubmitted)
                            _workflowMessageService.SendNewVatSubmittedStoreOwnerNotification(customer, model.VatNumber, vatAddress, _localizationSettings.DefaultAdminLanguageId);
                    }

                    //form fields
                    if (_customerSettings.GenderEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.GenderAttribute, model.Gender, customer.RegisteredInStoreId);
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FirstNameAttribute, model.FirstName, customer.RegisteredInStoreId);
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.LastNameAttribute, model.LastName, customer.RegisteredInStoreId);
                    if (_customerSettings.DateOfBirthEnabled)
                    {
                        var dateOfBirth = model.ParseDateOfBirth();
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.DateOfBirthAttribute, dateOfBirth, customer.RegisteredInStoreId);
                    }
                    if (_customerSettings.CompanyEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CompanyAttribute, model.Company, customer.RegisteredInStoreId);
                    if (_customerSettings.StreetAddressEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StreetAddressAttribute, model.StreetAddress, customer.RegisteredInStoreId);
                    if (_customerSettings.StreetAddress2Enabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StreetAddress2Attribute, model.StreetAddress2, customer.RegisteredInStoreId);
                    if (_customerSettings.ZipPostalCodeEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.ZipPostalCodeAttribute, model.ZipPostalCode, customer.RegisteredInStoreId);
                    if (_customerSettings.CityEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CityAttribute, model.City, customer.RegisteredInStoreId);
                    if (_customerSettings.CountyEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CountyAttribute, model.County, customer.RegisteredInStoreId);
                    if (_customerSettings.CountryEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CountryIdAttribute, model.CountryId, customer.RegisteredInStoreId);
                    if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StateProvinceIdAttribute,
                            model.StateProvinceId);
                    if (_customerSettings.PhoneEnabled)
                    {
                        if (model.ISDId != null && model.ISDId > 0)
                        {
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, (model.ISDId.ToString() + " " + model.Phone).Trim(), customer.RegisteredInStoreId); //added customer.RegisteredInStoreId
                        }
                        else
                        {
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, model.Phone, customer.RegisteredInStoreId);
                        }
                    }
                    if (_customerSettings.FaxEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FaxAttribute, model.Fax, customer.RegisteredInStoreId);

                    //newsletter
                    if (_customerSettings.NewsletterEnabled)
                    {
                        //save newsletter value
                        var newsletter = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(model.Email, customer.RegisteredInStoreId);
                        if (newsletter != null)
                        {
                            if (model.Newsletter)
                            {
                                newsletter.Active = true;
                                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(newsletter);

                                //GDPR
                                if (_gdprSettings.GdprEnabled && _gdprSettings.LogNewsletterConsent)
                                {
                                    _gdprService.InsertLog(customer, 0, GdprRequestType.ConsentAgree, _localizationService.GetResource("Gdpr.Consent.Newsletter"));
                                }
                            }
                            //else
                            //{
                            //When registering, not checking the newsletter check box should not take an existing email address off of the subscription list.
                            //_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletter);
                            //}
                        }
                        else
                        {
                            if (model.Newsletter)
                            {
                                _newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
                                {
                                    NewsLetterSubscriptionGuid = Guid.NewGuid(),
                                    Email = model.Email,
                                    Active = true,
                                    StoreId = customer.RegisteredInStoreId,
                                    CreatedOnUtc = DateTime.UtcNow
                                });

                                //GDPR
                                if (_gdprSettings.GdprEnabled && _gdprSettings.LogNewsletterConsent)
                                {
                                    _gdprService.InsertLog(customer, 0, GdprRequestType.ConsentAgree, _localizationService.GetResource("Gdpr.Consent.Newsletter"));
                                }
                            }
                        }
                    }

                    if (_customerSettings.AcceptPrivacyPolicyEnabled)
                    {
                        //privacy policy is required
                        //GDPR
                        if (_gdprSettings.GdprEnabled && _gdprSettings.LogPrivacyPolicyConsent)
                        {
                            _gdprService.InsertLog(customer, 0, GdprRequestType.ConsentAgree, _localizationService.GetResource("Gdpr.Consent.PrivacyPolicy"));
                        }
                    }

                    //GDPR
                    if (_gdprSettings.GdprEnabled)
                    {
                        var consents = _gdprService.GetAllConsents().Where(consent => consent.DisplayDuringRegistration).ToList();
                        foreach (var consent in consents)
                        {
                            var controlId = $"consent{consent.Id}";
                            var cbConsent = form[controlId];
                            if (!StringValues.IsNullOrEmpty(cbConsent) && cbConsent.ToString().Equals("on"))
                            {
                                //agree
                                _gdprService.InsertLog(customer, consent.Id, GdprRequestType.ConsentAgree, consent.Message);
                            }
                            else
                            {
                                //disagree
                                _gdprService.InsertLog(customer, consent.Id, GdprRequestType.ConsentDisagree, consent.Message);
                            }
                        }
                    }

                    //save customer attributes
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CustomCustomerAttributes, customerAttributesXml, customer.RegisteredInStoreId);
                    if (model.RegisterationType == RegistrationCustomerType.RestaurantOwner)
                    {
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.RestaurantNameAttribute, model.RestaurantName, customer.RegisteredInStoreId);
                    }

                    //login customer now
                    if (isApproved)
                        _authenticationService.SignIn(customer, true);

                    //insert default address (if possible)
                    var defaultAddress = new Address
                    {
                        FirstName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute, customer.RegisteredInStoreId),
                        LastName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute, customer.RegisteredInStoreId),
                        Email = customer.Email,
                        Company = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CompanyAttribute, customer.RegisteredInStoreId),
                        County = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CountyAttribute, customer.RegisteredInStoreId),
                        Address1 = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.StreetAddressAttribute, customer.RegisteredInStoreId),
                        Address2 = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.StreetAddress2Attribute, customer.RegisteredInStoreId),
                        ZipPostalCode = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.ZipPostalCodeAttribute, customer.RegisteredInStoreId),
                        PhoneNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute, customer.RegisteredInStoreId),
                        FaxNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FaxAttribute, customer.RegisteredInStoreId),
                        CreatedOnUtc = customer.CreatedOnUtc
                    };
                    if (model.RegisterationType == RegistrationCustomerType.Agent)
                    {
                        defaultAddress.CountryId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderCountryIdAttribute, customer.RegisteredInStoreId) > 0
                            ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderCountryIdAttribute, customer.RegisteredInStoreId)
                            : null;
                        defaultAddress.StateProvinceId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderStateProvinceIdAttribute, customer.RegisteredInStoreId) > 0
                            ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderStateProvinceIdAttribute, customer.RegisteredInStoreId)
                            : null;
                        defaultAddress.City = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.AgentRiderCityAttribute, customer.RegisteredInStoreId);
                    }
                    else
                    {
                        defaultAddress.CountryId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderCountryIdAttribute) > 0
                            ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderCountryIdAttribute)
                            : null;
                        defaultAddress.StateProvinceId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderStateProvinceIdAttribute) > 0
                            ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AgentRiderStateProvinceIdAttribute)
                            : null;
                        defaultAddress.City = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.AgentRiderCityAttribute, customer.RegisteredInStoreId);
                    }

                    //store owner store regsiter 
                    if (model.RegisterationType == RegistrationCustomerType.StoreOwner)
                    {
                        var store = new Store
                        {
                            Name = model.VenueName,
                            Url = string.Format(_localizationService.GetResource("NB.Account.Fields.Store.Url"), model.VenueURL),
                            SslEnabled = true,
                            Hosts = string.Format(_localizationService.GetResource("NB.Account.Fields.Store.HostsName"), model.VenueURL, model.VenueURL, model.VenueURL, model.VenueURL),
                            DefaultLanguageId = 1,
                            DisplayOrder = 1,
                            CompanyName = model.VenueName,
                            CompanyPhoneNumber = model.Phone,
                            StoreOwnerId = customer.Id,
                            IsDisplayDeliveryOrTakeaway = true,
                            IsDelivery = true,
                            IsTakeaway = true,
                            IsDinning = true
                        };

                        //ensure we have "/" at the end
                        if (!store.Url.EndsWith("/"))
                            store.Url += "/";

                        _storeService.InsertStore(store);

                        customer.RegisteredInStoreId = store.Id;
                        _customerService.UpdateCustomer(customer);
                    }

                    //raise event       
                    _eventPublisher.Publish(new CustomerRegisteredEvent(customer));

                    //notifications
                    if (_customerSettings.NotifyNewCustomerRegistration)
                        _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer,
                            _localizationSettings.DefaultAdminLanguageId);

                    if (_storeInformationSettings.EnableOTPAuthentication)
                    {
                        RegistrationOTP(customer);
                        return RedirectToAction("OTPVerification", new { id = customer.Id });
                    }

                    //notifications
                    if (_customerSettings.NotifyNewCustomerRegistration)
                        _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer,
                            _localizationSettings.DefaultAdminLanguageId);

                    switch (_customerSettings.UserRegistrationType)
                    {
                        case UserRegistrationType.EmailValidation:
                            {
                                //email validation message
                                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenAttribute, Guid.NewGuid().ToString(), customer.RegisteredInStoreId);
                                _workflowMessageService.SendCustomerEmailValidationMessage(customer, _workContext.WorkingLanguage.Id);

                                //result
                                return RedirectToRoute("RegisterResult",
                                    new { resultId = (int)UserRegistrationType.EmailValidation });
                            }
                        case UserRegistrationType.AdminApproval:
                            {
                                return RedirectToRoute("RegisterResult",
                                    new { resultId = (int)UserRegistrationType.AdminApproval });
                            }
                        case UserRegistrationType.Standard:
                            {
                                if (model.RegisterationType == RegistrationCustomerType.RestaurantOwner)
                                {
                                    if (_addressService.IsAddressValid(defaultAddress))
                                    {
                                        //some validation
                                        if (defaultAddress.CountryId == 0)
                                            defaultAddress.CountryId = null;
                                        if (defaultAddress.StateProvinceId == 0)
                                            defaultAddress.StateProvinceId = null;
                                        //set default address
                                        //customer.Addresses.Add(defaultAddress);
                                        customer.CustomerAddressMappings.Add(new CustomerAddressMapping { Address = defaultAddress });
                                        customer.BillingAddress = defaultAddress;
                                        customer.ShippingAddress = defaultAddress;
                                        _customerService.UpdateCustomer(customer);
                                    }

                                    var resultError = AddVendor(model, customer);
                                    if (!string.IsNullOrEmpty(resultError))
                                    {
                                        ModelState.AddModelError("", resultError);
                                        return View(model);
                                    }

                                    var vendorRole = _customerService.GetCustomerRoleBySystemName("Vendors");
                                    if (vendorRole != null)
                                    {
                                        customer.CustomerCustomerRoleMappings.Add(new CustomerCustomerRoleMapping { CustomerRole = vendorRole });
                                        _customerService.UpdateCustomer(customer);
                                    }

                                    #region CustomerPackage Mapping

                                    if (model.IsStoreRegisterAllowPackage && model.RestaurantRegisterAllow && model.RegisterationType == RegistrationCustomerType.RestaurantOwner)
                                    {
                                        var customerPackageMapping = new NBCustomerPackageMapping();

                                        customerPackageMapping.CustomerId = customer.Id;
                                        customerPackageMapping.PackageId = model.PackageId;
                                        customerPackageMapping.PackagePlanId = model.PackagePlanId;
                                        customerPackageMapping.CreatedOnUtc = DateTime.UtcNow;

                                        _nBCustomerPackageService.InsertCustomerPackageMapping(customerPackageMapping);

                                        // we will add the logic to redirect to route to package plan from here  like this 
                                        return RedirectToAction("PackagePayment", new { id = customer.Id });

                                    }

                                    #endregion

                                    //send tutor welcome message
                                    //_workflowMessageService.SendTutorWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

                                    return RedirectToRoute("RegisterResult",
                                   new { resultId = (int)UserRegistrationType.AdminApproval });
                                }

                                //send customer welcome message
                                _workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

                                var redirectUrl = Url.RouteUrl("RegisterResult",
                                    new { resultId = (int)UserRegistrationType.Standard, returnUrl }, _webHelper.CurrentRequestProtocol);
                                return Redirect(redirectUrl);
                            }
                        default:
                            {
                                return RedirectToRoute("Homepage");
                            }
                    }
                }

                //errors
                foreach (var error in registrationResult.Errors)
                    ModelState.AddModelError("", error);
            }

            //If we got this far, something failed, redisplay form
            model = _customerModelFactory.PrepareRegisterModel(model, true, customerAttributesXml);
            return View(model);
        }

        #endregion

        public virtual IActionResult OTPVerification(int id)
        {
            var customer = _customerService.GetCustomerById(id);
            var model = new OTPVerificationModel()
            {
                CustomerId = id,
                CurrentDate = DateTime.UtcNow,
                Timer = _settingService.GetSettingByKey<int>("adminareasettings.loginotptimer", 2, _storeContext.CurrentStore.Id),
                Date = _genericAttributeService.GetAttribute<string>(customer, "OTPSendDateTime", customer.RegisteredInStoreId, "")
            };
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult OTPVerification(OTPVerificationModel model)
        {
            var customer = _customerService.GetCustomerById(model.CustomerId);
            if (customer == null)
                return RedirectToRoute("Homepage");

            var phone = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute, customer.RegisteredInStoreId, "");
            if (!phone.Contains(" "))
                phone = _workContext.CurrentStoreISDCode + " " + phone;
            var otpPhone = _genericAttributeService.GetAttribute<string>(customer, "RegistrationOTP", _workContext.GetCurrentStoreId);
            if (string.IsNullOrEmpty(model.OTP))
            {
                ModelState.AddModelError("", _localizationService.GetResource("Registration.LoginSignup.EnterOTP"));
            }
            else if (otpPhone != model.OTP)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Checkout.LoginSignup.InvalidOTP.Validate"));
            }

            //return model if errors
            if (ModelState.ErrorCount > 0)
            {
                return View(model);
            }

            //send customer welcome message
            _workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);
            _workflowMessageService.SendCustomerWelcomeMessageOnPhone(customer, phone, _workContext.WorkingLanguage.Id);

            var loginResult = _customerRegistrationService.NBValidateCustomer(customer.Email, model.OTP);

            switch (loginResult)
            {
                case CustomerLoginResults.Successful:
                    {
                        //migrate shopping cart
                        _shoppingCartService.MigrateShoppingCart(_workContext.CurrentCustomer, customer, true);

                        //Custom code from v4.0
                        var orderDetails = _workContext.CurrentOrderDetails;
                        orderDetails.CustomerId = customer.Id;
                        _workContext.CurrentOrderDetails = orderDetails;

                        //sign in new customer
                        _authenticationService.SignIn(customer, true);
                        _staticCacheManager.Clear();

                        //raise event       
                        _eventPublisher.Publish(new CustomerLoggedinEvent(customer));

                        return RedirectToRoute("Homepage");
                    }
                case CustomerLoginResults.CustomerNotExist:
                    ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));
                    break;
                case CustomerLoginResults.Deleted:
                    ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.Deleted"));
                    break;
                case CustomerLoginResults.NotActive:
                    ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotActive"));
                    break;
                case CustomerLoginResults.NotRegistered:
                    ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered"));
                    break;
                case CustomerLoginResults.LockedOut:
                    ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.LockedOut"));
                    break;
                case CustomerLoginResults.WrongPassword:
                default:
                    ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials"));
                    break;
            }
            if (ModelState.ErrorCount > 0)
            {
                return View(model);
            }
            return Redirect("/");
        }

        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult RegisterResult(int resultId)
        {
            var model = _customerModelFactory.PrepareRegisterResultModel(resultId);
            return View(model);
        }

        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        [HttpPost]
        public virtual IActionResult RegisterResult(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                return RedirectToRoute("Homepage");

            return Redirect(returnUrl);
        }

        [HttpPost]
        [PublicAntiForgery]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult CheckUsernameAvailability(string username)
        {
            var usernameAvailable = false;
            var statusText = _localizationService.GetResource("Account.CheckUsernameAvailability.NotAvailable");

            if (!UsernamePropertyValidator.IsValid(username, _customerSettings))
            {
                statusText = _localizationService.GetResource("Account.Fields.Username.NotValid");
            }
            else if (_customerSettings.UsernamesEnabled && !string.IsNullOrWhiteSpace(username))
            {
                if (_workContext.CurrentCustomer != null &&
                    _workContext.CurrentCustomer.Username != null &&
                    _workContext.CurrentCustomer.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
                {
                    statusText = _localizationService.GetResource("Account.CheckUsernameAvailability.CurrentUsername");
                }
                else
                {
                    var customer = _customerService.GetCustomerByUsername(username);
                    if (customer == null)
                    {
                        statusText = _localizationService.GetResource("Account.CheckUsernameAvailability.Available");
                        usernameAvailable = true;
                    }
                }
            }

            return Json(new { Available = usernameAvailable, Text = statusText });
        }

        [HttpsRequirement(SslRequirement.Yes)]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult AccountActivation(string token, string email)
        {
            var customer = _customerService.GetCustomerByEmail(email);
            if (customer == null)
                return RedirectToRoute("Homepage");

            var cToken = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.AccountActivationTokenAttribute);
            if (string.IsNullOrEmpty(cToken))
                return
                    View(new AccountActivationModel
                    {
                        Result = _localizationService.GetResource("Account.AccountActivation.AlreadyActivated")
                    });

            if (!cToken.Equals(token, StringComparison.InvariantCultureIgnoreCase))
                return RedirectToRoute("Homepage");

            //activate user account
            customer.Active = true;
            _customerService.UpdateCustomer(customer);
            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenAttribute, "");
            //send welcome message
            _workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

            var model = new AccountActivationModel
            {
                Result = _localizationService.GetResource("Account.AccountActivation.Activated")
            };
            return View(model);
        }

        private string AddVendor(RegisterModel model, Customer customer)
        {
            if (ModelState.IsValid)
            {
                var description = Core.Html.HtmlHelper.FormatText(model.FirstName + " " + model.LastName, false, false, true, false, false, false);
                //disabled by default
                var vendor = new Vendor
                {
                    Name = model.FirstName + " " + model.LastName,
                    Email = model.Email,
                    AddressId = customer.BillingAddressId.HasValue ? customer.BillingAddressId.Value : 0,
                    //some default settings
                    PageSize = 6,
                    AllowCustomersToSelectPageSize = true,
                    PageSizeOptions = _vendorSettings.DefaultVendorPageSizeOptions,
                    PictureId = 0,
                    Description = description,
                    StoreId = _workContext.GetCurrentStoreId,
                    Active = false,
                    //Add this extra field becuase while regiser getting issue that fields are not allow null value
                    FixedOrDynamic = "f",
                    KmOrMilesForDynamic = "k",
                    PriceForDynamic = decimal.Zero,
                    DistanceForDynamic = decimal.Zero,
                    PricePerUnitDistanceForDynamic = decimal.Zero
                };
                _vendorService.InsertVendor(vendor);
                //search engine name (the same as vendor name)
                var seName = _urlRecordService.ValidateSeName(vendor, vendor.Name, vendor.Name, true);
                _urlRecordService.SaveSlug(vendor, seName, 0);

                //associate to the current customer
                //but a store owner will have to manually add this customer role to "Vendors" role
                //if he wants to grant access to admin area
                customer.VendorId = vendor.Id;
                _customerService.UpdateCustomer(customer);

                //notify store owner here (email)
                _workflowMessageService.SendNewVendorAccountApplyStoreOwnerNotification(_workContext.CurrentCustomer,
                    vendor, _localizationSettings.DefaultAdminLanguageId);

            }
            return "";
        }

        #endregion

        #region My account / Info

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult Info()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var model = new CustomerInfoModel();
            model = _customerModelFactory.PrepareCustomerInfoModel(model, _workContext.CurrentCustomer, false);
            //added by mah 27022019
            if (model.Phone != null)
            {
                if (model.Phone.ToString().Trim().Split(' ').Length > 1)
                {
                    string[] phArr = model.Phone.ToString().Trim().Split(' ');
                    model.Phone = phArr[1];
                }
            }
            return View(model);
        }

        [HttpPost]
        [PublicAntiForgery]
        public virtual IActionResult Info(CustomerInfoModel model, IFormCollection form)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var oldCustomerModel = new CustomerInfoModel();

            var customer = _workContext.CurrentCustomer;

            //get customer info model before changes for gdpr log
            if (_gdprSettings.GdprEnabled & _gdprSettings.LogUserProfileChanges)
                oldCustomerModel = _customerModelFactory.PrepareCustomerInfoModel(oldCustomerModel, customer, false);

            //custom customer attributes
            var customerAttributesXml = ParseCustomCustomerAttributes(form);
            var customerAttributeWarnings = _customerAttributeParser.GetAttributeWarnings(customerAttributesXml);
            foreach (var error in customerAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    //username 
                    if (_customerSettings.UsernamesEnabled && _customerSettings.AllowUsersToChangeUsernames)
                    {
                        if (
                            !customer.Username.Equals(model.Username.Trim(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            //change username
                            _customerRegistrationService.SetUsername(customer, model.Username.Trim());

                            //re-authenticate
                            //do not authenticate users in impersonation mode
                            if (_workContext.OriginalCustomerIfImpersonated == null)
                                _authenticationService.SignIn(customer, true);
                        }
                    }
                    //email
                    if (!customer.Email.Equals(model.Email.Trim(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        //change email
                        var requireValidation = _customerSettings.UserRegistrationType == UserRegistrationType.EmailValidation;
                        _customerRegistrationService.SetEmail(customer, model.Email.Trim(), requireValidation);

                        //do not authenticate users in impersonation mode
                        if (_workContext.OriginalCustomerIfImpersonated == null)
                        {
                            //re-authenticate (if usernames are disabled)
                            if (!_customerSettings.UsernamesEnabled && !requireValidation)
                                _authenticationService.SignIn(customer, true);
                        }
                    }

                    //properties
                    if (_dateTimeSettings.AllowCustomersToSetTimeZone)
                    {
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.TimeZoneIdAttribute,
                            model.TimeZoneId, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                    }
                    //VAT number
                    if (_taxSettings.EuVatEnabled)
                    {
                        var prevVatNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.VatNumberAttribute, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);

                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.VatNumberAttribute,
                            model.VatNumber, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                        if (prevVatNumber != model.VatNumber)
                        {
                            var vatNumberStatus = _taxService.GetVatNumberStatus(model.VatNumber, out string _, out string vatAddress);
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.VatNumberStatusIdAttribute, (int)vatNumberStatus, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                            //send VAT number admin notification
                            if (!string.IsNullOrEmpty(model.VatNumber) && _taxSettings.EuVatEmailAdminWhenNewVatSubmitted)
                                _workflowMessageService.SendNewVatSubmittedStoreOwnerNotification(customer,
                                    model.VatNumber, vatAddress, _localizationSettings.DefaultAdminLanguageId);
                        }
                    }

                    //form fields
                    if (_customerSettings.GenderEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.GenderAttribute, model.Gender, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FirstNameAttribute, model.FirstName, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.LastNameAttribute, model.LastName, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                    if (_customerSettings.DateOfBirthEnabled)
                    {
                        var dateOfBirth = model.ParseDateOfBirth();
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.DateOfBirthAttribute, dateOfBirth, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                    }
                    if (_customerSettings.CompanyEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CompanyAttribute, model.Company, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                    if (_customerSettings.StreetAddressEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StreetAddressAttribute, model.StreetAddress, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                    if (_customerSettings.StreetAddress2Enabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StreetAddress2Attribute, model.StreetAddress2, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                    if (_customerSettings.ZipPostalCodeEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.ZipPostalCodeAttribute, model.ZipPostalCode, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                    if (_customerSettings.CityEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CityAttribute, model.City, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                    if (_customerSettings.CountyEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CountyAttribute, model.County, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                    if (_customerSettings.CountryEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CountryIdAttribute, model.CountryId, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                    if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StateProvinceIdAttribute, model.StateProvinceId, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                    if (_customerSettings.PhoneEnabled)
                    {
                        if (model.ISDId != null && model.ISDId > 0)
                        {
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, (model.ISDId.ToString().Trim() + " " + model.Phone).Trim(), customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                        }
                        else
                        {
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, model.Phone, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);
                        }
                    }
                    if (_customerSettings.FaxEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FaxAttribute, model.Fax, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);

                    //newsletter
                    if (_customerSettings.NewsletterEnabled)
                    {
                        //save newsletter value
                        var newsletter = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : _workContext.GetCurrentStoreId);
                        if (newsletter != null)
                        {
                            if (model.Newsletter)
                            {
                                var wasActive = newsletter.Active;
                                newsletter.Active = true;
                                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(newsletter);
                            }
                            else
                            {
                                _newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletter);
                            }
                        }
                        else
                        {
                            if (model.Newsletter)
                            {
                                _newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
                                {
                                    NewsLetterSubscriptionGuid = Guid.NewGuid(),
                                    Email = customer.Email,
                                    Active = true,
                                    StoreId = customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : _workContext.GetCurrentStoreId,
                                    CreatedOnUtc = DateTime.UtcNow
                                });
                            }
                        }
                    }

                    if (_forumSettings.ForumsEnabled && _forumSettings.SignaturesEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.SignatureAttribute, model.Signature, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);

                    //save customer attributes
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        NopCustomerDefaults.CustomCustomerAttributes, customerAttributesXml, customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0);

                    //GDPR
                    if (_gdprSettings.GdprEnabled)
                        LogGdpr(customer, oldCustomerModel, model, form);

                    return RedirectToRoute("CustomerInfo");
                }
            }
            catch (Exception exc)
            {
                ModelState.AddModelError("", exc.Message);
            }

            //If we got this far, something failed, redisplay form
            model = _customerModelFactory.PrepareCustomerInfoModel(model, customer, true, customerAttributesXml);
            return View(model);
        }

        [HttpPost]
        [PublicAntiForgery]
        public virtual IActionResult RemoveExternalAssociation(int id)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            //ensure it's our record
            var ear = _workContext.CurrentCustomer.ExternalAuthenticationRecords.FirstOrDefault(x => x.Id == id);

            if (ear == null)
            {
                return Json(new
                {
                    redirect = Url.Action("Info"),
                });
            }

            _externalAuthenticationService.DeleteExternalAuthenticationRecord(ear);

            return Json(new
            {
                redirect = Url.Action("Info"),
            });
        }

        [HttpsRequirement(SslRequirement.Yes)]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult EmailRevalidation(string token, string email)
        {
            var customer = _customerService.GetCustomerByEmail(email);
            if (customer == null)
                return RedirectToRoute("Homepage");

            var cToken = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.EmailRevalidationTokenAttribute);
            if (string.IsNullOrEmpty(cToken))
                return View(new EmailRevalidationModel
                {
                    Result = _localizationService.GetResource("Account.EmailRevalidation.AlreadyChanged")
                });

            if (!cToken.Equals(token, StringComparison.InvariantCultureIgnoreCase))
                return RedirectToRoute("Homepage");

            if (string.IsNullOrEmpty(customer.EmailToRevalidate))
                return RedirectToRoute("Homepage");

            if (_customerSettings.UserRegistrationType != UserRegistrationType.EmailValidation)
                return RedirectToRoute("Homepage");

            //change email
            try
            {
                _customerRegistrationService.SetEmail(customer, customer.EmailToRevalidate, false);
            }
            catch (Exception exc)
            {
                return View(new EmailRevalidationModel
                {
                    Result = _localizationService.GetResource(exc.Message)
                });
            }
            customer.EmailToRevalidate = null;
            _customerService.UpdateCustomer(customer);
            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.EmailRevalidationTokenAttribute, "");

            //re-authenticate (if usernames are disabled)
            if (!_customerSettings.UsernamesEnabled)
            {
                _authenticationService.SignIn(customer, true);
            }

            var model = new EmailRevalidationModel()
            {
                Result = _localizationService.GetResource("Account.EmailRevalidation.Changed")
            };
            return View(model);
        }

        #endregion

        #region My account / Addresses

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult Addresses()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var model = _customerModelFactory.PrepareCustomerAddressListModel();
            return View(model);
        }

        [HttpPost]
        [PublicAntiForgery]
        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult AddressDelete(int addressId)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            //find address (ensure that it belongs to the current customer)
            var address = customer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address != null)
            {
                _customerService.RemoveCustomerAddress(customer, address);
                _customerService.UpdateCustomer(customer);
                //now delete the address record
                _addressService.DeleteAddress(address);
            }

            //redirect to the address list page
            return Json(new
            {
                redirect = Url.RouteUrl("CustomerAddresses"),
            });
        }

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult AddressAdd()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var model = new CustomerAddressEditModel();
            _addressModelFactory.PrepareAddressModel(model.Address,
                address: null,
                excludeProperties: false,
                addressSettings: _addressSettings,
                loadCountries: () => _countryService.GetAllCountries(_workContext.WorkingLanguage.Id));

            return View(model);
        }

        [HttpPost]
        [PublicAntiForgery]
        public virtual IActionResult AddressAdd(CustomerAddressEditModel model, IFormCollection form)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            //custom address attributes
            var customAttributes = _addressAttributeParser.ParseCustomAddressAttributes(form);
            var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
            foreach (var error in customAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            var newAddress = model.Address;
            if ((string.IsNullOrEmpty(newAddress.Address1) || string.IsNullOrEmpty(newAddress.Address2) || string.IsNullOrEmpty(newAddress.Landmark)) || (!customer.IsRegistered() && (string.IsNullOrEmpty(newAddress.FirstName) || string.IsNullOrEmpty(newAddress.Email))))
            {
                ModelState.AddModelError("", _localizationService.GetResource("Checkout.BillingNewAddress.Validation.Message"));
                //If we got this far, something failed, redisplay form
                _addressModelFactory.PrepareAddressModel(model.Address,
                    address: null,
                    excludeProperties: true,
                    addressSettings: _addressSettings,
                    loadCountries: () => _countryService.GetAllCountries(_workContext.WorkingLanguage.Id),
                    overrideAttributesXml: customAttributes);

                return View(model);
            }

            var address = model.Address.ToEntity();
            address.CustomAttributes = customAttributes;
            address.CreatedOnUtc = DateTime.UtcNow;
            if (address.CountryId == 0)
            {
                if (_storeContext.CurrentStore.CountryId > 0)
                    address.CountryId = _storeContext.CurrentStore.CountryId;
                else
                    address.CountryId = null;
            }
            if (address.StateProvinceId == 0)
                address.StateProvinceId = null;
            if (address.CountryId.HasValue && address.CountryId.Value > 0)
            {
                address.Country = _countryService.GetCountryById(address.CountryId.Value);
            }
            address.PhoneNumber = newAddress.PhoneNumber;
            if (model.Address.ISDId != null && !string.IsNullOrEmpty(address.PhoneNumber))
            {
                if (address.PhoneNumber.ToString().Split(' ').Length == 1)
                {
                    address.PhoneNumber = model.Address.ISDId.ToString() + " " + address.PhoneNumber.ToString();
                }
            }

            address.FirstName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute, (customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0));
            address.LastName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute, (customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0));
            address.Email = newAddress.Email;
            address.Landmark = newAddress.LocationType;
            address.ZipPostalCode = newAddress.ZipPostalCode;
            address.City = newAddress.City;
            address.County = newAddress.Landmark;
            address.FlatNo = newAddress.HouseNumber;
            customer.CustomerAddressMappings.Add(new CustomerAddressMapping { Address = address });
            _customerService.UpdateCustomer(customer);

            return RedirectToRoute("CustomerAddresses");


        }

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult AddressEdit(int addressId)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;
            //find address (ensure that it belongs to the current customer)
            var address = customer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                //address is not found
                return RedirectToRoute("CustomerAddresses");

            var model = new CustomerAddressEditModel();
            _addressModelFactory.PrepareAddressModel(model.Address,
                address: address,
                excludeProperties: false,
                addressSettings: _addressSettings,
                loadCountries: () => _countryService.GetAllCountries(_workContext.WorkingLanguage.Id));
            //Added By Mah 27022019
            if (!string.IsNullOrEmpty(model.Address.PhoneNumber) && model.Address.PhoneNumber.Contains(" "))
            {
                string[] pharr = model.Address.PhoneNumber.ToString().Split(' ');
                if (pharr != null)
                {
                    if (pharr.Length > 1)
                    {
                        model.Address.PhoneNumber = pharr[1];
                    }
                }
            }
            return View(model);
        }

        [HttpPost]
        [PublicAntiForgery]
        public virtual IActionResult AddressEdit(CustomerAddressEditModel model, int addressId, IFormCollection form)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;
            if (string.IsNullOrEmpty(model.Address.Landmark))
                model.Address.Landmark = "Home";
            //find address (ensure that it belongs to the current customer)
            var address = customer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                //address is not found
                return RedirectToRoute("CustomerAddresses");

            //custom address attributes
            var customAttributes = _addressAttributeParser.ParseCustomAddressAttributes(form);
            var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
            foreach (var error in customAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            var newAddress = model.Address;

            if ((string.IsNullOrEmpty(newAddress.Address1) || string.IsNullOrEmpty(newAddress.Address2) || string.IsNullOrEmpty(newAddress.Landmark)) || (!customer.IsRegistered() && (string.IsNullOrEmpty(newAddress.FirstName) || string.IsNullOrEmpty(newAddress.Email))))
            {
                ModelState.AddModelError("", _localizationService.GetResource("Checkout.BillingNewAddress.Validation.Message"));

                //If we got this far, something failed, redisplay form
                _addressModelFactory.PrepareAddressModel(model.Address,
                    address: address,
                    excludeProperties: true,
                    addressSettings: _addressSettings,
                    loadCountries: () => _countryService.GetAllCountries(_workContext.WorkingLanguage.Id),
                    overrideAttributesXml: customAttributes);
                return View(model);
            }
            address = model.Address.ToEntity(address);

            if (address.CountryId == 0)
            {
                if (_storeContext.CurrentStore.CountryId > 0)
                    address.CountryId = _storeContext.CurrentStore.CountryId;
                else
                    address.CountryId = null;
            }
            if (address.StateProvinceId == 0)
                address.StateProvinceId = null;
            if (address.CountryId.HasValue && address.CountryId.Value > 0)
            {
                address.Country = _countryService.GetCountryById(address.CountryId.Value);
            }
            address.PhoneNumber = newAddress.PhoneNumber;
            if (model.Address.ISDId != null && !string.IsNullOrEmpty(address.PhoneNumber))
            {
                if (address.PhoneNumber.ToString().Split(' ').Length == 1)
                {
                    address.PhoneNumber = model.Address.ISDId.ToString() + " " + address.PhoneNumber.ToString();
                }
            }

            address.FirstName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute, (customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0));
            address.LastName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute, (customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0));
            address.Email = newAddress.Email;
            address.Landmark = newAddress.LocationType;
            address.ZipPostalCode = newAddress.ZipPostalCode;
            address.City = newAddress.City;
            address.County = newAddress.Landmark;
            address.FlatNo = newAddress.HouseNumber;
            address.CustomAttributes = customAttributes;
            _addressService.UpdateAddress(address);

            return RedirectToRoute("CustomerAddresses");


        }

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult WishList()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            //Added By Mah
            int currStoreId = _checkoutAttributeService.GetCurrentStoreId();

            var customer = _workContext.CurrentCustomer;
            if (customer == null)
                return RedirectToRoute("Homepage");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.Wishlist, currStoreId);

            var model = new WishlistModel();
            model = _shoppingCartModelFactory.PrepareWishlistModel(model, cart, true);
            return View(model);
        }

        #endregion

        #region My account / Downloadable products

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult DownloadableProducts()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            if (_customerSettings.HideDownloadableProductsTab)
                return RedirectToRoute("CustomerInfo");

            var model = _customerModelFactory.PrepareCustomerDownloadableProductsModel();
            return View(model);
        }

        public virtual IActionResult UserAgreement(Guid orderItemId)
        {
            var orderItem = _orderService.GetOrderItemByGuid(orderItemId);
            if (orderItem == null)
                return RedirectToRoute("Homepage");

            var product = orderItem.Product;
            if (product == null || !product.HasUserAgreement)
                return RedirectToRoute("Homepage");

            var model = _customerModelFactory.PrepareUserAgreementModel(orderItem, product);
            return View(model);
        }

        #endregion

        #region My account / Change password

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult ChangePassword()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var model = _customerModelFactory.PrepareChangePasswordModel();

            //display the cause of the change password 
            if (_customerService.PasswordIsExpired(_workContext.CurrentCustomer))
                ModelState.AddModelError(string.Empty, _localizationService.GetResource("Account.ChangePassword.PasswordIsExpired"));

            return View(model);
        }

        [HttpPost]
        [PublicAntiForgery]
        public virtual IActionResult ChangePassword(ChangePasswordModel model)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            if (ModelState.IsValid)
            {
                var changePasswordRequest = new ChangePasswordRequest(customer.Email,
                    true, _customerSettings.DefaultPasswordFormat, model.NewPassword, model.OldPassword);
                var changePasswordResult = _customerRegistrationService.ChangePassword(changePasswordRequest);
                if (changePasswordResult.Success)
                {
                    model.Result = _localizationService.GetResource("Account.ChangePassword.Success");
                    return View(model);
                }

                //errors
                foreach (var error in changePasswordResult.Errors)
                    ModelState.AddModelError("", error);
            }
            else
                ModelState.AddModelError("", _localizationService.GetResource("NB.Customer.Account.ChangePassword.ModelState.Error"));

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        #endregion

        #region My account / Avatar

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult Avatar()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            if (!_customerSettings.AllowCustomersToUploadAvatars)
                return RedirectToRoute("CustomerInfo");

            var model = new CustomerAvatarModel();
            model = _customerModelFactory.PrepareCustomerAvatarModel(model);
            return View(model);
        }

        [HttpPost, ActionName("Avatar")]
        [PublicAntiForgery]
        [FormValueRequired("upload-avatar")]
        public virtual IActionResult UploadAvatar(CustomerAvatarModel model, IFormFile uploadedFile)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            if (!_customerSettings.AllowCustomersToUploadAvatars)
                return RedirectToRoute("CustomerInfo");

            var customer = _workContext.CurrentCustomer;

            if (ModelState.IsValid)
            {
                try
                {
                    var customerAvatar = _pictureService.GetPictureById(_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AvatarPictureIdAttribute));
                    if (uploadedFile != null && !string.IsNullOrEmpty(uploadedFile.FileName))
                    {
                        var avatarMaxSize = _customerSettings.AvatarMaximumSizeBytes;
                        if (uploadedFile.Length > avatarMaxSize)
                            throw new NopException(string.Format(_localizationService.GetResource("Account.Avatar.MaximumUploadedFileSize"), avatarMaxSize));

                        var customerPictureBinary = _downloadService.GetDownloadBits(uploadedFile);
                        if (customerAvatar != null)
                            customerAvatar = _pictureService.UpdatePicture(customerAvatar.Id, customerPictureBinary, uploadedFile.ContentType, null);
                        else
                            customerAvatar = _pictureService.InsertPicture(customerPictureBinary, uploadedFile.ContentType, null);
                    }

                    var customerAvatarId = 0;
                    if (customerAvatar != null)
                        customerAvatarId = customerAvatar.Id;

                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AvatarPictureIdAttribute, customerAvatarId);

                    model.AvatarUrl = _pictureService.GetPictureUrl(
                        _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AvatarPictureIdAttribute),
                        _mediaSettings.AvatarPictureSize,
                        false);
                    return View(model);
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError("", exc.Message);
                }
            }

            //If we got this far, something failed, redisplay form
            model = _customerModelFactory.PrepareCustomerAvatarModel(model);
            return View(model);
        }

        [HttpPost, ActionName("Avatar")]
        [PublicAntiForgery]
        [FormValueRequired("remove-avatar")]
        public virtual IActionResult RemoveAvatar(CustomerAvatarModel model)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            if (!_customerSettings.AllowCustomersToUploadAvatars)
                return RedirectToRoute("CustomerInfo");

            var customer = _workContext.CurrentCustomer;

            var customerAvatar = _pictureService.GetPictureById(_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AvatarPictureIdAttribute));
            if (customerAvatar != null)
                _pictureService.DeletePicture(customerAvatar);
            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AvatarPictureIdAttribute, 0);

            return RedirectToRoute("CustomerAvatar");
        }

        #endregion

        #region GDPR tools

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult GdprTools()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            if (!_gdprSettings.GdprEnabled)
                return RedirectToRoute("CustomerInfo");

            var model = _customerModelFactory.PrepareGdprToolsModel();
            return View(model);
        }

        [HttpPost, ActionName("GdprTools")]
        [PublicAntiForgery]
        [FormValueRequired("export-data")]
        public virtual IActionResult GdprToolsExport()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            if (!_gdprSettings.GdprEnabled)
                return RedirectToRoute("CustomerInfo");

            //log
            _gdprService.InsertLog(_workContext.CurrentCustomer, 0, GdprRequestType.ExportData, _localizationService.GetResource("Gdpr.Exported"));

            //export
            var bytes = _exportManager.ExportCustomerGdprInfoToXlsx(_workContext.CurrentCustomer, _storeContext.CurrentStore.Id);

            return File(bytes, MimeTypes.TextXlsx, "customerdata.xlsx");
        }

        [HttpPost, ActionName("GdprTools")]
        [PublicAntiForgery]
        [FormValueRequired("delete-account")]
        public virtual IActionResult GdprToolsDelete()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            if (!_gdprSettings.GdprEnabled)
                return RedirectToRoute("CustomerInfo");

            //log
            _gdprService.InsertLog(_workContext.CurrentCustomer, 0, GdprRequestType.DeleteCustomer, _localizationService.GetResource("Gdpr.DeleteRequested"));

            var model = _customerModelFactory.PrepareGdprToolsModel();
            model.Result = _localizationService.GetResource("Gdpr.DeleteRequested.Success");
            return View(model);
        }

        #endregion

        #region Check gift card balance

        //check gift card balance page
        [HttpsRequirement(SslRequirement.Yes)]
        //available even when a store is closed
        [CheckAccessClosedStore(true)]
        public virtual IActionResult CheckGiftCardBalance()
        {
            if (!(_captchaSettings.Enabled && _customerSettings.AllowCustomersToCheckGiftCardBalance))
            {
                return RedirectToRoute("CustomerInfo");
            }

            var model = _customerModelFactory.PrepareCheckGiftCardBalanceModel();
            return View(model);
        }

        [HttpPost, ActionName("CheckGiftCardBalance")]
        [FormValueRequired("checkbalancegiftcard")]
        [ValidateCaptcha]
        public virtual IActionResult CheckBalance(CheckGiftCardBalanceModel model, bool captchaValid)
        {
            //validate CAPTCHA
            if (_captchaSettings.Enabled && !captchaValid)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptchaMessage"));
            }

            if (ModelState.IsValid)
            {
                var giftCard = _giftCardService.GetAllGiftCards(giftCardCouponCode: model.GiftCardCode).FirstOrDefault();
                if (giftCard != null && _giftCardService.IsGiftCardValid(giftCard))
                {
                    var remainingAmount = _currencyService.ConvertFromPrimaryStoreCurrency(_giftCardService.GetGiftCardRemainingAmount(giftCard), _workContext.WorkingCurrency);
                    model.Result = _priceFormatter.FormatPrice(remainingAmount, true, false);
                }
                else
                {
                    model.Message = _localizationService.GetResource("CheckGiftCardBalance.GiftCardCouponCode.Invalid");
                }
            }

            return View(model);
        }

        #endregion

        #region Agent register

        [HttpsRequirement(SslRequirement.Yes)]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult AgentRegister()
        {
            //check whether registration is allowed
            if (_customerSettings.UserRegistrationType == UserRegistrationType.Disabled)
                return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.Disabled });

            var model = new AgentRegisterModel();
            model = _customerModelFactory.PrepareAgentRegisterModel(model, false, setDefaultValues: true);

            return View(model);
        }

        [HttpPost]
        [ValidateCaptcha]
        [ValidateHoneypot]
        [PublicAntiForgery]
        //available even when navigation is not allowed
        [CheckAccessPublicStore(true)]
        public virtual IActionResult AgentRegister(AgentRegisterModel model, string returnUrl, bool captchaValid, IFormCollection form)
        {
            //check whether registration is allowed
            if (_customerSettings.UserRegistrationType == UserRegistrationType.Disabled)
                return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.Disabled });

            if (_workContext.CurrentCustomer.IsRegistered())
            {
                //Already registered customer. 
                _authenticationService.SignOut();

                //raise logged out event       
                _eventPublisher.Publish(new CustomerLoggedOutEvent(_workContext.CurrentCustomer));

                //Save a new record
                _workContext.CurrentCustomer = _customerService.InsertGuestCustomer();
            }
            var customer = _workContext.CurrentCustomer;
            customer.RegisteredInStoreId = _storeContext.CurrentStore.Id;

            //custom customer attributes
            var customerAttributesXml = ParseCustomCustomerAttributes(form);
            var customerAttributeWarnings = _customerAttributeParser.GetAttributeWarnings(customerAttributesXml);
            foreach (var error in customerAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnRegistrationPage && !captchaValid)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptchaMessage"));
            }

            if (ModelState.IsValid)
            {
                if (_customerSettings.UsernamesEnabled && model.Username != null)
                {
                    model.Username = model.Username.Trim();
                }

                var isApproved = false;
                var registrationRequest = new CustomerRegistrationRequest(customer,
                    model.Email,
                    _customerSettings.UsernamesEnabled ? model.Username : model.Email,
                    model.Password,
                    _customerSettings.DefaultPasswordFormat,
                    _storeContext.CurrentStore.Id,
                    isApproved);
                //For checking Phone No 16-05-2019
                if (!string.IsNullOrEmpty(model.Phone))
                {
                    var attributes = _genericAttributeService.GetAttributesForPhoneEntity(model.ISDId + " " + model.Phone, customer.RegisteredInStoreId);
                    if (attributes.Count > 0)

                    {
                        var result = new CustomerRegistrationResult();
                        result.AddError("Mobile number is already exist!!");
                        if (!result.Success)
                        {
                            foreach (var error in result.Errors)
                                ModelState.AddModelError("", error);
                            model = _customerModelFactory.PrepareAgentRegisterModel(model, true, customerAttributesXml);
                            return View(model);
                        }

                    }
                }
                //End For checking Phone No 16-05-2019
                var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);
                if (registrationResult.Success)
                {
                    //properties
                    if (_dateTimeSettings.AllowCustomersToSetTimeZone)
                    {
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.TimeZoneIdAttribute, model.TimeZoneId, customer.RegisteredInStoreId);
                    }
                    //VAT number
                    if (_taxSettings.EuVatEnabled)
                    {
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.VatNumberAttribute, model.VatNumber, customer.RegisteredInStoreId);

                        var vatNumberStatus = _taxService.GetVatNumberStatus(model.VatNumber, out string _, out string vatAddress);
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.VatNumberStatusIdAttribute, (int)vatNumberStatus, customer.RegisteredInStoreId);
                        //send VAT number admin notification
                        if (!string.IsNullOrEmpty(model.VatNumber) && _taxSettings.EuVatEmailAdminWhenNewVatSubmitted)
                            _workflowMessageService.SendNewVatSubmittedStoreOwnerNotification(customer, model.VatNumber, vatAddress, _localizationSettings.DefaultAdminLanguageId);
                    }

                    //form fields
                    if (_customerSettings.GenderEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.GenderAttribute, model.Gender, customer.RegisteredInStoreId);
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FirstNameAttribute, model.FirstName, customer.RegisteredInStoreId);
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.LastNameAttribute, model.LastName, customer.RegisteredInStoreId);
                    if (_customerSettings.DateOfBirthEnabled)
                    {
                        var dateOfBirth = model.ParseDateOfBirth();
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.DateOfBirthAttribute, dateOfBirth, customer.RegisteredInStoreId);
                    }
                    if (_customerSettings.CompanyEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CompanyAttribute, model.Company, customer.RegisteredInStoreId);
                    if (_customerSettings.AgentStreetAddressEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StreetAddressAttribute, model.StreetAddress, customer.RegisteredInStoreId);
                    if (_customerSettings.AgentStreetAddress2Enabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StreetAddress2Attribute, model.StreetAddress2, customer.RegisteredInStoreId);
                    if (_customerSettings.AgentZipPostalCodeEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.ZipPostalCodeAttribute, model.ZipPostalCode, customer.RegisteredInStoreId);
                    if (_customerSettings.CityEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CityAttribute, model.City, customer.RegisteredInStoreId);
                    if (_customerSettings.AgentStateProvinceEnabled && _customerSettings.AgentCityDdlEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CityIdAttribute, model.CityId, customer.RegisteredInStoreId);
                    if (_customerSettings.CountyEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CountyAttribute, model.County, customer.RegisteredInStoreId);
                    if (_customerSettings.AgentCountryEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CountryIdAttribute, model.CountryId, customer.RegisteredInStoreId);
                    if (_customerSettings.AgentCountryEnabled && _customerSettings.AgentStateProvinceEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.StateProvinceIdAttribute,
                            model.StateProvinceId);
                    if (_customerSettings.PhoneEnabled)
                    {
                        if (model.ISDId != null && model.ISDId > 0)
                        {
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, (model.ISDId.ToString() + " " + model.Phone).Trim(), customer.RegisteredInStoreId); //added customer.RegisteredInStoreId
                        }
                        else
                        {
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, model.Phone, customer.RegisteredInStoreId);
                        }
                    }
                    if (_customerSettings.FaxEnabled)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FaxAttribute, model.Fax, customer.RegisteredInStoreId);

                    //newsletter
                    if (_customerSettings.NewsletterEnabled)
                    {
                        //save newsletter value
                        var newsletter = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(model.Email, customer.RegisteredInStoreId);
                        if (newsletter != null)
                        {
                            if (model.Newsletter)
                            {
                                newsletter.Active = true;
                                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(newsletter);

                                //GDPR
                                if (_gdprSettings.GdprEnabled && _gdprSettings.LogNewsletterConsent)
                                {
                                    _gdprService.InsertLog(customer, 0, GdprRequestType.ConsentAgree, _localizationService.GetResource("Gdpr.Consent.Newsletter"));
                                }
                            }
                            //else
                            //{
                            //When registering, not checking the newsletter check box should not take an existing email address off of the subscription list.
                            //_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletter);
                            //}
                        }
                        else
                        {
                            if (model.Newsletter)
                            {
                                _newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
                                {
                                    NewsLetterSubscriptionGuid = Guid.NewGuid(),
                                    Email = model.Email,
                                    Active = true,
                                    StoreId = customer.RegisteredInStoreId,
                                    CreatedOnUtc = DateTime.UtcNow
                                });

                                //GDPR
                                if (_gdprSettings.GdprEnabled && _gdprSettings.LogNewsletterConsent)
                                {
                                    _gdprService.InsertLog(customer, 0, GdprRequestType.ConsentAgree, _localizationService.GetResource("Gdpr.Consent.Newsletter"));
                                }
                            }
                        }
                    }

                    if (_customerSettings.AcceptPrivacyPolicyEnabled)
                    {
                        //privacy policy is required
                        //GDPR
                        if (_gdprSettings.GdprEnabled && _gdprSettings.LogPrivacyPolicyConsent)
                        {
                            _gdprService.InsertLog(customer, 0, GdprRequestType.ConsentAgree, _localizationService.GetResource("Gdpr.Consent.PrivacyPolicy"));
                        }
                    }

                    //GDPR
                    if (_gdprSettings.GdprEnabled)
                    {
                        var consents = _gdprService.GetAllConsents().Where(consent => consent.DisplayDuringRegistration).ToList();
                        foreach (var consent in consents)
                        {
                            var controlId = $"consent{consent.Id}";
                            var cbConsent = form[controlId];
                            if (!StringValues.IsNullOrEmpty(cbConsent) && cbConsent.ToString().Equals("on"))
                            {
                                //agree
                                _gdprService.InsertLog(customer, consent.Id, GdprRequestType.ConsentAgree, consent.Message);
                            }
                            else
                            {
                                //disagree
                                _gdprService.InsertLog(customer, consent.Id, GdprRequestType.ConsentDisagree, consent.Message);
                            }
                        }
                    }

                    //save customer attributes
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.CustomCustomerAttributes, customerAttributesXml, customer.RegisteredInStoreId);

                    //login customer now
                    if (isApproved)
                        _authenticationService.SignIn(customer, true);

                    //insert default address (if possible)
                    var defaultAddress = new Address
                    {
                        FirstName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute, customer.RegisteredInStoreId),
                        LastName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute, customer.RegisteredInStoreId),
                        Email = customer.Email,
                        Company = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CompanyAttribute, customer.RegisteredInStoreId),
                        CountryId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.CountryIdAttribute) > 0
                            ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.CountryIdAttribute)
                            : null,
                        StateProvinceId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.StateProvinceIdAttribute) > 0
                            ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.StateProvinceIdAttribute)
                            : null,
                        County = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CountyAttribute, customer.RegisteredInStoreId),
                        City = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CityAttribute, customer.RegisteredInStoreId),
                        Address1 = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.StreetAddressAttribute, customer.RegisteredInStoreId),
                        Address2 = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.StreetAddress2Attribute, customer.RegisteredInStoreId),
                        ZipPostalCode = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.ZipPostalCodeAttribute, customer.RegisteredInStoreId),
                        PhoneNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute, customer.RegisteredInStoreId),
                        FaxNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FaxAttribute, customer.RegisteredInStoreId),
                        CreatedOnUtc = customer.CreatedOnUtc
                    };
                    if (_addressService.IsAddressValid(defaultAddress))
                    {
                        //some validation
                        if (defaultAddress.CountryId == 0)
                            defaultAddress.CountryId = null;
                        if (defaultAddress.StateProvinceId == 0)
                            defaultAddress.StateProvinceId = null;
                        //set default address
                        //customer.Addresses.Add(defaultAddress);
                        customer.CustomerAddressMappings.Add(new CustomerAddressMapping { Address = defaultAddress });
                        customer.BillingAddress = defaultAddress;
                        customer.ShippingAddress = defaultAddress;
                        _customerService.UpdateCustomer(customer);
                    }

                    //notifications
                    if (_customerSettings.NotifyNewCustomerRegistration)
                        _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer,
                            _localizationSettings.DefaultAdminLanguageId);

                    //raise event       
                    _eventPublisher.Publish(new CustomerRegisteredEvent(customer));

                    switch (_customerSettings.UserRegistrationType)
                    {
                        case UserRegistrationType.EmailValidation:
                            {
                                //email validation message
                                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenAttribute, Guid.NewGuid().ToString(), customer.RegisteredInStoreId);
                                _workflowMessageService.SendCustomerEmailValidationMessage(customer, _workContext.WorkingLanguage.Id);

                                //result
                                return RedirectToRoute("RegisterResult",
                                    new { resultId = (int)UserRegistrationType.EmailValidation });
                            }
                        case UserRegistrationType.AdminApproval:
                            {
                                return RedirectToRoute("RegisterResult",
                                    new { resultId = (int)UserRegistrationType.AdminApproval });
                            }
                        case UserRegistrationType.Standard:
                            {
                                var agentRole = _customerService.GetCustomerRoleBySystemName("Agents");
                                if (agentRole != null)
                                {
                                    customer.CustomerCustomerRoleMappings.Add(new CustomerCustomerRoleMapping { CustomerRole = agentRole });
                                    _customerService.UpdateCustomer(customer);
                                }

                                //send customer welcome message
                                //_workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

                                return RedirectToRoute("RegisterResult",
                               new { resultId = (int)UserRegistrationType.AdminApproval });
                            }
                        default:
                            {
                                return RedirectToRoute("Homepage");
                            }
                    }
                }

                //errors
                foreach (var error in registrationResult.Errors)
                    ModelState.AddModelError("", error);
            }

            //If we got this far, something failed, redisplay form
            model = _customerModelFactory.PrepareAgentRegisterModel(model, true, customerAttributesXml);
            return View(model);
        }

        #endregion

        public virtual IActionResult DeleteVendorToken(string token)
        {
            var nbVendorToken = _nBVendorTokenService.GetNBVendorTokenByToken(token);

            //delete vendor notification token on logout
            if (nbVendorToken != null)
            {
                nbVendorToken.Deleted = true;
                _nBVendorTokenService.UpdateNBVendorToken(nbVendorToken);
            }
            return Json("");
        }

        #region Facebook
        public JsonResult FacebookLoginCallback(string accessToken, string email, string userId, string name)
        {
            //create external authentication parameters
            var authenticationParameters = new ExternalAuthenticationParameters
            {
                ProviderSystemName = "ExternalAuth.Facebook",
                AccessToken = accessToken,
                Email = email,
                ExternalIdentifier = userId,
                ExternalDisplayIdentifier = name,
            };

            //authenticate Nop user
            _externalAuthenticationService.Authenticate(authenticationParameters, "/", false);

            return Json(new { success = true });
        }
        #endregion

        #region My account / Favorite merchnats

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult FavoriteMerchants(int? pageNumber)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;
            if (customer == null)
                return RedirectToRoute("Homepage");

            var model = _catalogModelFactory.PrepareCustomerFavoriteMerchantsModel(customer.Id, pageNumber);
            return View(model);
        }

        #endregion

        #region Plan and packages payment

        public virtual IActionResult PackagePayment(int id)
        {
            var model = PreparePackagePaymentInfoModel(id);
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult PackagePayment(PackagePaymentInfoModel model)
        {
            var customer = _customerService.GetCustomerById(model.CustomerId);
            if (customer == null)
                return RedirectToRoute("Homepage");

            var customerPackage = _nBCustomerPackageService.GetPackageByCustomerId(customer.Id);
            if (customerPackage == null)
                ModelState.AddModelError("", _localizationService.GetResource("NB.Package.PackagePayment.CustomerPackage.CustomerSubscriptionNotExit"));

            if (string.IsNullOrEmpty(customerPackage.StripeCustomerId) == false)
                ModelState.AddModelError("", _localizationService.GetResource("NB.Package.PackagePayment.CustomerPackage.AlreadySubscribed"));

            string key = _settingService.GetSettingByKey<string>("setting.storesetup.accountbillingstripeapikey", string.Empty, _storeContext.CurrentStore.Id);
            if (string.IsNullOrEmpty(key))
                ModelState.AddModelError("", _localizationService.GetResource("NB.Package.PackagePayment.CustomerPackage.StripeAccountKeyMissing"));

            var warnings = new List<string>();
            //validate
            var validator = new PackagePaymentInfoValidator(_localizationService);
            var validationResult = validator.Validate(model);
            if (!validationResult.IsValid)
                warnings.AddRange(validationResult.Errors.Select(error => error.ErrorMessage));

            //return model if errors
            if (ModelState.ErrorCount > 0)
            {
                model = PreparePackagePaymentInfoModel(customer.Id);
                return View(model);
            }

            #region Stripe payment process

            try
            {
                stripe.StripeConfiguration.ApiKey = key;
                var address = customer.BillingAddress;

                var card = new stripe.TokenCardOptions();
                card.Name = model.CardholderName;
                card.Number = model.CardNumber;
                card.ExpYear = long.Parse(model.ExpireYear);
                card.ExpMonth = long.Parse(model.ExpireMonth);
                card.Cvc = model.CardCode;
                card.AddressCity = address?.City;
                card.AddressCountry = address?.Country?.Name;

                card.AddressLine1 = address?.Address1;

                //Assign Card to Token Object and create Token  
                var token = new stripe.TokenCreateOptions();
                token.Card = card;
                var serviceToken = new stripe.TokenService();
                var newToken = serviceToken.Create(token);

                //Create Customer Object and Register it on Stripe  
                var myCustomer = new stripe.CustomerCreateOptions();
                myCustomer.Email = customer.Email;
                myCustomer.Source = newToken.Id;
                var customerService = new stripe.CustomerService();
                var stripeCustomer = customerService.Create(myCustomer);

                //if price has no value it will take 1$ default
                if (model.Price == decimal.Zero)
                    model.Price = 1.00m;
                //get price
                var totalAmt =
                       _currencyService.ConvertFromPrimaryStoreCurrency(model.Price, _workContext.WorkingCurrency);

                //Create Charge Object with details of Charge  
                var options = new stripe.ChargeCreateOptions
                {
                    Amount = Convert.ToInt32(Math.Round(totalAmt * 100)),
                    Currency = _workContext.WorkingCurrency.CurrencyCode,
                    ReceiptEmail = customer.Email,
                    Customer = stripeCustomer.Id,
                    Description = model.CardholderName + " (" + customer.Email + ")" //Convert.ToString(tParams.TransactionId), //Optional  
                };
                //and Create Method of this object is doing the payment execution.  
                var service = new stripe.ChargeService();
                var charge = service.Create(options); // This will do the Payment  
                if (charge.Paid)
                {
                    if (customerPackage != null)
                    {
                        if (model.SaveCardDetail)
                        {
                            customerPackage.CreditCardType = model.CreditCardType;
                            customerPackage.CardholderName = model.CardholderName;
                            customerPackage.CardNumber = model.CardNumber;
                            customerPackage.ExpireYear = model.ExpireYear;
                            customerPackage.ExpireMonth = model.ExpireMonth;
                            customerPackage.CardCode = model.CardCode;
                        }

                        //adding extra field which can be useful in admin side 
                        customerPackage.PackageAmount = model.Price;
                        customerPackage.StripeId = charge.Id;
                        customerPackage.StripeCustomerId = charge.CustomerId;

                        _nBCustomerPackageService.UpdateCustomerPackageMapping(customerPackage);
                    }
                }
                else
                {
                    ModelState.AddModelError("", string.Format(_localizationService.GetResource("NB.Admin.AccountBillings.CreateStripe.Payment.Failed"), charge.FailureMessage));
                    _logger.Error(string.Format(_localizationService.GetResource("NB.Admin.AccountBillings.CreateStripe.Payment.Failed"), charge.FailureMessage));
                    model = PreparePackagePaymentInfoModel(customer.Id);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", string.Format(_localizationService.GetResource("NB.Admin.AccountBillings.CreateStripe.Payment.Failed"), ex.Message));
                _logger.Error(string.Format(_localizationService.GetResource("NB.Admin.AccountBillings.CreateStripe.Payment.Failed"), ex.Message));
                model = PreparePackagePaymentInfoModel(customer.Id);
                return View(model);
            }

            #endregion

            //activate customer
            customer.Active = true;
            _customerService.UpdateCustomer(customer);

            //Update the vendor and make active 
            var vendor = _vendorService.GetVendorById(customer.VendorId);
            if (vendor != null)
            {
                vendor.Active = true;
                _vendorService.UpdateVendor(vendor);
            }

            ////send customer welcome message
            _workflowMessageService.SendStoreCustomerSubscribingPackageNotificationMesage(customer, _workContext.WorkingLanguage.Id);

            var redirectUrl = Url.RouteUrl("RegisterResult",
                new { resultId = (int)UserRegistrationType.Standard }, _webHelper.CurrentRequestProtocol);
            return Redirect(redirectUrl);
        }

        #endregion

        protected virtual PackagePaymentInfoModel PreparePackagePaymentInfoModel(int customerId)
        {
            var model = new PackagePaymentInfoModel();

            var customerPackage = _nBCustomerPackageService.GetPackageByCustomerId(customerId);

            var packagePlan = _nbPackageService.GetPackagePlanById(customerPackage.PackagePlanId);
            model.PackageName = _nbPackageService.GetPackageById(customerPackage.PackageId)?.Name;
            if (packagePlan != null)
            {
                int.TryParse(packagePlan.TrailPeriod, out int trailPeriod);

                model.PlanName = packagePlan?.Name;
                model.Price = packagePlan.Price;
                model.PlanPrice = _priceFormatter.FormatPrice(packagePlan.Price, true, false);
                model.Validity = DateTime.UtcNow.AddDays(trailPeriod);
            }

            model.CustomerId = customerId;

            model.CreditCardTypes = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Visa", Value = "visa" },
                    new SelectListItem { Text = "Master card", Value = "MasterCard" },
                    new SelectListItem { Text = "Discover", Value = "Discover" },
                    new SelectListItem { Text = "Amex", Value = "Amex" },
                };
            //years
            for (var i = 0; i < 15; i++)
            {
                var year = (DateTime.Now.Year + i).ToString();
                model.ExpireYears.Add(new SelectListItem { Text = year, Value = year, });
            }

            //months
            for (var i = 1; i <= 12; i++)
            {
                model.ExpireMonths.Add(new SelectListItem { Text = i.ToString("D2"), Value = i.ToString(), });
            }

            return model;
        }

        /// <summary>
        /// Venue Name Is Valid
        /// </summary>
        /// <param name="venueName"></param>
        /// <returns></returns>
        protected virtual bool VenueNameExist(string venueName)
        {
            return _storeService.GetStoreNameExist(venueName);
        }

        /// <summary>
        /// Venue Url Is Valid
        /// </summary>
        /// <param name="venueName"></param>
        /// <returns></returns>
        protected virtual bool VenueUrlHostExist(string venueUrl)
        {
            return _storeService.GetStoreHostExist(venueUrl);
        }

        [HttpPost]
        public virtual IActionResult VenueNameExistCall(string venueName)
        {
            return Json(VenueNameExist(venueName));
        }

        [HttpPost]
        public virtual IActionResult VenueUrlHostExistCall(string venueUrl)
        {
            return Json(VenueUrlHostExist(venueUrl));
        }

        #region Rider Documents Upload

        [HttpPost]
        public virtual IActionResult UploadDocument(IFormFile uploadedFile, bool callFromPublicSide = false)
        {
            var customer = _workContext.CurrentCustomer;
            if (customer == null)
                return Challenge();

            //if its admin call than check in request form
            if (!callFromPublicSide)
            {
                var httpPostedFile = Request.Form.Files.FirstOrDefault();
                uploadedFile = httpPostedFile;
            }

            if (uploadedFile == null)
            {
                return Json(new
                {
                    success = false,
                    message = _localizationService.GetResource("NB.Common.FileUploader.NofileUploaded")
                });
            }

            try
            {
                //validate file type
                if (!_pictureService.IsValidDocFile(uploadedFile.FileName))
                {
                    var supportedFormatStr = _settingService.GetSettingByKey<string>("nb.document.supportedDocumentTypes.commasaperated.value", string.Empty, _workContext.GetCurrentStoreId, true);
                    if (string.IsNullOrEmpty(supportedFormatStr))
                        //add .pdf by default
                        supportedFormatStr = "pdf";
                    throw new NopException(string.Format(_localizationService.GetResource("NB.Register.Rider.Document.InvalideDocType"), supportedFormatStr));
                }

                //validate file size
                var docMaxSize = _settingService.GetSettingByKey<int>("nb.register.ridersetting.DocumentMaximumSizeBytes", storeId: _workContext.GetCurrentStoreId, loadSharedValueIfNotFound: true);
                if (uploadedFile.Length > docMaxSize)
                    throw new NopException(string.Format(_localizationService.GetResource("Nb.Account.Rider.Document.MaximumUploadedFileSize"), docMaxSize));

                var customerPictureBinary = _downloadService.GetDownloadBits(uploadedFile);
                var document = _pictureService.InsertPicture(customerPictureBinary, uploadedFile.ContentType, null, validateBinary: false);

                var uploadedFileId = 0;
                if (document != null)
                    uploadedFileId = document.Id;

                //get document url
                var documentUrl = _pictureService.GetDocumentUrl(document);

                //when returning JSON the mime-type must be set to text/plain
                //otherwise some browsers will pop-up a "Save As" dialog.
                return Json(new
                {
                    success = true,
                    uploadedFileId = uploadedFileId,
                    documentUrl = documentUrl,
                    message = _localizationService.GetResource("NB.Common.FileUploader.Upload.Success")
                });

            }
            catch (Exception exc)
            {
                return Json(new
                {
                    success = false,
                    message = exc.Message
                });
            }
        }

        #endregion

        #endregion
    }
}