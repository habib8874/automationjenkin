﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.AccountsBilling
{
    /// <summary>
    /// Represents a Account Billing model
    /// </summary>
    public partial class AccountBillingModel : BaseNopEntityModel
    {
        #region Ctor

        public AccountBillingModel()
        {
            AccountSubscriptionDetailModel = new AccountSubscriptionDetailModel();
            AccountDocumentModel = new AccountDocumentModel();
            AccountTransactionSearchModel = new AccountTransactionSearchModel();
            AvailableCountries = new List<SelectListItem>();
            AvailableStates = new List<SelectListItem>();
            AccountPayoutMethodModel = new AccountPayoutMethodModel();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("NB.Admin.AccountBillings.MerchantBilling.Field.FirstName")]
        public string FirstName { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.MerchantBilling.Field.LastName")]
        public string LastName { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.MerchantBilling.Field.PhoneNumber")]
        public string PhoneNumber { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.MerchantBilling.Field.EmailAddress")]
        public string EmailAddress { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.MerchantBilling.Field.AddressLine1")]
        public string AddressLine1 { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.MerchantBilling.Field.AddressLine2")]
        public string AddressLine2 { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.MerchantBilling.Field.City")]
        public string City { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.MerchantBilling.Field.ZipCode")]
        public string ZipCode { get; set; }

        [NopResourceDisplayName("Admin.Customers.Customers.Fields.Country")]
        public int CountryId { get; set; }

        public IList<SelectListItem> AvailableCountries { get; set; }

        [NopResourceDisplayName("Admin.Customers.Customers.Fields.StateProvince")]
        public int StateProvinceId { get; set; }

        public IList<SelectListItem> AvailableStates { get; set; }

        public int CustomerId { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.MerchantBilling.Field.CurrentStripeBalance")]
        public decimal CurrentStripeBalance { get; set; }

        //pictures
        public AccountSubscriptionDetailModel AccountSubscriptionDetailModel { get; set; }

        public AccountDocumentModel AccountDocumentModel { get; set; }

        public AccountTransactionSearchModel AccountTransactionSearchModel { get; set; }

        public AccountPayoutMethodModel AccountPayoutMethodModel { get; set; }

        #endregion
    }
}
