﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.Orders
{
    public class AgentAvailibiltyForPushNotification : BaseEntity
    {
        public int AgentId { get; set; }
        public long MobileNo { get; set; }
        public string DeviceToken { get; set; }
    }

    public class AgentStoreName : BaseEntity
    {
        public string StoreName { get; set; }
    }

    
    public class AgentNotificationKey : BaseEntity
    {
        public string KW1 { get; set; }
        public string KW2 { get; set; }
        public string AW2 { get; set; }
        public string AW3 { get; set; }
        public string KA3 { get; set; }
        public string KA1 { get; set; }
    }
}
