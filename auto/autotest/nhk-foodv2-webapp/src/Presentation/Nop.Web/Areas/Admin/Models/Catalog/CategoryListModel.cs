﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category list model
    /// </summary>
    public partial class CategoryListModel : BasePagedListModel<CategoryModel>
    {
        public CategoryListModel()
        {

            AvailableVendors = new List<SelectListItem>(); //Mah
        }

        //Mah
        [NopResourceDisplayName("Admin.Catalog.Categories.List.SearchVendor")]
        public int SearchVendorId { get; set; }
        public IList<SelectListItem> AvailableVendors { get; set; }
        //Mah
    }
}