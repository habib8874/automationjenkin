﻿using Nop.Web.Framework.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a product attribute search model
    /// </summary>
    public partial class ProductAttributeSearchModel : BaseSearchModel
    {
        public ProductAttributeSearchModel()
        {
            AvailableStores = new List<SelectListItem>();
            AvailableVendors = new List<SelectListItem>();
        }
  

        public IList<SelectListItem> AvailableStores { get; set; }
        public int SearchStores { get; set; }

        public IList<SelectListItem> AvailableVendors { get; set; }
        public int SearchVendor { get; set; }
    }
}