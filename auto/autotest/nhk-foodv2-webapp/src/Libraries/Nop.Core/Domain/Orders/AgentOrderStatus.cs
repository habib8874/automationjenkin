﻿using System;

namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents an order item
    /// </summary>
    public partial class AgentOrderStatus : BaseEntity
    {
        
        public int AgentId { get; set; }

        public int OrderId { get; set; }
        public Nullable<DateTime> OrderDt { get; set; }
        public int OrderStatus { get; set; }
        public Nullable<DateTime> AgentStatusDt { get; set; }
        public string AgentLat { get; set; }
        public string AgentLong { get; set; }
        public Nullable<DateTime> SysDt { get; set; }

        public int CityId { get; set; }
    }
}