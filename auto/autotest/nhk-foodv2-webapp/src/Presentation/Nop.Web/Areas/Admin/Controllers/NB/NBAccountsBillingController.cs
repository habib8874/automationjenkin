﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.NB.Package;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Factories.NB;
using Nop.Web.Areas.Admin.Models.NB.AccountsBilling;
using Nop.Web.Areas.Admin.Validators.Packages;
using Nop.Web.Validators.NB.CustomerPackage;
using Stripe;

namespace Nop.Web.Areas.Admin.Controllers.NB
{
    public partial class NBAccountsBillingController : BaseAdminController
    {
        #region Fields

        private readonly IStateProvinceService _stateProvinceService;
        private readonly ICountryService _countryService;
        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IPictureService _pictureService;
        private readonly IWorkContext _workContext;
        private readonly ISettingService _settingService;
        private readonly ILogger _logger;
        private readonly IStoreContext _storeContext;
        private readonly INBAccountBillingModelFactory _nBAccountBillingModelFactory;
        private readonly ICustomerService _customerService;
        private readonly INBPackageService _nBPackageService;
        private readonly ICurrencyService _currencyService;
        private readonly INBCustomerPackageService _nBCustomerPackageService;

        #endregion

        #region Ctor

        public NBAccountsBillingController(
            IStateProvinceService stateProvinceService,
            ICountryService countryService,
            ILocalizationService localizationService,
            INotificationService notificationService,
            IPermissionService permissionService,
            IPictureService pictureService,
            IWorkContext workContext,
            ISettingService settingService,
            ILogger logger,
            IStoreContext storeContext,
            INBAccountBillingModelFactory nBAccountBillingModelFactory,
            ICustomerService customerService,
            INBPackageService nBPackageService,
            ICurrencyService currencyService,
            INBCustomerPackageService nBCustomerPackageService)
        {
            _stateProvinceService = stateProvinceService;
            _countryService = countryService;
            _localizationService = localizationService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _pictureService = pictureService;
            _workContext = workContext;
            _settingService = settingService;
            _logger = logger;
            _storeContext = storeContext;
            _nBAccountBillingModelFactory = nBAccountBillingModelFactory;
            _customerService = customerService;
            _nBPackageService = nBPackageService;
            _currencyService = currencyService;
            _nBCustomerPackageService = nBCustomerPackageService;
        }

        #endregion

        #region Methods

        #region Create / Edit / Delete

        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBAccountsBillings))
                return AccessDeniedView();

            var model = new AccountBillingModel();
            try
            {
                string key = _settingService.GetSettingByKey<string>("setting.storesetup.accountbillingstripeapikey", string.Empty, _storeContext.CurrentStore.Id);
                if (string.IsNullOrEmpty(key))
                {
                    //prepare model
                    model = _nBAccountBillingModelFactory.PrepareAccountBillingModel(new StripeAccountDetailResponseModel());
                    _notificationService.ErrorNotification(_localizationService.GetResource("NB.Package.PackagePayment.CustomerPackage.StripeAccountKeyMissing"));
                    return View(model);
                }

                StripeConfiguration.ApiKey = key;
                var stripeAccountId = _settingService.GetSettingByKey<string>(string.Format("accountbillingstripeconnectedaccount-{0}", _workContext.CurrentCustomer.Id), null, _storeContext.CurrentStore.Id);

                //try to get previously saved restricted customer role identifier
                if (string.IsNullOrEmpty(stripeAccountId) == false)
                {
                    var service = new AccountService();
                    var getAccountDetail = service.Get(stripeAccountId.ToString());
                    if (getAccountDetail.StripeResponse.StatusCode.ToString() == "OK")
                    {
                        var responseModel = JsonConvert.DeserializeObject<StripeAccountDetailResponseModel>(getAccountDetail.StripeResponse.Content);
                        model = _nBAccountBillingModelFactory.PrepareAccountBillingModel(responseModel);

                        var balanceService = new BalanceService();
                        var balance = balanceService.Get();
                        if (balance.StripeResponse.StatusCode.ToString() == "OK")
                        {
                            model.CurrentStripeBalance = balance.Available.FirstOrDefault().Amount;
                        }
                        var documentFrontPictureId = _settingService.GetSettingByKey<int>(string.Format("accountbillingstripedocumentpicturefront-{0}", _workContext.CurrentCustomer.Id), storeId: _storeContext.CurrentStore.Id);
                        var documentBackPictureId = _settingService.GetSettingByKey<int>(string.Format("accountbillingstripedocumentpictureback-{0}", _workContext.CurrentCustomer.Id), storeId: _storeContext.CurrentStore.Id);
                        if (documentFrontPictureId > 0 && documentBackPictureId > 0)
                        {
                            model.AccountDocumentModel.IdentityProofFront = documentFrontPictureId;
                            model.AccountDocumentModel.IdentityProofBack = documentBackPictureId;
                        }

                        model.AccountPayoutMethodModel.AccountNumber = _settingService.GetSettingByKey<string>(string.Format("accountbillingstripeaccountnumber-{0}", _workContext.CurrentCustomer.Id), storeId: _storeContext.CurrentStore.Id);
                        model.AccountPayoutMethodModel.RoutingNumber = _settingService.GetSettingByKey<string>(string.Format("accountbillingstriperoutingnumber-{0}", _workContext.CurrentCustomer.Id), storeId: _storeContext.CurrentStore.Id);
                     
                        return View(model);
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error(_localizationService.GetResource("NB.Admin.AccountBillings.stripeConnectedAccount.Fetching.ConnectedAccount.Error"), ex);
            }

            //prepare model
            model = _nBAccountBillingModelFactory.PrepareAccountBillingModel(new StripeAccountDetailResponseModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Create(AccountBillingModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBAccountsBillings))
                return AccessDeniedView();

            var stripeAccountId = _settingService.GetSettingByKey<string>(string.Format("accountbillingstripeconnectedaccount-{0}", _workContext.CurrentCustomer.Id), null, _storeContext.CurrentStore.Id);
            if (string.IsNullOrEmpty(stripeAccountId) == false)
            {
                _notificationService.ErrorNotification(string.Format(_localizationService.GetResource("NB.Admin.AccountBillings.stripeConnectedAccount.AlreadyAdded"),
                    _customerService.GetCustomerFullName(_workContext.CurrentCustomer)));
                return Create();
            }
            var warnings = new List<string>();

            var validator = new AccountsBillingValidator(_localizationService, _stateProvinceService);

            var validationResult = validator.Validate(model);

            if (!validationResult.IsValid)
                warnings.AddRange(validationResult.Errors.Select(error => error.ErrorMessage));

            //return model if errors
            if (ModelState.ErrorCount > 0)
            {
                model = _nBAccountBillingModelFactory.PrepareAccountBillingModel(new StripeAccountDetailResponseModel());
                return View(model);
            }
            try
            {
                string key = _settingService.GetSettingByKey<string>("setting.storesetup.accountbillingstripeapikey", string.Empty, _storeContext.CurrentStore.Id);
                if (string.IsNullOrEmpty(key))
                {
                    //prepare model
                    model = _nBAccountBillingModelFactory.PrepareAccountBillingModel(new StripeAccountDetailResponseModel());
                    _notificationService.ErrorNotification(_localizationService.GetResource("NB.Package.PackagePayment.CustomerPackage.StripeAccountKeyMissing"));
                    return View(model);
                }
                StripeConfiguration.ApiKey = key;
                var country = _countryService.GetCountryById(model.CountryId);

                #region Upload Document
                //Add document Name in the list 
                var documentList = new List<string>();
                var webClient = new WebClient();
                //declare the file service of Stripe
                var fileService = new FileService();

                //reading file using picture url
                var streamIdentityProofFront = webClient.OpenRead(_pictureService.GetPictureUrl(model.AccountDocumentModel.IdentityProofFront));
                var streamIdentityProofBack = webClient.OpenRead(_pictureService.GetPictureUrl(model.AccountDocumentModel.IdentityProofBack));

                //Create file service for stripe
                var identityProofFront = new FileCreateOptions
                {
                    File = streamIdentityProofFront,
                    Purpose = FilePurpose.DisputeEvidence
                };
                var identityProofBack = new FileCreateOptions
                {
                    File = streamIdentityProofBack,
                    Purpose = FilePurpose.DisputeEvidence
                };

                //adding file id in list 
                documentList.Add(fileService.Create(identityProofFront)?.Id);
                documentList.Add(fileService.Create(identityProofBack)?.Id);

                #endregion

                var options = new AccountCreateOptions
                {
                    //The type of Stripe account to create. May be one of custom, express or standard.
                    Type = "custom",
                    Country = country != null ? country.TwoLetterIsoCode : "US",
                    Email = model.EmailAddress,
                    BusinessType = "individual",
                    Capabilities = new AccountCapabilitiesOptions
                    {
                        CardPayments = new AccountCapabilitiesCardPaymentsOptions
                        {
                            Requested = true,
                        },
                        Transfers = new AccountCapabilitiesTransfersOptions
                        {
                            Requested = true,
                        },
                    },
                    //Information about the company or business.This field is available for any business_type.
                    Company = new AccountCompanyOptions
                    {
                        Address = new AddressOptions
                        {
                            City = model.City,
                            Country = country != null ? country.TwoLetterIsoCode : "US",
                            Line1 = model.AddressLine1,
                            Line2 = model.AddressLine2,
                            PostalCode = model.ZipCode,
                            State = _stateProvinceService.GetStateProvinceById(model.StateProvinceId)?.Abbreviation,
                        },
                        Phone = model.PhoneNumber,
                        Name = model.FirstName + model.LastName
                    },
                    Documents = new AccountDocumentsOptions
                    {
                        BankAccountOwnershipVerification = new AccountDocumentsBankAccountOwnershipVerificationOptions
                        {
                            Files = documentList
                        },
                    },
                };

                var service = new AccountService();
                //create  a new service account 
                var accountCreate = service.Create(options);

                //store account id in setting file 
                _settingService.SetSetting(string.Format("accountbillingstripeconnectedaccount-{0}", _workContext.CurrentCustomer.Id), accountCreate.Id, _storeContext.CurrentStore.Id);
                //Store Picture id in setting as need to display after success 
                _settingService.SetSetting(string.Format("accountbillingstripedocumentpicturefront-{0}", _workContext.CurrentCustomer.Id), model.AccountDocumentModel.IdentityProofFront, _storeContext.CurrentStore.Id);
                _settingService.SetSetting(string.Format("accountbillingstripedocumentpictureback-{0}", _workContext.CurrentCustomer.Id), model.AccountDocumentModel.IdentityProofBack, _storeContext.CurrentStore.Id);

                _notificationService.SuccessNotification(_localizationService.GetResource("NB.Admin.AccountBillings.stripeConnectedAccount.Success"));

                return Create();
            }
            catch (Exception ex)
            {
                _notificationService.ErrorNotification(_localizationService.GetResource("NB.Admin.AccountBillings.stripeConnectedAccount.Error") + ex);
                _logger.Error(_localizationService.GetResource("NB.Admin.AccountBillings.stripeConnectedAccount.Error"), ex);
            }

            model = _nBAccountBillingModelFactory.PrepareAccountBillingModel(new StripeAccountDetailResponseModel());
            //if we got this far, something failed, redisplay form
            return View(model);
        }

        #endregion

        #region Transaction log

        [HttpPost]
        public virtual IActionResult ListAccountTransaction(AccountTransactionSearchModel searchModel)
        {
            //prepare model
            var model = _nBAccountBillingModelFactory.PrepareAccountTransactionListModel(searchModel);

            return Json(model);
        }

        #endregion

        #region Package Subscription Update Module

        public virtual IActionResult PackageUpdatePopup(int customerId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBAccountsBillings))
                return AccessDeniedView();

            //prepare model
            var model = _nBAccountBillingModelFactory.PrepareAccountPackagePaymentUpdateModel(customerId);
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult PackageUpdatePopup(AccountPackagePaymentUpdateModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNBAccountsBillings))
                return AccessDeniedView();

            if (model.PackageId == 0 || model.PackagePlanId == 0)
            {
                _notificationService.ErrorNotification(_localizationService.GetResource("NB.Admin.AccountBillings.PackagePlanMissing.Error"));
                return View(_nBAccountBillingModelFactory.PrepareAccountPackagePaymentUpdateModel(model.CustomerId));
            }

            if (model.PackageId>0 && model.PackagePlanId >0)
            {
                // we will add the logic to redirect to route to package plan from here  like this 
                return RedirectToAction("UpdatePackagePayment", new {model.PackageId,model.PackagePlanId, formId = "accountbilling-form" });
            }
            //prepare model
            var accountPackageModel = _nBAccountBillingModelFactory.PrepareAccountPackagePaymentUpdateModel(model.CustomerId);
            return View(accountPackageModel);
        }

        public virtual IActionResult GetPackagePlanByPackageId(string packageId, bool? addSelectStateItem)
        {
            //permission validation is not required here

            // This action method gets called via an ajax request
            if (string.IsNullOrEmpty(packageId))
                throw new ArgumentNullException(nameof(packageId));

            var package = _nBPackageService.GetPackageById(Convert.ToInt32(packageId));
            var packagePlans = _nBPackageService.GetPackagePlans(package != null ? package.Id : 0).ToList();
            var result = (from s in packagePlans
                          select new { id = s.Id, name = s.Name }).ToList();

            result.Insert(0, new { id = 0, name = _localizationService.GetResource("NB.Account.Fields.SelectPackagePlan") });

            return Json(result);
        }

        #endregion

        #region Plan and packages payment

        public virtual IActionResult UpdatePackagePayment(int packageId, int packagePlanId, string formId)
        {
            var model = _nBAccountBillingModelFactory.PreparePackagePaymentInfoModel(packageId, packagePlanId,_workContext.CurrentCustomer.Id);
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult UpdatePackagePayment(AccountPackagePaymentUpdateModel model)
        {
            var customer = _customerService.GetCustomerById(model.CustomerId);
            if (customer == null)
                return RedirectToRoute("Create");

            var customerPackage = _nBCustomerPackageService.GetPackageByCustomerId(customer.Id);

            //if (model.UseExistingCard == false)
            //{
                var warnings = new List<string>();
                //validate
                var validator = new AccountPackagePaymentInfoValidator(_localizationService);
                var validationResult = validator.Validate(model);
                if (!validationResult.IsValid)
                    warnings.AddRange(validationResult.Errors.Select(error => error.ErrorMessage));

                //return model if errors
                if (ModelState.ErrorCount > 0)
                {
                    model = _nBAccountBillingModelFactory.PreparePackagePaymentInfoModel(model.PackageId,model.PackagePlanId,customer.Id);
                    return View(model);
                }
            //}

            #region Stripe payment process

            try
            {
                string key = _settingService.GetSettingByKey<string>("setting.storesetup.accountbillingstripeapikey", string.Empty, _storeContext.CurrentStore.Id);
                if (string.IsNullOrEmpty(key))
                {
                    _notificationService.ErrorNotification(_localizationService.GetResource("NB.Package.PackagePayment.CustomerPackage.StripeAccountKeyMissing"));
                    model = _nBAccountBillingModelFactory.PreparePackagePaymentInfoModel(model.PackageId, model.PackagePlanId, customer.Id);
                    return View(model);
                }

                if (customerPackage == null)
                {
                    _notificationService.ErrorNotification(string.Format(_localizationService.GetResource("NB.Admin.AccountBillings.CustomerPackge.Customer.NotFound"),
                   _customerService.GetCustomerFullName(_workContext.CurrentCustomer)));
                    model = _nBAccountBillingModelFactory.PreparePackagePaymentInfoModel(model.PackageId, model.PackagePlanId, customer.Id);
                    return View(model);
                }

                // After discuss with mam , we will add dynamic key here 
                StripeConfiguration.ApiKey = key;
                var address = customer.BillingAddress;

                var card = new TokenCardOptions();

                card.Name = model.UseExistingCard ? customerPackage.CardholderName : model.CardholderName;
                card.Number = model.UseExistingCard ? customerPackage.CardNumber : model.CardNumber;
                card.ExpYear = model.UseExistingCard ? long.Parse(customerPackage.ExpireYear) : long.Parse(model.ExpireYear);
                card.ExpMonth = model.UseExistingCard ? long.Parse(customerPackage.ExpireMonth) : long.Parse(model.ExpireMonth);
                card.Cvc = model.UseExistingCard ? customerPackage.CardCode : model.CardCode;

                card.AddressCity = address?.City;
                card.AddressCountry = address?.Country?.Name;

                card.AddressLine1 = address?.Address1;

                //Assign Card to Token Object and create Token  
                var token = new TokenCreateOptions();
                token.Card = card;
                var serviceToken = new TokenService();
                var newToken = serviceToken.Create(token);

                //if price has no value it will take 1$ default
                if (model.AmountToBePaidPrice == decimal.Zero)
                    model.AmountToBePaidPrice = 2.00m;
                //get price
                var totalAmt =
                       _currencyService.ConvertFromPrimaryStoreCurrency(model.AmountToBePaidPrice, _workContext.WorkingCurrency);

                //Create Charge Object with details of Charge  
                var options = new ChargeCreateOptions
                {
                    Amount = Convert.ToInt32(Math.Round(totalAmt * 100)),
                    Currency = _workContext.WorkingCurrency.CurrencyCode,
                    ReceiptEmail = customer.Email,
                    Customer = customerPackage.StripeCustomerId,
                    Description = model.CardholderName + " (" + customer.Email + ")" //Convert.ToString(tParams.TransactionId), //Optional  
                };
                //and Create Method of this object is doing the payment execution.  
                var service = new ChargeService();
                var charge = service.Create(options); // This will do the Payment  
                if (charge.Paid)
                {
                    if (customerPackage != null)
                    {
                        if (model.SaveCardDetail)
                        {
                            customerPackage.CreditCardType = model.CreditCardType;
                            customerPackage.CardholderName = model.CardholderName;
                            customerPackage.CardNumber = model.CardNumber;
                            customerPackage.ExpireYear = model.ExpireYear;
                            customerPackage.ExpireMonth = model.ExpireMonth;
                            customerPackage.CardCode = model.CardCode;
                        }

                        //adding extra field which can be useful in admin side 
                        customerPackage.PackageId = model.PackageId;
                        customerPackage.PackagePlanId = model.PackagePlanId;
                        customerPackage.PackageAmount = model.AmountToBePaidPrice;
                        customerPackage.StripeId = charge.Id;

                        _nBCustomerPackageService.UpdateCustomerPackageMapping(customerPackage);
                    }
                }
                else
                {
                    ModelState.AddModelError("", string.Format(_localizationService.GetResource("NB.Admin.AccountBillings.CreateStripe.Payment.Failed"), charge.FailureMessage));
                    _logger.Error(string.Format(_localizationService.GetResource("NB.Admin.AccountBillings.CreateStripe.Payment.Failed"), charge.FailureMessage));
                    model = _nBAccountBillingModelFactory.PreparePackagePaymentInfoModel(model.PackageId, model.PackagePlanId, customer.Id);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", string.Format(_localizationService.GetResource("NB.Admin.AccountBillings.CreateStripe.Payment.Failed"), ex));
                _logger.Error(string.Format(_localizationService.GetResource("NB.Admin.AccountBillings.CreateStripe.Payment.Failed"), ex));
                model = _nBAccountBillingModelFactory.PreparePackagePaymentInfoModel(model.PackageId, model.PackagePlanId, customer.Id);
                return View(model);
            }

            #endregion

            ViewBag.RefreshPage = true;
            return View(model);
        }

        #endregion

        public virtual IActionResult PayoutMethodAdd(string accountNumber, string routingNumber)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            if (string.IsNullOrEmpty(accountNumber))
            {
                return Json(new
                {
                    Result = false,
                    message = _localizationService.GetResource("NB.Admin.AccountBillings.PayoutMethods.Required.AccountNumber")
                });
            }
            try
            {
                string key = _settingService.GetSettingByKey<string>("setting.storesetup.accountbillingstripeapikey", string.Empty, _storeContext.CurrentStore.Id);
                if (string.IsNullOrEmpty(key))
                {
                    return Json(new
                    {
                        Result = false,
                        message = _localizationService.GetResource("NB.Admin.AccountBillings.PayoutMethods.StripeKey.Required")
                    });
                }

                StripeConfiguration.ApiKey = key;

                var customer = _workContext.CurrentCustomer;

                var country = _countryService.GetCountryById(customer.BillingAddressId ?? 0)?.TwoLetterIsoCode;
                var options = new TokenCreateOptions
                {
                    BankAccount = new TokenBankAccountOptions
                    {
                        Country = string.IsNullOrEmpty(country) ? "US" : country,
                        //Currency = _workContext.WorkingCurrency.CurrencyCode,
                        Currency = _workContext.WorkingCurrency.CurrencyCode,
                        AccountHolderName = _customerService.GetCustomerFullName(customer),
                        AccountHolderType = "individual",
                        //RoutingNumber = "110000000",
                        //AccountNumber = "000123456789",
                        RoutingNumber = routingNumber,
                        AccountNumber = accountNumber,
                    },
                };
                var service = new TokenService();
                var bankaccount = service.Create(options);

                if(string.IsNullOrEmpty(bankaccount?.Id)==false)
                {
                    // suggest to mam , to store all this setting into DB  , so saving same thing to setting
                    _settingService.SetSetting(string.Format("accountbillingstripeaccountnumber-{0}", _workContext.CurrentCustomer.Id), accountNumber, _storeContext.CurrentStore.Id);
                    _settingService.SetSetting(string.Format("accountbillingstriperoutingnumber-{0}", _workContext.CurrentCustomer.Id), routingNumber, _storeContext.CurrentStore.Id);
                    _settingService.SetSetting(string.Format("accountbillingstripebankaccountnumber-{0}", _workContext.CurrentCustomer.Id), bankaccount.Id, _storeContext.CurrentStore.Id);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Result = false,
                    message = _localizationService.GetResource("NB.Admin.AccountBillings.PayoutMethods.StripeBankCreate.Fail") + ex
                });
            }

            return Json(new
            {
                Result = true,
                message = _localizationService.GetResource("NB.Admin.AccountBillings.PayoutMethods.StripeBankCreate.success")
            });
        }
        #endregion
    }
}