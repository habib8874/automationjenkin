﻿namespace Nop.Plugin.Payments.WalletPay
{
    /// <summary>
    /// Represents defaults for the 'WalletPay' plugin
    /// </summary>
    public static class WalletPayDefaults
    {
        /// <summary>
        /// Gets a name of the view component to display payment info in public store
        /// </summary>
        public const string PAYMENT_INFO_VIEW_COMPONENT_NAME = "PaymentWalletPay";
    }
}
