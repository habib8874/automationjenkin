﻿using NHKCustomerApplication.Models;
using NHKCustomerApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NHKCustomerApplication.Repository
{
    public interface IWalletRepository
    {
        /// <summary>
        /// GetWalletTransactions
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        List<WalletRequestModel> GetWalletTransactions(int customerId);
        /// <summary>
        /// AddMoneyToWallet
        /// </summary>
        /// <param name="wallet"></param>
        void AddMoneyToWallet(CustomerWallet wallet);
    }
}
