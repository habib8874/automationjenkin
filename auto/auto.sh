#!/bin/bash
PROJECT_TYPE=$1
#DEFINE PROJECT NAME FROM SECOND ARGUMENT
PROJECT_NAME=$2

mkdir -p $PROJECT_NAME

web=$3
customer=$4
agent=$5

USERNAME="habib8874"
USERPASS="Hrs%4012345"

if [[ "$PROJECT_TYPE" == "ecommerce" ]]; then
    db="ecommercev2dev"
    web_repo="nhk-ecommercev1.1-webapp"
    web_folder="NHK-Ecommercev2-webapp"
    customer_repo="nhk-ecommercev1.1-customerapi"
    customer_folder="NHK-Ecommercev2-customerAPI"
    agent_repo=""
    agent_folder=""
    new_web_repo="nhk-ecommerce-${PROJECT_NAME}-web"
    new_customer_repo="nhk-ecommerce-${PROJECT_NAME}-customerAPI"
    new_agent_repo="nhk-ecommerce-${PROJECT_NAME}-agentAPI"
    newdb=$2dev

elif [[ "$PROJECT_TYPE" == "food" ]]; then
    db="foodv2dev"
    web_repo="nhk-foodv2-webapp"
    web_folder="nhk-foodv2-webapp"
    customer_repo="nhk-foodv2-customerapi"
    customer_folder="NHK-Foodv2-customerAPI"
    agent_repo=nhk-"foodv2-agentapi"
    agent_folder="NHK-Foodv2-agentAPI"
    new_web_repo="nhk-food-${PROJECT_NAME}-web"
    new_customer_repo="nhk-food-${PROJECT_NAME}-customerAPI"
    new_agent_repo="nhk-food-${PROJECT_NAME}-agentAPI"
    newdb=$2dev

elif [[ "$PROJECT_TYPE" == "grocery" ]]; then
    db=""
    web_repo=""
    web_folder=""
    customer_repo=""
    customer_folder=""
    agent_repo=""
    agent_folder=""
    new_web_repo="nhk-grocery-${PROJECT_NAME}-web"
    new_customer_repo="nhk-grocery-${PROJECT_NAME}-customerAPI"
    new_agent_repo="nhk-grocery-${PROJECT_NAME}-agentAPI"
    newdb=$2dev

elif [[ "$PROJECT_TYPE" == "rental" ]]; then
    db="fooddevdb"
    web_repo=""
    web_folder=""
    customer_repo=""
    customer_folder=""
    agent_repo=""
    agent_folder=""
    new_web_repo="nhk-rental-${PROJECT_NAME}-web"
    new_customer_repo="nhk-rental-${PROJECT_NAME}-customerAPI"
    new_agent_repo="nhk-rental-${PROJECT_NAME}-agentAPI"
    newdb=$2dev

elif [[ "$PROJECT_TYPE" == "foodkiosk" ]]; then
    db="fooddevdb"
    web_repo="self-ordering-kiosk-web"
    web_folder="Self Ordering Kiosk Web"
    customer_repo="self-ordering-customer-api"
    customer_folder="self-ordering-customer-api"
    agent_repo=""
    agent_folder=""
    new_web_repo="nhk-foodkiosk-${PROJECT_NAME}-web"
    new_customer_repo="nhk-foodkiosk-${PROJECT_NAME}-customerAPI"
    new_agent_repo="nhk-foodkiosk-${PROJECT_NAME}-agentAPI"
    newdb=$2dev
else
    echo "invalid project type"
fi


#############################################################################################################################################################################################
#DB SETUP

#BACKUP DATABASE
/opt/mssql-tools/bin/sqlcmd -S 122.186.73.186,1433 -U SA -P 'CXs#@221Qasa' -Q "BACKUP DATABASE [$db] TO DISK = N'/var/opt/mssql/data/autobackups/$db.bak' WITH NOFORMAT, NOINIT, NAME = '$db', SKIP, NOREWIND, NOUNLOAD, STATS = 10"


/opt/mssql-tools/bin/sqlcmd -S 122.186.73.186,1433 -U SA -P 'CXs#@221Qasa' -Q " RESTORE FILELISTONLY FROM DISK='/var/opt/mssql/data/autobackups/$db.bak'" > datalogfile
grep -ow '\bDB_\w*' datalogfile > datalogfile1
rm -rf datalogfile
filecount=0
for word in $(cat datalogfile1)
do
        declare -a filecount_array
        filecount_array[filecount]=`echo $word`
        filecount=+1
done

#RESTORE DATABASE
/opt/mssql-tools/bin/sqlcmd -S 122.186.73.186,1433 -U SA -P 'CXs#@221Qasa'  -Q "
RESTORE DATABASE [$newdb] FROM DISK='/var/opt/mssql/data/autobackups/$db.bak'
WITH
   MOVE '${filecount_array[0]}' TO '/var/opt/mssql/data/${newdb}.mdf',
   MOVE '${filecount_array[1]}' TO '/var/opt/mssql/data/${newdb}_log.ldf' "



#set username and password
DBUSER=$2userdev
DBPASS=`echo $1 | base64`
#echo $DBPASS
/opt/mssql-tools/bin/sqlcmd -S 122.186.73.186,1433 -U SA -P 'CXs#@221Qasa'  -Q "
CREATE LOGIN $DBUSER WITH PASSWORD = '${DBPASS}';
GO
"
#Missing assisgning db privileges to above user


#UPDATE STORE URL AND HOSTS VALUE
/opt/mssql-tools/bin/sqlcmd -S 122.186.73.186,1433 -U SA -P 'CXs#@221Qasa' -Q "
USE [$newdb]
GO
update store set Url='http://${2}dev.nestorhawk.com' ;
update store set Hosts='http://${2}dev.nestorhawk.com';
"
#############################################################################################################################################################################################
#LOCALPASS=8Rm%4S4A7F
#############################################################################################################################################################################################
#delete backups
#sshpass -p $LOCALPASS ssh devops@122.186.73.186  docker exec -it romantic_antonelli /bin/rm -rf /var/opt/mssql/data/autobackups
ssh -i /home/ubuntu/auto/dev-server.pem devops@122.186.73.186  docker exec --tty romantic_antonelli /bin/rm -rf /var/opt/mssql/data/autobackups

#############################################################################################################################################################################################



#REPO SETUP
sum=0
declare -a my_array
declare -a domain_array

if [[ "$3" == "web" ]]; then
     my_array+="\"${new_web_repo}\""
     domain_array+="\"${PROJECT_NAME}dev\""
     sum=$((sum + 1))
     cp -r conf/sampledev.nestorhawk.com.conf $PROJECT_NAME
fi
if [[ "$4" == "customer" ]]; then
     my_array+=", \"${new_customer_repo}\""
     domain_array+=", \"${PROJECT_NAME}customerapi\""
     sum=$((sum + 1))
     cp kestrel-sampledevcustomerapi.service $PROJECT_NAME
     cp -r conf/samplecustomerapidev.nestorhawk.com.conf $PROJECT_NAME
fi
if [[ "$5" == "agent" ]]; then
     my_array+=", \"${new_agent_repo}\""
     domain_array+=", \"${PROJECT_NAME}agentapi\""
     sum=$((sum + 1 ))
     cp kestrel-sampledevagentapi.service $PROJECT_NAME
     cp -r conf/sampleagentapidev.nestorhawk.com.conf $PROJECT_NAME
fi

mkdir $PROJECT_NAME/cloudflare
mkdir $PROJECT_NAME/repocreate

sed -e "s/value/$my_array/g" -e "s/num/$sum/g" test.tf > $PROJECT_NAME/repocreate/${PROJECT_NAME}.tf
sed -e "s/sub_domain/$domain_array/g" -e "s/num/$sum/g" cloudflare.tf > $PROJECT_NAME/cloudflare/${PROJECT_NAME}.tf

cd /home/ubuntu/auto/$PROJECT_NAME/repocreate/

/var/lib/jenkins/bin/terraform init
/var/lib/jenkins/bin/terraform apply --auto-approve


cd /home/ubuntu/auto/$PROJECT_NAME/cloudflare

/var/lib/jenkins/bin/terraform init
/var/lib/jenkins/bin/terraform apply --auto-approve


cd /home/ubuntu/auto/$PROJECT_NAME/

###########################################################################################################################################################################################

if [[ "$3" == "web" ]]; then

   #git clone and create repo:
   git clone https://$USERNAME:$USERPASS@gitlab.com/nhkteam/${web_repo}.git
   git clone https://$USERNAME:$USERPASS@gitlab.com/habib8874/${new_web_repo}.git

   rm -rf /home/ubuntu/auto/${PROJECT_NAME}/${web_repo}/.git

   WEB_PORT=4000
   VALUE="false"

   while [[ "$VALUE" == "false" ]]
   do
     if [[ $(ssh -i /home/ubuntu/auto/dev-server.pem devops@122.186.73.186 grep -wir ${WEB_PORT} /etc/apache2/sites-available) ]]; then
         WEB_PORT=$((WEB_PORT + 1))
         VALUE="false"
     else
         echo $WEB_PORT
         VALUE="true"
     fi
   done

   echo "print value `pwd`"

   sed -e "s/WEB_PORT/$WEB_PORT/g" -e "s/PROJECT_NAME/$PROJECT_NAME/g" /home/ubuntu/auto/conf/sampledev.nestorhawk.com.conf > /home/ubuntu/auto/${PROJECT_NAME}/${PROJECT_NAME}dev.nestorhawk.com.conf
   
  # sshpass -p $LOCALPASS rsync --progress -avz ${PROJECT_NAME}dev.nestorhawk.com.conf devops@122.186.73.186:/etc/apache2/sites-available/
   scp -i /home/ubuntu/auto/dev-server.pem /home/ubuntu/auto/${PROJECT_NAME}/${PROJECT_NAME}dev.nestorhawk.com.conf devops@122.186.73.186:/etc/apache2/sites-available/
   ssh -i /home/ubuntu/auto/dev-server.pem devops@122.186.73.186 "cd /etc/apache2/sites-available/ && sudo chmod 777 ${PROJECT_NAME}dev.nestorhawk.com.conf"
   mv ${web_folder}/* ${new_web_repo}
   cd ${new_web_repo}

   #update connection string
   sed -e "s/DBUSER/$DBUSER/g" -e "s/DBPASS/$DBPASS/g" -e "s/DBNAME/$newdb/g" /home/ubuntu/auto/dataSettings.json > dataSettings.json

   git config --global user.email "habibur@nestorbird.com"
   git config --global user.name "habib8874"
   git add --all
   git commit -am "initial commit"
   git branch -m deployment-development
   git push origin -u deployment-development

   GIT_CLONE_WEB_URL="https://gitlab.com/habib8874/${new_web_repo}.git"
   echo $GIT_CLONE_WEB_URL


   cp /home/ubuntu/auto/webconf.xml  /home/ubuntu/auto/${PROJECT_NAME}/${PROJECT_NAME}webconf.xml

   cd /home/ubuntu/auto/${PROJECT_NAME}/

   sed -i "s/WEB_PORT/$WEB_PORT/g" ${PROJECT_NAME}webconf.xml

   sed -i "s|GIT_CLONE_WEB_URL|$GIT_CLONE_WEB_URL|g" ${PROJECT_NAME}webconf.xml
   echo ${PROJECT_NAME}webconf.xml
   curl -s -XPOST 'http://jenkinsdev.nestorhawk.com/createItem?name='${PROJECT_NAME}'-webdev' -u habib:11dd75c14de77a1603f51f9e50bf09ee59 --data-binary @${PROJECT_NAME}webconf.xml -H "Content-Type:text/xml"

#uncomment below line to run the pipeline
   #curl -X POST http://ajit:1143e5e90f13206ea18be9558081795273@jenkinsdev.nestorhawk.com/job/${PROJECT_NAME}-webdev/build/

fi

cd /home/ubuntu/auto/$PROJECT_NAME/


if [[ "$4" == "customer" ]]; then

   #git clone and create repo:
   git clone https://$USERNAME:$USERPASS@gitlab.com/nhkteam/${customer_repo}.git
   git clone https://$USERNAME:$USERPASS@gitlab.com/habib8874/${new_customer_repo}.git

   rm -rf ${customer_repo}/.git

   #port assigning
   CUSTOMER_PORT=5000
   VALUE="false"

   while [[ "$VALUE" == "false" ]]
   do
     if [[ $(ssh -i /home/ubuntu/auto/dev-server.pem devops@122.186.73.186 grep -wir ${CUSTOMER_PORT} /etc/apache2/sites-available) ]]; then
         CUSTOMER_PORT=$((CUSTOMER_PORT + 1))
         VALUE="false"
     else
         echo $CUSTOMER_PORT
         VALUE="true"
     fi
   done

   sed -e "s/PROJECT_NAME/$PROJECT_NAME/g" -e "s/CUSTOMER_PORT/$CUSTOMER_PORT/g" conf/samplecustomerapidev.nestorhawk.com.conf > ${PROJECT_NAME}customerapidev.nestorhawk.com.conf
   sed "s/PROJECT_NAME/$PROJECT_NAME/g" kestrel-sampledevcustomerapi.service > kestrel-${PROJECT_NAME}customerdevapi.service

   #send conf to local server
   ssh -i /home/ubuntu/auto/dev-server.pem rsync --progress -avz ${PROJECT_NAME}customerapidev.nestorhawk.com.conf devops@122.186.73.186:/etc/apache2/sites-available/
   ssh -i /home/ubuntu/auto/dev-server.pem rsync --progress -avz kestrel-${PROJECT_NAME}customerdevapi.service devops@122.186.73.186:/etc/systemd/system/
   ssh -i /home/ubuntu/auto/dev-server.pem devops@122.186.73.186 mkdir /var/www/html/${PROJECT_NAME}devcustomerapi.nestorhawk.com
   ssh -i /home/ubuntu/auto/dev-server.pem devops@122.186.73.186 chmod 777 /var/www/html/${PROJECT_NAME}devcustomerapi.nestorhawk.com
   #sshpass -p $LOCALPASS ssh devops@122.186.73.186 chmod 777 /var/www/html/${PROJECT_NAME}devcustomerapi.nestorhawk.com


   mv ${customer_folder}/* ${new_customer_repo}
   cd ${new_customer_repo}
   #update connection string

   sed -i "s/localhost:[0-9][0-9][0-9][0-9]/localhost:$CUSTOMER_PORT/g" NHKCustomerApplication/Program.cs
   #left with updating connection striing
   sed -e "s/DBUSER/$DBUSER/g" -e "s/DBPASS/$DBPASS/g" -e "s/DBNAME/$newdb/g" /home/ubuntu/auto/appsettings.json > NHKCustomerApplication/appsettings.json


   git config --global user.email "habibur@nestorbird.com"
   git config --global user.name "habib8874"
   git add --all
   git commit -am "initial commit"
   git branch -m deployment-development
   git push origin -u deployment-development
   echo $CUSTOMER_PORT >> /etc/apache2/sites-available/port

   GIT_CLONE_CUSTOMER_URL="https://gitlab.com/habib8874/${new_customer_repo}.git"
   echo $GIT_CLONE_CUSTOMER_URL


   cp /home/ubuntu/auto/customerapi.xml  /home/ubuntu/auto/${PROJECT_NAME}/${PROJECT_NAME}customerapi.xml

   cd /home/ubuntu/auto/${PROJECT_NAME}/

   sed -i "s/PROJECT_NAME/$PROJECT_NAME/g" ${PROJECT_NAME}customerapi.xml

   sed -i "s|GIT_CLONE_CUSTOMER_URL|$GIT_CLONE_CUSTOMER_URL|g" ${PROJECT_NAME}customerapi.xml
   echo ${PROJECT_NAME}customerapi.xml

   curl -s -XPOST 'http://jenkinsdev.nestorhawk.com/createItem?name='${PROJECT_NAME}'-customerdev' -u habib:11dd75c14de77a1603f51f9e50bf09ee59 --data-binary @${PROJECT_NAME}customerapi.xml -H "Content-Type:text/xml"
   #curl -X POST http://ajit:1143e5e90f13206ea18be9558081795273@jenkinsdev.nestorhawk.com/job/${PROJECT_NAME}-customerdev/build/
fi

cd /home/ubuntu/auto/${PROJECT_NAME}

if [[ "$5" == "agent" ]]; then

   #git clone and create repo:
   git clone https://$USERNAME:$USERPASS@gitlab.com/nhkteam/${agent_repo}.git
   git clone https://$USERNAME:$USERPASS@gitlab.com/habib8874/${new_agent_repo}.git

   rm -rf ${agent_repo}/.git
   new_agent_repo_small=`echo $new_agent_repo | awk '{print tolower($0)}'`
   agent_repo_small=`echo $agent_repo | awk '{print tolower($0)}'`
   pwd

   #port assigning
   AGENT_PORT=6000
   VALUE="false"

   while [[ "$VALUE" == "false" ]]
   do
     if [[ $(ssh -i /home/ubuntu/auto/dev-server.pem devops@122.186.73.186 grep -wir ${AGENT_PORT} /etc/apache2/sites-available) ]]; then
         AGENT_PORT=$((AGENT_PORT + 1))
         VALUE="false"
     else
         echo $AGENT_PORT
         VALUE="true"
     fi
   done

   sed -e "s/PROJECT_NAME/$PROJECT_NAME/g" -e "s/AGENT_PORT/$AGENT_PORT/g" conf/sampleagentapidev.nestorhawk.com.conf > ${PROJECT_NAME}agentapidev.nestorhawk.com.conf
   sed "s/PROJECT_NAME/$PROJECT_NAME/g" kestrel-sampledevagentapi.service > kestrel-${PROJECT_NAME}agentdevapi.service

   ssh -i /home/ubuntu/auto/dev-server.pem rsync --progress -avz ${PROJECT_NAME}agentapidev.nestorhawk.com.conf devops@122.186.73.186:/etc/apache2/sites-available/
   #sshpass -p $LOCALPASS rsync --progress -avz ${PROJECT_NAME}agentapidev.nestorhawk.com.conf devops@122.186.73.186:/etc/apache2/sites-available/
   ssh -i /home/ubuntu/auto/dev-server.pem rsync --progress -avz kestrel-${PROJECT_NAME}agentdevapi.service devops@122.186.73.186:/etc/systemd/system/
   ssh -i /home/ubuntu/auto/dev-server.pem devops@122.186.73.186 mkdir /var/www/html/${PROJECT_NAME}devagentapi.nestorhawk.com
   #sshpass -p ${LOCALPASS} ssh devops@122.186.73.186 mkdir /var/www/html/${PROJECT_NAME}devagentapi.nestorhawk.com
   ssh -i /home/ubuntu/auto/dev-server.pem devops@122.186.73.186 chmod 777 /var/www/html/${PROJECT_NAME}devagentapi.nestorhawk.com



   mv ${agent_folder}/* ${new_agent_repo}
   cd ${new_agent_repo}

   #update connection string
   #sed -i -e "s/DBUSER/$DBUSER/g" -e "s/DBPASS/$DBPASS/g" NHKAgentApplication/appsettings.json


   #rsync --progress -avz -e "ssh -i  key.pem" ${PROJECT_NAME}agentapidev.nestorhawk.com.conf root@165.232.182.1:/etc/httpd/conf.d
   sed -i "s/localhost:[0-9][0-9][0-9][0-9]/localhost:$AGENT_PORT/g" NHKAgentApplication/Program.cs

   #left with updating connection striing
   sed -e "s/DBUSER/$DBUSER/g" -e "s/DBPASS/$DBPASS/g" -e "s/DBNAME/$newdb/g" /home/ubuntu/auto/appsettings.json > NHKAgentApplication/appsettings.json

   git config --global user.email "habibur@nestorbird.com"
   git config --global user.name "habib8874"
   git add --all
   git commit -am "initial commit"
   git branch -m deployment-development
   git push origin -u deployment-development

      echo $AGENT_PORT >> /etc/apache2/sites-available/port

   GIT_CLONE_AGENT_URL="https://gitlab.com/habib8874/${new_agent_repo}.git"
   echo $GIT_CLONE_AGENT_URL


   cp /home/ubuntu/auto/agentapi.xml  /home/ubuntu/auto/${PROJECT_NAME}/${PROJECT_NAME}agentapi.xml

   cd /home/ubuntu/auto/${PROJECT_NAME}/

   sed -i "s/PROJECT_NAME/$PROJECT_NAME/g" ${PROJECT_NAME}agentapi.xml

   sed -i "s|GIT_CLONE_CUSTOMER_URL|$GIT_CLONE_AGENT_URL|g" ${PROJECT_NAME}agentapi.xml
   echo ${PROJECT_NAME}agentapi.xml

   curl -s -XPOST 'http://jenkinsdev.nestorhawk.com/createItem?name='${PROJECT_NAME}'-agentdev' -u habib:11dd75c14de77a1603f51f9e50bf09ee59 --data-binary @${PROJECT_NAME}agentapi.xml -H "Content-Type:text/xml"
   #curl -X POST http://ajit:1143e5e90f13206ea18be9558081795273@jenkinsdev.nestorhawk.com/job/${PROJECT_NAME}-agentdev/build/


fi


