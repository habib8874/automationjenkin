﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.NB.Package;
using Nop.Web.Areas.Admin.Models.NB.AccountsBilling;
using Nop.Web.Framework.Models.Extensions;
using Stripe;

namespace Nop.Web.Areas.Admin.Factories.NB
{
    public partial class NBAccountBillingModelFactory : INBAccountBillingModelFactory
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly ILogger _logger;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly ICountryService _countryService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly IWorkContext _workContext;
        private readonly INBCustomerPackageService _nBCustomerPackageService;
        private readonly INBPackageService _nBPackageService;
        private readonly IPriceFormatter _priceFormatter;

        #endregion

        #region Ctor

        public NBAccountBillingModelFactory(
            ILocalizationService localizationService,
            IDateTimeHelper dateTimeHelper,
            ISettingService settingService,
            IStoreContext storeContext,
            ILogger logger,
            IStateProvinceService stateProvinceService,
            ICountryService countryService,
            IBaseAdminModelFactory baseAdminModelFactory,
            IWorkContext workContext,
            INBCustomerPackageService nBCustomerPackageService,
            INBPackageService nBPackageService,
            IPriceFormatter priceFormatter)
        {
            _localizationService = localizationService;
            _dateTimeHelper = dateTimeHelper;
            _settingService = settingService;
            _storeContext = storeContext;
            _logger = logger;
            _stateProvinceService = stateProvinceService;
            _countryService = countryService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _workContext = workContext;
            _nBCustomerPackageService = nBCustomerPackageService;
            _nBPackageService = nBPackageService;
            _priceFormatter = priceFormatter;
        }

        #endregion

        #region Methods

        public virtual AccountTransactionListModel PrepareAccountTransactionListModel(AccountTransactionSearchModel searchModel)
        {
            var accountTransactionListModel = new AccountTransactionListModel();

            string key = _settingService.GetSettingByKey<string>("setting.storesetup.accountbillingstripeapikey", string.Empty, _storeContext.CurrentStore.Id);
            if (string.IsNullOrEmpty(key))
            {
                return accountTransactionListModel;
            }

            try
            {
                StripeConfiguration.ApiKey = key;
                var options = new BalanceTransactionListOptions
                {
                    Limit = 100,
                };
                var transactionService = new BalanceTransactionService();
                var balanceTransactions = transactionService.List(
                  options
                );

                if (balanceTransactions.StripeResponse.StatusCode.ToString() == "OK")
                {
                    var transactionResponse = JsonConvert.DeserializeObject<AccountTransactionResponseModel>(balanceTransactions.StripeResponse.Content).Data;
                    var accountList = new List<AccountTransactionModel>();

                    var pageList = transactionResponse.Select(transaction =>
                    {
                        var transactionModel = new AccountTransactionModel();
                        var dtOffset = DateTimeOffset.FromUnixTimeSeconds(transaction.Created);
                        transactionModel.Amount = _priceFormatter.FormatPrice(transaction.Amount, true, false);
                        transactionModel.Description = transaction.Description;
                        transactionModel.Date =
                            _dateTimeHelper.ConvertToUtcTime(dtOffset.DateTime);

                        return transactionModel;
                    }).ToList().ToPagedList(searchModel);

                    //prepare list model
                    var model = new AccountTransactionListModel().PrepareToGrid(searchModel, pageList, () => pageList);

                    return model;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Fetching the transactions getting error :", ex);
            }
            return accountTransactionListModel;
        }


        /// <summary>
        /// Prepare customer model
        /// </summary>
        /// <param name="model">Customer model</param>
        /// <param name="customer">Customer</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Customer model</returns>
        public virtual AccountBillingModel PrepareAccountBillingModel(StripeAccountDetailResponseModel stripeAccountDetailModel)
        {
            var model = new AccountBillingModel();

            if (string.IsNullOrEmpty(stripeAccountDetailModel.Id) == false)
            {
                var companyAddress = stripeAccountDetailModel.Company?.Address;
                var countryId = _countryService.GetCountryByTwoLetterIsoCode(stripeAccountDetailModel.Country)?.Id ?? 0;
                var stateId = _stateProvinceService.GetStateProvinceByAbbreviation(companyAddress.State?.ToString())?.Id ?? 0;

                model.FirstName = stripeAccountDetailModel.BusinessProfile?.Name;
                model.LastName = stripeAccountDetailModel.BusinessProfile?.Name;
                model.EmailAddress = stripeAccountDetailModel.Email;
                model.PhoneNumber = stripeAccountDetailModel.Company?.Phone;

                model.AddressLine1 = companyAddress?.Line1;
                model.AddressLine2 = companyAddress?.Line2;
                model.City = companyAddress?.City;
                model.CountryId = countryId;
                model.StateProvinceId = stateId;
                model.ZipCode = companyAddress.PostalCode.ToString();
            }

            //prepare page parameters
            model.AccountTransactionSearchModel.SetGridPageSize();
            model.CustomerId = _workContext.CurrentCustomer.Id;

            _baseAdminModelFactory.PrepareCountries(model.AvailableCountries);
            _baseAdminModelFactory.PrepareStatesAndProvinces(model.AvailableStates, model.CountryId == 0 ? null : (int?)model.CountryId);

            model.AccountSubscriptionDetailModel = PrepareSubscriptionDetailModel();
            return model;
        }


        public virtual AccountPackagePaymentUpdateModel PrepareAccountPackagePaymentUpdateModel(int customerId)
        {
            var model = new AccountPackagePaymentUpdateModel();
            var customerPackage = _nBCustomerPackageService.GetPackageByCustomerId(customerId);
            if (customerPackage == null)
                return model;

            model.CustomerId = customerId;
            //prepare available Packages
            var availablePackages = _nBPackageService.GetAllPackages(_storeContext.CurrentStore.Id, string.Empty).Where(x => x.Deleted == false);
            foreach (var packages in availablePackages)
            {
                model.AvailablePackages.Add(new SelectListItem { Value = packages.Id.ToString(), Text = packages.Name });
            }
            //insert special item for the default value
            PrepareDefaultItem(model.AvailablePackages, true, _localizationService.GetResource("NB.Account.Fields.SelectPackage"));

            //prepare available states and provinces of the country
            var availablePlanPackages = _nBPackageService.GetPackagePlans(packageId: customerPackage.PackageId).ToList();
            foreach (var planPackages in availablePlanPackages)
            {
                model.AvailablePackagesPlan.Add(new SelectListItem { Value = planPackages.Id.ToString(), Text = planPackages.Name });
            }

            //insert special item for the default value
            if (model.AvailablePackagesPlan.Any())
                PrepareDefaultItem(model.AvailablePackagesPlan, true, _localizationService.GetResource("NB.Account.Fields.SelectPackagePlan"));
          
            return model;
        }

        public virtual bool UpdateCustomerPackagePlan(AccountPackagePaymentUpdateModel model)
        {
            var isUpdate = false;
            var customerPackage = _nBCustomerPackageService.GetPackageByCustomerId(_workContext.CurrentCustomer.Id);
            if (customerPackage == null)
                return isUpdate;

            customerPackage.PackageId = model.PackageId;
            customerPackage.PackagePlanId = model.PackagePlanId;
            _nBCustomerPackageService.UpdateCustomerPackageMapping(customerPackage);
            isUpdate = true;

            return isUpdate;
        }


        public virtual AccountPackagePaymentUpdateModel PreparePackagePaymentInfoModel(int packageId, int packagePlanId,int customerId)
        {
            var model = new AccountPackagePaymentUpdateModel();

            var customerPackage = _nBCustomerPackageService.GetPackageByCustomerId(customerId);
            if(customerPackage!=null)
            {
                var packagePlan = _nBPackageService.GetPackagePlanById(packagePlanId);
                model.PackageName = _nBPackageService.GetPackageById(packageId)?.Name;
                if (packagePlan != null)
                {
                    int.TryParse(packagePlan.TrailPeriod, out int trailPeriod);

                    var amountToBePaid = packagePlan.Price - customerPackage.PackageAmount;
                    model.PlanName = packagePlan?.Name;
                    model.PlanPrice = _priceFormatter.FormatPrice(packagePlan.Price, true, false);
                    model.Validity = DateTime.UtcNow.AddDays(trailPeriod);

                    model.AmountToBePaidPrice = amountToBePaid;
                    model.AmountToBePaid = _priceFormatter.FormatPrice(amountToBePaid, true, false);

                    //model.ExistingCardDetail = "xxxx-xxxx-xxxx-" + customerPackage.CardNumber.Substring(customerPackage.CardNumber.Length - 4, 4);

                    //model.CardholderName = "cardholdername";
                    //model.CardCode = "123";
                }
            }
          

            model.CustomerId = customerId;

            model.CreditCardTypes = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Visa", Value = "visa" },
                    new SelectListItem { Text = "Master card", Value = "MasterCard" },
                    new SelectListItem { Text = "Discover", Value = "Discover" },
                    new SelectListItem { Text = "Amex", Value = "Amex" },
                };
            //years
            for (var i = 0; i < 15; i++)
            {
                var year = (DateTime.Now.Year + i).ToString();
                model.ExpireYears.Add(new SelectListItem { Text = year, Value = year, });
            }

            //months
            for (var i = 1; i <= 12; i++)
            {
                model.ExpireMonths.Add(new SelectListItem { Text = i.ToString("D2"), Value = i.ToString(), });
            }

            return model;
        }
        #endregion

        #region Utilities
        public virtual AccountSubscriptionDetailModel PrepareSubscriptionDetailModel()
        {
            var model = new AccountSubscriptionDetailModel();
            var customerPackage = _nBCustomerPackageService.GetPackageByCustomerId(_workContext.CurrentCustomer.Id);
            if (customerPackage != null)
            {
                var package = _nBPackageService.GetPackageById(customerPackage.PackageId);
                if (package == null)
                    return model;

                var packagePlan = _nBPackageService.GetPackagePlanById(customerPackage.PackagePlanId);
               
                int.TryParse(packagePlan.TrailPeriod, out int trailPeriod);

                model.PackageName = package.Name;
                model.PackagePlanName = packagePlan.Name;
                model.Validaty = DateTime.UtcNow.AddDays(trailPeriod);
                model.NextDueDate = DateTime.UtcNow.AddDays(trailPeriod);
                model.Amount = packagePlan.Price;
                model.PlanPrice = _priceFormatter.FormatPrice(packagePlan.Price, true, false);
            }
            return model;
        }

        /// <summary>
        /// Prepare default item
        /// </summary>
        /// <param name="items">Available items</param>
        /// <param name="withSpecialDefaultItem">Whether to insert the first special item for the default value</param>
        /// <param name="defaultItemText">Default item text; pass null to use "All" text</param>
        protected virtual void PrepareDefaultItem(IList<SelectListItem> items, bool withSpecialDefaultItem, string defaultItemText = null)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));

            //whether to insert the first special item for the default value
            if (!withSpecialDefaultItem)
                return;

            //at now we use "0" as the default value
            const string value = "0";

            //prepare item text
            defaultItemText = defaultItemText ?? _localizationService.GetResource("Admin.Common.All");

            //insert this default item at first
            items.Insert(0, new SelectListItem { Text = defaultItemText, Value = value });
        }

        #endregion
    }
}
