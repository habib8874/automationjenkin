﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.BookingTable
{
    public partial class BookingTableTransaction:BaseEntity
    {
        public int StoreId { get; set; }
        public int VendorId { get; set; }
        public DateTime BookingDate { get; set; }
        public TimeSpan BookingTimeFrom { get; set; }
        public TimeSpan BookingTimeTo { get; set; }
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public int TableNumber { get; set; }
        public DateTime CreatedDt { get; set; }
        public int Status { get; set; }
        public string SeatingCapacity { get; set; }
        public string SpecialNotes { get; set; }
    }
}
