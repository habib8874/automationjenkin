﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Directory;
using Nop.Services.Events;
using Nop.Services.Localization;

namespace Nop.Services.Directory
{
    /// <summary>
    /// State City
    /// </summary>
    public partial class CityService : ICityService
    {
        #region Fields

        private readonly ICacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILocalizationService _localizationService;
        private readonly IRepository<NHK_City> _cityRepository;
        private readonly IRepository<StoreCountryStateMapping> _storeCountryStateMappingRepository;

        #endregion

        #region Ctor

        public CityService(ICacheManager cacheManager,
            IEventPublisher eventPublisher,
            ILocalizationService localizationService,
            IRepository<NHK_City> cityRepository,
            IRepository<StoreCountryStateMapping> storeCountryStateMappingRepository)
        {
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _localizationService = localizationService;
            _cityRepository = cityRepository;
            _storeCountryStateMappingRepository = storeCountryStateMappingRepository;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Deletes a City
        /// </summary>
        /// <param name="City">The City</param>
        public virtual void DeleteCity (NHK_City city)
        {
            if (city == null)
                throw new ArgumentNullException(nameof(city));

            _cityRepository.Delete(city);
            //event notification
            _eventPublisher.EntityDeleted(city);
        }

        /// <summary>
        /// Gets a City
        /// </summary>
        /// <param name="CityId">The City identifier</param>
        /// <returns>City</returns>
        public virtual NHK_City GetCityById(int cityId)
        {
            if (cityId == 0)
                return null;

            return _cityRepository.GetById(cityId);
        }

        /// <summary>
        /// Gets a City by abbreviation
        /// </summary>
        /// <param name="code">The City code</param>
        /// <param name="countryId">Country identifier; pass null to load the state regardless of a country</param>
        /// <returns>City</returns>
        public virtual NHK_City GetCityByCode(string code, int? countryId = null)
        {
            if (string.IsNullOrEmpty(code))
                return null;

            var key = string.Format(NopDirectoryDefaults.StateProvincesByAbbreviationCacheKey, code, countryId.HasValue ? countryId.Value : 0);
            return _cacheManager.Get(key, () =>
            {
                var query = _cityRepository.Table.Where(state => state.Code == code);

                //filter by country
                if (countryId.HasValue)
                    query = query.Where(state => state.StateId == countryId);

                return query.FirstOrDefault();
            });
        }

        /// <summary>
        /// Gets a City collection by country identifier
        /// </summary>
        /// <param name="countryId">Country identifier</param>
        /// <param name="languageId">Language identifier. It's used to sort states by localized names (if specified); pass 0 to skip it</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>States</returns>
      public virtual IList<NHK_City> GetCityByStateId(int stateId, int languageId = 0, bool showHidden = false)
        {
           
                var query = from sp in _cityRepository.Table
                            orderby sp.DisplayOrder, sp.Name
                            where sp.StateId == stateId &&
                            (showHidden || sp.Published)
                            select sp;
                var cities  = query.ToList();

                if (languageId > 0)
                {
                    //we should sort states by localized names when they have the same display order
                    cities = cities
                        .OrderBy(c => c.DisplayOrder)
                        .ThenBy(c => _localizationService.GetLocalized(c, x => x.Name, languageId))
                        .ToList();
                }

                return cities;
           
        }

        /// <summary>
        /// Gets all City
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>States</returns>
        public virtual IList<NHK_City> GetCity (bool showHidden = false)
        {
            var query = from sp in _cityRepository.Table
                        orderby sp.StateId, sp.DisplayOrder, sp.Name
                        where showHidden || sp.Published
                        select sp;
            var stateProvinces = query.ToList();
            return stateProvinces;
        }

        /// <summary>
        /// Inserts a City
        /// </summary>
        /// <param name="city">City</param>
        public virtual void InsertCity(NHK_City city)
        {
            if (city == null)
                throw new ArgumentNullException(nameof(city));

            _cityRepository.Insert(city);
            //event notification
            _eventPublisher.EntityInserted(city);
        }

        /// <summary>
        /// Updates a City
        /// </summary>
        /// <param name="City">City</param>
        public virtual void UpdateCity (NHK_City city)
        {
            if (city == null)
                throw new ArgumentNullException(nameof(city));

            _cityRepository.Update(city);
            //event notification
            _eventPublisher.EntityUpdated(city);
        }

        #endregion

        #region StoreCountryStateMapping
        /// <summary>
        /// Inserts a StoreCountryStateMapping
        /// </summary>
        /// <param name="storeCountryStateMapping">State/province</param>
        public virtual void InsertStoreCountryStateMapping(StoreCountryStateMapping storeCountryStateMapping)
        {
            if (storeCountryStateMapping == null)
                throw new ArgumentNullException(nameof(storeCountryStateMapping));

            _storeCountryStateMappingRepository.Insert(storeCountryStateMapping);

            //event notification
            _eventPublisher.EntityInserted(storeCountryStateMapping);
        }

        /// <summary>
        /// Updates a StoreCountryStateMapping
        /// </summary>
        /// <param name="storeCountryStateMapping">State/province</param>
        public virtual void UpdateStoreCountryStateMapping(StoreCountryStateMapping storeCountryStateMapping)
        {
            if (storeCountryStateMapping == null)
                throw new ArgumentNullException(nameof(storeCountryStateMapping));

            _storeCountryStateMappingRepository.Update(storeCountryStateMapping);

            //event notification
            _eventPublisher.EntityUpdated(storeCountryStateMapping);
        }

        /// <summary>
        /// Gets a StoreCountryStateMapping collection by country identifier
        /// </summary>
        /// <param name="countryId">Country identifier</param>
        /// <param name="storeId">store identifier</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>States</returns>
        public virtual IList<StoreCountryStateMapping> GetStoreCountryStateMappingsByCountryId(int countryId, int storeId = 0, bool showHidden = false)
        {
            var query = from sp in _storeCountryStateMappingRepository.Table
                        orderby sp.StateName
                        where sp.CountryId == countryId &&
                        (storeId == 0 || sp.StoreId == storeId) &&
                        (showHidden || sp.IsActive.Value)
                        select sp;
            var storeCountryStateMappings = query.ToList();

            return storeCountryStateMappings;
        }

        /// <summary>
        /// Gets a StoreCountryStateMapping
        /// </summary>
        /// <param name="storeCountryStateMappingId">The StoreCountryStateMapping identifier</param>
        /// <returns>State/province</returns>
        public virtual StoreCountryStateMapping GetStoreCountryStateMappingById(int storeCountryStateMappingId)
        {
            if (storeCountryStateMappingId == 0)
                return null;

            return _storeCountryStateMappingRepository.GetById(storeCountryStateMappingId);
        }
        #endregion
    }
}