﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.AccountsBilling
{
    /// <summary>
    /// Represents a Account Payout Method Model 
    /// </summary>
    public partial class AccountPayoutMethodModel : BaseNopEntityModel
    {

        #region Properties

        [NopResourceDisplayName("NB.Admin.AccountBillings.AccountPayoutMethodModel.Field.AccountNumber")]
        public string AccountNumber { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.AccountPayoutMethodModel.Field.RoutingNumber")]
        public string RoutingNumber { get; set; }

        #endregion
    }
}
