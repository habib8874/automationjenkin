﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.NB.Tip;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.NB.TipMaster
{
    /// <summary>
    /// question property service
    /// </summary>
    public partial class TipMasterService : ITipMasterService
    {

        #region Fields

        private readonly IRepository<Tip> _tipRepository;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="tipRepository">tip repository</param>
        /// <param name="workContext">Work context</param>
        /// <param name="storeContext">Store context</param>
        /// <param name="eventPublisher">Event publisher</param>
        public TipMasterService(ICacheManager cacheManager,
            IRepository<Tip> tipRepository,
            IWorkContext workContext,
            IStoreContext storeContext,
            IEventPublisher eventPublisher
)
        {
            _cacheManager = cacheManager;
            _tipRepository = tipRepository;
            _workContext = workContext;
            _storeContext = storeContext;
            _eventPublisher = eventPublisher;
        }

        #endregion

        #region TipMaster

        /// <summary>
        /// Gets a Tip  
        /// </summary>
        /// <param name="tipId">Tip  identifier</param>
        /// <returns>Tip </returns>
        public virtual Tip GetTipById(int tipId)
        {
            if (tipId == 0)
                return null;

            return _tipRepository.GetById(tipId);
        }

        /// <summary>
        /// Gets a Tips 
        /// </summary>
        /// <returns>Tips </returns>
        public virtual IList<Tip> GetAllTips(int storeId = 0)
        {
            var query = from eq in _tipRepository.Table
                        where !eq.Deleted
                        select eq;

            if (storeId > 0)
            {
                query = query.Where(x => x.StoreId == storeId);
            }

            return query.OrderBy(x => x.DisplayOrder).ToList();
        }

        /// <summary>
        /// Inserts a Tip
        /// </summary>
        /// <param name="Tip">Tip </param>
        public virtual void InsertTip(Tip tip)
        {
            if (tip == null)
                throw new ArgumentNullException("tip");

            _tipRepository.Insert(tip);

            //event notification
            _eventPublisher.EntityInserted(tip);
        }

        /// <summary>
        /// Updates the Tip 
        /// </summary>
        /// <param name="Tip">Tip </param>
        public virtual void UpdateTip(Tip tip)
        {
            if (tip == null)
                throw new ArgumentNullException("tip");

            _tipRepository.Update(tip);

            //event notification
            _eventPublisher.EntityUpdated(tip);
        }

        /// <summary>
        /// Deletes a Tip
        /// </summary>
        /// <param name="Tip">Tip</param>
        public virtual void DeleteTip(Tip tip)
        {
            if (tip == null)
                throw new ArgumentNullException("tip");

            _tipRepository.Delete(tip);

            //event notification
            _eventPublisher.EntityDeleted(tip);
        }

        /// <summary>
        /// Gets a Tips  
        /// </summary>
        /// <param name="TipsIds">Tips  identifier</param>
        /// <returns>Questions list</returns>
        public virtual IList<Tip> GetTipsByIds(int[] tipsIds)
        {
            if (tipsIds == null || tipsIds.Length == 0)
                return new List<Tip>();

            var query = from p in _tipRepository.Table
                        where tipsIds.Contains(p.Id) && !p.Deleted
                        select p;
            var tips = query.ToList();
            //sort by passed identifiers
            var sortedTips = new List<Tip>();
            foreach (var id in tipsIds)
            {
                var tip = tips.Find(x => x.Id == id);
                if (tip != null)
                    sortedTips.Add(tip);
            }

            return sortedTips;
        }
        #endregion
    }
}
