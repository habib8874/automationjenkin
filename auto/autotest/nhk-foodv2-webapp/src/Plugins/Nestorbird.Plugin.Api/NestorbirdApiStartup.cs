﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nop.Core.Infrastructure;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Nestorbird.Plugin.Api
{
    public class SwaggerCustomHeaderFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (context.ApiDescription.RelativePath == "Admin/Atluz/Razorpay/License")
                return;

            if (operation.Parameters == null)
            {
                operation.Parameters = new List<IParameter>();
            }

            operation.Parameters.Add(new NonBodyParameter
            {
                Name = "LanguageId",
                In = "header",
                Type = "string",
                Required = true,
                Default = 1
            });
        }
    }

    public class NestorbirdApiStartup : INopStartup
    {
        public int Order => 1;

        public void Configure(IApplicationBuilder application)
        {
            application.UseSwagger();
            application.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Nestorbird API");
            });
        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddMvc(c => c.Conventions.Add(new ApiExplorerIgnores()));
            services.AddSwaggerGen(c =>
            {                
                c.SwaggerDoc("v1", new Info { Title = "Nestorbird API", Version = "v1" });
                c.OperationFilter<SwaggerCustomHeaderFilter>();
                c.ResolveConflictingActions(apiDescription => apiDescription.Last());
                c.CustomSchemaIds(x => x.FullName);
            });
        }
    }

    public class ApiExplorerIgnores : IActionModelConvention
    {
        public void Apply(ActionModel action)
        {
            // We have to ignore atluzlicence plugins APIs
            if (action.Controller.ControllerName.Equals("AtluzLicense"))
                action.ApiExplorer.IsVisible = false;
        }
    }
}
