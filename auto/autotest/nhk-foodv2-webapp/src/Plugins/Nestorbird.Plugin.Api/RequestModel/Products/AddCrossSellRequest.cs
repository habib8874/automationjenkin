﻿using System.Collections.Generic;

namespace Nestorbird.Plugin.Api.RequestModel.Products
{
    public class AddCrossSellRequest
    {
        public AddCrossSellRequest()
        {
            CrossSellProductIds = new List<int>();
        }

        public int ProductId { get; set; }

        public IList<int> CrossSellProductIds { get; set; }
    }
}
