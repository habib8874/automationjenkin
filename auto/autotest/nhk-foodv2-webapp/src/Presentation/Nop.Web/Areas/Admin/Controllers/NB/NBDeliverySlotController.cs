﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.NB;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.NB;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.NB;
using Nop.Web.Areas.Admin.Models.Vendors;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.Extensions;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers.NB
{
    public partial class NBDeliverySlotController : BaseAdminController
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IWorkContext _workContext;
        private readonly IDeliverySlotService _deliverySlotService;
        private readonly IPictureService _pictureService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly IStoreService _storeService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly ILocalizedModelFactory _localizedModelFactory;
        private readonly IVendorService _vendorService;

        #endregion

        #region Ctor

        public NBDeliverySlotController(ILocalizationService localizationService,
            INotificationService notificationService,
            IPermissionService permissionService,
            IWorkContext workContext,
            IDeliverySlotService deliverySlotService,
            IPictureService pictureService,
            IBaseAdminModelFactory baseAdminModelFactory,
            IStoreService storeService,
            ILocalizedEntityService localizedEntityService,
            ILocalizedModelFactory localizedModelFactory,
            IVendorService vendorService)
        {
            _localizationService = localizationService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _workContext = workContext;
            _deliverySlotService = deliverySlotService;
            _pictureService = pictureService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _storeService = storeService;
            _localizedEntityService = localizedEntityService;
            _localizedModelFactory = localizedModelFactory;
            _vendorService = vendorService;
        }

        #endregion

        #region Methods
        #region DeliverySlot

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDeliverySlots))
                return AccessDeniedView();

            if (_workContext.SubAdmin.IsSubAdminRole)
                return AccessDeniedView();

            return View(new DeliverySlotSearchModel());
        }

        /// <summary>
        /// Functionality to get DeliverySlots list
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual IActionResult DeliverySlotsList(DeliverySlotSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDeliverySlots))
                return AccessDeniedDataTablesJson();

            if (_workContext.SubAdmin.IsSubAdminRole)
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = PrepareDeliverySlotListModel(searchModel);

            return Json(model);
        }

        /// <summary>
        /// Functionality to create a new DeliverySlot
        /// </summary>
        /// <returns></returns>
        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDeliverySlots))
                return AccessDeniedView();

            if (_workContext.SubAdmin.IsSubAdminRole)
                return AccessDeniedView();

            //prepare model
            var model = PrepareDeliverySlotModel(new DeliverySlotModel(), null);

            return View(model);
        }

        /// <summary>
        /// Functionality to save a newly created DeliverySlot
        /// </summary>
        /// <param name="model"></param>
        /// <param name="continueEditing"></param>
        /// <returns></returns>
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Create(DeliverySlotModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDeliverySlots))
                return AccessDeniedView();

            if (_workContext.SubAdmin.IsSubAdminRole)
                return AccessDeniedView();

            if (string.IsNullOrEmpty(model.CustomDuration) && model.Duration == 0)
                ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.DeliverySlot.SelectAnyDurationOption"));

            if (model.StartDateUtc.Add(DateTime.Parse(model.StartTime).TimeOfDay) >= model.EndDateUtc.Add(DateTime.Parse(model.EndTime).TimeOfDay))
                ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.DeliverySlot.FromDateTimeLessThenToDateTime"));

            if (_workContext.IsVendorRole)
            {
                model.MerchantId = _workContext.CurrentCustomer.VendorId;
            }

            var slotExists = _deliverySlotService.CheckSameSlotExists(model.StartDateUtc.Add(DateTime.Parse(model.StartTime).TimeOfDay), model.EndDateUtc.Add(DateTime.Parse(model.EndTime).TimeOfDay), model.StoreId , model.MerchantId , model.Id);

            if(slotExists)
                ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.DeliverySlot.SameSlotAlreadyExists"));

            if (ModelState.IsValid)
            {
                //_notificationService.ErrorNotification(_localizationService.GetResource("NB.Admin.DeliverySlot.Empty.Error"));
                //return RedirectToAction("Create");

                var deliverySlot = new DeliverySlot
                {
                    Name = model.Name,
                    IsDefaultSlot = model.IsDefaultSlot,
                    Duration = !string.IsNullOrEmpty(model.CustomDuration) ? Convert.ToInt32(model.CustomDuration) : model.Duration,
                    IsCustomDuration = !string.IsNullOrEmpty(model.CustomDuration),
                    CutOffTime = model.CutOffTime,
                    StartDateUtc = model.StartDateUtc,
                    EndDateUtc = model.EndDateUtc,
                    StartTime = DateTime.Parse(model.StartTime).TimeOfDay,
                    EndTime = DateTime.Parse(model.EndTime).TimeOfDay,
                    NumberOfOrders = model.NumberOfOrders,
                    SurChargeFee = model.SurChargeFee,
                    DisplayOrder = model.DisplayOrder,
                    StoreId = model.StoreId,
                    MerchantId = model.MerchantId,
                    Published = model.Published,
                    Deleted = model.Deleted,
                    CreatedOnUtc = DateTime.UtcNow
                };
                _deliverySlotService.InsertDeliverySlot(deliverySlot);

                //locales
                UpdateLocales(deliverySlot, model);

                _notificationService.SuccessNotification(_localizationService.GetResource("NB.Admin.DeliverySlot.Added.Succesfully"));
                if (!continueEditing)
                {
                    return RedirectToAction("List");
                }

                return RedirectToAction("Edit", new { id = deliverySlot.Id });
            }

            //prepare model
            model = PrepareDeliverySlotModel(model, null);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        /// <summary>
        /// Functionality to edit a DeliverySlot
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDeliverySlots))
                return AccessDeniedView();

            if (_workContext.SubAdmin.IsSubAdminRole)
                return AccessDeniedView();

            var deliverySlot = _deliverySlotService.GetDeliverySlotById(id);
            if (deliverySlot == null)
            {
                throw new Exception("No deliverySlot found with the specified id");
            }

            //prepare model
            var model = PrepareDeliverySlotModel(null, deliverySlot);

            return View(model);
        }

        /// <summary>
        /// Functionality to edit a DeliverySlot
        /// </summary>
        /// <param name="model"></param>
        /// <param name="continueEditing"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(DeliverySlotModel model, bool continueEditing, IFormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDeliverySlots))
                return AccessDeniedView();

            if (_workContext.SubAdmin.IsSubAdminRole)
                return AccessDeniedView();

            var deliverySlot = _deliverySlotService.GetDeliverySlotById(model.Id);

            if (deliverySlot == null)
            {
                throw new Exception("No deliverySlot found with the specified id");
            }

            if(string.IsNullOrEmpty(model.CustomDuration) && model.Duration == 0)
                ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.DeliverySlot.SelectAnyDurationOption"));

            if (model.StartDateUtc.Add(DateTime.Parse(model.StartTime).TimeOfDay) >= model.EndDateUtc.Add(DateTime.Parse(model.EndTime).TimeOfDay))
                ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.DeliverySlot.FromDateTimeLessThenToDateTime"));

            if (_workContext.IsVendorRole)
            {
                model.MerchantId = _workContext.CurrentCustomer.VendorId;
            }

            var slotExists = _deliverySlotService.CheckSameSlotExists(model.StartDateUtc.Add(DateTime.Parse(model.StartTime).TimeOfDay), model.EndDateUtc.Add(DateTime.Parse(model.EndTime).TimeOfDay), model.StoreId, model.MerchantId, model.Id);

            if (slotExists)
                ModelState.AddModelError("", _localizationService.GetResource("NB.Admin.DeliverySlot.SameSlotAlreadyExists"));

            if (ModelState.IsValid)
            {
                //_notificationService.ErrorNotification(_localizationService.GetResource("NB.Admin.deliverySlot.Empty.Error"));
                //return RedirectToAction("Edit", new { id = model.Id });

                deliverySlot.Name = model.Name;
                deliverySlot.IsDefaultSlot = model.IsDefaultSlot;
                deliverySlot.Duration = !string.IsNullOrEmpty(model.CustomDuration) ? Convert.ToInt32(model.CustomDuration) : model.Duration;
                deliverySlot.IsCustomDuration = !string.IsNullOrEmpty(model.CustomDuration);
                deliverySlot.CutOffTime = model.CutOffTime;
                deliverySlot.StartDateUtc = model.StartDateUtc;
                deliverySlot.EndDateUtc = model.EndDateUtc;
                deliverySlot.StartTime = DateTime.Parse(model.StartTime).TimeOfDay;
                deliverySlot.EndTime = DateTime.Parse(model.EndTime).TimeOfDay;
                deliverySlot.NumberOfOrders = model.NumberOfOrders;
                deliverySlot.SurChargeFee = model.SurChargeFee;
                deliverySlot.DisplayOrder = model.DisplayOrder;
                deliverySlot.StoreId = model.StoreId;
                deliverySlot.MerchantId = model.MerchantId;
                deliverySlot.Published = model.Published;
                deliverySlot.UpdatedOnUtc = DateTime.UtcNow;

                _deliverySlotService.UpdateDeliverySlot(deliverySlot);

                /* TODO: Need to work on future */
                //if (model.IsDefaultSlot)
                //{
                //    var existingProductDeliverys = _deliverySlotService.GetProductDeliverySlotsByDeliverySlotId(deliverySlot.Id);

                //    //delete deliverySlots
                //    foreach (var existingProductDelivery in existingProductDeliverys)
                //        _deliverySlotService.DeleteProductDeliverySlot(existingProductDelivery);
                //}

                //locales
                UpdateLocales(deliverySlot, model);

                _notificationService.SuccessNotification(_localizationService.GetResource("NB.Admin.DeliverySlot.Updated.Succesfully"));
                if (!continueEditing)
                {
                    return RedirectToAction("List");
                }

                return RedirectToAction("Edit", new { id = model.Id });
            }

            //prepare model
            model = PrepareDeliverySlotModel(model, deliverySlot);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        /// <summary>
        /// Functionality to delete the DeliverySlot
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDeliverySlots))
                return AccessDeniedView();

            if (_workContext.SubAdmin.IsSubAdminRole)
                return AccessDeniedView();

            var deliverySlot = _deliverySlotService.GetDeliverySlotById(id);
            deliverySlot.Deleted = true;
            _deliverySlotService.UpdateDeliverySlot(deliverySlot);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult DeleteSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDeliverySlots))
                return AccessDeniedView();

            if (_workContext.SubAdmin.IsSubAdminRole)
                return AccessDeniedView();

            if (selectedIds != null)
            {
                var deliverySlots = _deliverySlotService.GetDeliverySlotByIds(selectedIds.ToArray());
                foreach (var deliverySlot in deliverySlots)
                {
                    deliverySlot.Deleted = true;
                    _deliverySlotService.UpdateDeliverySlot(deliverySlot);
                }
            }

            return Json(new { Result = true });
        }

        /// <summary>
        /// Functionality to prepare DeliverySlot list model
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        public virtual DeliverySlotListModel PrepareDeliverySlotListModel(DeliverySlotSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get deliverySlots
            var deliverySlots = _deliverySlotService.GetAllDeliverySlot(_workContext.IsAdminRole ? 0 : _workContext.GetCurrentStoreId, searchName: searchModel.Name, duration: searchModel.Duration, pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize, _workContext.IsVendorRole ? _workContext.CurrentVendor.Id : 0);

            var allStores = _storeService.GetAllStores();

            //prepare grid model
            var model = new DeliverySlotListModel().PrepareToGrid(searchModel, deliverySlots, () =>
            {
                return deliverySlots.Select(deliverySlot =>
                {
                    //fill in model values from the entity
                    var deliverySlotModel = new DeliverySlotModel
                    {
                        Id = deliverySlot.Id,
                        Name = deliverySlot.Name,
                        DisplayOrder = deliverySlot.DisplayOrder,
                        Published = deliverySlot.Published,
                        IsDefaultSlot = deliverySlot.IsDefaultSlot
                    };

                    var store = allStores.Where(x => x.Id == deliverySlot.StoreId).FirstOrDefault();
                    deliverySlotModel.StoreName = store == null ? string.Empty : store.Name;


                    return deliverySlotModel;
                });
            });

            return model;
        }

        protected virtual void UpdateLocales(DeliverySlot deliverySlot, DeliverySlotModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(deliverySlot,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);
            }
        }

        /// <summary>
        /// Prepare DeliverySlot Merchant search model
        /// </summary>
        /// <param name="searchModel">DeliverySlot Merchant search model</param>
        /// <param name="DeliverySlot">DeliverySlot</param>
        /// <returns>DeliverySlot Merchant search model</returns>
        protected virtual DeliverySlotMerchantSearchModel PrepareDeliverySlotMerchantSearchModel(DeliverySlotMerchantSearchModel searchModel, DeliverySlot deliverySlot)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (deliverySlot == null)
                throw new ArgumentNullException(nameof(deliverySlot));

            searchModel.DeliverySlotId = deliverySlot.Id;
            searchModel.StoreId = deliverySlot.StoreId;

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare DeliverySlot model
        /// </summary>
        /// <param name="model">DeliverySlot model</param>
        /// <param name="category">DeliverySlot</param>
        /// <returns>DeliverySlot model</returns>
        public virtual DeliverySlotModel PrepareDeliverySlotModel(DeliverySlotModel model, DeliverySlot deliverySlot)
        {
            //define localized model configuration action
            Action<DeliverySlotLocalizedModel, int> localizedModelConfiguration = null;
            
            if (deliverySlot != null)
            {
                //fill in model values from the entity
                if (model == null)
                {
                    model = new DeliverySlotModel
                    {
                        Id = deliverySlot.Id,
                        Name = deliverySlot.Name,
                        IsDefaultSlot = deliverySlot.IsDefaultSlot,
                        Duration = deliverySlot.Duration,
                        IsCustomDuration = deliverySlot.IsCustomDuration,
                        CutOffTime = deliverySlot.CutOffTime,
                        StartDateUtc = deliverySlot.StartDateUtc,
                        EndDateUtc = deliverySlot.EndDateUtc,
                        StartTime = DateTime.Today.Add(deliverySlot.StartTime).ToString("hh:mm tt"),
                        EndTime = DateTime.Today.Add(deliverySlot.EndTime).ToString("hh:mm tt"),
                        NumberOfOrders = deliverySlot.NumberOfOrders,
                        SurChargeFee = deliverySlot.SurChargeFee,
                        DisplayOrder = deliverySlot.DisplayOrder,
                        StoreId = deliverySlot.StoreId,
                        MerchantId = deliverySlot.MerchantId,
                        Published = deliverySlot.Published
                    };
                }

                //prepare nested search model
                PrepareDeliverySlotMerchantSearchModel(model.DeliverySlotMerchantSearchModel, deliverySlot);

                //define localized model configuration action
                localizedModelConfiguration = (locale, languageId) =>
                {
                    locale.Name = _localizationService.GetLocalized(deliverySlot, entity => entity.Name, languageId, false, false);
                };
            }

            //set default values for the new model
            if (deliverySlot == null)
            {
                model.Duration = 15;
                model.Published = true;
            }

            //prepare localized models
            model.Locales = _localizedModelFactory.PrepareLocalizedModels(localizedModelConfiguration);

            //prepare available stores
            _baseAdminModelFactory.PrepareStores(model.AvailableStores, false);

            //prepare available vendors
            PrepareVendors(model.AvailableMerchants, deliverySlot == null ? Convert.ToInt32(model.AvailableStores.FirstOrDefault().Value) : deliverySlot.StoreId);
            //_baseAdminModelFactory.PrepareVendors(model.AvailableMerchants, false);

            return model;
        }

        public void PrepareVendors(IList<SelectListItem> items , int storeId) 
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));
            int storeIdReg = storeId;
            int vendorIdReg = 0;
            if (_workContext.IsStoreOwnerRole)
            {
                storeIdReg = _workContext.GetCurrentStoreId;
            }
            else if (_workContext.IsVendorRole)
            {
                storeIdReg = _workContext.GetCurrentStoreId;
                vendorIdReg = _workContext.CurrentVendor.Id;
            }

            if (_workContext.IsStoreOwnerRole || _workContext.IsAdminRole)
            {
                var vendors = _vendorService.GetAllVendors(showHidden: false, storeId: storeIdReg);
                var lst = vendors.Select(v => new SelectListItem
                {
                    Text = v.Name,
                    Value = v.Id.ToString()
                }).ToList();
                foreach (var vendorItem in lst)
                    items.Add(vendorItem);

            }
            else if (_workContext.IsVendorRole)
            {
                var vendors = _vendorService.GetAllVendors(showHidden: false).Where(x => x.StoreId == storeIdReg && x.Id == vendorIdReg);
                var lst = vendors.Select(v => new SelectListItem
                {
                    Text = v.Name,
                    Value = v.Id.ToString()
                }).ToList();
                foreach (var vendorItem in lst)
                    items.Add(vendorItem);
            }

            if (!_workContext.IsVendorRole)
            {
                items.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            }
        }

        public virtual IActionResult GetMerchantsByStoreId(string storeId)
        {
            // This action method gets called via an ajax request
            if (string.IsNullOrEmpty(storeId))
                throw new ArgumentNullException(nameof(storeId));

            var vendors = _vendorService.GetAllVendors(showHidden: false,storeId: Convert.ToInt32(storeId));
            var result = (from s in vendors
                          select new { id = s.Id, name = s.Name }).ToList();
            result.Insert(0, new { id = 0, name = _localizationService.GetResource("Admin.Common.All") });

            return Json(result);
        }

        public virtual IActionResult GetDeliverySlotsByMerchantId(int merchantId)
        {
            var lst = new List<SelectListItem>();

            var deliverySlots = _deliverySlotService.GetAllDeliverySlot(merchantId: merchantId);
            lst = deliverySlots.Select(v => new SelectListItem
            {
                Text = v.Name,
                Value = v.Id.ToString()
            }).ToList();

            return Json(lst);
        }
        #endregion
        #endregion
    }
}