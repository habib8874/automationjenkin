﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Services.Common;
using Nop.Services.Tasks;

namespace Nop.Services.Caching
{
    /// <summary>
    /// Clear cache scheduled task implementation
    /// </summary>
    public partial class ClearCacheTask : IScheduleTask
    {
        #region Fields

        private readonly IStaticCacheManager _staticCacheManager;
        private readonly ICommonService _commonService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public ClearCacheTask(IStaticCacheManager staticCacheManager, ICommonService commonService, IWorkContext workContext)
        {
            _staticCacheManager = staticCacheManager;
            _commonService = commonService;
            _workContext = workContext;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Executes a task
        /// </summary>
        public void Execute()
        {
            _staticCacheManager.Clear();
            _commonService.ClearCache(_workContext.GetCurrentStoreId, string.Empty);
            _commonService.ClearCacheAgent(_workContext.GetCurrentStoreId, string.Empty);
        }

        #endregion
    }
}