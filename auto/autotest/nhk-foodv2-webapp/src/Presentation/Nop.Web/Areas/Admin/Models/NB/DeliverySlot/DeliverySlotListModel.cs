﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB
{
    /// <summary>
    /// Represents a DeliverySlot list model
    /// </summary>
    public partial class DeliverySlotListModel : BasePagedListModel<DeliverySlotModel>
    {
    }
}