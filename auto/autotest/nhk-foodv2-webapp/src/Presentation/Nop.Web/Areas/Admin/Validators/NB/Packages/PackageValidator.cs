﻿using FluentValidation;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Services.Seo;
using Nop.Web.Framework.Validators;
using Nop.Web.Areas.Admin.Models.NB.Package;

namespace Nop.Web.Areas.Admin.Validators.Packages
{
    public partial class PackageValidator : BaseNopValidator<PackageModel>
    {
        public PackageValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("NB.Admin.Package.Fields.Name.Required"));
        }
    }
}