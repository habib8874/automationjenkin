﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Areas.Admin.Validators.Catalog;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    public partial class ProductTagImageMapppingListModel : BasePagedListModel<ProductTagImageMapppingModel>
    {
    }
    public partial class ProductTagImageMapppingModel : BaseNopEntityModel, ILocalizedModel<ProductTagImageMapppingLocalizedModel>
    {
        public ProductTagImageMapppingModel()
        {
            Locales = new List<ProductTagImageMapppingLocalizedModel>();
            #region Custom code from v4.0
            SelectedStoreIds = new List<int>();
            AvailableStores = new List<SelectListItem>();
            #endregion
        }
        #region Custom code from v4.0
        //store mapping
        [NopResourceDisplayName("Admin.Catalog.Products.Fields.LimitedToStores")]
        public IList<int> SelectedStoreIds { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
        #endregion
        [NopResourceDisplayName("Admin.Catalog.ProductTagsImageMapping.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Catalog.ProductTagsImageMapping.Fields.TagId")]
        public int TagId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.ProductTagsImageMapping.Fields.ProductCount")]
        public int ProductCount { get; set; }

        [UIHint("Picture")]
        [NopResourceDisplayName("Admin.Catalog.ProductTagsImageMapping.Fields.PictureId")]
        public int Picture_Id { get; set; }

        [NopResourceDisplayName("Admin.Catalog.ProductTagsImageMapping.Fields.PictureThumbnailUrl")]
        public string PictureThumbnailUrl { get; set; }


        public IList<ProductTagImageMapppingLocalizedModel> Locales { get; set; }
    }

    public partial class ProductTagImageMapppingLocalizedModel : ILocalizedLocaleModel
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.ProductTagsImageMapping.Fields.LanguageName")]
        public string Name { get; set; }
    }
}