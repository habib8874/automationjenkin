﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.ViewModels
{
	public class OrderReviewModel
	{
		public string ApiKey { get; set; }
		public int CustomerId { get; set; }
		public int OrderId { get; set; }
		public int StoreId { get; set; }
		public string UniqueSeoCode { get; set; }
	}
	public class NotificationModel
	{
		public string ApiKey { get; set; }
		public int CustomerId { get; set; }
		public int StoreId { get; set; }
		public string UniqueSeoCode { get; set; }
	}
	public class OrderTrackingModel
	{
		public int OrderId { get; set; }
		public DateTime OrderDate { get; set; }
		public string OrderNumber { get; set; }
		public string Amount { get; set; }
		public string DiscountAmount { get; set; }
		public string OrderStatus { get; set; }
		public string BillingAddress { get; set; }
		public string ShippingAddress { get; set; }
		public string CustomerName { get; set; }
		public string PhoneNumber { get; set; }
		public string BillingAddressType { get; set; }
		public string ShippingAddressType { get; set; }
		public string MerchantAddressType { get; set; }
		public string CooknChefAddress { get; set; }
		public List<OrderedItems> OrderedItems { get; set; }
		public List<OrderedStatusArray> OrderedStatusArray { get; set; }
		public int OrderType { get; set; }
		public string PackingCharges { get; set; }
		public string SubAmount { get; set; }
		public string Tax { get; set; }
		public string DeliveryCharges { get; set; }
		public string ServiceChargeAmount { get; set; }
		public string OrderNotes { get; set; }
		public string AgentOrderNotes { get; set; }
		public bool IsOrderNotes { get; set; }
		public string Tip { get; set; }
		public string AdditionalInstructions { get; set; }
		public bool IsAdditionalInstructions { get; set; }
		public decimal MerchantRating { get; set; }
		public decimal MerchantRatingCount { get; set; }
		public string MerchantComment { get; set; }
		public decimal AgentRating { get; set; }
		public decimal AgentRatingCount { get; set; }
		public string AgentComment { get; set; }
		public string AgentResource { get; set; }
		public string MerchantResource { get; set; }
		public string MerchantImage { get; set; }
		public string AgentImage { get; set; }

	}

	public class OrderTrackingModelV1
	{
		public OrderTrackingModelV1()
		{
			LiveTrackingModel = new LiveTrackingModel();
		}

		public int OrderId { get; set; }
		public DateTime OrderDate { get; set; }
		public string OrderNumber { get; set; }
		public string Amount { get; set; }
		public string DiscountAmount { get; set; }
		public string OrderStatus { get; set; }
		public string BillingAddress { get; set; }
		public string ShippingAddress { get; set; }
		public string CustomerName { get; set; }
		public string PhoneNumber { get; set; }
		public string BillingAddressType { get; set; }
		public string ShippingAddressType { get; set; }
		public string MerchantAddressType { get; set; }
		public string CooknChefAddress { get; set; }
		public List<OrderedItems> OrderedItems { get; set; }
		public List<OrderedStatusArrayV1> OrderedStatusArray { get; set; }
		public int OrderType { get; set; }
		public string PackingCharges { get; set; }
		public string SubAmount { get; set; }
		public string Tax { get; set; }
		public string DeliveryCharges { get; set; }
		public string ServiceChargeAmount { get; set; }
		public string OrderNotes { get; set; }
		public string AgentOrderNotes { get; set; }
		public bool IsOrderNotes { get; set; }
		public string Tip { get; set; }
		public string AdditionalInstructions { get; set; }
		public bool IsAdditionalInstructions { get; set; }
		public decimal MerchantRating { get; set; }
		public decimal MerchantRatingCount { get; set; }
		public string MerchantComment { get; set; }
		public decimal AgentRating { get; set; }
		public decimal AgentRatingCount { get; set; }
		public string AgentComment { get; set; }
		public string AgentResource { get; set; }
		public string MerchantResource { get; set; }
		public string MerchantImage { get; set; }
		public string AgentImage { get; set; }
		public LiveTrackingModel LiveTrackingModel { get; set; }
		//Average rating
		public decimal MerchantAveragRating { get; set; }
		public int MerchantTotalRatingCount { get; set; }
		public decimal AgentAveragRating { get; set; }
		public int AgentTotalRatingCount { get; set; }
		//phone detail
		public string MerhantPhone { get; set; }
		public string AgentPhone { get; set; }

	}
	public class OrderedItems
	{
		public bool RatingEnable { get; set; }
		public int ProductId { get; set; }
		public string ProductName { get; set; }
		public int ProductQuantity { get; set; }
		public string ProductTotalPrice { get; set; }
		public string ProductPrice { get; set; }
		public string ProductImage { get; set; }
		public string ProductDescription { get; set; }
		public string BrandName { get; set; }
		public bool IsItemReviewFound { get; set; }
	}
	public class OrderNotesNotifiactionModel
	{
		public int OrderId { get; set; }
		public string Title { get; set; }
		public string OrderNumber { get; set; }
		public int OrderStatus { get; set; }
		public string Description { get; set; }
		public DateTime OrderDate { get; set; }
		public DateTime OrderNotificationDateTime { get; set; }
		public string OrderNotificationDate { get; set; }
		public string OrderNotificationTime { get; set; }
		public string OrderDeliveryTime { get; set; }
		public string OrderDeliveryDate { get; set; }
	}

	#region Order live tracking

	/// <summary>
	/// Represetn Order live tracking model
	/// </summary>
	public class LiveTrackingModel
	{
		public LiveTrackingModel()
		{
			RestauratTrakingInfo = new RestauratTrakingInfo();
			CustomerTrakingInfo = new CustomerTrakingInfo();
			RiderTrakingInfo = new RiderTrakingInfo();
			ErrorMessage = new List<string>();
		}

		public int OrderId { get; set; }
		public RestauratTrakingInfo RestauratTrakingInfo { get; set; }
		public CustomerTrakingInfo CustomerTrakingInfo { get; set; }
		public RiderTrakingInfo RiderTrakingInfo { get; set; }
		public bool TrackingPossible { get; set; }
		public IList<string> ErrorMessage { get; set; }
	}

	#region Info models

	public class RestauratTrakingInfo : LatLong
	{
		public int RestaurantId { get; set; }
		public string RestauratName { get; set; }
	}
	public class CustomerTrakingInfo : LatLong
	{
		public int CustomertId { get; set; }
		public string CustomerName { get; set; }
	}
	public class RiderTrakingInfo : LatLong
	{
		public int RidertId { get; set; }
		public string RiderName { get; set; }
	}

	public class LatLong
	{
		public string Latitude { get; set; }
		public string Longitude { get; set; }
	}

	#endregion

	#region AgentLocation

	public class AgentLocationRequestModel
	{
		public string ApiKey { get; set; }
		public int StoreId { get; set; }
		public int OrderId { get; set; }
		public int AgentId { get; set; }
	}

	public class AgentLocationResponceModel : LatLong
	{
		public bool IsOrderDelivered { get; set; }
	}

	#endregion

	#endregion
}