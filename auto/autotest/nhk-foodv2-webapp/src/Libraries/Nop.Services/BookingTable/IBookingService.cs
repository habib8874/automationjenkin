﻿using Nop.Core;
using Nop.Core.Domain.BookingTable;
using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.BookingTable
{
    /// <summary>
    /// SMS Master service
    /// </summary>
    public partial interface IBookingService
    {
        IPagedList<BookingTableMaster> SearchBooking(
             int pageIndex = 0,
             int pageSize = int.MaxValue,
             int TableNumber = 0,
             int storeId = 0,
             int vendorId = 0,
             bool showHidden=true
             );
        IPagedList<BookingTableTransaction> SearchBookingTables(
            int pageIndex = 0,
             int pageSize = int.MaxValue,
             DateTime? BookingSearchDate = null,
             string From = "",
             string To = "",
             int storeId = 0,
             int vendorId = 0,
              string SearchCustomerName = "",
              int SearchTableNumber = 0,
             string SearchContactNumber = "",
             int SearchBookingStatusId = 0,
             bool showHidden = true);

        void UpdateBooking(BookingTableTransaction bookingTableTransaction);
        void CancelBooking(BookingTableTransaction bookingTableTransaction, bool value);
        BookingTableTransaction GetBookingById(int bookingId);


        #region Booking Table Master
        /// <summary>
        /// GetMasterBookingById
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        BookingTableMaster GetMasterBookingById(int bookingId);
        /// <summary>
        /// DeleteMasterBookingById
        /// </summary>
        /// <param name="bookingTableMaster"></param>
        /// <returns></returns>
        bool DeleteMasterBookingById(BookingTableMaster bookingTableMaster);
        /// <summary>
        /// CreateBookingMaster
        /// </summary>
        /// <param name="bookingTableMaster"></param>
        /// <returns></returns>
        bool CreateBookingMaster(BookingTableMaster bookingTableMaster);
        #endregion


    }


}
