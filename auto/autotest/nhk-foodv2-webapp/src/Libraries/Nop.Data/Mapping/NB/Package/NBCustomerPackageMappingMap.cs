﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB.Package;

namespace Nop.Data.Mapping.NB.Package
{
    /// <summary>
    /// Represents a Package
    /// </summary>
    public partial class NBCustomerPackageMappingMap : NopEntityTypeConfiguration<NBCustomerPackageMapping>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<NBCustomerPackageMapping> builder)
        {
            builder.ToTable(NopMappingDefaults.CustomerPackageMappingTable);
            builder.HasKey(p => p.Id);
            base.Configure(builder);
        }

        #endregion
    }
}