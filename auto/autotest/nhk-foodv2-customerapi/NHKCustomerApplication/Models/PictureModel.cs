﻿namespace NHKCustomerApplication.Models
{
    public class PictureModel
    {
        public int PictureId { get; set; }
        public string PictureUrl { get; set; }
        public string AltText { get; set; }
    }
}
