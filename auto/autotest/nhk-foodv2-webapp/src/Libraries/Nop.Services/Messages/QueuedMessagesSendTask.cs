﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Nop.Services.Configuration;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace Nop.Services.Messages
{
    /// <summary>
    /// Represents a task for sending queued message 
    /// </summary>
    public partial class QueuedMessagesSendTask : IScheduleTask
    {
        #region Fields

        private readonly IEmailSender _emailSender;
        private readonly ISettingService _settingService;
        private readonly ILogger _logger;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailAccountService _emailAccountService;

        #endregion

        private string HtmlToPlainText(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);
            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);
            return text;
        }

        #region Ctor

        public QueuedMessagesSendTask(IEmailSender emailSender,
            ILogger logger,
            ISettingService settingService,
            IQueuedEmailService queuedEmailService,
            IEmailAccountService emailAccountService)
        {
            _emailSender = emailSender;
            _logger = logger;
            _settingService = settingService;
            _queuedEmailService = queuedEmailService;
            _emailAccountService = emailAccountService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Executes a task
        /// </summary>
        public virtual void Execute()
        {
            var maxTries = 3;
            var queuedEmails = _queuedEmailService.SearchEmails(null, null, null, null,
                true, true, maxTries, false, 0, 500);
            string accountSid = string.Empty;
            string authToken = string.Empty;
            string senderId = string.Empty;
            
            PhoneNumber from = null;
            if (queuedEmails != null && queuedEmails.Count > 0 && queuedEmails.Any(x => x.To.IndexOf('@') < 0))
            {
                accountSid = _settingService.GetSettingByKey<string>("twillio.accountsid");
                authToken = _settingService.GetSettingByKey<string>("twillio.authtoken");
                senderId = _settingService.GetSettingByKey<string>("twillio.senderid");
                if (string.IsNullOrEmpty(accountSid))
                {
                    accountSid = "AC039a08adfdbdc51566e5ebe18654dccd";
                }
                if (string.IsNullOrEmpty(authToken))
                {
                    authToken = "b07f1451c3b921e08f924fe81a86a0e3d";
                }
                if (string.IsNullOrEmpty(senderId))
                {
                    senderId = "5123578406";
                }
               from = new PhoneNumber(senderId);
            }
            foreach (var queuedEmail in queuedEmails)
            {
                var bcc = string.IsNullOrWhiteSpace(queuedEmail.Bcc)
                            ? null
                            : queuedEmail.Bcc.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                var cc = string.IsNullOrWhiteSpace(queuedEmail.CC)
                            ? null
                            : queuedEmail.CC.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                try
                {
                    if (queuedEmail.To.IndexOf("@") > -1)
                    {
                        _emailSender.SendEmail(queuedEmail.EmailAccount,
                            queuedEmail.Subject,
                            queuedEmail.Body,
                           queuedEmail.From,
                           queuedEmail.FromName,
                           queuedEmail.To,
                           queuedEmail.ToName,
                           queuedEmail.ReplyTo,
                           queuedEmail.ReplyToName,
                           bcc,
                           cc,
                           queuedEmail.AttachmentFilePath,
                           queuedEmail.AttachmentFileName,
                           queuedEmail.AttachedDownloadId);
                    }
                    else
                    {
                        var emailAcc = _emailAccountService.GetEmailAccountById(queuedEmail.EmailAccountId);
                        var storeId = emailAcc?.StoreId ?? 0;

                        bool useTwilio = _settingService.GetSettingByKey("NB.SMS.Use.Twilio", false, storeId, true);
                        if (!useTwilio)
                        {
                            var mashastrauserid = _settingService.GetSettingByKey("mashastra.userid", "", storeId, true);
                            var mashastrapassword = _settingService.GetSettingByKey("mashastra.password", "", storeId, true);
                            var mashastrasenderId = _settingService.GetSettingByKey("mashastra.senderid", "", storeId, true);
                            if (!string.IsNullOrEmpty(mashastrauserid) && !string.IsNullOrEmpty(mashastrapassword) && !string.IsNullOrEmpty(mashastrasenderId))
                            {
                                var toPhone = queuedEmail.To.Contains("+") ? queuedEmail.To : ("+" + queuedEmail.To);
                                var apiUrl = string.Format("https://mshastra.com/sendurlcomma.aspx?user={0}&pwd={1}&senderid={2}&mobileno={3}&msgtext={4}&priority=High&CountryCode=ALL&smstype=13&pe_id=1701159947934474629", mashastrauserid, mashastrapassword, mashastrasenderId, toPhone.Replace(" ", ""), HtmlToPlainText(queuedEmail.Body));

                                WebClient client = new WebClient();
                                Stream data = client.OpenRead(apiUrl);
                                StreamReader reader = new StreamReader(data);
                                var result = reader.ReadToEnd();
                                data.Close();
                                reader.Close();
                            }
                        }
                        else
                        {
                            // send sms
                            TwilioClient.Init(accountSid, authToken);
                            var to = new PhoneNumber(queuedEmail.To);
                            var message = MessageResource.Create(
                            to: to,
                            from: from,
                            body: HtmlToPlainText(queuedEmail.Body));
                        }
                    }
                    queuedEmail.SentOnUtc = DateTime.UtcNow;
                }
                catch (Exception exc)
                {
                    _logger.Error($"Error sending e-mail. {exc.Message}", exc);
                }
                finally
                {
                    queuedEmail.SentTries = queuedEmail.SentTries + 1;
                    _queuedEmailService.UpdateQueuedEmail(queuedEmail);
                }
            }
        }

        #endregion
    }
}