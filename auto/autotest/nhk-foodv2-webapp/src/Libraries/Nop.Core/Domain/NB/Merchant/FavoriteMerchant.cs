﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Vendors;

namespace Nop.Core.Domain.NB.Merchant
{
    /// <summary>
    /// Represents a favorite vendor(merchant) entity class
    /// </summary>
    public partial class FavoriteMerchant : BaseEntity
    {
        /// <summary>
        /// Gets or sets the customer identifier
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the vendor identifier
        /// </summary>
        public int VendorId { get; set; }

        /// <summary>
        /// Gets or sets the customer role
        /// </summary>
        public virtual Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets the vendor
        /// </summary>
        public virtual Vendor Vendor { get; set; }
    }
}
