﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nestorbird.Plugin.Api.RequestModel.Categories;
using Nestorbird.Plugin.Api.ResponseModel.Categories;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Vendors;

namespace Nestorbird.Plugin.Api.Controllers
{
    [Route("merchantapi/category")]
    public class MerchantCategoryController : BaseAPIController
    {
        #region Fields

        private readonly ICategoryService _categoryService;
        private readonly ILocalizationService _localizationService;
        private readonly IProductService _productService;
        private readonly IPictureService _pictureService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IVendorService _vendorService;
        private readonly IStoreService _storeService;

        #endregion

        #region Ctor

        public MerchantCategoryController(ILanguageService languageService,
            ICategoryService categoryService,
            ILocalizationService localizationService,
            IProductService productService,
            IPictureService pictureService,
            IUrlRecordService urlRecordService,
            ILocalizedEntityService localizedEntityService,
            IStoreMappingService storeMappingService,
            IVendorService vendorService,
            IStoreService storeService) : base(languageService)
        {
            _categoryService = categoryService;
            _localizationService = localizationService;
            _productService = productService;
            _pictureService = pictureService;
            _urlRecordService = urlRecordService;
            _localizedEntityService = localizedEntityService;
            _storeMappingService = storeMappingService;
            _vendorService = vendorService;
            _storeService = storeService;
        }

        #endregion

        #region Methods

        private void SaveStoreMappings(Category category, int vendorId, IList<int> storeIds)
        {
            category.LimitedToStores = storeIds.Any();

            var existingStoreMappings = _storeMappingService.GetStoreMappings(category);
            var allStores = _storeService.GetAllStores();
            foreach (var store in allStores)
            {
                if (storeIds.Contains(store.Id))
                {
                    //new store
                    if (existingStoreMappings.Count(sm => sm.StoreId == store.Id) == 0)
                        _storeMappingService.InsertStoreMapping(category, store.Id, vendorId);
                    else
                    {
                        var storeMapping = existingStoreMappings.FirstOrDefault(sm => sm.StoreId == store.Id);
                        storeMapping.VendorId = vendorId;
                        _storeMappingService.UpdateStoreMapping(storeMapping);
                    }
                }
                else
                {
                    //remove store
                    var storeMappingToDelete = existingStoreMappings.FirstOrDefault(sm => sm.StoreId == store.Id);
                    if (storeMappingToDelete != null)
                        _storeMappingService.DeleteStoreMapping(storeMappingToDelete);
                }
            }
        }

        private string GeMimeTypeFromImageByteArray(byte[] byteArray)
        {
            using (var stream = new MemoryStream(byteArray))
            using (var image = Image.FromStream(stream))
            {
                return ImageCodecInfo.GetImageEncoders().First(codec => codec.FormatID == image.RawFormat.Guid).MimeType;
            }
        }

        private void UpdatePictures(Category category, string imageBase64, string imageName, bool removeImage)
        {
            var pictureId = SavePicture(imageBase64, category.Id + imageName);
            if(pictureId > 0)
            {
                category.PictureId = pictureId;
                _categoryService.UpdateCategory(category);
            }
            else if (removeImage)
            {
                category.PictureId = 0;
                _categoryService.UpdateCategory(category);
            }
        }

        [HttpPost]
        [Route("getcategories")]
        [ProducesResponseType(typeof(CategoryListResponse), StatusCodes.Status200OK)]
        public virtual IActionResult GetCategories(CategoryListRequest request)
        {
            if (request == null)
                return Error("Please provide proper request");

            if (request.PageSize <= 0)
                request.PageSize = int.MaxValue;

            var categories = _categoryService.GetAllCategoriesVendorwise(vendorId: request.VendorId,
                pageIndex: request.PageIndex, pageSize: request.PageSize, showHidden: true);

            var categoryList = categories.Select(category =>
            {
                var picture = _pictureService.GetPictureById(category.PictureId);
                return new CategoryResponse
                {
                    Id = category.Id,
                    Name = _localizationService.GetLocalized(category, x => x.Name, CurrentLanguageId),
                    Description = _localizationService.GetLocalized(category, x => x.Description, CurrentLanguageId),
                    NumberOfProducts = _productService.GetProductCountByCategoryId(vendorId: request.VendorId, categoryId: category.Id),
                    ImageUrl = _pictureService.GetPictureUrl(picture, 100)
                };
            }).ToList();

            var response = new CategoryListResponse
            {
                Categories = categoryList,
                PageIndex = categories.PageIndex,
                PageSize = categories.PageSize,
                TotalCount = categories.TotalCount,
                TotalPages = categories.TotalPages
            };

            return Success(response);
        }

        [HttpPost]
        [Route("deletecategories")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        public virtual IActionResult DeleteCategories([FromBody]IList<int> categoryIds)
        {
            if (categoryIds.Count <= 0)
                return Error("Please enter category ids");

            foreach (var id in categoryIds)
            {
                var category = _categoryService.GetCategoryById(id);
                if (category != null)
                {
                    _categoryService.DeleteCategory(category);
                }
            }

            return Success("", "Categories deleted successfully");
        }

        [HttpGet]
        [Route("getcategorydetails")]
        [ProducesResponseType(typeof(AddCategoryRequest), StatusCodes.Status200OK)]
        public virtual IActionResult GetCategoryDetail(int id, int vendorId)
        {
            if (id <= 0)
                return Error("Please provide category id");

            var category = _categoryService.GetCategoryById(id);
            if (category == null)
                return Error("Please provide valid category id");

            var picture = _pictureService.GetPictureById(category.PictureId);
            var response = new AddCategoryRequest
            {
                Name = category.Name,
                Description = category.Description,
                Published = category.Published,
                DisplayOrder = category.DisplayOrder,
                ParentCategoryId = category.ParentCategoryId,
                ShowOnHomepage = category.ShowOnHomepage,
                Id = category.Id,
                VendorId = vendorId,
                CategoryTemplateId = category.CategoryTemplateId,
                ImageUrl = _pictureService.GetPictureUrl(picture, 500)
            };

            var productIds = _categoryService.GetProductCategoriesByCategoryId(category.Id, showHidden: true);
            var products = productIds.Where(x => x.Product.VendorId == vendorId).ToList();
            response.ProductIds = products.Select(x => x.ProductId).ToList();

            return Success(response);
        }

        [HttpPost]
        [Route("addcategoy")]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        public IActionResult AddCategory(AddCategoryRequest request)
        {
            try
            {
                if (request == null)
                    return Error("Please provide proper request");

                if (string.IsNullOrEmpty(request.Name))
                    return Error("Please enter name");

                if (request.VendorId <= 0)
                    return Error("Please enter vendor id");

                var category = new Category
                {
                    Name = request.Name,
                    CreatedOnUtc = DateTime.UtcNow,
                    UpdatedOnUtc = DateTime.UtcNow,
                    Description = request.Description,
                    Published = request.Published,
                    DisplayOrder = request.DisplayOrder,
                    ParentCategoryId = request.ParentCategoryId,
                    ShowOnHomepage = request.ShowOnHomepage,
                    CategoryTemplateId = request.CategoryTemplateId,
                    PageSize = 6,
                    AllowCustomersToSelectPageSize = true,
                    PageSizeOptions = "6, 3, 9"
                };
                _categoryService.InsertCategory(category);

                // add sename
                var seName = _urlRecordService.ValidateSeName(category, request.Name, category.Name, true);
                _urlRecordService.SaveSlug(category, seName, 0);

                // add locale
                _localizedEntityService.SaveLocalizedValue(category, x => x.Name, request.Name, CurrentLanguageId);

                // add store 
                var vendorStoreId = _vendorService.GetVendorById(request.VendorId)?.StoreId ?? 0;
                if (vendorStoreId > 0)
                {
                    SaveStoreMappings(category, request.VendorId, new List<int> { vendorStoreId });
                }

                // add product mappings
                if (request.ProductIds?.Count > 0)
                {
                    foreach (var productId in request.ProductIds)
                    {
                        if (productId > 0)
                        {
                            _categoryService.InsertProductCategory(new ProductCategory
                            {
                                CategoryId = category.Id,
                                ProductId = productId,
                                IsFeaturedProduct = false,
                                DisplayOrder = 1
                            });
                        }
                    }
                }

                UpdatePictures(category, request.ImageBase64, request.ImageName, request.RemoveImage);
                return Success(category.Id, "Category added successfully");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }

        [HttpPost]
        [Route("editcategoy")]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        public IActionResult EditCategory(AddCategoryRequest request)
        {
            try
            {
                if (request == null)
                    return Error("Please provide proper request");

                if (request.Id <= 0)
                    return Error("Please provide category id");

                if (string.IsNullOrEmpty(request.Name))
                    return Error("Please enter name");

                if (request.VendorId <= 0)
                    return Error("Please enter vendor id");

                var category = _categoryService.GetCategoryById(request.Id);
                if (category == null)
                    return Error("Please provide proper category id");

                category.Name = request.Name;
                category.UpdatedOnUtc = DateTime.UtcNow;
                category.Description = request.Description;
                category.Published = request.Published;
                category.DisplayOrder = request.DisplayOrder;
                category.ParentCategoryId = request.ParentCategoryId;
                category.ShowOnHomepage = request.ShowOnHomepage;
                category.CategoryTemplateId = request.CategoryTemplateId;
                _categoryService.UpdateCategory(category);

                // add sename
                var seName = _urlRecordService.ValidateSeName(category, request.Name, category.Name, true);
                _urlRecordService.SaveSlug(category, seName, 0);

                // add locale
                _localizedEntityService.SaveLocalizedValue(category, x => x.Name, request.Name, CurrentLanguageId);

                // add store 
                var vendorStoreId = _vendorService.GetVendorById(request.VendorId)?.StoreId ?? 0;
                if (vendorStoreId > 0)
                {
                    SaveStoreMappings(category, request.VendorId, new List<int> { vendorStoreId });
                }

                // add product mappings
                var existingProducts = _categoryService.GetProductCategoriesByCategoryId(category.Id, showHidden: true);
                var existproducts = existingProducts.Where(x => x.Product.VendorId == request.VendorId).ToList();
                var removedProducts = existproducts.Where(x => !request.ProductIds.Any(y => y == x.ProductId)).ToList();

                foreach (var item in removedProducts)
                {
                    if (item.Product.VendorId == request.VendorId)
                    {
                        _categoryService.DeleteProductCategory(item);
                    }
                }

                if (request.ProductIds?.Count > 0)
                {
                    foreach (var productId in request.ProductIds)
                    {
                        if (productId > 0)
                        {
                            if (existingProducts.Any(x => x.ProductId == productId))
                                continue;

                            _categoryService.InsertProductCategory(new ProductCategory
                            {
                                CategoryId = category.Id,
                                ProductId = productId,
                                IsFeaturedProduct = false,
                                DisplayOrder = 1
                            });
                        }
                    }
                }

                UpdatePictures(category, request.ImageBase64, request.ImageName, request.RemoveImage);
                return Success(category.Id, "Category updated successfully");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }

        #endregion
    }
}
