﻿using System;

namespace Nestorbird.Plugin.Api.ResponseModel.Order
{
    public class OrderNoteResponse
    {        
        public string Note { get; set; }

        public bool DisplayToCustomer { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
