﻿using NHKCustomerApplication.Models;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace NHKCustomerApplication.Repository
{
	public class WalletRepository : IWalletRepository
	{
		#region Fields

		private readonly string _connectionString;
		private readonly ecuadordevContext _db;

		#endregion

		#region Constructor

		public WalletRepository(ecuadordevContext db)
		{
			_db = db;
			_connectionString = Helper.GetConnectionString();
		}

		#endregion

		#region Methods

		public List<WalletRequestModel> GetWalletTransactions(int customerID)
		{
			return _db.CustomerWallet.Where(x => x.CustomerId == customerID).Select(x => new WalletRequestModel
			{
				Amount = x.Amount,
				Credited = x.Credited,
				Note = x.Note,
				TransactionOnUtc = Helper.ConvertToUserTime(x.TransactionOnUtc, DateTimeKind.Utc, _db).ToString("dd MMM yyyy hh:mm:ss tt")
			}).ToList();
		}

		public void AddMoneyToWallet(CustomerWallet wallet)
		{
			_db.CustomerWallet.Add(wallet);
			_db.SaveChanges();
		}
		#endregion
	}
}
