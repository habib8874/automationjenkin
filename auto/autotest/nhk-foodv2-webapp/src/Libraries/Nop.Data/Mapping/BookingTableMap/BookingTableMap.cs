﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.BookingTable;

namespace Nop.Data.Mapping.BookingTableMap
{
    public partial class BookingTableMap:NopEntityTypeConfiguration<BookingTableMaster>
    {
        public override void Configure(EntityTypeBuilder<BookingTableMaster> builder)
        {
            builder.ToTable("BookingTableMaster");
            builder.HasKey(p => p.Id);
            base.Configure(builder);
        }
    }
}
