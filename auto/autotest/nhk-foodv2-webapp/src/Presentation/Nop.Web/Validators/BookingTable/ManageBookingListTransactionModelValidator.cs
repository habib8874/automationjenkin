﻿using FluentValidation;
using Nop.Services.Localization;
using Nop.Web.Areas.Admin.Models.BookingTable;
using Nop.Web.Framework.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Validators.BookingTable
{
    public partial class ManageBookingListTransactionModelValidator : BaseNopValidator<BookingTableTransactionListSearchModel>
    {
        public ManageBookingListTransactionModelValidator(ILocalizationService localizationService)
       {

            RuleFor(x => x.DateOfBirthDay).NotEmpty().WithMessage(localizationService.GetResource("Booking.BookingDate.TextCannotBeEmpty"));
            RuleFor(x => x.SearchStoreId).NotEmpty().WithMessage(localizationService.GetResource("Booking.Store.TextCannotBeEmpty"));
            RuleFor(x => x.SearchStoreId).Equal(0).WithMessage(localizationService.GetResource("Booking.Store.TextCannotBeEmpty"));
            RuleFor(x => x.SearchVendorId).Equal(0).WithMessage(localizationService.GetResource("Booking.Vendor.TextCannotBeEmpty"));
            RuleFor(x => x.SearchVendorId).NotEmpty().WithMessage(localizationService.GetResource("Booking.Vendor.TextCannotBeEmpty"));
            RuleFor(x => x.From).NotEmpty().WithMessage(localizationService.GetResource("Booking.From.TextCannotBeEmpty"));
            RuleFor(x => x.To).NotEmpty().WithMessage(localizationService.GetResource("Booking.To.TextCannotBeEmpty"));
            RuleFor(x => x.TableNumber).NotEmpty().WithMessage(localizationService.GetResource("Booking.Tablenumber.TextCannotBeEmpty"));
            RuleFor(x => x.MobileNumber).NotEmpty().WithMessage(localizationService.GetResource("Booking.CustomerPhone.TextCannotBeEmpty"));
            RuleFor(x => x.CustomerName).NotEmpty().WithMessage(localizationService.GetResource("Booking.CustomerName.TextCannotBeEmpty"));
            RuleFor(x => x.Email)
                 .NotEmpty()
                 .WithMessage(localizationService.GetResource("Address.Fields.Email.Required"));
            RuleFor(x => x.Email)
                .EmailAddress()
                .WithMessage(localizationService.GetResource("Common.WrongEmail"));


        }
    }
}