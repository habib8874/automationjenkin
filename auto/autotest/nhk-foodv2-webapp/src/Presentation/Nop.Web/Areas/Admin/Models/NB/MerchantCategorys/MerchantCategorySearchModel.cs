﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.MerchantCategorys
{
    /// <summary>
    /// Represents a MerchantCategory search model
    /// </summary>
    public partial class MerchantCategorySearchModel : BaseSearchModel
    {
        #region Properties
        public int Id { get; set; }


        [NopResourceDisplayName("Admin.Catalog.MerchantCategory.List.Name")]
        public string Name { get; set; }
        public int StoreId { get; set; }
        #endregion
    }
}