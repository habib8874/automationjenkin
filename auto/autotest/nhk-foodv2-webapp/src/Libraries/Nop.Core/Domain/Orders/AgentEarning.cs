﻿using System;

namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents an Agent Earning
    /// </summary>
    public partial class AgentEarning : BaseEntity
    {
        
        public int AgentId { get; set; }

        public int OrderId { get; set; }
        public decimal PayableAmount { get; set; }
        public decimal PaidAmount { get; set; }
    }
}