﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB.Discount;

namespace Nop.Data.Mapping.NB.Discount
{
    /// <summary>
    /// Represents a discount-vendor mapping configuration
    /// </summary>
    public partial class DiscountVendorMap : NopEntityTypeConfiguration<DiscountVendorMapping>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<DiscountVendorMapping> builder)
        {
            builder.ToTable(NopMappingDefaults.DiscountVendorMapping);
            builder.HasKey(mapping => new { mapping.DiscountId, mapping.VendorId });

            builder.Property(mapping => mapping.DiscountId).HasColumnName("Discount_Id");
            builder.Property(mapping => mapping.VendorId).HasColumnName("VendorId");

            builder.HasOne(mapping => mapping.Discount)
                .WithMany(discount => discount.DiscountVendorMapping)
                .HasForeignKey(mapping => mapping.DiscountId)
                .IsRequired();

            builder.HasOne(mapping => mapping.Vendor)
                .WithMany(vendor => vendor.DiscountVendorMapping)
                .HasForeignKey(mapping => mapping.VendorId)
                .IsRequired();

            builder.Ignore(mapping => mapping.Id);

            base.Configure(builder);
        }

        #endregion
    }
}
