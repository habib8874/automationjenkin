﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Vendors;

namespace Nop.Data.Mapping.Vendors
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class VendorScheduleMap : NopEntityTypeConfiguration<VendorSchedule>
    {
        /// <summary>
        /// Ctor
        /// </summary>
        public override void Configure(EntityTypeBuilder<VendorSchedule> builder)
        {
            builder.ToTable("Vendor_Schedule_Mapping");
            builder.HasKey(pp => pp.Id);

            builder.HasOne(value => value.Vendor)
              .WithMany()
              .HasForeignKey(value => value.VendorId)
              .IsRequired();

            base.Configure(builder);


        }
    }
}
