﻿namespace NHKCustomerApplication.ViewModel
{
    public class BaseAPIRequestModel
    {
        public int StoreId { get; set; }
        public string ApiKey { get; set; }
        public string UniqueSeoCode { get; set; }
    }
}
