﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Vendors
{
    /// <summary>
    /// Represents a vendor slider picture search model
    /// </summary>
    public partial class VendorSliderPictureSearchModel : BaseSearchModel
    {
        #region Properties

        public int VendorId { get; set; }

        #endregion
    }
}
