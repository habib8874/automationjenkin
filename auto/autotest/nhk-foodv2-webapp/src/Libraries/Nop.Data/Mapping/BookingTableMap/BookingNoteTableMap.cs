﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.BookingTable;

namespace Nop.Data.Mapping.BookingTableMap
{
    public partial class BookingNoteTableMap: NopEntityTypeConfiguration<BookingNote>
    {
        public override void Configure(EntityTypeBuilder<BookingNote> builder)
        {
            builder.ToTable("BookingNote");
            builder.HasKey(p => p.Id);
            builder.Property(c => c.BookingId).IsRequired();
            builder.Property(c => c.Note).IsRequired();
            builder.Property(c => c.DisplayToCustomer).IsRequired();
            builder.Property(c => c.DownloadId).IsRequired();
            builder.Property(c => c.CreatedOnUtc).IsRequired();
            base.Configure(builder);
        }
    }
}
