﻿using System;
using Nop.Core.Domain.Directory;

namespace Nop.Core.Domain.Common
{
    /// <summary>
    /// AddressType
    /// </summary>
    public partial class AddressType : BaseEntity
    {
        public string Name { get; set; }

        public string ImageURL { get; set; }

        public int? StoreId{ get; set; }

        public int? MerchantId { get; set; }
    }
}
