﻿using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Models.Checkout
{
    public partial class CheckoutPaymentInfoModel : BaseNopModel
    {
        public CheckoutPaymentInfoModel()//Custom code from v4.0
        {
            Warnings = new List<string>();
        }
        public string PaymentViewComponentName { get; set; }

        /// <summary>
        /// Used on one-page checkout page
        /// </summary>
        public bool DisplayOrderTotals { get; set; }
        public IList<string> Warnings { get; set; }//Custom code from v4.0
    }
}