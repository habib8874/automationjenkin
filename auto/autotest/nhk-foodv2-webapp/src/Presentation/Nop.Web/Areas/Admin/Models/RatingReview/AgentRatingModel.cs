﻿using System;
using Nop.Web.Areas.Admin.Validators.Catalog;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.RatingReview
{
    public partial class AgentRatingModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Store")]
        public string StoreName { get; set; }
        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Product")]
        public int ProductId { get; set; }
        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Product")]
        public string ProductName { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Customer")]
        public int CustomerId { get; set; }
        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Customer")]
        public string CustomerInfo { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Title")]
        public string Title { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.ReviewText")]
        public string ReviewText { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.ReplyText")]
        public string ReplyText { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Rating")]
        public int Rating { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.IsApproved")]
        public bool IsApproved { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }

        //vendor
        public bool IsLoggedInAsVendor { get; set; }



        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.ReviewOptions")]
        public string ReviewOptions { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.DislikedReviewOptions")]
        public string DislikedReviewOptions { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Agent")]
        public int AgentId { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Agent")]
        public string Agent { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.AgentRating.Fields.Like/Dislike")]
        public bool LikeDislike { get; set; }

        public FeedbackModel FeedbackModel { get; set; }
    }
}