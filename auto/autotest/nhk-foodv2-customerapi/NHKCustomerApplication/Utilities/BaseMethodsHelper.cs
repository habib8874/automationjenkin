﻿using NHKCustomerApplication.Models;
using NHKCustomerApplication.ViewModel;
using System;
using System.Linq;
using System.Net;

namespace NHKCustomerApplication.Utilities
{
    public static class BaseMethodsHelper
    {
        /// <summary>
        /// Check api key
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="storeId"></param>
        /// <param name="db"></param>
        /// <returns>bool</returns>
        public static bool CheckAPIKey(string apiKey, int storeId, ecuadordevContext db)
        {
            return db.VersionInfo.Any(x => x.Apikey == apiKey && x.StoreId==storeId);
        }

        /// <summary>
        /// Check value for need to check store front or not
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="db"></param>
        /// <returns>bool</returns>
        public static bool CheckStoreFront(int storeId, ecuadordevContext db)
        {
            return Convert.ToBoolean(db.Setting.FirstOrDefault(x => x.Name.ToLower() == "storeinformationsettings.checkstorefront" && x.StoreId == storeId)?.Value??"False");
        }

        /// <summary>
        /// Check store front is enabled or not
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="db"></param>
        /// <returns>bool</returns>
        public static bool IsStoreFront(int storeId, ecuadordevContext db)
        {
            return Convert.ToBoolean(db.Setting.FirstOrDefault(x => x.Name.ToLower() == "storeinformationsettings.isstorefront" && x.StoreId == storeId)?.Value ?? "False");
        }

        /// <summary>
        /// Prepare error response
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <param name="statusCode"></param>
        /// <returns>BaseAPIResponseModel</returns>
        public static BaseAPIResponseModel ErrorResponse(string errorMessage, HttpStatusCode statusCode = HttpStatusCode.NoContent)
        {
            return new BaseAPIResponseModel
            {
                ErrorMessageTitle = "Error!!",
                ErrorMessage = errorMessage,
                Status = false,
                StatusCode = (int)statusCode,
                ResponseObj = null
            };
        }

        /// <summary>
        /// Prepare sussess response
        /// </summary>
        /// <param name="successMessage"></param>
        /// <param name="objData"></param>
        /// <returns>BaseAPIResponseModel</returns>
        public static BaseAPIResponseModel SuccessResponse(string successMessage, object objData=null)
        {
            return new BaseAPIResponseModel
            {
                ErrorMessageTitle = "Success!!",
                ErrorMessage = successMessage,
                Status = true,
                StatusCode = (int)HttpStatusCode.OK,
                ResponseObj = objData
            };
        }

        /// <summary>
        /// Prepare error response of list
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <param name="statusCode"></param>
        /// <returns>BaseAPIListResponseModel</returns>
        public static BaseAPIListResponseModel ErrorListResponse(string errorMessage, HttpStatusCode statusCode = HttpStatusCode.NoContent)
        {
            return new BaseAPIListResponseModel
            {
                ErrorMessageTitle = "Error!!",
                ErrorMessage = errorMessage,
                Status = false,
                StatusCode = (int)statusCode,
                TotalRecords=0,
                ResponseObj = null
            };
        }

        /// <summary>
        /// Prepare success response of list
        /// </summary>
        /// <param name="successMessage"></param>
        /// <param name="objData"></param>
        /// <param name="totalRecords"></param>
        /// <returns>BaseAPIListResponseModel</returns>
        public static BaseAPIListResponseModel SuccessListResponse(string successMessage, object objData = null,int totalRecords=0)
        {
            return new BaseAPIListResponseModel
            {
                ErrorMessageTitle = "Success!!",
                ErrorMessage = successMessage,
                Status = true,
                StatusCode = (int)HttpStatusCode.OK,
                TotalRecords= totalRecords,
                ResponseObj = objData
            };
        }
    }
}