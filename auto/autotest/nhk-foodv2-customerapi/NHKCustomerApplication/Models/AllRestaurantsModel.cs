﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NHKCustomerApplication.Models
{
    public class AllRestaurantsModel
    {
        public int CustomerId;
        public int splitSize;
        public int endItemCount;
        public int restType;
        public int storeId;
        public int latPos;
        public int longPos;
        public int merchantCategoryIds;
        public string uniqueSeoCode;
        public string token;
        public string apiKey;
        public string skipLocationSearch;
    }
}
