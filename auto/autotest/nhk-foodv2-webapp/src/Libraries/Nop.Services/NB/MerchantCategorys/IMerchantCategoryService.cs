﻿using Nop.Core;
using Nop.Core.Domain.NB.MerchantCategorys;
using System.Collections.Generic;

namespace Nop.Services.NB.MerchantCategorys
{
    /// <summary>
    /// MerchantCategory Property service interface
    /// </summary>
    public partial interface IMerchantCategoryService
    {
        #region MerchantCategory

        /// <summary>
        /// Gets a MerchantCategory  
        /// </summary>
        /// <param name="MerchantCategoryId">MerchantCategory  identifier</param>
        /// <returns>MerchantCategory </returns>
        MerchantCategory GetMerchantCategoryById(int merchantCategoryId);

        /// <summary>
        /// Gets a MerchantCategory 
        /// </summary>
        /// <param name="storeId">storeId  identifier</param>
        /// <param name="searchName">searchName  identifier</param>
        /// <returns>MerchantCategory </returns>
        IList<MerchantCategory> GetAllMerchantCategory(int storeId = 0 ,string searchName = null, IList<int> vendorIds = null);

        /// <summary>
        /// Inserts a MerchantCategory
        /// </summary>
        /// <param name="MerchantCategory">MerchantCategory </param>
        void InsertMerchantCategory(MerchantCategory merchantCategory);

        /// <summary>
        /// Updates the MerchantCategory 
        /// </summary>
        /// <param name="MerchantCategory">MerchantCategory </param>
        void UpdateMerchantCategory(MerchantCategory merchantCategory);

        /// <summary>
        /// Delete a MerchantCategory
        /// </summary>
        /// <param name="MerchantCategory">MerchantCategory</param>
        void DeleteMerchantCategory(MerchantCategory merchantCategory);

        /// <summary>
        /// Gets a MerchantCategory  
        /// </summary>
        /// <param name="MerchantCategoryIds">MerchantCategory  identifier</param>
        /// <returns>MerchantCategory list</returns>
        IList<MerchantCategory> GetMerchantCategorysByIds(int[] merchantCategoryIds);

        #endregion

        #region MerchantCategoryMapping

        /// <summary>
        /// Inserts a MerchantCategoryMapping
        /// </summary>
        /// <param name="MerchantCategoryMapping">MerchantCategoryMapping</param>
        void InsertMerchantCategoryMapping(MerchantCategoryMapping merchantCategoryMapping);

        /// <summary>
        /// Gets all MerchantCategoryMapping
        /// </summary>
        /// <returns>MerchantCategoryMapping list</returns>
        IList<MerchantCategoryMapping> GetAllMerchantCategoryMapping();

        /// <summary>
        /// Gets Merchant Category mapping collection
        /// </summary>
        /// <param name="merchantCategoryId">MerchantCategory identifier</param>
        /// <returns>Merchant Category mapping collection</returns>
        IList<MerchantCategoryMapping> GetMerchantCategoryMerchantsByMerchantCategoryId(int merchantCategoryId);

        /// <summary>
        /// Gets a MerchantCategoryMapping  
        /// </summary>
        /// <param name="MerchantCategoryMappingId">MerchantCategoryMapping  identifier</param>
        /// <returns>MerchantCategory </returns>
        MerchantCategoryMapping GetMerchantCategoryMappingById(int merchantCategoryMappingId);


        /// <summary>
        /// Delete a merchant category mapping
        /// </summary>
        /// <param name="MerchantCategoryMapping">MerchantCategoryMapping</param>
        void DeleteMerchantCategoryMapping(MerchantCategoryMapping merchantCategoryMapping);

        /// <summary>
        /// Returns a MerchantCategoryMapping that has the specified values
        /// </summary>
        /// <param name="source">Source</param>
        /// <param name="vendorId">Vendor identifier</param>
        /// <param name="merchantCategoryId">merchantCategory identifier</param>
        /// <returns>A MerchantCategoryMapping that has the specified values; otherwise null</returns>
        MerchantCategoryMapping FindMerchantCategoryMapping(IList<MerchantCategoryMapping> source, int vendorId, int merchantCategoryId);
        #endregion
    }
}
