﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Common;
using Nop.Web.Framework.Security;
using Microsoft.AspNetCore.Http;
using Nop.Web.Models.Checkout;
using Nop.Core.Domain.Shipping;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.OnePageCheckoutModelv2
{
    public class OnePageCheckoutModelv2 : BaseNopModel
    {
        public OnePageCheckoutModelv2()
        {
            //Billing
            ExistingBillingAddresses = new List<AddressModel>();
            NewBillingAddress = new AddressModel();

            //Shipping
            ExistingShippingAddresses = new List<AddressModel>();
            NewShippingAddress = new AddressModel();

            //Shipping Methods
            ShippingMethods = new List<ShippingMethodModel>();
            ShippingMethodWarnings = new List<string>();

            //CheckoutConfirm
            CheckoutConfirmWarnings = new List<string>();

            //Payment Mthod
            PaymentMethods = new List<PaymentMethodModel>();
        }

        //MVC is suppressing further validation if the IFormCollection is passed to a controller method.That's why we add to the model
        public IFormCollection Form { get; set; }

        public bool ShippingRequired { get; set; }
        public bool PaymentMethodsMoreThanOne { get; set; }
        public bool ShippingMethodsMoreThanOne { get; set; }

        //Billing
        public IList<AddressModel> ExistingBillingAddresses { get; set; }
        public int SelectedBillingAddressID { get; set; }
        public AddressModel NewBillingAddress { get; set; }
        public bool RewardPointsEnoughToPayForOrder { get; set; }
        public string PaymentViewComponentName { get; set; }

        public bool TermsOfServiceOnOrderConfirmPage { get; set; }
        public bool TermsOfServicePopup { get; set; }
        
        public bool NewBillingAddressPreselected { get; set; }
      
        //Shipping
        public IList<AddressModel> ExistingShippingAddresses { get; set; }
        public int SelectedShippingAddressID { get; set; }
        public AddressModel NewShippingAddress { get; set; }
        public bool NewShippingAddressPreselected { get; set; }
      
        //Shipping Mehod
        public IList<ShippingMethodModel> ShippingMethods { get; set; }
        public IList<string> ShippingMethodWarnings { get; set; }

        //Payment Method
        public IList<PaymentMethodModel> PaymentMethods { get; set; }
        public bool DisplayRewardPoints { get; set; }
        public int RewardPointsBalance { get; set; }
        public string RewardPointsAmount { get; set; }
        public bool UseRewardPoints { get; set; }

        //Payment Info
        public string PaymentInfoActionName { get; set; }
        public string PaymentInfoControllerName { get; set; }
        public bool DisplayOrderTotals { get; set; }

        //Checkout Confirm
        public string MinOrderTotalWarning { get; set; }
        public IList<string> CheckoutConfirmWarnings { get; set; }

        //CheckoutCompleted
        public int OrderId { get; set; }

        #region Nested classes

        //Shipping Method
        public class ShippingMethodModel : BaseNopModel
        {
            public string ShippingRateComputationMethodSystemName { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string Fee { get; set; }
            public bool Selected { get; set; }

            public ShippingOption ShippingOption { get; set; }
        }

        //Payment Method
        public class PaymentMethodModel : BaseNopModel
        {
            public string PaymentMethodSystemName { get; set; }
            public string Description { get; set; }
            public string Name { get; set; }
            public string Fee { get; set; }
            public bool Selected { get; set; }
            public string LogoUrl { get; set; }
        }

        //UpdateSectionJson
        public string name { get; set; }
        public string html { get; set; }

        #endregion


    }
}