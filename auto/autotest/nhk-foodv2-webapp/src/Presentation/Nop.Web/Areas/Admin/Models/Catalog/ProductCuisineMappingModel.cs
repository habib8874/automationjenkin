﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Areas.Admin.Validators.Catalog;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    public partial class ProductCuisineMappingSearchModel : BaseSearchModel
    {
        public ProductCuisineMappingSearchModel()
        {
            AvailableStoreList = new List<SelectListItem>();
        }

        public IList<SelectListItem> AvailableStoreList { get; set; }
        [NopResourceDisplayName("Admin.Catalog.BulkEdit.List.SearchStores")]
        public string SearchStores { get; set; }

        [NopResourceDisplayName("Admin.Catalog.ProductTagsImageMapping.Fields.StoreId")]
        public int StoreId { get; set; }
    }
    public partial class ProductCuisineMappingListModel : BasePagedListModel<ProductCuisineMappingModel>
    {
    }
    public partial class ProductCuisineMappingModel : BaseNopEntityModel
    {
        public ProductCuisineMappingModel()
        {
           this.AvailableStoreList = new List<SelectListItem>();
        }
        public IList<SelectListItem> AvailableStoreList { get; set; }
    
        [NopResourceDisplayName("Admin.Catalog.ProductTagsImageMapping.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Catalog.ProductTagsImageMapping.Fields.Id")]
        public int Id { get; set; }

        [NopResourceDisplayName("Admin.Catalog.BulkEdit.List.SearchStores")]
        public string SearchStores { get; set; }

        [NopResourceDisplayName("Admin.Catalog.ProductTagsImageMapping.Fields.StoreId")]
        public int StoreId { get; set; }
        [NopResourceDisplayName("Admin.Catalog.ProductTagsImageMapping.Fields.StoreName")]
        public string StoreName { get; set; }
    }
    //public partial class ProductCuisineMappingLocalizedModel : ILocalizedModelLocal
    //{
    //    public int LanguageId { get; set; }

    //    [NopResourceDisplayName("Admin.Catalog.ProductTagsImageMapping.Fields.LanguageName")]
    //    public string Name { get; set; }
    //}
}
