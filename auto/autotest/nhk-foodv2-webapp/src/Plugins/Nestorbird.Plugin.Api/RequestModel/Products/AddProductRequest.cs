﻿using System.Collections.Generic;

namespace Nestorbird.Plugin.Api.RequestModel.Products
{
    public class AddProductRequest
    {
        public AddProductRequest()
        {
            ProductImages = new List<ProductImageRequest>();
            RemoveImageIds = new List<int>();
        }

        public int VendorId { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public string ShortDescription { get; set; }

        public string FullDescription { get; set; }

        public IList<int> CategoryIds { get; set; }

        public decimal Price { get; set; }

        public decimal OldPrice { get; set; }

        public int DisplayOrder { get; set; }

        public bool Published { get; set; }

        public int? DiscountId { get; set; }

        public int TaxCategoryStateId { get; set; }

        public int TaxCategoryId { get; set; }

        public IList<ProductImageRequest> ProductImages { get; set; }

        public IList<int> RemoveImageIds { get; set; }
    }

    public class ProductImageRequest
    {
        public string Base64 { get; set; }

        public string Name { get; set; }
    }
}
