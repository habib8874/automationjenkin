﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nestorbird.Plugin.Api.RequestModel.Products;
using Nestorbird.Plugin.Api.ResponseModel;
using Nestorbird.Plugin.Api.ResponseModel.Products;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Discounts;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;

namespace Nestorbird.Plugin.Api.Controllers
{
    [Route("merchantapi/product")]
    public class MerchantProductController : BaseAPIController
    {
        #region Fields

        private readonly IProductService _productService;
        private readonly ILocalizationService _localizationService;
        private readonly IPictureService _pictureService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly ICategoryService _categoryService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly IVendorService _vendorService;
        private readonly IDiscountService _discountService;
        private readonly IStoreService _storeService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly ITaxService _taxService;
        private readonly ITaxCategoryService _taxCategoryService;
        private readonly ICustomerService _customerService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly ISpecificationAttributeService _specificationAttributeService;

        #endregion

        #region Fields

        public MerchantProductController(ILanguageService languageService,
            IProductService productService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            IPriceFormatter priceFormatter,
            ICategoryService categoryService,
            IUrlRecordService urlRecordService,
            ILocalizedEntityService localizedEntityService,
            IVendorService vendorService,
            IDiscountService discountService,
            IStoreService storeService,
            IStateProvinceService stateProvinceService,
            ITaxService taxService,
            ITaxCategoryService taxCategoryService,
            ICustomerService customerService,
            IProductAttributeService productAttributeService,
            ISpecificationAttributeService specificationAttributeService) : base(languageService)
        {
            _productService = productService;
            _localizationService = localizationService;
            _pictureService = pictureService;
            _priceFormatter = priceFormatter;
            _categoryService = categoryService;
            _urlRecordService = urlRecordService;
            _localizedEntityService = localizedEntityService;
            _vendorService = vendorService;
            _discountService = discountService;
            _storeService = storeService;
            _stateProvinceService = stateProvinceService;
            _taxService = taxService;
            _taxCategoryService = taxCategoryService;
            _customerService = customerService;
            _productAttributeService = productAttributeService;
            _specificationAttributeService = specificationAttributeService;
        }

        #endregion

        #region Methods

        private void SaveDiscountMappings(Product product, AddProductRequest request)
        {
            var allDiscounts = _discountService.GetAllDiscounts(DiscountType.AssignedToSkus, showHidden: true);
            foreach (var discount in allDiscounts)
            {
                if (request.DiscountId == discount.Id)
                {
                    //new discount
                    if (product.DiscountProductMappings.Count(mapping => mapping.DiscountId == discount.Id) == 0)
                        product.DiscountProductMappings.Add(new DiscountProductMapping { Discount = discount });
                }
                else
                {
                    //remove discount
                    if (product.DiscountProductMappings.Count(mapping => mapping.DiscountId == discount.Id) > 0)
                    {
                        product.DiscountProductMappings
                            .Remove(product.DiscountProductMappings.FirstOrDefault(mapping => mapping.DiscountId == discount.Id));
                    }
                }
            }

            _productService.UpdateProduct(product);
            _productService.UpdateHasDiscountsApplied(product);
        }

        private void SaveCategoryMappings(Product product, AddProductRequest request)
        {
            var existingProductCategories = _categoryService.GetProductCategoriesByProductId(product.Id, true);

            //delete categories
            foreach (var existingProductCategory in existingProductCategories)
                if (!request.CategoryIds.Contains(existingProductCategory.CategoryId))
                    _categoryService.DeleteProductCategory(existingProductCategory);

            //add categories
            foreach (var categoryId in request.CategoryIds)
            {
                if (_categoryService.FindProductCategory(existingProductCategories, product.Id, categoryId) == null)
                {
                    //find next display order
                    var displayOrder = 1;
                    var existingCategoryMapping = _categoryService.GetProductCategoriesByCategoryId(categoryId, showHidden: true);
                    if (existingCategoryMapping.Any())
                        displayOrder = existingCategoryMapping.Max(x => x.DisplayOrder) + 1;

                    _categoryService.InsertProductCategory(new ProductCategory
                    {
                        ProductId = product.Id,
                        CategoryId = categoryId,
                        DisplayOrder = displayOrder
                    });
                }
            }
        }

        [HttpPost]
        [Route("getproducts")]
        [ProducesResponseType(typeof(ProductListResponse), StatusCodes.Status200OK)]
        public virtual IActionResult GetProducts(ProductListRequest request)
        {
            if (request == null)
                return Error("Please enter proper request");

            if (request.VendorId <= 0)
                return Error("Please enter proper vendor id");

            if (request.PageSize <= 0)
                request.PageSize = int.MaxValue;

            var searchCategoryIds = request.CategoryIds.Count(x => x != 0) > 0 ? request.CategoryIds : null;
            var products = _productService.SearchProducts(pageIndex: request.PageIndex, pageSize: request.PageSize, categoryIds: searchCategoryIds,
                                                          vendorId: request.VendorId);
            var data = products.Select(product =>
            {
                var productImageId = product.ProductPictures.FirstOrDefault()?.PictureId ?? 0;
                return new ProductItemResponse
                {
                    Name = _localizationService.GetLocalized(product, x => x.Name, CurrentLanguageId),
                    ShortDescription = _localizationService.GetLocalized(product, x => x.ShortDescription, CurrentLanguageId),
                    FullDescription = _localizationService.GetLocalized(product, x => x.FullDescription, CurrentLanguageId),
                    Id = product.Id,
                    ImageUrl = _pictureService.GetPictureUrl(productImageId, 100),
                    Price = _priceFormatter.FormatPrice(product.Price, true, false),
                    StockQuantity = product.StockQuantity
                };
            }).ToList();

            var response = new ProductListResponse
            {
                Products = data,
                PageIndex = products.PageIndex,
                PageSize = products.PageSize,
                TotalCount = products.TotalCount,
                TotalPages = products.TotalPages
            };

            return Success(response);
        }

        [HttpPost]
        [Route("deleteproducts")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        public virtual IActionResult DeleteProducts([FromBody]IList<int> productIds)
        {
            if (productIds.Count <= 0)
                return Error("Please enter product ids");

            foreach (var id in productIds)
            {
                var product = _productService.GetProductById(id);
                if (product != null)
                {
                    _productService.DeleteProduct(product);
                }
            }

            return Success("", "Products deleted successfully");
        }

        [HttpGet]
        [Route("getproductdetails")]
        [ProducesResponseType(typeof(ProductDetailResponse), StatusCodes.Status200OK)]
        public virtual IActionResult GetProductDetails(int id, int vendorId)
        {
            if (id <= 0)
                return Error("Please provide product id");

            var product = _productService.GetProductById(id);
            if (product == null)
                return Error("Please provide proper product id");

            var response = new ProductDetailResponse
            {
                Id = product.Id,
                CategoryIds = product.ProductCategories.Select(x => x.CategoryId).ToList(),
                Name = product.Name,
                DiscountId = product.DiscountProductMappings.FirstOrDefault()?.DiscountId,
                DisplayOrder = product.DisplayOrder,
                FullDescription = product.FullDescription,
                ShortDescription = product.ShortDescription,
                Price = product.Price,
                OldPrice = product.OldPrice,
                Published = product.Published,
                TaxCategoryId = product.TaxCategoryId,
                TaxCategoryStateId = product.TaxCategoryStateId,
                CrossSellProductIds = _productService.GetCrossSellProductsByProductId1(product.Id).Select(x => x.ProductId2).ToList()
            };

            var productImages = product.ProductPictures;
            foreach (var picture in productImages)
            {
                var productImageModel = new ProductImageModel
                {
                    ImageUrl = _pictureService.GetPictureUrl(picture.PictureId, 500),
                    ImageId = picture.Id
                };
                response.Images.Add(productImageModel);
            }

            var productAttributes = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id);
            foreach (var attribute in productAttributes)
            {
                var attributedetail = new ProductAttributeDetailResponse
                {
                    Id = attribute.Id,
                    Name = attribute.ProductAttribute.Name,
                    AttributeId = attribute.ProductAttributeId
                };

                var productAttributeValues = _productAttributeService.GetProductAttributeValues(attribute.Id);
                foreach (var value in productAttributeValues)
                {
                    var valueDetail = new ProductAttributeValueDetailResponse
                    {
                        Id = value.Id,
                        ImageUrl = value.PictureId > 0 ? _pictureService.GetPictureUrl(value.PictureId, 75, false) : "",
                        Name = value.Name
                    };

                    attributedetail.Values.Add(valueDetail);
                }

                response.ProductAttributes.Add(attributedetail);
            }

            var specificationAttributes = _specificationAttributeService.GetProductSpecificationAttributes(product.Id);
            foreach (var attribute in specificationAttributes)
            {
                var attributedetail = new SpecificationAttributeDetailResponse
                {
                    Id = attribute.Id,
                    Name = attribute.SpecificationAttributeOption?.SpecificationAttribute.Name,
                    SpecificationAttributeId = attribute.SpecificationAttributeOption?.SpecificationAttributeId,
                    Text = attribute.SpecificationAttributeOption == null ? attribute.CustomValue : attribute.SpecificationAttributeOption.Name
                };
                response.SpecificationAttributes.Add(attributedetail);
            }

            return Success(response);
        }

        [HttpPost]
        [Route("addcrosssellproduct")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        public virtual IActionResult AddCrossSellProducts(AddCrossSellRequest request)
        {
            if (request == null)
                return Error("Please provide proper request");

            if (request.ProductId <= 0)
                return Error("Please provide product id");

            if (!request.CrossSellProductIds.Any())
                return Error("Please provide cross sell product ids");

            foreach (var item in request.CrossSellProductIds)
            {
                if (item > 0)
                {
                    _productService.InsertCrossSellProduct(new Nop.Core.Domain.Catalog.CrossSellProduct
                    {
                        ProductId1 = request.ProductId,
                        ProductId2 = item
                    });
                }
            }

            return Success("", "Cross sell products added successfully");
        }

        [HttpPost]
        [Route("addproduct")]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        public virtual IActionResult InsertProduct(AddProductRequest request)
        {
            if (request == null)
                return Error("Please provide proper request");

            if (string.IsNullOrEmpty(request.Name))
                return Error("Please enter product name");

            var product = new Product
            {
                Name = request.Name,
                DisplayOrder = request.DisplayOrder,
                FullDescription = request.FullDescription,
                ShortDescription = request.ShortDescription,
                Price = request.Price,
                OldPrice = request.OldPrice,
                Published = request.Published,
                TaxCategoryId = request.TaxCategoryId,
                TaxCategoryStateId = request.TaxCategoryStateId,
                CreatedOnUtc = DateTime.UtcNow,
                UpdatedOnUtc = DateTime.UtcNow,
                VisibleIndividually = true,
                OrderMaximumQuantity = 10000,
                VendorId = request.VendorId
            };
            _productService.InsertProduct(product);

            //search engine name
            var seName = _urlRecordService.ValidateSeName(product, request.Name, product.Name, true);
            _urlRecordService.SaveSlug(product, request.Name, 0);

            // add locale
            _localizedEntityService.SaveLocalizedValue(product, x => x.Name, request.Name, CurrentLanguageId);

            // add store
            var vendorStoreId = _vendorService.GetVendorById(request.VendorId)?.StoreId ?? 0;
            _productService.UpdateProductStoreMappings(product, new List<int> { vendorStoreId });

            //discounts
            SaveDiscountMappings(product, request);

            // add categories
            SaveCategoryMappings(product, request);

            if (request.ProductImages?.Count > 0)
            {
                foreach (var item in request.ProductImages)
                {
                    var pictureId = SavePicture(item.Base64, product.Id + item.Name);
                    _productService.InsertProductPicture(new ProductPicture
                    {
                        PictureId = pictureId,
                        ProductId = product.Id,
                        DisplayOrder = 0
                    });
                }
            }

            return Success(product.Id, "Product created successfully");
        }

        [HttpPost]
        [Route("updateproduct")]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        public virtual IActionResult UpdateProduct(AddProductRequest request)
        {
            if (request == null)
                return Error("Please provide proper request");

            if (request.Id <= 0)
                return Error("Please provide product id");

            if (string.IsNullOrEmpty(request.Name))
                return Error("Please enter product name");

            var product = _productService.GetProductById(request.Id);
            if (product == null)
                return Error("Please provide proper product id");

            product.Name = request.Name;
            product.DisplayOrder = request.DisplayOrder;
            product.FullDescription = request.FullDescription;
            product.ShortDescription = request.ShortDescription;
            product.Price = request.Price;
            product.OldPrice = request.OldPrice;
            product.Published = request.Published;
            product.TaxCategoryId = request.TaxCategoryId;
            product.TaxCategoryStateId = request.TaxCategoryStateId;
            product.UpdatedOnUtc = DateTime.UtcNow;
            _productService.UpdateProduct(product);

            //search engine name
            var seName = _urlRecordService.ValidateSeName(product, request.Name, product.Name, true);
            _urlRecordService.SaveSlug(product, request.Name, 0);

            // add locale
            _localizedEntityService.SaveLocalizedValue(product, x => x.Name, request.Name, CurrentLanguageId);

            //discounts
            SaveDiscountMappings(product, request);

            // add categories
            SaveCategoryMappings(product, request);

            if (request.RemoveImageIds?.Count > 0)
            {
                foreach (var item in request.RemoveImageIds)
                {
                    if (item > 0)
                    {
                        var productPicture = _productService.GetProductPictureById(item);
                        if (productPicture != null)
                            _productService.DeleteProductPicture(productPicture);
                    }
                }
            }

            if (request.ProductImages?.Count > 0)
            {
                foreach (var item in request.ProductImages)
                {
                    var pictureId = SavePicture(item.Base64, product.Id + item.Name);
                    _productService.InsertProductPicture(new ProductPicture
                    {
                        PictureId = pictureId,
                        ProductId = product.Id,
                        DisplayOrder = 0
                    });
                }
            }

            return Success(product.Id, "Product created successfully");
        }

        [HttpGet]
        [Route("gettaxstates")]
        [ProducesResponseType(typeof(IList<ListItemResponse>), StatusCodes.Status200OK)]
        public virtual IActionResult GetTaxStates(int vendorId)
        {
            var stateList = new List<ListItemResponse>();
            var vendor = _vendorService.GetVendorById(vendorId);
            var store = _storeService.GetStoreById(vendor?.StoreId ?? 0, false);
            var states = _stateProvinceService.GetStateProvincesByCountryId(store?.CountryId ?? 0);
            foreach (var s in states)
            {
                stateList.Add(new ListItemResponse { Value = s.Id.ToString(), Text = s.Name });
            }

            return Success(stateList);
        }

        [HttpGet]
        [Route("gettaxcategories")]
        [ProducesResponseType(typeof(IList<ListItemResponse>), StatusCodes.Status200OK)]
        public virtual IActionResult GetTaxCategories(int stateId, int vendorId)
        {
            var customer = _customerService.GetCustomerByVendorId(vendorId);
            if (customer == null)
                return Error("Please provide valid vendor id");

            var vendor = _vendorService.GetVendorById(vendorId);
            if (vendor == null)
                return Error("Please provide valid vendor id");

            var categoryList = new List<ListItemResponse>();
            var categoryIds = _taxService.GetTaxCategoryIds(customer, vendor.StoreId, stateId);
            var availableTaxCategories = _taxCategoryService.GetAllTaxCategories();
            availableTaxCategories = availableTaxCategories.Where(x => categoryIds.Contains(x.Id)).ToList();
            foreach (var taxCategory in availableTaxCategories)
            {
                categoryList.Add(new ListItemResponse { Value = taxCategory.Id.ToString(), Text = taxCategory.Name });
            }

            return Success(categoryList);
        }

        [HttpGet]
        [Route("getdiscounts")]
        [ProducesResponseType(typeof(IList<ListItemResponse>), StatusCodes.Status200OK)]
        public virtual IActionResult GetDiscounts(int vendorId)
        {
            var vendor = _vendorService.GetVendorById(vendorId);
            if (vendor == null)
                return Error("Please provide valid vendor id");

            var availableDiscounts = _discountService.GetAllDiscounts(DiscountType.AssignedToSkus, showHidden: true, storeId: vendor.StoreId);
            var response = availableDiscounts.Select(x =>
            {
                return new ListItemResponse
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                };
            }).ToList();

            return Success(response);
        }

        [HttpGet]
        [Route("getattributecontroltypes")]
        [ProducesResponseType(typeof(IList<ListItemResponse>), StatusCodes.Status200OK)]
        public virtual IActionResult GetControlTypes()
        {
            var response = Enum.GetValues(typeof(AttributeControlType)).Cast<AttributeControlType>().Select(x =>
            {
                return new ListItemResponse
                {
                    Value = ((int)x).ToString(),
                    Text = _localizationService.GetLocalizedEnum(x)
                };
            }).ToList();

            return Success(response);
        }

        [HttpGet]
        [Route("getproductattributes")]
        [ProducesResponseType(typeof(IList<ProductAttributeResponse>), StatusCodes.Status200OK)]
        public virtual IActionResult GetProductAttributes(int vendorId = 0, int productId = 0)
        {
            var storeId = 0;
            if (vendorId > 0)
            {
                var vendor = _vendorService.GetVendorById(vendorId);
                if (vendor == null)
                    return Error("Please provide valid vendor id");

                storeId = vendor.StoreId;
            }

            var availableProductAttributes = _productAttributeService.GetAllProductAttributes(StoreId: storeId,
                VendorId: vendorId).Select(productAttribute =>
                {
                    var productAttributeValueList = new List<ProductAttributeValueResponse>();
                    if (productId > 0)
                    {
                        var productAttributeMapping = _productAttributeService.GetProductAttributeMappingsByProductId(productId).FirstOrDefault(x => x.ProductAttributeId == productAttribute.Id);
                        if (productAttributeMapping != null)
                        {
                            var productAttributeValues = _productAttributeService.GetProductAttributeValues(productAttributeMapping.Id);
                            foreach (var item in productAttributeValues)
                            {
                                productAttributeValueList.Add(new ProductAttributeValueResponse
                                {
                                    Id = item.Id,
                                    Name = item.Name,
                                    Price = _priceFormatter.FormatPrice(item.PriceAdjustment, true, false),
                                    ImageUrl = item.PictureId > 0 ? _pictureService.GetPictureUrl(item.PictureId, 75, false) : ""
                                });
                            }
                        }
                    }
                    else
                    {
                        var preDefinedproductAttributeValues = _productAttributeService.GetPredefinedProductAttributeValues(productAttribute.Id);
                        foreach (var item in preDefinedproductAttributeValues)
                        {
                            productAttributeValueList.Add(new ProductAttributeValueResponse
                            {
                                Id = item.Id,
                                Name = item.Name,
                                Price = _priceFormatter.FormatPrice(item.PriceAdjustment, true, false),
                            });
                        }
                    }

                    return new ProductAttributeResponse
                    {
                        Text = productAttribute.Name,
                        Value = productAttribute.Id.ToString(),
                        ProductAttributeValues = productAttributeValueList
                    };
                }).ToList();

            return Success(availableProductAttributes);
        }

        [HttpPost]
        [Route("addproductattribute")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        public virtual IActionResult AddProductAttribute(AddProductAttributeRequest request)
        {
            if (request == null)
                return Error("Please provide proper request");

            if (request.ProductId <= 0)
                return Error("Please provide product id");

            if (request.ProductAttributeId <= 0)
                return Error("Please provide product attribute id");

            var product = _productService.GetProductById(request.ProductId);
            if (product == null)
                return Error("Please provide proper product id");

            var productAttribute = _productAttributeService.GetProductAttributeById(request.ProductAttributeId);
            if (productAttribute == null)
                return Error("Please provide proper product attribute id");

            if (_productAttributeService.GetProductAttributeMappingsByProductId(product.Id)
                .Any(x => x.ProductAttributeId == request.ProductAttributeId))
            {
                return Error(_localizationService.GetResource("Admin.Catalog.Products.ProductAttributes.Attributes.AlreadyExists"));
            }

            var productAttributeMapping = new ProductAttributeMapping
            {
                AttributeControlTypeId = request.ControlType,
                ProductAttributeId = request.ProductAttributeId,
                ProductId = request.ProductId,
                IsRequired = request.IsRequired,
                TextPrompt = request.Name
            };
            _productAttributeService.InsertProductAttributeMapping(productAttributeMapping);

            var predefinedValues = _productAttributeService.GetPredefinedProductAttributeValues(request.ProductAttributeId);
            foreach (var predefinedValue in predefinedValues)
            {
                var pav = new ProductAttributeValue
                {
                    ProductAttributeMappingId = productAttributeMapping.Id,
                    AttributeValueType = AttributeValueType.Simple,
                    Name = predefinedValue.Name,
                    PriceAdjustment = predefinedValue.PriceAdjustment,
                    PriceAdjustmentUsePercentage = predefinedValue.PriceAdjustmentUsePercentage,
                    WeightAdjustment = predefinedValue.WeightAdjustment,
                    Cost = predefinedValue.Cost,
                    IsPreSelected = predefinedValue.IsPreSelected,
                    DisplayOrder = predefinedValue.DisplayOrder
                };
                _productAttributeService.InsertProductAttributeValue(pav);

                //locales
                var languages = _languageService.GetAllLanguages(true);

                //localization
                foreach (var lang in languages)
                {
                    var name = _localizationService.GetLocalized(predefinedValue, x => x.Name, lang.Id, false, false);
                    if (!string.IsNullOrEmpty(name))
                        _localizedEntityService.SaveLocalizedValue(pav, x => x.Name, name, lang.Id);
                }
            }

            return Success(productAttributeMapping.Id, "Product attribute added successfully");
        }

        [HttpPost]
        [Route("addproductattributevalue")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        public virtual IActionResult AddProductAttributeValue(AddProductAttributeValueRequest request)
        {
            if (request == null)
                return Error("Please provide proper request");

            if (request.ProductId <= 0)
                return Error("Please provide product id");

            if (request.ProductAttributeId <= 0)
                return Error("Please provide product attribute id");

            if (string.IsNullOrEmpty(request.Name))
                return Error("Please provide name");

            var productAttributeMappings = _productAttributeService.GetProductAttributeMappingsByProductId(request.ProductId);
            var mapping = productAttributeMappings.FirstOrDefault(x => x.ProductAttributeId == request.ProductAttributeId);
            if (mapping != null)
            {
                _productAttributeService.InsertProductAttributeValue(new ProductAttributeValue
                {
                    AssociatedProductId = request.ProductId,
                    IsPreSelected = request.IsPreSelected,
                    Name = request.Name,
                    PriceAdjustment = request.Price,
                    AttributeValueType = AttributeValueType.Simple,
                    ProductAttributeMappingId = mapping.Id
                });
            }

            return Success("", "Attribute value added successfully");
        }

        [HttpPost]
        [Route("removeproductattributevalues")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        public virtual IActionResult RemoveProductAttributeValues(IList<int> productAttributeValueIds)
        {
            if (productAttributeValueIds?.Count <= 0)
                return Error("Please provide ids");

            foreach (var item in productAttributeValueIds)
            {
                var productAttributeValue = _productAttributeService.GetProductAttributeValueById(item);
                if (productAttributeValue == null)
                    continue;

                var productAttributeMapping = _productAttributeService.GetProductAttributeMappingById(productAttributeValue.ProductAttributeMappingId);
                if (productAttributeMapping == null)
                    continue;

                _productAttributeService.DeleteProductAttributeValue(productAttributeValue);
            }

            return Success("", "Values removed successfully");
        }

        [HttpPost]
        [Route("removeproductattributemapping")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        public virtual IActionResult RemoveProductAttributeMapping(int id)
        {
            var productAttributeMapping = _productAttributeService.GetProductAttributeMappingById(id);
            if (productAttributeMapping == null)
                return Error("No product attribute mapping found with the specified id");

            var product = _productService.GetProductById(productAttributeMapping.ProductId);
            if (product == null)
                return Error("No product found with the specified id");

            _productAttributeService.DeleteProductAttributeMapping(productAttributeMapping);

            return Success("", _localizationService.GetResource("Admin.Catalog.Products.ProductAttributes.Attributes.Deleted"));
        }

        [HttpGet]
        [Route("getspecificationattributes")]
        [ProducesResponseType(typeof(IList<SpecificationAttributeResponse>), StatusCodes.Status200OK)]
        public virtual IActionResult GetSpecificationAttributes()
        {
            var specificationAttributes = _specificationAttributeService.GetSpecificationAttributesWithOptions();
            var response = specificationAttributes.Select(attribute =>
            {
                return new SpecificationAttributeResponse
                {
                    Id = attribute.Id,
                    Name = attribute.Name,
                    SpecificationAttributeValues = attribute.SpecificationAttributeOptions.Select(option =>
                    {
                        return new SpecificationAttributeValueResponse
                        {
                            Id = option.Id,
                            Name = option.Name
                        };
                    }).ToList()
                };
            }).ToList();

            return Success(response);
        }

        [HttpPost]
        [Route("addspecificationattribute")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        public virtual IActionResult AddSpecificationAttribute(AddSpecificationAttributeRequest request)
        {
            if (request == null)
                return Error("Please provide proper request");

            if (request.ProductId <= 0)
                return Error("Please provide product id");

            if (request.SpecificationAttributeId <= 0)
                return Error("Please provide specification attribute id");

            var product = _productService.GetProductById(request.ProductId);
            if (product == null)
                return Error("Please provide proper product id");

            var specificationAttribute = _specificationAttributeService.GetSpecificationAttributeById(request.SpecificationAttributeId);
            if (product == null)
                return Error("Please provide proper specification attribute id");

            var productSpecifictionAttribute = new ProductSpecificationAttribute
            {
                AttributeTypeId = request.AttributeTypeId,
                CustomValue = request.CustomValue,
                ProductId = request.ProductId,
                ShowOnProductPage = request.ShowOnProductPage,
                SpecificationAttributeOptionId = request.OptionId
            };
            _specificationAttributeService.InsertProductSpecificationAttribute(productSpecifictionAttribute);

            return Success(productSpecifictionAttribute.Id, "");

        }

        [HttpPost]
        [Route("deletespecificationattribute")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        public virtual IActionResult DeleteSpecificationAttribute(int id)
        {
            var productSpecificationAttribute = _specificationAttributeService.GetProductSpecificationAttributeById(id);
            if (productSpecificationAttribute == null)
                return Error("Please provide proper id");

            _specificationAttributeService.DeleteProductSpecificationAttribute(productSpecificationAttribute);

            return Success("", "You have successfully removed product specification attribute");
        }

        #endregion
    }
}
