﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Nop.Services.Localization;
using Nop.Services.NB.API;
using Nop.Services.NB.SignalR;

namespace Nop.Web.Controllers.NB
{
    [Route("api/[controller]")]
    [ApiController]
    public class NBCommonAPIController : ControllerBase
    {
        #region Fields

        private readonly IHubContext<SignalRHub> _signalRHubContext;
        private readonly ILocalizationService _localizationService;
        private readonly IVersionInfoServices _versionInfoServices;

        #endregion

        #region Ctor

        public NBCommonAPIController(IHubContext<SignalRHub> signalRHubContext,
            ILocalizationService localizationService,
            IVersionInfoServices versionInfoServices)
        {
            _signalRHubContext = signalRHubContext;
            _localizationService = localizationService;
            _versionInfoServices = versionInfoServices;
        }

        #endregion

        #region Utitlies

        public class CommonAPIResponses
        {
            public int StatusCode { get; set; }
            public string ErrorMessageTitle { get; set; }
            public string ErrorMessage { get; set; }
            public bool Status { get; set; }
            public object ResponseObj { get; set; }

        }

        public class AgentLocationRequestModel : LatLong
        {
            public string ApiKey { get; set; }
            public int StoreId { get; set; }
            public int OrderId { get; set; }
            public int AgentId { get; set; }
        }

        public class LatLong
        {
            public string Latitude { get; set; }
            public string Longitude { get; set; }
        }

        #endregion

        [Route("~/api/v1/InvokeMoveRiderSignalR")]
        [HttpPost]
        public CommonAPIResponses InvokeMoveRiderSignalR([FromBody] AgentLocationRequestModel agentLocationModel)
        {
            //Note: We will using hardcoded error message string because we want to make minimal sql call to improve performance 
            CommonAPIResponses customerAPIResponses = new CommonAPIResponses();
            if (agentLocationModel == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Model null";
                customerAPIResponses.ErrorMessage = "Model null";
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (agentLocationModel.AgentId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Agent Id Not Defined";
                customerAPIResponses.ErrorMessage = "Agent Id Not Defined";
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (agentLocationModel.OrderId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Order Id Not Defined";
                customerAPIResponses.ErrorMessage = "Order Id Not Defined";
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (agentLocationModel.StoreId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Store Id Not Defined";
                customerAPIResponses.ErrorMessage = "Store Id Not Defined";
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(agentLocationModel.Latitude))
            {
                customerAPIResponses.ErrorMessageTitle = "Latitude Not Defined";
                customerAPIResponses.ErrorMessage = "Latitude Not Defined";
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(agentLocationModel.Longitude))
            {
                customerAPIResponses.ErrorMessageTitle = "Longitude Not Defined";
                customerAPIResponses.ErrorMessage = "Longitude Not Defined";
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(agentLocationModel.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = "Api key Not Defined";
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (!string.IsNullOrEmpty(agentLocationModel.ApiKey))
            {
                //check Authentication
                if (!_versionInfoServices.IsValidAPIkey(agentLocationModel.ApiKey))
                {
                    customerAPIResponses.ErrorMessageTitle = "API key Error!!";
                    customerAPIResponses.ErrorMessage = "Api key not authenticate";
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
            }
            try
            {
                //call signalR
                _signalRHubContext.Clients.All.SendAsync("agentCoordinateRequest", agentLocationModel.OrderId, agentLocationModel.AgentId, agentLocationModel.Latitude, agentLocationModel.Longitude);

                customerAPIResponses.ResponseObj = "Agent SignalR invoked succesfully";
                customerAPIResponses.Status = true;
                return customerAPIResponses;
            }
            catch (Exception ex)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = 400;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
        }
    }
}
