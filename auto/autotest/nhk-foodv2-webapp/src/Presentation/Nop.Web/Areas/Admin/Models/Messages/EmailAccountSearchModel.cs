﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Messages
{
    /// <summary>
    /// Represents an email account search model
    /// </summary>
    public partial class EmailAccountSearchModel : BaseSearchModel
    {
        public EmailAccountSearchModel()
        {
            AvailableStores = new List<SelectListItem>();
            LimitedtoStores = new List<int>();

        }
        [NopResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.LimitedtoStores")]
        public IList<int> LimitedtoStores { get; set; }

        public IList<SelectListItem> AvailableStores { get; set; } // custom by Mohini for email

        [NopResourceDisplayName("Admin.Catalog.BulkEdit.List.SearchStores")]
        public int SearchStores { get; set; }
    }
}