﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class RestaurantNcookImages
    {
        public int Id { get; set; }
        public int? Image { get; set; }
        public int RestNcookId { get; set; }
        public string ImageCaption { get; set; }
    }
}
