﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.NB.Merchant;
using Nop.Services;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.NB.Merchant;
using Nop.Services.NB.MerchantCategorys;
using Nop.Services.Seo;
using Nop.Services.Vendors;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Customers;
using Nop.Web.Framework.Extensions;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Web.Areas.Admin.Factories.NB
{
    /// <summary>
    /// Represents the nb role model factory implementation
    /// </summary>
    public partial class NbRoleModelFactory : INbRoleModelFactory
    {
        #region Fields

        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ICustomerService _customerService;
        private readonly ILocalizationService _localizationService;
        private readonly IProductService _productService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IWorkContext _workContext;
        private readonly IMerchantCategoryService _merchantCategoryService;
        private readonly IVendorService _vendorService;
        private readonly IVendorCustomerRoleServices _vendorCustomerRoleServices;
        private readonly CatalogSettings _catalogSettings;

        #endregion

        #region Ctor

        public NbRoleModelFactory(IBaseAdminModelFactory baseAdminModelFactory,
            ICustomerService customerService,
            ILocalizationService localizationService,
            IProductService productService,
            IUrlRecordService urlRecordService,
            IWorkContext workContext,
            IMerchantCategoryService merchantCategoryService,
            IVendorService vendorService,
            IVendorCustomerRoleServices vendorCustomerRoleServices,
            CatalogSettings catalogSettings)
        {
            _baseAdminModelFactory = baseAdminModelFactory;
            _customerService = customerService;
            _localizationService = localizationService;
            _productService = productService;
            _urlRecordService = urlRecordService;
            _workContext = workContext;
            _merchantCategoryService = merchantCategoryService;
            _vendorService = vendorService;
            _vendorCustomerRoleServices = vendorCustomerRoleServices;
            _catalogSettings = catalogSettings;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Prepare available user groups
        /// </summary>
        /// <param name="items">items</param>
        protected virtual void PrepareUserGroups(IList<SelectListItem> items)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));

            //prepare available user groups
            var availableuserGroupsItems = UserGroups.Business.ToSelectList(false);
            foreach (var group in availableuserGroupsItems)
            {
                items.Add(group);
            }

            //insert this default item at first
            items.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
        }

        /// <summary>
        /// Prepare available merchants
        /// </summary>
        /// <param name="items">items</param>
        protected virtual void PrepareMerchants(IList<SelectListItem> items, int storeId = 0)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));

            //prepare available merchants for current store.
            var vendors = _vendorService.GetAllVendors(storeId: storeId);
            //var merchantCategorys = _merchantCategoryService.GetAllMerchantCategory(_workContext.GetCurrentStoreId);

            foreach (var vendor in vendors)
            {
                items.Add(new SelectListItem
                {
                    Text = vendor.Name,
                    Value = vendor.Id.ToString()
                });
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare nb role search model
        /// </summary>
        /// <param name="searchModel">nb role search model</param>
        /// <returns>nb role search model</returns>
        public virtual CustomerRoleSearchModel PrepareNBRoleSearchModel(CustomerRoleSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare available nb roles
            _baseAdminModelFactory.PrepareNBCustomerRoles(searchModel.AvailableNBRoles);

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged nb role list model
        /// </summary>
        /// <param name="searchModel">nb role search model</param>
        /// <returns>nb role list model</returns>
        public virtual CustomerRoleListModel PrepareNBRoleListModel(CustomerRoleSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get customer roles
            var customerRoles = _customerService.GetAllNBCustomerRoles(createdByNb: true, id: searchModel.SearchNBRoleId, roleSystemName: searchModel.SearchNBRole, showHidden: true,
                                                                     pageIndex: searchModel.Page, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new CustomerRoleListModel().PrepareToGrid(searchModel, customerRoles, () =>
            {
                return customerRoles.Select(role =>
                {
                    //fill in model values from the entity
                    var customerRoleModel = role.ToModel<CustomerRoleModel>();

                    //fill in additional values (not existing in the entity)
                    customerRoleModel.PurchasedWithProductName = _productService.GetProductById(role.PurchasedWithProductId)?.Name;

                    return customerRoleModel;
                });
            });

            return model;
        }

        /// <summary>
        /// Prepare nb role model
        /// </summary>
        /// <param name="model">nb role model</param>
        /// <param name="customerRole">nb role</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>nb role model</returns>
        public virtual CustomerRoleModel PrepareNBRoleModel(CustomerRoleModel model, CustomerRole customerRole, bool excludeProperties = false)
        {
            if (customerRole != null)
            {
                //fill in model values from the entity
                model = model ?? customerRole.ToModel<CustomerRoleModel>();
                model.PurchasedWithProductName = _productService.GetProductById(customerRole.PurchasedWithProductId)?.Name;
                model.SelctedMerchantIds = _vendorCustomerRoleServices.GetAllMappingsByCustomerRoleId(customerRole.Id).Select(x => x.VendorId)?.ToList();
                model.SelectedStoreId = _vendorService.GetVendorById(model.SelctedMerchantIds.FirstOrDefault())?.StoreId ?? 0;
            }

            //set default values for the new model
            if (customerRole == null)
                model.Active = true;

            //prepare available user groups
            PrepareUserGroups(model.AvailableUserGroups);

            //prepare available stores
            _baseAdminModelFactory.PrepareStores(model.AvailableStores, false);

            model.HideStoresList = _catalogSettings.IgnoreStoreLimitations || model.AvailableStores.SelectionIsNotPossible();

            //prepare available merchants
            PrepareMerchants(model.AvailableMerchants, model.SelectedStoreId);

            //prepare available tax display types
            _baseAdminModelFactory.PrepareTaxDisplayTypes(model.TaxDisplayTypeValues, false);

            return model;
        }

        #endregion
    }
}
