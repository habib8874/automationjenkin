﻿namespace Nop.Core.Domain.CustomEntities
{
    public partial class CLS_StoreCountryStateMapping
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
    }
}
