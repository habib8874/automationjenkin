﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModel;
using NHKCustomerApplication.ViewModels;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;

namespace NHKCustomerApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LanguageController : ControllerBase
    {
        private ecuadordevContext dbcontext;
        public LanguageController(ecuadordevContext context)
        {
            dbcontext = context;
        }

        /// <summary>
        /// Get all store languages
        /// </summary>
        /// <param name="requestObj">AllStoreLanguagesReqModel</param>
        /// <returns>BaseAPIResponseModel</returns>
        [Route("~/api/v1/AllStoreLanguages")]
        [HttpPost]
        public BaseAPIResponseModel GetAllStoreLanguages(AllStoreLanguagesReqModel requestObj)
        {
            int languageId = LanguageHelper.GetIdByLangCode(requestObj.StoreId, requestObj.UniqueSeoCode, dbcontext);
            var respObj = new AllStoreLanguagesResponseModel();

            if (requestObj.CustomerId == 0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext));

            if (string.IsNullOrEmpty(requestObj.ApiKey))
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext));

            if (!string.IsNullOrEmpty(requestObj.ApiKey))
                if (!BaseMethodsHelper.CheckAPIKey(requestObj.ApiKey, requestObj.StoreId, dbcontext))
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext));
            try
            {
                var languageResponse = new AllStoreLanguagesModel
                {
                    DefaultLanguageCode = LanguageHelper.GetDefaultLangCode(requestObj.StoreId, requestObj.CustomerId, dbcontext),
                    AllLanguages = LanguageHelper.GetAllLanguagesForList(requestObj.StoreId, dbcontext)
                };

                return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.SuccessMessage", languageId, dbcontext), languageResponse);
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                return BaseMethodsHelper.ErrorResponse(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Get all local resource strings and update customer default language if UniqueSeoCode has value
        /// </summary>
        /// <param name="requestObj">LocaleStringResourceReqModel</param>
        /// <returns>BaseAPIResponseModel</returns>
        [Route("~/api/v1/LocaleStringResources")]
        [HttpPost]
        public BaseAPIResponseModel GetLocaleStringResources(LocaleStringResourceReqModel requestObj)
        {
            int languageId = LanguageHelper.GetIdByLangCode(requestObj.StoreId, requestObj.UniqueSeoCode, dbcontext);

            var respObj = new BaseAPIResponseModel();

            if (string.IsNullOrEmpty(requestObj.ApiKey))
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext));

            if (!string.IsNullOrEmpty(requestObj.ApiKey))
                if (!BaseMethodsHelper.CheckAPIKey(requestObj.ApiKey, requestObj.StoreId, dbcontext))
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext));

            try
            {
                string languageCode = LanguageHelper.GetDefaultLangCode(requestObj.StoreId, requestObj.CustomerId, dbcontext);
                if (!string.IsNullOrEmpty(requestObj.UniqueSeoCode))
                {
                    var currentLang = LanguageHelper.GetAllLanguages(requestObj.StoreId, requestObj.UniqueSeoCode, dbcontext);
                    if (!currentLang.Any())
                        return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.ErrorMesaage.SystemNotSupportLanguage", languageId, dbcontext));
                       
                    languageCode = requestObj.UniqueSeoCode;
                }
                using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
                {
                    var data = db.Query<LocaleStringResourceModel>("GetLocaleStringResources", new
                    {
                        requestObj.StoreId,
                        requestObj.CustomerId,
                        requestObj.PrefixValue,
                        UniqueSeoCode = languageCode
                    }, commandType: CommandType.StoredProcedure).ToList();

                return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.ErrorMesaage.StringFetchedSuccessfully", languageId, dbcontext), data);
                }
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                return BaseMethodsHelper.ErrorResponse(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Get contents from topics
        /// </summary>
        /// <param name="requestObj"></param>
        /// <returns></returns>
        [Route("~/api/v1/GetTopicContents")]
        [HttpPost]
        public BaseAPIResponseModel GetTopicContents(TopicReqModel requestObj)
        {
            int languageId = LanguageHelper.GetIdByLangCode(requestObj.StoreId, requestObj.UniqueSeoCode, dbcontext);
            var respObj = new BaseAPIResponseModel();
            if (!string.IsNullOrEmpty(requestObj.ApiKey))
                if (!BaseMethodsHelper.CheckAPIKey(requestObj.ApiKey, requestObj.StoreId, dbcontext))
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext));

            try
            {
                requestObj.TopicType = requestObj.TopicType?.ToUpper();
                var versionInfo = dbcontext.VersionInfo.FirstOrDefault(x => x.StoreId == requestObj.StoreId && x.Apikey == requestObj.ApiKey);
                var dataTopicDetails = LanguageHelper.GetTopicContent(versionInfo.GetHelpUrl, versionInfo.Privacy, versionInfo.SupportChatUrl, versionInfo.TermsCondition, requestObj.TopicType, languageId, requestObj.StoreId, dbcontext);
                return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.SuccessMessage", languageId, dbcontext), dataTopicDetails);

            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                return BaseMethodsHelper.ErrorResponse(ex.Message, HttpStatusCode.BadRequest);
            }
        }
    }
}
