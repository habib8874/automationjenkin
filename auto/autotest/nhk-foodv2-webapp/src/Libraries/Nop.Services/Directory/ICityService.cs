﻿using System.Collections.Generic;
using Nop.Core.Domain.Directory;

namespace Nop.Services.Directory
{
    /// <summary>
    /// State province service interface
    /// </summary>
    public partial interface ICityService
    {
        /// <summary>
        /// Deletes a state/province
        /// </summary>
        /// <param name="stateProvince">The state/province</param>
        void DeleteCity(NHK_City city);

        /// <summary>
        /// Gets a state/province
        /// </summary>
        /// <param name="stateProvinceId">The state/province identifier</param>
        /// <returns>State/province</returns>
        NHK_City GetCityById(int cityId);

        /// <summary>
        /// Gets a state/province by abbreviation
        /// </summary>
        /// <param name="abbreviation">The state/province abbreviation</param>
        /// <param name="countryId">Country identifier; pass null to load the state regardless of a country</param>
        /// <returns>State/province</returns>
        NHK_City GetCityByCode(string code, int? countryId = null);

        /// <summary>
        /// Gets a state/province collection by country identifier
        /// </summary>
        /// <param name="countryId">Country identifier</param>
        /// <param name="languageId">Language identifier. It's used to sort states by localized names (if specified); pass 0 to skip it</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>States</returns>
        IList<NHK_City> GetCityByStateId(int stateId, int languageId = 0, bool showHidden = false);

        /// <summary>
        /// Gets all states/provinces
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>States</returns>
        IList<NHK_City> GetCity(bool showHidden = false);

        /// <summary>
        /// Inserts a state/province
        /// </summary>
        /// <param name="stateProvince">State/province</param>
        void InsertCity(NHK_City city);

        /// <summary>
        /// Updates a state/province
        /// </summary>
        /// <param name="stateProvince">State/province</param>
        void UpdateCity(NHK_City city);

        #region StoreCountryStateMapping
        /// <summary>
        /// Inserts a StoreCountryStateMapping
        /// </summary>
        /// <param name="storeCountryStateMapping">State/province</param>
        void InsertStoreCountryStateMapping(StoreCountryStateMapping storeCountryStateMapping);

        /// <summary>
        /// Updates a StoreCountryStateMapping
        /// </summary>
        /// <param name="storeCountryStateMapping">State/province</param>
        void UpdateStoreCountryStateMapping(StoreCountryStateMapping storeCountryStateMapping);

        /// <summary>
        /// Gets a StoreCountryStateMapping collection by country identifier
        /// </summary>
        /// <param name="countryId">Country identifier</param>
        /// <param name="storeId">store identifier</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>States</returns>
        IList<StoreCountryStateMapping> GetStoreCountryStateMappingsByCountryId(int countryId, int storeId = 0, bool showHidden = false);

        /// <summary>
        /// Gets a StoreCountryStateMapping
        /// </summary>
        /// <param name="storeCountryStateMappingId">The StoreCountryStateMapping identifier</param>
        /// <returns>State/province</returns>
        StoreCountryStateMapping GetStoreCountryStateMappingById(int storeCountryStateMappingId);
        #endregion
    }
}
