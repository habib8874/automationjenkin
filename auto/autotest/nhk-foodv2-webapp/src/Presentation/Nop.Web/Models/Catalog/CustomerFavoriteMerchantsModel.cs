﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Common;

namespace Nop.Web.Models.Catalog
{
    public class CustomerFavoriteMerchantsModel : BaseNopModel
    {
        public CustomerFavoriteMerchantsModel()
        {
            Merchants = new List<MerchantModel>();
        }

        public IList<MerchantModel> Merchants { get; set; }
        public PagerModel PagerModel { get; set; }

        #region Nested class

        /// <summary>
        /// Class that has only page for route value. Used for (My Account) My Favorite merchant pagination
        /// </summary>
        public partial class CustomerFavoriteMerchantsRouteValues : IRouteValues
        {
            public int pageNumber { get; set; }
        }

        #endregion
    }
}
