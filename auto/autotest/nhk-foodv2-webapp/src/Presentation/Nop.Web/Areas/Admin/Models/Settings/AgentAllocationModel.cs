﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Settings
{
    public partial class AgentAllocationModel : BaseNopModel
    {
        public AgentAllocationModel()
            {
                AvailableAgent =  new List<SelectListItem>();
        }
        public int ActiveStoreScopeConfiguration { get; set; }
        public bool AgentAllocation_OverrideForStore { get; set; }
        public string AgentAllocation { get; set; }
        public IList<SelectListItem> AvailableAgent { get; set; }
    }
}