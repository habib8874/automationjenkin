﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.NB;
using Nop.Services.Events;

namespace Nop.Services.NB
{
    /// <summary>
    /// Order service
    /// </summary>
    public partial class WalletDetailsService : IWalletDetailsService
    {
        #region Fields

        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<WalletDetails> _walletRepository;
               
        #endregion

        #region Ctor

        public WalletDetailsService(IEventPublisher eventPublisher,
            IRepository<WalletDetails> walletRepository)
        {
            _eventPublisher = eventPublisher;
            _walletRepository = walletRepository;
        }

        public virtual void DeleteWallet(WalletDetails wallet)
        {
            if (wallet == null)
                throw new ArgumentNullException(nameof(wallet));

            _walletRepository.Delete(wallet);
            //event notification
            _eventPublisher.EntityDeleted(wallet);
        }

        public virtual WalletDetails GetWalletById(int walletId)
        {
            if (walletId == 0)
                return null;

            return _walletRepository.GetById(walletId);
        }

        public virtual Decimal GetWalletBalance(int customerId)
        {
            if (customerId == 0)
                throw new ArgumentOutOfRangeException(nameof(customerId));

            var query = from walletItem in _walletRepository.Table

                        where customerId == walletItem.CustomerId
                        orderby walletItem.TransactionOnUtc descending
                        select walletItem;
            var walletItems = query.ToList();
            decimal totalAmount = 0;
            totalAmount += walletItems.Where(z => z.Credited == true).Sum(x => x.Amount);
            totalAmount -= walletItems.Where(z => z.Credited == false).Sum(x => x.Amount);
            return totalAmount;
        }
        public virtual IList<WalletDetails> GetWalletsByCustomerId(int customerId)
        {
            if (customerId == 0)
                throw new ArgumentOutOfRangeException(nameof(customerId));

            var query = from walletItem in _walletRepository.Table
                       
                        where customerId == walletItem.CustomerId
                        orderby walletItem.TransactionOnUtc descending
                        select walletItem;
            var walletItems = query.ToList();
            return walletItems;
        }

        public virtual void InsertWallet(WalletDetails wallet)
        {
            if (wallet == null)
                throw new ArgumentNullException(nameof(wallet));

            _walletRepository.Insert(wallet);

            //event notification
            _eventPublisher.EntityInserted(wallet);
        }

        public virtual IPagedList<WalletDetails> SearchWallets(int customerId = 0, int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false)
        {
            if (customerId == 0)
                throw new ArgumentOutOfRangeException(nameof(customerId));

            var query = _walletRepository.Table;
            if (customerId > 0)
                query = query.Where(w => w.CustomerId == customerId);
            query = query.OrderByDescending(w => w.TransactionOnUtc);
            //var walletItems = query.ToList();
            return new PagedList<WalletDetails>(query, pageIndex, pageSize, getOnlyTotalCount);
        }

        public virtual void UpdateWallet(WalletDetails wallet)
        {
            if (wallet == null)
                throw new ArgumentNullException(nameof(wallet));

            _walletRepository.Update(wallet);

            //event notification
            _eventPublisher.EntityUpdated(wallet);
        }

        #endregion

    }
}