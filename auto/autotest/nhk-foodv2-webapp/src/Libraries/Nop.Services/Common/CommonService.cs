﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Data;
using Nop.Services.Directory;
using Nop.Services.Events;
using Nop.Core.Domain.CustomEntities;
using Nop.Core.Data.Extensions;
using Nop.Core.Domain.Vendors;
using Nop.Core.Domain.NB;
using Newtonsoft.Json;
using Nop.Services.Configuration;
using Nop.Core;
using System.Net;
using System.IO;
using Castle.Core.Logging;
using Nop.Core.Infrastructure;
using Microsoft.AspNetCore.Http;

namespace Nop.Services.Common
{
    /// <summary>
    /// CommonService
    /// </summary>
    public partial class CommonService : ICommonService
    {
        #region Fields

        private readonly IDataProvider _dataProvider;
        private readonly IDbContext _dbContext;
        private readonly ISettingService _settingService;

        #endregion

        #region Ctor

        public CommonService(IDataProvider dataProvider,
            IDbContext dbContext,
            ISettingService settingService)
        {
            _dataProvider = dataProvider;
            _dbContext = dbContext;
            _settingService = settingService;
        }

        #endregion

        #region Methods

        public virtual IList<CLS_StoreCountryStateMapping> GetStoreCountryStateMapping(int storeId)
        {
            var pStoreId = _dataProvider.GetInt32Parameter("StoreId", storeId);
            var listData = _dbContext.QueryFromSql<CLS_StoreCountryStateMapping>("GetStoreCountryStateMapping",
                pStoreId).ToList();
            return listData;
        }

        public virtual IList<CLS_ISDCode> GetISDCode(int storeId, bool getAll = false)
        {
            var pStoreId = _dataProvider.GetInt32Parameter("StoreId", storeId);
            var pGetAll = _dataProvider.GetBooleanParameter("GetAll", getAll);
            var listData = _dbContext.QueryFromSql<CLS_ISDCode>("GetISDCode",
                pStoreId, pGetAll).ToList();
            return listData;
        }

        public virtual IList<int> GetVendorByLongLat(string latlong)
        {
            var platlong = _dataProvider.GetStringParameter("latlong", latlong);
            var listData = _dbContext.QueryFromSql<CLS_Merchant>("GetVendorByLongLat",
                platlong).Select(x => x.Id).ToList();
            return listData;
        }

        public virtual IList<int> GetAllVendorByLongLat(string latlong, int restType)
        {
            var platlong = _dataProvider.GetStringParameter("latlong", latlong);
            var pRestType = _dataProvider.GetInt32Parameter("RestType", restType);
            var listData = _dbContext.QueryFromSql<CLS_Merchant>("GetAllVendorByLongLat",
                platlong, pRestType).Select(x => x.Id).ToList();
            return listData;
        }

        public virtual IList<int> GetAutoAssignAgent(int storeId, int queryType)
        {
            var pStoreId = _dataProvider.GetInt32Parameter("StoreId", storeId);
            var pQueryType = _dataProvider.GetInt32Parameter("QueryType", queryType);
            var listData = _dbContext.QueryFromSql<CLS_AutoAssignAgent>("GetAutoAssignAgent",
                pStoreId, pQueryType).Select(x => x.AgId).ToList();
            return listData;
        }

        public virtual int CheckGeoArea(string param1, string param2)
        {
            var pParam1 = _dataProvider.GetStringParameter("Param1", param1);
            var pParam2 = _dataProvider.GetStringParameter("Param2", param2);
            var result = _dbContext.QueryFromSql<CLS_CheckGeoArea>("CheckGeoArea",
                pParam1, pParam2).Select(x => x.Result).FirstOrDefault();

            return result;
        }

        public virtual CLS_AgentDetailsForNotification GetAgentDetailsForNotification(int agentId)
        {
            var pAgentId = _dataProvider.GetInt32Parameter("AgentId", agentId);
            var listData = _dbContext.QueryFromSql<CLS_AgentDetailsForNotification>("GetAgentDetailsForNotification", pAgentId).ToList();
            return listData.FirstOrDefault();
        }

        public virtual IList<int> GetMerchantCategorysIdsByLongLat(int currStoreId, int orderType, string latlong, bool skipLocationSearch)
        {
            var pcurrStoreId = _dataProvider.GetInt32Parameter("StoreId", currStoreId);
            var porderType = _dataProvider.GetInt32Parameter("RestType", orderType);
            var platlong = _dataProvider.GetStringParameter("latLongPos", latlong);
            var pskipLocationSearch = _dataProvider.GetBooleanParameter("skipLocationSearch", skipLocationSearch);
            var listData = _dbContext.QueryFromSql<CLS_Merchant>("NB_Web_GetMerchantCategorys",
                pcurrStoreId, porderType, platlong, pskipLocationSearch).Select(x => x.Id).Distinct().ToList();

            return listData;
        }

        public virtual IList<DeliverySlot> GetDeliverySlots(DateTime deliveryDateTime, string productIds, int merchantId, int currStoreId)
        {
            var pdeliveryDateTime = _dataProvider.GetDateTimeParameter("deliveryDateTime", deliveryDateTime);
            var pProductIds = _dataProvider.GetStringParameter("productIds", productIds);
            var pmerchantId = _dataProvider.GetInt32Parameter("merchantId", merchantId);
            var pcurrStoreId = _dataProvider.GetInt32Parameter("storeId", currStoreId);
            var listData = _dbContext.EntityFromSql<DeliverySlot>("NB_Web_GetDeliverySlotIds",
                pdeliveryDateTime, pProductIds, pmerchantId, pcurrStoreId).ToList();

            return listData;
        }

        /// <summary>
        /// Get setting ids by lat long.
        /// </summary>
        /// <param name="latlong"></param>
        /// <param name="storeId"></param>
        /// <returns></returns>
        public virtual IList<int> GetAllSliderPictureSettingIdsByLongLat(string latlong, int storeId)
        {
            var platlong = _dataProvider.GetStringParameter("latlong", latlong);
            var pStoreId = _dataProvider.GetInt32Parameter("StoreId", storeId);
            var listData = _dbContext.QueryFromSql<NB_Setting>("GetSliderPicturesByLongLat",
                platlong, pStoreId).Select(x => x.SettingId).ToList();
            return listData;
        }
        /// <summary>
        /// Clear Cache on Customer Side..
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="cacheKeyPrefix"></param>
        /// <returns></returns>
        #region Clear Cache
        public ResultModel ClearCache(int storeId, string cacheKeyPrefix)
        {
            ResultModel resultModel = new ResultModel();
            ClearCaches clearCaches = new ClearCaches();
            try
            {
                var strUrl = string.Empty;

                var customersetting = _settingService.GetSettingByKey<string>("setting.customer.notificationurl", "", storeId);
                if (!string.IsNullOrEmpty(customersetting))
                {
                    strUrl = customersetting;
                }
                if (string.IsNullOrEmpty(strUrl))
                {
                    throw new Exception("Customer Notification Key not Found");
                }
                var sendNotifcationURl = strUrl + "/api/v1/ClearCache";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(sendNotifcationURl);

                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                clearCaches.StoreId = storeId;
                clearCaches.CacheKeyPrefix = cacheKeyPrefix;

                string json = JsonConvert.SerializeObject(clearCaches);
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {


                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    resultModel = JsonConvert.DeserializeObject<ResultModel>(streamReader.ReadToEnd());
                }
            }
            catch (Exception ex)
            {
                // _loger.Error(Convert.ToString(HttpStatusCode.BadRequest),ex);
                resultModel.Result = false;
                resultModel.ResultMsg = ex.ToString();
            }
            return resultModel;
        }
        #endregion


        /// <summary>
        /// Clear Cache on Agent Side..
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="cacheKeyPrefix"></param>
        /// <returns></returns>
        #region Clear Cache
        public AgentResultModel ClearCacheAgent(int storeId, string cacheKeyPrefix)
        {
            AgentResultModel agentResultModel = new AgentResultModel();
            //ResultModel resultModel = new ResultModel();
            ClearCaches clearCaches = new ClearCaches();
            try
            {
                var strUrl = string.Empty;

                var customersetting = _settingService.GetSettingByKey<string>("setting.storesetup.notificationpushurl", "", storeId);
                if (!string.IsNullOrEmpty(customersetting))
                {
                    strUrl = customersetting;
                }
                if (string.IsNullOrEmpty(strUrl))
                {
                    throw new Exception("Customer Notification Key not Found");
                }
                var sendNotifcationURl = strUrl + "/api/v1/ClearCache";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(sendNotifcationURl);

                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                clearCaches.StoreId = storeId;
                clearCaches.CacheKeyPrefix = cacheKeyPrefix;

                string json = JsonConvert.SerializeObject(clearCaches);
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {


                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    agentResultModel.Result = true;//JsonConvert.DeserializeObject<AgentResultModel>(streamReader.ReadToEnd());
                }
            }
            catch (Exception ex)
            {

                agentResultModel.Result = false;
                agentResultModel.ResultMsg = ex.ToString();
            }
            return agentResultModel;
        }
        #endregion

        #region Common API services

        /// <summary>
        /// Insert store host url in setting table, So it can be accessible from Customer/Agent App 
        /// and we can make API call to which we have developed in CommonAPI controller
        /// </summary>
        public void AddUpdateStoreBaseUrlInSetting()
        {
            var webHelper = EngineContext.Current.Resolve<IWebHelper>();
            var storeUrl = webHelper.GetStoreLocation();
            var storeId = EngineContext.Create().Resolve<IWorkContext>().GetCurrentStoreId;

            _settingService.SetSetting("Nb.Store.BaseUrl", storeUrl, storeId);
        }

        #endregion

        #endregion
    }
}