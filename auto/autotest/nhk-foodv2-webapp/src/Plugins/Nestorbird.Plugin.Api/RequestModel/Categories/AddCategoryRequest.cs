﻿using System.Collections.Generic;

namespace Nestorbird.Plugin.Api.RequestModel.Categories
{
    public class AddCategoryRequest
    {
        public AddCategoryRequest()
        {
            ProductIds = new List<int>();
            RemoveImage = false;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int CategoryTemplateId { get; set; }

        public bool ShowOnHomepage { get; set; }

        public int ParentCategoryId { get; set; }

        public int DisplayOrder { get; set; }

        public bool Published { get; set; }

        public int VendorId { get; set; }

        public string ImageUrl { get; set; }

        public string ImageBase64 { get; set; }

        public string ImageName { get; set; }

        public bool RemoveImage { get; set; }

        public IList<int> ProductIds { get; set; }
    }
}
