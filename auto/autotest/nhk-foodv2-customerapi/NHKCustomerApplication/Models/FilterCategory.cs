﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class FilterCategory
    {
        public FilterCategory()
        {
            FilterCategoryValues = new HashSet<FilterCategoryValues>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int? Type { get; set; }
        public string FilterFor { get; set; }
        public int StoreId { get; set; }

        public virtual ICollection<FilterCategoryValues> FilterCategoryValues { get; set; }
    }
}
