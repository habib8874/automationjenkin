﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using Nop.Web.Validators.BookingTable;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.BookingTable
{
    //[Validator(typeof(ManageBookingTransactionValidator))]
    public class BookingTableTransactionModel: BaseNopModel
    {
        public BookingTableTransactionModel()
        {
            AvailableStores = new List<SelectListItem>();
            AvailableVendors = new List<SelectListItem>();
            AvailableTimings = new List<SelectListItem>();
            AvailableBookingStatus = new List<SelectListItem>();
        }
        public IList<SelectListItem> AvailableVendors { get; set; }
        public IList<SelectListItem> AvailableTimings { get; set; }

        public IList<SelectListItem> AvailableStores { get; set; }
        public int Id { get; set; }
        public string Store { get; set; }
        public string Vendor { get; set; }
        public int StoreId { get; set; }
        public int VendorId { get; set; }
        [NopResourceDisplayName("Admin.Booking.List.Store")]
        public int SearchStoreId { get; set; }
        [NopResourceDisplayName("Admin.Booking.List.Vendor")]
        public int SearchVendorId { get; set; }
        [NopResourceDisplayName("Admin.Booking.BookingDate")]
        [UIHint("DateNullable")]
        public DateTime? BookingDate { get; set; }
        [NopResourceDisplayName("Admin.Booking.BookingTimeFrom")]
        public string BookingFrom { get; set; }
        public string DateOfBirthDay { get; set; }
        [NopResourceDisplayName("Admin.Booking.BookingTimeFrom")]
        public string From { get; set; }
        [NopResourceDisplayName("Admin.Booking.BookingTimeTo")]
        [UIHint("TimeNullable")]
        public string To { get; set; }
        public string BookingTo { get; set; }
        public TimeSpan BookingTimeFrom { get; set; }
        public TimeSpan BookingTimeTo { get; set; }
        [NopResourceDisplayName("Admin.Booking.CustomerName")]
        public string CustomerName { get; set; }
        [NopResourceDisplayName("Admin.Booking.Email")]
        public string Email { get; set; }
        [NopResourceDisplayName("Admin.Booking.Phone")]
        [MaxLength(10,ErrorMessage = "Mobile Number length should not be greater than 10 digits")]
        [MinLength(10, ErrorMessage = "Mobile Number length should not be less than 10 digits")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Mobile Number must be numeric")]
        public string MobileNumber { get; set; }
        [NopResourceDisplayName("Admin.Booking.TableNumber")]
        public int TableNumber { get; set; }
        public DateTime CreatedDt { get; set; }
        public bool IsLoggedInAsVendor { get; set; }
        [NopResourceDisplayName("Admin.Orders.Fields.BookingStatus")]
        public int BookingStatus { get; set; }
        [NopResourceDisplayName("Admin.Orders.Fields.BookingStatus")]
        public string Status { get; set; }
        public IList<SelectListItem> AvailableBookingStatus { get; set; }
        public bool CanCancelBooking { get; set; }
        [NopResourceDisplayName("Admin.Booking.SeatingCapacity")]
        public string SeatingCapacity { get; set; }
        public int Seats { get; set; }
        [NopResourceDisplayName("Admin.Booking.SpecialNotes")]
        public string SpecialNotes { get; set; }
        public bool AddBookingNoteHasDownload { get; set; }
        [NopResourceDisplayName("Admin.Orders.OrderNotes.Fields.Note")]
        public string AddBookingNoteMessage { get; set; }
        [NopResourceDisplayName("Admin.Orders.OrderNotes.Fields.Download")]
        [UIHint("Download")]
        public int AddBookingNoteDownloadId { get; set; }
        [NopResourceDisplayName("Admin.Orders.OrderNotes.Fields.DisplayToCustomer")]
        public bool AddBookingNoteDisplayToCustomer { get; set; }
        public partial class BookingNote : BaseNopEntityModel
        {
            public int BookingId { get; set; }
            [NopResourceDisplayName("Admin.Orders.OrderNotes.Fields.DisplayToCustomer")]
            public bool DisplayToCustomer { get; set; }
            [NopResourceDisplayName("Admin.Orders.OrderNotes.Fields.Note")]
            public string Note { get; set; }
            [NopResourceDisplayName("Admin.Orders.OrderNotes.Fields.Download")]
            public int DownloadId { get; set; }
            [NopResourceDisplayName("Admin.Orders.OrderNotes.Fields.Download")]
            public Guid DownloadGuid { get; set; }
            [NopResourceDisplayName("Admin.Orders.OrderNotes.Fields.CreatedOn")]
            public DateTime CreatedOn { get; set; }
        }
    }
    
}
