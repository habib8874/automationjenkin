﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB.Merchant;

namespace Nop.Data.Mapping.NB.Merchant
{
    /// <summary>
    /// Represents a favorite merchant configuration
    /// </summary>
    public partial class FavoriteMerchantMap : NopEntityTypeConfiguration<FavoriteMerchant>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<FavoriteMerchant> builder)
        {
            builder.ToTable(NopMappingDefaults.FavoriteMerchantTable);
            builder.HasKey(mapping => new { mapping.VendorId, mapping.CustomerId });

            builder.Property(mapping => mapping.CustomerId).HasColumnName("Customer_Id");
            builder.Property(mapping => mapping.VendorId).HasColumnName("Vendor_Id");

            builder.HasOne(mapping => mapping.Customer)
            .WithMany()
            .HasForeignKey(mapping => mapping.CustomerId)
            .IsRequired();

            builder.HasOne(mapping => mapping.Vendor)
                .WithMany()
                .HasForeignKey(mapping => mapping.VendorId)
                .IsRequired();

            builder.Ignore(mapping => mapping.Id);

            base.Configure(builder);
        }

        #endregion
    }
}
