﻿using System.Collections.Generic;
using Nop.Core.Domain.NB.VendorTokens;


namespace Nop.Services.NB.VendorTokens
{
    public partial interface INBVendorTokenService
    {
        /// <summary>
        /// GetNBVendorTokenByToken
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        NBVendorToken GetNBVendorTokenByToken(string token);

        /// <summary>
        /// GetNBVendorTokenByCustomerId
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        IList<NBVendorToken> GetNBVendorTokenByCustomerId(int customerId);

        /// <summary>
        /// InsertNBVendorToken
        /// </summary>
        /// <param name="vendorToken"></param>
        void InsertNBVendorToken(NBVendorToken vendorToken);

        /// <summary>
        /// UpdateNBVendorToken
        /// </summary>
        /// <param name="vendorToken"></param>
        void UpdateNBVendorToken(NBVendorToken vendorToken);
    }
}
