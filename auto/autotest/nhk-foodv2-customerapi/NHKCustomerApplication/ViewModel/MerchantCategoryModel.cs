﻿using NHKCustomerApplication.ViewModel;

namespace NHKCustomerApplication.ViewModels
{
    /// <summary>
    /// Represents a MerchantCategory model
    /// </summary>
    public partial class MerchantCategoryModel
    {
        public MerchantCategoryModel() 
        {
            
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string ShortDescription { get; set; }

        public int PictureId { get; set; }

        public string PictureThumbnailUrl { get; set; }

        public int DisplayOrder { get; set; }

        public int StoreId { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the entity is published
        /// </summary>
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }

        public string StoreName { get; set; }
    }

    public class MerchantCategorysModel
    {
        public int CustomerId { get; set; }
        public string ApiKey { get; set; }
        public int StoreId { get; set; }
        public double LatPos { get; set; }
        public double LongPos { get; set; }
        public int RestType { get; set; }
        public int MerchantCategoryId { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class MerchantCategorysModelV2 : BaseAPIRequestModel
    {
        public double LatPos { get; set; }
        public double LongPos { get; set; }
        public int RestType { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string Keywords { get; set; }


    }
    public class Categorys : BaseAPIRequestModel
    {
        public int RestncookId { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Keywords { get; set; }
        public int ParentCategoryId { get; set; }


    }
    public class CooknCategory
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ParentCategoryId { get; set; }
        public bool Published { get; set; }
        public bool Deleted { get; set; }
        public int PictureId { get; set; }
        public string PictureThumbnailUrl { get; set; }
        public int TotalRecords { get; set; }
        public int DisplayOrder { get; set; }

    }
}