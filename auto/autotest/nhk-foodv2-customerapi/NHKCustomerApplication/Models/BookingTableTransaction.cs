﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class BookingTableTransaction
    {
        public BookingTableTransaction()
        {
            BookingNote = new HashSet<BookingNote>();
        }

        public int Id { get; set; }
        public int StoreId { get; set; }
        public int VendorId { get; set; }
        public DateTime BookingDate { get; set; }
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public int TableNumber { get; set; }
        public DateTime CreatedDt { get; set; }
        public TimeSpan? BookingTimeFrom { get; set; }
        public TimeSpan? BookingTimeTo { get; set; }
        public int Status { get; set; }
        public string SeatingCapacity { get; set; }
        public string SpecialNotes { get; set; }

        public virtual ICollection<BookingNote> BookingNote { get; set; }
    }
}
