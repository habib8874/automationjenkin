using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Stores;
using Nop.Data;
using Nop.Services.Events;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Product tag service
    /// </summary>
    public partial class ProductCuisineMappingService  : IProductCuisineMappingService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : store ID
        /// </remarks>
        private const string PRODUCTTAG_COUNT_KEY = "Nop.producttagimagemapping.count-{0}";

        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PRODUCTTAG_PATTERN_KEY = "Nop.producttagimagemapping.";

        #endregion

        #region Fields

        private readonly IRepository<ProductCuisine> _productCuisineMappingReporitory;
        private readonly IRepository<ProductCuisineMapping> _productCuisine_MappingReporitory;
       // private readonly IRepository<ProductImageTag> _tagImageMappingReporitory;
        private readonly IRepository<ProductCuisine> _ProductCuisineRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly IDataProvider _dataProvider;
        private readonly IDbContext _dbContext;
        private readonly CommonSettings _commonSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly IProductService _productService;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="productServiceTagImageMappingRepository">Product Tag Image Mapping</param>
        /// <param name="productTagRepository">Product tag repository</param>
        /// <param name="dataProvider">Data provider</param>
        /// <param name="dbContext">Database Context</param>
        /// <param name="commonSettings">Common settings</param>
        /// <param name="cacheManager">Static cache manager</param>
        /// <param name="eventPublisher">Event published</param>
        /// <param name="storeMappingRepository">Store mapping repository</param>
        /// <param name="catalogSettings">Catalog settings</param>
        /// <param name="productService">Product service</param>
        public ProductCuisineMappingService (IRepository<ProductCuisine> productCuisineMappingRepository,
            IRepository<ProductCuisine> productCuisineRepository,
            IRepository<StoreMapping> storeMappingRepository,
            IDataProvider dataProvider,
            IDbContext dbContext,
            CommonSettings commonSettings,
            CatalogSettings catalogSettings,
            IStaticCacheManager cacheManager,
            IEventPublisher eventPublisher,
            IProductService productService,
            IRepository<ProductCuisineMapping> productCuisine_MappingReporitory)
        //IRepository<ProductImageTag> tagImageMappingReporitory)
        {
            _productCuisineMappingReporitory = productCuisineMappingRepository;
            _ProductCuisineRepository = productCuisineRepository;
            _storeMappingRepository = storeMappingRepository;
            _dataProvider = dataProvider;
            _dbContext = dbContext;
            _commonSettings = commonSettings;
            _catalogSettings = catalogSettings;
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _productService = productService;
            _productCuisine_MappingReporitory = productCuisine_MappingReporitory;
        }

        #endregion

        #region Nested classes

        private class ProductTagWithCount
        {
            public int ProductTagId { get; set; }
            //public int ProductCount { get; set; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Delete a product tag Image Mapping
        /// </summary>
        /// <param name="productTagImageMapping">Product tag Image Mapping</param>
        public virtual void DeleteProductCuisineMapping(ProductCuisine ProductCuisineMapping) //by Mohini
        {
            if (ProductCuisineMapping == null)
                throw new ArgumentNullException(nameof(ProductCuisineMapping));
            if (DeleteProductCuisine_Mapping(ProductCuisineMapping.Id))
            {
                _productCuisineMappingReporitory.Delete(ProductCuisineMapping);

                //event notification
                _eventPublisher.EntityDeleted(ProductCuisineMapping);
            }
        }
        public virtual bool DeleteProductCuisine_Mapping(int Id) //by Mohini
        {
            if (Id == 0)
                throw new ArgumentNullException(nameof(ProductCuisineMapping));
            bool result = false;
            var objst =_productCuisine_MappingReporitory.Table.Where(x => x.CuisineId == Id).ToList();
            foreach (var obj in objst)
            {
                _productCuisine_MappingReporitory.Delete(obj);
                //event notification
                _eventPublisher.EntityDeleted(obj);
            }
            result = true;
            return result;

        }

        public virtual IList<ProductCuisine> GetAllProductCuisineMapping ()
        {
            var ProductCuisines = _productCuisineMappingReporitory.Table.ToList();
            return ProductCuisines;
        }

        public virtual ProductCuisine GetProductCuisineMappingById(int ProductCuisineMappingId)
        {
            if (ProductCuisineMappingId == 0)
                return null;

            return _productCuisineMappingReporitory.GetById(ProductCuisineMappingId);
        }
        public IList<ProductCuisine> GetProductCuisineMappingByStoreId(int StoreId)
        {
            if (StoreId == 0)
                return new List<ProductCuisine>();

            return _productCuisineMappingReporitory.Table.Where(p => p.StoreId == StoreId ).ToList();
        }
        public virtual ProductCuisine GetProductCuisineMappingByName(string name)
        {

            return _productCuisineMappingReporitory.Table.FirstOrDefault(x=>x.Name.ToLower()==name.ToLower());
        }

        public virtual int InsertProductCuisineMapping(ProductCuisine ProductCuisineMapping)
        {
            if (ProductCuisineMapping == null)
                throw new ArgumentNullException(nameof(ProductCuisine));

            _productCuisineMappingReporitory.Insert(ProductCuisineMapping);

            //cache
            //_cacheManager.RemoveByPattern(PRODUCTTAG_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(ProductCuisineMapping);
            return ProductCuisineMapping.Id;
        }
        
        public virtual void UpdateProductCuisineMapping(ProductCuisine ProductCuisineMapping)
        {
            if (ProductCuisineMapping == null)
                throw new ArgumentNullException(nameof(ProductCuisineMapping));

            _productCuisineMappingReporitory.Update(ProductCuisineMapping);

            //cache
            //_cacheManager.RemoveByPattern(PRODUCTTAG_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(ProductCuisineMapping);
        }

        public virtual int InsertProductCuisine_Mapping(ProductCuisineMapping ProductCuisineMapping)
        {
            if (ProductCuisineMapping == null)
                throw new ArgumentNullException(nameof(ProductCuisine));

            var pc = _productCuisine_MappingReporitory.Table.Where(X => X.ProductId == ProductCuisineMapping.ProductId && X.CuisineId == ProductCuisineMapping.Id).ToList();
            if (pc.Count == 0)
            {
                _productCuisine_MappingReporitory.Insert(ProductCuisineMapping);

                //event notification
                _eventPublisher.EntityInserted(ProductCuisineMapping);
            }
            return ProductCuisineMapping.Id;
        }

        public virtual IList<ProductCuisineMapping> GetProductCuisine_MappingByProduct(int id)
        {

            return _productCuisine_MappingReporitory.Table.Where(X => X.ProductId == id).ToList();
            
        }
        public virtual void DeleteAllMappedProductCuisines(int productId) //by Mohini
        {
            var prd_cs = _productCuisine_MappingReporitory.Table.Where(X => X.ProductId == productId).ToList();
            if (prd_cs.Count > 0)
            {
                foreach (var item in prd_cs)
                {
                    _productCuisine_MappingReporitory.Delete(item);

                    //event notification
                    _eventPublisher.EntityDeleted(item);
                }
            }
        }
        #endregion
    }
}