﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Common
{
    public class ClearCaches
    {
        public int StoreId { get; set; }
        public string CacheKeyPrefix { get; set; }
    }
}
