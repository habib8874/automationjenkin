﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Catalog
{
    public partial class ProductTagsImagesModel : BaseNopEntityModel
    {
        public ProductTagsImagesModel()
        {
            TagImages = new List<ProductTagsImagesListModel>();
        }
        public bool ShowAll { get; set; }

        public IList<ProductTagsImagesListModel> TagImages { get; set; }

        #region nested class
        public partial class ProductTagsImagesListModel
        {
            public int Id { get; set; }
            public string TagName { get; set; }
            public string ImageUrl { get; set; }
        }
        #endregion
    }
}