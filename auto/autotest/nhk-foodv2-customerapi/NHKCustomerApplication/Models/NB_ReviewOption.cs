﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NHKCustomerApplication.Models
{
    public class NB_ReviewOption
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int EntityType { get; set; }
        public bool Active { get; set; }
    }
}
