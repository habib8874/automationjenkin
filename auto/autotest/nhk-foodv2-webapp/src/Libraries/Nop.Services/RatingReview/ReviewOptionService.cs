﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Data;
using Nop.Core.Domain.RatingReview;
using Nop.Services.Events;

namespace Nop.Services.RatingReview
{
    /// <summary>
    /// ReviewOption service
    /// </summary>
    public partial class ReviewOptionService : IReviewOptionService
    {
        #region Fields

        private readonly IRepository<ReviewOption> _reviewOptionRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        /// <summary>
        /// ReviewOption service
        /// </summary>
        /// <param name="reviewOptionRepository">Rating Reviews repository</param>
        /// <param name="storeMappingRepository">Store mapping repository</param>
        /// <param name="storeMappingService">Store mapping service</param>
        /// <param name="workContext">Work context</param>
        /// <param name="aclRepository">ACL repository</param>
        /// <param name="catalogSettings">Catalog settings</param>
        /// <param name="eventPublisher">Event publisher</param>
        /// <param name="cacheManager">Cache manager</param>
        public ReviewOptionService(IRepository<ReviewOption> reviewOptionRepository,
            IEventPublisher eventPublisher)
        {
            this._reviewOptionRepository = reviewOptionRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a reviewOption
        /// </summary>
        /// <param name="reviewOptionId">The reviewOption identifier</param>
        /// <returns>ReviewOption</returns>
        public ReviewOption GetReviewOptionById(int reviewOptionId)
        {
            if (reviewOptionId == 0)
                return null;

            return _reviewOptionRepository.GetById(reviewOptionId);
        }

        /// <summary>
        /// Gets all ReviewOption
        /// </summary>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <returns>ReviewOptions</returns>
        public IList<ReviewOption> GetAllReviewOption()
        {
            var query = _reviewOptionRepository.Table;
            //query = query.OrderByDescending(t => t.Id);
            return query.ToList();
        }

        /// <summary>
        /// Inserts a reviewOption
        /// </summary>
        /// <param name="reviewOption">ReviewOption</param>
        public void InsertReviewOption(ReviewOption reviewOption)
        {
            if (reviewOption == null)
                throw new ArgumentNullException(nameof(reviewOption));

            _reviewOptionRepository.Insert(reviewOption);

            //event notification
            _eventPublisher.EntityInserted(reviewOption);
        }

        /// <summary>
        /// Updates the reviewOption
        /// </summary>
        /// <param name="reviewOption">ReviewOption</param>
        public void UpdateReviewOption(ReviewOption reviewOption)
        {
            if (reviewOption == null)
                throw new ArgumentNullException(nameof(reviewOption));

            _reviewOptionRepository.Update(reviewOption);

            //event notification
            _eventPublisher.EntityUpdated(reviewOption);
        }

        /// <summary>
        /// Deletes a reviewOption
        /// </summary>
        /// <param name="reviewOption">ReviewOption</param>
        public void DeleteReviewOption(ReviewOption reviewOption)
        {
            if (reviewOption == null)
                throw new ArgumentNullException(nameof(reviewOption));

            _reviewOptionRepository.Delete(reviewOption);

            //event notification
            _eventPublisher.EntityDeleted(reviewOption);
        }

        #endregion
    }
}