﻿using Nop.Core;
using Nop.Services.Configuration;
using Nop.Services.Plugins;

namespace Nestorbird.Plugin.Api
{
    /// <summary>
    /// Google Analytics plugin
    /// </summary>
    public class NestorbirdAPIPlugin : BasePlugin
    {
        private readonly IWebHelper _webHelper;
        private readonly ISettingService _settingService;

        public static string DeviceTokenAttribute = "NestorBird_DeviceToken";

        public NestorbirdAPIPlugin(IWebHelper webHelper,
            ISettingService settingService)
        {
            _webHelper = webHelper;
            _settingService = settingService;
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/MerchantApi/Configure";
        }

        /// <summary>
        /// Install plugin
        /// </summary>
        public override void Install()
        {
            var settings = new MerchantAPISettings
            {
                MerchantAppWebKey = "AAAAVjnc4-Q:APA91bE8WMn6QHm6faICBO-pLX2xyxUvXkosyQH7DZrPGyTZfCtbSlHAYan9f0jc_almRhO0BCJd2FKc8jX2cQiLsyN99Nj4PK2CyR6eGF_qGX7pEE7PMOzwJYnMNuTCnUNJ9rGsT2Fo"
            };
            _settingService.SaveSetting(settings);

            base.Install();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            _settingService.DeleteSetting<MerchantAPISettings>();
            base.Uninstall();
        }
    }
}