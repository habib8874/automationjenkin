﻿using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Customer
{
    public partial class PasswordRecoveryModel : BaseNopModel
    {
        [DataType(DataType.EmailAddress)]
        [NopResourceDisplayName("Account.PasswordRecovery.Email")]
        public string Email { get; set; }

        [NopResourceDisplayName("Account.PasswordRecovery.EmailOrPhone")]
        public string EmailOrPhone { get; set; }
        public bool OTPVerificationEnabled { get; set; }

        public string Result { get; set; }

        [NopResourceDisplayName("Account.PasswordRecovery.OTP")]
        public string OTP { get; set; }

        public bool DisplayCaptcha { get; set; }
    }
}