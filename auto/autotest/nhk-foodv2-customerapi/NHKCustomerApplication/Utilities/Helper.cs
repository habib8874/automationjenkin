﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NHKCustomerApplication.Caching;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.ViewModel;
using NHKCustomerApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace NHKCustomerApplication.Utilities
{

	public static class Helper
	{
		private const string bucketName = "*** bucket name ***";
		// For simplicity the example creates two objects from the same file.
		// You specify key names for these objects.
		private const string keyName1 = "*** key name for first object created ***";
		private static readonly RegionEndpoint bucketRegion = RegionEndpoint.APSouth1;
		private static IAmazonS3 client = new AmazonS3Client(bucketRegion);

		public static string CreateSaltKey(int size)
		{
			//generate a cryptographic random number
			using (var provider = new RNGCryptoServiceProvider())
			{
				var buff = new byte[size];
				provider.GetBytes(buff);

				// Return a Base64 string representation of the random number
				return Convert.ToBase64String(buff);
			}
		}
		public static CustomerAPIResponses GetCustomerAPIErrorResponses(string errorMessage)
		{
			return new CustomerAPIResponses
			{
				ErrorMessageTitle = "Error!!",
				ErrorMessage = errorMessage,
				Status = false,
				StatusCode = (int)HttpStatusCode.NoContent,
				ResponseObj = null
			};
		}

		public static async Task<UploadPhotoModel> UploadObject(byte[] fileBytes, string ContentType, string fileName, ecuadordevContext db)
		{
			string accessKey = db.Setting.AsEnumerable().Where(x => x.Name.ToLower().Equals("s3.accesskey")).FirstOrDefault() != null ?
					db.Setting.AsEnumerable().Where(x => x.Name.ToLower().Equals("s3.accesskey")).FirstOrDefault().Value : "AC039a08adfdbdc51566e5ebe18654dccd";
			string accessSecret = db.Setting.AsEnumerable().Where(x => x.Name.ToLower().Equals("s3.accesssecret")).FirstOrDefault() != null ?
				db.Setting.AsEnumerable().Where(x => x.Name.ToLower().Equals("s3.accesssecret")).FirstOrDefault().Value : "b07f1451c3b921e08f924fe81a86a0e3d";

			// connecting to the client
			var client = new AmazonS3Client(accessKey, accessSecret, Amazon.RegionEndpoint.APSouth1);

			PutObjectResponse response = null;

			using (var stream = new MemoryStream(fileBytes))
			{
				var request = new PutObjectRequest
				{
					BucketName = "nhkprofileimages",
					Key = fileName,

					InputStream = stream,
					ContentType = ContentType,
					CannedACL = S3CannedACL.BucketOwnerFullControl
				};

				response = await client.PutObjectAsync(request);
			};

			if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
			{
				// this model is up to you, in my case I have to use it following;
				return new UploadPhotoModel
				{
					Success = true,
					FileName = fileName
				};
			}
			else
			{
				// this model is up to you, in my case I have to use it following;
				return new UploadPhotoModel
				{
					Success = false,
					FileName = fileName
				};
			}
		}

		public static IList<string> GetAllAvailableTiming()
		{
			List<string> timeIntervals = new List<string>();
			TimeSpan startTime = new TimeSpan(0, 0, 0);
			DateTime startDate = new DateTime(DateTime.MinValue.Ticks); // Date to be used to get shortTime format.
			for (int i = 0; i < 48; i++)
			{
				int minutesToBeAdded = 30 * i;      // Increasing minutes by 30 minutes interval
				TimeSpan timeToBeAdded = new TimeSpan(0, minutesToBeAdded, 0);
				TimeSpan t = startTime.Add(timeToBeAdded);
				DateTime result = startDate + t;
				timeIntervals.Add(result.ToShortTimeString());      // Use Date.ToShortTimeString() method to get the desired format                
			}
			return timeIntervals;
		}
		/// <summary>
		/// Create a password hash
		/// </summary>
		/// <param name="password">Password</param>
		/// <param name="saltkey">Salk key</param>
		/// <param name="passwordFormat">Password format (hash algorithm)</param>
		/// <returns>Password hash</returns>
		public static string CreatePasswordHash(string password, string saltkey, string passwordFormat)
		{
			return CreateHash(Encoding.UTF8.GetBytes(string.Concat(password, saltkey)), passwordFormat);
		}

		/// <summary>
		/// Create a data hash
		/// </summary>
		/// <param name="data">The data for calculating the hash</param>
		/// <param name="hashAlgorithm">Hash algorithm</param>
		/// <returns>Data hash</returns>
		public static string CreateHash(byte[] data, string hashAlgorithm)
		{
			if (string.IsNullOrEmpty(hashAlgorithm))
				throw new ArgumentNullException(nameof(hashAlgorithm));

			var algorithm = HashAlgorithm.Create(hashAlgorithm);
			if (algorithm == null)
				throw new ArgumentException("Unrecognized hash name");

			var hashByteArray = algorithm.ComputeHash(data);
			return BitConverter.ToString(hashByteArray).Replace("-", "");
		}

		public static async Task<bool> SendMessage(string to, string bodymessage, ecuadordevContext db)
		{
			try
			{

				string accountSid = db.Setting.AsEnumerable().Where(x => x.Name.Equals("twillio.accountsid")).FirstOrDefault() != null ?
					db.Setting.AsEnumerable().Where(x => x.Name.Equals("twillio.accountsid")).FirstOrDefault().Value : "AC039a08adfdbdc51566e5ebe18654dccd";
				string authToken = db.Setting.AsEnumerable().Where(x => x.Name.Equals("twillio.authtoken")).FirstOrDefault() != null ?
					db.Setting.AsEnumerable().Where(x => x.Name.Equals("twillio.authtoken")).FirstOrDefault().Value : "b07f1451c3b921e08f924fe81a86a0e3d";
				string senderId = db.Setting.AsEnumerable().Where(x => x.Name.Equals("twillio.senderid")).FirstOrDefault() != null ?
					db.Setting.AsEnumerable().Where(x => x.Name.Equals("twillio.senderid")).FirstOrDefault().Value : "5123578406";
				TwilioClient.Init(accountSid, authToken);
				var from = new PhoneNumber(senderId);
				var To = new PhoneNumber(to);
				var message = MessageResource.Create(
				to: To,
				from: from,
				body: bodymessage);
				return true;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				return false;
			}
		}
		public static string HtmlToPlainText(string html)
		{
			const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
			const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
			const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
			var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
			var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
			var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

			var text = html;
			//Decode html specific characters
			text = System.Net.WebUtility.HtmlDecode(text);
			//Remove tag whitespace/line breaks
			text = tagWhiteSpaceRegex.Replace(text, "><");
			//Replace <br /> with line breaks
			text = lineBreakRegex.Replace(text, Environment.NewLine);
			//Strip formatting
			text = stripFormattingRegex.Replace(text, string.Empty);

			return text;
		}
		public static string GetCurrencySymbolFromCode(string currencyCode)
		{
			var symbol = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => !c.IsNeutralCulture).Select(culture =>
			{
				try
				{
					if (culture.LCID != 127)
					{
						RegionInfo region = new RegionInfo(culture.LCID);
						//RegionInfo region = new RegionInfo(culture.LCID);
						return new RegionInfo(culture.LCID);
					}
					else
					{
						return null;
					}
				}
				catch (Exception ex)
				{
					Helper.SentryLogs(ex);
					return null;
				}
			}).Where(ri => ri != null && ri.ISOCurrencySymbol == currencyCode).Select(ri => ri.CurrencySymbol).FirstOrDefault();
			return symbol;
		}
		public static string GetStoreCurrency(int StoreId = 0, ecuadordevContext context = null)
		{
			var currencyCode = (from currency in context.Currency
								join mapPrimary in context.Setting.Where(x => x.StoreId == StoreId && x.Name.Contains("currencysettings.primarystorecurrencyid"))
								on currency.Id.ToString() equals mapPrimary.Value
								select new
								{
									currency.CurrencyCode
								}).FirstOrDefault()?.CurrencyCode ?? "USD";
			var symbol = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => !c.IsNeutralCulture).Select(culture =>
			{
				try
				{
					if (culture.LCID != 127)
					{
						RegionInfo region = new RegionInfo(culture.LCID);
						//RegionInfo region = new RegionInfo(culture.LCID);
						return new RegionInfo(culture.LCID);
					}
					else
					{
						return null;
					}

				}
				catch (Exception ex)
				{
					Helper.SentryLogs(ex);
					return null;
				}
			}).Where(ri => ri != null && ri.ISOCurrencySymbol == currencyCode).Select(ri => ri.CurrencySymbol).FirstOrDefault();
			return symbol;


		}

		public static string GetStoreCodeCurrency(int StoreId = 0, ecuadordevContext context = null)
		{
			var currencyCode = (from currency in context.Currency
								join mapPrimary in context.Setting.Where(x => x.StoreId == StoreId && x.Name.Contains("currencysettings.primarystorecurrencyid"))
								on currency.Id.ToString() equals mapPrimary.Value
								select new
								{
									currency.CurrencyCode
								}).FirstOrDefault()?.CurrencyCode ?? "USD";

			return currencyCode;


		}
		public static DateTime ConvertFromUnixTimestamp(double timestamp)
		{
			DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			return origin.AddSeconds(timestamp);
		}

		public static double ConvertToUnixTimestamp(DateTime date)
		{
			DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			TimeSpan diff = date.ToUniversalTime() - origin;
			return Math.Floor(diff.TotalSeconds);
		}
		public static double distance(double lat1, double lon1, double lat2, double lon2, string unit)
		{
			if ((lat1 == lat2) && (lon1 == lon2))
			{
				return 0;
			}
			else
			{
				double theta = lon1 - lon2;
				double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
				dist = Math.Acos(dist);
				dist = rad2deg(dist);
				dist = dist * 60 * 1.1515;
				if (unit == "K")
				{
					dist = dist * 1.609344;
				}
				else if (unit == "N")
				{
					dist = dist * 0.8684;
				}
				return (dist);
			}
		}

		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//::  This function converts decimal degrees to radians             :::
		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		private static double deg2rad(double deg)
		{
			return (deg * Math.PI / 180.0);
		}

		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//::  This function converts radians to decimal degrees             :::
		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		private static double rad2deg(double rad)
		{
			return (rad / Math.PI * 180.0);
		}
		public static DateTime ToTimeZoneTime(this DateTime time, string timeZoneId = "Pacific Standard Time")
		{
			TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
			return time.ToTimeZoneTime(tzi);
		}

		/// <summary>
		/// Returns TimeZone adjusted time for a given from a Utc or local time.
		/// Date is first converted to UTC then adjusted.
		/// </summary>
		/// <param name="time"></param>
		/// <param name="timeZoneId"></param>
		/// <returns></returns>
		public static DateTime ToTimeZoneTime(this DateTime time, TimeZoneInfo tzi)
		{
			return TimeZoneInfo.ConvertTimeFromUtc(time, tzi);
		}
		public static DateTime ConvertToUserTime_Old(DateTime dt, DateTimeKind sourceDateTimeKind, ecuadordevContext context)
		{

			var timeZoneId = string.Empty;
			timeZoneId = context.Setting.Where(x => x.Name.Contains("datetimesettings.defaultstoretimezoneid")).FirstOrDefault().Value;

			//System.Collections.ObjectModel.ReadOnlyCollection<TimeZoneInfo> tz;
			//tz = TimeZoneInfo.GetSystemTimeZones();
			//timeZoneId = tz[87].Id;
			var CurrentTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
			var timeZone = Helper.ToTimeZoneTime(dt, timeZoneId);
			return timeZone;
		}

		public static DateTime ConvertToUserTime(DateTime dt, DateTimeKind sourceDateTimeKind, ecuadordevContext context)
		{
			dt = DateTime.SpecifyKind(dt, DateTimeKind.Utc);

			var timeZoneId = context.Setting.Where(x => x.Name.Contains("datetimesettings.defaultstoretimezoneid")).FirstOrDefault().Value;
			var timezones = TimeZoneInfo.GetSystemTimeZones();
			var currentTimeZone = timezones.FirstOrDefault(x => x.Id == timeZoneId);
			if (currentTimeZone == null)
				currentTimeZone = timezones.FirstOrDefault(x => x.DisplayName.Contains("Kolkata"));
			var currentUserTimeZoneInfo = currentTimeZone;
			return TimeZoneInfo.ConvertTime(dt, currentUserTimeZoneInfo);
		}

		public static DateTime ConvertToUserTimeV2_1(DateTime dt, DateTimeKind sourceDateTimeKind, ecuadordevContext context, int userId, string userRole = "")
		{
			// string rolename = userRole;
			var cusomerInfo = context.Customer.Where(x => x.Id == userId).FirstOrDefault();
			if (cusomerInfo != null)
			{
				var timeZoneId = string.Empty;
				if (string.IsNullOrEmpty(userRole))
				{
					// get user role
					var role = (from rm in context.CustomerCustomerRoleMapping
								join r in context.CustomerRole on rm.CustomerRoleId equals r.Id
								where rm.CustomerId == userId
								select r
								);
					if (role.Any(x => x.Name == "Administrators"))
					{
						//rolename = "admin";
					}
					else if (role.Any(x => x.Name == "Store Owner"))
					{
						timeZoneId = context.Setting.Where(x => x.Name == "datetimesettings.storetimezoneid" && x.StoreId == cusomerInfo.RegisteredInStoreId).FirstOrDefault().Value;
						// rolename = "storeadmin";
					}
					else if (role.Any(x => x.Name == "Vendors"))
					{
						timeZoneId = context.GenericAttribute.Where(x => x.Key == "TimeZoneId" && x.KeyGroup == "Vendor" && x.EntityId == cusomerInfo.VendorId).FirstOrDefault().Value;
						if (string.IsNullOrEmpty(timeZoneId))
						{
							timeZoneId = context.Setting.Where(x => x.Name == "datetimesettings.storetimezoneid" && x.StoreId == cusomerInfo.RegisteredInStoreId).FirstOrDefault().Value;
						}
						// rolename = "vendor";
					}
				}
				if (string.IsNullOrEmpty(timeZoneId))
				{
					timeZoneId = context.GenericAttribute.Where(x => x.Key == "TimeZoneId" && x.KeyGroup == "Customer" && x.EntityId == cusomerInfo.Id).FirstOrDefault().Value;
				}
				if (string.IsNullOrEmpty(timeZoneId))
				{
					timeZoneId = context.Setting.Where(x => x.Name == "datetimesettings.storetimezoneid" && x.StoreId == cusomerInfo.RegisteredInStoreId).FirstOrDefault().Value;
				}
				if (string.IsNullOrEmpty(timeZoneId))
				{
					return Helper.ToTimeZoneTime(dt, TimeZoneInfo.Local);
				}
				try
				{
					var CurrentTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
					var timeZone = Helper.ToTimeZoneTime(dt, timeZoneId);
					return timeZone;
				}
				catch
				{
					return Helper.ToTimeZoneTime(dt, TimeZoneInfo.Local);
				}
			}
			else
			{
				return Helper.ToTimeZoneTime(dt, TimeZoneInfo.Local);
			}

		}

		public static DateTime ConvertToUserTimeV2(DateTime dt, DateTimeKind sourceDateTimeKind, string timeZoneId)
		{
			//System.Collections.ObjectModel.ReadOnlyCollection<TimeZoneInfo> tz;
			//tz = TimeZoneInfo.GetSystemTimeZones();
			//timeZoneId = tz[87].Id;
			var CurrentTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
			var timeZone = Helper.ToTimeZoneTime(dt, timeZoneId);
			return timeZone;
		}
		public static string GetStoreCurrencyCode(int storeId, ecuadordevContext db)
		{
			int _storeId = Convert.ToInt32(storeId);
			var symbol = (from setting in db.Setting.AsEnumerable()
						  join currency in db.Currency.AsEnumerable()
						  on setting.Value equals currency.Id.ToString()
						  where setting.Name.Contains("currencysettings.primaryexchangeratecurrencyid")
						  && setting.StoreId == _storeId
						  select new
						  {
							  currency.CurrencyCode
						  }).FirstOrDefault() != null ? (from setting in db.Setting.AsEnumerable()
														 join currency in db.Currency.AsEnumerable()
														 on setting.Value equals currency.Id.ToString()
														 where setting.Name.Contains("currencysettings.primaryexchangeratecurrencyid")
														 && setting.StoreId == _storeId
														 select new
														 {
															 currency.CurrencyCode
														 }).FirstOrDefault().CurrencyCode : "USD";
			return symbol;

		}

		public static string GetCurrentCurrencyCode(int storeId, ecuadordevContext db)
		{
			int _storeId = Convert.ToInt32(storeId);
			var symbol = (from setting in db.Setting.AsEnumerable()
						  join currency in db.Currency.AsEnumerable()
						  on setting.Value equals currency.Id.ToString()
						  where setting.Name.Contains("currencysettings.primarystorecurrencyid")
						  && setting.StoreId == _storeId
						  select new
						  {
							  currency.CurrencyCode
						  }).FirstOrDefault()?.CurrencyCode ?? "USD";
			return symbol;

		}
		public static string ConvertdecimaltoUptotwoPlaces(decimal number)
		{
			return string.Format("{0:N2}", number);
		}
		public static string GetFileExtensionFromMimeType(string mimeType)
		{
			if (mimeType == null)
				return null;

			//TODO use FileExtensionContentTypeProvider to get file extension

			var parts = mimeType.Split('/');
			var lastPart = parts[parts.Length - 1];
			switch (lastPart)
			{
				case "pjpeg":
					lastPart = "jpg";
					break;
				case "x-png":
					lastPart = "png";
					break;
				case "x-icon":
					lastPart = "ico";
					break;
			}
			return lastPart;
		}
		public static void SendEmail(EmailAccount emailAccount, string subject, string body,
		   string fromAddress, string fromName, string toAddress, string toName,
			string replyTo = null, string replyToName = null,
		   IEnumerable<string> bcc = null, IEnumerable<string> cc = null,
		   string attachmentFilePath = null, string attachmentFileName = null,
		   int attachedDownloadId = 0, IDictionary<string, string> headers = null)
		{
			var message = new MailMessage
			{
				//from, to, reply to
				From = new MailAddress(fromAddress, fromName)
			};
			message.To.Add(new MailAddress(toAddress, toName));
			if (!string.IsNullOrEmpty(replyTo))
			{
				message.ReplyToList.Add(new MailAddress(replyTo, replyToName));
			}

			//BCC
			if (bcc != null)
			{
				foreach (var address in bcc.Where(bccValue => !string.IsNullOrWhiteSpace(bccValue)))
				{
					message.Bcc.Add(address.Trim());
				}
			}

			//CC
			if (cc != null)
			{
				foreach (var address in cc.Where(ccValue => !string.IsNullOrWhiteSpace(ccValue)))
				{
					message.CC.Add(address.Trim());
				}
			}

			//content
			message.Subject = subject;
			message.Body = body;
			message.IsBodyHtml = true;

			//headers
			if (headers != null)
				foreach (var header in headers)
				{
					message.Headers.Add(header.Key, header.Value);
				}

			//create the file attachment for this e-mail message
			if (!string.IsNullOrEmpty(attachmentFilePath) &&
				File.Exists(attachmentFilePath))
			{
				var attachment = new Attachment(attachmentFilePath);
				attachment.ContentDisposition.CreationDate = File.GetCreationTime(attachmentFilePath);
				attachment.ContentDisposition.ModificationDate = File.GetLastWriteTime(attachmentFilePath);
				attachment.ContentDisposition.ReadDate = File.GetLastAccessTime(attachmentFilePath);
				if (!string.IsNullOrEmpty(attachmentFileName))
				{
					attachment.Name = attachmentFileName;
				}
				message.Attachments.Add(attachment);
			}


			//send email
			using (var smtpClient = new SmtpClient())
			{
				smtpClient.UseDefaultCredentials = emailAccount.UseDefaultCredentials;
				smtpClient.Host = emailAccount.Host;
				smtpClient.Port = emailAccount.Port;
				smtpClient.EnableSsl = emailAccount.EnableSsl;
				smtpClient.Credentials = emailAccount.UseDefaultCredentials ?
					CredentialCache.DefaultNetworkCredentials :
					new NetworkCredential(emailAccount.Username, emailAccount.Password);
				smtpClient.Send(message);
			}
		}
		public static async Task<bool> sendMyFileToS3Async(string localFilePath, string keyName2, string filePath, string contentType)
		{
			// input explained :
			// localFilePath = the full local file path e.g. "c:\mydir\mysubdir\myfilename.zip"
			// bucketName : the name of the bucket in S3 ,the bucket should be alreadt created
			// subDirectoryInBucket : if this string is not empty the file will be uploaded to
			// a subdirectory with this name
			// fileNameInS3 = the file name in the S3

			// create an instance of IAmazonS3 class ,in my case i choose RegionEndpoint.EUWest1
			// you can change that to APNortheast1 , APSoutheast1 , APSoutheast2 , CNNorth1
			// SAEast1 , USEast1 , USGovCloudWest1 , USWest1 , USWest2 . this choice will not
			// store your file in a different cloud storage but (i think) it differ in performance
			// depending on your location
			var putRequest1 = new PutObjectRequest
			{
				BucketName = bucketName,
				Key = keyName1,
				ContentBody = "sample text"
			};

			PutObjectResponse response1 = await Helper.client.PutObjectAsync(putRequest1);

			// 2. Put the object-set ContentType and add metadata.
			var putRequest2 = new PutObjectRequest
			{
				BucketName = bucketName,
				Key = keyName2,
				FilePath = filePath,
				ContentType = contentType
			};
			putRequest2.Metadata.Add("x-amz-meta-title", "someTitle");
			PutObjectResponse response2 = await Helper.client.PutObjectAsync(putRequest2);
			return true;
		}
		public static string sendNotificationToCustomer(OrderReviewModel orderReviewModel, int languageId, ecuadordevContext dbcontext)
		{
			try
			{
				FCSNotificationModel fCSNotificationModel = new FCSNotificationModel();
				fCSNotificationModel.ApiKey = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.customerapinotificationkey") && x.StoreId == orderReviewModel.StoreId).Any() ?
				dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.customerapinotificationkey") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault()?.Value : "";
				fCSNotificationModel.OrderId = orderReviewModel.OrderId.ToString();
				fCSNotificationModel.CustomerId = orderReviewModel.CustomerId.ToString();
				fCSNotificationModel.StoreId = orderReviewModel.StoreId.ToString();
				fCSNotificationModel.UniqueSeoCode = orderReviewModel.UniqueSeoCode;
				var FSCToken = dbcontext.CustomerDetails.Where(x => x.CustomerId == orderReviewModel.CustomerId && !string.IsNullOrEmpty(x.DeviceToken)).OrderByDescending(x => x.Id).FirstOrDefault()?.DeviceToken;
				fCSNotificationModel.To = FSCToken;
				fCSNotificationModel.Heading = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "nb.customer.notification.orderongoing", languageId, dbcontext);
				fCSNotificationModel.Status = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.TrackOrder.OdrReceivedTxt", languageId, dbcontext);
				var order = dbcontext.Order.Where(x => x.Id == orderReviewModel.OrderId).FirstOrDefault();
				var ProductsIds = dbcontext.OrderItem.Where(x => x.OrderId == orderReviewModel.OrderId).ToList().Select(x => x.ProductId).ToList();
				int PrepareTime = 0;
				int ProductPrepareTime = 0;
				string VendorLatLong = string.Empty;

				foreach (var productId in ProductsIds)
				{
					var productPrepareTime = (from p in dbcontext.Product
											  where p.Id == productId
											  select new
											  {
												  p.PrepareTime
											  }).FirstOrDefault() != null ? (from p in dbcontext.Product
																			 where p.Id == productId
																			 select new
																			 {
																				 p.PrepareTime
																			 }).FirstOrDefault().PrepareTime : 0;
					if (productPrepareTime == null)
					{
						productPrepareTime = 0;
					}
					ProductPrepareTime = productPrepareTime.Value + ProductPrepareTime;
					//var vendorId = dbcontext.Products.Where(x => x.Id == productId).Select(x => x.VendorId).FirstOrDefault();
					//VendorLatLong = dbcontext.Vendors.Where(x => x.Id == vendorId).FirstOrDefault().Geolocation;
				}
				PrepareTime = ProductPrepareTime;

				var ProductId = ProductsIds.FirstOrDefault();
				var VendorName = (from a in dbcontext.Product.Where(x => x.Id == ProductId)
								  join b in dbcontext.Vendor
								  on a.VendorId equals b.Id
								  select new
								  {
									  b
								  }).FirstOrDefault().b.Name;

				order.CreatedOnUtc = Helper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc, dbcontext);
				var OrderFromTime = order.CreatedOnUtc.AddMinutes(PrepareTime).ToString("hh:mm tt");
				var OrderToTime = order.CreatedOnUtc.AddMinutes(PrepareTime + 10).ToString("hh:mm tt");
				fCSNotificationModel.DeliveredTime = "ETA " + PrepareTime + " hrs";

				String strUrl = dbcontext.Setting.AsEnumerable().Where(x => x.Name.ToLower().Equals("setting.customer.notificationurl") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault().Value;
				var sendNotifcationURl = strUrl + "/api/v1/SendFCSNotifications";
				var httpWebRequest = (HttpWebRequest)WebRequest.Create(sendNotifcationURl);

				httpWebRequest.ContentType = "application/json";
				httpWebRequest.Method = "POST";

				string json = JsonConvert.SerializeObject(fCSNotificationModel);
				using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
				{

					streamWriter.Write(json);
					streamWriter.Flush();
					streamWriter.Close();
				}
				var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
				using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
				{
					var result = JsonConvert.DeserializeObject<FCSResultModel>(streamReader.ReadToEnd());

					return result.Result.ToString();
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				throw;
			}
		}

		/// <summary>
		/// send Notification to Merchant
		/// </summary>
		/// <param name="order"></param>
		/// <param name="orderReviewModel"></param>
		/// <param name="languageId"></param>
		/// <param name="dbcontext"></param>
		/// <returns></returns>
		public static bool sendNotificationToMerchant(Order order, OrderReviewModel orderReviewModel, int languageId, ecuadordevContext dbcontext)
		{
			try
			{
				var result = string.Empty;
				var productId = dbcontext.OrderItem.Where(x => x.OrderId == order.Id).FirstOrDefault().ProductId;
				var vendorId = dbcontext.Product.Where(x => x.Id == productId).FirstOrDefault().VendorId;
				var vendor = dbcontext.Vendor.Where(x => x.Id == vendorId).FirstOrDefault();
				var customer = dbcontext.Customer.Where(x => x.VendorId == vendor.Id).FirstOrDefault().Id;
				var deviceToken = dbcontext.GenericAttribute.Where(x => x.EntityId == customer && x.Key == "NestorBird_DeviceToken" && x.StoreId == orderReviewModel.StoreId).FirstOrDefault()?.Value ?? "";
				if (!string.IsNullOrEmpty(deviceToken))
				{
					var webKey = dbcontext.Setting.Where(x => x.Name.Contains("merchantapisettings.merchantappwebkey") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault()?.Value ?? "";
					var webAddr = "https://fcm.googleapis.com/fcm/send";
					var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
					httpWebRequest.ContentType = "application/json";
					httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, webKey);
					httpWebRequest.Method = "POST";

					var heading = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "NB.Merchant.OrderPlaced.Title", languageId, dbcontext);
					var text = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "NB.Merchant.OrderPlaced.Text", languageId, dbcontext);
					using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
					{
						var strNJson = @"{
                          ""to"": """ + deviceToken + @""",
                            
                            ""notification"": {
                                ""title"": """ + heading + @""",
                                ""text"": """ + text + @""",
                                ""sound"":""" + "default" + @""",

								}}";

						streamWriter.Write(strNJson);
						streamWriter.Flush();
					}
					var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
					using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
					{
						result = streamReader.ReadToEnd();
					}

				}
				var results = (string.IsNullOrEmpty(result)) ? false : true;
				return results;
			}
			catch (Exception ex)
			{
				SentryLogs(ex);
				throw;
			}

		}

		public static string sendNotificationToCustomerV2_1(OrderReviewModel orderReviewModel, int languageId, ecuadordevContext dbcontext, int userId)
		{
			try
			{
				FCSNotificationModel fCSNotificationModel = new FCSNotificationModel();
				fCSNotificationModel.ApiKey = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.customerapinotificationkey") && x.StoreId == orderReviewModel.StoreId).Any() ?
				dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.customerapinotificationkey") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault()?.Value : "";
				fCSNotificationModel.OrderId = orderReviewModel.OrderId.ToString();
				fCSNotificationModel.CustomerId = orderReviewModel.CustomerId.ToString();
				fCSNotificationModel.StoreId = orderReviewModel.StoreId.ToString();
				fCSNotificationModel.UniqueSeoCode = orderReviewModel.UniqueSeoCode;
				var FSCToken = dbcontext.CustomerDetails.Where(x => x.CustomerId == orderReviewModel.CustomerId && !string.IsNullOrEmpty(x.DeviceToken)).OrderByDescending(x => x.Id).FirstOrDefault()?.DeviceToken;
				fCSNotificationModel.To = FSCToken;
				fCSNotificationModel.Heading = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "nb.customer.notification.orderongoing", languageId, dbcontext);
				fCSNotificationModel.Status = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.TrackOrder.OdrReceivedTxt", languageId, dbcontext);
				var order = dbcontext.Order.Where(x => x.Id == orderReviewModel.OrderId).FirstOrDefault();
				var ProductsIds = dbcontext.OrderItem.Where(x => x.OrderId == orderReviewModel.OrderId).ToList().Select(x => x.ProductId).ToList();
				int PrepareTime = 0;
				int ProductPrepareTime = 0;
				string VendorLatLong = string.Empty;

				foreach (var productId in ProductsIds)
				{
					var productPrepareTime = (from p in dbcontext.Product
											  where p.Id == productId
											  select new
											  {
												  p.PrepareTime
											  }).FirstOrDefault() != null ? (from p in dbcontext.Product
																			 where p.Id == productId
																			 select new
																			 {
																				 p.PrepareTime
																			 }).FirstOrDefault().PrepareTime : 0;
					if (productPrepareTime == null)
					{
						productPrepareTime = 0;
					}
					ProductPrepareTime = productPrepareTime.Value + ProductPrepareTime;
				}
				PrepareTime = ProductPrepareTime;
				var ProductId = ProductsIds.FirstOrDefault();
				var VendorName = (from a in dbcontext.Product.Where(x => x.Id == ProductId)
								  join b in dbcontext.Vendor
								  on a.VendorId equals b.Id
								  select new
								  {
									  b
								  }).FirstOrDefault().b.Name;

				order.CreatedOnUtc = Helper.ConvertToUserTimeV2_1(order.CreatedOnUtc, DateTimeKind.Utc, dbcontext, userId);
				var OrderFromTime = order.CreatedOnUtc.AddMinutes(PrepareTime).ToString("hh:mm tt");
				var OrderToTime = order.CreatedOnUtc.AddMinutes(PrepareTime + 10).ToString("hh:mm tt");
				fCSNotificationModel.DeliveredTime = "ETA " + PrepareTime + " hrs";

				String strUrl = dbcontext.Setting.AsEnumerable().Where(x => x.Name.ToLower().Equals("setting.customer.notificationurl") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault().Value;
				var sendNotifcationURl = strUrl + "/api/v2.1/SendFCSNotifications";
				var httpWebRequest = (HttpWebRequest)WebRequest.Create(sendNotifcationURl);

				httpWebRequest.ContentType = "application/json";
				httpWebRequest.Method = "POST";

				string json = JsonConvert.SerializeObject(fCSNotificationModel);
				using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
				{

					streamWriter.Write(json);
					streamWriter.Flush();
					streamWriter.Close();
				}
				var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
				using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
				{
					var result = JsonConvert.DeserializeObject<FCSResultModel>(streamReader.ReadToEnd());

					return result.Result.ToString();
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				throw;
			}
		}

		public static decimal GetProductAttributeValuePriceAdjustment(ProductAttributeValue value, ecuadordevContext context)
		{
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			var adjustment = decimal.Zero;
			switch (value.AttributeValueTypeId)
			{
				case (int)AttributeValueType.Simple:
					{
						//simple attribute
						adjustment = value.PriceAdjustment;
					}
					break;
				case (int)AttributeValueType.AssociatedToProduct:
					{
						var associatedProduct = context.Product.AsEnumerable().Where(x => x.Id == value.AssociatedProductId).FirstOrDefault();
						if (associatedProduct != null)
						{
							adjustment = associatedProduct.Price * value.Quantity;
						}
						//bundled product

					}
					break;
				default:
					break;
			}

			return adjustment;
		}
		public static IList<int> ParseProductAttributeMappingIds(string attributesXml)
		{
			var ids = new List<int>();
			if (string.IsNullOrEmpty(attributesXml))
				return ids;

			try
			{
				var xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(attributesXml);

				var nodeList1 = xmlDoc.SelectNodes(@"//Attributes/ProductAttribute");
				foreach (XmlNode node1 in nodeList1)
				{
					if (node1.Attributes != null && node1.Attributes["ID"] != null)
					{
						var str1 = node1.Attributes["ID"].InnerText.Trim();
						if (int.TryParse(str1, out int id))
						{
							ids.Add(id);
						}
					}
				}
			}
			catch (Exception exc)
			{
				Helper.SentryLogs(exc);
				Debug.Write(exc.ToString());
			}
			return ids;
		}
		public static IList<ProductProductAttributeMapping> ParseProductAttributeMappings(string attributesXml, ecuadordevContext context)
		{
			var result = new List<ProductProductAttributeMapping>();
			if (string.IsNullOrEmpty(attributesXml))
				return result;

			var ids = ParseProductAttributeMappingIds(attributesXml);
			foreach (var id in ids)
			{
				var attribute = context.ProductProductAttributeMapping.AsEnumerable().Where(x => x.Id == id).FirstOrDefault();
				if (attribute != null)
				{
					result.Add(attribute);
				}
			}
			return result;

		}
		public static string FormatAttributes(/*Product product1,*/ string attributesXml,
		   string separator = "<br />", bool htmlEncode = true, bool renderPrices = true,
			bool renderProductAttributes = true, bool renderGiftCardAttributes = true,
			bool allowHyperlinks = true, ecuadordevContext context = null)
		{
			var result = new StringBuilder();

			//attributes
			if (renderProductAttributes)
			{
				foreach (var attribute in ParseProductAttributeMappings(attributesXml, context))
				{
					foreach (var attributeValue in ParseProductAttributeValues(attributesXml, attribute.Id, context))
					{
						var formattedAttribute = string.Empty;
						formattedAttribute = $"{attributeValue.Name}";

						if (renderPrices)
						{
							var attributeValuePriceAdjustment = GetProductAttributeValuePriceAdjustment(attributeValue, context);
						}



						//encode (if required)
						if (htmlEncode)
							formattedAttribute = WebUtility.HtmlEncode(formattedAttribute);

						if (!string.IsNullOrEmpty(formattedAttribute))
						{
							if (result.Length > 0)
								result.Append(separator);
							result.Append(formattedAttribute);
						}
					}

				}
			}
			var res = result.ToString();
			res = HttpUtility.HtmlDecode(Regex.Replace(res, "<.*?>", String.Empty)); ;

			return res;
		}
		public static IList<ProductAttributeValue> ParseProductAttributeValues(string attributesXml, int productAttributeMappingId = 0, ecuadordevContext context = null)
		{
			var values = new List<ProductAttributeValue>();
			if (string.IsNullOrEmpty(attributesXml))
				return values;

			var attributes = ParseProductAttributeMappings(attributesXml, context);

			//to load values only for the passed product attribute mapping
			if (productAttributeMappingId > 0)
				attributes = attributes.Where(attribute => attribute.Id == productAttributeMappingId).ToList();

			foreach (var attribute in attributes)
			{

				foreach (var attributeValue in ParseValuesWithQuantity(attributesXml, attribute.Id))
				{
					if (!string.IsNullOrEmpty(attributeValue.Item1) && int.TryParse(attributeValue.Item1, out int attributeValueId))
					{
						var value = context.ProductAttributeValue.AsEnumerable().Where(x => x.Id == attributeValueId).FirstOrDefault();
						if (value != null)
						{
							if (!string.IsNullOrEmpty(attributeValue.Item2) && int.TryParse(attributeValue.Item2, out int quantity) && quantity != value.Quantity)
							{
								//if customer enters quantity, use new entity with new quantity
								ProductAttributeValue oldValue = new ProductAttributeValue();
								oldValue.ProductAttributeMapping = attribute;
								oldValue.Quantity = quantity;
								values.Add(oldValue);
							}
							else
								values.Add(value);
						}
					}
				}
			}
			return values;

		}
		public static IList<Tuple<string, string>> ParseValuesWithQuantity(string attributesXml, int productAttributeMappingId)
		{
			var selectedValues = new List<Tuple<string, string>>();
			if (string.IsNullOrEmpty(attributesXml))
				return selectedValues;

			try
			{
				var xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(attributesXml);

				foreach (XmlNode attributeNode in xmlDoc.SelectNodes(@"//Attributes/ProductAttribute"))
				{
					if (attributeNode.Attributes != null && attributeNode.Attributes["ID"] != null)
					{
						if (int.TryParse(attributeNode.Attributes["ID"].InnerText.Trim(), out int attributeId) && attributeId == productAttributeMappingId)
						{
							foreach (XmlNode attributeValue in attributeNode.SelectNodes("ProductAttributeValue"))
							{
								var value = attributeValue.SelectSingleNode("Value").InnerText.Trim();
								var quantityNode = attributeValue.SelectSingleNode("Quantity");
								selectedValues.Add(new Tuple<string, string>(value, quantityNode != null ? quantityNode.InnerText.Trim() : string.Empty));
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
			}

			return selectedValues;
		}
		public static CustomerAPIResponses ApplyCoupon(int CustomerId, string CouponCode, decimal subTotal, ecuadordevContext dbcontext, int storeId, int languageId)
		{
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();

			try
			{
				var isCustomerExist = dbcontext.Customer.Where(x => x.Id == CustomerId).Any();
				if (!isCustomerExist)
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
				var isCouponCodeList = dbcontext.Discount.AsEnumerable().Where(x => x.DiscountTypeId == (int)DiscountType.AssignedToOrderTotal).ToList();
				CouponCode = CouponCode.ToUpper().ToString();
				var isCouponCodeExist = isCouponCodeList.Where(x => x.CouponCode != null && x.CouponCode.ToUpper().ToString().Equals(CouponCode)).FirstOrDefault();
				if (isCouponCodeExist != null)
				{
					if (isCouponCodeExist.StartDateUtc <= DateTime.UtcNow && isCouponCodeExist.EndDateUtc >= DateTime.UtcNow)
					{
						if (isCouponCodeExist.UsePercentage)
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (isCouponCodeExist.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (isCouponCodeExist.MaximumDiscountPercentageAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)isCouponCodeExist.MaximumDiscountPercentageAmount / 100f);
									if (isCouponCodeExist.DiscountAmount < maximumDiscount)
										maximumDiscount = isCouponCodeExist.DiscountAmount;
								}
								else
									maximumDiscount = isCouponCodeExist.DiscountAmount;

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = isCouponCodeExist.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + isCouponCodeExist.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
						else
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (isCouponCodeExist.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (isCouponCodeExist.MaximumDiscountPercentageAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)isCouponCodeExist.MaximumDiscountPercentageAmount / 100f);
									if (isCouponCodeExist.DiscountAmount < maximumDiscount)
										maximumDiscount = isCouponCodeExist.DiscountAmount;
								}
								else
									maximumDiscount = isCouponCodeExist.DiscountAmount;

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = isCouponCodeExist.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + isCouponCodeExist.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
					}
					else if (isCouponCodeExist.StartDateUtc == null && isCouponCodeExist.EndDateUtc >= DateTime.UtcNow)
					{
						if (isCouponCodeExist.UsePercentage)
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (isCouponCodeExist.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, db: dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (isCouponCodeExist.MaximumDiscountPercentageAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)isCouponCodeExist.MaximumDiscountPercentageAmount / 100f);
									if (isCouponCodeExist.DiscountAmount < maximumDiscount)
										maximumDiscount = isCouponCodeExist.DiscountAmount;
								}
								else
									maximumDiscount = isCouponCodeExist.DiscountAmount;

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = isCouponCodeExist.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + isCouponCodeExist.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
						else
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (isCouponCodeExist.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, db: dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (isCouponCodeExist.MaximumDiscountPercentageAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)isCouponCodeExist.MaximumDiscountPercentageAmount / 100f);
									if (isCouponCodeExist.DiscountAmount < maximumDiscount)
										maximumDiscount = isCouponCodeExist.DiscountAmount;
								}
								else
									maximumDiscount = isCouponCodeExist.DiscountAmount;

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = isCouponCodeExist.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + isCouponCodeExist.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
					}
					else if (isCouponCodeExist.StartDateUtc <= DateTime.UtcNow && isCouponCodeExist.EndDateUtc == null)
					{
						if (isCouponCodeExist.UsePercentage)
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (isCouponCodeExist.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (isCouponCodeExist.MaximumDiscountPercentageAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)isCouponCodeExist.MaximumDiscountPercentageAmount / 100f);
									if (isCouponCodeExist.DiscountAmount < maximumDiscount)
										maximumDiscount = isCouponCodeExist.DiscountAmount;
								}
								else
									maximumDiscount = isCouponCodeExist.DiscountAmount;

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = isCouponCodeExist.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + isCouponCodeExist.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
						else
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (isCouponCodeExist.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (isCouponCodeExist.MaximumDiscountPercentageAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)isCouponCodeExist.MaximumDiscountPercentageAmount / 100f);
									if (isCouponCodeExist.DiscountAmount < maximumDiscount)
										maximumDiscount = isCouponCodeExist.DiscountAmount;
								}
								else
									maximumDiscount = isCouponCodeExist.DiscountAmount;

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = isCouponCodeExist.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + isCouponCodeExist.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
					}
					else if (isCouponCodeExist.StartDateUtc != null)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodenotapplicable", languageId, dbcontext);

						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
					else if (isCouponCodeExist.EndDateUtc != null)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodenotapplicable", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
					else if (isCouponCodeExist.EndDateUtc != null && isCouponCodeExist.EndDateUtc != null)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodenotapplicable", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
					else
					{
						if (isCouponCodeExist.UsePercentage)
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (isCouponCodeExist.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (isCouponCodeExist.MaximumDiscountPercentageAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)isCouponCodeExist.MaximumDiscountPercentageAmount / 100f);
									if (isCouponCodeExist.DiscountAmount < maximumDiscount)
										maximumDiscount = isCouponCodeExist.DiscountAmount;
								}
								else
									maximumDiscount = isCouponCodeExist.DiscountAmount;

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = isCouponCodeExist.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + isCouponCodeExist.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
						else
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (isCouponCodeExist.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < isCouponCodeExist.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (isCouponCodeExist.MaximumDiscountPercentageAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)isCouponCodeExist.MaximumDiscountPercentageAmount / 100f);
									if (isCouponCodeExist.DiscountAmount < maximumDiscount)
										maximumDiscount = isCouponCodeExist.DiscountAmount;
								}
								else
									maximumDiscount = isCouponCodeExist.DiscountAmount;

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = isCouponCodeExist.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + isCouponCodeExist.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
					}
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodenotapplicable", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				throw;
			}
		}
		public static List<DiscountUsageHistory> GetAllDiscountUsageHistory(int? discountId = null,
			int? customerId = null, int? orderId = null, int pageIndex = 0, int pageSize = int.MaxValue, ecuadordevContext db = null)
		{
			var discountUsageHistory = db.DiscountUsageHistory.AsEnumerable().ToList();

			//filter by discount
			if (discountId.HasValue && discountId.Value > 0)
				discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.DiscountId == discountId.Value).ToList();

			//filter by customer
			if (customerId.HasValue && customerId.Value > 0)
				discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.Order != null && historyRecord.Order.CustomerId == customerId.Value).ToList();

			//filter by order
			if (orderId.HasValue && orderId.Value > 0)
				discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.OrderId == orderId.Value).ToList();

			//ignore deleted orders
			discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.Order != null && !historyRecord.Order.Deleted).ToList();

			//order
			discountUsageHistory = discountUsageHistory.OrderByDescending(historyRecord => historyRecord.CreatedOnUtc).ThenBy(historyRecord => historyRecord.Id).ToList();
			return discountUsageHistory;
		}

		public static string GetStripeKey(int storeId, ecuadordevContext db)
		{
			var stripeKey = db.Setting.FirstOrDefault(setting => setting.Name.Contains("stripepaymentsettings.secretkey")
						  && setting.StoreId == storeId)?.Value ?? "";
			return stripeKey;

		}

		public static string GetConnectionString()
		{
			try
			{
				return (new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("ConnectionStrings").GetSection("ecuadordatabase").Value).ToString();
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}
		public static void SentryLogs(Exception exception)
		{
			//var ravenClient = new RavenClient(sentryURL);
			//ravenClient.Capture(new SentryEvent(exception));
		}

		public static bool IsNumeric(string strValue)
		{
			int intValue;
			return int.TryParse(strValue, out intValue);
		}

		public static decimal GetTaxRate(Product product, int storeId, ecuadordevContext db)
		{
			decimal result = 0;
			try
			{
				if (product == null || product.VendorId == 0 || string.IsNullOrEmpty(product.TaxCategoryIds))
				{
					return result;
				}
				var address = (from a in db.Address
							   join v in db.Vendor
							   on a.Id equals v.AddressId
							   where v.Id == product.VendorId
							   select a).FirstOrDefault();
				if (address == null)
				{
					return result;
				}

				var allTaxRates = db.TaxRate.Select(taxRate => new
				{
					Id = taxRate.Id,
					StoreId = taxRate.StoreId,
					TaxCategoryId = taxRate.TaxCategoryId,
					CountryId = taxRate.CountryId,
					StateProvinceId = taxRate.StateProvinceId,
					Zip = taxRate.Zip,
					Percentage = taxRate.Percentage
				}).ToList();

				var countryId = address.CountryId;
				var stateProvinceId = address.StateProvinceId;
				var zip = address.ZipPostalCode?.Trim() ?? string.Empty;

				//get tax by tax categories
				var taxCategoryIds = new List<int>();
				if (!string.IsNullOrEmpty(product?.TaxCategoryIds))
					taxCategoryIds = product.TaxCategoryIds.Split(',').Select(x => int.Parse(x)).ToList();
				var existingRates = allTaxRates.Where(taxRate => taxRate.CountryId == countryId && taxCategoryIds.Contains(taxRate.TaxCategoryId));

				//filter by store
				var matchedByStore = existingRates.Where(taxRate => storeId == taxRate.StoreId || taxRate.StoreId == 0);

				//filter by state/province
				var matchedByStateProvince = matchedByStore.Where(taxRate => stateProvinceId == taxRate.StateProvinceId || taxRate.StateProvinceId == 0);

				//filter by zip
				var matchedByZip = matchedByStateProvince.Where(taxRate => string.IsNullOrWhiteSpace(taxRate.Zip) || taxRate.Zip.Equals(zip, StringComparison.InvariantCultureIgnoreCase));

				//sort from particular to general, more particular cases will be the first
				var foundRecords = matchedByZip.OrderBy(r => r.StoreId == 0).ThenBy(r => r.StateProvinceId == 0).ThenBy(r => string.IsNullOrEmpty(r.Zip));

				var foundRecord = foundRecords.ToList();

				if (foundRecord.Count > 0)
					result = foundRecord.Sum(x => x.Percentage);
			}
			catch (Exception ex)
			{
				//no need to show ex
			}
			return result;
		}

		public static string GetProductPictureUrl(Product product, int pictureSize, string baseUrl, ecuadordevContext dbcontext)
		{
			string productImageURL = string.Format("{0}default-prod-image_{1}.png?{2}", baseUrl, pictureSize, DateTime.Now.ToFileTime());
			var picture = (from ppm in dbcontext.ProductPictureMapping.Where(x => x.ProductId == product.Id)
						   join pic in dbcontext.Picture
						   on ppm.PictureId equals pic.Id
						   select pic).FirstOrDefault();
			if (picture != null)
			{
				if (picture != null)
				{

					string lastPart = GetFileExtensionFromMimeType(picture.MimeType);
					string thumbFileName = !string.IsNullOrEmpty(picture.SeoFilename)
					? $"{picture.Id:0000000}_{picture.SeoFilename}.{lastPart}" + "?" + DateTime.Now.ToFileTime()
					: $"{picture.Id:0000000}_{pictureSize}.{lastPart}" + "?" + DateTime.Now.ToFileTime();

					productImageURL = baseUrl + thumbFileName + "?" + DateTime.Now.ToFileTime();
				}
			}
			return productImageURL;
		}

		public static List<CooknRestProductProductAttributesResponse> GetProductAttributes(Product product, string currencySymbol, ecuadordevContext dbcontext)
		{
			List<CooknRestProductProductAttributesResponse> productAttributes = new List<CooknRestProductProductAttributesResponse>();
			productAttributes = (from _e in dbcontext.ProductProductAttributeMapping
								 join _f in dbcontext.ProductAttribute
								 on _e.ProductAttributeId equals _f.Id
								 where _e.ProductId == product.Id
								 select new CooknRestProductProductAttributesResponse
								 {
									 AttributeName = _f.Name,
									 IsRequired = _e.IsRequired,
									 AttributeTypeId = _e.AttributeControlTypeId,
									 ProductId = _e.ProductId,
									 ProductAttributeId = _f.Id,
									 ProductAttributeMappingId = _e.Id,
									 ProductAttribute = (from __d in dbcontext.ProductAttributeValue
														 where __d.ProductAttributeMappingId == _e.Id
														 select new ProductAttributes
														 {
															 Name = __d.Name,
															 Price = __d.PriceAdjustment.ToString(),
															 ProductAttributeValueId = __d.Id,
															 IsPreSelected = __d.IsPreSelected,
															 Currency = currencySymbol
														 }).ToList()
								 }).Distinct().ToList();
			return productAttributes;
		}

		public static ItemsAPIResponses GetAPIErrorResponses(string errorMessage, int StatusCode = (int)HttpStatusCode.NoContent)
		{
			return new ItemsAPIResponses
			{
				ErrorMessageTitle = "Error!!",
				ErrorMessage = errorMessage,
				Status = false,
				StatusCode = StatusCode,
				ResponseObj = null
			};
		}

		/// <summary>
		/// Functionality to calculate delivery fee
		/// </summary>
		/// <returns>returns Delivery Fees in decimal </returns>
		public static decimal CalculateDeliveryFee(int RestnCookId, int BillingAddressId, int storeId, ecuadordevContext dbcontext)
		{
			decimal distance = 0;
			decimal deliveryFee = 0;

			var merchant = dbcontext.Vendor.FirstOrDefault(x => x.Id == RestnCookId);
			if (merchant != null)
			{
				if (merchant.FixedOrDynamic == "f")
				{
					deliveryFee = merchant.DeliveryCharge;
				}
				else if (merchant.FixedOrDynamic == "d")
				{
					deliveryFee = merchant.PriceForDynamic;
					try
					{
						var venLtLngStr = merchant.Geolocation;
						var BillingAddress = dbcontext.Address.FirstOrDefault(x => x.Id == BillingAddressId);
						if (venLtLngStr.Any() && BillingAddress != null && !string.IsNullOrEmpty(BillingAddress.Address1))
						{
							var lat = string.Empty;
							var lon = string.Empty;
							string[] venLtLngStrArr = venLtLngStr.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
							if (venLtLngStrArr.Length > 0)
							{
								lat = venLtLngStrArr[0].ToString();
								lon = venLtLngStrArr[1].ToString();
							}
							if (!string.IsNullOrEmpty(lat) && !string.IsNullOrEmpty(lon))
							{
								decimal number = 0;
								var distanceString = GetDistance(string.Format("{0},{1}", lat, lon), BillingAddress.Address1, storeId, dbcontext);
								if (Decimal.TryParse(distanceString, out number))
								{
									if (merchant.KmOrMilesForDynamic == "k")
									{
										distance = Math.Round(number / 1000, 1);
									}
									else if (merchant.KmOrMilesForDynamic == "m")
									{
										distance = Math.Round(number / Convert.ToDecimal(1609.34), 1);
									}
								}
							}
						}
					}
					catch { }

					if (distance > merchant.DistanceForDynamic)
					{
						deliveryFee += (distance - merchant.DistanceForDynamic) * merchant.PricePerUnitDistanceForDynamic;
					}
				}
			}

			return deliveryFee;
		}

		/// <summary>
		/// Functionality to calculate delivery fee
		/// </summary>
		/// <returns>returns Delivery Fees in decimal </returns>
		public static string GetDistance(string origin, string destination, int storeId, ecuadordevContext dbcontext)
		{
			string result = string.Empty;
			var keyObject = dbcontext.Setting.Where(x => x.Name == "NB.Google.Account.Key" && x.StoreId == storeId).FirstOrDefault();
			string key = keyObject != null ? keyObject.Value : string.Empty;
			string url = @"https://maps.googleapis.com/maps/api/distancematrix/xml?origins=" + origin + "&destinations=" + destination + "&sensor=false&key=" + key;

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			WebResponse response = request.GetResponse();
			Stream dataStream = response.GetResponseStream();
			StreamReader sreader = new StreamReader(dataStream);
			string responsereader = sreader.ReadToEnd();
			response.Close();

			DataSet ds = new DataSet();
			ds.ReadXml(new XmlTextReader(new StringReader(responsereader)));
			if (ds.Tables.Count > 0)
			{
				if (ds.Tables["element"] != null && ds.Tables["element"].Rows[0]["status"] != null)
				{
					if (ds.Tables["element"].Rows[0]["status"].ToString() == "OK")
					{
						result = ds.Tables["distance"].Rows[0]["value"].ToString();
					}
				}

			}
			return result;
		}

		public static decimal CalculateServiceChargeAmount(decimal subTotal, int storeId, ecuadordevContext dbcontext)
		{
			decimal serviceChargeAmount = 0;

			if (storeId == 0)
				return serviceChargeAmount;
			var usePercentageObject = dbcontext.Setting.Where(x => x.Name == "setting.storesetup.UsePercentage" && x.StoreId == storeId).FirstOrDefault();
			bool usePercentage = usePercentageObject != null ? Convert.ToBoolean(usePercentageObject.Value) : false;

			var flatAmountObject = dbcontext.Setting.Where(x => x.Name == "setting.storesetup.FlatAmount" && x.StoreId == storeId).FirstOrDefault();
			decimal flatAmount = flatAmountObject != null ? Convert.ToDecimal(flatAmountObject.Value) : 0;

			var serviceChargePercentageObject = dbcontext.Setting.Where(x => x.Name == "setting.storesetup.ServiceChargePercentage" && x.StoreId == storeId).FirstOrDefault();
			decimal serviceChargePercentage = serviceChargePercentageObject != null ? Convert.ToDecimal(serviceChargePercentageObject.Value) : 0;

			var maximumServiceChargeAmountObject = dbcontext.Setting.Where(x => x.Name == "setting.storesetup.maximumservicechargeamount" && x.StoreId == storeId).FirstOrDefault();
			decimal maximumServiceChargeAmount = maximumServiceChargeAmountObject != null ? Convert.ToDecimal(maximumServiceChargeAmountObject.Value) : 0;


			if (usePercentage)
			{
				if (subTotal > 0 && serviceChargePercentage > 0)
				{
					var serviceAmount = (subTotal * serviceChargePercentage) / 100;

					if (serviceAmount > maximumServiceChargeAmount)
					{
						serviceChargeAmount = maximumServiceChargeAmount;
					}
					else
					{
						serviceChargeAmount = serviceAmount;
					}
				}
			}
			else
			{
				serviceChargeAmount = flatAmount;
			}

			return serviceChargeAmount;
		}

		#region PayPal section
		/// <summary>
		/// Get PayPal Payment Url
		/// </summary>
		/// <param name="order"></param>
		/// <param name="dbcontext"></param>
		/// <param name="httpRequest"></param>
		/// <returns>return paypal url</returns>
		public static string GetPayPalPaymentUrl(Order order, ecuadordevContext dbcontext, HttpContext httpRequest)
		{
			string sendBox = dbcontext.Setting.Where(x => x.Name.ToLower().Trim().Equals("paypalstandardpaymentsettings.usesandbox") && x.StoreId == order.StoreId).FirstOrDefault()?.Value ?? "";

			var baseUrl = !string.IsNullOrEmpty(sendBox) && Convert.ToBoolean(sendBox) ?
				"https://www.sandbox.paypal.com/us/cgi-bin/webscr" :
				"https://www.paypal.com/us/cgi-bin/webscr";

			//create common query parameters for the request
			var queryParameters = CreateQueryParameters(order, dbcontext, httpRequest);

			string passProductNamesAndTotals = dbcontext.Setting.Where(x => x.Name.ToLower().Trim().Equals("paypalstandardpaymentsettings.passproductnamesandtotals") && x.StoreId == order.StoreId).FirstOrDefault()?.Value ?? "";

			//whether to include order items in a transaction
			if (!string.IsNullOrEmpty(passProductNamesAndTotals) && Convert.ToBoolean(passProductNamesAndTotals))
			{
				//add order items query parameters to the request
				var parameters = new Dictionary<string, string>(queryParameters);
				AddItemsParameters(parameters, order, dbcontext);

				//remove null values from parameters
				parameters = parameters.Where(parameter => !string.IsNullOrEmpty(parameter.Value))
					.ToDictionary(parameter => parameter.Key, parameter => parameter.Value);

				//ensure redirect URL doesn't exceed 2K chars to avoid "too long URL" exception
				var redirectUrl = QueryHelpers.AddQueryString(baseUrl, parameters);
				if (redirectUrl.Length <= 2048)
				{
					return redirectUrl;
				}
			}

			//or add only an order total query parameters to the request
			AddOrderTotalParameters(queryParameters, order, dbcontext);

			//remove null values from parameters
			queryParameters = queryParameters.Where(parameter => !string.IsNullOrEmpty(parameter.Value))
				.ToDictionary(parameter => parameter.Key, parameter => parameter.Value);

			var url = QueryHelpers.AddQueryString(baseUrl, queryParameters);

			return url;
		}

		/// <summary>
		/// Create common query parameters for the request
		/// </summary>
		/// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
		/// <returns>Created query parameters</returns>
		private static IDictionary<string, string> CreateQueryParameters(Order order, ecuadordevContext dbcontext, HttpContext httpRequest)
		{
			//get store location
			var storeLocation = httpRequest.Request;

			//choosing correct order address
			var orderAddress = order.PickupInStore
					? order.PickupAddress
					: order.ShippingAddress;

			string businessEmail = dbcontext.Setting.Where(x => x.Name.ToLower().Trim().Equals("paypalstandardpaymentsettings.businessemail") && x.StoreId == order.StoreId).FirstOrDefault()?.Value ?? "";

			var storeUrl = dbcontext.Store.Where(x => x.Id == order.StoreId).FirstOrDefault().Url;

			//create query parameters
			return new Dictionary<string, string>
			{
				//PayPal ID or an email address associated with your PayPal account
				["business"] = businessEmail,

				//the character set and character encoding
				["charset"] = "utf-8",

				//set return method to "2" (the customer redirected to the return URL by using the POST method, and all payment variables are included)
				["rm"] = "2",

				["bn"] = PayPalHelper.NopCommercePartnerCode,
				["currency_code"] = order.CustomerCurrencyCode,

				//order identifier
				["invoice"] = order.CustomOrderNumber,
				["custom"] = order.OrderGuid.ToString(),

				//PDT, IPN and cancel URL
				["return"] = $"{storeLocation.Scheme}://{storeLocation.Host.Value}/api/PDTHandler",
				["notify_url"] = $"{storeUrl}Plugins/PaymentPayPalStandard/IPNHandler",
				["cancel_return"] = $"{storeLocation.Scheme}://{storeLocation.Host.Value}/api/CancelOrder",

				//shipping address, if exists
				["no_shipping"] = order.ShippingStatusId == (int)ShippingStatus.ShippingNotRequired ? "1" : "2",
				["address_override"] = order.ShippingStatusId == (int)ShippingStatus.ShippingNotRequired ? "0" : "1",
				["first_name"] = orderAddress?.FirstName,
				["last_name"] = orderAddress?.LastName,
				["address1"] = orderAddress?.Address1,
				["address2"] = orderAddress?.Address2,
				["city"] = orderAddress?.City,
				["state"] = orderAddress?.StateProvince?.Abbreviation,
				["country"] = orderAddress?.Country?.TwoLetterIsoCode,
				["zip"] = orderAddress?.ZipPostalCode,
				["email"] = orderAddress?.Email
			};
		}

		/// <summary>
		/// Add order items to the request query parameters
		/// </summary>
		/// <param name="parameters">Query parameters</param>
		/// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
		private static void AddItemsParameters(IDictionary<string, string> parameters, Order order, ecuadordevContext dbcontext)
		{
			//upload order items
			parameters.Add("cmd", "_cart");
			parameters.Add("upload", "1");

			var cartTotal = decimal.Zero;
			var roundedCartTotal = decimal.Zero;
			var itemCount = 1;

			//add shopping cart items
			foreach (var item in order.OrderItem)
			{
				var roundedItemPrice = Math.Round(item.UnitPriceExclTax, 2);

				//add query parameters
				parameters.Add($"item_name_{itemCount}", item.Product.Name);
				parameters.Add($"amount_{itemCount}", roundedItemPrice.ToString("0.00", CultureInfo.InvariantCulture));
				parameters.Add($"quantity_{itemCount}", item.Quantity.ToString());

				cartTotal += item.PriceExclTax;
				roundedCartTotal += roundedItemPrice * item.Quantity;
				itemCount++;
			}

			//add shipping fee as a separate order item, if it has price
			var roundedShippingPrice = Math.Round(order.OrderShippingExclTax, 2);
			if (roundedShippingPrice > decimal.Zero)
			{
				parameters.Add($"item_name_{itemCount}", "Shipping fee");
				parameters.Add($"amount_{itemCount}", roundedShippingPrice.ToString("0.00", CultureInfo.InvariantCulture));
				parameters.Add($"quantity_{itemCount}", "1");

				cartTotal += order.OrderShippingExclTax;
				roundedCartTotal += roundedShippingPrice;
				itemCount++;
			}

			//add payment method additional fee as a separate order item, if it has price
			var roundedPaymentMethodPrice = Math.Round(order.PaymentMethodAdditionalFeeExclTax, 2);
			if (roundedPaymentMethodPrice > decimal.Zero)
			{
				parameters.Add($"item_name_{itemCount}", "Payment method fee");
				parameters.Add($"amount_{itemCount}", roundedPaymentMethodPrice.ToString("0.00", CultureInfo.InvariantCulture));
				parameters.Add($"quantity_{itemCount}", "1");

				cartTotal += order.PaymentMethodAdditionalFeeExclTax;
				roundedCartTotal += roundedPaymentMethodPrice;
				itemCount++;
			}

			//add tax as a separate order item, if it has positive amount
			var roundedTaxAmount = Math.Round(order.OrderTax, 2);
			if (roundedTaxAmount > decimal.Zero)
			{
				parameters.Add($"item_name_{itemCount}", "Tax amount");
				parameters.Add($"amount_{itemCount}", roundedTaxAmount.ToString("0.00", CultureInfo.InvariantCulture));
				parameters.Add($"quantity_{itemCount}", "1");

				cartTotal += order.OrderTax;
				roundedCartTotal += roundedTaxAmount;
			}

			if (cartTotal > order.OrderTotal)
			{
				//get the difference between what the order total is and what it should be and use that as the "discount"
				var discountTotal = Math.Round(cartTotal - order.OrderTotal, 2);
				roundedCartTotal -= discountTotal;

				//gift card or rewarded point amount applied to cart in nopCommerce - shows in PayPal as "discount"
				parameters.Add("discount_amount_cart", discountTotal.ToString("0.00", CultureInfo.InvariantCulture));
			}

			//save order total that actually sent to PayPal (used for PDT order total validation)
			GenericAttribute genericAttributeGender = new GenericAttribute
			{
				Key = PayPalHelper.OrderTotalSentToPayPal,
				EntityId = order.Id,
				Value = roundedCartTotal.ToString(),
				KeyGroup = "Order",
				StoreId = order.StoreId
			};
			dbcontext.GenericAttribute.Add(genericAttributeGender);
			dbcontext.SaveChanges();
		}

		/// <summary>
		/// Add order total to the request query parameters
		/// </summary>
		/// <param name="parameters">Query parameters</param>
		/// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
		private static void AddOrderTotalParameters(IDictionary<string, string> parameters, Order order, ecuadordevContext dbcontext)
		{
			//round order total
			var roundedOrderTotal = Math.Round(order.OrderTotal, 2);

			parameters.Add("cmd", "_xclick");
			parameters.Add("item_name", $"Order Number {order.CustomOrderNumber}");
			parameters.Add("amount", roundedOrderTotal.ToString("0.00", CultureInfo.InvariantCulture));

			//save order total that actually sent to PayPal (used for PDT order total validation)
			GenericAttribute genericAttributeGender = new GenericAttribute
			{
				Key = PayPalHelper.OrderTotalSentToPayPal,
				EntityId = order.Id,
				Value = roundedOrderTotal.ToString(),
				KeyGroup = "Order",
				StoreId = order.StoreId
			};
			dbcontext.GenericAttribute.Add(genericAttributeGender);
			dbcontext.SaveChanges();
		}

		/// <summary>
		/// Gets a value indicating whether order can be marked as paid
		/// </summary>
		/// <param name="order">Order</param>
		/// <returns>A value indicating whether order can be marked as paid</returns>
		public static bool CanMarkOrderAsPaid(Models.Order order)
		{
			if (order.PaymentStatusId == (int)PaymentStatus.Paid ||
				order.PaymentStatusId == (int)PaymentStatus.Refunded ||
				order.PaymentStatusId == (int)PaymentStatus.Voided)
				return false;

			return true;
		}

		/// <summary>
		/// Gets PDT details
		/// </summary>
		/// <param name="tx">TX</param>
		/// <param name="values">Values</param>
		/// <param name="response">Response</param>
		/// <returns>Result</returns>
		public static bool GetPdtDetails(string tx, string customOrderNumber, ecuadordevContext db, out Dictionary<string, string> values, out string response)
		{
			response = WebUtility.UrlDecode(GetPdtDetailsAsync(tx, customOrderNumber, db).Result);

			values = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
			bool firstLine = true, success = false;
			foreach (var l in response.Split('\n'))
			{
				var line = l.Trim();
				if (firstLine)
				{
					success = line.Equals("SUCCESS", StringComparison.OrdinalIgnoreCase);
					firstLine = false;
				}
				else
				{
					var equalPox = line.IndexOf('=');
					if (equalPox >= 0)
						values.Add(line.Substring(0, equalPox), line.Substring(equalPox + 1));
				}
			}

			return success;
		}


		/// <summary>
		/// Gets PDT details
		/// </summary>
		/// <param name="tx">TX</param>
		/// <returns>The asynchronous task whose result contains the PDT details</returns>
		public static async Task<string> GetPdtDetailsAsync(string tx, string customOrderNumber, ecuadordevContext dbcontext)
		{
			HttpClient client = new HttpClient();
			//configure client
			client.Timeout = TimeSpan.FromMilliseconds(5000);
			client.DefaultRequestHeaders.Add("User-Agent", $"nopCommerce-4.20");

			var orderNumberGuid = Guid.Empty;
			try
			{
				orderNumberGuid = new Guid(customOrderNumber);
			}
			catch
			{
				// ignored
			}

			var order = dbcontext.Order.Where(x => x.OrderGuid == orderNumberGuid).FirstOrDefault();

			//get response
			string sendBox = dbcontext.Setting.Where(x => x.Name.ToLower().Trim().Equals("paypalstandardpaymentsettings.usesandbox") && x.StoreId == order.StoreId).FirstOrDefault()?.Value ?? "";

			//get response
			string pdtToken = dbcontext.Setting.Where(x => x.Name.ToLower().Trim().Equals("paypalstandardpaymentsettings.pdttoken") && x.StoreId == order.StoreId).FirstOrDefault()?.Value ?? "";

			var url = !string.IsNullOrEmpty(sendBox) && Convert.ToBoolean(sendBox) ?
				"https://www.sandbox.paypal.com/us/cgi-bin/webscr" :
				"https://www.paypal.com/us/cgi-bin/webscr";
			var requestContent = new StringContent($"cmd=_notify-synch&at={pdtToken}&tx={tx}",
				Encoding.UTF8, "application/x-www-form-urlencoded");
			var response = await client.PostAsync(url, requestContent);
			response.EnsureSuccessStatusCode();
			return await response.Content.ReadAsStringAsync();
		}

		#endregion

		#region Email notification

		public static void SendEmailNotification(Order order, OrderReviewModel orderReviewModel, int languageId, ecuadordevContext dbcontext)
		{
			try
			{
				var store = dbcontext.Store.Where(x => x.Id == order.StoreId).FirstOrDefault();
				string customerName = string.Empty;
				string customerEmail = string.Empty;
				var customer = dbcontext.Customer.Where(x => x.Id == order.CustomerId).ToList().FirstOrDefault();
				if (IsUserRegistered(order.CustomerId, dbcontext))
				{
					var customerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == order.CustomerId).ToList();
					customerName = customerAddressDetails.Where(x => x.Key.Contains("FirstName"))?.FirstOrDefault()?.Value + " " + (customerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? customerAddressDetails.Where(x => x.Key.Contains("LastName"))?.FirstOrDefault()?.Value : "");
					customerEmail = customer.Email;
				}
				else
				{
					var billingAddress = dbcontext.Address.FirstOrDefault(x => x.Id == order.BillingAddressId);
					customerName = billingAddress?.FirstName;
					customerEmail = billingAddress?.Email;
				}

				/* Store Owner Email notification*/
				var messageTemplate = dbcontext.MessageTemplate.Where(x => x.Name.Contains("CustomerApp.Notification.OrderPlaced")
											&& (dbcontext.StoreMapping.Any(y => y.StoreId == orderReviewModel.StoreId && y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
											).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();
				if (messageTemplate != null)
				{
					//Order confirm message for mobile app
					string body = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Body", languageId, messageTemplate.Body, dbcontext);
					string subject = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Subject", languageId, messageTemplate.Subject, dbcontext);

					body = body.Replace("%Store.URL%", store.Url);
					body = body.Replace("%Store.Name%", LanguageHelper.GetLocalizedValue(store.Id, "Name", "Store", languageId, store.Name, dbcontext));
					body = body.Replace("%Customer.OrderNumber%", order.CustomOrderNumber);
					body = body.Replace("%Customer.FullName%", customerName);

					var emailAccount = (from b in dbcontext.EmailAccount
										where b.StoreId == store.Id
										select new
										{
											b
										}).FirstOrDefault()?.b;
					if (emailAccount != null)
					{
						try
						{
							Helper.InsertQueuedEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, customerEmail, customerName, messageTemplate, dbcontext);
						}
						catch (Exception ex)
						{
							Helper.SentryLogs(ex);
						}
					}

				}
				/* Store Owner Email notification*/
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				throw;
			}
		}

		public static void InsertQueuedEmail(EmailAccount emailAccount, string subject, string body,
		   string fromAddress, string fromName, string toAddress, string toName, MessageTemplate messageTemplate, ecuadordevContext dbcontext,
			string replyTo = null, string replyToName = null,
		   IEnumerable<string> bcc = null, IEnumerable<string> cc = null,
		   string attachmentFilePath = null, string attachmentFileName = null,
		   int attachedDownloadId = 0, IDictionary<string, string> headers = null)
		{
			var email = new QueuedEmail
			{
				PriorityId = 5,
				From = !string.IsNullOrEmpty(fromAddress) ? fromAddress : emailAccount.Email,
				FromName = !string.IsNullOrEmpty(fromName) ? fromName : emailAccount.DisplayName,
				To = toAddress,
				ToName = toName,
				ReplyTo = string.Empty,
				ReplyToName = replyToName,
				Cc = string.Empty,
				Bcc = bcc != null && bcc.Count() > 0 ? string.Join(",", bcc.Select(x => x)) : string.Empty,
				Subject = subject,
				Body = body,
				AttachmentFilePath = attachmentFilePath,
				AttachmentFileName = attachmentFileName,
				AttachedDownloadId = messageTemplate.AttachedDownloadId,
				CreatedOnUtc = DateTime.UtcNow,
				EmailAccountId = emailAccount.Id,
				DontSendBeforeDateUtc = null
			};

			dbcontext.Add(email);
			dbcontext.SaveChanges();
		}
		#endregion

		#region Send SMS
		public static bool SendSMSToCustomer(Order order, OrderReviewModel orderReviewModel, int languageId, ecuadordevContext dbcontext)
		{
			try
			{
				var store = dbcontext.Store.AsEnumerable().Where(x => x.Id == orderReviewModel.StoreId).FirstOrDefault();
				string customerName = string.Empty;
				string isdCode = string.Empty;
				int countryId = (int)dbcontext.Store.FirstOrDefault(x => x.Id == orderReviewModel.StoreId)?.CountryId;
				isdCode = dbcontext.Country.FirstOrDefault(x => x.Id == countryId)?.Isdcode;
				if (IsUserRegistered(order.CustomerId, dbcontext))
				{
					var customerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == order.CustomerId).ToList();
					customerName = customerAddressDetails.Where(x => x.Key.Contains("FirstName"))?.FirstOrDefault()?.Value + " " + (customerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? customerAddressDetails.Where(x => x.Key.Contains("LastName"))?.FirstOrDefault()?.Value : "");

				}
				else
				{
					var billingAddress = dbcontext.Address.FirstOrDefault(x => x.Id == order.BillingAddressId);
					customerName = billingAddress?.FirstName;

				}
				var phoneNumber = (from data in dbcontext.Order
								   join add in dbcontext.Address
								   on data.BillingAddressId equals add.Id
								   where data.Id == orderReviewModel.OrderId
								   select add.PhoneNumber
									   ).FirstOrDefault();
				var messageTemplate = dbcontext.MessageTemplate.Where(x => x.Name.Contains("CustomerApp.Notification.OrderPlaced")
												  && (dbcontext.StoreMapping.Any(y => y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
												  ).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();

				if (!string.IsNullOrEmpty(phoneNumber) && messageTemplate != null)
				{
					phoneNumber = "+" + isdCode + phoneNumber;
					string body = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Body", languageId, messageTemplate.Body, dbcontext);
					string subject = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Subject", languageId, messageTemplate.Subject, dbcontext);

					body = body.Replace("%Store.URL%", store.Url);
					body = body.Replace("%Store.Name%", LanguageHelper.GetLocalizedValue(store.Id, "Name", "Store", languageId, store.Name, dbcontext));
					body = body.Replace("%Customer.OrderNumber%", order.CustomOrderNumber);
					body = body.Replace("%Customer.FullName%", customerName);

					var plainText = Helper.HtmlToPlainText(body);
					var isSMSSend = Helper.SendMessage(phoneNumber, plainText, dbcontext);

					return true;
				}

				return false;

			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				return false;
			}
		}

		#endregion

		public static bool SendSMSToMerchant(OrderReviewModel orderReviewModel, int languageId, ecuadordevContext dbcontext)
		{
			try
			{

				var store = dbcontext.Store.AsEnumerable().Where(x => x.Id == orderReviewModel.StoreId).FirstOrDefault();
				string isdCode = string.Empty;
				int countryId = (int)dbcontext.Store.FirstOrDefault(x => x.Id == orderReviewModel.StoreId)?.CountryId;
				isdCode = dbcontext.Country.FirstOrDefault(x => x.Id == countryId)?.Isdcode;

				var phoneNumber = (from data in dbcontext.Order
								   join add in dbcontext.Address
								   on data.PickupAddressId equals add.Id
								   where data.Id == orderReviewModel.OrderId
								   select add.PhoneNumber
									).FirstOrDefault();
				var messageTemplate = dbcontext.MessageTemplate.Where(x => x.Name.Contains("Merchant.SMS.OrderPlaced")
											   && (dbcontext.StoreMapping.Any(y => y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
											   ).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();

				if (!string.IsNullOrEmpty(phoneNumber) && messageTemplate != null)
				{
					phoneNumber = "+" + isdCode + phoneNumber;
					string bodyForSMS = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Body", languageId, messageTemplate.Body, dbcontext);

					bodyForSMS = bodyForSMS.Replace("%Customer.FullName%", store.Name)
					.Replace("%Customer.OrderNumber%", orderReviewModel.OrderId.ToString());

					var plainText = Helper.HtmlToPlainText(bodyForSMS);
					var isSMSSend = Helper.SendMessage(phoneNumber, plainText, dbcontext);

					return true;
				}

				return false;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				return false;
			}


		}


		public static bool IsUserRegistered(int customerId, ecuadordevContext db)
		{
			return (from c in db.Customer
					join crm in db.CustomerCustomerRoleMapping
					on c.Id equals crm.CustomerId
					join cr in db.CustomerRole
					on crm.CustomerRoleId equals cr.Id
					where c.Id == customerId && cr.SystemName == "Registered"
					select c).Any();
		}

		#region ShoppingCartItems

		/// <summary>
		/// Get ShoppingCart Items
		/// </summary>
		/// <param name="storeId"></param>
		/// <param name="customerId"></param>
		/// <param name="shoppingCartTypeId"></param>
		/// <param name="languageId"></param>
		/// <param name="dbcontext"></param>
		/// <returns></returns>
		public static List<CooknChefWishListProductsV2> GetShoppingCartItems(int storeId, int customerId, int shoppingCartTypeId, int languageId, ecuadordevContext dbcontext)
		{
			List<CooknChefWishListProductsV2> wishListItems = new List<CooknChefWishListProductsV2>();
			string chefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == storeId).FirstOrDefault().Value;
			string currencySymbol = Helper.GetStoreCurrency(storeId, context: dbcontext);

			var cartItems = (from p in dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.StoreId == storeId && x.CustomerId == customerId && x.ShoppingCartTypeId == shoppingCartTypeId)
							 join pro in dbcontext.Product.AsEnumerable()
							 on p.ProductId equals pro.Id
							 select new CooknChefWishListProductsV2
							 {
								 ItemId = p.Id,
								 ItemName = LanguageHelper.GetLocalizedValueProduct(pro.Id, "Name", languageId, pro.Name, dbcontext),
								 Quantity = shoppingCartTypeId == (int)ShoppingCartType.Wishlist
								 ? dbcontext.ShoppingCartItem.Where(x => x.StoreId == storeId && x.CustomerId == customerId && x.ProductId == p.ProductId && x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).Sum(x => x.Quantity)
								 : p.Quantity,
								 Description = LanguageHelper.GetLocalizedValueProduct(pro.Id, "ShortDescription", languageId, pro.ShortDescription ?? "", dbcontext),
								 ProductId = p.ProductId,
								 Available = pro.Published,
								 ProductOffer = Helper.GetProductOffer(storeId, p.ProductId, dbcontext),
								 Price = dbcontext.Product.AsEnumerable().Where(x => x.Id == pro.Id && x.TaxCategoryId == 0).FirstOrDefault() != null
								? dbcontext.Product.AsEnumerable().Where(x => x.Id == pro.Id && x.TaxCategoryId == 0).FirstOrDefault().Price.ToString()
								: "" + (from priceCategoryTax in dbcontext.Product.AsEnumerable()
										join categoryTax in dbcontext.TaxRate.AsEnumerable()
										on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
										where categoryTax.StoreId == storeId &&
										priceCategoryTax.Id == pro.Id

										select new
										{
											PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
										}).FirstOrDefault().PriceCalculated != null ? (from priceCategoryTax in dbcontext.Product.AsEnumerable()
																					   join categoryTax in dbcontext.TaxRate.AsEnumerable()
																									   on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
																					   where categoryTax.StoreId == storeId &&
																					   priceCategoryTax.Id == pro.Id

																					   select new
																					   {
																						   PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
																					   }).FirstOrDefault().PriceCalculated.ToString() : "0.0",
								 OldPrice = Convert.ToString(Helper.ConvertdecimaltoUptotwoPlaces(pro.OldPrice)),
								 Cuisine = (from productcuisine in dbcontext.ProductProductCuisineMapping.AsEnumerable()
											join product in dbcontext.ProductCuisine.AsEnumerable()
											on productcuisine.CuisineId equals product.Id
											where productcuisine.ProductId == pro.Id

											select new
											{
												product.Name
											}).FirstOrDefault() != null ? (from productcuisine in dbcontext.ProductProductCuisineMapping.AsEnumerable()
																		   join product in dbcontext.ProductCuisine.AsEnumerable()
																		   on productcuisine.CuisineId equals product.Id
																		   where productcuisine.ProductId == pro.Id
																		   select new
																		   {
																			   product.Name
																		   }).FirstOrDefault()?.Name : "",
								 Preferences = (from ProductTag in dbcontext.ProductProductTagMapping.AsEnumerable().Where(x => x.ProductId == pro.Id)
												join ProductTagss in dbcontext.ProductTag.AsEnumerable()
												on ProductTag.ProductTagId equals ProductTagss.Id
												join ProductTagImage in dbcontext.ProductTagImageMapping.AsEnumerable()
												on ProductTagss.Id equals ProductTagImage.TagId
												join ProductTagImageDetails in dbcontext.Picture.AsEnumerable()
												on ProductTagImage.PictureId equals ProductTagImageDetails.Id
												select new PreferencesV2
												{
													Name = ProductTagss?.Name,
													Image =
														ProductTagImageDetails?.Id.ToString(),
													ImageType = ProductTagImageDetails?.MimeType,


												}
																								).Distinct().ToList(),
								 ImageUrl = (from product in dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == pro.Id)
											 join picture in dbcontext.Picture.AsEnumerable()
											 on product.PictureId equals picture.Id
											 select new CooknChefProductsImagesV2
											 {
												 ProductImageURL = picture?.Id.ToString(),
												 MimeType = picture?.MimeType,
												 FileSeo = picture.SeoFilename

											 }).ToList(),
								 ProductProductAttributes = (from _d in dbcontext.Product.AsEnumerable().Where(x => x.Id == pro.Id)
															 join _e in dbcontext.ProductProductAttributeMapping.AsEnumerable()
															 on _d.Id equals _e.ProductId
															 join _f in dbcontext.ProductAttribute.AsEnumerable()
															 on _e.ProductAttributeId equals _f.Id
															 select new CooknRestProductProductAttributesResponse
															 {
																 AttributeName = _f.Name,
																 IsRequired = _e.IsRequired,
																 AttributeTypeId = _e.AttributeControlTypeId,
																 ProductId = _d.Id,
																 ProductAttributeId = _f.Id,
																 ProductAttributeMappingId = _e.Id,
																 ProductAttribute = (from __d in dbcontext.ProductAttributeValue.AsEnumerable()
																					 where __d.ProductAttributeMappingId == _e.Id
																					 select new ProductAttributes
																					 {
																						 Name = __d.Name,
																						 Price = __d.PriceAdjustment.ToString(),
																						 ProductAttributeValueId = __d.Id,
																						 IsPreSelected = __d.IsPreSelected,
																						 Currency = currencySymbol
																					 }).ToList()
															 }).ToList(),
								 IsProductAttributesExist = (from _d in dbcontext.Product.AsEnumerable().Where(x => x.Id == pro.Id)
															 join _e in dbcontext.ProductProductAttributeMapping.AsEnumerable()
															 on _d.Id equals _e.ProductId
															 join _f in dbcontext.ProductAttribute.AsEnumerable()
															 on _e.ProductAttributeId equals _f.Id
															 select new CooknRestProductProductAttributesResponse
															 {
																 AttributeName = _f.Name,
																 IsRequired = _e.IsRequired,
																 AttributeTypeId = _e.AttributeControlTypeId,
																 ProductId = _d.Id,
																 ProductAttributeId = _f.Id,
																 ProductAttributeMappingId = _e.Id,
																 ProductAttribute = (from __d in dbcontext.ProductAttributeValue.AsEnumerable()
																					 where __d.ProductAttributeMappingId == _e.Id
																					 select new ProductAttributes
																					 {
																						 Name = __d.Name,
																						 Price = __d.PriceAdjustment.ToString(),
																						 ProductAttributeValueId = __d.Id,
																						 IsPreSelected = __d.IsPreSelected,
																						 Currency = currencySymbol
																					 }).ToList()
															 }).Any(),
								 Rating = (!(from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == pro.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item)

											 select new
											 {
												 rating.Rating
											 }).Any()) ? 0 :
															 (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == pro.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item)

															  select new
															  {
																  rating.Rating
															  }).ToList().Sum(x => x.Rating),
								 RatingCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == pro.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item)

												select new
												{
													rating.Rating
												}).Count(),
								 ReviewCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == pro.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item && !string.IsNullOrEmpty(x.ReviewText))

												select new
												{
													rating.ReviewText
												}).Count(),
								 CustomerImages = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == pro.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item && !string.IsNullOrEmpty(x.ReviewText))
												   join customerImage in dbcontext.GenericAttribute.AsEnumerable().Where(x => x.Key == "ProfilePic")
												   on rating.CustomerId equals customerImage.EntityId
												   select new CustomerImage
												   {
													   Image = customerImage.Value
												   }).ToList(),
								 Specifications = (from Product in dbcontext.Product.AsEnumerable()
												   join psm in dbcontext.ProductSpecificationAttributeMapping.AsEnumerable()
												   on Product.Id equals psm.ProductId
												   join sao1 in dbcontext.SpecificationAttributeOption.AsEnumerable()
												   on psm.SpecificationAttributeOptionId equals sao1.Id
												   join sa1 in dbcontext.SpecificationAttribute.AsEnumerable()
												   on sao1.SpecificationAttributeId equals sa1.Id
												   where psm.ProductId == pro.Id && sao1.SpecificationAttributeId == sa1.Id
												   group sa1 by sao1.SpecificationAttributeId into productSpecificationsAttributes
												   select new SpecificationModel
												   {
													   Id = productSpecificationsAttributes.Key,
													   Name = productSpecificationsAttributes.Select(x => x.Name).FirstOrDefault(),
													   ProductSpecificationAttribute = (from Product in dbcontext.Product.AsEnumerable()
																						join sam in dbcontext.ProductSpecificationAttributeMapping.AsEnumerable()
																						on Product.Id equals sam.ProductId
																						join sao in dbcontext.SpecificationAttributeOption.AsEnumerable()
																						on sam.SpecificationAttributeOptionId equals sao.Id
																						where Product.Id == pro.Id && sao.SpecificationAttributeId == productSpecificationsAttributes.Key
																						select new productSpecificationAttribute
																						{
																							Id = sao.Id,
																							Name = sao.Name
																						}).Distinct().ToList()
												   }).Distinct().ToList(),


							 }).ToList();

			foreach (var category in cartItems)
			{
				category.Price = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(category.Price));
				//CurrencySymbol + "" + Math.Round(Convert.ToDecimal(category.Price), 2);
				foreach (var pref in category.ProductProductAttributes)
				{
					foreach (var item in pref.ProductAttribute)
					{
						item.Price = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Price));
					}
				}
				foreach (var pref in category.Preferences)
				{

					int pictureId = Convert.ToInt32(pref.Image);
					string lastPart = Helper.GetFileExtensionFromMimeType(pref.ImageType);
					var prefdetail = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();
					var fileName = !string.IsNullOrEmpty(prefdetail.SeoFilename)
								? $"{pictureId:0000000}_{prefdetail.SeoFilename}_{75}.{lastPart}" + "? " + DateTime.Now
								: $"{pictureId:0000000}_{75}.{lastPart}" + "? " + DateTime.Now;
					pref.Image = chefBaseUrl + fileName;
				}

				int pictureSize = 75;
				var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
				if (setting != null)
				{
					pictureSize = Convert.ToInt32(setting.Value);
				}
				if (category.ImageUrl.Any())
					foreach (var pref in category.ImageUrl)
					{
						if (!string.IsNullOrEmpty(pref.ProductImageURL))
						{
							int pictureId = Convert.ToInt32(pref.ProductImageURL);
							if (pictureId != 0)
							{
								string lastPart = Helper.GetFileExtensionFromMimeType(pref.MimeType);
								string thumbFileName = !string.IsNullOrEmpty(pref.FileSeo)
								? $"{pictureId:0000000}_{pref.FileSeo}.{lastPart}" + "? " + DateTime.Now
								: $"{pictureId:0000000}_{pictureSize}.{lastPart}" + "? " + DateTime.Now;
								pref.ProductImageURL = chefBaseUrl + thumbFileName;
							}
							else
							{
								pref.ProductImageURL = chefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
							}
						}
						else
						{
							pref.ProductImageURL = chefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
						}

					}
				else
				{
					CooknChefProductsImagesV2 cooknChefProductsImagesV2 = new CooknChefProductsImagesV2();
					cooknChefProductsImagesV2.ProductImageURL = chefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
					category.ImageUrl.Add(cooknChefProductsImagesV2);
				}
				if (category.Rating != 0)
				{
					category.Rating = Convert.ToDouble(category.Rating / category.RatingCount);
					category.Rating = Math.Round(category.Rating, 2);
				}
				wishListItems.Add(category);
			}


			return wishListItems;
		}

		/// <summary>
		/// Get Product Price
		/// </summary>
		/// <param name="product"></param>
		/// <param name="taxCategoryId"></param>
		/// <param name="price"></param>
		/// <param name="includingTax"></param>
		/// <param name="customer"></param>
		/// <param name="priceIncludesTax"></param>
		/// <param name="storeId"></param>
		/// <param name="taxRate"></param>
		/// <param name="ProductTaxPrice"></param>
		/// <param name="Quantity"></param>
		/// <param name="dbcontext"></param>
		/// <returns></returns>
		public static decimal GetProductPrice(Product product, int taxCategoryId,
		   decimal price, bool includingTax, Customer customer,
		   bool priceIncludesTax, int storeId, out decimal taxRate, out decimal ProductTaxPrice, decimal Quantity, ecuadordevContext dbcontext)
		{
			//no need to calculate tax rate if passed "price" is 0
			ProductTaxPrice = decimal.Zero;
			if (price == decimal.Zero)
			{
				taxRate = decimal.Zero;
				ProductTaxPrice = decimal.Zero;
				return taxRate;
			}

			GetTaxRate(product, taxCategoryId, customer, price, storeId, out taxRate, out var isTaxable, out ProductTaxPrice, Quantity, dbcontext);

			if (priceIncludesTax)
			{
				//"price" already includes tax
				if (includingTax)
				{
					//we should calculate price WITH tax
					if (!isTaxable)
					{
						//but our request is not taxable
						//hence we should calculate price WITHOUT tax
						price = CalculatePrice(price, taxRate, false);
					}
				}
				else
				{
					//we should calculate price WITHOUT tax
					price = CalculatePrice(price, taxRate, false);
				}
			}
			else
			{
				//"price" doesn't include tax
				if (includingTax)
				{
					//we should calculate price WITH tax
					//do it only when price is taxable
					if (isTaxable)
					{
						price = CalculatePrice(price, taxRate, true);
					}
				}
			}

			if (!isTaxable)
			{
				//we return 0% tax rate in case a request is not taxable
				taxRate = decimal.Zero;
			}

			return price;
		}

		/// <summary>
		/// Calculate Price
		/// </summary>
		/// <param name="price"></param>
		/// <param name="percent"></param>
		/// <param name="increase"></param>
		/// <returns></returns>
		public static decimal CalculatePrice(decimal price, decimal percent, bool increase)
		{
			if (percent == decimal.Zero)
				return price;

			decimal result;
			if (increase)
			{
				result = price * (1 + percent / 100);
			}
			else
			{
				result = price - price / (100 + percent) * percent;
			}

			return result;
		}

		/// <summary>
		/// Gets tax rate
		/// </summary>
		/// <param name="product">Product</param>
		/// <param name="taxCategoryId">Tax category identifier</param>
		/// <param name="customer">Customer</param>
		/// <param name="price">Price (taxable value)</param>
		/// <param name="taxRate">Calculated tax rate</param>
		/// <param name="isTaxable">A value indicating whether a request is taxable</param>
		/// <param name="dbcontext">dbcontext</param>
		public static void GetTaxRate(Product product, int taxCategoryId,
			Customer customer, decimal price, int storeId, out decimal taxRate, out bool isTaxable, out decimal ProductTaxPrice, decimal Quantity, ecuadordevContext dbcontext)
		{
			taxRate = decimal.Zero;
			ProductTaxPrice = decimal.Zero;
			isTaxable = true;

			//tax request
			var calculateTaxRequest = CreateCalculateTaxRequest(product, taxCategoryId, customer, price, storeId, dbcontext);

			//tax exempt
			if (IsTaxExempt(product, calculateTaxRequest.Customer))
			{
				isTaxable = false;
			}

			var euvatenabled = Convert.ToBoolean(dbcontext.Setting.Where(s => s.StoreId == storeId && s.Name == "taxsettings.euvatenabled").Select(s => s.Value).FirstOrDefault());

			//make EU VAT exempt validation (the European Union Value Added Tax)
			if (isTaxable &&
				euvatenabled) //&& IsVatExempt(calculateTaxRequest.Address, calculateTaxRequest.Customer))
			{
				//VAT is not chargeable
				isTaxable = false;
			}

			//get tax rate
			var calculateTaxResult = GetTaxRate(calculateTaxRequest, storeId, out ProductTaxPrice, Quantity, dbcontext);
			if (calculateTaxResult.Success)
			{
				//ensure that tax is equal or greater than zero
				if (calculateTaxResult.TaxRate < decimal.Zero)
					calculateTaxResult.TaxRate = decimal.Zero;

				taxRate = calculateTaxResult.TaxRate;
			}
		}

		/// <summary>
		/// Get Tax Rate
		/// </summary>
		/// <param name="calculateTaxRequest"></param>
		/// <param name="store_Id"></param>
		/// <param name="ProductTaxPrice"></param>
		/// <param name="Quantity"></param>
		/// <param name="dbcontext"></param>
		/// <returns></returns>
		public static CalculateTaxResult GetTaxRate(CalculateTaxRequest calculateTaxRequest, int store_Id, out decimal ProductTaxPrice, decimal Quantity, ecuadordevContext dbcontext)
		{
			var result = new CalculateTaxResult();
			int storeId_Id = store_Id;
			//the tax rate calculation by fixed rate
			ProductTaxPrice = 0;
			var countryStateZipEnabled = Convert.ToBoolean(dbcontext.Setting.Where(s => (s.StoreId == storeId_Id || s.StoreId == 0) && s.Name == "fixedorbycountrystateziptaxsettings.countrystatezipenabled").Select(s => s.Value).FirstOrDefault());

			if (!countryStateZipEnabled)
			{
				result.TaxRate = Convert.ToDecimal(dbcontext.Setting.Where(s => s.StoreId == storeId_Id && s.Name == (string.Format("Tax.TaxProvider.FixedOrByCountryStateZip.TaxCategoryId{0}", calculateTaxRequest.TaxCategoryId))).Select(x => x.Value).FirstOrDefault());
				return result;
			}

			//the tax rate calculation by country & state & zip 
			if (calculateTaxRequest.Address == null)
			{
				result.Errors.Add("Address is not set");
				return result;
			}

			var alltaxRates = dbcontext.TaxRate.Where(s => s.StoreId == 0 || s.StoreId == store_Id);


			var allTaxRates = alltaxRates.Select(taxRate => new TaxRateForCaching
			{
				Id = taxRate.Id,
				StoreId = taxRate.StoreId,
				TaxCategoryId = taxRate.TaxCategoryId,
				CountryId = taxRate.CountryId,
				StateProvinceId = taxRate.StateProvinceId,
				Zip = taxRate.Zip,
				Percentage = taxRate.Percentage
			}).ToList();

			var storeId = calculateTaxRequest.CurrentStoreId;
			var taxCategoryId = calculateTaxRequest.TaxCategoryId;
			var countryId = calculateTaxRequest.Address.CountryId;
			var stateProvinceId = calculateTaxRequest.Address.StateProvinceId;
			var zip = calculateTaxRequest.Address.ZipPostalCode?.Trim() ?? string.Empty;

			//get tax by tax categories
			var taxCategoryIds = new List<int>();
			if (!string.IsNullOrEmpty(calculateTaxRequest.Product?.TaxCategoryIds))
				taxCategoryIds = calculateTaxRequest.Product.TaxCategoryIds.Split(',').Select(x => int.Parse(x)).ToList();
			var existingRates = allTaxRates.Where(taxRate => taxRate.CountryId == countryId && taxCategoryIds.Contains(taxRate.TaxCategoryId));

			//filter by store
			var matchedByStore = existingRates.Where(taxRate => storeId == taxRate.StoreId || taxRate.StoreId == 0);

			//filter by state/province
			var matchedByStateProvince = matchedByStore.Where(taxRate => stateProvinceId == taxRate.StateProvinceId || taxRate.StateProvinceId == 0);

			//filter by zip
			var matchedByZip = matchedByStateProvince.Where(taxRate => string.IsNullOrWhiteSpace(taxRate.Zip) || taxRate.Zip.Equals(zip, StringComparison.InvariantCultureIgnoreCase));

			//sort from particular to general, more particular cases will be the first
			var foundRecords = matchedByZip.OrderBy(r => r.StoreId == 0).ThenBy(r => r.StateProvinceId == 0).ThenBy(r => string.IsNullOrEmpty(r.Zip));

			var foundRecord = foundRecords.ToList();

			if (foundRecord.Count > 0)
			{
				result.TaxRate = foundRecord.Sum(x => x.Percentage);
				ProductTaxPrice = ProductTaxPrice + (calculateTaxRequest.Price * Quantity * (result.TaxRate / 100));

			}

			return result;
		}

		/// <summary>
		/// Gets a value indicating whether a product is tax exempt
		/// </summary>
		/// <param name="product">Product</param>
		/// <param name="customer">Customer</param>
		/// <returns>A value indicating whether a product is tax exempt</returns>
		public static bool IsTaxExempt(Product product, Customer customer)
		{
			if (customer != null)
			{
				if (customer.IsTaxExempt)
					return true;

				//#TODO RAM
				//if (customer.CustomerRoles.Where(cr => cr.Active).Any(cr => cr.TaxExempt))
				//    return true;
			}

			if (product == null)
			{
				return false;
			}

			if (product.IsTaxExempt)
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// Create request for tax calculation
		/// </summary>
		/// <param name="product">Product</param>
		/// <param name="taxCategoryId">Tax category identifier</param>
		/// <param name="customer">Customer</param>
		/// <param name="price">Price</param>
		/// <param name="store_id">store_id</param>
		/// <param name="dbcontext">dbcontext</param>
		/// <returns>Package for tax calculation</returns>
		public static CalculateTaxRequest CreateCalculateTaxRequest(Product product,
			int taxCategoryId, Customer customer, decimal price, int store_id, ecuadordevContext dbcontext)
		{
			if (customer == null)
				throw new ArgumentNullException(nameof(customer));

			var calculateTaxRequest = new CalculateTaxRequest
			{
				Customer = customer,
				Product = product,
				Price = price,
				TaxCategoryId = taxCategoryId > 0 ? taxCategoryId : product?.TaxCategoryId ?? 0,
				CurrentStoreId = store_id
			};

			var basedOn = dbcontext.Setting.Where(s => s.StoreId == store_id && s.Name == "taxsettings.taxbasedon").Select(s => s.Value).FirstOrDefault();

			var euvatenabled = Convert.ToBoolean(dbcontext.Setting.Where(s => s.StoreId == store_id && s.Name == "taxsettings.euvatenabled").Select(s => s.Value).FirstOrDefault());
			//new EU VAT rules starting January 1st 2015
			//find more info at http://ec.europa.eu/taxation_customs/taxation/vat/how_vat_works/telecom/index_en.htm#new_rules
			var overriddenBasedOn =
				////EU VAT enabled?
				euvatenabled &&
				////telecommunications, broadcasting and electronic services?
				product != null && product.IsTelecommunicationsOrBroadcastingOrElectronicServices;//&&
																								  ////January 1st 2015 passed? Yes, not required anymore
																								  // //DateTime.UtcNow > new DateTime(2015, 1, 1, 0, 0, 0, DateTimeKind.Utc) &&
																								  // //Europe Union consumer?
																								  // IsEuConsumer(customer); //commented by Arun
			if (overriddenBasedOn)
			{
				//We must charge VAT in the EU country where the customer belongs (not where the business is based)
				basedOn = "BillingAddress";
			}

			var TaxBasedOnPickupPointAddress = Convert.ToBoolean(dbcontext.Setting.Where(s => s.StoreId == store_id && s.Name == "taxsettings.taxbasedonpickuppointaddress").Select(s => s.Value).FirstOrDefault());
			var AllowPickupInStore = Convert.ToBoolean(dbcontext.Setting.Where(s => s.StoreId == store_id && s.Name == "shippingsettings.allowpickupinstore").Select(s => s.Value).FirstOrDefault());

			//tax is based on pickup point address
			if (!overriddenBasedOn && TaxBasedOnPickupPointAddress && AllowPickupInStore)
			{
				var pickupPoint = dbcontext.GenericAttribute.Where(g => g.StoreId == store_id && g.Key == "SelectedPickupPoint" && g.EntityId == customer.Id).ToList();

				if (pickupPoint != null && pickupPoint.Count > 0)
				{
					calculateTaxRequest.Address = new CalculateTaxRequest.TaxAddress
					{
						//#TODO Ram
						//CountryId = pickupPointcountry?.Id ?? 0,
						//StateProvinceId = state?.Id ?? 0,
						//County = pickupPoint.County,
						//City = pickupPoint.City,
						//Address1 = pickupPoint.Address,
						//ZipPostalCode = pickupPoint.ZipPostalCode
					};
					return calculateTaxRequest;
				}
			}

			//tax by merchant address

			var vendorAddressId = Convert.ToInt32(dbcontext.Vendor.Where(v => v.Id == product.VendorId).Select(v => v.AddressId).FirstOrDefault());
			var vendorAddress = dbcontext.Address.Where(a => a.Id == vendorAddressId).FirstOrDefault();
			if (vendorAddress != null)
			{
				calculateTaxRequest.Address = PrepareTaxAddress(vendorAddress);
			}
			else
			{
				if (basedOn == "BillingAddress" && customer.BillingAddress == null ||
					basedOn == "ShippingAddress" && customer.ShippingAddress == null)
				{
					basedOn = "DefaultAddress";
				}

				switch (basedOn)
				{
					case "BillingAddress":
						calculateTaxRequest.Address = PrepareTaxAddress(customer.BillingAddress);
						break;
					case "ShippingAddress":
						calculateTaxRequest.Address = PrepareTaxAddress(customer.ShippingAddress);
						break;
					case "DefaultAddress":
					default:
						calculateTaxRequest.Address = LoadDefaultTaxAddress(store_id, dbcontext);
						break;
				}
			}

			return calculateTaxRequest;
		}

		/// <summary>
		/// Gets a default tax address
		/// </summary>
		/// <returns>Address</returns>
		public static CalculateTaxRequest.TaxAddress LoadDefaultTaxAddress(int store_id, ecuadordevContext dbcontext)
		{
			var addressId = Convert.ToInt32(dbcontext.Setting.Where(s => s.StoreId == store_id && s.Name == " taxsettings.defaulttaxaddressid").Select(s => s.Value).FirstOrDefault());
			var address = dbcontext.Address.Where(s => s.Id == addressId).FirstOrDefault();
			return PrepareTaxAddress(address);
		}

		/// <summary>
		/// Prepare TaxAddress object based on a specific address
		/// </summary>
		/// <param name="address">Address</param>
		/// <returns>Address</returns>
		public static CalculateTaxRequest.TaxAddress PrepareTaxAddress(Address address)
		{
			if (address == null)
				return null;

			return new CalculateTaxRequest.TaxAddress
			{
				AddressId = address.Id,
				CountryId = address.CountryId,
				StateProvinceId = address.StateProvinceId,
				County = address.County,
				City = address.City,
				Address1 = address.Address1,
				Address2 = address.Address2,
				ZipPostalCode = address.ZipPostalCode
			};
		}
		#endregion
		public static string GetPictureUrl(int pictureId, string storeUrl, int picturesize)
		{
			if (pictureId == 0)
				return string.Empty;
			return string.Format("{0}Picture/GetPictureUrl?pictureId={1}&size={2}", storeUrl, pictureId, picturesize).ToString();
		}
		public static Customer GetCustomer(int customerId, int storeId, ecuadordevContext context)
		{
			var customer = context.Customer.Where(x => x.Id == customerId && x.Active && !x.Deleted).FirstOrDefault();
			if (customer != null)
				return customer;
			return null;
		}
		public static VersionInfo AuthenticateKey(string apikey, int storeId, ecuadordevContext context, ICacheManager _cacheManager = null)
		{

			var key = string.Format(CacheKeys.AuthenticateKey, storeId, apikey);

			var apiKeyExists = _cacheManager.Get(key, () =>
			{

				var apiKeyExist = context.VersionInfo.Where(x => x.Apikey == apikey && x.StoreId == storeId).FirstOrDefault();
				if (apiKeyExist != null)
				{
					return apiKeyExist;
				}
				return null;

			});
			return apiKeyExists;
		}
		public static string GetStoreName(int storeId, ecuadordevContext context)
		{
			var storeName = context.Store.Where(x => x.Id == storeId)?.FirstOrDefault().Name ?? "";
			return storeName;
		}
		public static string GetBaseUrl(int storeId, ecuadordevContext context)
		{
			string BaseUrl = context.Store.Where(x => x.Id == storeId)?.FirstOrDefault().Url ?? "";
			return BaseUrl;
		}

		public static string SendEmailOtp(int customerId, int storeId, string email, string seconds, int languageId, ecuadordevContext dbcontext)
		{
			var store = dbcontext.Store.AsEnumerable().Where(x => x.Id == storeId).FirstOrDefault();
			var customerDetails = dbcontext.CustomerDetails.Where(x => x.CustomerId == customerId).OrderByDescending(x => x.Id).FirstOrDefault();
			int _min = 1000;
			int _max = 9999;
			Random _rdm = new Random();
			customerDetails.Otp = _rdm.Next(_min, _max).ToString();
			customerDetails.CrDt = DateTime.UtcNow;
			dbcontext.Entry(customerDetails).State = EntityState.Modified;
			dbcontext.SaveChanges();
			var messageTemplate = dbcontext.MessageTemplate.Where(x => x.Name.Contains("CustomerApp.Email.WelcomeMessage")
													   && (dbcontext.StoreMapping.Any(y => y.StoreId == store.Id && y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
													   ).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();
			if (messageTemplate != null)
			{
				//welcome message for mobile app as SMS
				string body = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Body", languageId, messageTemplate.Body, dbcontext);
				string subject = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Subject", languageId, messageTemplate.Subject, dbcontext);
				subject = subject.Replace("%Store.Name%", store.Name);
				body = body.Replace("%Store.Name%", store.Name);
				body = body.Replace("%Store.Email%", "");
				body = body.Replace("%Store.URL%", store.Url);
				body = body.Replace("%Customer.OTP%", customerDetails.Otp);
				body = body.Replace("%Customer.Seconds%", seconds);
				var emailAccount = (from b in dbcontext.EmailAccount
									where b.StoreId == store.Id
									select new
									{
										b
									}).FirstOrDefault()?.b;
				if (emailAccount != null)
				{
					try
					{
						Helper.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, email, null);
					}
					catch (Exception ex)
					{
						Helper.SentryLogs(ex);

					}
				}
			}
			return customerDetails.Otp;

		}

		public static string SendPhoneOtp(int customerId, int storeId, string phone, string seconds, int languageId, ecuadordevContext _dbcontext)
		{
			var store = _dbcontext.Store.AsEnumerable().Where(x => x.Id == storeId).FirstOrDefault();
			var customerDetails = _dbcontext.CustomerDetails.Where(x => x.CustomerId == customerId).OrderByDescending(x => x.Id).FirstOrDefault();
			int _min = 1000;
			int _max = 9999;
			Random _rdm = new Random();
			customerDetails.Otp = _rdm.Next(_min, _max).ToString();
			customerDetails.CrDt = DateTime.UtcNow;
			_dbcontext.Entry(customerDetails).State = EntityState.Modified;
			_dbcontext.SaveChanges();
			var welcomeMessage = _dbcontext.MessageTemplate.Where(x => x.Name.Contains("CustomerApp.SMS.WelcomeMessage")
												   && (_dbcontext.StoreMapping.Any(y => y.StoreId == store.Id && y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
												   ).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();
			if (welcomeMessage != null)
			{
				string bodyForSMS = LanguageHelper.GetLocalizedValueMessageTemplate(welcomeMessage.Id, "Body", languageId, welcomeMessage.Body, _dbcontext);
				bodyForSMS = bodyForSMS.Replace("%Store.Name%", store.Name);
				bodyForSMS = bodyForSMS.Replace("%Store.Email%", "");
				bodyForSMS = bodyForSMS.Replace("%Store.URL%", store.Url);
				bodyForSMS = bodyForSMS.Replace("%Customer.OTP%", customerDetails.Otp);
				bodyForSMS = bodyForSMS.Replace("%Customer.Seconds%", seconds);
				var plainText = Helper.HtmlToPlainText(bodyForSMS);
				var isSMSSend = Helper.SendMessage(phone, plainText, _dbcontext);

			}
			return customerDetails.Otp;
		}


		#region Get CustomerEmail Dynamic if phone number Entered in case of External Login
		public static string GetCustomEmail(int storeId, string mobileno, ecuadordevContext context)
		{
			var emails = string.Empty;
			var chefbaseurl = Helper.GetBaseUrl(storeId, context);
			string url = chefbaseurl.Split("/")[2];
			string Number = mobileno.Split(' ')[1];
			emails = Number + "@" + url;
			return emails;
		}
		#endregion
		#region Update Product  & Product Attribute Stock
		/// <summary>
		/// Update the Product and Product Attribute Stocks
		/// </summary>
		/// <param name="orderItems"></param>
		/// <param name="storeId"></param>
		/// <param name="orderId"></param>
		/// <param name="languageId"></param>
		/// <param name="customerAPIResponses"></param>
		/// <param name="dbcontext"></param>
		public static void UpdateStock(List<OrderItem> orderItems, int storeId, int orderId, int languageId, ecuadordevContext dbcontext)
		{

			foreach (var orders in orderItems)
			{

				var products = dbcontext.Product.Where(x => x.Id == orders.ProductId).FirstOrDefault();
				var productattribute = dbcontext.ProductAttributeCombination.Where(x => x.ProductId == orders.ProductId && x.AttributesXml == orders.AttributesXml).FirstOrDefault();
				// Don't Track Inventory
				if (products.ManageInventoryMethodId == 0 && products.Published && !products.Deleted)
					BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.OrderNotFound", storeId, dbcontext));

				// Track Inventory
				else if (products.ManageInventoryMethodId == 1 && products.Published && !products.Deleted)
					Helper.ManageProductStock(storeId, products, orders, orderId, languageId, dbcontext);

				// Track Attribute Inventory
				else if (products.ManageInventoryMethodId == 2 && products.Published && !products.Deleted)
					Helper.ManageProductAttributeStock(storeId, products, orders, storeId, productattribute, languageId, dbcontext);
			}


		}
		public static void ManageProductStock(int storeId, Product products, OrderItem orders, int orderId, int languageId, ecuadordevContext dbcontext)
		{
			//Stock Qurantity Updated
			if (products.StockQuantity != 0 && products.Published && !products.Deleted)
			{
				products.StockQuantity = products.StockQuantity - orders.Quantity;
				dbcontext.Entry(products).State = EntityState.Modified;
				dbcontext.SaveChanges();
			}


			//Shoot Email if Stock quantity is less then MinStockQuantity
			if (products.MinStockQuantity >= products.StockQuantity)
			{
				// bool isSuccess = Helper.SendSMSToMerchant_v1(product, languageId, dbcontext);
				Helper.EmailNotificationBelowProduct(products.Id, products.Name, products.StockQuantity, orderId, storeId, languageId, dbcontext);
			}


			// Mark Out Of Stock or Unpublished if Product Stock Becomes 0  
			if (products.StockQuantity == 0 && products.LowStockActivityId == 1)
			{
				products.DisableBuyButton = true;
				dbcontext.Entry(products).State = EntityState.Modified;
				dbcontext.SaveChanges();
			}
			else if (products.StockQuantity == 0 && products.LowStockActivityId == 2)
			{
				products.Published = true;
				dbcontext.Entry(products).State = EntityState.Modified;
				dbcontext.SaveChanges();
			}
			else
			{
				if (products.StockQuantity == 0 && products.LowStockActivityId == 0)
				{
					BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.OrderNotFound", languageId, dbcontext));
				}
			}


		}

		public static void ManageProductAttributeStock(int storeId, Product products, OrderItem orders, int orderId, ProductAttributeCombination productattribute, int languageId, ecuadordevContext dbcontext)
		{
			//Stock Qurantity Updated
			if (productattribute != null && productattribute.StockQuantity != 0)
			{
				productattribute.StockQuantity = productattribute.StockQuantity - orders.Quantity;
				dbcontext.Entry(productattribute).State = EntityState.Modified;
				dbcontext.SaveChanges();
			}


			//Shoot Email if Stock quantity is less then MinStockQuantity 
			if (productattribute != null && products.MinStockQuantity >= productattribute.StockQuantity)
			{
				Helper.EmailNotificationBelowProductAttribute(products.Id, products.Name, productattribute.StockQuantity, orderId, storeId, languageId, dbcontext);
			}


			// Mark Out Of Stock or Unpublished if Product Stock Becomes 0  
			if (productattribute != null && productattribute.StockQuantity == 0 && products.LowStockActivityId == 1)
			{
				productattribute.AllowOutOfStockOrders = true;
				dbcontext.Entry(productattribute).State = EntityState.Modified;
				dbcontext.SaveChanges();
			}
			else if (productattribute != null && productattribute.StockQuantity == 0 && products.LowStockActivityId == 2)
			{
				BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.OrderNotFound", languageId, dbcontext));
			}
			else
			{
				if (productattribute != null && productattribute.StockQuantity == 0 && products.LowStockActivityId == 0)
				{
					BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.OrderNotFound", languageId, dbcontext));
				}

			}

		}
		public static void EmailNotificationBelowProduct(int productId, string productName, int stockQuantity, int orderId, int storeId, int languageId, ecuadordevContext context)
		{
			var storeName = context.Store.Where(x => x.Id == storeId).FirstOrDefault();

			var Email = (from oI in context.OrderItem
						 join pro in context.Product
						 on oI.ProductId equals pro.Id
						 join ven in context.Vendor
						 on pro.VendorId equals ven.Id
						 where oI.OrderId == orderId && oI.ProductId == productId
						 select new { Name = ven.Name, Email = ven.Email }).FirstOrDefault();

			var messageTemplate = context.MessageTemplate.Where(x => x.Name.Contains("QuantityBelow.StoreOwnerNotification")
											  && (context.StoreMapping.Any(y => y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
											  ).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();

			if (Email != null && messageTemplate != null)
			{
				string body = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Body", languageId, messageTemplate.Body, context);
				string subject = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Subject", languageId, messageTemplate.Subject, context);


				body = body.Replace("%Store.URL%", storeName.Url);
				body = body.Replace("%Store.Name%", LanguageHelper.GetLocalizedValue(storeId, "Name", "Store", languageId, storeName.Name, context));
				body = body.Replace("%Product.Name%", productName);
				body = body.Replace("%Product.ID%", Convert.ToString(productId));
				body = body.Replace("%Product.StockQuantity%", Convert.ToString(stockQuantity));

				var emailAccount = (from b in context.EmailAccount
									where b.StoreId == storeId
									select new
									{
										b
									}).FirstOrDefault()?.b;
				if (emailAccount != null)
				{
					try
					{
						Helper.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, Email.Email, Email.Name, null);
					}
					catch (Exception ex)
					{
						Helper.SentryLogs(ex);
					}
				}

			}
		}

		public static void EmailNotificationBelowProductAttribute(int productId, string productName, int stockQuantity, int orderId, int storeId, int languageId, ecuadordevContext context)
		{
			var storeName = context.Store.Where(x => x.Id == storeId).FirstOrDefault();
			var Email = (from oI in context.OrderItem
						 join pro in context.Product
						 on oI.ProductId equals pro.Id
						 join ven in context.Vendor
						 on pro.VendorId equals ven.Id
						 where oI.OrderId == orderId && oI.ProductId == productId
						 select new { Name = ven.Name, Email = ven.Email }).FirstOrDefault();

			var messageTemplate = context.MessageTemplate.Where(x => x.Name.Contains("QuantityBelow.AttributeCombination.StoreOwnerNotification")
											  && (context.StoreMapping.Any(y => y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
											  ).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();

			if (Email != null && messageTemplate != null)
			{
				string body = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Body", languageId, messageTemplate.Body, context);
				string subject = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Subject", languageId, messageTemplate.Subject, context);

				body = body.Replace("%Store.URL%", storeName.Url);
				body = body.Replace("%Store.Name%", LanguageHelper.GetLocalizedValue(storeId, "Name", "Store", languageId, storeName.Name, context));
				body = body.Replace("%Product.Name%", productName);
				body = body.Replace("%Product.ID%", Convert.ToString(productId));
				body = body.Replace("%AttributeCombination.StockQuantity%", Convert.ToString(stockQuantity));

				var emailAccount = (from b in context.EmailAccount
									where b.StoreId == storeId
									select new
									{
										b
									}).FirstOrDefault()?.b;
				if (emailAccount != null)
				{
					try
					{
						Helper.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, Email.Email, Email.Name, null);
					}
					catch (Exception ex)
					{
						Helper.SentryLogs(ex);
					}
				}

			}
		}
		#endregion

		#region Web Notification

		/// <summary>
		/// Send FCM notification to merchant on web
		/// </summary>
		/// <param name="orderId"></param>
		/// <param name="vendorId"></param>
		/// <param name="storeId"></param>
		/// <param name="languageId"></param>
		/// <param name="dbcontext"></param>
		public static void SendWebNotification(int orderId, int storeId, int languageId, ecuadordevContext dbcontext)
		{
			try
			{
				var vendorId = (from oi in dbcontext.OrderItem
								join p in dbcontext.Product
								on oi.ProductId equals p.Id
								where oi.OrderId == orderId
								select p.VendorId).FirstOrDefault();
				if (orderId > 0 && vendorId > 0)
				{
					var tokenList = (from vt in dbcontext.NBVendorToken
									 join c in dbcontext.Customer
									 on vt.CustomerId equals c.Id
									 where !vt.Deleted && c.VendorId == vendorId
									 && !c.Deleted && c.Active
									 select vt).ToList();

					var varNotificationSettings = dbcontext.Setting.Where(s => s.Name.Contains("NBWebFCMNotificationSetting") && s.StoreId == storeId).ToList();
					var fcmWebAddress = varNotificationSettings?.FirstOrDefault(s => s.Name.ToLower().Contains("webaddress"))?.Value;
					var fcmWebKey = varNotificationSettings?.FirstOrDefault(s => s.Name.ToLower().Contains("webkey"))?.Value;

					var newOrderTitle = LanguageHelper.GetResourseValueByName(storeId, "NB.WebFCMNotification.Fields.NewOrderTitle", languageId, dbcontext);
					var newOrderBody = LanguageHelper.GetResourseValueByName(storeId, "NB.WebFCMNotification.Fields.NewOrderBody", languageId, dbcontext);

					if (tokenList.Count > 0 && !string.IsNullOrEmpty(fcmWebAddress) && !string.IsNullOrEmpty(fcmWebKey))
					{
						foreach (var token in tokenList)
						{
							var httpWebRequest = (HttpWebRequest)WebRequest.Create(fcmWebAddress);
							httpWebRequest.ContentType = "application/json";
							httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, fcmWebKey);
							httpWebRequest.Method = "POST";

							using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
							{
								string strNJson = @"{
                        ""to"": """ + token.Token + @""",
                        ""data"": {
                        ""id"": """ + orderId + @""",
                            },
                        ""notification"":{
                            ""title"": """ + newOrderTitle + @""",
                            ""body"": """ + newOrderBody + @""",
                            ""icon"": """ + "/logo.png" + @""",
                        }
                    }";
								streamWriter.Write(strNJson);
								System.Diagnostics.Debug.WriteLine(strNJson);
								streamWriter.Flush();
							}
							var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
							using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
							{
								string result = streamReader.ReadToEnd();
							}
						}
					}
				}
			}
			catch (Exception ex) { }
		}

		#endregion

		#region Discount

		/// <summary>
		/// vendor discount
		/// </summary>
		/// <param name="storeId"></param>
		/// <param name="vendorId"></param>
		/// <param name="customerId"></param>
		/// <param name="subtotal"></param>
		/// <param name="languageId"></param>
		/// <param name="context"></param>
		/// <returns>CustomerAPIResponses</returns>
		public static CustomerAPIResponses VendorDiscount(int storeId, int vendorId, int customerId, decimal subtotal, int languageId, ecuadordevContext context)
		{
			var discountId = (from sm in context.StoreMapping
							  join discount in context.Discount
							  on sm.EntityId equals discount.Id
							  join dav in context.DiscountAppliedToVendor
							  on discount.Id equals dav.DiscountId
							  where sm.EntityName == "Discount" && sm.StoreId == storeId && discount.DiscountTypeId == 25
							  select dav.DiscountId).FirstOrDefault();
			var applyDiscount = new CustomerAPIResponses();
			if (discountId > 0)
				applyDiscount = ApplyDiscount(customerId, discountId, subtotal, storeId, languageId, context);
			else
			{
				applyDiscount.ErrorMessageTitle = "No Discount Found";
				applyDiscount.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Discountdoesnotapplicable", languageId, context);
				applyDiscount.Status = false;
				applyDiscount.StatusCode = (int)HttpStatusCode.BadRequest;
				applyDiscount.ResponseObj = null;
			}
			return applyDiscount;
		}

		/// <summary>
		/// product discount
		/// </summary>
		/// <param name="storeId"></param>
		/// <param name="productId"></param>
		/// <param name="customerId"></param>
		/// <param name="productPrice"></param>
		/// <param name="languageId"></param>
		/// <param name="context"></param>
		/// <returns>CustomerAPIResponses</returns>
		public static CustomerAPIResponses ProductDiscount(int storeId, int productId, int customerId, decimal productPrice, int languageId, ecuadordevContext context)
		{
			var discountId = context.Product.Where(p => p.Id == productId).Select(x => x.DiscountAppliedToProducts.Where(y => y.ProductId == productId).Select(z => z.DiscountId).FirstOrDefault()).FirstOrDefault();
			var applyDiscount = new CustomerAPIResponses();
			if (discountId > 0)
				applyDiscount = ApplyDiscount(customerId, discountId, productPrice, storeId, languageId, context);
			else
			{
				applyDiscount.ErrorMessageTitle = "No Discount Found";
				applyDiscount.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Discountdoesnotapplicable", languageId, context);
				applyDiscount.Status = false;
				applyDiscount.StatusCode = (int)HttpStatusCode.BadRequest;
				applyDiscount.ResponseObj = null;
			}
			return applyDiscount;
		}

		/// <summary>
		/// discount on order total
		/// </summary>
		/// <param name="storeId"></param>
		/// <param name="customerId"></param>
		/// <param name="subtotal"></param>
		/// <param name="languageId"></param>
		/// <param name="context"></param>
		/// <returns>CustomerAPIResponses</returns>
		public static CustomerAPIResponses OrderTotalDiscount(int storeId, int customerId, decimal subtotal, int languageId, ecuadordevContext context)
		{
			var discountId = (from sm in context.StoreMapping
							  join discount in context.Discount
							  on sm.EntityId equals discount.Id
							  where sm.EntityName == "Discount" && sm.StoreId == storeId && discount.DiscountTypeId == 1
							  select discount.Id).FirstOrDefault();
			var applyDiscount = new CustomerAPIResponses();
			if (discountId > 0)
				applyDiscount = ApplyDiscount(customerId, discountId, subtotal, storeId, languageId, context);
			else
			{
				applyDiscount.ErrorMessageTitle = "No Discount Found";
				applyDiscount.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Discountdoesnotapplicable", languageId, context);
				applyDiscount.Status = false;
				applyDiscount.StatusCode = (int)HttpStatusCode.BadRequest;
				applyDiscount.ResponseObj = null;
			}
			return applyDiscount;
		}

		/// <summary>
		/// apply discount
		/// </summary>
		/// <param name="customerId"></param>
		/// <param name="discountId"></param>
		/// <param name="subTotal"></param>
		/// <param name="storeId"></param>
		/// <param name="languageId"></param>
		/// <param name="dbcontext"></param>
		/// <returns>CustomerAPIResponses</returns>
		public static CustomerAPIResponses ApplyDiscount(int customerId, int discountId, decimal subTotal, int storeId, int languageId, ecuadordevContext dbcontext)
		{
			var customerAPIResponses = new CustomerAPIResponses();
			try
			{
				var isCustomerExist = dbcontext.Customer.Where(x => x.Id == customerId).Any();
				if (!isCustomerExist)
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
				var discount = dbcontext.Discount.Where(x => x.Id == discountId).FirstOrDefault();
				if (discountId > 0)
				{
					if (discount.StartDateUtc <= DateTime.UtcNow && discount.EndDateUtc >= DateTime.UtcNow)
					{
						if (discount.UsePercentage)
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (discount.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, null, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, customerId, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (discount.MaximumDiscountPercentageAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)discount.MaximumDiscountPercentageAmount / 100f);
									if (discount.DiscountAmount < maximumDiscount)
										maximumDiscount = discount.DiscountAmount;
								}
								else
									maximumDiscount = discount.DiscountAmount;

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = discount.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + discount.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
						else
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (discount.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, null, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, customerId, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (discount.MaximumDiscountPercentageAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)discount.MaximumDiscountPercentageAmount / 100f);
									if (discount.DiscountAmount < maximumDiscount)
										maximumDiscount = discount.DiscountAmount;
								}
								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = discount.DiscountAmount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = discount.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + discount.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}

							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
					}
					else if (discount.StartDateUtc == null && discount.EndDateUtc >= DateTime.UtcNow)
					{
						if (discount.UsePercentage)
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (discount.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, null, null, 0, 1, db: dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, customerId, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (discount.MaximumDiscountAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)discount.DiscountPercentage / 100f);
									if (discount.MaximumDiscountAmount < maximumDiscount)
										maximumDiscount = discount.MaximumDiscountAmount ?? 0;
								}
								else
									maximumDiscount = (decimal)((float)subTotal * (float)discount.DiscountPercentage / 100f);

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = discount.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + discount.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
						else
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (discount.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, null, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, customerId, null, 0, 1, db: dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (discount.MaximumDiscountPercentageAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)discount.MaximumDiscountPercentageAmount / 100f);
									if (discount.DiscountAmount < maximumDiscount)
										maximumDiscount = discount.DiscountAmount;
								}
								else
									maximumDiscount = discount.DiscountAmount;

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = discount.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + discount.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
					}
					else if (discount.StartDateUtc <= DateTime.UtcNow && discount.EndDateUtc == null)
					{
						if (discount.UsePercentage)
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (discount.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, null, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, customerId, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (discount.MaximumDiscountAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)discount.DiscountPercentage / 100f);
									if (discount.MaximumDiscountAmount < maximumDiscount)
										maximumDiscount = discount.MaximumDiscountAmount ?? 0;

								}
								else
									maximumDiscount = (decimal)((float)subTotal * (float)discount.DiscountPercentage / 100f);

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = discount.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + discount.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
						else
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (discount.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, null, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, customerId, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (discount.MaximumDiscountPercentageAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)discount.MaximumDiscountPercentageAmount / 100f);
									if (discount.DiscountAmount < maximumDiscount)
										maximumDiscount = discount.DiscountAmount;
								}
								else
									maximumDiscount = discount.DiscountAmount;

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = discount.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + discount.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}

							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
					}
					else if (discount.StartDateUtc != null)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodenotapplicable", languageId, dbcontext);

						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
					else if (discount.EndDateUtc != null)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodenotapplicable", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
					else if (discount.EndDateUtc != null && discount.EndDateUtc != null)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodenotapplicable", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
					else
					{
						if (discount.UsePercentage)
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (discount.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, null, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, customerId, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (discount.MaximumDiscountAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)discount.DiscountPercentage / 100f);
									if (discount.MaximumDiscountAmount < maximumDiscount)
										maximumDiscount = discount.MaximumDiscountAmount ?? 0;
								}
								else
									maximumDiscount = (decimal)((float)subTotal * (float)discount.DiscountPercentage / 100f);

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = discount.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + discount.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
						else
						{
							bool nTimes = false;
							bool nTimesPerCustomer = false;
							bool unLimited = false;
							switch (discount.DiscountLimitationId)
							{
								case (int)DiscountLimitationType.NTimesOnly:
									{
										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, null, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimes = true;
										}
										else
										{
											nTimes = false;
										}
									}
									break;
								case (int)DiscountLimitationType.NTimesPerCustomer:
									{

										var usedTimes = Helper.GetAllDiscountUsageHistory(discount.Id, customerId, null, 0, 1, dbcontext).ToList().Count();
										if (usedTimes < discount.LimitationTimes)
										{
											nTimesPerCustomer = true;
										}
										else
										{
											nTimesPerCustomer = false;
										}
									}
									break;
								case (int)DiscountLimitationType.Unlimited:
									unLimited = true;
									break;
								default:
									break;

							}
							if (nTimes || nTimesPerCustomer || unLimited)
							{
								var maximumDiscount = decimal.Zero;
								if (discount.MaximumDiscountPercentageAmount > 0)
								{
									maximumDiscount = (decimal)((float)subTotal * (float)discount.MaximumDiscountPercentageAmount / 100f);
									if (discount.DiscountAmount < maximumDiscount)
										maximumDiscount = discount.DiscountAmount;
								}
								else
									maximumDiscount = discount.DiscountAmount;

								if (subTotal >= maximumDiscount)
								{
									var DiscountAmount = maximumDiscount;
									DiscountModel discountModel = new DiscountModel();
									discountModel.DiscountAmount = DiscountAmount;
									discountModel.DiscountId = discount.Id;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
									customerAPIResponses.Status = true;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.ResponseObj = discountModel;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + discount.DiscountAmount, languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}

							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
					}
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodenotapplicable", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				throw;
			}
		}

		public static MerchantOffer GetMerchantOffer(int storeId, int vendorId, ecuadordevContext context)
		{
			var vendorDiscount = (from sm in context.StoreMapping
								  join discount in context.Discount
								  on sm.EntityId equals discount.Id
								  join dav in context.DiscountAppliedToVendor
								  on discount.Id equals dav.DiscountId
								  where sm.EntityName == "Discount" && sm.StoreId == storeId && discount.DiscountTypeId == 25 && dav.VendorId == vendorId
								  select discount).FirstOrDefault();
			if (vendorDiscount == null)
				return null;

			var response = new MerchantOffer()
			{
				DiscountId = vendorDiscount.Id,
				DiscountName = vendorDiscount.Name,
				CouponCode = vendorDiscount.CouponCode
			};
			return response;
		}
		#region Merchant and Product Offers
		public static string GetMerchantOffers(int storeId, int vendorId, ecuadordevContext context)
		{
			var response = string.Empty;
			var vendorDiscount = (from sm in context.StoreMapping
								  join discount in context.Discount
								  on sm.EntityId equals discount.Id
								  join dav in context.DiscountAppliedToVendor
								  on discount.Id equals dav.DiscountId
								  where sm.EntityName == "Discount" && sm.StoreId == storeId && discount.DiscountTypeId == 25 && dav.VendorId == vendorId
								  select discount).FirstOrDefault();
			if (vendorDiscount == null)
			{
				response = string.Empty;
				return null;
			}
			else
			{
				response = vendorDiscount.Name;
			}


			return response;
		}
		public static string GetProductOffer(int storeId, int productId, ecuadordevContext context)
		{
			var response = string.Empty;
			var productOffer = (from sm in context.StoreMapping
								join discount in context.Discount
								on sm.EntityId equals discount.Id
								join dav in context.DiscountAppliedToProducts
								on discount.Id equals dav.DiscountId
								where sm.EntityName == "Discount" && sm.StoreId == storeId && discount.DiscountTypeId == 2 && dav.ProductId == productId
								select discount).FirstOrDefault();

			if (productOffer == null)
			{
				response = string.Empty;
				return null;
			}
			else
			{
				response = productOffer.Name;
			}
			return response;
		}
		#endregion
		#endregion


		#region Rating and review

		/// <summary>
		/// Get value idicates wether all reviews for this order is done, including Agent, Merchant and for all order items.
		/// </summary>
		/// <param name="orderId"></param>
		/// <returns></returns>
		public static bool ShowRatingReviewOption(int orderId, int storeId, ecuadordevContext dbcontext)
		{
			if (orderId > 0)
			{
				var order = dbcontext.Order.FirstOrDefault(o => o.Id == orderId);
				if (order == null)
					return false;

				if (order.OrderStatusId != (int)OrderHistoryEnum.OrderDelivered)
					return false;

				var query = dbcontext.RatingReviews.AsEnumerable()?.Where(q => q.OrderId == orderId);
				//any review found?
				if (query.Count() == 0)
					return true;

				var orderReviews = query.ToList();
				//agent review enabled and found?
				var IsAgentReviewEnabled = dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == storeId && x.Name == "ratingreviewsettings.agent.isactive").Any() ?
										   dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == storeId && x.Name == "ratingreviewsettings.agent.isactive").FirstOrDefault().Value.ToLower().ToString() == "true" : false;
				if (IsAgentReviewEnabled)
				{
					if (orderReviews.Where(o => o.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Agent)?.Count() == 0)
						return true;
				}

				//merchant review found?
				var IsMerchantReviewEnabled = dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == storeId && x.Name == "ratingreviewsettings.merchant.isactive").Any() ?
											  dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == storeId && x.Name == "ratingreviewsettings.merchant.isactive").FirstOrDefault().Value.ToLower().ToString() == "true" : false;
				if (IsMerchantReviewEnabled)
				{
					if (orderReviews.Where(o => o.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant)?.Count() == 0)
						return true;
				}

				//all item review found?
				var isItemReviewEnabled = dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == storeId && x.Name == "ratingreviewsettings.item.isactive").Any() ?
										dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == storeId && x.Name == "ratingreviewsettings.item.isactive").FirstOrDefault().Value.ToLower().ToString() == "true" : false;
				if (isItemReviewEnabled)
				{
					var orderItems = dbcontext.OrderItem.Where(o => o.OrderId == orderId);
					if (orderReviews.Where(o => o.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item && o.EntityId > 0)?.Count() != orderItems?.Count())
						return true;
				}
			}

			return false;
		}

		/// <summary>
		/// check agent review
		/// </summary>
		/// <param name="storeId"></param>
		/// <param name="orderId"></param>
		/// <returns></returns>
		public static bool IsAgentReviewEnabledAndFound(int storeId, int orderId, ecuadordevContext dbcontext)
		{
			var enabledAndFound = false;
			var IsAgentReviewEnabled = dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == storeId && x.Name == "ratingreviewsettings.agent.isactive").Any() ?
										dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == storeId && x.Name == "ratingreviewsettings.agent.isactive").FirstOrDefault().Value.ToLower().ToString() == "true" : false;

			if (IsAgentReviewEnabled)
			{
				var isFound = dbcontext.RatingReviews.Where(x => x.OrderId == orderId
														  && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Agent).Any() ? true : false;
				enabledAndFound = isFound;
			}
			return enabledAndFound;
		}

		/// <summary>
		/// check merchant review
		/// </summary>
		/// <param name="storeId"></param>
		/// <param name="orderId"></param>
		/// <returns></returns>
		public static bool IsMerchantReviewEnabledAndFound(int storeId, int orderId, ecuadordevContext dbcontext)
		{
			var enabledAndFound = false;
			var IsMerchantReviewEnabled = dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == storeId && x.Name == "ratingreviewsettings.merchant.isactive").Any() ?
			dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == storeId && x.Name == "ratingreviewsettings.merchant.isactive").FirstOrDefault().Value.ToLower().ToString() == "true" : false;

			if (IsMerchantReviewEnabled)
			{
				var isFound = dbcontext.RatingReviews.Where(x => x.OrderId == orderId
														  && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant).Any() ? true : false;
				enabledAndFound = isFound;
			}
			return enabledAndFound;
		}

		/// <summary>
		/// check item review 
		/// </summary>
		/// <param name="storeId"></param>
		/// <param name="orderId"></param>
		/// <param name="productId"></param>
		/// <returns></returns>
		public static bool IsItemReviewEnabledAndFound(int storeId, int orderId, int productId, ecuadordevContext db)
		{
			var enabledAndFound = false;
			var isEnabled = db.Setting.AsEnumerable().Where(x => x.StoreId == storeId && x.Name == "ratingreviewsettings.item.isactive").Any() ?
										db.Setting.AsEnumerable().Where(x => x.StoreId == storeId && x.Name == "ratingreviewsettings.item.isactive").FirstOrDefault().Value.ToLower().ToString() == "true" : false;
			if (isEnabled)
			{
				var isFound = db.RatingReviews.Where(x => x.OrderId == orderId
														&& x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item && x.EntityId == productId).Any() ? true : false;
				enabledAndFound = isFound;
			}
			return enabledAndFound;
		}

		#endregion

		#region Favorite merchant

		/// <summary>
		/// Add/Remove Favorite merchant based on customer
		/// </summary>
		/// <param name="model"></param>
		/// <param name="languageId"></param>
		/// <param name="context"></param>
		/// <returns>FavoriteMerchantResponce</returns>
		public static FavoriteMerchantResponce AddREmoveFavoriteMerchant(AddRemoveFavoriteMerchant model, int languageId, ecuadordevContext context)
		{
			if (context == null)
				return null;
			if (model == null)
				return null;
			if (model.CustomerId == 0)
				return null;
			if (model.MerchantId == 0)
				return null;
			var responce = new FavoriteMerchantResponce();
			if (context.Customer.FirstOrDefault(c => c.Id == model.CustomerId && !c.Deleted) == null)
			{
				responce.Message = LanguageHelper.GetResourseValueByName(model.StoreId, "NB.Merchant.Favorite.Customer..NotFound", languageId, context);
				return responce;
			}
			if (context.Vendor.FirstOrDefault(v => v.Id == model.MerchantId && !v.Deleted) == null)
			{
				responce.Message = LanguageHelper.GetResourseValueByName(model.StoreId, "NB.Merchant.Favorite.Merchant..NotFound", languageId, context);
				return responce;
			}

			var record = context.FavoriteMerchant.AsEnumerable().FirstOrDefault(f => f.CustomerId == model.CustomerId && f.VendorId == model.MerchantId);
			if (record == null)
			{
				//insert
				var favoriteMerchant = new FavoriteMerchant()
				{
					CustomerId = model.CustomerId,
					VendorId = model.MerchantId
				};

				context.FavoriteMerchant.Add(favoriteMerchant);
				responce.IsRemoved = false;
				responce.Success = true;
				responce.Message = LanguageHelper.GetResourseValueByName(model.StoreId, "NB.Merchant.Favorite.Added.Message", languageId, context);
			}
			else
			{
				//remove
				context.FavoriteMerchant.Remove(record);
				responce.IsRemoved = true;
				responce.Success = true;
				responce.Message = LanguageHelper.GetResourseValueByName(model.StoreId, "NB.Merchant.Favorite.Removed.Message", languageId, context);
			}
			context.SaveChanges();
			return responce;
		}

		/// <summary>
		/// value idicates whether merchant already in favorite merchant table for this customer
		/// </summary>
		/// <param name="merchantId"></param>
		/// <param name="customerId"></param>
		/// <returns>True/False</returns>
		public static bool IsMerchantInFavorite(int merchantId, int customerId, ecuadordevContext context)
		{
			if (customerId == 0 || merchantId == 0)
				return false;

			return context.FavoriteMerchant.AsEnumerable().Where(f => f.CustomerId == customerId && f.VendorId == merchantId).Any();
		}

		/// <summary>
		/// Get customer full name
		/// </summary>
		/// <param name="customerId"></param>
		/// <param name="dbcontext"></param>
		/// <returns></returns>
		public static string GetCustomerFullName(int customerId, ecuadordevContext dbcontext)
		{
			if (customerId == 0)
				return string.Empty;

			var customerAttributes = dbcontext.GenericAttribute.Where(x => x.KeyGroup == "Customer" && x.EntityId == customerId).ToList();
			var firstName = customerAttributes?.FirstOrDefault(c => c.Key == "FirstName")?.Value;
			var lastName = customerAttributes?.FirstOrDefault(c => c.Key == "LastName")?.Value;

			var fullName = string.Empty;
			if (!string.IsNullOrWhiteSpace(firstName) && !string.IsNullOrWhiteSpace(lastName))
				fullName = $"{firstName} {lastName}";
			else
			{
				if (!string.IsNullOrWhiteSpace(firstName))
					fullName = firstName;

				if (!string.IsNullOrWhiteSpace(lastName))
					fullName = lastName;
			}

			return fullName;
		}

		#endregion

		#region Product and Merchant Name
		public static void ProductsAndMerchantName(List<OrderHistoryResponseModel> orderHistoryResponseModels, ecuadordevContext dbcontext)
		{
			try
			{
				foreach (var Items in orderHistoryResponseModels)
				{
					var orderItems = dbcontext.OrderItem.Where(i => i.OrderId == Items.OrderId);
					var product = dbcontext.Product.FirstOrDefault(p => p.Id == orderItems.FirstOrDefault().ProductId);
					var merchantName = dbcontext.Vendor.FirstOrDefault(x => x.Id == product.VendorId)?.Name;

					var groupItems = orderItems.GroupBy(p => p.ProductId).Select(g => new
					{
						ProductId = g.Key,
						Quantity = g.Sum(t => t.Quantity)
					});
					foreach (var prod in groupItems?.ToList())
					{
						var item = new OrdersItem
						{
							Name = dbcontext.Product.FirstOrDefault(p => p.Id == prod.ProductId).Name,
							Quantity = prod.Quantity
						};
						Items.OrderItem.Add(item);
					}
					Items.MerchantName = merchantName;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
			}

		}
		#endregion

		public static int ProductPrepareTime(int orderId, ecuadordevContext dbcontext)
		{
			var ProductsIds = dbcontext.OrderItem.Where(x => x.OrderId == orderId).ToList();
			int PrepareTime = 0;
			int ProductPrepareTime = 0;
			string VendorLatLong = string.Empty;

			foreach (var productId in ProductsIds)
			{
				var productPrepareTime = (from p in dbcontext.Product
										  where p.Id == productId.ProductId
										  select new
										  {
											  p.PrepareTime
										  }) != null ? (from p in dbcontext.Product
														where p.Id == productId.ProductId
														select new
														{
															p.PrepareTime
														}).FirstOrDefault().PrepareTime : 0;
				if (productPrepareTime.HasValue)
					ProductPrepareTime = productPrepareTime.Value + ProductPrepareTime;
				var vendorId = dbcontext.Product.Where(x => x.Id == productId.ProductId).Select(x => x.VendorId).FirstOrDefault();
				VendorLatLong = dbcontext.Vendor.Where(x => x.Id == vendorId).FirstOrDefault().Geolocation;
			}
			PrepareTime = ProductPrepareTime / ProductsIds.Count();
			return PrepareTime;
		}

		#region Delivery Date Time
		public static bool DeliveryDateTime(List<OrderNotesNotifiactionModel> orderNotesNotifiactionModels, int customerId, int storeId, ecuadordevContext dbcontext)
		{
			try
			{
				foreach (var Items in orderNotesNotifiactionModels)
				{

					var deliveryslottiming = dbcontext.DeliverySlotBooking.Where(x => x.OrderId == Items.OrderId && x.CustomerId == customerId).Any();
					if (deliveryslottiming)
					{
						var deliverytime = dbcontext.DeliverySlotBooking.Where(x => x.OrderId == Items.OrderId && x.CustomerId == customerId).FirstOrDefault();
						var startTime = Helper.ConvertToUserTime(deliverytime.DeliveryStartTime, DateTimeKind.Utc, context: dbcontext);
						var endTime = Helper.ConvertToUserTime(deliverytime.DeliveryEndTime, DateTimeKind.Utc, context: dbcontext);
						var deliveryDate = Helper.ConvertToUserTime(deliverytime.DeliveryDateUtc, DateTimeKind.Utc, context: dbcontext);
						var deliveryStartTime = startTime.ToString("hh:mm tt");
						var deliveryEndTime = endTime.ToString("hh:mm tt");
						Items.OrderDeliveryDate = deliveryDate.ToString("dd MMM yyyy");
						Items.OrderDeliveryTime = deliveryStartTime + "-" + deliveryEndTime;
					}
					else
					{
						var productPrepareTime = Helper.ProductPrepareTime(Items.OrderId, dbcontext);
						var createdOnUtc = Helper.ConvertToUserTime(Items.OrderDate, DateTimeKind.Utc, context: dbcontext);
						var OrderFromTime = createdOnUtc.AddMinutes(productPrepareTime).ToString("hh:mm tt");
						var arrivaltime = Convert.ToInt32(dbcontext.Setting.Where(x => x.Name.Contains("setting.customer.arrivaltime") && x.StoreId == storeId).FirstOrDefault()?.Value ?? "10");
						var OrderToTime = createdOnUtc.AddMinutes(productPrepareTime + arrivaltime).ToString("hh:mm tt");
						Items.OrderDeliveryDate = Items.OrderDate.ToString("dd MMM yyyy");
						Items.OrderDeliveryTime = OrderFromTime + "-" + OrderToTime;
					}
				}

			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				return false;
			}
			return true;

		}
		#endregion
		#region Wallet
		public static string EncryptText(string plainText, ecuadordevContext context, string encryptionPrivateKey = "")
		{
			string encKey = context.Setting.Where(x => x.Name.Contains("securitysettings.encryptionkey")).FirstOrDefault().Value;
			if (string.IsNullOrEmpty(plainText))
				return plainText;

			if (string.IsNullOrEmpty(encryptionPrivateKey))
				encryptionPrivateKey = encKey;

			using (var provider = new TripleDESCryptoServiceProvider())
			{
				provider.Key = Encoding.ASCII.GetBytes(encryptionPrivateKey.Substring(0, 16));
				provider.IV = Encoding.ASCII.GetBytes(encryptionPrivateKey.Substring(8, 8));

				var encryptedBinary = EncryptTextToMemory(plainText, provider.Key, provider.IV);
				return Convert.ToBase64String(encryptedBinary);
			}
		}
		private static byte[] EncryptTextToMemory(string data, byte[] key, byte[] iv)
		{
			using (var ms = new MemoryStream())
			{
				using (var cs = new CryptoStream(ms, new TripleDESCryptoServiceProvider().CreateEncryptor(key, iv), CryptoStreamMode.Write))
				{
					var toEncrypt = Encoding.Unicode.GetBytes(data);
					cs.Write(toEncrypt, 0, toEncrypt.Length);
					cs.FlushFinalBlock();
				}

				return ms.ToArray();
			}
		}
		public static string GetMaskedCreditCardNumber(string creditCardNumber)
		{
			if (string.IsNullOrEmpty(creditCardNumber))
				return string.Empty;

			if (creditCardNumber.Length <= 4)
				return creditCardNumber;

			var last4 = creditCardNumber.Substring(creditCardNumber.Length - 4, 4);
			var maskedChars = string.Empty;
			for (var i = 0; i < creditCardNumber.Length - 4; i++)
			{
				maskedChars += "*";
			}

			return maskedChars + last4;
		}
		public static CustomerAddresses GetCustomerBillingAddress(int customerId, ecuadordevContext context)
		{


			//existing addresses
			var addresses = context.CustomerAddresses.Where(x => x.CustomerId == customerId).ToList();
			var customer = context.Customer.Where(x => x.Id == customerId).FirstOrDefault();
			if (addresses != null && addresses.Count > 0)
			{

				return addresses.FirstOrDefault();
			}
			else
			{

				return null;
			}


		}

		public static void GenerateOrderGUID(ProcessPaymentRequest processPaymentRequest)
		{
			if (processPaymentRequest.OrderGuid == Guid.Empty)
			{
				processPaymentRequest.OrderGuid = Guid.NewGuid();
				processPaymentRequest.OrderGuidGeneratedOnUtc = DateTime.UtcNow;
			}

		}
		public static string GenerateOrderNumberCustom(Order order, ecuadordevContext context)
		{
			string customOrderMask = context.Setting.Where(x => x.Name.Contains("customordernumbermask")).FirstOrDefault().Value;
			if (string.IsNullOrEmpty(customOrderMask))
				return order.Id.ToString();

			var customNumber = customOrderMask
				.Replace("{ID}", order.Id.ToString())
				.Replace("{YYYY}", order.CreatedOnUtc.ToString("yyyy"))
				.Replace("{YY}", order.CreatedOnUtc.ToString("yy"))
				.Replace("{MM}", order.CreatedOnUtc.ToString("MM"))
				.Replace("{DD}", order.CreatedOnUtc.ToString("dd")).Trim();


			return customNumber;
		}
		#endregion

		#region Payment plugins
		/// <summary>
		/// payment method managed from admin panel
		/// </summary>
		/// <param name="cartProductsModel"></param>
		/// <param name="Paymentsetting"></param>
		/// <param name="languageId"></param>
		/// <param name="dbcontext"></param>
		/// <returns></returns>
		public static List<PaymentMethodModel> PaymentSection(CustomerDetailsModel cartProductsModel, string Paymentsetting, int languageId, ecuadordevContext dbcontext)
		{
			List<PaymentMethodModel> paymentMethodModels = new List<PaymentMethodModel>();
			PaymentMethodModel paymentMethodModel3 = new PaymentMethodModel();
			//PayPal Payment
			paymentMethodModel3.PaymentMethodId = (int)PaymentMethodEnum.PayPal;
			paymentMethodModel3.PaymentMethodName = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllPaymentMethods.Paypal", languageId, dbcontext);
			if (Paymentsetting.Contains("Payments.PayPalStandard"))
			{
				paymentMethodModel3.isShow = true;
			}
			else
			{
				paymentMethodModel3.isShow = false;
			}
			paymentMethodModels.Add(paymentMethodModel3);

			//stripe payment
			paymentMethodModel3 = new PaymentMethodModel();
			paymentMethodModel3.PaymentMethodId = (int)PaymentMethodEnum.Card;
			paymentMethodModel3.PaymentMethodName = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllPaymentMethods.CreditDebitCart", languageId, dbcontext);
			if (Paymentsetting.Contains("Payments.Stripe"))
			{
				paymentMethodModel3.isShow = true;
			}
			else
			{
				paymentMethodModel3.isShow = false;
			}
			paymentMethodModels.Add(paymentMethodModel3);

			//Cash on Delivery
			paymentMethodModel3 = new PaymentMethodModel();
			paymentMethodModel3.PaymentMethodId = (int)PaymentMethodEnum.COD;
			paymentMethodModel3.PaymentMethodName = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllPaymentMethods.COD", languageId, dbcontext);
			if (Paymentsetting.Contains("Payments.CashOnDelivery"))
			{
				paymentMethodModel3.isShow = true;
			}
			else
			{
				paymentMethodModel3.isShow = false;
			}
			paymentMethodModels.Add(paymentMethodModel3);

			//Sqaure Payment
			paymentMethodModel3 = new PaymentMethodModel();
			paymentMethodModel3.PaymentMethodId = (int)PaymentMethodEnum.Square;
			paymentMethodModel3.PaymentMethodName = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllPaymentMethods.Square", languageId, dbcontext);
			if (Paymentsetting.Contains("Payments.Square"))
			{
				paymentMethodModel3.isShow = true;
			}
			else
			{
				paymentMethodModel3.isShow = false;
			}

			paymentMethodModels.Add(paymentMethodModel3);

			//RazorPay Payment
			paymentMethodModel3 = new PaymentMethodModel();
			paymentMethodModel3.PaymentMethodId = (int)PaymentMethodEnum.Razorpay;
			paymentMethodModel3.PaymentMethodName = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllPaymentMethods.RazorPay", languageId, dbcontext);
			if (Paymentsetting.Contains("Atluz.Razorpay"))
			{
				paymentMethodModel3.isShow = true;
			}
			else
			{
				paymentMethodModel3.isShow = false;
			}
			paymentMethodModels.Add(paymentMethodModel3);
			return paymentMethodModels;
		}
		#endregion

		#region WebApp API calling

		/// <summary>
		/// Invoke order placed Web API 
		/// </summary>
		/// <param name="db"></param>
		/// <param name="storeId"></param>
		/// <param name="ApiKey"></param>
		/// <param name="OrderId"></param>
		public static void InvokeOrderPlacedSignalR(ecuadordevContext db, int storeId, string ApiKey, int OrderId)
		{
			var storeUrl = GetStoreBaseURL(db, storeId);
			var signalrApiUrl = $"{storeUrl}api/v1/InvokeOrderPlacedNotifySignalR";
			object model = new { ApiKey, OrderId };
			//call API
			var result = CallAPI(signalrApiUrl, "POST", model);
		}

		/// <summary>
		/// Get store base url
		/// </summary>
		/// <param name="db"></param>
		/// <param name="storeId"></param>
		/// <returns>store base url</returns>
		private static string GetStoreBaseURL(ecuadordevContext db, int storeId)
		{
			string storeUrl = "";
			var setting = db.Setting.FirstOrDefault(x => x.Name.ToLower().Equals("Nb.Store.BaseUrl") && x.StoreId == storeId);
			if (setting != null)
				storeUrl = setting.Value;

			if (string.IsNullOrEmpty(storeUrl))
			{
				//StoreBaseUrl still not found in setting table, than use store url
				var store = db.Store.FirstOrDefault(s => s.Id == storeId);
				storeUrl = store?.Url;
			}

			return storeUrl;
		}

		/// <summary>
		/// Call any web API and get Common response
		/// </summary>
		/// <returns>The CommonAPIResponses</returns>
		public static CommonAPIResponses CallAPI(string url, string methodType, object jsonPayload)
		{
			var resultModel = new CommonAPIResponses();
			try
			{
				var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
				httpWebRequest.ContentType = "application/json";
				httpWebRequest.Method = methodType;

				string json = JsonConvert.SerializeObject(jsonPayload);
				using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
				{
					streamWriter.Write(json);
					streamWriter.Flush();
					streamWriter.Close();
				}

				var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
				using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
				{
					resultModel = JsonConvert.DeserializeObject<CommonAPIResponses>(streamReader.ReadToEnd());
				}
				return resultModel;
			}
			catch (Exception ex)
			{
				//log error
				SentryLogs(ex);

				resultModel.ErrorMessageTitle = "Error!!";
				resultModel.ErrorMessage = ex.Message;
				resultModel.Status = false;
				resultModel.StatusCode = 400;
				resultModel.ResponseObj = null;
				return resultModel;
			}
		}

		#endregion

	}
}