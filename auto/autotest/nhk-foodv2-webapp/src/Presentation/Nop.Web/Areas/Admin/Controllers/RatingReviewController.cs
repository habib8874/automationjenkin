﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Services.Catalog;
using Nop.Services.Events;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.RatingReview;
using Nop.Web.Areas.Admin.Models.RatingReview;
using Nop.Core.Domain.RatingReview;
using System.Collections.Generic;
using Nop.Core.Domain.Stores;
using Nop.Services.Customers;
using Nop.Services.Vendors;
using Nop.Web.Framework.Models.Extensions;
using Nop.Services.Messages;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Framework.Extensions;
using Nop.Core.Domain.Catalog;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class RatingReviewController : BaseAdminController
    {
        #region Fields
        private readonly IVendorService _vendorService;
        private readonly IRatingReviewService _ratingReviewService;
        private readonly IProductService _productService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly IEventPublisher _eventPublisher;
        private readonly IStoreService _storeService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly INotificationService _notificationService;
        private readonly IReviewOptionService _reviewOptionService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly CatalogSettings _catalogSettings;

        #endregion Fields

        #region Ctor

        public RatingReviewController(
            IVendorService vendorService,
            IRatingReviewService ratingReviewService,
            IProductService productService,
            IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService,
            IPermissionService permissionService,
            IEventPublisher eventPublisher,
            IStoreService storeService,
            ICustomerActivityService customerActivityService,
            IWorkContext workContext,
            ICustomerService customerService,
            INotificationService notificationService,
            IReviewOptionService reviewOptionService,
            IBaseAdminModelFactory baseAdminModelFactory,
            CatalogSettings catalogSettings)
        {
            _vendorService = vendorService;
            _ratingReviewService = ratingReviewService;
            _productService = productService;
            _dateTimeHelper = dateTimeHelper;
            _localizationService = localizationService;
            _permissionService = permissionService;
            _eventPublisher = eventPublisher;
            _storeService = storeService;
            _customerActivityService = customerActivityService;
            _workContext = workContext;
            _customerService = customerService;
            _notificationService = notificationService;
            _reviewOptionService = reviewOptionService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _catalogSettings = catalogSettings;
        }

        #endregion

        #region Utilities

        protected virtual void PrepareItemReviewModel(ItemRatingModel model,
            RatingReviews itemReview, bool excludeProperties, bool formatReviewAndReplyText)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (itemReview == null)
                throw new ArgumentNullException(nameof(itemReview));

            //get require entities
            var customer = _customerService.GetCustomerById(itemReview.CustomerId);
            var store = _storeService.GetStoreById(itemReview.StoreId);
            var product = _productService.GetProductById(itemReview.EntityId);
            var vendor = _vendorService.GetVendorById(product?.VendorId ?? 0);

            model.Id = itemReview.Id;
            model.StoreName = store?.Name ?? _localizationService.GetResource("NB.Admin.Common.Entity.Deleted");
            model.ItemId = itemReview.EntityId;
            model.Item = product?.Name;
            model.Merchant = vendor?.Name;
            model.CustomerId = itemReview.CustomerId;
            model.CustomerInfo = customer == null ? _localizationService.GetResource("NB.Admin.Common.Entity.Deleted") : customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest");
            model.Rating = itemReview.Rating;
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(itemReview.CreatedOnUtc, DateTimeKind.Utc);

            if (!excludeProperties)
            {
                model.Title = itemReview.Title;
                if (formatReviewAndReplyText)
                {
                    model.ReviewText = Core.Html.HtmlHelper.FormatText(itemReview.ReviewText, false, true, false, false, false, false);
                    model.ReplyText = Core.Html.HtmlHelper.FormatText(itemReview.ReplyText, false, true, false, false, false, false);
                }
                else
                {
                    model.ReviewText = itemReview.ReviewText;
                    model.ReplyText = itemReview.ReplyText;
                }
                model.IsApproved = itemReview.IsApproved;
            }

            //a vendor should have access only to his products
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

            //prepare feedback model
            PrepareItemFeedbackModel(model);
        }


        protected virtual void PrepareMerchantReviewModel(MerchantRatingModel model,
           RatingReviews merchantReview, bool excludeProperties, bool formatReviewAndReplyText)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (merchantReview == null)
                throw new ArgumentNullException(nameof(merchantReview));

            //get require entities
            var customer = _customerService.GetCustomerById(merchantReview.CustomerId);
            var store = _storeService.GetStoreById(merchantReview.StoreId);
            var vendor = _vendorService.GetVendorById(merchantReview?.EntityId ?? 0);

            model.Id = merchantReview.Id;
            model.ReviewOptions = merchantReview.ReviewOptions;
            model.DislikedReviewOptions = merchantReview.DislikedReviewOptions;
            model.StoreName = store?.Name ?? _localizationService.GetResource("NB.Admin.Common.Entity.Deleted");
            model.MerchantId = merchantReview.EntityId;
            model.Merchant = vendor?.Name;
            model.LikeDislike = merchantReview.IsLiked;
            model.CustomerId = merchantReview.CustomerId;
            model.CustomerInfo = customer == null ? _localizationService.GetResource("NB.Admin.Common.Entity.Deleted") : customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest");
            model.Rating = merchantReview.Rating;
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(merchantReview.CreatedOnUtc, DateTimeKind.Utc);
            if (!excludeProperties)
            {
                model.Title = merchantReview.Title;
                if (formatReviewAndReplyText)
                {
                    model.ReviewText = Core.Html.HtmlHelper.FormatText(merchantReview.ReviewText, false, true, false, false, false, false);
                    model.ReplyText = Core.Html.HtmlHelper.FormatText(merchantReview.ReplyText, false, true, false, false, false, false);
                }
                else
                {
                    model.ReviewText = merchantReview.ReviewText;
                    model.ReplyText = merchantReview.ReplyText;
                }
                model.IsApproved = merchantReview.IsApproved;
            }

            //a vendor should have access only to his products
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

            //prepare feedback model
            PrepareMerchantFeedbackModel(model);
        }

        protected virtual void PrepareAgentReviewModel(AgentRatingModel model,
           RatingReviews agentReview, bool excludeProperties, bool formatReviewAndReplyText)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (agentReview == null)
                throw new ArgumentNullException(nameof(agentReview));

            var customer = _customerService.GetCustomerById(agentReview.CustomerId);
            var store = _storeService.GetStoreById(agentReview.StoreId);
            var agentCust = _customerService.GetCustomerById(agentReview.EntityId);

            model.Id = agentReview.Id;
            model.StoreName = store?.Name ?? _localizationService.GetResource("NB.Admin.Common.Entity.Deleted");
            model.AgentId = agentReview.EntityId;
            model.Agent = agentCust?.GetFullName();
            model.ReviewOptions = agentReview.ReviewOptions;
            model.DislikedReviewOptions = agentReview.DislikedReviewOptions;
            model.LikeDislike = agentReview.IsLiked;
            model.CustomerId = agentReview.CustomerId;
            model.CustomerInfo = customer == null ? _localizationService.GetResource("NB.Admin.Common.Entity.Deleted") : customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest");
            model.Rating = agentReview.Rating;
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(agentReview.CreatedOnUtc, DateTimeKind.Utc);
            if (!excludeProperties)
            {
                model.Title = agentReview.Title;
                if (formatReviewAndReplyText)
                {
                    model.ReviewText = Core.Html.HtmlHelper.FormatText(agentReview.ReviewText, false, true, false, false, false, false);
                    model.ReplyText = Core.Html.HtmlHelper.FormatText(agentReview.ReplyText, false, true, false, false, false, false);
                }
                else
                {
                    model.ReviewText = agentReview.ReviewText;
                    model.ReplyText = agentReview.ReplyText;
                }
                model.IsApproved = agentReview.IsApproved;
            }

            //a vendor should have access only to his products
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

            //prepare feedback model
            PrepareAgentFeedbackModel(model);
        }

        /// <summary>
        /// Prepare feddback model
        /// </summary>
        /// <param name="model">Item rating model</param>
        protected virtual void PrepareItemFeedbackModel(ItemRatingModel model)
        {
            model.FeedbackModel = new FeedbackModel();

            //add additional note
            model.FeedbackModel.FeedBackList.Add(new Feedback
            {
                Name = _localizationService.GetResource("Admin.Configuration.Settings.RatingReviewSettings.AdditionalNotes"),
                Value = model.ReviewText
            });
        }

        /// <summary>
        /// Prepare feddback model
        /// </summary>
        /// <param name="model">Merchant rating model</param>
        protected virtual void PrepareMerchantFeedbackModel(MerchantRatingModel model)
        {
            //liked review option list
            var reviewOptionids = StringToIntList(model.ReviewOptions);

            //disliked review option list
            var disLikedreviewOptionids = StringToIntList(model.DislikedReviewOptions);

            var listOfIds = new List<int>();

            if (reviewOptionids != null)
                listOfIds.AddRange(reviewOptionids);
            if (disLikedreviewOptionids != null)
                listOfIds.AddRange(disLikedreviewOptionids);

            model.FeedbackModel = new FeedbackModel();
            foreach (var id in listOfIds)
            {
                var reviewOption = _reviewOptionService.GetReviewOptionById(id);
                if (reviewOption != null)
                {
                    model.FeedbackModel.FeedBackList.Add(new Feedback
                    {
                        Id = reviewOption.Id,
                        Name = _localizationService.GetLocalized(reviewOption, entity => entity.Name, _workContext.WorkingCurrency.Id, false, false) ?? reviewOption.Name,
                        Value = reviewOptionids.Contains(id) ? _localizationService.GetResource("Admin.RatingReview.Common.Feedback.Liked") : _localizationService.GetResource("Admin.RatingReview.Common.Feedback.Disliked")
                    });
                }
            }

            //add additional note
            model.FeedbackModel.FeedBackList.Add(new Feedback
            {
                //Id = listOfIds?.Max() + 1 ?? 1,
                Name = _localizationService.GetResource("Admin.Configuration.Settings.RatingReviewSettings.AdditionalNotes"),
                Value = model.ReviewText
            });

            model.FeedbackModel.FeedBackList = model.FeedbackModel.FeedBackList.OrderBy(x => x.Id).ToList();
        }

        /// <summary>
        /// Prepare feddback model
        /// </summary>
        /// <param name="model">Agent rating model</param>
        protected virtual void PrepareAgentFeedbackModel(AgentRatingModel model)
        {
            //liked review option list
            var reviewOptionids = StringToIntList(model.ReviewOptions);

            //disliked review option list
            var disLikedreviewOptionids = StringToIntList(model.DislikedReviewOptions);

            var listOfIds = new List<int>();

            if (reviewOptionids != null)
                listOfIds.AddRange(reviewOptionids);
            if (disLikedreviewOptionids != null)
                listOfIds.AddRange(disLikedreviewOptionids);

            model.FeedbackModel = new FeedbackModel();
            foreach (var id in listOfIds)
            {
                var reviewOption = _reviewOptionService.GetReviewOptionById(id);
                if (reviewOption != null)
                {
                    model.FeedbackModel.FeedBackList.Add(new Feedback
                    {
                        Id = reviewOption.Id,
                        Name = _localizationService.GetLocalized(reviewOption, entity => entity.Name, _workContext.WorkingCurrency.Id, false, false) ?? reviewOption.Name,
                        Value = reviewOptionids.Contains(id) ? _localizationService.GetResource("Admin.RatingReview.Common.Feedback.Liked") : _localizationService.GetResource("Admin.RatingReview.Common.Feedback.Disliked")
                    });
                }
            }

            //add additional note
            model.FeedbackModel.FeedBackList.Add(new Feedback
            {
                //Id = listOfIds?.Max() + 1 ?? 1,
                Name = _localizationService.GetResource("Admin.Configuration.Settings.RatingReviewSettings.AdditionalNotes"),
                Value = model.ReviewText
            });

            model.FeedbackModel.FeedBackList = model.FeedbackModel.FeedBackList.OrderBy(x => x.Id).ToList();
        }

        /// <summary>
        /// string to int list
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static IEnumerable<int> StringToIntList(string str)
        {
            if (String.IsNullOrEmpty(str))
                yield break;

            foreach (var s in str.Split(','))
            {
                int num;
                if (int.TryParse(s, out num))
                    yield return num;
            }
        }

        #endregion

        #region Custom Code By Deeksha

        #region Item Code by Deeksha

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult ItemReviewList()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageItemReviews))
                return AccessDeniedView();

            var model = new ItemRatingListSearchModel();

            //prepare available stores
            _baseAdminModelFactory.PrepareStores(model.AvailableStores);

            //prepare vendors
            _baseAdminModelFactory.PrepareVendors(model.AvailableVendors);

            model.HideStoresList = _catalogSettings.IgnoreStoreLimitations || model.AvailableStores.SelectionIsNotPossible();

            //"approved" property
            //0 - all
            //1 - approved only
            //2 - disapproved only
            model.AvailableApprovedOptions.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.RatingReview.ItemReviews.List.SearchApproved.All"), Value = "0" });
            model.AvailableApprovedOptions.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.RatingReview.ItemReviews.List.SearchApproved.ApprovedOnly"), Value = "1" });
            model.AvailableApprovedOptions.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.RatingReview.ItemReviews.List.SearchApproved.DisapprovedOnly"), Value = "2" });
            model.SetGridPageSize();
            return View(model);
        }

        //list
        [HttpPost]
        public virtual IActionResult ItemReviewList(ItemRatingListSearchModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageItemReviews))
                return AccessDeniedDataTablesJson();

            //a vendor should have access only to his products
            var vendorId = 0;
            if (_workContext.CurrentVendor != null)
                vendorId = _workContext.CurrentVendor.Id;

            var createdOnFromValue = (model.CreatedOnFrom == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CreatedOnFrom.Value, _dateTimeHelper.CurrentTimeZone);

            var createdToFromValue = (model.CreatedOnTo == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CreatedOnTo.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            bool? approved = null;
            if (model.SearchApprovedId > 0)
                approved = model.SearchApprovedId == 1;

            var ReviewType = (int)Nop.Core.Domain.RatingReview.ReviewType.Item;

            var subAdminAccosiatedvendorIds = new List<int>();
            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                if (model.SearchStoreId == 0)
                {
                    model.SearchStoreId = _workContext.GetCurrentStoreId;
                }
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
            {
                if (model.SearchVendorId == 0)
                {
                    model.SearchStoreId = _workContext.GetCurrentStoreId;
                    model.SearchVendorId = _workContext.CurrentMerchant.Id;
                }
            }
            //a subadmin should have access only to his accosiated vendors products
            else if (_workContext.SubAdmin.IsSubAdminRole)
            {
                model.SearchStoreId = _workContext.GetCurrentStoreId;
                subAdminAccosiatedvendorIds = _workContext.SubAdmin.AssociatedVenorIds?.ToList();
            }

            var itemReviews = _ratingReviewService.GetAllRatingReviewsWithPaging(0,
                approved,
                createdOnFromValue,
                createdToFromValue,
                model.SearchText,
                ReviewType,
                model.SearchStoreId,
                model.SearchItemId,
                model.SearchVendorId,
                vendorId,
                model.Page - 1,
                model.PageSize,
                subAdminAccosiatedvendorIds);
            var modelList = new ItemRatingListModel().PrepareToGrid(model, itemReviews, () =>
            {
                //fill in model values from the entity
                return itemReviews.Select(x =>
                {
                    var m = new ItemRatingModel();
                    PrepareItemReviewModel(m, x, false, true);
                    return m;
                });
            });
            return Json(modelList);
        }

        public virtual IActionResult ProductSearchAutoComplete(string term)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageItemReviews))
                return Content("");

            const int searchTermMinimumLength = 3;
            if (string.IsNullOrWhiteSpace(term) || term.Length < searchTermMinimumLength)
                return Content("");

            //a vendor should have access only to his products
            var vendorId = 0;
            var subAdminAccosiatedvendorIds = new List<int>();
            if (_workContext.CurrentVendor != null)
            {
                vendorId = _workContext.CurrentVendor.Id;
            }
            //a subadmin should have access only to his accosiated vendors products
            else if (_workContext.SubAdmin.IsSubAdminRole)
            {
                subAdminAccosiatedvendorIds = _workContext.SubAdmin.AssociatedVenorIds?.ToList();
            }

            //products
            const int productNumber = 15;
            var products = _productService.SearchProducts(
                keywords: term,
                vendorId: vendorId,
                vendorIds: subAdminAccosiatedvendorIds,
                pageSize: productNumber,
                showHidden: true);

            var result = (from p in products
                          select new
                          {
                              label = p.Name,
                              productid = p.Id
                          })
                .ToList();
            return Json(result);
        }


        //edit
        public virtual IActionResult ItemReviewEdit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageItemReviews))
                return AccessDeniedView();

            var itemReview = _ratingReviewService.GetRatingReviewsById(id);
            if (itemReview == null)
                //No product review found with the specified id
                return RedirectToAction("ItemReviewList");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && itemReview.Store.Id != _workContext.CurrentVendor.Id)
                return RedirectToAction("ItemReviewList");

            var model = new ItemRatingModel();
            PrepareItemReviewModel(model, itemReview, false, false);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult ItemReviewEdit(ItemRatingModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageItemReviews))
                return AccessDeniedView();

            var itemReview = _ratingReviewService.GetRatingReviewsById(model.Id);

            if (itemReview == null)
                //No product found with the specified id
                return RedirectToAction("ItemReviewList");

            if (ModelState.IsValid)
            {
                var isLoggedInAsVendor = _workContext.CurrentVendor != null;

                //var previousIsApproved = itemReview.;
                //vendor can edit "Reply text" only
                if (!isLoggedInAsVendor)
                {
                    itemReview.Title = model.Title;
                    itemReview.ReviewText = model.ReviewText;
                    itemReview.IsApproved = model.IsApproved;
                }

                itemReview.ReplyText = model.ReplyText;

                //activity log
                // _customerActivityService.InsertActivity("ItemReviewEdit", _localizationService.GetResource("ActivityLog.ItemReviewEdit"), itemReview.Id);

                //vendor can edit "Reply text" only
                //if (!isLoggedInAsVendor)
                //{
                //    //update product totals
                //    _productService.UpdateProductReviewTotals(itemReview.Product);

                //    //raise event (only if it wasn't approved before and is approved now)
                //    if (!previousIsApproved && itemReview.IsApproved)
                //        _eventPublisher.Publish(new ItemReviewApprovedEvent(itemReview));

                //}

                _ratingReviewService.UpdateRatingReviews(itemReview);
                //activity log
                //_customerActivityService.InsertActivity("Edit", _localizationService.GetResource("ActivityLog.EditProduct"), itemReview.Id);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.RatingReview.ItemRating.Updated"));

                return continueEditing ? RedirectToAction("ItemReviewEdit", new { id = itemReview.Id }) : RedirectToAction("ItemReviewList");
            }
            //If we got this far, something failed, redisplay form
            PrepareItemReviewModel(model, itemReview, false, false);
            return View(model);
        }



        [HttpPost]
        public virtual IActionResult ApproveSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageItemReviews))
                return AccessDeniedView();

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("ItemReviewList");

            if (selectedIds != null)
            {
                //filter not approved reviews
                var ratingReviews = _ratingReviewService.GetRatingReviewsByIds(selectedIds.ToArray()).Where(review => !review.IsApproved);
                foreach (var itemReview in ratingReviews)
                {
                    itemReview.IsApproved = true;
                    _ratingReviewService.UpdateRatingReviews(itemReview);

                    //update product totals
                    //_productService.UpdateProductReviewTotals(itemReview.Product);

                    //raise event 
                    //_eventPublisher.Publish(new ItemReviewApprovedEvent(itemReview));
                }
            }

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult DisapproveSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageItemReviews))
                return AccessDeniedView();

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("ItemReviewList");

            if (selectedIds != null)
            {
                //filter approved reviews
                var itemReviews = _ratingReviewService.GetRatingReviewsByIds(selectedIds.ToArray()).Where(review => review.IsApproved);
                foreach (var itemReview in itemReviews)
                {
                    itemReview.IsApproved = false;
                    _ratingReviewService.UpdateRatingReviews(itemReview);

                    //update product totals
                    //_productService.UpdateProductReviewTotals(itemReview.Product);
                }
            }

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult DeleteSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageItemReviews))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                _ratingReviewService.DeleteRatingReviews(_ratingReviewService.GetRatingReviewsByIds(selectedIds.ToArray()).Where(p => _workContext.CurrentVendor == null || p.EntityId == _workContext.CurrentVendor.Id).ToList());
            }

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageItemReviews))
                return AccessDeniedView();

            var merchantReview = _ratingReviewService.GetRatingReviewsById(id);
            if (merchantReview == null)
                //No product found with the specified id
                return RedirectToAction("ItemReviewList");

            //a vendor should have access only to his products


            _ratingReviewService.DeleteRatingReviews(merchantReview);

            //activity log
            _customerActivityService.InsertActivity("DeleteProduct", _localizationService.GetResource("ActivityLog.DeleteProduct"), merchantReview);

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.RatingReview.ItemRating.Deleted"));
            return RedirectToAction("ItemReviewList");
        }

        #endregion

        #region Merchant Code by Deeksha

        public virtual IActionResult MerchantList()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchant))
                return AccessDeniedView();

            var model = new MerchantRatingListSearchModel();

            //prepare available stores
            _baseAdminModelFactory.PrepareStores(model.AvailableStores);

            //prepare vendors
            _baseAdminModelFactory.PrepareVendors(model.AvailableVendors);

            model.HideStoresList = _catalogSettings.IgnoreStoreLimitations || model.AvailableStores.SelectionIsNotPossible();

            //"approved" property
            //0 - all
            //1 - approved only
            //2 - disapproved only
            model.AvailableApprovedOptions.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.RatingReview.ItemReviews.List.SearchApproved.All"), Value = "0" });
            model.AvailableApprovedOptions.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.RatingReview.ItemReviews.List.SearchApproved.ApprovedOnly"), Value = "1" });
            model.AvailableApprovedOptions.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.RatingReview.ItemReviews.List.SearchApproved.DisapprovedOnly"), Value = "2" });

            model.SetGridPageSize();

            return View(model);
        }

        //list
        [HttpPost]
        public virtual IActionResult MerchantList(MerchantRatingListSearchModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchant))
                return AccessDeniedDataTablesJson();

            //a vendor should have access only to his products
            var vendorId = 0;
            if (_workContext.CurrentVendor != null)
            {
                vendorId = _workContext.CurrentVendor.Id;
            }

            var createdOnFromValue = (model.CreatedOnFrom == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CreatedOnFrom.Value, _dateTimeHelper.CurrentTimeZone);

            var createdToFromValue = (model.CreatedOnTo == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CreatedOnTo.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            var subAdminAccosiatedvendorIds = new List<int>();
            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                if (model.SearchStoreId == 0)
                {
                    model.SearchStoreId = _workContext.GetCurrentStoreId;
                }
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
            {
                if (model.SearchVendorId == 0)
                {
                    model.SearchStoreId = _workContext.GetCurrentStoreId;
                    model.SearchVendorId = _workContext.CurrentMerchant.Id;
                }
            }
            //a subadmin should have access only to his accosiated vendors products
            else if (_workContext.SubAdmin.IsSubAdminRole)
            {
                model.SearchStoreId = _workContext.GetCurrentStoreId;
                subAdminAccosiatedvendorIds = _workContext.SubAdmin.AssociatedVenorIds?.ToList();
            }

            bool? approved = null;
            if (model.SearchApprovedId > 0)
                approved = model.SearchApprovedId == 1;
            var ReviewType = (int)Nop.Core.Domain.RatingReview.ReviewType.Merchant;
            var merchantReviews = _ratingReviewService.GetAllRatingReviewsWithPaging(0,
                approved,
                createdOnFromValue,
                createdToFromValue,
                model.SearchText,
                ReviewType,
                model.SearchStoreId,
                model.SearchMerchantId,
                model.SearchVendorId,
                vendorId,
                model.Page - 1,
                model.PageSize,
                subAdminAccosiatedvendorIds);

            var modelList = new MerchantRatingListModel().PrepareToGrid(model, merchantReviews, () =>
            {
                //fill in model values from the entity
                return merchantReviews.Select(x =>
                {
                    var m = new MerchantRatingModel();
                    PrepareMerchantReviewModel(m, x, false, true);
                    return m;
                });
            });

            return Json(modelList);
        }

        public virtual IActionResult MerchantSearchAutoComplete(string term)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchant))
                return Content("");

            const int searchTermMinimumLength = 3;
            if (string.IsNullOrWhiteSpace(term) || term.Length < searchTermMinimumLength)
                return Content("");

            //a vendor should have access only to his products
            var vendorId = 0;
            if (_workContext.CurrentVendor != null)
            {
                vendorId = _workContext.CurrentVendor.Id;
            }

            //products
            const int merchantNumber = 15;
            var merchants = _vendorService.GetAllVendors(
                name: term,
                pageSize: merchantNumber);

            var result = (from p in merchants
                          select new
                          {
                              label = p.Name,
                              productid = p.Id
                          })
                .ToList();
            return Json(result);
        }

        //edit
        public virtual IActionResult MerchantEdit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchant))
                return AccessDeniedView();

            var merchantReview = _ratingReviewService.GetRatingReviewsById(id);
            if (merchantReview == null)
                //No product review found with the specified id
                return RedirectToAction("MerchantList");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && merchantReview.Store.Id != _workContext.CurrentVendor.Id)
                return RedirectToAction("MerchantList");

            var model = new MerchantRatingModel();
            PrepareMerchantReviewModel(model, merchantReview, false, false);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult MerchantEdit(MerchantRatingModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchant))
                return AccessDeniedView();

            var merchantReview = _ratingReviewService.GetRatingReviewsById(model.Id);

            if (merchantReview == null)
                //No product found with the specified id
                return RedirectToAction("MerchantList");

            //a vendor should have access only to his products

            //check if the product quantity has been changed while we were editing the product
            //and if it has been changed then we show error notification
            //and redirect on the editing page without data saving

            if (ModelState.IsValid)
            {
                var isLoggedInAsVendor = _workContext.CurrentVendor != null;

                //var previousIsApproved = itemReview.;
                //vendor can edit "Reply text" only
                if (!isLoggedInAsVendor)
                {
                    merchantReview.Title = model.Title;
                    merchantReview.ReviewText = model.ReviewText;
                    merchantReview.IsApproved = model.IsApproved;
                }

                merchantReview.ReplyText = model.ReplyText;
                _ratingReviewService.UpdateRatingReviews(merchantReview);

                //activity log
                _customerActivityService.InsertActivity("MerchantEdit", _localizationService.GetResource("ActivityLog.MerchantEdit"), merchantReview);

                //vendor can edit "Reply text" only
                //if (!isLoggedInAsVendor)
                //{
                //    //update product totals
                //    _productService.UpdateProductReviewTotals(itemReview.Product);

                //    //raise event (only if it wasn't approved before and is approved now)
                //    if (!previousIsApproved && itemReview.IsApproved)
                //        _eventPublisher.Publish(new ItemReviewApprovedEvent(itemReview));

                //}
                //a vendor should have access only to his products

                //we do not validate maximum number of products per vendor when editing existing products (only during creation of new products)

                //vendors cannot edit "Show on home page" property

                //product

                _ratingReviewService.UpdateRatingReviews(merchantReview);
                //activity log
                //_customerActivityService.InsertActivity("EditProduct", _localizationService.GetResource("ActivityLog.EditProduct"), merchantReview.Id);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.RatingReview.MerchantRating.Updated"));
                return continueEditing ? RedirectToAction("MerchantEdit", new { id = merchantReview.Id }) : RedirectToAction("MerchantList");
            }
            //If we got this far, something failed, redisplay form
            PrepareMerchantReviewModel(model, merchantReview, false, false);
            return View(model);
        }


        [HttpPost]
        public virtual IActionResult MerchantApproveSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchant))
                return AccessDeniedView();

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("MerchantList");

            if (selectedIds != null)
            {
                //filter not approved reviews
                var ratingReviews = _ratingReviewService.GetRatingReviewsByIds(selectedIds.ToArray()).Where(review => !review.IsApproved);
                foreach (var merchantReview in ratingReviews)
                {
                    merchantReview.IsApproved = true;
                    _ratingReviewService.UpdateRatingReviews(merchantReview);

                    //update product totals
                    //_ratingReviewService.UpdateProductReviewTotals(itemReview.Product);

                    //raise event 
                    //_eventPublisher.Publish(new ItemReviewApprovedEvent(merchantReview));
                }
            }

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult MerchantDisapproveSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchant))
                return AccessDeniedView();

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("MerchantList");

            if (selectedIds != null)
            {
                //filter approved reviews
                var merchantReviews = _ratingReviewService.GetRatingReviewsByIds(selectedIds.ToArray()).Where(review => review.IsApproved);
                foreach (var merchantReview in merchantReviews)
                {
                    merchantReview.IsApproved = false;
                    _ratingReviewService.UpdateRatingReviews(merchantReview);

                    //update product totals
                    //_productService.UpdateProductReviewTotals(itemReview.Product);
                }
            }

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult MerchantDeleteSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchant))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                _ratingReviewService.DeleteRatingReviews(_ratingReviewService.GetRatingReviewsByIds(selectedIds.ToArray()).Where(p => _workContext.CurrentVendor == null || p.EntityId == _workContext.CurrentVendor.Id).ToList());
            }

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult MerchantDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchant))
                return AccessDeniedView();

            var merchantReview = _ratingReviewService.GetRatingReviewsById(id);
            if (merchantReview == null)
                //No product found with the specified id
                return RedirectToAction("MerchantList");

            //a vendor should have access only to his products


            _ratingReviewService.DeleteRatingReviews(merchantReview);

            //activity log
            _customerActivityService.InsertActivity("DeleteProduct", _localizationService.GetResource("ActivityLog.DeleteProduct"), merchantReview);

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.RatingReview.MerchantRating.Deleted"));
            return RedirectToAction("MerchantList");
        }



        #endregion

        #region Agent Code by Deeksha

        public virtual IActionResult AgentList()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgent))
                return AccessDeniedView();

            var model = new AgentRatingListSearchModel();

            //prepare available stores
            _baseAdminModelFactory.PrepareStores(model.AvailableStores);

            //prepare vendors
            _baseAdminModelFactory.PrepareVendors(model.AvailableVendors);

            model.HideStoresList = _catalogSettings.IgnoreStoreLimitations || model.AvailableStores.SelectionIsNotPossible();

            //"approved" property
            //0 - all
            //1 - approved only
            //2 - disapproved only
            model.AvailableApprovedOptions.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.RatingReview.ItemReviews.List.SearchApproved.All"), Value = "0" });
            model.AvailableApprovedOptions.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.RatingReview.ItemReviews.List.SearchApproved.ApprovedOnly"), Value = "1" });
            model.AvailableApprovedOptions.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.RatingReview.ItemReviews.List.SearchApproved.DisapprovedOnly"), Value = "2" });
            model.SetGridPageSize();

            return View(model);
        }

        //list
        [HttpPost]
        public virtual IActionResult AgentList(AgentRatingListSearchModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgent))
                return AccessDeniedDataTablesJson();

            //a vendor should have access only to his products
            var vendorId = 0;
            if (_workContext.CurrentVendor != null)
            {
                vendorId = _workContext.CurrentVendor.Id;
            }

            var createdOnFromValue = (model.CreatedOnFrom == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CreatedOnFrom.Value, _dateTimeHelper.CurrentTimeZone);

            var createdToFromValue = (model.CreatedOnTo == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CreatedOnTo.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            var subAdminAccosiatedvendorIds = new List<int>();
            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                if (model.SearchStoreId == 0)
                {
                    model.SearchStoreId = _workContext.GetCurrentStoreId;
                }
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
            {
                if (model.SearchVendorId == 0)
                {
                    model.SearchStoreId = _workContext.GetCurrentStoreId;
                    model.SearchVendorId = _workContext.CurrentMerchant.Id;
                }
            }
            //a subadmin should have access only to his accosiated vendors products
            else if (_workContext.SubAdmin.IsSubAdminRole)
            {
                model.SearchStoreId = _workContext.GetCurrentStoreId;
                subAdminAccosiatedvendorIds = _workContext.SubAdmin.AssociatedVenorIds?.ToList();
            }

            bool? approved = null;
            if (model.SearchApprovedId > 0)
                approved = model.SearchApprovedId == 1;

            var ReviewType = (int)Nop.Core.Domain.RatingReview.ReviewType.Agent;



            var agentReviews = _ratingReviewService.GetAllRatingReviewsWithPaging(0,
                approved,
                createdOnFromValue,
                createdToFromValue,
                model.SearchText,
                ReviewType,
                model.SearchStoreId,
                model.SearchAgentId,
                model.SearchVendorId,
                vendorId,
                model.Page - 1,
                model.PageSize,
                subAdminAccosiatedvendorIds);
            var modelList = new AgentRatingListModel().PrepareToGrid(model, agentReviews, () =>
            {
                //fill in model values from the entity
                return agentReviews.Select(x =>
                {
                    var m = new AgentRatingModel();
                    PrepareAgentReviewModel(m, x, false, true);
                    return m;
                });
            });

            return Json(modelList);
        }

        public virtual IActionResult AgentSearchAutoComplete(string term)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgent))
                return Content("");

            const int searchTermMinimumLength = 3;
            if (string.IsNullOrWhiteSpace(term) || term.Length < searchTermMinimumLength)
                return Content("");

            //a vendor should have access only to his products
            var vendorId = 0;
            if (_workContext.CurrentVendor != null)
            {
                vendorId = _workContext.CurrentVendor.Id;
            }

            //products
            const int merchantNumber = 15;
            List<int> customerRoleIds = new List<int>();
            var custRoles = _customerService.GetAllCustomerRoles();
            if (custRoles.Count > 0)
            {
                var agentRole = custRoles.FirstOrDefault(x => x.Name == "Agents");
                if (agentRole != null)
                    customerRoleIds.Add(agentRole.Id);
            }

            var merchants = _customerService.GetAllCustomers(
                firstName: term,
                vendorId: vendorId,
                pageSize: merchantNumber,
                customerRoleIds: customerRoleIds.ToArray());

            var result = (from p in merchants
                          select new
                          {
                              label = p.GetFullName(),
                              productid = p.Id
                          })
                .ToList();
            return Json(result);
        }


        //edit
        public virtual IActionResult AgentEdit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgent))
                return AccessDeniedView();

            var agentReview = _ratingReviewService.GetRatingReviewsById(id);
            if (agentReview == null)
                //No product review found with the specified id
                return RedirectToAction("AgentList");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && agentReview.Store.Id != _workContext.CurrentVendor.Id)
                return RedirectToAction("AgentList");

            var model = new AgentRatingModel();
            PrepareAgentReviewModel(model, agentReview, false, false);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult AgentEdit(AgentRatingModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgent))
                return AccessDeniedView();

            var agentReview = _ratingReviewService.GetRatingReviewsById(model.Id);

            if (agentReview == null)
                //No product found with the specified id
                return RedirectToAction("AgentList");

            //a vendor should have access only to his products

            //check if the product quantity has been changed while we were editing the product
            //and if it has been changed then we show error notification
            //and redirect on the editing page without data saving

            if (ModelState.IsValid)
            {
                var isLoggedInAsVendor = _workContext.CurrentVendor != null;

                //var previousIsApproved = itemReview.;
                //vendor can edit "Reply text" only
                if (!isLoggedInAsVendor)
                {
                    agentReview.Title = model.Title;
                    agentReview.ReviewText = model.ReviewText;
                    agentReview.IsApproved = model.IsApproved;
                }

                agentReview.ReplyText = model.ReplyText;
                _ratingReviewService.UpdateRatingReviews(agentReview);

                //activity log
                _customerActivityService.InsertActivity("AgentEdit", _localizationService.GetResource("ActivityLog.AgentEdit"), agentReview);

                //vendor can edit "Reply text" only
                //if (!isLoggedInAsVendor)
                //{
                //    //update product totals
                //    _productService.UpdateProductReviewTotals(itemReview.Product);

                //    //raise event (only if it wasn't approved before and is approved now)
                //    if (!previousIsApproved && itemReview.IsApproved)
                //        _eventPublisher.Publish(new ItemReviewApprovedEvent(itemReview));

                //}
                //a vendor should have access only to his products

                //we do not validate maximum number of products per vendor when editing existing products (only during creation of new products)

                //vendors cannot edit "Show on home page" property

                //product

                _ratingReviewService.UpdateRatingReviews(agentReview);
                //activity log
                //_customerActivityService.InsertActivity("EditProduct", _localizationService.GetResource("ActivityLog.EditProduct"), agentReview.Id);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.RatingReview.AgentRating.Updated"));
                return continueEditing ? RedirectToAction("AgentEdit", new { id = agentReview.Id }) : RedirectToAction("AgentList");
            }
            //If we got this far, something failed, redisplay form
            PrepareAgentReviewModel(model, agentReview, false, false);
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult AgentApproveSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgent))
                return AccessDeniedView();

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("AgentList");

            if (selectedIds != null)
            {
                //filter not approved reviews
                var ratingReviews = _ratingReviewService.GetRatingReviewsByIds(selectedIds.ToArray()).Where(review => !review.IsApproved);
                foreach (var agentReview in ratingReviews)
                {
                    agentReview.IsApproved = true;
                    _ratingReviewService.UpdateRatingReviews(agentReview);

                    //update product totals
                    //_productService.UpdateProductReviewTotals(itemReview.Product);

                    //raise event 
                    //_eventPublisher.Publish(new ItemReviewApprovedEvent(itemReview));
                }
            }

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult AgentDisapproveSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgent))
                return AccessDeniedView();

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("AgentList");

            if (selectedIds != null)
            {
                //filter approved reviews
                var agentReviews = _ratingReviewService.GetRatingReviewsByIds(selectedIds.ToArray()).Where(review => review.IsApproved);
                foreach (var agentReview in agentReviews)
                {
                    agentReview.IsApproved = false;
                    _ratingReviewService.UpdateRatingReviews(agentReview);

                    //update product totals
                    //_productService.UpdateProductReviewTotals(itemReview.Product);
                }
            }

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult AgentDeleteSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgent))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                _ratingReviewService.DeleteRatingReviews(_ratingReviewService.GetRatingReviewsByIds(selectedIds.ToArray()).Where(p => _workContext.CurrentVendor == null || p.EntityId == _workContext.CurrentVendor.Id).ToList());
            }

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult AgentDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgent))
                return AccessDeniedView();

            var merchantReview = _ratingReviewService.GetRatingReviewsById(id);
            if (merchantReview == null)
                //No product found with the specified id
                return RedirectToAction("AgentList");

            //a vendor should have access only to his products


            _ratingReviewService.DeleteRatingReviews(merchantReview);

            //activity log
            _customerActivityService.InsertActivity("DeleteProduct", _localizationService.GetResource("ActivityLog.DeleteProduct"), merchantReview);

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.RatingReview.AgentRating.Deleted"));
            return RedirectToAction("AgentList");
        }

        #endregion

        public virtual IActionResult GetVendorByStoreId(string storeId, string vendorId)
        {
            var allVendors = _vendorService.GetAllVendors();
            int.TryParse(storeId, out int storeIdentifier);
            if (storeIdentifier > 0)
            {
                var vendors = allVendors.Where(x => x.StoreId == storeIdentifier).ToList();
                var result = (from v in vendors
                              select new { Id = v.Id, Name = v.Name }).ToList();
                result.Insert(0, new { Id = 0, Name = _localizationService.GetResource("Admin.Common.All") });
                return Json(result);
            }

            var resultAll = (from v in allVendors
                             select new { Id = v.Id, Name = v.Name }).ToList();
            resultAll.Insert(0, new { Id = 0, Name = _localizationService.GetResource("Admin.Common.All") });
            return Json(resultAll);
        }

        #endregion

    }
}