﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Common
{
    public partial class TipModel : BaseNopModel
    {
        public TipModel()
        {
            Tips = new List<TipsModel>();
        }

        public decimal SelectedTipAmountValue { get; set; }
        public IList<TipsModel> Tips { get; set; }

        #region Nested classes

        public class TipsModel : BaseNopEntityModel
        {
            public decimal TipAmountValue { get; set; }
            public string TipAmount { get; set; }
        }
        
        #endregion
    }
}