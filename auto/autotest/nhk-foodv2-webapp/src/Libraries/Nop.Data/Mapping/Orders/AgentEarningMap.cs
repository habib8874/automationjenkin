﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Orders;

namespace Nop.Data.Mapping.Orders
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class AgentEarningMap : NopEntityTypeConfiguration<AgentEarning>
    {
        public override void Configure(EntityTypeBuilder<AgentEarning> builder)
        {
            builder.ToTable(NopMappingDefaults.AgentEarningTable);
            builder.HasKey(sci => sci.Id);

            base.Configure(builder);
        }
    }
}