﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nestorbird.Plugin.Api.Factories;
using Nestorbird.Plugin.Api.RequestModel.Orders;
using Nestorbird.Plugin.Api.ResponseModel;
using Nestorbird.Plugin.Api.ResponseModel.Account;
using Nestorbird.Plugin.Api.ResponseModel.Order;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Vendors;

namespace Nestorbird.Plugin.Api.Controllers
{
    [Route("merchantapi/order")]
    public class MerchantOrderController : BaseAPIController
    {
        #region Fields

        private readonly IOrderService _orderService;
        private readonly ICustomerService _customerService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly ICurrencyService _currencyService;
        private readonly ILocalizationService _localizationService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPaymentPluginManager _paymentPluginManager;
        private readonly IPdfService _pdfService;
        private readonly ICityService _cityService;
        private readonly IPictureService _pictureService;
        private readonly IVendorService _vendorService;
        private readonly IOrderModelFactory _orderModelFactory;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public MerchantOrderController(IOrderService orderService,
            ICustomerService customerService,
            IPriceFormatter priceFormatter,
            ICurrencyService currencyService,
            ILanguageService languageService,
            ILocalizationService localizationService,
            IDateTimeHelper dateTimeHelper,
            IPaymentPluginManager paymentPluginManager,
            IPdfService pdfService,
            ICityService cityService,
            IPictureService pictureService,
            IVendorService vendorService,
            IOrderModelFactory orderModelFactory,
            IWorkContext workContext) : base(languageService)
        {
            _orderService = orderService;
            _customerService = customerService;
            _priceFormatter = priceFormatter;
            _currencyService = currencyService;
            _localizationService = localizationService;
            _dateTimeHelper = dateTimeHelper;
            _paymentPluginManager = paymentPluginManager;
            _pdfService = pdfService;
            _cityService = cityService;
            _pictureService = pictureService;
            _vendorService = vendorService;
            _orderModelFactory = orderModelFactory;
            _workContext = workContext;
        }

        #endregion

        #region Methods

        [HttpPost]
        [Route("getorders")]
        [ProducesResponseType(typeof(OrderListResponse), StatusCodes.Status200OK)]
        public IActionResult GetOrders(OrderListRequest request)
        {
            if (request == null)
                return Error("Please provide proper request");

            if (request.VendorId <= 0)
                return Error("Please provide vendor id");

            if (request.PageSize <= 0)
                request.PageSize = int.MaxValue;

            var response = _orderModelFactory.GetOrders(request, CurrentLanguage);
            return Success(response);
        }

        [HttpGet]
        [Route("getordersnotes")]
        [ProducesResponseType(typeof(List<OrderNoteResponse>), StatusCodes.Status200OK)]
        public IActionResult GetOrderNotes(int orderId)
        {
            if (orderId <= 0)
                return Error("Please provide order id");

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                return Error("Order is not exist");

            var response = _orderModelFactory.GetOrderNotes(order);
            return Success(response);
        }

        [HttpGet]
        [Route("getorderdetails")]
        [ProducesResponseType(typeof(OrderDetailResponse), StatusCodes.Status200OK)]
        public IActionResult GetOrderDetails(int orderId)
        {
            if (orderId <= 0)
                return Error("Please provide order id");

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                return Error("Order is not exist");

            var response = _orderModelFactory.GetOrderDetails(order, CurrentLanguage);
            return Success(response);
        }

        [HttpGet]
        [Route("getorderstatus")]
        [ProducesResponseType(typeof(ListItemResponse), StatusCodes.Status200OK)]
        public IActionResult GetOrderStatus()
        {
            var orderStatuses = Enum.GetValues(typeof(OrderStatus)).Cast<OrderStatus>().Select(x =>
            {
                return new ListItemResponse
                {
                    Text = _localizationService.GetLocalizedEnum(x, CurrentLanguageId),
                    Value = ((int)x).ToString()
                };
            });

            return Success(orderStatuses);
        }

        [HttpPost]
        [Route("changeorderstatus")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        public IActionResult ChangeOrderStatus(int orderId, int statusId)
        {
            if (orderId <= 0)
                return Error("Please enter order id");

            if (statusId <= 0)
                return Error("Please enter status id");

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                return Error("Please enter valid order id");

            var orderStatus = (OrderStatus)statusId;
            order.OrderStatusId = statusId;

            //add a note
            order.OrderNotes.Add(new OrderNote
            {
                Note = $"Order status has been edited. New status: {_localizationService.GetLocalizedEnum(orderStatus)}",
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow,
                OrderStatus = statusId
            });
            _orderService.UpdateOrder(order);

            //send signalR notification
            _orderService.SendOrderStatusSignalR(order);

            //send notification to customer
            if (order.StoreId == _workContext.GetCurrentStoreId)
                _orderService.SendNotificationtoCustomer(order);

            //send sms to customer
            _orderService.SendMessage(order, statusId);

            return Success("", "Order status changed successfully");
        }

        [HttpGet]
        [Route("getpdfinvoice")]
        [ProducesResponseType(typeof(FileContentResult), StatusCodes.Status200OK)]
        public virtual IActionResult GetPdfInvoice(int orderId)
        {
            if (orderId <= 0)
                return Error("Please enter order id");

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                return Error("Please enter valid order id");

            var orders = new List<Order>
            {
                order
            };

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintOrdersToPdf(stream, orders, CurrentLanguageId);
                bytes = stream.ToArray();
            }
            return File(bytes, MimeTypes.ApplicationPdf, $"order_{order.Id}.pdf");
        }

        [HttpGet]
        [Route("getagents")]
        [ProducesResponseType(typeof(ListItemResponse), StatusCodes.Status200OK)]
        public virtual IActionResult GetAgents(int cityId = 0)
        {
            var unassignedAgentsList = _customerService.GetAllUnassignedAgentsList(0, cityId);
            var agents = new List<ListItemResponse>();
            foreach (var customer in unassignedAgentsList)
            {
                agents.Add(new ListItemResponse
                {
                    Text = customer.GetFullName(),
                    Value = customer.Id.ToString(),
                });
            }

            return Success(agents);
        }

        [HttpGet]
        [Route("getagentstatus")]
        [ProducesResponseType(typeof(ListItemResponse), StatusCodes.Status200OK)]
        public virtual IActionResult GetAgentStatus()
        {
            var agentStatuses = Enum.GetValues(typeof(AgentStatus)).Cast<AgentStatus>().Select(agentStatus =>
            {
                return new ListItemResponse
                {
                    Text = Enum.GetName(typeof(AgentStatus), agentStatus),
                    Value = ((int)agentStatus).ToString()
                };
            });

            return Success(agentStatuses);
        }

        [HttpGet]
        [Route("getcities")]
        [ProducesResponseType(typeof(ListItemResponse), StatusCodes.Status200OK)]
        public virtual IActionResult GetCities()
        {
            var cities = _cityService.GetCity(true).Select(city =>
            {
                return new ListItemResponse
                {
                    Text = city.Name,
                    Value = city.Id.ToString()
                };
            });

            return Success(cities);
        }

        [HttpPost]
        [Route("assignagent")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        public virtual IActionResult AssignAgent(AssignAgentRequest request)
        {
            if (request == null)
                return Error("Please provide proper request");

            if (request.OrderId <= 0)
                return Error("Please enter order id");

            if (request.AgentId <= 0)
                return Error("Please enter agent id");

            if (request.CityId <= 0)
                return Error("Please enter city id");

            var order = _orderService.GetOrderById(request.OrderId);
            if (order == null)
                return Error("Please enter valid order id");

            var assignedAgent = _customerService.GetAssignedAgentByOrderId(order.Id);
            if (assignedAgent == null)
            {
                assignedAgent = new AgentOrderStatus
                {
                    OrderId = order.Id,
                    AgentId = request.AgentId,
                    OrderStatus = request.StatusId,
                    SysDt = DateTime.UtcNow,
                    OrderDt = DateTime.UtcNow,
                    AgentStatusDt = DateTime.UtcNow,
                    CityId = request.CityId
                };
                _customerService.InsertAssignedAgent(assignedAgent);
            }
            else
            {
                assignedAgent.AgentId = request.AgentId;
                assignedAgent.OrderStatus = request.StatusId;
                assignedAgent.CityId = request.CityId;
                _customerService.UpdateAssignedAgent(assignedAgent);
            }

            return Success("", "Agent assigned successfully");
        }

        [HttpGet]
        [Route("ordernotificationlist")]
        [ProducesResponseType(typeof(IList<NotificationResponse>), StatusCodes.Status200OK)]
        public virtual IActionResult OrderNotification(int vendorId)
        {
            if (vendorId <= 0)
                return Error("Please provide vendor id");

            var vendor = _vendorService.GetVendorById(vendorId);
            if (vendor == null)
                return Error("Please provide proper vendor id");

            var orderList = _orderModelFactory.GetNotifications(vendorId);
            return Success(orderList);
        }

        #endregion
    }
}
