﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.Suppliers
{
    public partial class AddProductToSupplierSearchModel:BaseSearchModel
    {
        public AddProductToSupplierSearchModel()
        {
            AvailableSuppliers = new List<SelectListItem>();
            AvailableManufacturers = new List<SelectListItem>();
            AvailableStores = new List<SelectListItem>();
            AvailableVendors = new List<SelectListItem>();
            AvailableProductTypes = new List<SelectListItem>();
        }

        #region Properties

        [NopResourceDisplayName("Admin.supplier.Products.List.SearchProductName")]
        public string SearchProductName { get; set; }

        [NopResourceDisplayName("Admin.supplier.Products.List.SearchSupplier")]
        public int SearchSupplierId { get; set; }

        [NopResourceDisplayName("Admin.supplier.Products.List.SearchManufacturer")]
        public int SearchManufacturerId { get; set; }

        [NopResourceDisplayName("Admin.supplier.Products.List.SearchStore")]
        public int SearchStoreId { get; set; }

        [NopResourceDisplayName("Admin.supplier.Products.List.SearchVendor")]
        public int SearchVendorId { get; set; }

        [NopResourceDisplayName("Admin.supplier.Products.List.SearchProduct")]
        public int SearchProductId { get; set; }

        [NopResourceDisplayName("Admin.supplier.Products.List.Price")]
        public decimal SearchPrice { get; set; }

        [NopResourceDisplayName("Admin.supplier.Products.List.SearchProductType")]
        public int SearchProductTypeId { get; set; }

        [NopResourceDisplayName("Admin.supplier.Products.List.CurrentVendor")]
        public bool IsCurrentVendor { get; set; }

        public IList<SelectListItem> AvailableSuppliers { get; set; }

        public IList<SelectListItem> AvailableManufacturers { get; set; }

        public IList<SelectListItem> AvailableStores { get; set; }

        public IList<SelectListItem> AvailableVendors { get; set; }
      

        public IList<SelectListItem> AvailableProductTypes { get; set; }

        #endregion

    }
}
