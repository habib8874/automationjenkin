﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB
{
    /// <summary>
    /// Represents a tip model
    /// </summary>
    public partial class TipModel : BaseNopEntityModel
    {
        public TipModel() 
        {
            AvailableStores = new List<SelectListItem>();
        }

        [NopResourceDisplayName("NB.Admin.TipMaster.Fields.TipAmount")]
        public decimal TipAmount { get; set; }
        [NopResourceDisplayName("NB.Admin.TipMaster.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }
        [NopResourceDisplayName("NB.Admin.TipMaster.Fields.Deleted")]
        public bool Deleted { get; set; }
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        [NopResourceDisplayName("NB.Admin.TipMaster.Fields.StoreList")]
        public IList<SelectListItem> AvailableStores { get; set; }
    }
}