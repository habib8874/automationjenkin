﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.AccountsBilling
{
    public class AccountPackagePaymentUpdateModel : BaseNopModel
    {
        public AccountPackagePaymentUpdateModel()
        {
            CreditCardTypes = new List<SelectListItem>();
            ExpireMonths = new List<SelectListItem>();
            ExpireYears = new List<SelectListItem>();

            //package module
            AvailablePackages = new List<SelectListItem>();
            AvailablePackagesPlan = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Payment.SelectCreditCard")]
        public string CreditCardType { get; set; }

        [NopResourceDisplayName("Payment.SelectCreditCard")]
        public IList<SelectListItem> CreditCardTypes { get; set; }

        [NopResourceDisplayName("Payment.CardholderName")]
        public string CardholderName { get; set; }

        [NopResourceDisplayName("Payment.CardNumber")]
        public string CardNumber { get; set; }

        [NopResourceDisplayName("Payment.ExpirationDate")]
        public string ExpireMonth { get; set; }

        [NopResourceDisplayName("Payment.ExpirationDate")]
        public string ExpireYear { get; set; }

        public IList<SelectListItem> ExpireMonths { get; set; }

        public IList<SelectListItem> ExpireYears { get; set; }

        [NopResourceDisplayName("Payment.CardCode")]
        public string CardCode { get; set; }

        public int CustomerId { get; set; }

        public string PackageName { get; set; }

        public string PlanName { get; set; }

        public decimal AmountToBePaidPrice { get; set; }

        public string PlanPrice { get; set; }

        [NopResourceDisplayName("NB.Account.Fields.AmountToBePaid")]
        public string AmountToBePaid { get; set; }

        public DateTime Validity { get; set; }

        [NopResourceDisplayName("NB.Account.Fields.SelectPackage")]
        public int PackageId { get; set; }
        public IList<SelectListItem> AvailablePackages { get; set; }


        [NopResourceDisplayName("NB.Account.Fields.SelectPackagePlan")]
        public int PackagePlanId { get; set; }
        public IList<SelectListItem> AvailablePackagesPlan { get; set; }

        public bool SaveCardDetail { get; set; }

        public bool UseExistingCard { get; set; }
        public string ExistingCardDetail { get; set; }
    }
}
