﻿using System.ComponentModel.DataAnnotations;

namespace Nop.Core.Domain.NB.Merchant
{
    public enum  SortingFilters
    {
        [Display(Name = "Product Name A-Z")]
        SortByNameAtoZ = 5,

        [Display(Name = "Product Name Z-A")]
        SortByNameZtoA = 6,

        [Display(Name = "Price Low to High")]
        SortByPriceLowToHigh = 10,

        [Display(Name = "Price High to Low")]
        SortByPriceHighToLow = 11,
    }
}
