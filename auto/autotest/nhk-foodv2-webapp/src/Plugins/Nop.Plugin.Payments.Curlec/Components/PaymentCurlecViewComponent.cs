﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Payments.Curlec.Components
{
    [ViewComponent(Name = "PaymentCurlec")]
    public class PaymentCurlecViewComponent : NopViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("~/Plugins/Payments.Curlec/Views/PaymentInfo.cshtml");
        }
    }
}
