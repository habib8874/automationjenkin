﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.Orders
{
    public class AgentPushOrderVendorDetails: BaseNopEntityModel
    {
        public string PickupLocation { get; set; }
        public string PickupLat { get; set; }
        public string PickupLong { get; set; }
        public string PickupMobileNo { get; set; }
    }
}
