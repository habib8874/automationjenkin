﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Stores;
using Nop.Core.Infrastructure;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Stores;

namespace Nop.Web.Framework
{
    /// <summary>
    /// Store context for web application
    /// </summary>
    public partial class WebStoreContext : IStoreContext
    {
        #region Fields

        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IStoreService _storeService;

        private Store _cachedStore;
        private int? _cachedActiveStoreScopeConfiguration;
        private string _cachedGoogleMapAccountKey;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="genericAttributeService">Generic attribute service</param>
        /// <param name="httpContextAccessor">HTTP context accessor</param>
        /// <param name="storeService">Store service</param>
        public WebStoreContext(IGenericAttributeService genericAttributeService,
            IHttpContextAccessor httpContextAccessor,
            IStoreService storeService)
        {
            _genericAttributeService = genericAttributeService;
            _httpContextAccessor = httpContextAccessor;
            _storeService = storeService;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the current store
        /// </summary>
        public virtual Store CurrentStore
        {
            get
            {
                if (_cachedStore != null)
                    return _cachedStore;

                //try to determine the current store by HOST header
                string host = _httpContextAccessor.HttpContext?.Request?.Headers[HeaderNames.Host];

                var allStores = _storeService.GetAllStores(false);
                //var store = allStores.FirstOrDefault(s => s.Id== 47);
                var store = allStores.FirstOrDefault(s => host != null && s.Hosts != null && s.Hosts.Contains(host));

                if (store == null)
                {
                    //load the first found store
                    store = allStores.FirstOrDefault();
                }

                _cachedStore = store ?? throw new Exception("No store could be loaded");

                return _cachedStore;
            }
        }

        /// <summary>
        /// Gets active store scope configuration
        /// </summary>
        public virtual int ActiveStoreScopeConfiguration
        {
            get
            {
                if (_cachedActiveStoreScopeConfiguration.HasValue)
                    return _cachedActiveStoreScopeConfiguration.Value;

                //ensure that we have 2 (or more) stores
                if (_storeService.GetAllStores().Count > 1)
                {
                    //do not inject IWorkContext via constructor because it'll cause circular references
                    var currentCustomer = EngineContext.Current.Resolve<IWorkContext>().CurrentCustomer;

                    //try to get store identifier from attributes
                    var storeId = _genericAttributeService
                        .GetAttribute<int>(currentCustomer, NopCustomerDefaults.AdminAreaStoreScopeConfigurationAttribute);

                    //sub admin active store configuration
                    if (EngineContext.Current.Resolve<IWorkContext>().SubAdmin.IsSubAdminRole && storeId == 0)
                    {
                        storeId = EngineContext.Current.Resolve<IWorkContext>().GetCurrentStoreId;
                        _genericAttributeService
                        .SaveAttribute(currentCustomer, NopCustomerDefaults.AdminAreaStoreScopeConfigurationAttribute, storeId);
                    }
                    _cachedActiveStoreScopeConfiguration = _storeService.GetStoreById(storeId)?.Id ?? 0;
                }
                else
                    _cachedActiveStoreScopeConfiguration = 0;

                return _cachedActiveStoreScopeConfiguration ?? 0;
            }
        }

        #endregion

        #region Custom code from v4.0
        public virtual Store CurrentStoreById(int storeId)
        {
            //try to determine the current store by HOST header
            //string host = _httpContextAccessor.HttpContext?.Request?.Headers[HeaderNames.Host];

            var allStores = _storeService.GetAllStores(false);
            var store = allStores.FirstOrDefault(s => s.Id == storeId);

            if (store == null)
            {
                //load the first found store
                store = allStores.FirstOrDefault();
            }

            _cachedStore = store ?? throw new Exception("No store could be loaded");

            return _cachedStore;
        }

        /// <summary>
        /// Gets google map account key
        /// </summary>
        public virtual string GoogleMapAccountKey
        {
            get
            {
                if (!string.IsNullOrEmpty(_cachedGoogleMapAccountKey))
                    return _cachedGoogleMapAccountKey;

                //ensure that we have 1 (or more) stores
                if (CurrentStore.Id > 0)
                {
                    //do not inject ISettingService via constructor because it'll cause circular references
                    var settingService = EngineContext.Current.Resolve<ISettingService>();

                    //try to get store identifier from attributes
                    var googleAccountKey = settingService.GetSettingByKey<string>("NB.Google.Account.Key", "", CurrentStore.Id);

                    _cachedGoogleMapAccountKey = googleAccountKey;
                }
                else
                    _cachedGoogleMapAccountKey = "";

                return _cachedGoogleMapAccountKey;
            }
        }
        #endregion
    }
}