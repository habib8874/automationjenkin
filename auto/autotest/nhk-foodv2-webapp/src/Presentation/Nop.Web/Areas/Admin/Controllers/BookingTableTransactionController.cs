﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Primitives;
using Nop.Web.Areas.Admin.Helpers;
using Nop.Web.Areas.Admin.Infrastructure.Cache;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Areas.Admin.Models.Orders;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Vendors;
using Nop.Services;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.ExportImport;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Services.Shipping.Date;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Extensions;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Core.Data;
using Nop.Services.BookingTable;
using Nop.Web.Areas.Admin.Models.BookingTable;
using Nop.Core.Domain.BookingTable;
using System.Globalization;
using Nop.Services.Messages;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Configuration;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class BookingTableTransactionController : BaseAdminController
    {
        #region Fields
        private readonly IEmailAccountService _emailAccountService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IRepository<BookingTableMaster> _bookingrepository;
        private readonly IRepository<BookingNote> _bookingNoterepository;
        private readonly IRepository<BookingTableTransaction> _bookingTransactionrepository;
        private readonly IBookingService _bookingService;
        private readonly IEmailSender _emailSender;
        private readonly ILanguageService _SMSLanguageModuleService;
        private readonly IProductService _productService;
        private readonly IProductTemplateService _productTemplateService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly ICustomerService _customerService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IWorkContext _workContext;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly IPictureService _pictureService;
        private readonly ITaxCategoryService _taxCategoryService;
        private readonly IProductTagService _productTagService;
        private readonly ICopyProductService _copyProductService;
        private readonly IPdfService _pdfService;
        private readonly IExportManager _exportManager;
        private readonly IImportManager _importManager;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IPermissionService _permissionService;
        private readonly IAclService _aclService;
        private readonly IStoreService _storeService;
        private readonly IOrderService _orderService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IVendorService _vendorService;
        private readonly IDateRangeService _dateRangeService;
        private readonly IShippingService _shippingService;
        private readonly IShipmentService _shipmentService;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;
        private readonly IMeasureService _measureService;
        private readonly MeasureSettings _measureSettings;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IDiscountService _discountService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IBackInStockSubscriptionService _backInStockSubscriptionService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IDownloadService _downloadService;
        private readonly ISettingService _settingService;
        private readonly TaxSettings _taxSettings;
        private readonly VendorSettings _vendorSettings;
        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly IStoreContext _storeContext;
        private readonly ITokenizer _tokenizer;
        private readonly IRepository<MessageTemplate> _messageTemplate;
        private readonly IRepository<Setting> _settings;
        private readonly INotificationService _notificationService;

        #endregion

        #region Ctor

        public BookingTableTransactionController(
            IEmailAccountService emailAccountService,
            IEmailSender emailSender,
            IWorkflowMessageService _workflowMessageService,
            IBookingService bookingService,
            IRepository<BookingNote> bookingNoterepository,
            ILanguageService SMSLanguageModuleService,
            IProductService productService,
            IProductTemplateService productTemplateService,
            ICategoryService categoryService,
            IManufacturerService manufacturerService,
            ICustomerService customerService,
            IUrlRecordService urlRecordService,
            IWorkContext workContext,
            ILanguageService languageService,
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService,
            ISpecificationAttributeService specificationAttributeService,
            IPictureService pictureService,
            ITaxCategoryService taxCategoryService,
            IProductTagService productTagService,
            ICopyProductService copyProductService,
            IPdfService pdfService,
            IExportManager exportManager,
            IImportManager importManager,
            ICustomerActivityService customerActivityService,
            IPermissionService permissionService,
            IAclService aclService,
            IStoreService storeService,
            IOrderService orderService,
            IStoreMappingService storeMappingService,
            IVendorService vendorService,
            IDateRangeService dateRangeService,
            IShippingService shippingService,
            IShipmentService shipmentService,
            ICurrencyService currencyService,
            CurrencySettings currencySettings,
            IMeasureService measureService,
            MeasureSettings measureSettings,
            IStaticCacheManager cacheManager,
            IDateTimeHelper dateTimeHelper,
            IDiscountService discountService,
            IProductAttributeService productAttributeService,
            IBackInStockSubscriptionService backInStockSubscriptionService,
            IShoppingCartService shoppingCartService,
            IProductAttributeFormatter productAttributeFormatter,
            IProductAttributeParser productAttributeParser,
            IDownloadService downloadService,
            ISettingService settingService,
            TaxSettings taxSettings,
            IRepository<BookingTableMaster> repository,
            IRepository<BookingTableTransaction> bookingTransactionrepository,
            VendorSettings vendorSettings,
            IMessageTokenProvider messageTokenProvider,
            IStoreContext storeContext, ITokenizer tokenizer,
            IRepository<MessageTemplate> messageTemplate,
            IRepository<Setting> settings,
            INotificationService notificationService)




        {
            _emailAccountService = emailAccountService;
            _emailSender = emailSender;
            _workflowMessageService = _workflowMessageService;
            _bookingNoterepository = bookingNoterepository;
            _bookingrepository = repository;
            _bookingTransactionrepository = bookingTransactionrepository;
            _bookingService = bookingService;
            _SMSLanguageModuleService = SMSLanguageModuleService;
            _productService = productService;
            _productTemplateService = productTemplateService;
            _categoryService = categoryService;
            _manufacturerService = manufacturerService;
            _customerService = customerService;
            _urlRecordService = urlRecordService;
            _workContext = workContext;
            _languageService = languageService;
            _localizationService = localizationService;
            _localizedEntityService = localizedEntityService;
            _specificationAttributeService = specificationAttributeService;
            _pictureService = pictureService;
            _taxCategoryService = taxCategoryService;
            _productTagService = productTagService;
            _copyProductService = copyProductService;
            _pdfService = pdfService;
            _exportManager = exportManager;
            _importManager = importManager;
            _customerActivityService = customerActivityService;
            _permissionService = permissionService;
            _aclService = aclService;
            _storeService = storeService;
            _orderService = orderService;
            _storeMappingService = storeMappingService;
            _vendorService = vendorService;
            _dateRangeService = dateRangeService;
            _shippingService = shippingService;
            _shipmentService = shipmentService;
            _currencyService = currencyService;
            _currencySettings = currencySettings;
            _measureService = measureService;
            _measureSettings = measureSettings;
            _cacheManager = cacheManager;
            _dateTimeHelper = dateTimeHelper;
            _discountService = discountService;
            _productAttributeService = productAttributeService;
            _backInStockSubscriptionService = backInStockSubscriptionService;
            _shoppingCartService = shoppingCartService;
            _productAttributeFormatter = productAttributeFormatter;
            _productAttributeParser = productAttributeParser;
            _downloadService = downloadService;
            _settingService = settingService;
            _taxSettings = taxSettings;
            _vendorSettings = vendorSettings;
            _messageTokenProvider = messageTokenProvider;
            _storeContext = storeContext;
            _tokenizer = tokenizer;
            _messageTemplate = messageTemplate;
            _settings = settings;
            _notificationService = notificationService;



        }

        #endregion

        #region Utilities

        protected virtual void UpdateLocales(Product product, ProductModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(product,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                    x => x.ShortDescription,
                    localized.ShortDescription,
                    localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                    x => x.FullDescription,
                    localized.FullDescription,
                    localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                    x => x.MetaKeywords,
                    localized.MetaKeywords,
                    localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                    x => x.MetaDescription,
                    localized.MetaDescription,
                    localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                    x => x.MetaTitle,
                    localized.MetaTitle,
                    localized.LanguageId);

                //search engine name
                var seName = _urlRecordService.ValidateSeName(product, localized.SeName, localized.Name, false);
                _urlRecordService.SaveSlug(product, seName, localized.LanguageId);
            }
        }

        protected virtual void UpdateLocales(ProductTag productTag, ProductTagModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(productTag,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);
            }
        }

        protected virtual void UpdateLocales(ProductAttributeMapping pam, ProductAttributeMappingModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(pam,
                    x => x.TextPrompt,
                    localized.TextPrompt,
                    localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(pam,
                    x => x.DefaultValue,
                    localized.DefaultValue,
                    localized.LanguageId);
            }
        }

        protected virtual void UpdateLocales(ProductAttributeValue pav, ProductAttributeValueModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(pav,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);
            }
        }



        #endregion

        #region Methods

        #region Product list / create / edit / delete

        //list products
        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingTransaction))
                return AccessDeniedView();

            var model = new BookingTableTransactionListSearchModel();
            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
            {
                var customerId = _workContext.CurrentCustomer.Id;
                var customer = _customerService.GetAllCustomers().Where(x => x.Id == customerId).FirstOrDefault();

                //vendor
                model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                var vandorslist = _vendorService.GetAllVendors();
                if (vandorslist != null && vandorslist.Any())
                {
                    foreach (var c in vandorslist)
                    {
                        model.AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
                model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                foreach (var s in _storeService.GetAllStores())
                {
                    model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });
                }

            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                var customer = _customerService.GetAllCustomers().Where(x => x.RegisteredInStoreId == _workContext.CurrentCustomer.RegisteredInStoreId).ToList();
                model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                var vandorslist = _vendorService.GetAllVendors();
                if (vandorslist != null && vandorslist.Any())
                {
                    foreach (var c in vandorslist)
                    {
                        if (c.StoreId == stores.Id)
                            model.AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
            }
            //vendor
            //model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            //var vandors = SelectListHelper.GetVendorList(_vendorService, _cacheManager, true);
            //foreach (var c in vandors)
            //    model.AvailableVendors.Add(c);
            ////stores
            //model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            //foreach (var s in _storeService.GetAllStores())
            //    model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });
            model.AvailableTimings.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
            foreach (var s in _storeService.GetAllAvailableTiming())
            {
                model.AvailableTimings.Add(new SelectListItem { Text = s, Value = s.ToString() });
            }
            model.AvailableBookingStatus.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
            Array values = Enum.GetValues(typeof(BookingTableStatus));
            foreach (var s in values)
            {
                model.AvailableBookingStatus.Add(new SelectListItem { Text = Enum.GetName(typeof(BookingTableStatus), s), Value = ((int)s).ToString() });
            }
            model.SetGridPageSize();
            return View(model);
        }
        public virtual IActionResult BookingNoteAdd(int BookingId, int downloadId, bool displayToCustomer, string message)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _bookingTransactionrepository.GetById(BookingId);
            if (order == null)
                return Json(new { Result = false });




            var orderNote = new BookingNote
            {
                DisplayToCustomer = displayToCustomer,
                Note = message,
                DownloadId = downloadId,
                CreatedOnUtc = DateTime.UtcNow,
                BookingId = BookingId
            };
            _bookingNoterepository.Insert(orderNote);

            //new order notification
            if (displayToCustomer)
            {
                string subject = "Booking Notes";
                string body = orderNote.Note;
                var emailAccount = _emailAccountService.GetEmailAccountById(2);
                _emailSender.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, order.Email, null);
                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Configuration.EmailAccounts.SendTestEmail.Success"), false);
                //email
                //_workflowMessageService.SendNewBookingNoteAddedCustomerNotification(
                //    orderNote, _workContext.WorkingLanguage.Id);

            }

            return Json(new { Result = true });
        }
        public virtual IActionResult BookingNotesSelect(int BookingId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingTransaction))
                return AccessDeniedDataTablesJson();

            var order = _bookingTransactionrepository.GetById(BookingId);
            if (order == null)
                throw new ArgumentException("No Booking Note found with the specified id");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return Content("");

            //order notes
            var orderNoteModels = new List<BookingTableTransactionModel.BookingNote>();
            var ordernotes = _bookingNoterepository.Table.ToList();
            foreach (var orderNote in ordernotes
                .OrderByDescending(on => on.CreatedOnUtc))
            {
                var download = _downloadService.GetDownloadById(orderNote.DownloadId);
                orderNoteModels.Add(new BookingTableTransactionModel.BookingNote
                {
                    Id = orderNote.Id,
                    BookingId = orderNote.BookingId,
                    DownloadId = orderNote.DownloadId,
                    DownloadGuid = download != null ? download.DownloadGuid : Guid.Empty,
                    DisplayToCustomer = orderNote.DisplayToCustomer,
                    Note = orderNote.Note,
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(orderNote.CreatedOnUtc, DateTimeKind.Utc)
                });
            }

            //var gridModel = new DataSourceResult
            //{
            //    Data = orderNoteModels,
            //    Total = orderNoteModels.Count
            //};

            return Json(orderNoteModels);
        }
        [HttpPost]
        public virtual IActionResult BookingTransactionList(BookingTableTransactionListSearchModel model)
        {
            model.SetGridPageSize();
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingTransaction))
                return AccessDeniedDataTablesJson();

            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
            {
                if (model.SearchStoreId <= 0)
                    model.SearchStoreId = 0;
                if (model.SearchVendorId <= 0)
                    model.SearchVendorId = 0;
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                if (model.SearchStoreId == 0)
                {
                    var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                    model.SearchStoreId = stores.Id;
                }
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
            {
                if (model.SearchVendorId == 0)
                {
                    var vendorId = _customerService.GetAllCustomers().FirstOrDefault(x => x.Id == _workContext.CurrentCustomer.Id).VendorId;
                    model.SearchVendorId = vendorId;
                }
            }

            var bookingTables = _bookingService.SearchBookingTables(
                //categoryIds: categoryIds,
                //manufacturerId: model.SearchManufacturerId,
                BookingSearchDate: model.BookingSearchDate,
                From: model.From,
                To: model.To,
                storeId: model.SearchStoreId,
                vendorId: model.SearchVendorId,
                SearchCustomerName: model.SearchCustomerName,
                SearchTableNumber: model.SearchTableNumber,
                SearchContactNumber: model.SearchContactNumber,
                SearchBookingStatusId: model.SearchBookingStatusId,
                //warehouseId: model.SearchWarehouseId,
                //productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
                //keywords: model.SearchProductName,
                pageIndex: model.Page - 1,
                pageSize: model.PageSize,
                showHidden: true
            //overridePublished: overridePublished
            );

            var modelList = new BookingTableTransactionListModel().PrepareToGrid(model, bookingTables, () =>
            {
                //fill in model values from the entity
                return bookingTables.Select(x =>
                {
                    var booktable = new BookingTableTransactionModel
                    {
                        Id = x.Id,
                        BookingDate = x.BookingDate,
                        BookingTimeFrom = x.BookingTimeFrom,
                        BookingTimeTo = x.BookingTimeTo,
                        CustomerName = x.CustomerName,
                        TableNumber = x.TableNumber,
                        Email = x.Email,
                        MobileNumber = x.MobileNumber,
                        //TableNo = x.TableNo,
                        SeatingCapacity = x.SeatingCapacity
                        //Seats = x.Seats
                    };
                    var values = Enum.GetValues(typeof(BookingTableStatus));
                    foreach (var s in values)
                    {
                        if ((int)s == x.Status)
                        {
                            booktable.Status = (Enum.GetName(typeof(BookingTableStatus), s));
                        }
                    }
                    booktable.Store = _storeService.GetStoreById(x.StoreId) == null ? "Na" : _storeService.GetStoreById(x.StoreId).Name;
                    booktable.Vendor = _vendorService.GetVendorById(x.VendorId) == null ? "Na" : _vendorService.GetVendorById(x.VendorId).Name;
                    DateTime time = DateTime.Today.Add(x.BookingTimeFrom);
                    booktable.BookingFrom = time.ToString("hh:mm tt");
                    DateTime time2 = DateTime.Today.Add(x.BookingTimeTo);
                    booktable.BookingTo = time2.ToString("hh:mm tt");
                    //sms.Language = _SMSLanguageModuleService.GetLanguageById(x.LanguageId).Name;
                    //sms.MessageSource = _SMSMessageSourceService.GetById(Convert.ToInt32(x.MessageSource)).Name;
                    //  sms.MessageModule = _SMSMessageModuleService.GetById(Convert.ToInt32(x.MessageModule)).Name;
                    return booktable;
                });
            });
           

            return Json(modelList);
        }
        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingTransaction))
                return AccessDeniedView();

            var order = _bookingService.GetBookingById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            ////a vendor does not have access to this functionality
            //if (_workContext.CurrentVendor != null && !HasAccessToOrder(order))
            //    return RedirectToAction("List");

            var model = new BookingTableTransactionModel();
            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
            {
                var customerId = _workContext.CurrentCustomer.Id;
                var customer = _customerService.GetAllCustomers().Where(x => x.Id == customerId).FirstOrDefault();

                //vendor
                model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                var vandorslist = _vendorService.GetAllVendors();
                if (vandorslist != null && vandorslist.Any())
                {
                    foreach (var c in vandorslist)
                    {
                        model.AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
                model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                foreach (var s in _storeService.GetAllStores())
                {
                    model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });
                }
                model.SearchVendorId = 0;
                model.SearchStoreId = 0;

            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                var customer = _customerService.GetAllCustomers().Where(x => x.RegisteredInStoreId == _workContext.CurrentCustomer.RegisteredInStoreId).ToList();
                model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                var vandorslist = _vendorService.GetAllVendors();
                if (vandorslist != null && vandorslist.Any())
                {
                    foreach (var c in vandorslist)
                    {
                        if (c.StoreId == stores.Id)
                            model.AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
                model.SearchStoreId = stores.Id;
                model.SearchVendorId = 0;
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
            {
                var vendor = _customerService.GetAllCustomers().FirstOrDefault(x => x.Id == _workContext.CurrentCustomer.Id);
                ///*bookingTableTransaction.StoreId = vendor.Re*/gisteredInStoreId;
                model.SearchVendorId = vendor.VendorId;
                model.SearchStoreId = vendor.RegisteredInStoreId;
            }
            model.SearchStoreId = order.StoreId;
            model.SearchVendorId = order.VendorId;
            model.DateOfBirthDay = order.BookingDate.ToShortDateString();
            DateTime time = DateTime.Today.Add(order.BookingTimeFrom);
            model.From = time.ToString("h:mm tt");
            DateTime totime = DateTime.Today.Add(order.BookingTimeTo);
            model.To = totime.ToString("h:mm tt");
            model.TableNumber = order.TableNumber;
            PrepareBookingDetailsModel(model, order);

            return View(model);
        }
        [HttpPost, ActionName("Edit")]
        [FormValueRequired("cancelbooking")]
        public virtual IActionResult CancelBooking(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingTransaction))
                return AccessDeniedView();

            var order = _bookingService.GetBookingById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");




            try
            {
                _bookingService.CancelBooking(order, true);
                //LogEditOrder(order.Id);
                var model = new BookingTableTransactionModel();
                PrepareBookingDetailsModel(model, order);
                var emailId = _settings.Table.Where(x => x.Name.Contains("emailaccountsettings.defaultemailaccountid")).FirstOrDefault();
                if (emailId != null)
                {
                    int Email = Convert.ToInt32(emailId.Value);

                    var emailAccount = _emailAccountService.GetEmailAccountById(Email);
                    var tokens = new List<Token>();
                    _messageTokenProvider.AddStoreTokens(tokens, _storeContext.CurrentStore, emailAccount);
                    var store = _storeService.GetStoreById(model.StoreId);
                    if (store != null)
                    {
                        _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
                    }
                    var vendorBody = _vendorService.GetVendorById(model.VendorId);
                    if (vendorBody != null)
                    {
                        _messageTokenProvider.AddVendorTokens(tokens, vendorBody);
                    }
                    var welcomeMessage = _messageTemplate.Table.Where(x => x.Name.ToString().Contains("Customer.Email.BookTable")).FirstOrDefault();

                    var subject = _tokenizer.Replace(welcomeMessage.Subject, tokens, false);
                    subject = subject.Replace("%Booking.Status%", "Cancelled");
                    var body = _tokenizer.Replace(welcomeMessage.Body, tokens, true);
                    body = body.Replace("%Booking.Status%", "Cancelled");
                    body = body.Replace("%Customer.FullName%", model.CustomerName);
                    _emailSender.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, model.Email, null);
                    // _emailSender.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, bookingTableTransaction.Email, null);
                }
                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Configuration.EmailAccounts.SendTestEmail.Success"), false);
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                //error
                var model = new BookingTableTransactionModel();
                PrepareBookingDetailsModel(model, order);

                _notificationService.ErrorNotification(exc, false);
                return View(model);
            }
        }
        [HttpPost, ActionName("Edit")]
        [FormValueRequired("btnSaveBookingStatus")]
        public virtual IActionResult ChangeBookingStatus(int id, BookingTableTransactionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingTransaction))
                return AccessDeniedView();


            var order = _bookingService.GetBookingById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            //if (_workContext.CurrentVendor != null)
            //    return RedirectToAction("Edit", "Order", new { id = id });

            try
            {
                //order.OrderStatusId = model.OrderStatusId;
                //_orderService.UpdateOrder(order);

                ////add a note
                //order.OrderNotes.Add(new OrderNote
                //{
                //    Note = $"Order status has been edited. New status: {order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext)}",
                //    DisplayToCustomer = false,
                //    CreatedOnUtc = DateTime.UtcNow
                //});
                order.Status = model.BookingStatus;
                _bookingService.UpdateBooking(order);
                //LogEditOrder(order.Id);
                model = new BookingTableTransactionModel();
                PrepareBookingDetailsModel(model, order);
                var emailId = _settings.Table.Where(x => x.Name.Contains("emailaccountsettings.defaultemailaccountid")).FirstOrDefault();
                if (emailId != null)
                {
                    int Email = Convert.ToInt32(emailId.Value);

                    var emailAccount = _emailAccountService.GetEmailAccountById(Email);
                    var tokens = new List<Token>();
                    _messageTokenProvider.AddStoreTokens(tokens, _storeContext.CurrentStore, emailAccount);
                    var store = _storeService.GetStoreById(model.StoreId);
                    if (store != null)
                    {
                        _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
                    }
                    var vendorBody = _vendorService.GetVendorById(model.VendorId);
                    if (vendorBody != null)
                    {
                        _messageTokenProvider.AddVendorTokens(tokens, vendorBody);
                    }
                    var welcomeMessage = _messageTemplate.Table.Where(x => x.Name.ToString().Contains("Customer.Email.BookTable")).FirstOrDefault();
                    string BookingStatus = Enum.GetValues(typeof(BookingTableStatus)).Cast<BookingTableStatus>().Where(x => (int)x == model.BookingStatus).Select(x => x.ToString()).ToList()[0];
                    var subject = _tokenizer.Replace(welcomeMessage.Subject, tokens, false);
                    subject = subject.Replace("%Booking.Status%", BookingStatus);
                    var body = _tokenizer.Replace(welcomeMessage.Body, tokens, true);
                    body = body.Replace("%Booking.Status%", BookingStatus);
                    body = body.Replace("%Customer.FullName%", model.CustomerName);
                    _emailSender.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, model.Email, null);
                    // _emailSender.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, bookingTableTransaction.Email, null);
                }
                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Configuration.EmailAccounts.SendTestEmail.Success"), false);
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                //error
                model = new BookingTableTransactionModel();
                PrepareBookingDetailsModel(model, order);
                _notificationService.ErrorNotification(exc, false);
                return View(model);
            }
        }

        protected virtual void PrepareBookingDetailsModel(BookingTableTransactionModel model, BookingTableTransaction order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            if (model == null)
                throw new ArgumentNullException(nameof(model));

            model.Id = order.Id;
            model.BookingDate = order.BookingDate;
            model.BookingTimeFrom = order.BookingTimeFrom;
            model.BookingTimeTo = order.BookingTimeTo;
            model.TableNumber = order.TableNumber;
            model.CustomerName = order.CustomerName;
            model.Email = order.Email;
            model.MobileNumber = order.MobileNumber;
            model.Seats = Convert.ToInt32(order.SeatingCapacity);
            model.SpecialNotes = order.SpecialNotes;
            //a vendor should have access only to his products
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;
            model.CanCancelBooking = true;
            model.BookingStatus = order.Status;
            model.VendorId = order.VendorId;
            model.StoreId = order.StoreId;


            var values = Enum.GetValues(typeof(BookingTableStatus));
            foreach (var s in values)
            {
                if ((int)s == order.Status)
                {
                    model.Status = (Enum.GetName(typeof(BookingTableStatus), s));
                }
            }
            //model.Status= 
            //custom values

            //var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
            //if (primaryStoreCurrency == null)
            //    throw new Exception("Cannot load primary store currency");

            ////order totals
            //PrepareOrderTotals(model, order, primaryStoreCurrency);

            ////payment info
            //PreparePaymentInfo(model, order);

            ////billing info
            //PrepareBillingInfo(model, order);

            ////shipping info
            //PrepareShippingInfo(model, order);

            ////products
            //PrepareProducts(model, order, primaryStoreCurrency);
            foreach (var s in values)
            {
                model.AvailableBookingStatus.Add(new SelectListItem { Text = Enum.GetName(typeof(BookingTableStatus), s), Value = ((int)s).ToString() });
            }
        }
        [ActionName("Create")]
        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingTransaction))
                return AccessDeniedView();


            var model = new BookingTableTransactionListSearchModel();

            try
            {
                if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
                {
                    var customerId = _workContext.CurrentCustomer.Id;
                    var customer = _customerService.GetAllCustomers().Where(x => x.Id == customerId).FirstOrDefault();

                    //vendor
                    model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                    var vandorslist = _vendorService.GetAllVendors();
                    if (vandorslist != null && vandorslist.Any())
                    {
                        foreach (var c in vandorslist)
                        {
                            model.AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                        }
                    }
                    model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                    foreach (var s in _storeService.GetAllStores())
                    {
                        model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });
                    }
                    model.SearchVendorId = 0;
                    model.SearchStoreId = 0;

                }
                else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
                {
                    var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                    var customer = _customerService.GetAllCustomers().Where(x => x.RegisteredInStoreId == _workContext.CurrentCustomer.RegisteredInStoreId).ToList();
                    model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                    var vandorslist = _vendorService.GetAllVendors();
                    if (vandorslist != null && vandorslist.Any())
                    {
                        foreach (var c in vandorslist)
                        {
                            if (c.StoreId == stores.Id)
                                model.AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                        }
                    }
                    model.SearchStoreId = stores.Id;
                    model.SearchVendorId = 0;
                }
                else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
                {
                    var vendor = _customerService.GetAllCustomers().FirstOrDefault(x => x.Id == _workContext.CurrentCustomer.Id);
                    ///*bookingTableTransaction.StoreId = vendor.Re*/gisteredInStoreId;
                    model.SearchVendorId = vendor.VendorId;
                    model.SearchStoreId = vendor.RegisteredInStoreId;
                }

                //order.OrderStatusId = model.OrderStatusId;
                //_orderService.UpdateOrder(order);

                ////add a note
                //order.OrderNotes.Add(new OrderNote
                //{
                //    Note = $"Order status has been edited. New status: {order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext)}",
                //    DisplayToCustomer = false,
                //    CreatedOnUtc = DateTime.UtcNow
                //});
                //LogEditOrder(order.Id);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                model = new BookingTableTransactionListSearchModel();
                _notificationService.ErrorNotification(exc, false);
                return View(model);
            }
        }
        public virtual BookingTableTransactionListSearchModel GetAvailableTimings(int id, string Store, string VendorId)
        {
            string date = DateTime.UtcNow.ToShortDateString();
            var model = new BookingTableTransactionListSearchModel();
            model.DateOfBirthDay = date;
            var timingsList = _storeService.GetAllAvailableTiming();

            var storeId = Convert.ToInt32(Store);
            var vendorId = Convert.ToInt32(VendorId);
            if (storeId == 0 || vendorId == 0)
            {
                timingsList = new List<string>();
                model.AvailableTimings.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
            }
            else
            {
                model.AvailableTimings.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
            }
            if (_workContext.CurrentVendor != null)
            {
                //vendorId = _workContext.CurrentVendor.Id;
            }
            //var storeId = 1;
            if (id != 0)
            {
                var order = _bookingTransactionrepository.Table.Where(x => x.Id == id).FirstOrDefault();
                DateTime time = DateTime.Today.Add(order.BookingTimeFrom);
                model.From = time.ToString("h:mm tt");
            }
            var booked = _bookingrepository.Table.Where(x => x.StoreId == storeId && x.VendorId == vendorId).ToList();
            if (booked.Any())
            {
                DateTime dateTime = DateTime.Now;
                TimeSpan fromTime = dateTime.TimeOfDay;
                TimeSpan toTime = dateTime.TimeOfDay;
                foreach (var item in booked)
                {
                    fromTime = item.TableAvailableFrom;
                    toTime = item.TableAvailableTo;
                }
                foreach (var s in timingsList)
                {
                    DateTime dateTimetoConvert = DateTime.ParseExact(s,
                                        "h:mm tt", CultureInfo.InvariantCulture);
                    TimeSpan span = dateTimetoConvert.TimeOfDay;
                    if (span >= fromTime && span <= toTime)
                        model.AvailableTimings.Add(new SelectListItem { Text = s, Value = s.ToString() });
                }
            }

            return model;
        }

        public virtual IList<SelectListItem> GetAvailableToTimings(int id, string From, string Store, string Vendor)
        {
            IList<SelectListItem> AvailableTimings = new List<SelectListItem>();
            var timingsList = _storeService.GetAllAvailableTiming();
            var storeId = Convert.ToInt32(Store);
            var vendorId = Convert.ToInt32(Vendor);
            if (storeId == 0 || vendorId == 0)
            {
                timingsList = new List<string>();
                AvailableTimings.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
            }
            else
            {
                AvailableTimings.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
            }
            var booked = _bookingrepository.Table.Where(x => x.StoreId == storeId && x.VendorId == vendorId).ToList();
            DateTime dateTime = DateTime.Now;
            TimeSpan fromTime = dateTime.TimeOfDay;
            TimeSpan toTime = dateTime.TimeOfDay;
            foreach (var item in booked)
            {
                fromTime = item.TableAvailableFrom;
                toTime = item.TableAvailableTo;
            }
            string from = string.Empty;
            if (id != 0)
            {
                var order = _bookingTransactionrepository.Table.Where(x => x.Id == id).FirstOrDefault();
                DateTime time = DateTime.Today.Add(order.BookingTimeTo);
                from = time.ToString("h:mm tt");
            }
            foreach (var s in timingsList)
            {
                DateTime dateTimetoConvert = DateTime.ParseExact(s,
                                    "h:mm tt", CultureInfo.InvariantCulture);
                DateTime ConvertFromTiming = DateTime.ParseExact(From,
                                    "h:mm tt", CultureInfo.InvariantCulture);
                TimeSpan span = dateTimetoConvert.TimeOfDay;
                TimeSpan spanFrom = ConvertFromTiming.TimeOfDay;
                if (span > spanFrom && span <= toTime)
                    AvailableTimings.Add(new SelectListItem { Text = s, Value = s.ToString(), Selected = (from == s) });
            }
            return AvailableTimings;
        }
        public virtual IList<SelectListItem> GetAvailableTables(int id, string From, string To, string date, string Store, string Vendor)
        {
            IList<SelectListItem> AvailableTableforBookings = new List<SelectListItem>();
            var CurrStoreId = _workContext.CurrentCustomer.RegisteredInStoreId;
            int CurrVendorId = 0;
            var storeId = Convert.ToInt32(Store);
            var vendorId = Convert.ToInt32(Vendor);

            var TableAvailableforbooking = _bookingrepository.Table.Where(x => x.StoreId == storeId && x.VendorId == vendorId && x.Status).ToList();
            if (storeId == 0 || vendorId == 0)
            {
                TableAvailableforbooking = new List<BookingTableMaster>();
                AvailableTableforBookings.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
            }
            else
            {
                AvailableTableforBookings.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
            }
            var model = new BookingTableTransactionListModel();
            // model.DateOfBirthDay = date;
            //var storeId = _workContext.GetCurrentStoreId(); //_workContext.CurrentCustomer.RegisteredInStoreId;
            //var storeId = 1;
            DateTime dateTimeDate = DateTime.Parse(date);
            DateTime dateTimetoConvert = DateTime.ParseExact(From,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanFrom = dateTimetoConvert.TimeOfDay;
            DateTime dateTimetoConvertTo = DateTime.ParseExact(To,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanTo = dateTimetoConvertTo.TimeOfDay;
            //spanFrom.Subtract(new TimeSpan(0, 30, 0));
            //spanFrom=spanFrom.Add(new TimeSpan(0, 30, 0));
            //spanTo = spanTo.Subtract(new TimeSpan(0, 30, 0));

            DateTime dateTime = DateTime.Now;
            TimeSpan fromTime = dateTime.TimeOfDay;
            TimeSpan toTime = dateTime.TimeOfDay;
            var BookedTables = _bookingTransactionrepository.Table.Where(x => x.StoreId == storeId && x.VendorId == vendorId && x.BookingDate == dateTimeDate
            && ((x.BookingTimeTo >= spanFrom && x.BookingTimeFrom <= spanFrom)) && x.Status == (int)BookingTableStatus.Booked).ToList();
            var BookedTablesto = _bookingTransactionrepository.Table.Where(x => x.StoreId == storeId && x.VendorId == vendorId && x.BookingDate == dateTimeDate
            && ((x.BookingTimeTo >= spanTo && x.BookingTimeFrom <= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
            var BookedTables3 = _bookingTransactionrepository.Table.Where(x => x.StoreId == storeId && x.VendorId == vendorId && x.BookingDate == dateTimeDate
            && ((x.BookingTimeFrom >= spanFrom && x.BookingTimeFrom <= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
            //BookedTables = _bookingTransactionrepository.Table.Where(x => x.StoreId == storeId && x.VendorId == vendorId && x.BookingDate == dateTimeDate
            //&& ((x.BookingTimeTo >= spanTo && x.BookingTimeFrom <= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
            var BookedTablesto3 = _bookingTransactionrepository.Table.Where(x => x.StoreId == storeId && x.VendorId == vendorId && x.BookingDate == dateTimeDate
            && ((x.BookingTimeTo <= spanFrom && x.BookingTimeTo >= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
            var lstBooksTable = BookedTables.Select(x => x.TableNumber).Union(BookedTablesto.Select(x => x.TableNumber))
                .Union(BookedTables3.Select(x => x.TableNumber)).Union(BookedTablesto3.Select(x => x.TableNumber));
            //List<int> lstBooksTables = new List<int>();
            //if (lstBooksTable.Any())
            //{
            //     lstBooksTables = BookedTables.Select(x => x.TableNumber).Except(lstBooksTable.Select(x=>x.TableNumber)).ToList();
            //}
            var lstTables = BookedTables.Where(x => x.Id == id).FirstOrDefault();
            foreach (var item in TableAvailableforbooking)
            {
                fromTime = item.TableAvailableFrom;
                toTime = item.TableAvailableTo;
            }

            //select new
            //{
            //    Tablenumber = a.TableNo,
            //    IsAvailable = !BookedTables.Where(x => x.TableNumber == a.TableNo && x.StoreId == storeId && (x.BookingTimeTo <= spanFrom && x.BookingTimeTo >= spanTo)).Any()

            //}
            var TableAvailableAccordingToSelectedTiming = TableAvailableforbooking.Where(x => !lstBooksTable.Contains(x.TableNo)).ToList();

            foreach (var item in TableAvailableAccordingToSelectedTiming)
            {
                //if (item.IsAvailable)
                AvailableTableforBookings.Add(new SelectListItem { Text = item.TableNo.ToString(), Value = item.TableNo.ToString() });

            }
            if (lstTables != null)
                AvailableTableforBookings.Add(new SelectListItem { Text = lstTables.TableNumber.ToString(), Value = lstTables.TableNumber.ToString() });
            //foreach (var s in timingsList)
            //{
            //    DateTime dateTimetoConvert = DateTime.ParseExact(s,
            //                        "h:mm tt", CultureInfo.InvariantCulture);
            //    TimeSpan span = dateTimetoConvert.TimeOfDay;
            //    if (span >= fromTime && span <= toTime)
            //        model.AvailableTimings.Add(new SelectListItem { Text = s, Value = s.ToString() });
            //}
            return AvailableTableforBookings;
        }
        public virtual IActionResult SubmitTableDetails(BookingTableTransactionListSearchModel bookingTableTransactionModel)
        {
            IList<SelectListItem> AvailableTableforBookings = new List<SelectListItem>();
            BookingTableTransaction bookingTableTransaction = new BookingTableTransaction();
            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
            {
                bookingTableTransaction.StoreId = bookingTableTransactionModel.SearchStoreId;
                bookingTableTransaction.VendorId = bookingTableTransactionModel.SearchVendorId;

            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                bookingTableTransaction.StoreId = stores.Id;
                bookingTableTransaction.VendorId = bookingTableTransactionModel.SearchVendorId;
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
            {
                var vendor = _customerService.GetAllCustomers().FirstOrDefault(x => x.Id == _workContext.CurrentCustomer.Id);
                bookingTableTransaction.StoreId = vendor.RegisteredInStoreId;
                bookingTableTransaction.VendorId = vendor.VendorId;
            }
            DateTime dateTimetoConvert = DateTime.ParseExact(bookingTableTransactionModel.From,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanFrom = dateTimetoConvert.TimeOfDay;
            DateTime dateTimetoConvertTo = DateTime.ParseExact(bookingTableTransactionModel.To,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanTo = dateTimetoConvertTo.TimeOfDay;
            bookingTableTransaction.BookingTimeFrom = spanFrom;
            bookingTableTransaction.BookingTimeTo = spanTo;
            bookingTableTransaction.CreatedDt = DateTime.UtcNow;
            bookingTableTransaction.BookingDate = DateTime.Parse(bookingTableTransactionModel.DateOfBirthDay);
            bookingTableTransaction.CustomerName = bookingTableTransactionModel.CustomerName;
            bookingTableTransaction.Email = bookingTableTransactionModel.Email;
            bookingTableTransaction.MobileNumber = bookingTableTransactionModel.MobileNumber;
            bookingTableTransaction.TableNumber = bookingTableTransactionModel.TableNumber;
            bookingTableTransaction.Status = (int)BookingTableStatus.Booked;
            bookingTableTransaction.SeatingCapacity = bookingTableTransactionModel.Seats.ToString();
            bookingTableTransaction.SpecialNotes = bookingTableTransactionModel.SpecialNotes;
            _bookingTransactionrepository.Insert(bookingTableTransaction);
            BookingNote bookingNote = new BookingNote();
            bookingNote.BookingId = bookingTableTransaction.Id;
            //bookingNote.IsNotify = true;
            bookingNote.Note = "Table Booked Successfully for" + bookingTableTransaction.CustomerName;
            bookingNote.DownloadId = 0;
            bookingNote.DisplayToCustomer = true;
            bookingNote.CreatedOnUtc = DateTime.UtcNow;
            _bookingNoterepository.Insert(bookingNote);
            var emailId = _settings.Table.Where(x => x.Name.Contains("emailaccountsettings.defaultemailaccountid")).FirstOrDefault();
            if (emailId != null)
            {
                int Email = Convert.ToInt32(emailId.Value);

                var emailAccount = _emailAccountService.GetEmailAccountById(Email);
                var tokens = new List<Token>();
                _messageTokenProvider.AddStoreTokens(tokens, _storeContext.CurrentStore, emailAccount);
                var store = _storeService.GetStoreById(bookingTableTransaction.StoreId);
                if (store != null)
                {
                    _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
                }
                var vendorBody = _vendorService.GetVendorById(bookingTableTransaction.VendorId);
                if (vendorBody != null)
                {
                    _messageTokenProvider.AddVendorTokens(tokens, vendorBody);
                }

                var welcomeMessage = _messageTemplate.Table.Where(x => x.Name.ToString().Contains("Customer.Email.BookTable")).FirstOrDefault();

                var subject = _tokenizer.Replace(welcomeMessage.Subject, tokens, false);
                subject = subject.Replace("%Booking.Status%", "Booked");
                var body = _tokenizer.Replace(welcomeMessage.Body, tokens, true);
                body = body.Replace("%Booking.Status%", "Booked");
                body = body.Replace("%Customer.FullName%", bookingTableTransaction.CustomerName);
                _emailSender.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, bookingTableTransaction.Email, null);
                // _emailSender.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, bookingTableTransaction.Email, null);
            }
            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Configuration.EmailAccounts.SendTestEmail.Success"), false);
            return RedirectToAction("List");
        }
        [HttpPost]
        public virtual IActionResult Edit(BookingTableTransactionModel bookingTableTransactionModel)
        {
            var bookingTableTransaction = _bookingTransactionrepository.Table.Where(x => x.Id == bookingTableTransactionModel.Id).FirstOrDefault();
            IList<SelectListItem> AvailableTableforBookings = new List<SelectListItem>();
            if (_workContext.CurrentVendor != null)
            {
                bookingTableTransactionModel.SearchStoreId = _workContext.CurrentCustomer.RegisteredInStoreId;
            }
            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
            {
                bookingTableTransaction.StoreId = bookingTableTransactionModel.SearchStoreId;
                bookingTableTransaction.VendorId = bookingTableTransactionModel.SearchVendorId;

            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                bookingTableTransaction.StoreId = stores.Id;
                bookingTableTransaction.VendorId = bookingTableTransactionModel.SearchVendorId;
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
            {
                var vendor = _customerService.GetAllCustomers().FirstOrDefault(x => x.Id == _workContext.CurrentCustomer.Id);
                bookingTableTransaction.StoreId = vendor.RegisteredInStoreId;
                bookingTableTransaction.VendorId = vendor.VendorId;
            }
            DateTime dateTimetoConvert = DateTime.ParseExact(bookingTableTransactionModel.From,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanFrom = dateTimetoConvert.TimeOfDay;
            DateTime dateTimetoConvertTo = DateTime.ParseExact(bookingTableTransactionModel.To,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanTo = dateTimetoConvertTo.TimeOfDay;
            bookingTableTransaction.BookingTimeFrom = spanFrom;
            bookingTableTransaction.BookingTimeTo = spanTo;
            bookingTableTransaction.CreatedDt = DateTime.UtcNow;
            bookingTableTransaction.BookingDate = DateTime.Parse(bookingTableTransactionModel.DateOfBirthDay);
            bookingTableTransaction.CustomerName = bookingTableTransactionModel.CustomerName;
            bookingTableTransaction.Email = bookingTableTransactionModel.Email;
            bookingTableTransaction.MobileNumber = bookingTableTransactionModel.MobileNumber;
            bookingTableTransaction.TableNumber = bookingTableTransactionModel.TableNumber;
            bookingTableTransaction.Status = bookingTableTransactionModel.BookingStatus;
            bookingTableTransaction.SeatingCapacity = bookingTableTransactionModel.Seats.ToString();
            bookingTableTransaction.SpecialNotes = bookingTableTransactionModel.SpecialNotes;
            _bookingTransactionrepository.Update(bookingTableTransaction);
            return RedirectToAction("List");
        }
        [HttpPost]
        public virtual IActionResult BookingNoteDelete(int id, int bookingId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingTransaction))
                return AccessDeniedView();

            var order = _bookingTransactionrepository.GetById(bookingId);
            if (order == null)
                throw new ArgumentException("No Bookinf found with the specified id");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = bookingId });

            var orderNote = _bookingNoterepository.GetById(id);
            if (orderNote == null)
                throw new ArgumentException("No order note found with the specified id");
            _bookingNoterepository.Delete(orderNote);

            return new NullJsonResult();
        }

        public virtual IList<SelectListItem> GetAvailableVendors(string Store)
        {
            string select = "0";
            IList<SelectListItem> AvailableVendors = new List<SelectListItem>();
            if (Store == select)
            {

                AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
            }
            else
            {
                int storeId = Convert.ToInt32(Store);
                AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "" });
                var vandorslist = _vendorService.GetAllVendors();
                if (vandorslist != null && vandorslist.Any())
                {
                    foreach (var c in vandorslist)
                    {
                        if (c.StoreId == storeId)
                            AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
            }
            //var storeId = _workContext.CurrentCustomer.RegisteredInStoreId;

            return AvailableVendors;
        }
        #endregion

        public enum BookingTableStatus
        {
            Cancelled = 3,
            Booked = 1,
            Complete = 2
        }
        #endregion
    }
}