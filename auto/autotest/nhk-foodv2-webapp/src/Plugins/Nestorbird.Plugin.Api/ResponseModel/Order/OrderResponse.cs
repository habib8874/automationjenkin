﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Orders;

namespace Nestorbird.Plugin.Api.ResponseModel.Order
{
    public class OrderListResponse : PagingResponse
    {
        public OrderListResponse()
        {
            Orders = new List<OrderResponse>();
        }

        public IList<OrderResponse> Orders { get; set; }
    }

    public class OrderResponse
    {
        public int OrderId { get; set; }

        public string OrderGuid { get; set; }

        public string CustomOrderId { get; set; }

        public string OrderTotal { get; set; }

        public OrderStatus OrderStatusEnum { get; set; }

        public string OrderStatus { get; set; }

        public string PaymentStatus { get; set; }

        public string ShippingStatus { get; set; }

        public string CustomerName { get; set; }

        public string CurrencyCode { get; set; }

        public decimal OrderTotalAmount { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
