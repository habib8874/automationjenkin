﻿namespace Nestorbird.Plugin.Api.ResponseModel.Account
{
    public class LoginResponse
    {
        public int CustomerId { get; set; }

        public int VendorId { get; set; }

        public int LanguageId { get; set; }

        public string FullName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string ImageUrl { get; set; }
    }
}
