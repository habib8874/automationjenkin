﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Payments.Curlec
{
    /// <summary>
    /// Represents settings of the Curlec payment plugin
    /// </summary>
    public class CurlecPaymentSettings : ISettings
    {
        /// <summary>
        /// Gets or sets a merchant id
        /// </summary>
        public string MerchantID { get; set; }

        /// <summary>
        /// Gets or sets a employee id
        /// </summary>
        public string EmployeeID { get; set; }

        /// <summary>
        /// Gets or sets a merchant url
        /// </summary>
        public string MerchantURL { get; set; }

        /// <summary>
        /// Gets or sets a merchant callback url
        /// </summary>
        public string MerchantCallbackURL { get; set; }

        /// <summary>
        /// Gets or sets a collection callback url
        /// </summary>
        public string CollectionCallbackURL { get; set; }

        /// <summary>
        /// Gets or sets a enrollment callback url
        /// </summary>
        public string EnrollmentCallbackURL { get; set; }
    }
}
