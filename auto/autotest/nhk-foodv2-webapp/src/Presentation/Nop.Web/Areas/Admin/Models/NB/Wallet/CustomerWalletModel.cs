﻿using System;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB
{
    /// <summary>
    /// Represents a customer order model
    /// </summary>
    public partial class CustomerWalletModel : BaseNopEntityModel
    {
        #region Properties

        public int CustomerId { get; set; }
        public int? OrderId { get; set; }
        public Decimal Amount { get; set; }
        [NopResourceDisplayName("Admin.Customers.Customers.Wallets.DisplayAmount")]
        public string DisplayAmount { get; set; }
        [NopResourceDisplayName("Admin.Customers.Customers.Wallets.Description")]
        public string Note { get; set; }
        public bool Credited { get; set; }
        [NopResourceDisplayName("Admin.Customers.Customers.Wallets.CreatedOn")]
        public DateTime TransactionOnUtc { get; set; }
        public int TransactionBy { get; set; }
        public bool FromWeb { get; set; }

        #endregion
    }
}