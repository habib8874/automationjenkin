﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Stores;
using Nop.Services.Customers;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Framework.Models;

namespace Nop.Web.Framework.Factories
{
    /// <summary>
    /// Represents the base store mapping supported model factory implementation
    /// </summary>
    public partial class StoreMappingSupportedModelFactory : IStoreMappingSupportedModelFactory
    {
        #region Fields

        private readonly IStoreMappingService _storeMappingService;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly IVendorService _vendorService;

        #endregion

        #region Ctor

        public StoreMappingSupportedModelFactory(IStoreMappingService storeMappingService,
            IStoreService storeService,
            IWorkContext workContext,
            ICustomerService customerService,
            IVendorService vendorService)
        {
            _storeMappingService = storeMappingService;
            _storeService = storeService;
            _workContext = workContext;
            _customerService = customerService;
            _vendorService = vendorService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare selected and all available stores for the passed model
        /// </summary>
        /// <typeparam name="TModel">Store mapping supported model type</typeparam>
        /// <param name="model">Model</param>
        public virtual void PrepareModelStores<TModel>(TModel model) where TModel : IStoreMappingSupportedModel
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            //prepare available stores
            int storeIdReg = 0;
            if (_workContext.IsStoreOwnerRole || _workContext.IsVendorRole)
            {
                storeIdReg = _workContext.GetCurrentStoreId;
            }

            //sub admin
            var subAdminStoreIds = new List<int>();
            var subAdmin = _workContext.SubAdmin;
            if (subAdmin.IsSubAdminRole)
            {
                if (subAdmin.AssociatedVenorIds.Any())
                {
                    var vendors = _vendorService.GetVendorsByIds(subAdmin.AssociatedVenorIds?.ToArray());
                    if (vendors.Any())
                        subAdminStoreIds.AddRange(vendors.Select(v => v.StoreId));
                }
            }

            //prepare available stores
            var availableStores = _storeService.GetAllStores();
            foreach (var store in availableStores)
            {
                if (!subAdmin.IsSubAdminRole)
                {
                    if (storeIdReg == 0 || store.Id == storeIdReg)
                    {
                        model.AvailableStores.Add(new SelectListItem
                        {
                            Text = store.Name,
                            Value = store.Id.ToString(),
                            Selected = model.SelectedStoreIds.Contains(store.Id)
                        });
                    }
                }
                else
                {
                    if (subAdminStoreIds.Contains(store.Id))
                    {
                        model.AvailableStores.Add(new SelectListItem
                        {
                            Text = store.Name,
                            Value = store.Id.ToString(),
                            Selected = model.SelectedStoreIds.Contains(store.Id)
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Prepare selected and all available stores for the passed model by store mappings
        /// </summary>
        /// <typeparam name="TModel">Store mapping supported model type</typeparam>
        /// <typeparam name="TEntity">Store mapping supported entity type</typeparam>
        /// <param name="model">Model</param>
        /// <param name="entity">Entity</param>
        /// <param name="ignoreStoreMappings">Whether to ignore existing store mappings</param>
        public virtual void PrepareModelStores<TModel, TEntity>(TModel model, TEntity entity, bool ignoreStoreMappings)
            where TModel : IStoreMappingSupportedModel where TEntity : BaseEntity, IStoreMappingSupported
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            //prepare stores with granted access
            if (!ignoreStoreMappings && entity != null)
                model.SelectedStoreIds = _storeMappingService.GetStoresIdsWithAccess(entity).ToList();

            PrepareModelStores(model);
        }

        #endregion
    }
}