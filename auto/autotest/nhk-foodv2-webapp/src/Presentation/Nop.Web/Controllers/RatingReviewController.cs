﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.RatingReview;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.RatingReview;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Topics;
using Nop.Web.Factories;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.Themes;
using Nop.Web.Models.RatingReview;
using System;
using System.Collections.Generic;

namespace Nop.Web.Controllers
{
    public partial class RatingReviewController : BasePublicController
    {
        #region Fields

        private readonly IRatingReviewService _ratingReviewService;
        private readonly IRatingReviewFactory _ratingReviewFactory;
        private readonly ITopicService _topicService;
        private readonly ILocalizationService _localizationService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IAclService _aclService;
        private readonly IPermissionService _permissionService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingService;
        private readonly IOrderService _orderService;
        private readonly IThemeContext _themeContext;

        #endregion

        #region Ctor

        public RatingReviewController(IRatingReviewService ratingReviewService,
            IRatingReviewFactory ratingReviewFactory,
            ITopicService topicService,
            ILocalizationService localizationService,
            IStoreMappingService storeMappingService,
            IAclService aclService,
            IPermissionService permissionService,
            IWorkContext workContext,
            IStoreContext storeContext,
            ISettingService settingService,
            IOrderService orderService,
            IThemeContext themeContext)
        {
            this._ratingReviewService = ratingReviewService;
            this._ratingReviewFactory = ratingReviewFactory;
            this._topicService = topicService;
            this._localizationService = localizationService;
            this._storeMappingService = storeMappingService;
            this._aclService = aclService;
            this._permissionService = permissionService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._settingService = settingService;
            this._orderService = orderService;
            this._themeContext = themeContext;
        }

        #endregion

        #region Methods
        public virtual string LoadRatingReview(int orderId, bool skipNotInterestedCheck = false, int directItemId = 0)
        {
            var viewPath = "~/Themes/" + _themeContext.WorkingThemeName.ToString() + "/Views/RatingReview/_LoadRatingReview.cshtml";

            var model = new RatingReviewMainModel();
            model.OrderId = orderId;
            model.SkipNotInterestedCheck = skipNotInterestedCheck;
            model.DirectItemId = directItemId;

            return RenderPartialViewToString(viewPath, model);
        }
        public virtual IActionResult SaveRatingReview(RatingReviewModel model)
        {
            try
            {
                int storeId = _workContext.GetCurrentStoreId;

                RatingReviews objEntity = new RatingReviews();
                objEntity.CustomerId = _workContext.CurrentCustomer.Id;
                objEntity.EntityId = model.EntityId;
                objEntity.OrderId = model.OrderId;
                objEntity.ReviewType = model.ReviewType;
                objEntity.StoreId = storeId;
                if (model.ReviewType == (int)ReviewType.Agent)
                {
                    objEntity.IsApproved = _settingService.GetSettingByKey<bool>("ratingreviewsettings.agent.isapproved", true, _workContext.GetCurrentStoreId, true);
                    objEntity.Title = "Agent";
                }
                else if (model.ReviewType == (int)ReviewType.Merchant)
                {
                    objEntity.IsApproved = _settingService.GetSettingByKey<bool>("ratingreviewsettings.merchant.isapproved", true, _workContext.GetCurrentStoreId, true);
                    objEntity.Title = "Merchant";
                }
                else if (model.ReviewType == (int)ReviewType.Item)
                {
                    objEntity.IsApproved = _settingService.GetSettingByKey<bool>("ratingreviewsettings.item.isapproved", true, _workContext.GetCurrentStoreId, true);
                    objEntity.Title = model.ReviewTitle;
                }

                objEntity.ReviewText = model.ReviewText;
                //objEntity.Rating = model.IsLiked ? 5 : 0;
                objEntity.Rating = model.Rating;
                objEntity.ReviewOptions = model.SelectedReviewOptions;
                objEntity.DislikedReviewOptions = model.DisLikedReviewOptions;
                objEntity.IsLiked = model.IsLiked;
                objEntity.IsNotInterested = model.IsNotInterested;
                objEntity.IsSkipped = model.IsSkipped;
                objEntity.CreatedOnUtc = DateTime.UtcNow;

                //save review
                _ratingReviewService.InsertRatingReviews(objEntity);

                //if not interested
                if (model.IsNotInterested)
                {
                    return Json(new
                    {
                        success = true,
                        html = string.Empty
                    });
                }

                var order = _orderService.GetOrderById(model.OrderId);
                if (model.ReviewType == (int)ReviewType.Agent)
                {
                    model = _ratingReviewFactory.PrepareRatingReviewModel((int)ReviewType.Merchant, order);
                }
                else if (model.ReviewType == (int)ReviewType.Merchant)
                {
                    model = _ratingReviewFactory.PrepareRatingReviewModel((int)ReviewType.Item, order);
                }

                var viewPath = "~/Themes/" + _themeContext.WorkingThemeName.ToString() + "/Views/Shared/Components/RatingReview/Default.cshtml";

                //To handle issue, when item review already submitted before Agent & Merchant review.
                var isActiveItem = _settingService.GetSettingByKey<bool>("ratingreviewsettings.item.isactive", true, storeId, true);
                if ((model == null) || (model.ReviewType == (int)ReviewType.Item && !isActiveItem) ||
                    (model.ReviewType == (int)ReviewType.Item && model.Items?.Count == 0) ||
                    (model.ReviewType == (int)ReviewType.Merchant && _ratingReviewService.IsMerchantReviewEnabledAndFound(order.Id)))
                {
                    return Json(new
                    {
                        success = true,
                        html = ""
                    });
                }

                return Json(new
                {
                    success = true,
                    html = ((model != null) ? RenderPartialViewToString(viewPath, model) : "")
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    html = string.Empty
                });
            }
        }

        public virtual IActionResult SaveItemRatingReview(IFormCollection form)
        {
            try
            {
                int storeId = _workContext.GetCurrentStoreId;
                foreach (var formValue in form.Keys)
                {
                    int itemId = 0;
                    if (formValue.StartsWith("itemId-", StringComparison.InvariantCultureIgnoreCase))
                    {
                        itemId = Convert.ToInt32(formValue.Substring("itemId-".Length));
                        RatingReviews objEntity = new RatingReviews();
                        objEntity.CustomerId = _workContext.CurrentCustomer.Id;
                        objEntity.EntityId = itemId;
                        objEntity.OrderId = Convert.ToInt32(form["OrderId"]);
                        objEntity.ReviewType = Convert.ToInt32(form["ReviewType"]);
                        objEntity.StoreId = storeId;
                        objEntity.IsApproved = _settingService.GetSettingByKey<bool>("ratingreviewsettings.item.isapproved", true, _workContext.GetCurrentStoreId, true);
                        objEntity.Title = form["ReviewTitle"];
                        objEntity.ReviewText = form["ReviewText"];
                        string ratingField = "rating-" + itemId;
                        objEntity.Rating = Convert.ToInt32(form[ratingField]);
                        objEntity.ReviewOptions = string.Empty;
                        objEntity.IsLiked = true;
                        objEntity.IsNotInterested = false;
                        objEntity.IsSkipped = false;
                        objEntity.CreatedOnUtc = DateTime.UtcNow;

                        //save review
                        _ratingReviewService.InsertRatingReviews(objEntity);

                    }
                }
                var model = new RatingReviewModel();
                model.IsReviewSubmitted = true;

                //prepare model for next item
                var order = _orderService.GetOrderById(Convert.ToInt32(form["OrderId"]));
                model = _ratingReviewFactory.PrepareRatingReviewModel((int)ReviewType.Item, order);
                var allItemReviewSubmited = model.Items.Count == 0;
                var allreviewsubmitted = _ratingReviewService.AllReviewFound(order.Id);

                var viewPath = "~/Themes/" + _themeContext.WorkingThemeName.ToString() + "/Views/Shared/Components/RatingReview/Default.cshtml";
                return Json(new
                {
                    success = true,
                    allreviewsubmitted,
                    allItemReviewSubmited,
                    html = ((model != null) ? RenderPartialViewToString(viewPath, model) : "")
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    html = string.Empty
                });
            }
        }

        #endregion
    }
}