﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Web.Infrastructure.Cache;
using System.Collections.Generic;
using Nop.Services.Configuration;

namespace Nop.Web.Components
{
    public class HomepageBestSellersViewComponent : NopViewComponent
    {
        private readonly CatalogSettings _catalogSettings;
        private readonly IAclService _aclService;
        private readonly IOrderReportService _orderReportService;
        private readonly IProductModelFactory _productModelFactory;
        private readonly IProductService _productService;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IStoreContext _storeContext;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IWorkContext _workContext;

        public HomepageBestSellersViewComponent(CatalogSettings catalogSettings,
            IAclService aclService,
            IOrderReportService orderReportService,
            IProductModelFactory productModelFactory,
            IProductService productService,
            IStaticCacheManager cacheManager,
            IStoreContext storeContext,
            IStoreMappingService storeMappingService,
            IWorkContext workContext)
        {
            _catalogSettings = catalogSettings;
            _aclService = aclService;
            _orderReportService = orderReportService;
            _productModelFactory = productModelFactory;
            _productService = productService;
            _cacheManager = cacheManager;
            _storeContext = storeContext;
            _storeMappingService = storeMappingService;
            _workContext = workContext;
        }

        public IViewComponentResult Invoke(int? productThumbPictureSize)
        {
                if (!_catalogSettings.ShowBestsellersOnHomepage || _catalogSettings.NumberOfBestsellersOnHomepage == 0)
                return Content("");

            //load and cache report
            var bestsellerPrd = _cacheManager.Get(string.Format(NopModelCacheDefaults.HomepageBestsellersIdsKey, _storeContext.CurrentStore.Id),
                () => _orderReportService.BestSellersReport(
                        storeId: _workContext.GetCurrentStoreId,
                        pageSize: _catalogSettings.NumberOfBestsellersOnHomepage)
                    .ToList());

            //load products
            string merchantid = (_workContext.CurrentMerchant?.Id??-1).ToString();
            var products = _productService.GetAllProductsDisplayedOnHomePageMerchant(merchantid,keywords : "",selectedImageTags : "");
            if (!products.Any())
                return Content("");

            products = (from p in products join jn in bestsellerPrd on p.Id equals jn.ProductId orderby jn.TotalQuantity descending select p).ToList();


            //prepare model
            //var model = _productModelFactory.PrepareProductOverviewModels(products, true, true, productThumbPictureSize).ToList();
            var model = _productModelFactory.PrepareProductOverviewModelsMerchant(products, _workContext.GetCurrentStoreId, true, true, productThumbPictureSize, prepareSpecificationAttributes: true).ToList();

            return View(model);
        }
    }
}