﻿using Microsoft.AspNetCore.Mvc;
using NHKCustomerApplication.Caching;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModel;
using NHKCustomerApplication.ViewModels;
using System;
using System.Net;

namespace NHKCustomerApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CacheManagerController : Controller
    {
        private ecuadordevContext dbcontext;
        private readonly ICacheManager _cacheManager;

        public CacheManagerController(ecuadordevContext context, ICacheManager cacheManager)
        {
            dbcontext = context;
            this._cacheManager = cacheManager;
        }

        [Route("~/api/v1/ClearCache")]
        [HttpPost]

        public BaseAPIResponseModel GetAndClearCaches(ClearCaches requestmodel)
        {
            int languageId = LanguageHelper.GetIdByLangCode(requestmodel.StoreId, requestmodel.UniqueSeoCode, dbcontext);

            if (requestmodel == null)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestmodel.StoreId, "NB.API.ErrorMesaage.ModelNotDefined", languageId, dbcontext));
          
            else if (requestmodel.StoreId == 0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestmodel.StoreId, "NB.API.GetAllStoreLanguages.StoreNotFound", languageId, dbcontext));
 
            try
            {
                if(string.IsNullOrEmpty(requestmodel.CacheKeyPrefix))
                {
                     _cacheManager.Clear();
                }
                else
                {
                     _cacheManager.RemoveByPrefix(requestmodel.CacheKeyPrefix);
                }
               return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestmodel.StoreId, "NB.API.GetAllStoreLanguages.CacheRemovedSuccessFully", languageId, dbcontext));
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                return BaseMethodsHelper.ErrorResponse(ex.Message, HttpStatusCode.BadRequest);

            }
        }

    }
}
