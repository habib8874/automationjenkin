﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Vendors
{
    /// <summary>
    /// Represents the customer registration type formatting enumeration
    /// </summary>
    public enum AvailableTypeEnum
    {
        /// <summary>
        /// Custom
        /// </summary>
        Custom = 1,
        /// <summary>
        /// All Day
        /// </summary>
        AllDay = 2,
    }
    public enum AvailableDaysEnum
    {
        /// <summary>
        /// Monday
        /// </summary>
        Monday = 1,
        /// <summary>
        /// Tuesday
        /// </summary>
        Tuesday = 2,
        /// <summary>
        /// Wednesday
        /// </summary>
        Wednesday = 3,
        /// <summary>
        /// Thursday
        /// </summary>
        Thursday = 4,
        /// <summary>
        /// Friday
        /// </summary>
        Friday = 5,
        /// <summary>
        /// Saturday
        /// </summary>
        Saturday = 6,
        /// <summary>
        /// Sunday
        /// </summary>
        Sunday =0    

    }
}
