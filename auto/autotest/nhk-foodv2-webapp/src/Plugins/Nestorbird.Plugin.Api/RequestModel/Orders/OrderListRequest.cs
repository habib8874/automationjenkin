﻿using System;
using System.Collections.Generic;

namespace Nestorbird.Plugin.Api.RequestModel.Orders
{
    public class OrderListRequest : PagingRequest
    {
        public OrderListRequest()
        {
            OrderStatusIds = new List<int>();
            PaymentStatusIds = new List<int>();
            ShippingStatusIds = new List<int>();
        }

        public int VendorId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public IList<int> OrderStatusIds { get; set; }

        public IList<int> PaymentStatusIds { get; set; }

        public IList<int> ShippingStatusIds { get; set; }
    }
}
