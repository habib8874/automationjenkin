﻿namespace Nestorbird.Plugin.Api.ResponseModel.Language
{
    public class LanguageResponse
    {
        public int LanguageId { get; set; }

        public string Name { get; set; }
    }

    public class LocalizedResourcesResponse
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
