﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;

namespace Nop.Web.Components
{
    public class ProductfiltrationViewComponent : NopViewComponent
    {
        #region Fields

        private readonly ICatalogModelFactory _catalogModelFactory;

        #endregion

        #region Ctor
        public ProductfiltrationViewComponent(ICatalogModelFactory catalogModelFactory)
        {
            _catalogModelFactory = catalogModelFactory;
        }

        #endregion

        #region Methods
        public IViewComponentResult Invoke()
        {
            var model = _catalogModelFactory.PrepareProductFiltrationAttributesModel();
            
            return View(model);    
        }
        #endregion
    }
}
