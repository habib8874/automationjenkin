﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB;

namespace Nop.Data.Mapping.NB
{
    /// <summary>
    /// Represents a DeliverySlotBooking configuration
    /// </summary>
    public partial class DeliverySlotBookingMap : NopEntityTypeConfiguration<DeliverySlotBooking>
    {
        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<DeliverySlotBooking> builder)
        {
            builder.ToTable(NopMappingDefaults.DeliverySlotBookingTable);
            builder.HasKey(pc => pc.Id);
            base.Configure(builder);
        }
    }
}
