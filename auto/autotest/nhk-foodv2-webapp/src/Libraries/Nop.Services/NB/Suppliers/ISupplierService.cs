﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.NB.Suppliers;

namespace Nop.Services.NB.Suppliers
{
    public interface ISupplierService
    {


        /// <summary>
        /// Check Geofancing Format
        /// </summary>
        /// <param name="geofancing">Supplier</param>
        /// <summary>
        bool CheckGeofancingFormat(string geofancing);

        /// <summary>
        /// Delete a Supplier
        /// </summary>
        /// <param name="supplier">Supplier</param>
        /// <summary>
        void DeleteSupplier(Supplier supplier);

        /// <summary>
        /// Get All Supplier
        /// </summary>
        /// <param name="supplier">Supplier</param>
        /// <summary>
        IPagedList<Supplier> GetAllSupplier(string name = "", int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, int storeId = 0);

        /// <summary>
        /// Get Supplier By Id
        /// </summary>
        /// <param name="supplier">Supplier</param>
        /// <summary>
        Supplier GetSupplierById(int id);

        /// <summary>
        /// Get Supplier By Ids
        /// </summary>
        /// <param name="supplier">Supplier</param>
        /// <summary>
        IList<Supplier> GetSupplierByIds(int[] supplierIDs);

        /// <summary>
        /// Inserts a supplier
        /// </summary>
        /// <param name="supplier">Supplier</param>
        /// <summary>
        void InsertSupplier(Supplier supplier);
        /// <summary>
        /// Update a supplier
        /// </summary>
        /// <param name="supplier">Supplier</param>
        /// <summary>
        void UpdateSupplier(Supplier supplier);

        /// <summary>
        /// Delete a supplier
        /// </summary>
        /// <param name="supplier">Supplier</param>
        /// <summary>
        void DeleteSupplier1(Supplier supplier);
        /// <summary>
        /// Update a ProductSupplier
        /// </summary>
        /// <param name="productsupplier">ProductSupplier</param>
        /// <summary>

        void UpdateProductSupplier(ProductSupplier productSupplier);

        /// <summary>
        /// Delete a ProductSupplier
        /// </summary>
        /// <param name="productsupplier">ProductSupplier</param>
        /// <summary>
        void DeleteProductSupplier(ProductSupplier productSupplier);

        /// <summary>
        /// Insert a ProductSupplier
        /// </summary>
        /// <param name="productsupplier">ProductSupplier</param>
        /// <summary>
        void InsertProductSupplier(ProductSupplier productSupplier);

        /// <summary>
        /// Delete a suppliers
        /// </summary>
        /// <param name="supplierIds">Suppliers</param>
        ///  <summary>
        void DeleteSuppliers(ICollection<int> supplierIds);

        /// <summary>
        /// Updates the suppliers
        /// </summary>
        /// <param name="suppliers">Suppliers</param>
        ///  /// <summary>
        void UpdateSuppliers(IList<Supplier> suppliers);

        /// <summary>
        /// Get Suppliers By Id
        /// </summary>
        /// <param name="supplierId">Supplier</param>
        /// <summary>
        Supplier GetSuppliersById(int supplierId);
        /// <summary>
        /// Get ProductSupplier By Id
        /// </summary>
        /// <param name="productsupplier">ProductSupplier</param>
        /// <summary>
        ProductSupplier GetProductSupplierById(int productSupplierId);
        /// <summary>
        /// Find a ProductSupplier
        /// </summary>
        /// <param name="productsupplier">ProductSupplier</param>
        /// <summary>

        ProductSupplier FindProductSupplier(IList<ProductSupplier> source, int productId, int supplierId);

        /// <summary>
        /// Gets all suppliers by store
        /// </summary>
        /// <param name="name">Supplier name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Suppliers</returns>
        IPagedList<Supplier> GetAllSupplierByStore(List<int> storeids, string name = "",
           int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        /// <summary>
        /// Gets all suppliers by store
        /// </summary>
        /// <param name="name">Supplier name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>suppliers</returns>
        IPagedList<ProductSupplier> GetProductSuppliersBySupplierId(int supplierId,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        /// <summary>
        ///Get All Suppliers Vendorwise
        /// </summary>
        /// <param name="name">Vendor name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Suppliers</returns>
        IPagedList<Supplier> GetAllSuppliersVendorwise(string supplierName = "", int storeId = 0, int vendorId = 0,
           int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        /// <summary>
        /// Get Supplier BreadCrumb
        /// </summary>
        /// <param name="name">Vendor name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Supplierd</returns>
        IList<Supplier> GetSupplierBreadCrumb(Supplier supplier, IList<Supplier> allSuppliers = null, bool showHidden = false);


        /// <summary>
        /// Get Formatted BreadCrumb
        /// </summary>
        /// <param name="name">Vendor name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Supplierd</returns>

        string GetFormattedBreadCrumb(Supplier supplier, IList<Supplier> allSuppliers = null,
            string separator = ">>", int languageId = 0);

        /// <summary>
        /// get the stores by id
        /// </summary>
        /// <param name="storeId">Suppliers</param>
        ///  /// <summary>
        ///  
        IList<Supplier> GetSupplierByStoreId(int storeId, int languageId = 0, bool showHidden = false);
    }
}