﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    public class OrderVendorDetails 
    {
        public string MerchantName { get; set; }
        public string PickupLocation { get; set; }
        public string PickupLat { get; set; }
        public string PickupLong { get; set; }
        public string PickupMobileNo { get; set; }
        public decimal OrderTotal { get; set; }
        public string ISDCode { get; set; }
    }

    public class OrderCustomerDetails
    {
        public string CustomerName { get; set; }
        public string CustomerLocation { get; set; }
        public string DropLat { get; set; }
        public string DropLong { get; set; }
        public string CustomerMobileNo { get; set; }
    }
}
