﻿namespace NHKCustomerApplication.Utilities
{
    public enum GenderEnum
    {
        Male=1,
        Female=2
    }
    public enum RestType
    {
        Dining=3,
        TakeAway=2,
        Delivery=1
    }
    public enum BookingTableStatus
    {
        Cancelled = 3,
        Booked = 1,
        Complete = 2
    }
    public enum HashAlgorithmv2
    {
        SHA1,
        SHA512
    }
    public enum AttributeValueType
    {
        /// <summary>
        /// Simple attribute value
        /// </summary>
        Simple = 0,
        /// <summary>
        /// Associated to a product (used when configuring bundled products)
        /// </summary>
        AssociatedToProduct = 10,
    }
    public enum PaymentMethodEnum
    {
        PayPal = 1,
        Razorpay = 2,
        Card = 3,
        COD = 4,
        Square = 5
    }
    public enum ProductAttributeEnum
    {
        DropDown = 1,
        Radiobutton = 2,
        Checkboxes = 3
    }
    public enum AvailableTypeEnum
    {
        /// <summary>
        /// Custom
        /// </summary>
        Custom = 1,
        /// <summary>
        /// All Day
        /// </summary>
        AllDay = 2,
    }
    public enum AvailableDaysEnum
    {
        /// <summary>
        /// Monday
        /// </summary>
        Monday = 1,
        /// <summary>
        /// Tuesday
        /// </summary>
        Tuesday = 2,
        /// <summary>
        /// Wednesday
        /// </summary>
        Wednesday = 3,
        /// <summary>
        /// Thursday
        /// </summary>
        Thursday = 4,
        /// <summary>
        /// Friday
        /// </summary>
        Friday = 5,
        /// <summary>
        /// Saturday
        /// </summary>
        Saturday = 6,
        /// <summary>
        /// Sunday
        /// </summary>
        Sunday = 0,
        /// <summary>
        /// AllDay
        /// </summary>
        AllDay = 8,

    }
}