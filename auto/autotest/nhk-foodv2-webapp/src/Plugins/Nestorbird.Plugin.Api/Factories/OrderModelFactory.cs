﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nestorbird.Plugin.Api.RequestModel.Orders;
using Nestorbird.Plugin.Api.ResponseModel.Account;
using Nestorbird.Plugin.Api.ResponseModel.Order;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Vendors;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;

namespace Nestorbird.Plugin.Api.Factories
{
    public class OrderModelFactory : IOrderModelFactory
    {
        #region Fields

        private readonly IRepository<Address> _addressRepository;
        private readonly IRepository<Vendor> _vendorRespository;
        private readonly IRepository<Order> _orderRespository;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICustomerService _customerService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderService _orderService;
        private readonly ICurrencyService _currencyService;
        private readonly ILocalizationService _localizationService;
        private readonly ICityService _cityService;
        private readonly IPaymentPluginManager _paymentPluginManager;
        private readonly IPictureService _pictureService;

        #endregion

        #region Ctor
        public OrderModelFactory(IRepository<Address> addressRepository,
            IRepository<Vendor> vendorRespository,
            IRepository<Order> orderRespository, 
            IDateTimeHelper dateTimeHelper, 
            ICustomerService customerService, 
            IPriceFormatter priceFormatter,
            IOrderService orderService,
            ICurrencyService currencyService,
            ILocalizationService localizationService,
            ICityService cityService,
            IPaymentPluginManager paymentPluginManager,
            IPictureService pictureService)
        {
            _addressRepository = addressRepository;
            _vendorRespository = vendorRespository;
            _orderRespository = orderRespository;
            _dateTimeHelper = dateTimeHelper;
            _customerService = customerService;
            _priceFormatter = priceFormatter;
            _orderService = orderService;
            _currencyService = currencyService;
            _localizationService = localizationService;
            _cityService = cityService;
            _paymentPluginManager = paymentPluginManager;
            _pictureService = pictureService;
        }

        #endregion

        #region Methods       

        /// <summary>
        /// Get orders
        /// </summary>
        /// <param name="request">request</param>
        /// <returns></returns>
        public virtual OrderListResponse GetOrders(OrderListRequest request, Language currentLanguage)
        {
            var orderStatusIds = (request.OrderStatusIds?.Contains(0) ?? true) ? null : request.OrderStatusIds.ToList();
            var paymentStatusIds = (request.PaymentStatusIds?.Contains(0) ?? true) ? null : request.PaymentStatusIds.ToList();
            var shippingStatusIds = (request.ShippingStatusIds?.Contains(0) ?? true) ? null : request.ShippingStatusIds.ToList();
            var startDateValue = !request.StartDate.HasValue ? null
              : (DateTime?)_dateTimeHelper.ConvertToUtcTime(request.StartDate.Value, _dateTimeHelper.CurrentTimeZone);
            var endDateValue = !request.EndDate.HasValue ? null
                : (DateTime?)_dateTimeHelper.ConvertToUtcTime(request.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            var orders = _orderService.SearchOrders(vendorId: request.VendorId,
                createdFromUtc: startDateValue,
                createdToUtc: endDateValue,
                osIds: orderStatusIds,
                psIds: paymentStatusIds,
                ssIds: shippingStatusIds,
                pageIndex: request.PageIndex,
                pageSize: request.PageSize);

            var orderList = orders.Select(order =>
            {
                var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
                var orderTotal = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false,
                    currentLanguage);

                return new OrderResponse
                {
                    CustomerName = _customerService.GetCustomerFullName(order.Customer),
                    OrderGuid = order.OrderGuid.ToString(),
                    CustomOrderId = order.CustomOrderNumber,
                    OrderId = order.Id,
                    OrderStatusEnum = order.OrderStatus,
                    OrderStatus = _localizationService.GetLocalizedEnum(order.OrderStatus, currentLanguage.Id),
                    PaymentStatus = _localizationService.GetLocalizedEnum(order.PaymentStatus, currentLanguage.Id),
                    ShippingStatus = _localizationService.GetLocalizedEnum(order.ShippingStatus, currentLanguage.Id),
                    OrderTotal = orderTotal,
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc),
                    CurrencyCode = order.CustomerCurrencyCode,
                    OrderTotalAmount = orderTotalInCustomerCurrency
                };
            }).ToList();

            var response = new OrderListResponse
            {
                Orders = orderList,
                PageIndex = orders.PageIndex,
                PageSize = orders.PageSize,
                TotalCount = orders.TotalCount,
                TotalPages = orders.TotalPages
            };

            return response;
        }

        /// <summary>
        /// Get order notes
        /// </summary>
        /// <param name="order">order</param>
        /// <returns>order notes</returns>
        public virtual IList<OrderNoteResponse> GetOrderNotes(Order order)
        {
            var notes = order.OrderNotes;
            var response = notes.Select(note =>
            {
                return new OrderNoteResponse
                {
                    Note = note.Note,
                    DisplayToCustomer = note.DisplayToCustomer,
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc)
                };
            }).ToList();
            return response;
        }

        /// <summary>
        /// Get order details
        /// </summary>
        /// <param name="order">order</param>
        /// <param name="currentLanguage">current language</param>
        /// <returns></returns>
        public virtual OrderDetailResponse GetOrderDetails(Order order, Language currentLanguage)
        {
            var customer = order.Customer;
            var billingAddress = order.BillingAddress;
            var shippingAddress = order.ShippingAddress;
            var orderItems = order.OrderItems;

            var assignedAgent = _customerService.GetAssignedAgentByOrderId(order.Id);
            var response = new OrderDetailResponse
            {
                CustomerName = _customerService.GetCustomerFullName(customer),
                OrderId = order.Id,
                OrderGuid = order.OrderGuid.ToString(),
                CustomOrderId = order.CustomOrderNumber,
                CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc),
                OrderStatusEnum = order.OrderStatus,
                OrderStatus = _localizationService.GetLocalizedEnum(order.OrderStatus, currentLanguage.Id),
                PaymentStatus = _localizationService.GetLocalizedEnum(order.PaymentStatus, currentLanguage.Id),
                ShippingStatus = _localizationService.GetLocalizedEnum(order.ShippingStatus, currentLanguage.Id),
                Tax = _priceFormatter.FormatPrice(order.OrderTax, true, false),
                Discount = _priceFormatter.FormatPrice(-order.OrderDiscount, true, false),
                OrderTotal = _priceFormatter.FormatPrice(order.OrderTotal, true, false),
                SubTotal = _priceFormatter.FormatPrice(order.OrderSubtotalExclTax, true, false),
                ShippingMethod = order.ShippingMethod,
                CurrencyCode = order.CustomerCurrencyCode,
                OrderTotalAmount = order.OrderTotal,
                DeliveryCharges = _priceFormatter.FormatPrice(order.DeliveryAmount, true, false),
                ShippingCharges = _priceFormatter.FormatPrice(order.OrderShippingExclTax, true, false),
                ServiceCharges = _priceFormatter.FormatPrice(order.ServiceChargeAmount, true, false),
            };

            var details = order.GetOrderDetails();
            if (details != null && details.OrderType != null)
            {
                response.OrderTypeId = details.OrderType;
                response.OrderType = (details.OrderType == 1) ? _localizationService.GetResource("Delivery") : _localizationService.GetResource("Takeaway");
            }

            if (assignedAgent != null)
            {
                response.IsAgentAssigned = true;
                response.AgentId = assignedAgent.AgentId;
                response.AgentStatus = ((AgentStatus)assignedAgent.OrderStatus).ToString();
                response.AgentStatusEnum = (AgentStatus)assignedAgent.OrderStatus;
                response.AgentName = _customerService.GetCustomerById(assignedAgent.AgentId)?.GetFullName();
                response.AgentCityId = assignedAgent.CityId;
                response.AgentCityName = _cityService.GetCityById(assignedAgent.CityId)?.Name;
            }
            else
            {
                response.IsAgentAssigned = false;
            }

            //payment method
            var paymentMethod = _paymentPluginManager.LoadPluginBySystemName(order.PaymentMethodSystemName);
            response.PaymentMethod = paymentMethod != null ? _localizationService.GetLocalizedFriendlyName(paymentMethod,
                currentLanguage.Id) : order.PaymentMethodSystemName;

            if (billingAddress != null)
            {
                response.BillingAddress = new AddressResponse
                {
                    Address1 = billingAddress.Address1,
                    Address2 = billingAddress.Address2,
                    City = billingAddress.City,
                    Company = billingAddress.Company,
                    Country = billingAddress.Country?.Name,
                    County = billingAddress.County,
                    CreatedOnUtc = billingAddress.CreatedOnUtc,
                    Email = billingAddress.Email,
                    FaxNumber = billingAddress.FaxNumber,
                    FirstName = billingAddress.FirstName,
                    LastName = billingAddress.LastName,
                    PhoneNumber = billingAddress.PhoneNumber,
                    StateProvince = billingAddress.StateProvince?.Name,
                    ZipPostalCode = billingAddress.ZipPostalCode
                };
            }

            if (shippingAddress != null)
            {
                response.ShippingAddress = new AddressResponse
                {
                    Address1 = shippingAddress.Address1,
                    Address2 = shippingAddress.Address2,
                    City = shippingAddress.City,
                    Company = shippingAddress.Company,
                    Country = shippingAddress.Country?.Name,
                    County = shippingAddress.County,
                    CreatedOnUtc = shippingAddress.CreatedOnUtc,
                    Email = shippingAddress.Email,
                    FaxNumber = shippingAddress.FaxNumber,
                    FirstName = shippingAddress.FirstName,
                    LastName = shippingAddress.LastName,
                    PhoneNumber = shippingAddress.PhoneNumber,
                    StateProvince = shippingAddress.StateProvince?.Name,
                    ZipPostalCode = shippingAddress.ZipPostalCode
                };
            }

            response.OrderItems = orderItems.Select(orderItem =>
            {
                var defaultPicture = orderItem.Product.ProductPictures.FirstOrDefault();
                var pictureUrl = defaultPicture != null ? _pictureService.GetPictureUrl(defaultPicture.PictureId, 100)
                                                        : _pictureService.GetDefaultPictureUrl(100);

                return new OrderItemResponse
                {
                    Id = orderItem.ProductId,
                    Name = _localizationService.GetLocalized(orderItem.Product, x => x.Name, currentLanguage.Id),
                    Qty = orderItem.Quantity,
                    UnitTotalPrice = _priceFormatter.FormatPrice(orderItem.UnitPriceExclTax, true, false),
                    TotalPrice = _priceFormatter.FormatPrice(orderItem.PriceExclTax, true, false),
                    Image = pictureUrl
                };
            }).ToList();

            return response;
        }

        /// <summary>
        /// Get notifications
        /// </summary>
        /// <param name="vendorId">vendor id</param>
        /// <returns></returns>
        public virtual IList<NotificationResponse> GetNotifications(int vendorId)
        {
            var model = new List<NotificationResponse>();

            var orderlist = (from or in _orderRespository.Table
                             join ven in _vendorRespository.Table
                             on or.BillingAddressId equals ven.AddressId
                             where ven.Id == vendorId
                             select or).ToList();
            foreach (var item in orderlist)
            {
                var customer = _customerService.GetCustomerById(item.CustomerId);
                var orderModel = new NotificationResponse
                {
                    OrderId = item.Id,
                    OrderDate = _dateTimeHelper.ConvertToUserTime(item.CreatedOnUtc, DateTimeKind.Utc),
                    CustomerName = _customerService.GetCustomerFullName(customer),
                    Price = _priceFormatter.FormatPrice(item.OrderTotal, true, false)
                };
                model.Add(orderModel);
            }

            return model;
        }

        #endregion
    }
}
