﻿using System.Collections.Generic;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Stores;

namespace Nop.Services.Messages
{
    /// <summary>
    /// Email account service
    /// </summary>
    public partial interface IEmailAccountService
    {
        /// <summary>
        /// Inserts an email account
        /// </summary>
        /// <param name="emailAccount">Email account</param>
        void InsertEmailAccount(EmailAccount emailAccount);

        /// <summary>
        /// Updates an email account
        /// </summary>
        /// <param name="emailAccount">Email account</param>
        void UpdateEmailAccount(EmailAccount emailAccount);

        /// <summary>
        /// Deletes an email account
        /// </summary>
        /// <param name="emailAccount">Email account</param>
        void DeleteEmailAccount(EmailAccount emailAccount);

        /// <summary>
        /// Gets an email account by identifier
        /// </summary>
        /// <param name="emailAccountId">The email account identifier</param>
        /// <returns>Email account</returns>
        EmailAccount GetEmailAccountById(int emailAccountId);

        /// <summary>
        /// Gets all email accounts
        /// </summary>
        /// <returns>Email accounts list</returns>
        IList<EmailAccount> GetAllEmailAccounts(int storeId = 0);

        //Custom code from v4.0
        IList<Store> GetStoreNotAssignedToEmail();

        /// <summary>
        /// check existed record
        /// </summary>
        /// <param name="emailAccountId"></param>
        /// <param name="storeId"></param>
        /// <returns></returns>
        bool CheckExistedRecord(int emailAccountId, int storeId);
    }
}
