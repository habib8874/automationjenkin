﻿using Nop.Core.Domain.Localization;

namespace Nop.Core.Domain.Directory
{
    /// <summary>
    /// Represents a store country and state mapping
    /// </summary>
    public partial class StoreCountryStateMapping : BaseEntity
    {

        /// <summary>
        /// Gets or sets the store identifier
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the country identifier
        /// </summary>
        public int CountryId { get; set; }

        /// <summary>
        /// Gets or sets the countryname
        /// </summary>
        public string CountryName { get; set; }

        /// <summary>
        /// Gets or sets the state identifier
        /// </summary>
        public int StateId { get; set; }

        /// <summary>
        /// Gets or sets the statename
        /// </summary>
        public string StateName { get; set; }

        /// <summary>
        /// Gets or sets the IsActive
        /// </summary>
        public bool? IsActive { get; set; }

    }
}
