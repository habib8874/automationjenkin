﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Models.Home
{
    public class OffersModel
    {
        public string Name { get; set; }
        public bool IsPercentage { get; set; }
    }
}
