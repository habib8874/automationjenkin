﻿using NHKCustomerApplication.Models;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModels;
using System;
using System.Linq;

namespace NHKCustomerApplication.Services
{
	public class OrderServices : IOrderServices
	{
		#region Fields

		private readonly ecuadordevContext _dbcontext;

		#endregion

		#region Ctor

		public OrderServices(ecuadordevContext dbcontext)
		{
			_dbcontext = dbcontext;
		}

		#endregion

		/// <summary>
		/// Get AgentOrderGeoLocation entity
		/// </summary>
		/// <param name="agentId"></param>
		/// <param name="orderId"></param>
		/// <returns>AgentOrderGeoLocation</returns>
		public AgentOrderGeoLocation GetAgentOrderGeoLocation(int agentId, int orderId)
		{
			return _dbcontext.AgentOrderGeoLocation.FirstOrDefault(a => a.AgentId == agentId && a.OderId == orderId);
		}

		/// <summary>
		/// Prepare LiveTrackingModel
		/// </summary>
		/// <param name="orderDetailmodel"></param>
		/// <param name="storeId"></param>
		/// <param name="languageId"></param>
		/// <returns>LiveTrackingModel</returns>
		public LiveTrackingModel PrepareLiveTrackingModel(OrderTrackingModelV1 orderDetailmodel, int storeId, int languageId)
		{
			var model = orderDetailmodel.LiveTrackingModel;
			model.OrderId = orderDetailmodel.OrderId;
			model.TrackingPossible = false;

			var order = _dbcontext.Order.FirstOrDefault(o => o.Id == orderDetailmodel.OrderId);
			if (order == null)
				return null;

			//restaurant(vendor/merchant) info
			var vendorId = _dbcontext.Product.FirstOrDefault(p => p.Id == orderDetailmodel.OrderedItems.FirstOrDefault().ProductId)?.VendorId ?? 0;
			if (vendorId > 0)
			{
				var vendor = _dbcontext.Vendor.FirstOrDefault(v => v.Id == vendorId);
				var vendorAddressId = vendor?.AddressId ?? 0;
				var vendorAddress = _dbcontext.Address.FirstOrDefault(a => a.Id == vendorAddressId);
				if (vendorAddress != null)
				{
					model.RestauratTrakingInfo.RestaurantId = vendorId;
					model.RestauratTrakingInfo.RestauratName = vendor?.Name ?? string.Empty;
					model.RestauratTrakingInfo.Latitude = vendorAddress?.Latitude?.ToString();
					model.RestauratTrakingInfo.Longitude = vendorAddress?.Longitude?.ToString();

					//check coordinates is not null
					if (vendorAddress.Latitude == null || vendorAddress.Longitude == null)
						model.ErrorMessage.Add(LanguageHelper.GetResourseValueByName(storeId, "NB.Order.LiveTracking.Restaurant.Address.Coordinates.NotFound", languageId, _dbcontext));
				}
				else
				{
					model.ErrorMessage.Add(LanguageHelper.GetResourseValueByName(storeId, "NB.Order.LiveTracking.Restaurant.Address.NotFound", languageId, _dbcontext));
				}
			}

			//customer info
			var customerAddress = _dbcontext.Address.FirstOrDefault(a => a.Id == order.BillingAddressId);
			if (customerAddress != null)
			{
				model.CustomerTrakingInfo.CustomertId = order.CustomerId;
				model.CustomerTrakingInfo.CustomerName = orderDetailmodel.CustomerName;
				model.CustomerTrakingInfo.Latitude = customerAddress?.Latitude?.ToString();
				model.CustomerTrakingInfo.Longitude = customerAddress?.Longitude?.ToString();

				//check coordinates is not null
				if (customerAddress.Latitude == null || customerAddress.Longitude == null)
					model.ErrorMessage.Add(LanguageHelper.GetResourseValueByName(storeId, "NB.Order.LiveTracking.Customer.Address.Coordinates.NotFound", languageId, _dbcontext));
			}
			else
			{
				model.ErrorMessage.Add(LanguageHelper.GetResourseValueByName(storeId, "NB.Order.LiveTracking.Customer.Address.NotFound", languageId, _dbcontext));
			}

			//rider info
			var assignedAgent = _dbcontext.AgentOrderStatus.FirstOrDefault(ag => ag.OrderId == order.Id);
			if (assignedAgent == null)
				model.ErrorMessage.Add(LanguageHelper.GetResourseValueByName(storeId, "NB.Order.LiveTracking.Agent.NotAssigned", languageId, _dbcontext));
			else
			{
				var agentCustomer = _dbcontext.Customer.FirstOrDefault(c => c.Id == assignedAgent.AgentId) ?? null;
				if (agentCustomer == null)
					model.ErrorMessage.Add(LanguageHelper.GetResourseValueByName(storeId, "NB.Order.LiveTracking.Agent.NotAssigned", languageId, _dbcontext));
				else
				{
					model.RiderTrakingInfo.RidertId = assignedAgent.AgentId.Value;
					model.RiderTrakingInfo.RiderName = Helper.GetCustomerFullName(assignedAgent.AgentId.Value, _dbcontext);

					//get rider latest coordinates if available, else assign restaurant coordinates 
					var agentCoordinatesrecord = GetAgentOrderGeoLocation(agentCustomer.Id, order.Id);
					if (agentCoordinatesrecord != null)
					{
						model.RiderTrakingInfo.Latitude = agentCoordinatesrecord.AgentLat;
						model.RiderTrakingInfo.Longitude = agentCoordinatesrecord.AgentLong;
					}
					else
					{
						//now here. initially in agent latlong, assign restaurant latlong detail, because rider starting point will be at restaurant address.
						//Further we will update agent latlong as rider moves on track
						model.RiderTrakingInfo.Latitude = model.RestauratTrakingInfo.Latitude;
						model.RiderTrakingInfo.Longitude = model.RestauratTrakingInfo.Longitude;
					}
				}
			}

			//check for error
			if (!model.ErrorMessage.Any())
				model.TrackingPossible = true;

			return model;
		}

		/// <summary>
		/// Get order status mini description
		/// </summary>
		/// <param name="orderStatusId"></param>
		/// <param name="orderReviewModel"></param>
		/// <param name="languageId"></param>
		/// <param name="vendor"></param>
		/// <param name="agent"></param>
		/// <returns></returns>
		public string GetStatusMiniDescription(int orderStatusId, int storeId, int languageId, Vendor vendor = null, int agentCustomerId = 0)
		{
			var miniDescription = string.Empty;
			if (orderStatusId == (int)OrderHistoryEnumV1.Received)
			{
				miniDescription = LanguageHelper.GetResourseValueByName(storeId, "API.Order.OrderDetail.Status.MiniDescription.Recived", languageId, _dbcontext);
			}
			else if (orderStatusId == (int)OrderHistoryEnumV1.Confirmed)
			{
				miniDescription = LanguageHelper.GetResourseValueByName(storeId, "API.Order.OrderDetail.Status.MiniDescription.Confirmed", languageId, _dbcontext);
			}
			else if (orderStatusId == (int)OrderHistoryEnumV1.RiderAssigned)
			{
				miniDescription = string.Format(LanguageHelper.GetResourseValueByName(storeId, "API.Order.OrderDetail.Status.MiniDescription.RiderAssigned", languageId, _dbcontext), Helper.GetCustomerFullName(agentCustomerId, _dbcontext) ?? string.Empty);
			}
			else if (orderStatusId == (int)OrderHistoryEnumV1.Prepared)
			{
				miniDescription = string.Format(LanguageHelper.GetResourseValueByName(storeId, "API.Order.OrderDetail.Status.MiniDescription.Prepared", languageId, _dbcontext), vendor?.Name ?? string.Empty);
			}
			else if (orderStatusId == (int)OrderHistoryEnumV1.Picked)
			{
				miniDescription = string.Format(LanguageHelper.GetResourseValueByName(storeId, "API.Order.OrderDetail.Status.MiniDescription.Picked", languageId, _dbcontext), Helper.GetCustomerFullName(agentCustomerId, _dbcontext) ?? string.Empty);
			}
			else if (orderStatusId == (int)OrderHistoryEnumV1.Delivered)
			{
				miniDescription = LanguageHelper.GetResourseValueByName(storeId, "API.Order.OrderDetail.Status.MiniDescription.Delivered", languageId, _dbcontext);
			}
			else if (orderStatusId == (int)OrderHistoryEnumV1.Cancelled)
			{
				miniDescription = LanguageHelper.GetResourseValueByName(storeId, "API.Order.OrderDetail.Status.MiniDescription.Cancelled", languageId, _dbcontext);
			}

			return miniDescription;
		}

		/// <summary>
		/// Get rating values
		/// </summary>
		/// <param name="storeId"></param>
		/// <param name="entityId"></param>
		/// <param name="reviewType"></param>
		/// <returns></returns>
		public virtual decimal GetAverageRating(int storeId, int entityId, int reviewType, out int reviewCount)
		{
			decimal ratingValue = 0;

			var ratingRecords = _dbcontext.RatingReviews.Where(t => t.StoreId == storeId && t.EntityId == entityId && t.ReviewType == reviewType && t.IsApproved);
			reviewCount = ratingRecords.Count();
			if (ratingRecords.Count() > 0)
			{
				var oneStartCount = ratingRecords.Where(x => x.Rating == 1).Count();
				var twoStartCount = ratingRecords.Where(x => x.Rating == 2).Count();
				var threeStartCount = ratingRecords.Where(x => x.Rating == 3).Count();
				var fourStartCount = ratingRecords.Where(x => x.Rating == 4).Count();
				var fiveStartCount = ratingRecords.Where(x => x.Rating == 5).Count();
				var countSum = fiveStartCount + fourStartCount + threeStartCount + twoStartCount + oneStartCount;

				if (countSum > 0)
					ratingValue = (5 * fiveStartCount + 4 * fourStartCount + 3 * threeStartCount + 2 * twoStartCount + 1 * oneStartCount) / (countSum);
			}

			return ratingValue;
		}
	}
}
