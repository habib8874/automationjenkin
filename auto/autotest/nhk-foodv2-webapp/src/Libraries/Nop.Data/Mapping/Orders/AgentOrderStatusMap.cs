using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Orders;

namespace Nop.Data.Mapping.Orders
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class AgentOrderStatusMap : NopEntityTypeConfiguration<AgentOrderStatus>
    {
        public override void Configure(EntityTypeBuilder<AgentOrderStatus> builder)
        {
            builder.ToTable(nameof(AgentOrderStatus));
            builder.HasKey(sci => sci.Id);

            base.Configure(builder);
        }
    }
}