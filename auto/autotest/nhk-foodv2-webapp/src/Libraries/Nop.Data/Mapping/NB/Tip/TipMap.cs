﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB.Tip;

namespace Nop.Data.Mapping.NB.QuestionAnswer
{
    public partial class TipMap : NopEntityTypeConfiguration<Tip>
    {
        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<Tip> builder)
        {
            builder.ToTable(NopMappingDefaults.Tip);
            builder.HasKey(pc => pc.Id);
            base.Configure(builder);
        }
    }
}
