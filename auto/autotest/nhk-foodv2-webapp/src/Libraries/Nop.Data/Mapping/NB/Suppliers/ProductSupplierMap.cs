﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB.Suppliers;

namespace Nop.Data.Mapping.NB.Suppliers
{
    public partial class ProductSupplierMap:NopEntityTypeConfiguration<ProductSupplier>
    {
        public override void Configure(EntityTypeBuilder<ProductSupplier> builder)
        {
            builder.ToTable(NopMappingDefaults.ProductSupplier);
            builder.HasKey(productSupplier => productSupplier.Id);

            builder.HasOne(productSupplier => productSupplier.Supplier)
                .WithMany()
                .HasForeignKey(productSupplier => productSupplier.SupplierId)
                .IsRequired();

            builder.HasOne(productSupplier => productSupplier.Product)
                .WithMany()
                .HasForeignKey(productSupplier => productSupplier.ProductId)
                .IsRequired();
            builder.Property(productSupplier => productSupplier.Price).HasColumnType("decimal(18, 4)").IsRequired();
            builder.Property(productSupplier => productSupplier.DisplayOrder).IsRequired();
            builder.Property(productSupplier => productSupplier.IsFeaturedProduct).IsRequired();
            base.Configure(builder);
        }

    }
}
