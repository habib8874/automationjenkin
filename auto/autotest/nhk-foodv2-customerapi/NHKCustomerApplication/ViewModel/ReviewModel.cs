﻿using System.Collections.Generic;

namespace NHKCustomerApplication.ViewModel
{
	public class ReviewModel
	{
		public int Id { get; set; }
	}

	public class ReviewRequestModel : BaseAPIRequestModel
	{
		public int ReviewType { get; set; }
		public int EntityId { get; set; }
		public int PageIndex { get; set; }
		public int PageSize { get; set; }
	}

	public class ReviewResponceModel
	{
		public bool ValidData { get; set; }
		public string ResultData { get; set; }
	}

	public class ReviewOverviewModel : ReviewResponceModel
	{
		public ReviewOverviewModel()
		{
			Reviews = new List<ReviewEntityModel>();
		}

		/// <summary>
		/// Review type, By default Agent:1, Merchant:2, Product:3
		/// </summary>
		public int ReviewType { get; set; }
		/// <summary>
		/// Agent or Merchant or product id
		/// </summary>
		public int EntityId { get; set; }
		public int TotalReviews { get; set; }
		public int RatingSum { get; set; }
		public int PageIndex { get; set; }
		public int PageSize { get; set; }
		public IList<ReviewEntityModel> Reviews { get; set; }
	}

	public class ReviewEntityModel
	{
		public int ReviewType { get; set; }
		public int EntityId { get; set; }
		public string CustomerName { get; set; }
		public string Name { get; set; }
		public string Title { get; set; }
		public string ReviewText { get; set; }
		public string ReplyText { get; set; }
		public int Rating { get; set; }
		public string ReviewDate { get; set; }
		public string ApprovalStatus { get; set; }
	}
}
