﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Models.Media;

namespace Nop.Web.Models.Order
{
    /// <summary>
    /// Represetn Order live tracking model
    /// </summary>
    public class LiveTrackingModel
    {
        public LiveTrackingModel()
        {
            RestauratTrakingInfo = new RestauratTrakingInfo();
            CustomerTrakingInfo = new CustomerTrakingInfo();
            RiderTrakingInfo = new RiderTrakingInfo();
            ErrorMessage = new List<string>();
        }

        public int OrderId { get; set; }
        public RestauratTrakingInfo RestauratTrakingInfo { get; set; }
        public CustomerTrakingInfo CustomerTrakingInfo { get; set; }
        public RiderTrakingInfo RiderTrakingInfo { get; set; }
        public bool TrackingPossible { get; set; }
        public IList<string> ErrorMessage { get; set; }
    }

    #region Info models

    public class RestauratTrakingInfo : LatLong
    {
        public int RestaurantId { get; set; }
        public string RestauratName { get; set; }
        public string RestHelpLink { get; set; }
        public string RestPhone { get; set; }
        public int RestRatingCount { get; set; }
        public decimal RestAverageRating { get; set; }

        public PictureModel PictureModel { get; set; }
    }
    public class CustomerTrakingInfo : LatLong
    {
        public int CustomertId { get; set; }
        public string CustomerName { get; set; }
    }
    public class RiderTrakingInfo : LatLong
    {
        public int RidertId { get; set; }
        public string RiderName { get; set; }
        public string RiderPhone { get; set; }
        public int RiderRatingCount { get; set; }
        public decimal RiderAverageRating { get; set; }
        public string PictureUrl { get; set; }
    }

    public class LatLong
    {
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
    }

    #endregion

    #region Status Tracking

    public enum OrderHistoryEnum
    {
        [Display(Name = "Order Placed")]
        Received = 10,
        [Display(Name = "Order Accepted")]
        Confirmed = 20,
        [Display(Name = "Rider Accepted Order")]
        RiderAssigned = 25,
        [Display(Name = "Order is preparing")]
        Prepared = 30,
        [Display(Name = "Order is Picked Up and On the way")]
        Picked = 3,
        [Display(Name = "Order Delivered")]
        Delivered = 4,
        [Display(Name = "Order is Cancelled")]
        Cancelled = 40,
    }

    public class OrderedStatusTracking
    {
        public int StatusId { get; set; }
        public string StatusTitle { get; set; }
        public string MiniDescription { get; set; }
        public DateTime? StatusTime { get; set; }
        public bool IsUpdate { get; set; }
    }

    #endregion
}