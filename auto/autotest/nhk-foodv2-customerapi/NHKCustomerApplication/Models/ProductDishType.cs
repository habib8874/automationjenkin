﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class ProductDishType
    {
        public ProductDishType()
        {
            ProductProductDishTypeMapping = new HashSet<ProductProductDishTypeMapping>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ProductProductDishTypeMapping> ProductProductDishTypeMapping { get; set; }
    }
}
