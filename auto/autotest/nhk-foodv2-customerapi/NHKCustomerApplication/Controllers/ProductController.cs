﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NHKCustomerApplication.Caching;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Repository;
using NHKCustomerApplication.Services;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModel;
using NHKCustomerApplication.ViewModels;
using System.Linq;

namespace NHKCustomerApplication.Controllers
{
    public class ProductController : ControllerBase
    {
        #region "fields"

        private string connectionString = string.Empty;
        private ecuadordevContext dbcontext;
        private string CurrencyCode = string.Empty;
        private readonly IConfiguration _configuration;
        private readonly IUserRepository _userRepository;
        private readonly ICommonService _commonservice;
        private readonly ICacheManager _cacheManager;
        public string CurrencySymbol { get; set; }

        #endregion

        #region 
        public ProductController(IUserRepository userRepository, ICacheManager cacheManager, ICommonService commonService ,ecuadordevContext context, IConfiguration configuration)
        {
            dbcontext = context;
            _configuration = configuration;
            this._commonservice = commonService;
            this._userRepository = userRepository;
            this._cacheManager = cacheManager;
        }
        #endregion

        /// <summary>
        /// Get list of products
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        [Route("~/api/v1/GetProductItemsByCategoryId")]
        [HttpPost]
        public BaseAPIResponseModel GetCooknRestItemsByCategoryId([FromBody] CooknRestProductByCategoryIds requestModel)
        {
            int languageId = LanguageHelper.GetIdByLangCode(requestModel.StoreId, requestModel.UniqueSeoCode, dbcontext);
            if (requestModel == null)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMessage.ModelNotDefined", languageId, dbcontext));
            if (requestModel.CustId != 0)
            {
                var customerExist = Helper.GetCustomer(requestModel.CustId, requestModel.StoreId, dbcontext);
                if (customerExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.CustomerNotFound", languageId, dbcontext));
            }
            if (!string.IsNullOrEmpty(requestModel.ApiKey) || !string.IsNullOrWhiteSpace(requestModel.ApiKey))
            {
                var keyExist = Helper.AuthenticateKey(requestModel.ApiKey, requestModel.StoreId, dbcontext,_cacheManager);
                if (keyExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext));
            }
            if (requestModel.StoreId == 0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.StoreIdMissing", languageId, dbcontext));
            //Bring Product Lisiting
            var key = string.Format(CacheKeys.ProductListV1Key, requestModel.StoreId,  requestModel.merchantid, requestModel.CategoryId, requestModel.keywords, requestModel.CustId,requestModel.page,requestModel.perPage, languageId);
            var data = _cacheManager.Get(key, () =>
            {
                return _commonservice.GetCooknRestItemsByCategoryIdV1(requestModel);
            });
            if(data.Count<=0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.NoProductForThisCategory", languageId, dbcontext));
            return BaseMethodsHelper.SuccessListResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.ProductFoundForThisCategory", languageId, dbcontext), data,data.Select(x=>x.TotalRecords).FirstOrDefault());
        }


        /// <summary>
        /// Get list of products
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        [Route("~/api/v2/GetProductItemsByCategoryId")]
        [HttpPost]
        public BaseAPIResponseModel GetCooknRestItemsByCategoryId_V2([FromBody] CooknRestProductByCategoryIdsV2 requestModel)
        {
            int languageId = LanguageHelper.GetIdByLangCode(requestModel.StoreId, requestModel.UniqueSeoCode, dbcontext);
            if (requestModel == null)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMessage.ModelNotDefined", languageId, dbcontext));
            if (requestModel.CustId != 0)
            {
                var customerExist = Helper.GetCustomer(requestModel.CustId, requestModel.StoreId, dbcontext);
                if (customerExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.CustomerNotFound", languageId, dbcontext));
            }
            if (!string.IsNullOrEmpty(requestModel.ApiKey) || !string.IsNullOrWhiteSpace(requestModel.ApiKey))
            {
                var keyExist = Helper.AuthenticateKey(requestModel.ApiKey, requestModel.StoreId, dbcontext,_cacheManager);
                if (keyExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext));
            }
            if (requestModel.StoreId == 0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.StoreIdMissing", languageId, dbcontext));
            //Bring Product Lisiting
            var key = string.Format(CacheKeys.ProductListKey, requestModel.StoreId, languageId, requestModel.MerchantId,requestModel.CategoryId,requestModel.Keywords,requestModel.CustId);
            var data = _cacheManager.Get(key, () =>
            {
                return _commonservice.GetCooknRestItemsByCategoryIdV2(requestModel);
            });
            if (data.Count <= 0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.NoProductForThisCategory", languageId, dbcontext));
            return BaseMethodsHelper.SuccessListResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.ProductFoundForThisCategory", languageId, dbcontext), data, data.Select(x => x.TotalRecords).FirstOrDefault());

        }

        /// <summary>
        /// Filtered and Sorted Products
        /// <param name="requestModel"></param>
        /// <returns></returns>
        [Route("~/api/v1/ProductsFilternSorting")]
        [HttpPost]
        public BaseAPIResponseModel ProductsFilternSorting([FromBody] CustomerProductFilter requestModel)
        {
            int languageId = LanguageHelper.GetIdByLangCode(requestModel.StoreId, requestModel.UniqueSeoCode, dbcontext);
            if (requestModel == null)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMessage.ModelNotDefined", languageId, dbcontext));
            if (!string.IsNullOrEmpty(requestModel.ApiKey) || !string.IsNullOrWhiteSpace(requestModel.ApiKey))
            {
                var keyExist = Helper.AuthenticateKey(requestModel.ApiKey, requestModel.StoreId, dbcontext,_cacheManager);
                if (keyExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext));
            }
            if (requestModel.StoreId == 0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.StoreIdMissing", languageId, dbcontext));
            var key=string.Format(CacheKeys.ProductFilterListKey, requestModel.StoreId,  requestModel.CategoryIds,requestModel.CustId,requestModel.FeaturedProducts,requestModel.FilterableProductAttributeOptionIds,requestModel.FilterableSpecificationAttributeOptionIds,requestModel.FilteredAttribute,requestModel.FilteredSpecs,requestModel.FullTextMode,requestModel.Keywords,requestModel.LoadFilterableProductAttributeOptionIds,requestModel.LoadFilterableSpecificationAttributeOptionIds,requestModel.ManufacturerId,requestModel.OrderBy,requestModel.PageIndex,requestModel.PageSize,requestModel.PriceMax,requestModel.PriceMin,requestModel.ProductTypeId,requestModel.SearchDescriptions,requestModel.UseFullTextSearch,requestModel.VendorId,languageId);
            var data = _cacheManager.Get(key, () =>
            {
                return _commonservice.GetproductsItemByFilternSorting(requestModel);
            });
            if (data == null)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.NoProductForThisCategory", languageId, dbcontext));
            return BaseMethodsHelper.SuccessListResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.ErrorMesaage.ProductFoundForThisCategory", languageId, dbcontext), data, data.Count);

        }


    }
}
 
