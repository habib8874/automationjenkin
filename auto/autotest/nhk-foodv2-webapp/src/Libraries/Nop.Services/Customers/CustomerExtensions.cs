﻿using Nop.Core.Domain.Customers;
using Nop.Core.Infrastructure;
using Nop.Services.Common;
using Nop.Services.RatingReview;
using System;
namespace Nop.Services.Customers
{
    /// <summary>
    /// Customer extensions
    /// </summary>
    public static class CustomerExtensions
    {
        #region Custom code from v4.0
        public static string GetFullName(this Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));
            var genericAttributeService = EngineContext.Current.Resolve<IGenericAttributeService>();
            var firstName = genericAttributeService.GetAttribute<string>(customer,NopCustomerDefaults.FirstNameAttribute, customer.RegisteredInStoreId,""); //added customer.RegisteredInStoreId
            var lastName = genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute, customer.RegisteredInStoreId,""); //added customer.RegisteredInStoreId

            var fullName = "";
            if (!string.IsNullOrWhiteSpace(firstName) && !string.IsNullOrWhiteSpace(lastName))
                fullName = $"{firstName} {lastName}";
            else
            {
                if (!string.IsNullOrWhiteSpace(firstName))
                    fullName = firstName;

                if (!string.IsNullOrWhiteSpace(lastName))
                    fullName = lastName;
            }
            return fullName;
        }

        public static int CheckReviewForCustomer(this Customer customer, int reviewType, int orderId, int storeId, bool skipNotInterestedCheck = false)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            //the guests don't have a password
            if (customer.IsGuest())
                return 0;

            var ratingReviewService = EngineContext.Current.Resolve<IRatingReviewService>();
            var reviewResult = ratingReviewService.CheckReviewForCustomer(storeId, customer.Id, reviewType, orderId, skipNotInterestedCheck);
            return reviewResult;
        }
        #endregion
    }
}
