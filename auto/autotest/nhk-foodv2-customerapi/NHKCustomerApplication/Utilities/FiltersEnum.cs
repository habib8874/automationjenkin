﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NHKCustomerApplication.Utilities
{
    public enum FiltersEnum
    {
        RestnCook = 1,
        Dishes = 2
    }

   
    public enum MerchantFilterEnum
    {
        Sort = 1,
        Distance = 2,
        Cuisine = 3,
        Rating = 4,
        Preferences = 5
    }
    public enum AddressTypeEnum
    {
        Home = 1,
        Office = 2,
        Others = 3,
    }
    public enum OrderHistoryEnum
    {
        [Display(Name = "Order Placed")]
        Received = 10,
        [Display(Name = "Order Delivered")]
        OrderDelivered = 4,
        [Display(Name = "Order Accepted")]
        Confirmed = 20,
        OntheWay = 50,
        [Display(Name = "Order is preparing")]
        Preparing = 30,
        [Display(Name = "Order is Picked Up")]
        Picked = 3,
        [Display(Name = "Order is Cancelled")]
        Cancelled = 40,
        [Display(Name = "Rider Accepted Order")]
        RiderAssigned = 25

    }

    public enum OrderHistoryEnumV1
    {
        [Display(Name = "Order Placed")]
        Received = 10,
        [Display(Name = "Order Accepted")]
        Confirmed = 20,
        [Display(Name = "Rider Accepted Order")]
        RiderAssigned = 25,
        [Display(Name = "Order is preparing")]
        Prepared = 30,
        [Display(Name = "Order is Picked Up and On the way")]
        Picked = 3,
        [Display(Name = "Order Delivered")]
        Delivered = 4,
        [Display(Name = "Order is Cancelled")]
        Cancelled = 40,

        //ConfirmedArrive = 35
        //Note: If anyone want to use ConfirmedArrive then use enum value 35 for same.
    }
    public enum ShoppingCartType
    {
        /// <summary>
        /// Shopping cart
        /// </summary>
        ShoppingCart = 1,
        /// <summary>
        /// Wishlist
        /// </summary>
        Wishlist = 2,
    }
    public enum ProductType
    {
        /// <summary>
        /// Simple
        /// </summary>
        SimpleProduct = 5,
        /// <summary>
        /// Grouped (product with variants)
        /// </summary>
        GroupedProduct = 10,
    }
    public enum TaxDisplayType
    {
        /// <summary>
        /// Including tax
        /// </summary>
        IncludingTax = 0,
        /// <summary>
        /// Excluding tax
        /// </summary>
        ExcludingTax = 10,
    }
    public enum PaymentStatus
    {
        /// <summary>
        /// Pending
        /// </summary>
        Pending = 10,
        /// <summary>
        /// Authorized
        /// </summary>
        Authorized = 20,
        /// <summary>
        /// Paid
        /// </summary>
        Paid = 30,
        /// <summary>
        /// Partially Refunded
        /// </summary>
        PartiallyRefunded = 35,
        /// <summary>
        /// Refunded
        /// </summary>
        Refunded = 40,
        /// <summary>
        /// Voided
        /// </summary>
        Voided = 50,
        /// <summary>
        /// Failed
        /// </summary>
        Failed = 60,
    }
    public enum ShippingStatus
    {
        /// <summary>
        /// Shipping not required
        /// </summary>
        ShippingNotRequired = 10,
        /// <summary>
        /// Not yet shipped
        /// </summary>
        NotYetShipped = 20,
        /// <summary>
        /// Partially shipped
        /// </summary>
        PartiallyShipped = 25,
        /// <summary>
        /// Shipped
        /// </summary>
        Shipped = 30,
        /// <summary>
        /// Delivered
        /// </summary>
        Delivered = 40,
    }
    /// <summary>
    /// Represents an ReviewType
    /// </summary>
    public enum ReviewType
    {
        Agent = 1,
        Merchant = 2,
        Item = 3,
        ItemAttribute = 4,
    }
    public enum DiscountType
    {
        /// <summary>
        /// Assigned to order total 
        /// </summary>
        AssignedToOrderTotal = 1,
       
    }
    public enum DiscountLimitationType
    {
        /// <summary>
        /// None
        /// </summary>
        Unlimited = 0,
        /// <summary>
        /// N Times Only
        /// </summary>
        NTimesOnly = 15,
        /// <summary>
        /// N Times Per Customer
        /// </summary>
        NTimesPerCustomer = 25,
    }

    public enum OrderStatus
    {
        //Custom code from v4.0
        /// <summary>
        /// Pending
        /// </summary>
        //Pending = 10,
        Received = 10,
        /// <summary>
        /// Processing
        /// </summary>
        //Processing = 20,
        Confirmed = 20,
        /// <summary>
        /// Complete
        /// </summary>
        //Complete = 30,
        Prepared = 30,
        OrderPickedUp = 3,
        OrderDelivered = 4,

        /// <summary>
        /// Cancelled
        /// </summary>
        Cancelled = 40
    }
}