﻿using NHKCustomerApplication.Models;
using NHKCustomerApplication.ViewModel;
using NHKCustomerApplication.ViewModels;
using System.Collections.Generic;

namespace NHKCustomerApplication.Services
{
	public interface ICommonService
	{
		/// <summary>
		/// Get vendor slider pictures
		/// </summary>
		/// <param name="vendorId">Vendor identifier</param>
		/// <param name="storeId">Store identifier</param>
		/// <returns>List of PictureModel</returns>
		public List<PictureModel> GetVendorSliderPictures(int vendorId, int storeId);


		/// <summary>
		/// Get Store slider pictures
		/// </summary>
		/// <param name="vendorId">Vendor identifier</param>
		/// <param name="storeId">Store identifier</param>
		/// <returns>List of PictureModel</returns>
		public List<PictureModel> GetStoreSliderPictures(int storeId);

		/// <summary>
		/// Get slider pictures by geo location
		/// </summary>
		/// <param name="latitude"></param>
		/// <param name="langitute"></param>
		/// <param name="storeId"></param>
		/// <returns>List of pictures</returns>
		IList<PictureModel> GetSliderPicturesByGeoLocation(string latitude, string langitute, int storeId);

		/// <summary>
		/// All Restaurants Db(TakeAway + Delivery)
		/// </summary>
		/// <returns>List of ALl Rated Restaurants</returns>
		public string GetTopRatedCooknRestDbResult(TopRatedRestnChefModel requestModel);

		/// <summary>
		/// Restaurants(TakeAway + Delivery)
		/// </summary>
		/// <returns>List of ALl Rated Restaurants</returns>
		public List<CooknRestV2> GetTopRatedCooknRest(TopRatedRestnChefModel requestModel, List<int> listmodel);

		/// <summary>
		/// Product List
		/// </summary>
		/// <returns>List of ALl Rated products</returns>
		List<CooknChefCatProductsV2> GetCooknRestItemsByCategoryIdV1(CooknRestProductByCategoryIds requestModel);
		List<CooknChefCatProductsV2> GetCooknRestItemsByCategoryIdV2(CooknRestProductByCategoryIdsV2 requestModel);

		/// <summary>
		/// Filtered ans sorted product list
		/// </summary>
		/// <param name="requestModel"></param>
		/// <returns>List of ALl Rated products</returns>
		List<ProductsFilternSorting> GetproductsItemByFilternSorting(CustomerProductFilter requestModel);

		#region Favorite merchant

		/// <summary>
		/// Get favorite merchant by customer
		/// </summary>
		/// <param name="requestModel"></param>
		/// <returns>CustomerFavoriteMerchantResponce</returns>
		CustomerFavoriteMerchantResponce GetFavoriteMerchantsByCustomerId(CustomerFavoriteMerchant requestModel);

		#endregion

		#region Reviews

		/// <summary>
		/// Get review list for agnet/merchant/product based on entity id and store.
		/// </summary>
		/// <param name="requestModel"></param>
		/// <returns>ReviewOverviewModel</returns>
		ReviewOverviewModel GetReviewsByEntityId(ReviewRequestModel requestModel, int languageId, ecuadordevContext dbContext);

		#endregion
		void  AddressToFar(AddRemoveProductCart addProductToCart, ProductDetailRootModel productDetailRoot,  int VendorId, int languageId);
	}
}
