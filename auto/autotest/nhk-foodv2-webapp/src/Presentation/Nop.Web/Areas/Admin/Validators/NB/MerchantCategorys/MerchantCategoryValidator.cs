﻿using FluentValidation;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Areas.Admin.Models.NB.MerchantCategorys;
using Nop.Web.Framework.Validators;

namespace Nop.Web.Areas.Admin.Validators.NB.MerchantCategorys
{
    public partial class MerchantCategoryValidator : BaseNopValidator<MerchantCategoryModel>
    {
        public MerchantCategoryValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("NB.Admin.MerchantCategory.Fields.Name.Required"));
        }
    }
}