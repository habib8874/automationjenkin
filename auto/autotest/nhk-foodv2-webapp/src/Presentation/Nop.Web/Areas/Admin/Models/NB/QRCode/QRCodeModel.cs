﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB
{
    /// <summary>
    /// Represents a QRCode model
    /// </summary>
    public partial class QRCodeModel : BaseNopEntityModel
    {
        public QRCodeModel() 
        {
            AvailableVendor = new List<SelectListItem>();
        }
        
        [NopResourceDisplayName("NB.Admin.Catalog.QRCode.Fields.VendorId")]
        public int VendorId { get; set; }
        
        [UIHint("Picture")]
        [NopResourceDisplayName("NB.Admin.Catalog.QRCode.Fields.QRCodeLogoId")]
        public int QRCodeLogoId { get; set; }

        [NopResourceDisplayName("NB.Admin.Catalog.QRCode.Fields.ResturantContent")]
        public string ResturantContent { get; set; }

        [NopResourceDisplayName("NB.Admin.Catalog.QRCode.Fields.WebsiteURL")]
        public string WebsiteURL { get; set; }

        [NopResourceDisplayName("NB.Admin.Catalog.QRCode.Fields.QRCodePictureId")]
        public int QRCodePictureId { get; set; }

        [NopResourceDisplayName("NB.Admin.Catalog.QRCode.Fields.LogoURL")]
        public string LogoURL { get; set; }

        [NopResourceDisplayName("NB.Admin.Catalog.QRCode.Fields.QRCodeURL")]
        public string QRCodeURL { get; set; }

        [NopResourceDisplayName("NB.Admin.Catalog.QRCode.Fields.PDFLink")]
        public string PDFLink { get; set; }

        [NopResourceDisplayName("NB.Admin.Catalog.QRCode.Fields.VendorName")]
        public string VendorName { get; set; }

        [NopResourceDisplayName("NB.Admin.Catalog.QRCode.Fields.AvailableVendor")]
        public IList<SelectListItem> AvailableVendor { get; set; }
    }
}