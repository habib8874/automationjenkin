﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB.Package
{
    /// <summary>
    /// Represents a package plan search model
    /// </summary>
    public partial class PackagePlanSearchModel : BaseSearchModel
    {
        public int PackageId { get; set; }
    }
}