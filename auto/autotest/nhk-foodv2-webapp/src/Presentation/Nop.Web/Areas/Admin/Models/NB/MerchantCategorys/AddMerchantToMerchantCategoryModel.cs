﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB.MerchantCategorys
{
    /// <summary>
    /// Represents a Merchant model to add to the Merchant category
    /// </summary>
    public partial class AddMerchantToMerchantCategoryModel : BaseNopModel
    {
        #region Ctor

        public AddMerchantToMerchantCategoryModel()
        {
            SelectedMerchantIds = new List<int>();
        }
        #endregion

        #region Properties

        public int MerchantCategoryId { get; set; }

        public IList<int> SelectedMerchantIds { get; set; }

        #endregion
    }
}