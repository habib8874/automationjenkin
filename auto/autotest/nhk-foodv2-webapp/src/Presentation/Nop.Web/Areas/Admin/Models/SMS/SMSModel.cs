﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.SMS
{
    public class SMSModel
    {
        public string To { get; set; }
        public string From { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Message { get; set; }
    }
}
