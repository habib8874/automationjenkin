﻿namespace Nestorbird.Plugin.Api.RequestModel.Orders
{
    public class AssignAgentRequest
    {
        public int OrderId { get; set; }
        public int AgentId { get; set; }
        public int StatusId { get; set; }
        public int CityId { get; set; }
    }
}
