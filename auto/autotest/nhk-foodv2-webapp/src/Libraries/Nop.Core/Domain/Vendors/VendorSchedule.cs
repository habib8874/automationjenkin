﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Vendors
{
   
    /// <summary>
    /// Represents a VendorSchedule mapping
    /// </summary>
    public partial class VendorSchedule : BaseEntity
    {
        /// <summary>
        /// Gets or sets the VendorId
        /// </summary>
        public int VendorId { get; set; }

        /// <summary>
        /// Gets or sets the ScheduleName
        /// </summary>
        public string ScheduleName { get; set; }

        /// <summary>
        /// Gets or sets the ScheduleFromTime
        /// </summary>
        public TimeSpan ScheduleFromTime { get; set; }

        /// <summary>
        /// Gets or set the ScheduleToTime
        /// </summary>
        public TimeSpan ScheduleToTime { get; set; }
        /// <summary>
        /// Gets or set the Deleted
        /// </summary>
        public bool Deleted { get; set; }
        /// <summary>
        /// Gets or set the DisplayOrder
        /// </summary>
        public int DisplayOrder { get; set; }
        /// <summary>
        /// Gets or set the AvailableType
        /// </summary>
        public int AvailableType { get; set; }
        public int AvailableOn { get; set; }

        /// <summary>
        /// Gets the Vendor
        /// </summary>
        public virtual Vendor Vendor { get; set; }
    }
}
