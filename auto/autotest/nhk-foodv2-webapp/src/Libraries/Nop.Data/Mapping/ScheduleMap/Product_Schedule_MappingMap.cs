﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Schedule;

namespace Nop.Data.Mapping.ScheduleMap
{
    public partial class Product_Schedule_MappingMap : NopEntityTypeConfiguration<Product_Schedule_Mapping>
    {
        public override void Configure(EntityTypeBuilder<Product_Schedule_Mapping> builder)
        {
            builder.ToTable("Product_Schedule_Mapping");
            builder.HasKey(p => p.Id);
            base.Configure(builder);
        }
    }
}
