﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.RatingReview;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Stores;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Stores;
using Nop.Core.Domain.Vendors;
using Nop.Services.Orders;
using Nop.Services.Configuration;
using Nop.Services.Catalog;

namespace Nop.Services.RatingReview
{
    /// <summary>
    /// RatingReviews service
    /// </summary>
    public partial class RatingReviewService : IRatingReviewService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : store ID
        /// {1} : ignore ACL?
        /// {2} : show hidden?
        /// </remarks>
        private const string TOPICS_ALL_KEY = "Nop.ratingreview.all-{0}-{1}-{2}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : ratingReview ID
        /// </remarks>
        private const string TOPICS_BY_ID_KEY = "Nop.ratingreview.id-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string TOPICS_PATTERN_KEY = "Nop.ratingreview.";

        #endregion

        #region Fields

        private readonly IRepository<RatingReviews> _ratingReviewsRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IWorkContext _workContext;
        private readonly IRepository<AclRecord> _aclRepository;
        private readonly CatalogSettings _catalogSettings;
        private readonly IEventPublisher _eventPublisher;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IOrderService _orderService;
        private readonly ISettingService _settingService;

        #endregion

        #region Ctor

        /// <summary>
        /// RatingReviews service
        /// </summary>
        /// <param name="ratingReviewsRepository">Rating Reviews repository</param>
        /// <param name="storeMappingRepository">Store mapping repository</param>
        /// <param name="storeMappingService">Store mapping service</param>
        /// <param name="workContext">Work context</param>
        /// <param name="aclRepository">ACL repository</param>
        /// <param name="catalogSettings">Catalog settings</param>
        /// <param name="eventPublisher">Event publisher</param>
        /// <param name="cacheManager">Cache manager</param>
        public RatingReviewService(IRepository<RatingReviews> ratingReviewsRepository,
            IRepository<StoreMapping> storeMappingRepository,
            IStoreMappingService storeMappingService,
            IWorkContext workContext,
            IRepository<AclRecord> aclRepository,
            CatalogSettings catalogSettings,
            IEventPublisher eventPublisher,
            IStaticCacheManager cacheManager, IRepository<Vendor> vendorRepository,
            IRepository<Product> productRepository,
            IOrderService orderService,
            ISettingService settingService)
        {
            this._ratingReviewsRepository = ratingReviewsRepository;
            this._storeMappingRepository = storeMappingRepository;
            this._storeMappingService = storeMappingService;
            this._workContext = workContext;
            this._aclRepository = aclRepository;
            this._catalogSettings = catalogSettings;
            this._eventPublisher = eventPublisher;
            this._cacheManager = cacheManager;
            this._vendorRepository = vendorRepository;
            this._productRepository = productRepository;
            _orderService = orderService;
            _settingService = settingService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes a ratingReview
        /// </summary>
        /// <param name="ratingReview">RatingReviews</param>
        public virtual void DeleteRatingReviews(RatingReviews ratingReview)
        {
            if (ratingReview == null)
                throw new ArgumentNullException(nameof(ratingReview));

            _ratingReviewsRepository.Delete(ratingReview);

            //cache
            //_cacheManager.RemoveByPattern(TOPICS_PATTERN_KEY);
            _cacheManager.RemoveByPrefix(NopNBDefaults.EntityReviewsPrefixCacheKey);
            _cacheManager.RemoveByPrefix(TOPICS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(ratingReview);
        }

        /// <summary>
        /// Gets a ratingReview
        /// </summary>
        /// <param name="ratingReviewId">The ratingReview identifier</param>
        /// <returns>RatingReviews</returns>
        public virtual RatingReviews GetRatingReviewsById(int ratingReviewId)
        {
            if (ratingReviewId == 0)
                return null;

            var key = string.Format(TOPICS_BY_ID_KEY, ratingReviewId);
            return _cacheManager.Get(key, () => _ratingReviewsRepository.GetById(ratingReviewId));
        }
        /// <summary>
        /// Gets all ratingReviews
        /// </summary>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <returns>RatingReviewss</returns>
        public virtual IList<RatingReviews> GetAllRatingReviews(int storeId)
        {
            var key = string.Format(TOPICS_ALL_KEY, storeId, 0, 0);
            return _cacheManager.Get(key, () =>
            {
                var query = _ratingReviewsRepository.Table;
                if (storeId > 0)
                {
                    query = query.Where(t => t.StoreId == storeId);
                }
                query = query.OrderByDescending(t => t.CreatedOnUtc);
                return query.ToList();
            });
        }

        /// <summary>
        /// Inserts a ratingReview
        /// </summary>
        /// <param name="ratingReview">RatingReviews</param>
        public virtual void InsertRatingReviews(RatingReviews ratingReview)
        {
            if (ratingReview == null)
                throw new ArgumentNullException(nameof(ratingReview));

            _ratingReviewsRepository.Insert(ratingReview);

            //cache
            //_cacheManager.RemoveByPattern(TOPICS_PATTERN_KEY);
            _cacheManager.RemoveByPrefix(NopNBDefaults.EntityReviewsPrefixCacheKey);
            _cacheManager.RemoveByPrefix(TOPICS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(ratingReview);
        }

        /// <summary>
        /// Updates the ratingReview
        /// </summary>
        /// <param name="ratingReview">RatingReviews</param>
        public virtual void UpdateRatingReviews(RatingReviews ratingReview)
        {
            if (ratingReview == null)
                throw new ArgumentNullException(nameof(ratingReview));

            _ratingReviewsRepository.Update(ratingReview);

            //cache
            //_cacheManager.RemoveByPattern(TOPICS_PATTERN_KEY);
            _cacheManager.RemoveByPrefix(NopNBDefaults.EntityReviewsPrefixCacheKey);
            _cacheManager.RemoveByPrefix(TOPICS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(ratingReview);
        }

        /// <summary>
        /// check whether customer 
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="CustomerId"></param>
        /// <param name="reviewType"></param>
        /// <param name="orderId"></param>
        /// <param name="skipNotInterestedCheck"></param>
        /// <returns></returns>
        public virtual int CheckReviewForCustomer(int storeId, int customerId, int reviewType, int orderId, bool skipNotInterestedCheck = false)
        {
            var isReviewRequired = 0;
            var query = _ratingReviewsRepository.Table;
            if (storeId > 0)
                query = query.Where(t => t.StoreId == storeId);

            query = query.Where(t => t.CustomerId == customerId && t.OrderId == orderId);

            if (skipNotInterestedCheck)
                query = query.Where(t => !t.IsNotInterested);

            if (query.Any(t => t.IsNotInterested))
                return 3;

            query = query.Where(t => t.ReviewType == reviewType);
            var reviewsList = query.ToList();

            if (reviewsList.Count == 0)
                isReviewRequired = 1;
            else if (reviewsList.Any(x => x.IsSkipped) || (skipNotInterestedCheck && reviewsList.Count > 0))
                isReviewRequired = 2;
            else if (reviewsList.Any(x => x.IsNotInterested))
                isReviewRequired = 3;
            else
                isReviewRequired = 4;

            //check all item review submitted code
            if (reviewType == (int)Nop.Core.Domain.RatingReview.ReviewType.Item)
            {
                var order = _orderService.GetOrderById(orderId);
                if (query.ToList().Count < order.OrderItems.Count)
                    isReviewRequired = 1;
            }

            return isReviewRequired;
        }


        /// <summary>
        /// Gets all product reviews
        /// </summary>
        /// <param name="customerId">Customer identifier (who wrote a review); 0 to load all records</param>
        /// <param name="approved">A value indicating whether to content is approved; null to load all records</param> 
        /// <param name="fromUtc">Item creation from; null to load all records</param>
        /// <param name="toUtc">Item item creation to; null to load all records</param>
        /// <param name="message">Search title or review text; null to load all records</param>
        /// <param name="storeId">The store identifier; pass 0 to load all records</param>
        /// <param name="itemId">The product identifier; pass 0 to load all records</param>
        /// <param name="vendorId">The vendor identifier (limit to products of this vendor); pass 0 to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Reviews</returns>
        public virtual IPagedList<RatingReviews> GetAllRatingReviewsWithPaging(int customerId,
            bool? approved,
            DateTime? fromUtc = null,
            DateTime? toUtc = null,
            string message = null,
            int reviewType = 0,
            int storeId = 0,
            int itemId = 0,
            int vendorsId = 0,// For vendor dropdown
            int vendorId = 0,
            int pageIndex = 0,
            int pageSize = int.MaxValue,
            IList<int> vendorIds = null)
        {
            var key = string.Format(TOPICS_ALL_KEY, storeId, 0, 0);
            var query = _ratingReviewsRepository.Table;

            if (approved.HasValue)
                query = query.Where(pr => pr.IsApproved == approved);
            if (customerId > 0)
                query = query.Where(pr => pr.CustomerId == customerId);
            if (fromUtc.HasValue)
                query = query.Where(pr => fromUtc.Value <= pr.CreatedOnUtc);
            if (toUtc.HasValue)
                query = query.Where(pr => toUtc.Value >= pr.CreatedOnUtc);
            if (!string.IsNullOrEmpty(message))
                query = query.Where(pr => pr.Title.Contains(message) || pr.ReviewText.Contains(message));

            if (reviewType > 0)
                query = query.Where(pr => pr.ReviewType == reviewType);
            if (storeId > 0)
                query = query.Where(pr => pr.StoreId == storeId);

            if (itemId > 0)
                query = query.Where(pr => pr.EntityId == itemId);
            if (reviewType == (int)Nop.Core.Domain.RatingReview.ReviewType.Agent)
            {
                //filter for sub admin
                if (vendorIds != null && vendorIds.Any())
                    query = query.Where(r => r.ReviewType == (int)Nop.Core.Domain.RatingReview.ReviewType.Agent
                                        && vendorIds.Contains(r.EntityId));

                if (vendorsId > 0)
                    query = query.Where(pr => pr.Customer.VendorId == vendorsId);
            }
            else if (reviewType == (int)Nop.Core.Domain.RatingReview.ReviewType.Merchant)
            {
                //filter for sub admin
                if (vendorIds != null && vendorIds.Any())
                {
                    query = from pr in query
                            join v in _vendorRepository.Table on pr.EntityId equals v.Id
                            where pr.ReviewType == (int)Nop.Core.Domain.RatingReview.ReviewType.Merchant
                            && vendorIds.Contains(v.Id)
                            select pr;
                }

                if (vendorsId > 0)
                    query = from pr in query
                            join v in _vendorRepository.Table on pr.EntityId equals v.Id
                            where pr.ReviewType == (int)Nop.Core.Domain.RatingReview.ReviewType.Merchant
                            && v.Id == vendorsId
                            select pr;
            }
            else if (reviewType == (int)Nop.Core.Domain.RatingReview.ReviewType.Item)
            {
                //filter for sub admin
                if (vendorIds != null && vendorIds.Any())
                {
                    query = from pr in query
                            join prod in _productRepository.Table on pr.EntityId equals prod.Id
                            join v in _vendorRepository.Table on prod.VendorId equals v.Id
                            where vendorIds.Contains(v.Id)
                            select pr;
                }

                if (vendorsId > 0)
                    query = from pr in query
                            join prod in _productRepository.Table on pr.EntityId equals prod.Id
                            join v in _vendorRepository.Table on prod.VendorId equals v.Id
                            where vendorsId == v.Id
                            select pr;
            }

            //query = query.OrderBy(pr => pr.Id);
            query = query.OrderByDescending(pr => pr.CreatedOnUtc);


            var ratingReviews = new PagedList<RatingReviews>(query, pageIndex, pageSize);
            return ratingReviews;
        }


        /// <summary>
        /// Get rating reviews by identifiers
        /// </summary>
        /// <param name="ratingReviewIds">Rating review identifiers</param>
        /// <returns>Product reviews</returns>
        public virtual IList<RatingReviews> GetRatingReviewsByIds(int[] ratingReviewIds)
        {
            if (ratingReviewIds == null || ratingReviewIds.Length == 0)
                return new List<RatingReviews>();

            var query = from pr in _ratingReviewsRepository.Table
                        where ratingReviewIds.Contains(pr.Id)
                        select pr;
            var productReviews = query.ToList();
            //sort by passed identifiers
            var sortedRatingReviews = new List<RatingReviews>();
            foreach (var id in ratingReviewIds)
            {
                var ratingReview = productReviews.Find(x => x.Id == id);
                if (ratingReview != null)
                    sortedRatingReviews.Add(ratingReview);
            }
            return sortedRatingReviews;
        }


        /// <summary>
        /// Deletes rating reviews
        /// </summary>
        /// <param name="ratingReviews">Product reviews</param>
        public virtual void DeleteRatingReviews(IList<RatingReviews> ratingReviews)
        {
            if (ratingReviews == null)
                throw new ArgumentNullException(nameof(ratingReviews));

            _ratingReviewsRepository.Delete(ratingReviews);

            //_cacheManager.RemoveByPattern(PRODUCTS_PATTERN_KEY);
            //clear cache
            _cacheManager.RemoveByPrefix(NopNBDefaults.EntityReviewsPrefixCacheKey);
            _cacheManager.RemoveByPrefix(TOPICS_PATTERN_KEY);

            //event notification
            foreach (var productReview in ratingReviews)
            {
                _eventPublisher.EntityDeleted(productReview);
            }
        }

        /// <summary>
        /// Get rating values
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="entityId"></param>
        /// <param name="reviewType"></param>
        /// <returns></returns>
        public virtual decimal GetAverageRating(int storeId, int entityId, int reviewType, out int reviewCount)
        {
            decimal ratingValue = 0;

            // 19-05-2021
            var ratingRecords = _ratingReviewsRepository.Table.Where(t => t.StoreId == storeId && t.EntityId == entityId && t.ReviewType == reviewType && t.IsApproved);
            reviewCount = ratingRecords.Count();
            if (ratingRecords.Count() > 0)
            {
                var oneStartCount = ratingRecords.Where(x => x.Rating == 1).Count();
                var twoStartCount = ratingRecords.Where(x => x.Rating == 2).Count();
                var threeStartCount = ratingRecords.Where(x => x.Rating == 3).Count();
                var fourStartCount = ratingRecords.Where(x => x.Rating == 4).Count();
                var fiveStartCount = ratingRecords.Where(x => x.Rating == 5).Count();
                var countSum = fiveStartCount + fourStartCount + threeStartCount + twoStartCount + oneStartCount;

                if (countSum > 0)
                    ratingValue = (5 * fiveStartCount + 4 * fourStartCount + 3 * threeStartCount + 2 * twoStartCount + 1 * oneStartCount) / (countSum);
            }

            return ratingValue;
        }

        /// <summary>
        /// get all ratings of items
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        public virtual IList<RatingReviews> GetItemRatings(int storeId, int productId)
        {
            var ratings = _ratingReviewsRepository.Table.Where(t =>
            t.StoreId == storeId &&
            t.EntityId == productId && t.ReviewType == ((int)Nop.Core.Domain.RatingReview.ReviewType.Item) && t.IsApproved && !t.IsSkipped).ToList();
            return ratings;
        }

        /// <summary>
        /// Get value idicates wether all reviews for this order is done, including Agent, Merchant and for all order items.
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public bool AllReviewFound(int orderId)
        {
            var storeId = _workContext.GetCurrentStoreId;
            var query = _ratingReviewsRepository.Table;
            if (storeId > 0)
                query.Where(q => q.StoreId == storeId);
            if (orderId > 0)
            {
                var order = _orderService.GetOrderById(orderId);
                query = query.Where(q => q.OrderId == orderId);
                var orderReviews = query.ToList();
                //any review found?
                if (orderReviews.Count() == 0)
                    return false;
                //agent review found?
                var isActiveAgent = _settingService.GetSettingByKey<bool>("ratingreviewsettings.agent.isactive", true, storeId, true);
                if (isActiveAgent)
                {
                    if (orderReviews.Where(o => o.ReviewType == (int)Nop.Core.Domain.RatingReview.ReviewType.Agent)?.Count() == 0)
                        return false;
                }
                //merchant review found?
                var isActiveMerchant = _settingService.GetSettingByKey<bool>("ratingreviewsettings.merchant.isactive", true, storeId, true);
                if (isActiveMerchant)
                {
                    if (orderReviews.Where(o => o.ReviewType == (int)Nop.Core.Domain.RatingReview.ReviewType.Merchant)?.Count() == 0)
                        return false;
                }
                //all item review found?
                var isActiveItem = _settingService.GetSettingByKey<bool>("ratingreviewsettings.item.isactive", true, storeId, true);
                if (isActiveItem)
                {
                    if (orderReviews.Where(o => o.ReviewType == (int)Nop.Core.Domain.RatingReview.ReviewType.Item && o.EntityId > 0)?.Count() != order.OrderItems.Count())
                        return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Get value idicates is rating review enabled and rating found for merchant based on order
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public bool IsMerchantReviewEnabledAndFound(int orderId)
        {
            var storeId = _workContext.GetCurrentStoreId;
            var enabledAndFound = true;
            var isActiveMerchant = _settingService.GetSettingByKey<bool>("ratingreviewsettings.merchant.isactive", true, storeId, true);

            if (isActiveMerchant)
            {
                var isFound = _ratingReviewsRepository.Table.Where(x => x.OrderId == orderId
                                                          && x.ReviewType == (int)Nop.Core.Domain.RatingReview.ReviewType.Merchant).Any() ? true : false;
                enabledAndFound = isFound;
            }
            return enabledAndFound;
        }

        #endregion
    }
}
