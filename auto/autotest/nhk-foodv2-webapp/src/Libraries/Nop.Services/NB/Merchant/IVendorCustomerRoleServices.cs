﻿using System.Collections.Generic;
using Nop.Core.Domain.NB.Merchant;

namespace Nop.Services.NB.Merchant
{
    /// <summary>
    /// VendorCustomerRoleMapping service interface
    /// </summary>
    public interface IVendorCustomerRoleServices
    {
        /// <summary>
        /// Gets all vendor vendor customer role mapping mappings
        /// </summary>
        /// <returns>Vendor vendor customer role mapping mappings</returns>
        IList<VendorCustomerRoleMapping> GetAllVendorCustomerRoleMappings();

        /// <summary>
        /// Inserts a vendor customer role mapping
        /// </summary>
        /// <param name="VendorCustomerRoleMapping">vendor customer role mapping</param>
        void InsertVendorCustomerRoleMapping(VendorCustomerRoleMapping vendorCustomerRoleMapping);

        /// <summary>
        /// Updates the vendor customer role mapping
        /// </summary>
        /// <param name="VendorCustomerRoleMapping">vendor customer role mapping</param>
        void UpdateVendorCustomerRoleMapping(VendorCustomerRoleMapping vendorCustomerRoleMapping);

        /// <summary>
        /// Delete a vendor customer role mapping
        /// </summary>
        /// <param name="VendorCustomerRoleMapping">vendor customer role mapping</param>
        void DeleteVendorCustomerRoleMapping(VendorCustomerRoleMapping vendorCustomerRoleMapping);

        /// <summary>
        /// Gets all vendor vendor customer role mappings by customer role identifier
        /// </summary>
        /// <param name="id">identifier</param>
        /// <returns>Vendor vendor customer role mapping mappings</returns>
        VendorCustomerRoleMapping GetAllMappingsById(int id);

        /// <summary>
        /// Gets all vendor vendor customer role mappings by customer role identifier
        /// </summary>
        /// <param name="customerRoleId">customer role identifier</param>
        /// <returns>Vendor vendor customer role mapping mappings</returns>
        IList<VendorCustomerRoleMapping> GetAllMappingsByCustomerRoleId(int customerRoleId);
    }
}
