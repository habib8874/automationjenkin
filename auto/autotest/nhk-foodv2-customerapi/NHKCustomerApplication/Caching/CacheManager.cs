﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace NHKCustomerApplication.Caching
{
    /// <summary>
    /// Represents a memory cache manager 
    /// </summary>
    public partial class CacheManager : ICacheManager
    {
        #region Fields

        private readonly IMemoryCache _provider;

        #endregion

        #region Ctor

        public CacheManager(IMemoryCache provider)
        {
            _provider = provider;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get a cached item. If it's not in the cache yet, then load and cache it
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="key">Cache key</param>
        /// <param name="acquire">Function to load item if it's not in the cache yet</param>
        /// <param name="cacheTime">Cache time in minutes; pass 0 to do not cache; pass null to use the default time</param>
        /// <returns>The cached value associated with the specified key</returns>
        public T Get<T>(string key, Func<T> acquire, int? cacheTime = null)
        {
            if (cacheTime <= 0)
                return acquire();

            //if (!_provider.TryGetValue<T>(key, out T data))
            //{
            //    var cacheExpirationOptions = new MemoryCacheEntryOptions();
            //    cacheExpirationOptions.SlidingExpiration = TimeSpan.FromMinutes(cacheTime ?? 60);
            //    return _provider.Set<T>(key, acquire(), cacheExpirationOptions);
            //}
            return acquire();
        }

        /// <summary>
        /// Adds the specified key and object to the cache
        /// </summary>
        /// <param name="key">Key of cached item</param>
        /// <param name="data">Value for caching</param>
        /// <param name="cacheTime">Cache time in minutes</param>
        public void Set(string key, object data, int cacheTime)
        {
            if (cacheTime <= 0)
                return;

            var cacheExpirationOptions = new MemoryCacheEntryOptions();
            cacheExpirationOptions.AbsoluteExpiration = DateTime.Now.AddMinutes(cacheTime);
            cacheExpirationOptions.Priority = CacheItemPriority.Normal;
            _provider.Set(key, data, cacheExpirationOptions);
        }

        /// <summary>
        /// Gets a value indicating whether the value associated with the specified key is cached
        /// </summary>
        /// <param name="key">Key of cached item</param>
        /// <returns>True if item already is in cache; otherwise false</returns>
        public bool IsSet(string key)
        {
            return _provider.Get(key) != null;
        }

        /// <summary>
        /// Removes the value with the specified key from the cache
        /// </summary>
        /// <param name="key">Key of cached item</param>
        public void Remove(string key)
        {
            _provider.Remove(key);
        }

        /// <summary>
        /// Removes items by key prefix
        /// </summary>
        /// <param name="prefix">String key prefix</param>
        public void RemoveByPrefix(string prefix)
        {
            foreach (var key in GetCachedKeys().Where(x => x.StartsWith(prefix)))
                _provider.Remove(key);
        }

        /// <summary>
        /// Clear all cache data
        /// </summary>
        public void Clear()
        {
            foreach (var key in GetCachedKeys())
                _provider.Remove(key);
        }

        /// <summary>
        /// Get list of cache keys
        /// </summary>
        public List<string> GetCacheKeys(string prefix = "")
        {
            return typeof(CacheKeys).GetProperties().Select(x => Convert.ToString(x.GetValue(x.Name))).Where(x => string.IsNullOrEmpty(prefix) || x.StartsWith(prefix)).ToList();
        }

        /// <summary>
        /// Get cached keys
        /// </summary>
        /// <returns></returns>
        public List<string> GetCachedKeys()
        {
           var field = typeof(MemoryCache).GetProperty("EntriesCollection", BindingFlags.NonPublic | BindingFlags.Instance);
            var collection = field.GetValue(_provider) as dynamic;
            var items = new List<string>();
            if (collection != null)
            {
                foreach (var item in collection)
                {
                    var methodInfo = item.GetType().GetProperty("Key");
                    var val = methodInfo.GetValue(item);
                    items.Add(val.ToString());
                }
            }
            return items;
        }
        #endregion
    }
}