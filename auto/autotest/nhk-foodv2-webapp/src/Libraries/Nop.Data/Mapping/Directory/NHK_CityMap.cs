﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Directory;

namespace Nop.Data.Mapping.Directory
{
    /// <summary>
    /// Represents a state and province mapping configuration
    /// </summary>
    public partial class NHK_CityMap : NopEntityTypeConfiguration<NHK_City>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<NHK_City> builder)
        {
            builder.ToTable(nameof(NHK_City));
            builder.HasKey(state => state.Id);

            builder.Property(state => state.Name).HasMaxLength(100).IsRequired();
            builder.Property(state => state.Code).HasMaxLength(100);

            //builder.HasOne(state => state.StateProvince)
            //    .WithMany(city => city.City)
            //    .HasForeignKey(state => state.StateId)
            //    .IsRequired();

            base.Configure(builder);
        }

        #endregion
    }
}