﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB.VendorTokens;

namespace Nop.Data.Mapping.NB.VendorToken
{
    public partial class NBVendorTokenMap : NopEntityTypeConfiguration<NBVendorToken>
    {
        public override void Configure(EntityTypeBuilder<NBVendorToken> builder)
        {
            builder.ToTable(NopMappingDefaults.NBVendorToken);
            builder.HasKey(vt => vt.Id);
            base.Configure(builder);
        }
    }
}
