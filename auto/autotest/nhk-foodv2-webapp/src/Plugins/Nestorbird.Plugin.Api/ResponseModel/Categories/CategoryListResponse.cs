﻿using System.Collections.Generic;

namespace Nestorbird.Plugin.Api.ResponseModel.Categories
{
    public class CategoryListResponse : PagingResponse
    {
        public CategoryListResponse()
        {
            Categories = new List<CategoryResponse>();
        }

        public IList<CategoryResponse> Categories { get; set; }
    }

    public class CategoryResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int NumberOfProducts { get; set; }
        public string ImageUrl { get; set; }
    }
}
