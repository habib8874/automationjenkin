﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB.Merchant;

namespace Nop.Data.Mapping.NB.Merchant
{
    /// <summary>
    /// Represents a vendor-customer role mapping configuration
    /// </summary>
    public partial class VendorCustomerRoleMap : NopEntityTypeConfiguration<VendorCustomerRoleMapping>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<VendorCustomerRoleMapping> builder)
        {
            builder.ToTable(NopMappingDefaults.VendorCustomerRoleMappingTable);
            builder.HasKey(mapping => new { mapping.VendorId, mapping.CustomerRoleId });

            builder.Property(mapping => mapping.VendorId).HasColumnName("Vendor_Id");
            builder.Property(mapping => mapping.CustomerRoleId).HasColumnName("CustomerRole_Id");

            builder.HasOne(mapping => mapping.Vendor)
                .WithMany()
                .HasForeignKey(mapping => mapping.VendorId)
                .IsRequired();

            builder.HasOne(mapping => mapping.CustomerRole)
                .WithMany()
                .HasForeignKey(mapping => mapping.CustomerRoleId)
                .IsRequired();

            builder.Ignore(mapping => mapping.Id);

            base.Configure(builder);
        }

        #endregion
    }
}
