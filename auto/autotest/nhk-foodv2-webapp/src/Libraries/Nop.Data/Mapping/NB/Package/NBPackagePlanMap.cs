﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB.Package;
using Nop.Data.Mapping;

namespace NNop.Data.Mapping.NB.Package
{
    /// <summary>
    /// Represents a PackagePlan
    /// </summary>
    public partial class NBPackagePlanMap : NopEntityTypeConfiguration<NBPackagePlan>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<NBPackagePlan> builder)
        {
            builder.ToTable(NopMappingDefaults.PackagePlanTable);
            builder.HasKey(packageplan => packageplan.Id);
            base.Configure(builder);
        }

        #endregion
    }
}