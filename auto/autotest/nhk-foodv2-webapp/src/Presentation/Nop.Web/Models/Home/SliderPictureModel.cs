﻿using System.Collections.Generic;

namespace Nop.Web.Models.Home
{
    public partial class SliderPictureModel
    {
        public SliderPictureModel()
        {
            GeofaciningSettingIds = new List<int>();
        }
        public bool GetPicturesByGeofacing { get; set; }
        public List<int> GeofaciningSettingIds { get; set; }
    }
}
