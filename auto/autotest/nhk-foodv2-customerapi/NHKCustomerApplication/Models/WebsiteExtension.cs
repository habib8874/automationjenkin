﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class WebsiteExtension
    {
        public int Id { get; set; }
        public string Extension { get; set; }
    }
}
