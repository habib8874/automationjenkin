﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;

namespace Nop.Web.Components
{
    public class DisplayReviewsViewComponent : NopViewComponent
    {
        private readonly IWorkContext _workContext;
        private readonly ICatalogModelFactory _catalogModelFactory;

        public DisplayReviewsViewComponent(IWorkContext workContext,
            ICatalogModelFactory catalogModelFactory)
        {
            _workContext = workContext;
            _catalogModelFactory = catalogModelFactory;
        }

        public IViewComponentResult Invoke(int reviewType, int entityId = 0, int pageSize = 0)
        {
            if (reviewType == 0)
                return Content("");
            if (entityId == 0)
                return Content("");

            var model = _catalogModelFactory.PrepareReviewOverviewModel(reviewType, entityId, pageSize);

            if (model == null)
                return Content("");

            return View(model);
        }
    }
}
