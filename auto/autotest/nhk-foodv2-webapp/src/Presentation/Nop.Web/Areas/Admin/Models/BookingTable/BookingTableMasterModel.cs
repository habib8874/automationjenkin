﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.BookingTable
{
    public class BookingTableMasterModel: BaseNopEntityModel
    {
        public BookingTableMasterModel()
        {

        }
        public int StoreId { get; set; }
        public string Store { get; set; }
        public string Vendor { get; set; }
        public int VendorId { get; set; }
        public int TableNo { get; set; }
        public int Seats { get; set; }
        public bool Status { get; set; }
        public string Availabilty { get; set; }
        public DateTime CreatedDt { get; set; }
        public TimeSpan TableAvailableFrom { get; set; }
        public TimeSpan TableAvailableTo { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public string RestaurantImage { get; set; }
    }
}
