﻿using Newtonsoft.Json;

namespace Nop.Web.Areas.Admin.Models.NB.AccountsBilling
{
    public partial class StripeAccountDetailResponseModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("object")]
        public string Object { get; set; }

        [JsonProperty("business_profile")]
        public BusinessProfile BusinessProfile { get; set; }

        [JsonProperty("business_type")]
        public string BusinessType { get; set; }

        [JsonProperty("capabilities")]
        public Capabilities Capabilities { get; set; }

        [JsonProperty("charges_enabled")]
        public bool ChargesEnabled { get; set; }

        [JsonProperty("company")]
        public Company Company { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("created")]
        public long Created { get; set; }

        [JsonProperty("default_currency")]
        public string DefaultCurrency { get; set; }

        [JsonProperty("details_submitted")]
        public bool DetailsSubmitted { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("external_accounts")]
        public ExternalAccounts ExternalAccounts { get; set; }

        [JsonProperty("metadata")]
        public Metadata Metadata { get; set; }

        [JsonProperty("payouts_enabled")]
        public bool PayoutsEnabled { get; set; }

        [JsonProperty("requirements")]
        public Requirements Requirements { get; set; }

        [JsonProperty("settings")]
        public Settings Settings { get; set; }

        [JsonProperty("tos_acceptance")]
        public TosAcceptance TosAcceptance { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public partial class BusinessProfile
    {
        [JsonProperty("mcc")]
        public object Mcc { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("product_description")]
        public object ProductDescription { get; set; }

        [JsonProperty("support_address")]
        public object SupportAddress { get; set; }

        [JsonProperty("support_email")]
        public object SupportEmail { get; set; }

        [JsonProperty("support_phone")]
        public object SupportPhone { get; set; }

        [JsonProperty("support_url")]
        public object SupportUrl { get; set; }

        [JsonProperty("url")]
        public object Url { get; set; }
    }

    public partial class Capabilities
    {
        [JsonProperty("card_payments")]
        public string CardPayments { get; set; }

        [JsonProperty("transfers")]
        public string Transfers { get; set; }
    }

    public partial class Company
    {
        [JsonProperty("address")]
        public Address Address { get; set; }

        [JsonProperty("directors_provided")]
        public bool DirectorsProvided { get; set; }

        [JsonProperty("executives_provided")]
        public bool ExecutivesProvided { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("owners_provided")]
        public bool OwnersProvided { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("tax_id_provided")]
        public bool TaxIdProvided { get; set; }

        [JsonProperty("verification")]
        public Verification Verification { get; set; }
    }

    public partial class Address
    {
        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("line1")]
        public string Line1 { get; set; }

        [JsonProperty("line2")]
        public string Line2 { get; set; }

        [JsonProperty("postal_code")]
        public long PostalCode { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
    }

    public partial class Verification
    {
        [JsonProperty("document")]
        public Document Document { get; set; }
    }

    public partial class Document
    {
        [JsonProperty("back")]
        public object Back { get; set; }

        [JsonProperty("details")]
        public object Details { get; set; }

        [JsonProperty("details_code")]
        public object DetailsCode { get; set; }

        [JsonProperty("front")]
        public object Front { get; set; }
    }

    public partial class ExternalAccounts
    {
        [JsonProperty("object")]
        public string Object { get; set; }

        [JsonProperty("data")]
        public object[] Data { get; set; }

        [JsonProperty("has_more")]
        public bool HasMore { get; set; }

        [JsonProperty("total_count")]
        public long TotalCount { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public partial class Metadata
    {
    }

    public partial class Requirements
    {
        [JsonProperty("current_deadline")]
        public object CurrentDeadline { get; set; }

        [JsonProperty("currently_due")]
        public string[] CurrentlyDue { get; set; }

        [JsonProperty("disabled_reason")]
        public string DisabledReason { get; set; }

        [JsonProperty("errors")]
        public object[] Errors { get; set; }

        [JsonProperty("eventually_due")]
        public string[] EventuallyDue { get; set; }

        [JsonProperty("past_due")]
        public string[] PastDue { get; set; }

        [JsonProperty("pending_verification")]
        public object[] PendingVerification { get; set; }
    }

    public partial class Settings
    {
        [JsonProperty("bacs_debit_payments")]
        public Metadata BacsDebitPayments { get; set; }

        [JsonProperty("branding")]
        public Branding Branding { get; set; }

        [JsonProperty("card_payments")]
        public CardPayments CardPayments { get; set; }

        [JsonProperty("dashboard")]
        public Dashboard Dashboard { get; set; }

        [JsonProperty("payments")]
        public Payments Payments { get; set; }

        [JsonProperty("payouts")]
        public Payouts Payouts { get; set; }

        [JsonProperty("sepa_debit_payments")]
        public Metadata SepaDebitPayments { get; set; }
    }

    public partial class Branding
    {
        [JsonProperty("icon")]
        public object Icon { get; set; }

        [JsonProperty("logo")]
        public object Logo { get; set; }

        [JsonProperty("primary_color")]
        public object PrimaryColor { get; set; }

        [JsonProperty("secondary_color")]
        public object SecondaryColor { get; set; }
    }

    public partial class CardPayments
    {
        [JsonProperty("decline_on")]
        public DeclineOn DeclineOn { get; set; }

        [JsonProperty("statement_descriptor_prefix")]
        public object StatementDescriptorPrefix { get; set; }
    }

    public partial class DeclineOn
    {
        [JsonProperty("avs_failure")]
        public bool AvsFailure { get; set; }

        [JsonProperty("cvc_failure")]
        public bool CvcFailure { get; set; }
    }

    public partial class Dashboard
    {
        [JsonProperty("display_name")]
        public object DisplayName { get; set; }

        [JsonProperty("timezone")]
        public string Timezone { get; set; }
    }

    public partial class Payments
    {
        [JsonProperty("statement_descriptor")]
        public object StatementDescriptor { get; set; }

        [JsonProperty("statement_descriptor_kana")]
        public object StatementDescriptorKana { get; set; }

        [JsonProperty("statement_descriptor_kanji")]
        public object StatementDescriptorKanji { get; set; }
    }

    public partial class Payouts
    {
        [JsonProperty("debit_negative_balances")]
        public bool DebitNegativeBalances { get; set; }

        [JsonProperty("schedule")]
        public Schedule Schedule { get; set; }

        [JsonProperty("statement_descriptor")]
        public object StatementDescriptor { get; set; }
    }

    public partial class Schedule
    {
        [JsonProperty("delay_days")]
        public long DelayDays { get; set; }

        [JsonProperty("interval")]
        public string Interval { get; set; }
    }

    public partial class TosAcceptance
    {
        [JsonProperty("date")]
        public object Date { get; set; }

        [JsonProperty("ip")]
        public object Ip { get; set; }

        [JsonProperty("user_agent")]
        public object UserAgent { get; set; }
    }
}
