﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.BookingTable;

namespace Nop.Data.Mapping.BookingTableMap
{
    public partial class BookingTableTransactionMap : NopEntityTypeConfiguration<BookingTableTransaction>
    {
        public override void Configure(EntityTypeBuilder<BookingTableTransaction> builder)
        {
            builder.ToTable("BookingTableTransaction");
            builder.HasKey(p => p.Id);
            builder.Property(c => c.BookingDate).IsRequired();
            builder.Property(c => c.StoreId).IsRequired();
            builder.Property(c => c.VendorId).IsRequired();
            builder.Property(c => c.BookingTimeFrom).IsRequired();
            builder.Property(c => c.BookingTimeTo).IsRequired();
            builder.Property(c => c.CustomerName).IsRequired();
            builder.Property(c => c.Email).IsRequired();
            builder.Property(c => c.MobileNumber).IsRequired();
            builder.Property(c => c.TableNumber).IsRequired();
            builder.Property(c => c.CreatedDt).IsRequired();
            builder.Property(c => c.Status).IsRequired();
            builder.Ignore(c => c.SeatingCapacity);
            builder.Ignore(c => c.SpecialNotes);
            base.Configure(builder);
        }
    }
}