﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NHKCustomerApplication.ViewModels
{
    public class VersionViewModel
    {
    }
    public class VersionAPIResponse
    {
        public int Status { get; set; }
        public string ErrorMessageTitle { get; set; }
        public string ErrorMessage { get; set; }
        public string ApiVersion { get; set; }
        public string APIKey { get; set; }
        public bool TokenDispStatus { get; set; }
        public bool IsBaseSplashNeeded { get; set; }
        public bool IsMultiVendorSupported { get; set; }
        public string SplashURL { get; set; }
        public bool IsTutorialNeeded { get; set; }
        public string TutorialVersion { get; set; }
        public int TutorialCount { get; set; }
        public bool IsMultiLanguage { get; set; }
        public int LanguageCount { get; set; }
        public string DataBaseVersion { get; set; }
        public bool IsDispGoogMap { get; set; }
        public string DispGoogMapURL { get; set; }
        public bool IsSocketAvailable { get; set; }
        public string SocketURL { get; set; }
        public string SocketPort { get; set; }
        public bool IsDelTakeAwayOnDashboard { get; set; }
        public List<APIVerLangs> APIVerLangs { get; set; }
        public List<TutorialURLS> TutorialURLs { get; set; }
    }

    public class APIVerLangs
    {
        public int APIVerLangId { get; set; }
        public string APIVerLang { get; set; }
    }
    public class TutorialURLS
    {
        public int TutorialID { get; set; }
        public string TutorialURL { get; set; }
    }
    public class APIVersionV2
    {
        public int StatusCode { get; set; }
        public string ErrorMessageTitle { get; set; }
        public string ErrorMessage { get; set; }
        public bool Status { get; set; }
        public string ApiVersion { get; set; }
        public string ApiKey { get; set; }
        public decimal ResourceStringsVersion { get; set; }
        public bool TokenDispStatus { get; set; }
        public bool IsBaseSplashNeeded { get; set; }
        public bool IsMultiVendorSupported { get; set; }
        public string SplashURL { get; set; }
        public bool IsTutorialNeeded { get; set; }
        public string TutorialVersion { get; set; }
        public int TutorialCount { get; set; }
        public bool IsMultiLanguage { get; set; }
        public int LanguageCount { get; set; }
        public string DataBaseVersion { get; set; }
        public bool IsDispGoogMap { get; set; }
        public string DispGoogMapURL { get; set; }
        public bool IsSocketAvailable { get; set; }
        public string SocketURL { get; set; }
        public string SocketPort { get; set; }
        public bool IsDelTakeAwayOnDashboard { get; set; }
        public bool IsDelDeliveryOnDashboard { get; set; }
        public List<APIVersionLanguage> APIVerLangs { get; set; }
        public List<TutorialURLS> TutorialURLs { get; set; }
        public int StoreId { get; set; }
        public string GetHelp { get; set; }
        public string TermsCondition { get; set; }
        public string Privacy { get; set; }
        public string SupportChatUrl { get; set; }
        public string SupportPhone { get; set; }
        public string SupportEmail { get; set; }
        public string StoreCurrency { get; set; }
        public int RestnCookId { get; set; }
        public bool CheckStoreFront { get; set; }
        public bool IsStoreFront { get; set; }
        public string Seconds { get; set; }
        public string Minutes { get; set; }
        public string StoreCurrencyCode { get; set; }
        public string WebUrl { get; set; }
    }

    public class PublicKey
    {
        public string PublicKeyAuth { get; set; }
        public string DeviceOStype { get; set; }
        public string APKVersion { get; set; }
        public int StoreId { get; set; }
        public string AppName { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class SupportUrlModel
    {
        public int StoreId { get; set; }
        public string ApiKey { get; set; }

    }
    public class SupportUrlOPModel
    {
        public int StoreId { get; set; }
        public string GetHelp { get; set; }
        public string TermsCondition { get; set; }
        public string Privacy { get; set; }
        public string SupportChatUrl { get; set; }
        public string SupportPhone { get; set; }
        public string SupportEmail { get; set; }

    }
    public class APIVersionLanguage
    {
        public int APIVerLangId { get; set; }
        public string APIVerLang { get; set; }
    }
    public class CustomerAPIResponses
    {
        public int StatusCode { get; set; }
        public string ErrorMessageTitle { get; set; }
        public string ErrorMessage { get; set; }
        public bool Status { get; set; }
        public object ResponseObj { get; set; }
        public int TotalRecords { get; set; }

    }

    public class ItemsAPIResponses
    {
        public int TotalRecords { get; set; }
        public int StatusCode { get; set; }
        public string ErrorMessageTitle { get; set; }
        public string ErrorMessage { get; set; }
        public bool Status { get; set; }
        public object ResponseObj { get; set; }

    }
}
