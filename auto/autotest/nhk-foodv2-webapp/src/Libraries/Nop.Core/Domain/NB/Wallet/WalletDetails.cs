﻿using System;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;

namespace Nop.Core.Domain.NB
{
    /// <summary>
    /// Represents a OrderDetails
    /// </summary>
    public partial class WalletDetails : BaseEntity
    {
        public int CustomerId { get; set; }
        public int? OrderId { get; set; }
        public Decimal Amount { get; set; }
        public string Note { get; set; }
        public bool Credited { get; set; }
        public DateTime TransactionOnUtc { get; set; }
        public int TransactionBy { get; set; }
        public bool FromWeb { get; set; }
    }
}
