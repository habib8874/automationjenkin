﻿using System.Collections.Generic;

namespace Nop.Core.Domain.NB
{
    public class UserSubAdmin
    {
        public UserSubAdmin()
        {
            AssociatedVenorIds = new List<int>();
        }

        /// <summary>
        /// Value idicates wether current customer contain any of UserSubAdmin role (Merchant type or Business type)
        /// </summary>
        public bool IsSubAdminRole { get; set; }

        /// <summary>
        /// Mapped vendor id list
        public IList<int> AssociatedVenorIds { get; set; }

        /// <summary>
        /// Gets or sets a user group identifier. It's enum value (Business/Merchecnt) 
        /// </summary>
        public int UserGroupId { get; set; }
    }
}
