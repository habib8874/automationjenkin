﻿using FluentValidation;
using Nop.Web.Areas.Admin.Models.UserPushIdDetails;
using Nop.Core.Domain.UserPushIdDetails;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;


namespace Nop.Web.Areas.Admin.Validators.UserPushIdDetails
{
    public partial class UserPushIdDetailValidator : BaseNopValidator<UserPushIdDetailModel>
    {
        public UserPushIdDetailValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            RuleFor(x => x.UserId).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.UserPushIdDetails.Fields.UserId.Required"));
            RuleFor(x => x.Token).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.UserPushIdDetails.Fields.Token.Required"));

            SetDatabaseValidationRules<UserPushIdDetail>(dbContext);
        }
    }
}
