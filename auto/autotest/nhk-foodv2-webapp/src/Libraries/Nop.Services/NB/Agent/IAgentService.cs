﻿using System.Collections.Generic;
using System.Text;
using Nop.Core.Domain.NB.Agent;

namespace Nop.Services.NB.Agent
{
    /// <summary>
    /// Agent service interface
    /// </summary>
    public interface IAgentService
    {
        /// <summary>
        /// Get a AgentOrderGeoLocation
        /// </summary>
        /// <param name="FavoriteMerchant">favorite merchant(vendor)</param>
        AgentOrderGeoLocation GetAgentOrderGeoLocation(int agentId, int orderId);
    }
}
