﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Localization;

namespace Nop.Core.Domain.NB
{
    /// <summary>
    /// Represents a DeliverySlot
    /// </summary>
    public partial class DeliverySlot : BaseEntity, ILocalizedEntity
    {
        public string Name { get; set; }

        public bool IsDefaultSlot { get; set; }

        public int Duration { get; set; }

        public bool IsCustomDuration { get; set; }

        public int CutOffTime { get; set; }

        public DateTime StartDateUtc { get; set; }

        public DateTime EndDateUtc { get; set; }

        public TimeSpan StartTime { get; set; }

        public TimeSpan EndTime { get; set; }

        public int NumberOfOrders { get; set; }

        public decimal SurChargeFee { get; set; }

        public int DisplayOrder { get; set; }

        public int StoreId { get; set; }

        public int MerchantId { get; set; }

        public bool Published { get; set; }

        public bool Deleted { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public DateTime? UpdatedOnUtc { get; set; }
    }
}
