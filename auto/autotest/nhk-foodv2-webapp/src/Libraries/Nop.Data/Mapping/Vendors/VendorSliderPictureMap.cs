﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Vendors;

namespace Nop.Data.Mapping.Vendors
{
    public partial class VendorSliderPictureMap : NopEntityTypeConfiguration<VendorSliderPicture>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<VendorSliderPicture> builder)
        {
            builder.ToTable(NopMappingDefaults.VendorSliderPicture);
            builder.HasKey(vendorSliderPicture => vendorSliderPicture.Id);

            builder.HasOne(vendorSliderPicture => vendorSliderPicture.Picture)
                .WithMany()
                .HasForeignKey(vendorSliderPicture => vendorSliderPicture.PictureId)
                .IsRequired();

            builder.HasOne(vendorSliderPicture => vendorSliderPicture.Vendor)
                .WithMany(product => product.VendorSliderPictures)
                .HasForeignKey(vendorSliderPicture => vendorSliderPicture.VendorId)
                .IsRequired();

            base.Configure(builder);
        }

        #endregion
    }
}
