﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Reports
{
    /// <summary>
    /// Represents a bestseller brief search model
    /// </summary>
    public partial class BestsellerBriefSearchModel : BaseSearchModel
    {
        public BestsellerBriefSearchModel()
        {
            AvailableStores = new List<SelectListItem>();
            AvailableVendors = new List<SelectListItem>();
        }

        #region Properties

        //keep it synchronized to OrderReportService class, BestSellersReport() method, orderBy parameter
        //TODO: move from int to enum
        public int OrderBy { get; set; }

        public IList<SelectListItem> AvailableStores { get; set; }
        public int StoreId { get; set; }

        public IList<SelectListItem> AvailableVendors { get; set; }
        public int VendorId { get; set; }

        #endregion
    }
}