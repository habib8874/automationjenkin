﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class AgentInfo
    {
        public int Id { get; set; }
        public Guid CustomerGuid { get; set; }
        public string Password { get; set; }
        public int PasswordFormatId { get; set; }
        public string PasswordSalt { get; set; }
        public string Email { get; set; }
        public int RegisteredInStoreId { get; set; }
        public int Vendorid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ZipPostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string StateProvince { get; set; }
        public string ProfileUrl { get; set; }
        public string Rating { get; set; }
    }
}
