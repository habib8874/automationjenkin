﻿using System;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.AccountsBilling
{
    /// <summary>
    /// Represents a Account Transaction model
    /// </summary>
    public partial class AccountTransactionModel : BaseNopEntityModel
    {

        #region Properties

        [NopResourceDisplayName("NB.Admin.AccountBillings.AccountTransaction.Field.Amount")]
        public string Amount { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.AccountTransaction.Field.Description")]
        public string Description { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.AccountTransaction.Field.Date")]
        public DateTime Date { get; set; }

        #endregion
    }
}
