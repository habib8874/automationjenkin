﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.Package
{
    /// <summary>
    /// Represents a Package plan model
    /// </summary>
    public partial class PackagePlanModel : BaseNopEntityModel
    {
        #region Ctor

        public PackagePlanModel()
        {
            AvailableBillingInterval = new List<SelectListItem>();
            PackagePlanSearchModel = new PackagePlanSearchModel();
        }

        #endregion

        #region Properties

        public int? PackageId { get; set; }

        [NopResourceDisplayName("NB.Admin.Package.PackagePlan.Fields.PlanName")]
        public string PlanName { get; set; }

        [UIHint("DateNullable")]
        [NopResourceDisplayName("NB.Admin.Package.PackagePlan.Fields.StartDate")]
        public DateTime? StartDate { get; set; }

        [UIHint("DateNullable")]
        [NopResourceDisplayName("NB.Admin.Package.PackagePlan.Fields.EndDate")]
        public DateTime? EndDate { get; set; }

        [NopResourceDisplayName("NB.Admin.Package.PackagePlan.Fields.Price")]
        public decimal Price { get; set; }

        [NopResourceDisplayName("NB.Admin.Package.PackagePlan.Fields.BillingInterval")]
        public string BillingInterval { get; set; }
        public IList<SelectListItem> AvailableBillingInterval { get; set; }

        [NopResourceDisplayName("NB.Admin.Package.PackagePlan.Fields.TrailPeriod")]
        public string TrailPeriod { get; set; }

        public PackagePlanSearchModel PackagePlanSearchModel { get; set; }
        #endregion
    }

}