﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Vendors;

namespace Nop.Core.Domain.NB.Merchant
{
    /// <summary>
    /// Represents a vendor(merchant)-customer role mapping class
    /// </summary>
    public partial class VendorCustomerRoleMapping : BaseEntity
    {
        /// <summary>
        /// Gets or sets the vendor identifier
        /// </summary>
        public int VendorId { get; set; }

        /// <summary>
        /// Gets or sets the customer role identifier
        /// </summary>
        public int CustomerRoleId { get; set; }

        /// <summary>
        /// Gets or sets the vendor
        /// </summary>
        public virtual Vendor Vendor { get; set; }

        /// <summary>
        /// Gets or sets the customer role
        /// </summary>
        public virtual CustomerRole CustomerRole { get; set; }
    }
}
