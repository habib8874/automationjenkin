﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Vendors;

namespace Nop.Core.Domain.NB.MerchantCategorys
{
    /// <summary>
    /// Represents a MerchantCategory
    /// </summary>
    public partial class MerchantCategory : BaseEntity, ILocalizedEntity
    {
        //private ICollection<MerchantCategoryMapping> _merchantCategoryMappings;
        //private ICollection<Vendor> _vendors;

        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public int PictureId { get; set; }
        public int DisplayOrder { get; set; }
        public int StoreId { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the entity is published
        /// </summary>
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Gets or sets the date and time of product creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of product update
        /// </summary>
        public DateTime? UpdatedOnUtc { get; set; }

        ///// <summary>
        ///// Gets or sets product-product tag mappings
        ///// </summary>
        //public virtual ICollection<MerchantCategoryMapping> MerchantCategoryMappings
        //{
        //    get => _merchantCategoryMappings ?? (_merchantCategoryMappings = new List<MerchantCategoryMapping>());
        //    protected set => _merchantCategoryMappings = value;
        //}

        //public virtual ICollection<Vendor> Venodrs
        //{
        //    get { return _vendors ?? (_vendors = new List<Vendor>()); }
        //    protected set { _vendors = value; }
        //}
    }
}
