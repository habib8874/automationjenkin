﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB
{
    /// <summary>
    /// Represents a DeliverySlot model
    /// </summary>
    public partial class DeliverySlotModel : BaseNopEntityModel, ILocalizedModel<DeliverySlotLocalizedModel>
    {
        public DeliverySlotModel() 
        {
            AvailableStores = new List<SelectListItem>();
            AvailableMerchants = new List<SelectListItem>();
            Locales = new List<DeliverySlotLocalizedModel>();
            DeliverySlotMerchantSearchModel = new DeliverySlotMerchantSearchModel();
        }

        [NopResourceDisplayName("NB.Admin.DeliverySlot.Fields.Name")]
        public string Name { get; set; }
        [NopResourceDisplayName("NB.Admin.Catalog.DeliverySlot.Fields.IsDefaultSlot")]
        public bool IsDefaultSlot { get; set; }

        [NopResourceDisplayName("NB.Admin.Catalog.DeliverySlot.Fields.Duration")]
        public int Duration { get; set; }
        [NopResourceDisplayName("NB.Admin.Catalog.DeliverySlot.Fields.CutOffTime")]
        public int CutOffTime { get; set; }
        [NopResourceDisplayName("NB.Admin.DeliverySlot.Fields.DateRange")]
        [UIHint("Date")]
        public DateTime StartDateUtc { get; set; }
        [NopResourceDisplayName("NB.Admin.Catalog.DeliverySlot.Fields.EndDateUtc")]
        [UIHint("Date")]
        public DateTime EndDateUtc { get; set; }
        [NopResourceDisplayName("NB.Admin.DeliverySlot.Fields.TimeRange")]
        [UIHint("Time")]
        public string StartTime { get; set; }
        [NopResourceDisplayName("NB.Admin.Catalog.DeliverySlot.Fields.EndTime")]
        [UIHint("Time")]
        public string EndTime { get; set; }
        [NopResourceDisplayName("NB.Admin.Catalog.DeliverySlot.Fields.NumberOfOrders")]
        public int NumberOfOrders { get; set; }
        [NopResourceDisplayName("NB.Admin.Catalog.DeliverySlot.Fields.SurChargeFee")]
        public decimal SurChargeFee { get; set; }
        [NopResourceDisplayName("NB.Admin.DeliverySlot.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }
        [NopResourceDisplayName("NB.Admin.DeliverySlot.Fields.StoreId")]
        public int StoreId { get; set; }

        [NopResourceDisplayName("NB.Admin.DeliverySlot.Fields.MerchantId")]
        public int MerchantId { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the entity is published
        /// </summary>
        [NopResourceDisplayName("NB.Admin.DeliverySlot.Fields.Published")]
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }
        [NopResourceDisplayName("NB.Admin.DeliverySlot.Fields.StoreName")]
        public string StoreName { get; set; }
        [NopResourceDisplayName("NB.Admin.DeliverySlot.Fields.StoreList")]
        public IList<SelectListItem> AvailableStores { get; set; }

        public IList<SelectListItem> AvailableMerchants { get; set; }

        public IList<DeliverySlotLocalizedModel> Locales { get; set; }

        public DeliverySlotMerchantSearchModel DeliverySlotMerchantSearchModel { get; set; }

        [NopResourceDisplayName("NB.Admin.Catalog.DeliverySlot.Fields.Duration")]
        public string CustomDuration { get; set; }

        public bool IsCustomDuration { get; set; }
    }

    public partial class DeliverySlotLocalizedModel : ILocalizedLocaleModel
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("NB.Admin.DeliverySlot.Fields.Name")]
        public string Name { get; set; }
    }
}