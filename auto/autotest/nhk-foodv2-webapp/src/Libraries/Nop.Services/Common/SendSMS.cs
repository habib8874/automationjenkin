﻿using System;
using System.Linq;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Services.Directory;
using Nop.Services.Events;
using Microsoft.Extensions.Configuration;
using System.Net;
using System.Text;
using System.IO;

namespace Nop.Services.Common
{
    public partial class SendSMS
    {
        private readonly IConfiguration _configuration;
        public SendSMS(IConfiguration configuration)
        {
            this._configuration = configuration;
        }
        public virtual void SendSMSMessage(string storeName,string msgType,string msgFor,int countryCode, long customerMobileNo, string customerId = null, string cutomerName = null, string orderId = null, string agentId = null, string agentName = null, string agentMobileNo = null)
        {
            if (_configuration.GetSection("SMSMsgsSetting").GetSection("SendPermission").Value.Equals("True"))
            {
                //Your authentication key
                string authKey = "98038Ardljywf55c40c0b8";
                //Multiple mobiles numbers separated by comma
                string mobileNumber = "0";

                if (msgFor.Equals("Customer"))
                    mobileNumber = countryCode.ToString() + customerMobileNo.ToString();
                else
                    mobileNumber = countryCode.ToString() + agentMobileNo.ToString();

                //Sender ID,While using route4 sender id should be 6 characters long.
                //Get From APP Setting
                string senderId = _configuration.GetSection("SMSMsgsSetting").GetSection("SenderId").Value;
                string strMsg = "";
                if (msgType.Equals("Welcome"))
                    strMsg = _configuration.GetSection("CustSMSMsgs").GetSection("Welcome").Value.Replace("{CustomerName}", cutomerName).Replace("{StoreName}", storeName);
                else if (msgType.Equals("ChangePswd"))
                    strMsg = _configuration.GetSection("SMSMsgsSetting").GetSection("ChangePswd").Value;
                else if (msgType.Equals("NewOrder"))
                    strMsg = _configuration.GetSection("SMSMsgsSetting").GetSection("NewOrder").Value;
                else if (msgType.Equals("OrderAccepted"))
                    strMsg = _configuration.GetSection("SMSMsgsSetting").GetSection("OrderAccepted").Value;
                else if (msgType.Equals("OrderPrepared"))
                    strMsg = _configuration.GetSection("SMSMsgsSetting").GetSection("OrderPrepared").Value;
                else if (msgType.Equals("OrderPicked"))
                    strMsg = _configuration.GetSection("SMSMsgsSetting").GetSection("OrderPicked").Value;
                else if (msgType.Equals("OrderDelivered"))
                    strMsg = _configuration.GetSection("SMSMsgsSetting").GetSection("OrderDelivered").Value;
                else if (msgType.Equals("OrderCancelled"))
                    strMsg = _configuration.GetSection("SMSMsgsSetting").GetSection("OrderCancelled").Value;

                //Your message to send, Add URL encoding here.
                string message = System.Net.WebUtility.UrlEncode(strMsg);

                //Prepare you post parameters
                StringBuilder sbPostData = new StringBuilder();
                sbPostData.AppendFormat("authkey={0}", authKey);
                sbPostData.AppendFormat("&mobiles={0}", mobileNumber);
                sbPostData.AppendFormat("&message={0}", message);
                sbPostData.AppendFormat("&sender={0}", senderId);
                sbPostData.AppendFormat("&route={0}", "4");
                try
                {
                    //Call Send SMS API
                    string sendSMSUri = "http://api.msg91.com/api/sendhttp.php";
                    //Create HTTPWebrequest
                    HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                    //Prepare and Add URL Encoded data
                    UTF8Encoding encoding = new UTF8Encoding();
                    byte[] data = encoding.GetBytes(sbPostData.ToString());
                    //Specify post method
                    httpWReq.Method = "POST";
                    httpWReq.ContentType = "application/x-www-form-urlencoded";
                    httpWReq.ContentLength = data.Length;
                    using (Stream stream = httpWReq.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                    //Get the response
                    HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    string responseString = reader.ReadToEnd();

                    //Close the response
                    reader.Close();
                    response.Close();
                    //return "1";
                }
                catch (SystemException ex)
                {
                    string exSMS = ex.Message.ToString();
                }
            }
        }
    }
}
