﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;


namespace Nop.Web.Controllers
{
    public class NewHomeController : Controller
    {
        [HttpsRequirement(SslRequirement.No)]
        public IActionResult Index()
        {
            return View();
        }
    }
}