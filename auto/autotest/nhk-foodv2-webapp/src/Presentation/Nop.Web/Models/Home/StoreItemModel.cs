﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Models.Home
{
    public class StoreItemModel
    {
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhoneNumber { get; set; }
        public bool IsDisplayDeliveryOrTakeaway { get; set; }
        public bool IsAggrigator { get; set; }
        public bool IsDelivery { get; set; }
        public bool IsTakeaway { get; set; }
        public bool IsDinning { get; set; }
        public int CountryId { get; set; }
    }
}
