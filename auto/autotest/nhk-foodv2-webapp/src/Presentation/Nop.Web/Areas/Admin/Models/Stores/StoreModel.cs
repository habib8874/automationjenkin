﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Areas.Admin.Models.Customers;

namespace Nop.Web.Areas.Admin.Models.Stores
{
    /// <summary>
    /// Represents a store model
    /// </summary>
    public partial class StoreModel : BaseNopEntityModel, ILocalizedModel<StoreLocalizedModel>
    {
        #region Ctor

        public StoreModel()
        {
            AvailableTimeZones = new List<SelectListItem>();
            Locales = new List<StoreLocalizedModel>();
            AvailableLanguages = new List<SelectListItem>();
            AvailableStoreOwnerId = new List<SelectListItem>();
            AvailableCountryId = new List<SelectListItem>();
            CountryStateSearchModel = new CountryStateSearchModel();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.Url")]
        public string Url { get; set; }

        //time zone
        [NopResourceDisplayName("Admin.Customers.Customers.Fields.TimeZoneId")]
        public string TimeZoneId { get; set; }
        public IList<SelectListItem> AvailableTimeZones { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.SslEnabled")]
        public virtual bool SslEnabled { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.Hosts")]
        public string Hosts { get; set; }

        //default language
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.DefaultLanguage")]
        public int DefaultLanguageId { get; set; }

        public IList<SelectListItem> AvailableLanguages { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.CompanyName")]
        public string CompanyName { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.CompanyAddress")]
        public string CompanyAddress { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.CompanyPhoneNumber")]
        public string CompanyPhoneNumber { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.CompanyVat")]
        public string CompanyVat { get; set; }
        
        public IList<StoreLocalizedModel> Locales { get; set; }

        #region Custom code from v4.0
        //Create By Mah
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.StoreOwnerId")]
        public int StoreOwnerId { get; set; }

        //Create By Mah for diplay at minicart option
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.IsDisplayDeliveryOrTakeaway")]
        public bool IsDisplayDeliveryOrTakeaway { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.IsAggrigator")]
        public bool IsAggrigator { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.IsDelivery")]
        public bool IsDelivery { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.IsTakeaway")]
        public bool IsTakeaway { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.IsDinning")]
        public bool IsDinning { get; set; }

        public IList<SelectListItem> AvailableStoreOwnerId { get; set; }
        public IList<SelectListItem> AvailableCountryId { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.CountryId")]
        public int CountryId { get; set; }

        public CountryStateSearchModel CountryStateSearchModel { get; set; }

        // Adding new field for package plan 
        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.IsRegisterAsMerchant")]
        public bool IsRegisterAsMerchant { get; set; }

        // Adding new field for Resturant plan 
        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.RestaurantRegisterAllow")]
        public bool RestaurantRegisterAllow { get; set; }

        #endregion

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.WalletEnabled")]
        public virtual bool WalletEnabled { get; set; }
        #endregion
    }

    public partial class StoreLocalizedModel : ILocalizedLocaleModel
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.Name")]
        public string Name { get; set; }
    }

    #region Country State Mapping
    public partial class CountryStateSearchModel : BaseSearchModel
    {
        #region Properties
        public int CountryId { get; set; }
        public int StoreId { get; set; }
        #endregion
    }

    public partial class CountryStateListModel : BasePagedListModel<CountryStateModel>
    {
    }

    public partial class CountryStateModel : BaseNopEntityModel
    {
        #region Properties

        public int StoreId { get; set; }
        public int CountryId { get; set; }

        [NopResourceDisplayName("Admin.Store.CountryState.Fields.CountryName")]
        public string CountryName { get; set; }

        public int StateId { get; set; }

        [NopResourceDisplayName("Admin.Store.CountryState.Fields.StateName")]
        public string StateName { get; set; }

        [NopResourceDisplayName("Admin.Store.CountryState.Fields.IsActive")]
        public bool IsActive { get; set; }

        [NopResourceDisplayName("Admin.Store.CountryState.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        #endregion
    }
    #endregion
}