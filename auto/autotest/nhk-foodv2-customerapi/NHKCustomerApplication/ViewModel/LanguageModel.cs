﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NHKCustomerApplication.ViewModels
{
    public partial class AllStoreLanguagesModel
    {
        public AllStoreLanguagesModel()
        {
            AllLanguages = new List<LanguagesModel>();
        }
        public string DefaultLanguageCode { get; set; }
        public List<LanguagesModel> AllLanguages { get; set; }
    }

    public partial class LanguagesModel
    {
        public int Id { get; set; }
        public string UniqueSeoCode { get; set; }
        public string Name { get; set; }
    }

    public partial class AllStoreLanguagesReqModel
    {
        public int CustomerId { get; set; }
        public string ApiKey { get; set; }
        public string UniqueSeoCode { get; set; }
        public int StoreId { get; set; }
    }

    public partial class AllStoreLanguagesResponseModel
    {
        public int StatusCode { get; set; }
        public string ErrorMessageTitle { get; set; }
        public string ErrorMessage { get; set; }
        public bool Status { get; set; }
        public object ResponseObj { get; set; }

    }
    public partial class TopicReqModel
    {
        public string ApiKey { get; set; }
        public string UniqueSeoCode { get; set; }
        public string TopicType { get; set; }
        public int StoreId { get; set; }
    }
    public partial class TopicDetailsModel
    {
        public string Heading { get; set; }
        public string BodyContent { get; set; }
    }

    //Local resource strings
    public partial class LocaleStringResourceModel
    {
        public string ResourceName { get; set; }
        public string ResourceValue { get; set; }
    }

    public partial class LocaleStringResourceReqModel
    {
            public int CustomerId { get; set; }
            public string ApiKey { get; set; }
            public string UniqueSeoCode { get; set; }
            public string PrefixValue { get; set; }
            public int StoreId { get; set; }
    }

    public partial class LocaleStringResourceResponseModel
    {
        public int StatusCode { get; set; }
        public string ErrorMessageTitle { get; set; }
        public string ErrorMessage { get; set; }
        public bool Status { get; set; }
        public object ResponseObj { get; set; }

    }
}
