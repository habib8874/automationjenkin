﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Http.Extensions;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Web.Extensions;
using Nop.Web.Factories;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Checkout;
using System.IO;
using System.Net;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Nop.Data;
using Nop.Web.Areas.Admin.Models.SMS;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using IAuthenticationService = Nop.Services.Authentication.IAuthenticationService;
using Nop.Services.Events;
using Nop.Core.Infrastructure;
using Nop.Core.Data;
using Nop.Core.Domain.CustomEntities;
using Nop.Core.Data.Extensions;
using Nop.Core.Domain.Tax;
using Nop.Services.Vendors;
using Nop.Services.Configuration;
using Nop.Services.Catalog;
using Nop.Core.Domain;
using Nop.Services.Messages;
using Nop.Web.Models.Customer;
using Nop.Web.Models.Common;
using Nop.Web.Models.ShoppingCart;
using Nop.Web.Framework.Themes;
using Nop.Core.Domain.NB.Address;
using Nop.Services.NB;
using Nop.Web.Models.NB;
using Nop.Services.Stores;
using System.Globalization;
using Nop.Core.Domain.NB;
using Nop.Core.Domain.NB.VendorTokens;
using Nop.Core.Domain.NB.Common;
using Nop.Services.NB.VendorTokens;

namespace Nop.Web.Controllers
{
    [HttpsRequirement(SslRequirement.Yes)]
    public partial class CheckoutController : BasePublicController
    {
        #region Fields

        private readonly AddressSettings _addressSettings;
        private readonly CustomerSettings _customerSettings;
        private readonly IAddressAttributeParser _addressAttributeParser;
        private readonly IAddressService _addressService;
        private readonly ICheckoutModelFactory _checkoutModelFactory;
        private readonly ICountryService _countryService;
        private readonly ICustomerService _customerService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly ILogger _logger;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly IPaymentPluginManager _paymentPluginManager;
        private readonly IPaymentService _paymentService;
        private readonly IShippingService _shippingService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IStoreContext _storeContext;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly OrderSettings _orderSettings;
        private readonly PaymentSettings _paymentSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly IDbContext _dbContext;
        private readonly IConfiguration _configuration;
        private readonly IAuthenticationService _authenticationService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly ICommonService _commonService;
        private readonly TaxSettings _taxSettings;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly ICurrencyService _currencyService;
        private readonly ISettingService _settingService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly ICommonModelFactory _commonModelFactory;
        private readonly IAddressModelFactory _addressModelFactory;
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;
        private readonly IThemeContext _themeContext;
        private readonly IDeliverySlotService _deliverySlotService;
        private readonly IStoreService _storeService;
        private readonly IProductService _productService;
        private readonly INBVendorTokenService _nBVendorTokenService;

        #endregion

        #region Ctor

        public CheckoutController(AddressSettings addressSettings,
            CustomerSettings customerSettings,
            IAddressAttributeParser addressAttributeParser,
            IAddressService addressService,
            ICheckoutModelFactory checkoutModelFactory,
            ICountryService countryService,
            ICustomerService customerService,
            IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            ILogger logger,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IPaymentPluginManager paymentPluginManager,
            IPaymentService paymentService,
            IShippingService shippingService,
            IShoppingCartService shoppingCartService,
            IStateProvinceService stateProvinceService,
            IStoreContext storeContext,
            IWebHelper webHelper,
            IWorkContext workContext,
            OrderSettings orderSettings,
            PaymentSettings paymentSettings,
            RewardPointsSettings rewardPointsSettings,
            ShippingSettings shippingSettings,
            IDbContext dbContext,
            IConfiguration configuration,
            IAuthenticationService authenticationService,
            IEventPublisher eventPublisher,
            ICustomerActivityService customerActivityService,
            ICustomerRegistrationService customerRegistrationService,
            ICommonService commonService,
            TaxSettings taxSettings,
            IOrderTotalCalculationService orderTotalCalculationService,
            ICurrencyService currencyService,
            ISettingService settingService,
            IPriceFormatter priceFormatter,
            StoreInformationSettings storeInformationSettings,
            IWorkflowMessageService workflowMessageService,
            ICommonModelFactory commonModelFactory,
            IAddressModelFactory addressModelFactory,
            IShoppingCartModelFactory shoppingCartModelFactory,
            IThemeContext themeContext,
            IDeliverySlotService deliverySlotService,
            IStoreService storeService,
            IProductService productService,
            INBVendorTokenService nBVendorTokenService)
        {
            _addressSettings = addressSettings;
            _customerSettings = customerSettings;
            _addressAttributeParser = addressAttributeParser;
            _addressService = addressService;
            _checkoutModelFactory = checkoutModelFactory;
            _countryService = countryService;
            _customerService = customerService;
            _genericAttributeService = genericAttributeService;
            _localizationService = localizationService;
            _logger = logger;
            _orderProcessingService = orderProcessingService;
            _orderService = orderService;
            _paymentPluginManager = paymentPluginManager;
            _paymentService = paymentService;
            _shippingService = shippingService;
            _shoppingCartService = shoppingCartService;
            _stateProvinceService = stateProvinceService;
            _storeContext = storeContext;
            _webHelper = webHelper;
            _workContext = workContext;
            _orderSettings = orderSettings;
            _paymentSettings = paymentSettings;
            _rewardPointsSettings = rewardPointsSettings;
            _shippingSettings = shippingSettings;
            _dbContext = dbContext;
            _configuration = configuration;
            _authenticationService = authenticationService;
            _eventPublisher = eventPublisher;
            _customerActivityService = customerActivityService;
            _customerRegistrationService = customerRegistrationService;
            _commonService = commonService;
            _taxSettings = taxSettings;
            _orderTotalCalculationService = orderTotalCalculationService;
            _currencyService = currencyService;
            _settingService = settingService;
            _priceFormatter = priceFormatter;
            _storeInformationSettings = storeInformationSettings;
            _workflowMessageService = workflowMessageService;
            _commonModelFactory = commonModelFactory;
            _addressModelFactory = addressModelFactory;
            _shoppingCartModelFactory = shoppingCartModelFactory;
            _themeContext = themeContext;
            _deliverySlotService = deliverySlotService;
            _storeService = storeService;
            _productService = productService;
            _nBVendorTokenService = nBVendorTokenService;
        }

        #endregion

        #region Utilities
        protected void RegistrationOTP(Customer customer)
        {
            var phone = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute, customer.RegisteredInStoreId, "");
            if (!phone.Contains(" "))
                phone = _workContext.CurrentStoreISDCode + " " + phone;


            string sRandomOTP = _workContext.GenerateOTP;

            _genericAttributeService.SaveAttribute(customer,
            "RegistrationOTP", sRandomOTP, _workContext.GetCurrentStoreId);

            _workflowMessageService.SendCustomerOTPOnPhone(customer, phone, _workContext.WorkingLanguage.Id, sRandomOTP);
            _workflowMessageService.SendCustomerOTPOnEmail(customer, customer.Email, _workContext.WorkingLanguage.Id, sRandomOTP);
        }

        private IActionResult LoadStepAfterSignUp(Customer customer)
        {
            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, _workContext.GetCurrentStoreId);
            _workContext.CurrentCustomer = customer;
            var opcModel = _checkoutModelFactory.PrepareOnePageCheckoutModel(cart);
            var billingAddressModel = opcModel.BillingAddress;
            var headerLinkModel = _commonModelFactory.PrepareHeaderLinksModel();
            return Json(new
            {
                update_section = new UpdateSectionJsonModel
                {
                    name = "billing",
                    html = RenderPartialViewToString("OpcBillingAddress", billingAddressModel)
                },
                goto_section = "billing",
                changeHeaderLInk = true,
                headerLink = RenderPartialViewToString("_headerLinks", headerLinkModel)
            });
        }

        protected virtual bool IsMinimumOrderPlacementIntervalValid(Customer customer)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;  //_checkoutAttributeService.GetCurrentStoreId();            

            //prevent 2 orders being placed within an X seconds time frame
            if (_orderSettings.MinimumOrderPlacementInterval == 0)
                return true;

            var lastOrder = _orderService.SearchOrders(storeId: CurrStoreId,
                customerId: _workContext.CurrentCustomer.Id, pageSize: 1)
                .FirstOrDefault();
            if (lastOrder == null)
                return true;

            var interval = DateTime.UtcNow - lastOrder.CreatedOnUtc;
            return interval.TotalSeconds > _orderSettings.MinimumOrderPlacementInterval;
        }

        /// <summary>
        /// Generate an order GUID
        /// </summary>
        /// <param name="processPaymentRequest">Process payment request</param>
        protected virtual void GenerateOrderGuid(ProcessPaymentRequest processPaymentRequest)
        {
            if (processPaymentRequest == null)
                return;

            //we should use the same GUID for multiple payment attempts
            //this way a payment gateway can prevent security issues such as credit card brute-force attacks
            //in order to avoid any possible limitations by payment gateway we reset GUID periodically
            var previousPaymentRequest = HttpContext.Session.Get<ProcessPaymentRequest>("OrderPaymentInfo");
            if (_paymentSettings.RegenerateOrderGuidInterval > 0 &&
                previousPaymentRequest != null &&
                previousPaymentRequest.OrderGuidGeneratedOnUtc.HasValue)
            {
                var interval = DateTime.UtcNow - previousPaymentRequest.OrderGuidGeneratedOnUtc.Value;
                if (interval.TotalSeconds < _paymentSettings.RegenerateOrderGuidInterval)
                {
                    processPaymentRequest.OrderGuid = previousPaymentRequest.OrderGuid;
                    processPaymentRequest.OrderGuidGeneratedOnUtc = previousPaymentRequest.OrderGuidGeneratedOnUtc;
                }
            }

            if (processPaymentRequest.OrderGuid == Guid.Empty)
            {
                processPaymentRequest.OrderGuid = Guid.NewGuid();
                processPaymentRequest.OrderGuidGeneratedOnUtc = DateTime.UtcNow;
            }
        }

        #endregion

        #region Methods (common)

        public virtual IActionResult Index()
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;  //_checkoutAttributeService.GetCurrentStoreId();            

            //UseCustomerWalletDuring
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, NopCustomerDefaults.UseCustomerWalletDuringCheckoutAttribute, false, CurrStoreId);

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            var downloadableProductsRequireRegistration =
                _customerSettings.RequireRegistrationForDownloadableProducts && cart.Any(sci => sci.Product.IsDownload);

            if (_workContext.CurrentCustomer.IsGuest() && (!_orderSettings.AnonymousCheckoutAllowed || downloadableProductsRequireRegistration))
                return Challenge();

            //if we have only "button" payment methods available (displayed onthe shopping cart page, not during checkout),
            //then we should allow standard checkout
            //all payment methods (do not filter by country here as it could be not specified yet)
            var paymentMethods = _paymentPluginManager
                .LoadActivePlugins(_workContext.CurrentCustomer, CurrStoreId)
                .Where(pm => !pm.HidePaymentMethod(cart)).ToList();
            //payment methods displayed during checkout (not with "Button" type)
            var nonButtonPaymentMethods = paymentMethods
                .Where(pm => pm.PaymentMethodType != PaymentMethodType.Button)
                .ToList();
            //"button" payment methods(*displayed on the shopping cart page)
            var buttonPaymentMethods = paymentMethods
                .Where(pm => pm.PaymentMethodType == PaymentMethodType.Button)
                .ToList();
            if (!nonButtonPaymentMethods.Any() && buttonPaymentMethods.Any())
                return RedirectToRoute("ShoppingCart");

            //reset checkout data
            _customerService.ResetCheckoutData(_workContext.CurrentCustomer, CurrStoreId);

            //validation (cart)
            var checkoutAttributesXml = _genericAttributeService.GetAttribute<string>(_workContext.CurrentCustomer,
                NopCustomerDefaults.CheckoutAttributes, CurrStoreId);
            var scWarnings = _shoppingCartService.GetShoppingCartWarnings(cart, checkoutAttributesXml, true);
            if (scWarnings.Any())
                return RedirectToRoute("ShoppingCart");
            //validation (each shopping cart item)
            foreach (var sci in cart)
            {
                var sciWarnings = _shoppingCartService.GetShoppingCartItemWarnings(_workContext.CurrentCustomer,
                    sci.ShoppingCartType,
                    sci.Product,
                    sci.StoreId,
                    sci.AttributesXml,
                    sci.CustomerEnteredPrice,
                    sci.RentalStartDateUtc,
                    sci.RentalEndDateUtc,
                    sci.Quantity,
                    false,
                    sci.Id);
                if (sciWarnings.Any())
                    return RedirectToRoute("ShoppingCart");
            }

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            return RedirectToRoute("CheckoutBillingAddress");
        }

        public virtual IActionResult Completed(int? orderId)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            Order order = null;
            if (orderId.HasValue)
            {
                //load order by identifier (if provided)
                order = _orderService.GetOrderById(orderId.Value);
            }
            if (order == null)
            {
                order = _orderService.SearchOrders(storeId: CurrStoreId,
                customerId: _workContext.CurrentCustomer.Id, pageSize: 1)
                    .FirstOrDefault();
            }
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
            {
                return RedirectToRoute("Homepage");
            }

            //disable "order completed" page?
            if (_orderSettings.DisableOrderCompletedPage)
            {
                return RedirectToRoute("OrderDetails", new { orderId = order.Id });
            }

            //model
            var model = _checkoutModelFactory.PrepareCheckoutCompletedModel(order);

            return View(model);
        }

        #endregion

        #region Methods (multistep checkout)

        public virtual IActionResult BillingAddress(IFormCollection form)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            //model
            var model = _checkoutModelFactory.PrepareBillingAddressModel(cart, prePopulateNewAddressWithCustomerFields: true);

            //check whether "billing address" step is enabled
            if (_orderSettings.DisableBillingAddressCheckoutStep && model.ExistingAddresses.Any())
            {
                if (model.ExistingAddresses.Any())
                {
                    //choose the first one
                    return SelectBillingAddress(model.ExistingAddresses.First().Id);
                }

                TryValidateModel(model);
                TryValidateModel(model.BillingNewAddress);
                return NewBillingAddress(model, form);
            }

            return View(model);
        }

        public virtual IActionResult SelectBillingAddress(int addressId, bool shipToSameAddress = false)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                return RedirectToRoute("CheckoutBillingAddress");

            _workContext.CurrentCustomer.BillingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            //ship to the same address?
            //by default Shipping is available if the country is not specified
            var shippingAllowed = _addressSettings.CountryEnabled ? address.Country?.AllowsShipping ?? false : true;
            if (_shippingSettings.ShipToSameAddress && shipToSameAddress && _shoppingCartService.ShoppingCartRequiresShipping(cart) && shippingAllowed)
            {
                _workContext.CurrentCustomer.ShippingAddress = _workContext.CurrentCustomer.BillingAddress;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                //reset selected shipping method (in case if "pick up in store" was selected)
                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedShippingOptionAttribute, null, CurrStoreId);
                _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedPickupPointAttribute, null, CurrStoreId);
                //limitation - "Ship to the same address" doesn't properly work in "pick up in store only" case (when no shipping plugins are available) 
                return RedirectToRoute("CheckoutShippingMethod");
            }

            return RedirectToRoute("CheckoutShippingAddress");
        }

        [HttpPost, ActionName("BillingAddress")]
        [FormValueRequired("nextstep")]
        public virtual IActionResult NewBillingAddress(CheckoutBillingAddressModel model, IFormCollection form)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            //custom address attributes
            var customAttributes = _addressAttributeParser.ParseCustomAddressAttributes(form);
            var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
            foreach (var error in customAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            var newAddress = model.BillingNewAddress;

            if (ModelState.IsValid)
            {
                //try to find an address with the same values (don't duplicate records)
                var address = _addressService.FindAddress(_workContext.CurrentCustomer.Addresses.ToList(),
                    newAddress.FirstName, newAddress.LastName, newAddress.PhoneNumber,
                    newAddress.Email, newAddress.FaxNumber, newAddress.Company,
                    newAddress.Address1, newAddress.Address2, newAddress.City,
                    newAddress.County, newAddress.StateProvinceId, newAddress.ZipPostalCode,
                    newAddress.CountryId, customAttributes);
                if (address == null)
                {
                    //address is not found. let's create a new one
                    address = newAddress.ToEntity();
                    address.CustomAttributes = customAttributes;
                    address.CreatedOnUtc = DateTime.UtcNow;
                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;
                    //_workContext.CurrentCustomer.Addresses.Add(address);
                    _workContext.CurrentCustomer.CustomerAddressMappings.Add(new CustomerAddressMapping { Address = address });
                }
                _workContext.CurrentCustomer.BillingAddress = address;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                //ship to the same address?
                if (_shippingSettings.ShipToSameAddress && model.ShipToSameAddress && _shoppingCartService.ShoppingCartRequiresShipping(cart))
                {
                    _workContext.CurrentCustomer.ShippingAddress = _workContext.CurrentCustomer.BillingAddress;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                    //reset selected shipping method (in case if "pick up in store" was selected)
                    _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedShippingOptionAttribute, null, CurrStoreId);
                    _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedPickupPointAttribute, null, CurrStoreId);
                    //limitation - "Ship to the same address" doesn't properly work in "pick up in store only" case (when no shipping plugins are available) 
                    return RedirectToRoute("CheckoutShippingMethod");
                }

                return RedirectToRoute("CheckoutShippingAddress");
            }

            //If we got this far, something failed, redisplay form
            model = _checkoutModelFactory.PrepareBillingAddressModel(cart,
                selectedCountryId: newAddress.CountryId,
                overrideAttributesXml: customAttributes);
            return View(model);
        }

        public virtual IActionResult ShippingAddress()
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            if (!_shoppingCartService.ShoppingCartRequiresShipping(cart))
            {
                _workContext.CurrentCustomer.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                return RedirectToRoute("CheckoutShippingMethod");
            }

            //model
            var model = _checkoutModelFactory.PrepareShippingAddressModel(prePopulateNewAddressWithCustomerFields: true);
            return View(model);
        }

        public virtual IActionResult SelectShippingAddress(int addressId)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                return RedirectToRoute("CheckoutShippingAddress");

            _workContext.CurrentCustomer.ShippingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            if (_shippingSettings.AllowPickupInStore)
            {
                //set value indicating that "pick up in store" option has not been chosen
                _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedPickupPointAttribute, null, CurrStoreId);
            }

            return RedirectToRoute("CheckoutShippingMethod");
        }

        [HttpPost, ActionName("ShippingAddress")]
        [FormValueRequired("nextstep")]
        public virtual IActionResult NewShippingAddress(CheckoutShippingAddressModel model, IFormCollection form)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            if (!_shoppingCartService.ShoppingCartRequiresShipping(cart))
            {
                _workContext.CurrentCustomer.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                return RedirectToRoute("CheckoutShippingMethod");
            }

            //pickup point
            if (_shippingSettings.AllowPickupInStore)
            {
                if (model.PickupInStore)
                {
                    //no shipping address selected
                    _workContext.CurrentCustomer.ShippingAddress = null;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                    var pickupPoint = form["pickup-points-id"].ToString().Split(new[] { "___" }, StringSplitOptions.None);
                    var pickupPoints = _shippingService.GetPickupPoints(_workContext.CurrentCustomer.BillingAddress,
                        _workContext.CurrentCustomer, pickupPoint[1], CurrStoreId).PickupPoints.ToList();
                    var selectedPoint = pickupPoints.FirstOrDefault(x => x.Id.Equals(pickupPoint[0]));
                    if (selectedPoint == null)
                        return RedirectToRoute("CheckoutShippingAddress");

                    var pickUpInStoreShippingOption = new ShippingOption
                    {
                        Name = string.Format(_localizationService.GetResource("Checkout.PickupPoints.Name"), selectedPoint.Name),
                        Rate = selectedPoint.PickupFee,
                        Description = selectedPoint.Description,
                        ShippingRateComputationMethodSystemName = selectedPoint.ProviderSystemName
                    };

                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedShippingOptionAttribute, pickUpInStoreShippingOption, CurrStoreId);
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedPickupPointAttribute, selectedPoint, CurrStoreId);

                    return RedirectToRoute("CheckoutPaymentMethod");
                }

                //set value indicating that "pick up in store" option has not been chosen
                _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedPickupPointAttribute, null, CurrStoreId);
            }

            //custom address attributes
            var customAttributes = _addressAttributeParser.ParseCustomAddressAttributes(form);
            var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
            foreach (var error in customAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            var newAddress = model.ShippingNewAddress;

            if (ModelState.IsValid)
            {
                //try to find an address with the same values (don't duplicate records)
                var address = _addressService.FindAddress(_workContext.CurrentCustomer.Addresses.ToList(),
                    newAddress.FirstName, newAddress.LastName, newAddress.PhoneNumber,
                    newAddress.Email, newAddress.FaxNumber, newAddress.Company,
                    newAddress.Address1, newAddress.Address2, newAddress.City,
                    newAddress.County, newAddress.StateProvinceId, newAddress.ZipPostalCode,
                    newAddress.CountryId, customAttributes);
                if (address == null)
                {
                    address = newAddress.ToEntity();
                    address.CustomAttributes = customAttributes;
                    address.CreatedOnUtc = DateTime.UtcNow;
                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;
                    //_workContext.CurrentCustomer.Addresses.Add(address);
                    _workContext.CurrentCustomer.CustomerAddressMappings.Add(new CustomerAddressMapping { Address = address });
                }
                _workContext.CurrentCustomer.ShippingAddress = address;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                return RedirectToRoute("CheckoutShippingMethod");
            }

            //If we got this far, something failed, redisplay form
            model = _checkoutModelFactory.PrepareShippingAddressModel(
                selectedCountryId: newAddress.CountryId,
                overrideAttributesXml: customAttributes);
            return View(model);
        }

        public virtual IActionResult ShippingMethod()
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            if (!_shoppingCartService.ShoppingCartRequiresShipping(cart))
            {
                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedShippingOptionAttribute, null, CurrStoreId);
                return RedirectToRoute("CheckoutPaymentMethod");
            }

            //model
            var model = _checkoutModelFactory.PrepareShippingMethodModel(cart, _workContext.CurrentCustomer.ShippingAddress);

            if (_shippingSettings.BypassShippingMethodSelectionIfOnlyOne &&
                model.ShippingMethods.Count == 1)
            {
                //if we have only one shipping method, then a customer doesn't have to choose a shipping method
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedShippingOptionAttribute,
                    model.ShippingMethods.First().ShippingOption,
                    CurrStoreId);

                return RedirectToRoute("CheckoutPaymentMethod");
            }

            return View(model);
        }

        [HttpPost, ActionName("ShippingMethod")]
        [FormValueRequired("nextstep")]
        public virtual IActionResult SelectShippingMethod(string shippingoption)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            if (!_shoppingCartService.ShoppingCartRequiresShipping(cart))
            {
                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedShippingOptionAttribute, null, CurrStoreId);
                return RedirectToRoute("CheckoutPaymentMethod");
            }

            //parse selected method 
            if (string.IsNullOrEmpty(shippingoption))
                return ShippingMethod();
            var splittedOption = shippingoption.Split(new[] { "___" }, StringSplitOptions.RemoveEmptyEntries);
            if (splittedOption.Length != 2)
                return ShippingMethod();
            var selectedName = splittedOption[0];
            var shippingRateComputationMethodSystemName = splittedOption[1];

            //find it
            //performance optimization. try cache first
            var shippingOptions = _genericAttributeService.GetAttribute<List<ShippingOption>>(_workContext.CurrentCustomer,
                NopCustomerDefaults.OfferedShippingOptionsAttribute, CurrStoreId);
            if (shippingOptions == null || !shippingOptions.Any())
            {
                //not found? let's load them using shipping service
                shippingOptions = _shippingService.GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress,
                    _workContext.CurrentCustomer, shippingRateComputationMethodSystemName, CurrStoreId).ShippingOptions.ToList();
            }
            else
            {
                //loaded cached results. let's filter result by a chosen shipping rate computation method
                shippingOptions = shippingOptions.Where(so => so.ShippingRateComputationMethodSystemName.Equals(shippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                    .ToList();
            }

            var shippingOption = shippingOptions
                .Find(so => !string.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedName, StringComparison.InvariantCultureIgnoreCase));
            if (shippingOption == null)
                return ShippingMethod();

            //save
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedShippingOptionAttribute, shippingOption, CurrStoreId);

            return RedirectToRoute("CheckoutPaymentMethod");
        }

        public virtual IActionResult PaymentMethod()
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            //Check whether payment workflow is required
            //we ignore reward points during cart total calculation
            var isPaymentWorkflowRequired = _orderProcessingService.IsPaymentWorkflowRequired(cart, false, false);
            if (!isPaymentWorkflowRequired)
            {
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedPaymentMethodAttribute, null, CurrStoreId);
                return RedirectToRoute("CheckoutPaymentInfo");
            }

            //filter by country
            var filterByCountryId = 0;
            if (_addressSettings.CountryEnabled &&
                _workContext.CurrentCustomer.BillingAddress != null &&
                _workContext.CurrentCustomer.BillingAddress.Country != null)
            {
                filterByCountryId = _workContext.CurrentCustomer.BillingAddress.Country.Id;
            }

            //model
            var paymentMethodModel = _checkoutModelFactory.PreparePaymentMethodModel(cart, filterByCountryId);

            if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
            {
                //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                //so customer doesn't have to choose a payment method

                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedPaymentMethodAttribute,
                    paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName,
                    CurrStoreId);
                return RedirectToRoute("CheckoutPaymentInfo");
            }

            return View(paymentMethodModel);
        }

        [HttpPost, ActionName("PaymentMethod")]
        [FormValueRequired("nextstep")]
        public virtual IActionResult SelectPaymentMethod(string paymentmethod, CheckoutPaymentMethodModel model)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            //reward points
            if (_rewardPointsSettings.Enabled)
            {
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    NopCustomerDefaults.UseRewardPointsDuringCheckoutAttribute, model.UseRewardPoints,
                    CurrStoreId);
            }

            //CustomerWallet
            if (_settingService.GetSettingByKey<bool>("storesettings.store.walletenabled", false, CurrStoreId))
            {
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    NopCustomerDefaults.UseCustomerWalletDuringCheckoutAttribute, model.UseCustomerWallet,
                    CurrStoreId);
            }

            //Check whether payment workflow is required
            var isPaymentWorkflowRequired = _orderProcessingService.IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedPaymentMethodAttribute, null, CurrStoreId);
                return RedirectToRoute("CheckoutPaymentInfo");
            }
            //payment method 
            if (string.IsNullOrEmpty(paymentmethod))
                return PaymentMethod();

            if (!_paymentPluginManager.IsPluginActive(paymentmethod, _workContext.CurrentCustomer, CurrStoreId))
                return PaymentMethod();

            //save
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                NopCustomerDefaults.SelectedPaymentMethodAttribute, paymentmethod, CurrStoreId);

            return RedirectToRoute("CheckoutPaymentInfo");
        }

        public virtual IActionResult PaymentInfo()
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            //Check whether payment workflow is required
            var isPaymentWorkflowRequired = _orderProcessingService.IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                return RedirectToRoute("CheckoutConfirm");
            }

            //load payment method
            var paymentMethodSystemName = _genericAttributeService.GetAttribute<string>(_workContext.CurrentCustomer,
                NopCustomerDefaults.SelectedPaymentMethodAttribute, CurrStoreId);
            var paymentMethod = _paymentPluginManager.LoadPluginBySystemName(paymentMethodSystemName);
            if (paymentMethod == null)
                return RedirectToRoute("CheckoutPaymentMethod");

            //Check whether payment info should be skipped
            if (paymentMethod.SkipPaymentInfo ||
                (paymentMethod.PaymentMethodType == PaymentMethodType.Redirection && _paymentSettings.SkipPaymentInfoStepForRedirectionPaymentMethods))
            {
                //skip payment info page
                var paymentInfo = new ProcessPaymentRequest();

                //session save
                HttpContext.Session.Set("OrderPaymentInfo", paymentInfo);

                return RedirectToRoute("CheckoutConfirm");
            }

            //model
            var model = _checkoutModelFactory.PreparePaymentInfoModel(paymentMethod);
            return View(model);
        }

        [HttpPost, ActionName("PaymentInfo")]
        [FormValueRequired("nextstep")]
        public virtual IActionResult EnterPaymentInfo(IFormCollection form)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            //Check whether payment workflow is required
            var isPaymentWorkflowRequired = _orderProcessingService.IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                return RedirectToRoute("CheckoutConfirm");
            }

            //load payment method
            var paymentMethodSystemName = _genericAttributeService.GetAttribute<string>(_workContext.CurrentCustomer,
                NopCustomerDefaults.SelectedPaymentMethodAttribute, CurrStoreId);
            var paymentMethod = _paymentPluginManager.LoadPluginBySystemName(paymentMethodSystemName);
            if (paymentMethod == null)
                return RedirectToRoute("CheckoutPaymentMethod");

            var warnings = paymentMethod.ValidatePaymentForm(form);
            foreach (var warning in warnings)
                ModelState.AddModelError("", warning);
            if (ModelState.IsValid)
            {
                //get payment info
                var paymentInfo = paymentMethod.GetPaymentInfo(form);
                //set previous order GUID (if exists)
                GenerateOrderGuid(paymentInfo);

                //session save
                HttpContext.Session.Set("OrderPaymentInfo", paymentInfo);
                return RedirectToRoute("CheckoutConfirm");
            }

            //If we got this far, something failed, redisplay form
            //model
            var model = _checkoutModelFactory.PreparePaymentInfoModel(paymentMethod);
            return View(model);
        }

        public virtual IActionResult Confirm()
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            //model
            var model = _checkoutModelFactory.PrepareConfirmOrderModel(cart);
            return View(model);
        }

        [HttpPost, ActionName("Confirm")]
        public virtual IActionResult ConfirmOrder()
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            //model
            var model = _checkoutModelFactory.PrepareConfirmOrderModel(cart);
            try
            {
                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //place order
                var processPaymentRequest = HttpContext.Session.Get<ProcessPaymentRequest>("OrderPaymentInfo");
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    if (_orderProcessingService.IsPaymentWorkflowRequired(cart))
                        return RedirectToRoute("CheckoutPaymentInfo");

                    processPaymentRequest = new ProcessPaymentRequest();
                }
                GenerateOrderGuid(processPaymentRequest);
                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = _genericAttributeService.GetAttribute<string>(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedPaymentMethodAttribute, CurrStoreId);
                HttpContext.Session.Set<ProcessPaymentRequest>("OrderPaymentInfo", processPaymentRequest);
                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
                if (placeOrderResult.Success)
                {
                    HttpContext.Session.Set<ProcessPaymentRequest>("OrderPaymentInfo", null);
                    var postProcessPaymentRequest = new PostProcessPaymentRequest
                    {
                        Order = placeOrderResult.PlacedOrder
                    };
                    _paymentService.PostProcessPayment(postProcessPaymentRequest);

                    if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                    {
                        //redirection or POST has been done in PostProcessPayment
                        return Content("Redirected");
                    }

                    return RedirectToRoute("CheckoutCompleted", new { orderId = placeOrderResult.PlacedOrder.Id });
                }

                foreach (var error in placeOrderResult.Errors)
                    model.Warnings.Add(error);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc);
                model.Warnings.Add(exc.Message);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        #endregion

        #region Methods (one page checkout)

        protected virtual JsonResult OpcLoadStepAfterShippingAddress(IList<ShoppingCartItem> cart)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;
            //mah
            //filter by country
            var filterByCountryId = 0;
            if (_workContext.CurrentCustomer.BillingAddress != null &&
                _workContext.CurrentCustomer.BillingAddress.Country != null)
            {
                filterByCountryId = _workContext.CurrentCustomer.BillingAddress.Country.Id;
            }
            var paymentMethodModel = _checkoutModelFactory.PreparePaymentMethodModel(cart, filterByCountryId);


            var shippingMethodModel = _checkoutModelFactory.PrepareShippingMethodModel(cart, _workContext.CurrentCustomer.ShippingAddress);
            if (_shippingSettings.BypassShippingMethodSelectionIfOnlyOne &&
                shippingMethodModel.ShippingMethods.Count == 1)
            {
                //if we have only one shipping method, then a customer doesn't have to choose a shipping method
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedShippingOptionAttribute,
                    shippingMethodModel.ShippingMethods.First().ShippingOption,
                   CurrStoreId);

                //load next step
                return OpcLoadStepAfterShippingMethod(cart);
            }

            return Json(new
            {
                update_section = new UpdateSectionJsonModel
                {
                    //name = "shipping-method",
                    name = "payment-method",
                    //html = RenderPartialViewToString("OpcShippingMethods", shippingMethodModel)
                    html = RenderPartialViewToString("OpcPaymentMethods", paymentMethodModel)
                },
                //goto_section = "shipping_method"
                goto_section = "payment_method"
            });
        }

        protected virtual JsonResult OpcLoadStepAfterShippingMethod(IList<ShoppingCartItem> cart, bool isNewAddress = false)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //Check whether delivery workflow is required
            //check any delivery slot active
            var deliveryScheduleActive = _settingService.GetSettingByKey<bool>("NB.Setting.DeliveryScheduleActive", false, _storeContext.CurrentStore.Id);
            if (deliveryScheduleActive)
            {
                deliveryScheduleActive = _deliverySlotService.AnySlotAvailbleToMerchant(storeId: _storeContext.CurrentStore.Id, merchantId: _workContext.CurrentMerchant.Id);
            }

            if (deliveryScheduleActive)
            {
                var totalDeliveryWeeks = _settingService.GetSettingByKey<int>("NB.Setting.DeliveryScheduleWeeks", 4, _storeContext.CurrentStore.Id);

                IList<DeliverySlotModel> deliverySlotlist = new List<DeliverySlotModel>();
                var dateTimeNow = DateTime.Now;
                var deliveryDateTime = dateTimeNow.Date;
                var dateTimeWeeks = dateTimeNow.AddDays(totalDeliveryWeeks * 7).Date;
                bool getDeliverySlot = true;

                // get current web url
                var storeUrl = _webHelper.GetStoreLocation();

                // get current store id
                var currStoreId = _storeService.GetAllStores().Where(x => x.Hosts.IndexOf(storeUrl) >= 0).Select(x => x.Id).FirstOrDefault();

                /* TODO: Need to work on future */
                //var productIds = string.Join(",", (_workContext.CurrentCustomer.ShoppingCartItems.Where(x => x.StoreId == currStoreId).Select(x => x.Id)));

                while (getDeliverySlot && deliveryDateTime <= dateTimeWeeks)
                {
                    var deliverySlots = _commonService.GetDeliverySlots(deliveryDateTime, "", _workContext.CurrentMerchant.Id, currStoreId);

                    if (deliverySlots.Count > 0)
                    {
                        var bookedDeliverySlots = _deliverySlotService.GetAllDeliverySlotsBooking(storeId: currStoreId, deliveryDate: deliveryDateTime).ToList();
                        foreach (var deliverySlot in deliverySlots)
                        {
                            var startTimeEnd = deliveryDateTime.Add(deliverySlot.StartTime);
                            var endTime = deliveryDateTime.Add(deliverySlot.EndTime);

                            //bypass today date deliveryslot if todaydatetime is greater then to start time
                            if (startTimeEnd.Date == dateTimeNow.Date && dateTimeNow > startTimeEnd)
                            {
                                while (startTimeEnd < dateTimeNow)
                                {
                                    double minuts = +deliverySlot.Duration;
                                    startTimeEnd = startTimeEnd.AddMinutes(minuts);
                                }
                            }

                            var startTimeStart = startTimeEnd;

                            try
                            {
                                // check Merchant start and close time 
                                TimeSpan openTimeSpan = DateTime.Parse(_workContext.CurrentMerchant.Opentime).TimeOfDay;
                                TimeSpan closetimeSpan = DateTime.Parse(_workContext.CurrentMerchant.Closetime).TimeOfDay;

                                var openTime = deliveryDateTime.Date.Add(openTimeSpan);
                                var closetime = deliveryDateTime.Date.Add(closetimeSpan);

                                if (startTimeEnd < openTime)
                                {
                                    while (startTimeEnd < openTime)
                                    {
                                        double minuts = +deliverySlot.Duration;
                                        startTimeEnd = startTimeEnd.AddMinutes(minuts);
                                    }
                                    startTimeStart = startTimeEnd;
                                }

                                if (endTime > closetime)
                                {
                                    endTime = closetime;
                                }

                                //check cut off time
                                if (deliverySlot.CutOffTime > 0 && startTimeEnd.Date == dateTimeNow.Date)
                                {
                                    var cutOffDateTime = dateTimeNow.AddHours(deliverySlot.CutOffTime);

                                    while (startTimeEnd < cutOffDateTime)
                                    {
                                        double minuts = +deliverySlot.Duration;
                                        startTimeEnd = startTimeEnd.AddMinutes(minuts);
                                    }
                                    startTimeStart = startTimeEnd;
                                }

                            }
                            catch { }

                            while (startTimeEnd != endTime && startTimeEnd <= endTime)
                            {
                                double minuts = +deliverySlot.Duration;
                                startTimeEnd = startTimeEnd.AddMinutes(minuts);
                                if (bookedDeliverySlots.Where(x => x.DeliveryStartTime == startTimeStart && x.DeliveryEndTime == startTimeEnd && x.OrderId > 0).ToList().Count < deliverySlot.NumberOfOrders)
                                {

                                    var deliverySlotObj = new DeliverySlotModel
                                    {
                                        Id = deliverySlot.Id,
                                        StartDateUtc = startTimeStart,
                                        StartDateString = startTimeStart.ToString("MM/dd/yyyy"),
                                        StartTime = startTimeStart.ToString("hh:mm tt"),
                                        EndTime = startTimeEnd.ToString("hh:mm tt"),
                                        SurChargeFeeString = _priceFormatter.FormatPrice(deliverySlot.SurChargeFee, true, _workContext.WorkingCurrency)
                                    };

                                    deliverySlotlist.Add(deliverySlotObj);
                                }
                                startTimeStart = startTimeStart.AddMinutes(minuts);
                            }
                        }
                        deliverySlotlist = deliverySlotlist.OrderBy(x => x.StartDateUtc).ToList();

                        if (deliverySlotlist.Count > 0)
                        {
                            getDeliverySlot = false;
                        }

                    }
                    deliveryDateTime = deliveryDateTime.AddDays(1);
                }

                if (deliverySlotlist.Count > 0)
                {
                    var model = new MiniShoppingCartModel();
                    model = _shoppingCartModelFactory.PrepareMiniShoppingCartModel(model);

                    IList<DeliveryDaysModel> deliveryDayslist = new List<DeliveryDaysModel>();

                    var deliverydaysDate = dateTimeNow;

                    while (deliverydaysDate <= dateTimeWeeks)
                    {
                        var deliveryDays = new DeliveryDaysModel
                        {
                            WeekDay = deliverydaysDate.ToString("ddd").ToUpper(),
                            WeekDateValue = string.Format("{0} {1}", deliverydaysDate.ToString("dd"), deliverydaysDate.ToString("MMM")),
                            WeekDate = deliverydaysDate.Date
                        };

                        deliveryDayslist.Add(deliveryDays);
                        deliverydaysDate = deliverydaysDate.AddDays(1);
                    }

                    var deliveryModel = new DeliveryModel
                    {
                        DeliverySlotList = deliverySlotlist,
                        DeliveryDaysList = deliveryDayslist,
                        ActiveSlotDate = deliveryDateTime.AddDays(-1).Date
                    };
                    //customer have to choose a payment method
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel
                        {
                            name = "deliveryschedule-method",
                            html = RenderPartialViewToString("OpcDeliveryScheduleMethods", deliveryModel)
                        },
                        goto_section = "deliveryschedule_method",
                        updateFlyoutShoppingCart = true,
                        updateFlyoutShoppingCartHtml = RenderPartialViewToString("~/Themes/" + _themeContext.WorkingThemeName.ToString() + "/Views/Shared/Components/FlyoutShoppingCart/Default.cshtml", model)
                    });
                }
            }

            return OpcLoadStepAfterDeliveryMethod(cart, isNewAddress);
        }

        /// <summary>
        /// OpcLoadStepAfterDeliveryMethod
        /// </summary>
        /// <param name="cart"></param>
        /// <param name="isNewAddress"></param>
        /// <returns></returns>
        protected virtual JsonResult OpcLoadStepAfterDeliveryMethod(IList<ShoppingCartItem> cart, bool isNewAddress = false)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //Check whether payment workflow is required
            //we ignore reward points during cart total calculation
            var isPaymentWorkflowRequired = _orderProcessingService.IsPaymentWorkflowRequired(cart, false, false);
            if (isPaymentWorkflowRequired)
            {
                //filter by country
                var filterByCountryId = 0;
                if (_addressSettings.CountryEnabled &&
                    _workContext.CurrentCustomer.BillingAddress != null &&
                    _workContext.CurrentCustomer.BillingAddress.Country != null)
                {
                    filterByCountryId = _workContext.CurrentCustomer.BillingAddress.Country.Id;
                }

                //payment is required
                var paymentMethodModel = _checkoutModelFactory.PreparePaymentMethodModel(cart, filterByCountryId);

                if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                    paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
                {
                    //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                    //so customer doesn't have to choose a payment method

                    var selectedPaymentMethodSystemName = paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName;
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        NopCustomerDefaults.SelectedPaymentMethodAttribute,
                        selectedPaymentMethodSystemName, CurrStoreId);

                    var paymentMethodInst = _paymentPluginManager
                        .LoadPluginBySystemName(selectedPaymentMethodSystemName, _workContext.CurrentCustomer, CurrStoreId);
                    if (!_paymentPluginManager.IsPluginActive(paymentMethodInst))
                        throw new Exception("Selected payment method can't be parsed");

                    return OpcLoadStepAfterPaymentMethod(paymentMethodInst, cart);
                }

                var model = new MiniShoppingCartModel();
                model = _shoppingCartModelFactory.PrepareMiniShoppingCartModel(model);

                if (isNewAddress)
                {
                    var checkoutBillingAddressModel = new CheckoutBillingAddressModel();

                    //existing addresses
                    var address = _workContext.CurrentCustomer.BillingAddress;

                    var addressTypes = _addressService.GetAddressTypes(_workContext.GetCurrentStoreId, 0);
                    var addressModel = new AddressModel();
                    _addressModelFactory.PrepareAddressModel(addressModel,
                        address: address,
                        excludeProperties: false,
                        addressSettings: _addressSettings);

                    addressModel.Landmark = address.Landmark;
                    addressModel.AddressTypeId = address.AddressTypeId;
                    if (address.AddressTypeId.HasValue && addressTypes.Count > 0)
                    {
                        var type = addressTypes.FirstOrDefault(x => x.Id == addressModel.AddressTypeId);
                        if (type != null)
                        {
                            addressModel.AddressType = type.Name;
                        }
                    }

                    checkoutBillingAddressModel.ExistingAddresses.Add(addressModel);

                    //customer have to choose a payment method
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel
                        {
                            name = "payment-method",
                            html = RenderPartialViewToString("OpcPaymentMethods", paymentMethodModel)
                        },
                        goto_section = "payment_method",
                        isNewAddress = isNewAddress,
                        newAddress = RenderPartialViewToString("_opcNewBillingAddress", checkoutBillingAddressModel),
                        updateFlyoutShoppingCart = true,
                        updateFlyoutShoppingCartHtml = RenderPartialViewToString("~/Themes/" + _themeContext.WorkingThemeName.ToString() + "/Views/Shared/Components/FlyoutShoppingCart/Default.cshtml", model)
                    });
                }


                //customer have to choose a payment method
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "payment-method",
                        html = RenderPartialViewToString("OpcPaymentMethods", paymentMethodModel)
                    },
                    goto_section = "payment_method",
                    updateFlyoutShoppingCart = true,
                    updateFlyoutShoppingCartHtml = RenderPartialViewToString("~/Themes/" + _themeContext.WorkingThemeName.ToString() + "/Views/Shared/Components/FlyoutShoppingCart/Default.cshtml", model)
                });
            }

            //payment is not required
            _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                NopCustomerDefaults.SelectedPaymentMethodAttribute, null, CurrStoreId);

            var confirmOrderModel = _checkoutModelFactory.PrepareConfirmOrderModel(cart);
            return Json(new
            {
                update_section = new UpdateSectionJsonModel
                {
                    name = "confirm-order",
                    html = RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                },
                goto_section = "confirm_order"
            });
        }

        protected virtual JsonResult OpcLoadStepAfterPaymentMethod(IPaymentMethod paymentMethod, IList<ShoppingCartItem> cart)
        {
            if (paymentMethod.SkipPaymentInfo ||
                (paymentMethod.PaymentMethodType == PaymentMethodType.Redirection && _paymentSettings.SkipPaymentInfoStepForRedirectionPaymentMethods))
            {
                //skip payment info page
                var paymentInfo = new ProcessPaymentRequest();

                //session save
                HttpContext.Session.Set("OrderPaymentInfo", paymentInfo);

                var confirmOrderModel = _checkoutModelFactory.PrepareConfirmOrderModel(cart);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "confirm-order",
                        html = RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                    },
                    goto_section = "confirm_order"
                });
            }

            //return payment info page
            var paymenInfoModel = _checkoutModelFactory.PreparePaymentInfoModel(paymentMethod);
            return Json(new
            {
                update_section = new UpdateSectionJsonModel
                {
                    name = "payment-info",
                    html = RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                },
                goto_section = "payment_info"
            });
        }

        public virtual IActionResult OnePageCheckout()
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //UseCustomerWalletDuring
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, NopCustomerDefaults.UseCustomerWalletDuringCheckoutAttribute, false, CurrStoreId);

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (!_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("Checkout");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            var model = _checkoutModelFactory.PrepareOnePageCheckoutModel(cart);

            //check any delivery slot active
            var deliveryScheduleActve = _settingService.GetSettingByKey<bool>("NB.Setting.DeliveryScheduleActive", false, _storeContext.CurrentStore.Id);
            if (deliveryScheduleActve)
            {
                model.DisplayDeliveryScheduleButton = _deliverySlotService.AnySlotAvailbleToMerchant(storeId: _storeContext.CurrentStore.Id, merchantId: _workContext.CurrentMerchant.Id);
            }

            return View(model);
        }

        public virtual IActionResult OpcSaveBilling(CheckoutBillingAddressModel model, IFormCollection form)
        {
            try
            {
                //Added By Mah
                int CurrStoreId = _workContext.GetCurrentStoreId;

                //validation
                if (_orderSettings.CheckoutDisabled)
                    throw new Exception(_localizationService.GetResource("Checkout.Disabled"));

                var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                int.TryParse(form["billing_address_id"], out var billingAddressId);

                if (billingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == billingAddressId)
                        ?? throw new Exception("Address can't be loaded");

                    _workContext.CurrentCustomer.BillingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                else
                {
                    //new address
                    //Added By Mah 26022019
                    model.BillingNewAddress.PhoneNumber = model.BillingNewAddress.ISDId.ToString() + " " + model.BillingNewAddress.PhoneNumber;

                    var newAddress = model.BillingNewAddress;

                    //custom address attributes
                    var customAttributes = _addressAttributeParser.ParseCustomAddressAttributes(form);
                    var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
                    foreach (var error in customAttributeWarnings)
                    {
                        ModelState.AddModelError("", error);
                    }

                    //validate model
                    if (!ModelState.IsValid)
                    {
                        //model is not valid. redisplay the form with errors
                        var billingAddressModel = _checkoutModelFactory.PrepareBillingAddressModel(cart,
                            selectedCountryId: newAddress.CountryId,
                            overrideAttributesXml: customAttributes);
                        billingAddressModel.NewAddressPreselected = true;
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel
                            {
                                name = "billing",
                                html = RenderPartialViewToString("OpcBillingAddress", billingAddressModel)
                            },
                            wrong_billing_address = true,
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _addressService.FindAddress(_workContext.CurrentCustomer.Addresses.ToList(),
                        newAddress.FirstName, newAddress.LastName, newAddress.PhoneNumber,
                        newAddress.Email, newAddress.FaxNumber, newAddress.Company,
                        newAddress.Address1, newAddress.Address2, newAddress.City,
                        newAddress.County, newAddress.StateProvinceId, newAddress.ZipPostalCode,
                        newAddress.CountryId, customAttributes);
                    if (address == null)
                    {
                        //address is not found. let's create a new one
                        address = newAddress.ToEntity();
                        address.CustomAttributes = customAttributes;
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //some validation
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        if (address.CountryId.HasValue && address.CountryId.Value > 0)
                        {
                            address.Country = _countryService.GetCountryById(address.CountryId.Value);
                        }
                        //_workContext.CurrentCustomer.Addresses.Add(address);
                        _workContext.CurrentCustomer.CustomerAddressMappings.Add(new CustomerAddressMapping { Address = address });
                    }
                    _workContext.CurrentCustomer.BillingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }

                if (_shoppingCartService.ShoppingCartRequiresShipping(cart))
                {
                    //shipping is required
                    var address = _workContext.CurrentCustomer.BillingAddress;

                    //by default Shipping is available if the country is not specified
                    var shippingAllowed = _addressSettings.CountryEnabled ? address.Country?.AllowsShipping ?? false : true;
                    if (_shippingSettings.ShipToSameAddress && model.ShipToSameAddress && shippingAllowed)
                    {
                        //ship to the same address
                        _workContext.CurrentCustomer.ShippingAddress = address;
                        _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                        //reset selected shipping method (in case if "pick up in store" was selected)
                        _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedShippingOptionAttribute, null, CurrStoreId);
                        _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedPickupPointAttribute, null, CurrStoreId);
                        //limitation - "Ship to the same address" doesn't properly work in "pick up in store only" case (when no shipping plugins are available) 
                        return OpcLoadStepAfterShippingAddress(cart);
                    }

                    //do not ship to the same address
                    var shippingAddressModel = _checkoutModelFactory.PrepareShippingAddressModel(prePopulateNewAddressWithCustomerFields: true);

                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel
                        {
                            name = "shipping",
                            html = RenderPartialViewToString("OpcShippingAddress", shippingAddressModel)
                        },
                        goto_section = "shipping"
                    });
                }

                //shipping is not required
                _workContext.CurrentCustomer.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedShippingOptionAttribute, null, CurrStoreId);

                //load next step
                return OpcLoadStepAfterShippingMethod(cart);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public virtual IActionResult OpcSaveShipping(CheckoutShippingAddressModel model, IFormCollection form)
        {
            try
            {
                //Added By Mah
                int CurrStoreId = _workContext.GetCurrentStoreId;

                //validation
                if (_orderSettings.CheckoutDisabled)
                    throw new Exception(_localizationService.GetResource("Checkout.Disabled"));

                var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                if (!_shoppingCartService.ShoppingCartRequiresShipping(cart))
                    throw new Exception("Shipping is not required");

                //pickup point
                if (_shippingSettings.AllowPickupInStore)
                {
                    if (model.PickupInStore)
                    {
                        //no shipping address selected
                        _workContext.CurrentCustomer.ShippingAddress = null;
                        _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                        var pickupPoint = form["pickup-points-id"].ToString().Split(new[] { "___" }, StringSplitOptions.None);
                        var pickupPoints = _shippingService.GetPickupPoints(_workContext.CurrentCustomer.BillingAddress,
                            _workContext.CurrentCustomer, pickupPoint[1], CurrStoreId).PickupPoints.ToList();
                        var selectedPoint = pickupPoints.FirstOrDefault(x => x.Id.Equals(pickupPoint[0]));
                        if (selectedPoint == null)
                            throw new Exception("Pickup point is not allowed");

                        var pickUpInStoreShippingOption = new ShippingOption
                        {
                            Name = string.Format(_localizationService.GetResource("Checkout.PickupPoints.Name"), selectedPoint.Name),
                            Rate = selectedPoint.PickupFee,
                            Description = selectedPoint.Description,
                            ShippingRateComputationMethodSystemName = selectedPoint.ProviderSystemName
                        };
                        _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedShippingOptionAttribute, pickUpInStoreShippingOption, CurrStoreId);
                        _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedPickupPointAttribute, selectedPoint, CurrStoreId);

                        //load next step
                        return OpcLoadStepAfterShippingMethod(cart);
                    }

                    //set value indicating that "pick up in store" option has not been chosen
                    _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedPickupPointAttribute, null, CurrStoreId);
                }

                int.TryParse(form["shipping_address_id"], out var shippingAddressId);

                if (shippingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == shippingAddressId)
                        ?? throw new Exception("Address can't be loaded");

                    _workContext.CurrentCustomer.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                else
                {
                    //new address
                    var newAddress = model.ShippingNewAddress;

                    //custom address attributes
                    var customAttributes = _addressAttributeParser.ParseCustomAddressAttributes(form);
                    var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
                    foreach (var error in customAttributeWarnings)
                    {
                        ModelState.AddModelError("", error);
                    }

                    //validate model
                    if (!ModelState.IsValid)
                    {
                        //model is not valid. redisplay the form with errors
                        var shippingAddressModel = _checkoutModelFactory.PrepareShippingAddressModel(
                            selectedCountryId: newAddress.CountryId,
                            overrideAttributesXml: customAttributes);
                        shippingAddressModel.NewAddressPreselected = true;
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel
                            {
                                name = "shipping",
                                html = RenderPartialViewToString("OpcShippingAddress", shippingAddressModel)
                            }
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _addressService.FindAddress(_workContext.CurrentCustomer.Addresses.ToList(),
                        newAddress.FirstName, newAddress.LastName, newAddress.PhoneNumber,
                        newAddress.Email, newAddress.FaxNumber, newAddress.Company,
                        newAddress.Address1, newAddress.Address2, newAddress.City,
                        newAddress.County, newAddress.StateProvinceId, newAddress.ZipPostalCode,
                        newAddress.CountryId, customAttributes);
                    if (address == null)
                    {
                        address = newAddress.ToEntity();
                        address.CustomAttributes = customAttributes;
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //little hack here (TODO: find a better solution)
                        //EF does not load navigation properties for newly created entities (such as this "Address").
                        //we have to load them manually 
                        //otherwise, "Country" property of "Address" entity will be null in shipping rate computation methods
                        if (address.CountryId.HasValue)
                            address.Country = _countryService.GetCountryById(address.CountryId.Value);
                        if (address.StateProvinceId.HasValue)
                            address.StateProvince = _stateProvinceService.GetStateProvinceById(address.StateProvinceId.Value);

                        //other null validations
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        //_workContext.CurrentCustomer.Addresses.Add(address);
                        _workContext.CurrentCustomer.CustomerAddressMappings.Add(new CustomerAddressMapping { Address = address });
                    }
                    _workContext.CurrentCustomer.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }

                return OpcLoadStepAfterShippingAddress(cart);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public virtual IActionResult OpcSaveShippingMethod(string shippingoption)
        {
            try
            {
                //Added By Mah
                int CurrStoreId = _workContext.GetCurrentStoreId;

                //validation
                if (_orderSettings.CheckoutDisabled)
                    throw new Exception(_localizationService.GetResource("Checkout.Disabled"));

                var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                if (!_shoppingCartService.ShoppingCartRequiresShipping(cart))
                    throw new Exception("Shipping is not required");

                //parse selected method 
                if (string.IsNullOrEmpty(shippingoption))
                    throw new Exception("Selected shipping method can't be parsed");
                var splittedOption = shippingoption.Split(new[] { "___" }, StringSplitOptions.RemoveEmptyEntries);
                if (splittedOption.Length != 2)
                    throw new Exception("Selected shipping method can't be parsed");
                var selectedName = splittedOption[0];
                var shippingRateComputationMethodSystemName = splittedOption[1];

                //find it
                //performance optimization. try cache first
                var shippingOptions = _genericAttributeService.GetAttribute<List<ShippingOption>>(_workContext.CurrentCustomer,
                    NopCustomerDefaults.OfferedShippingOptionsAttribute, _storeContext.CurrentStore.Id);
                if (shippingOptions == null || !shippingOptions.Any())
                {
                    //not found? let's load them using shipping service
                    shippingOptions = _shippingService.GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress,
                        _workContext.CurrentCustomer, shippingRateComputationMethodSystemName, _storeContext.CurrentStore.Id).ShippingOptions.ToList();
                }
                else
                {
                    //loaded cached results. let's filter result by a chosen shipping rate computation method
                    shippingOptions = shippingOptions.Where(so => so.ShippingRateComputationMethodSystemName.Equals(shippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                        .ToList();
                }

                var shippingOption = shippingOptions
                    .Find(so => !string.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedName, StringComparison.InvariantCultureIgnoreCase));
                if (shippingOption == null)
                    throw new Exception("Selected shipping method can't be loaded");

                //save
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedShippingOptionAttribute, shippingOption, CurrStoreId);

                //load next step
                return OpcLoadStepAfterShippingMethod(cart);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        //delivery slot fun start
        public virtual IActionResult OpcSaveDeliveryScheduleMethod(DeliveryModel model)
        {
            try
            {
                //Added By Mah
                int CurrStoreId = _workContext.GetCurrentStoreId;

                //validation
                if (_orderSettings.CheckoutDisabled)
                    throw new Exception(_localizationService.GetResource("Checkout.Disabled"));

                var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                if (model.SlotId > 0)
                {
                    //get user current date
                    var deliveryDate = DateTime.ParseExact(model.SlotDate, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                    // get current web url
                    var webHelper = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Core.IWebHelper>();
                    var storeUrl = webHelper.GetStoreLocation();

                    // get current store id
                    var currStoreId = _storeService.GetAllStores().Where(x => x.Hosts.IndexOf(storeUrl) >= 0).Select(x => x.Id).FirstOrDefault();

                    var deliverySlotBooking = new DeliverySlotBooking
                    {
                        SlotId = model.SlotId,
                        StoreId = currStoreId,
                        CustomerId = _workContext.CurrentCustomer.Id,
                        DeliveryDateUtc = deliveryDate.Date,
                        DeliveryStartTime = deliveryDate.Date.Add(DateTime.Parse(model.StartTime).TimeOfDay),
                        DeliveryEndTime = deliveryDate.Date.Add(DateTime.Parse(model.EndTime).TimeOfDay),
                        CreatedOnUtc = DateTime.UtcNow
                    };

                    _deliverySlotService.InsertDeliverySlotBooking(deliverySlotBooking);


                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                            NopCustomerDefaults.SelectedDeliverySlotBookingIdAttribute,
                            deliverySlotBooking.Id, currStoreId);
                }
                else
                {
                    // get current web url
                    var webHelper = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Core.IWebHelper>();
                    var storeUrl = webHelper.GetStoreLocation();

                    // get current store id
                    var currStoreId = _storeService.GetAllStores().Where(x => x.Hosts.IndexOf(storeUrl) >= 0).Select(x => x.Id).FirstOrDefault();

                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                                NopCustomerDefaults.SelectedDeliverySlotBookingIdAttribute,
                                0, currStoreId);
                }

                return OpcLoadStepAfterDeliveryMethod(cart);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        /// <summary>
        /// GetDeliverySlotsDateTime
        /// </summary>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        public IActionResult GetDeliverySlotsDateTime(string deliveryDate)
        {
            var status = false;
            string message = string.Empty;
            // get current web url
            var webHelper = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Core.IWebHelper>();
            var storeUrl = webHelper.GetStoreLocation();

            //get user current date
            var deliveryDateTime = DateTime.ParseExact(deliveryDate, "MM-dd-yyyy", CultureInfo.InvariantCulture);

            // get current store id
            var currStoreId = _storeService.GetAllStores().Where(x => x.Hosts.IndexOf(storeUrl) >= 0).Select(x => x.Id).FirstOrDefault();

            /* TODO: Need to work on future */
            //var productIds = string.Join(",", (_workContext.CurrentCustomer.ShoppingCartItems.Where(x => x.StoreId == currStoreId).Select(x => x.Id)));

            var deliverySlots = _commonService.GetDeliverySlots(deliveryDateTime, "", _workContext.CurrentMerchant.Id, currStoreId);

            IList<DeliverySlotModel> deliverySlotlist = new List<DeliverySlotModel>();

            if (deliverySlots.Count > 0)
            {
                var bookedDeliverySlots = _deliverySlotService.GetAllDeliverySlotsBooking(storeId: currStoreId, deliveryDate: deliveryDateTime).ToList();
                //var bookedDeliverySlot = 
                foreach (var deliverySlot in deliverySlots)
                {
                    var startTimeEnd = deliveryDateTime.Add(deliverySlot.StartTime);
                    var endTime = deliveryDateTime.Add(deliverySlot.EndTime);

                    //bypass today date deliveryslot if todaydatetime is greater then to start time
                    var dateTimeNow = DateTime.Now;
                    if (startTimeEnd.Date == dateTimeNow.Date && dateTimeNow > startTimeEnd)
                    {
                        while (startTimeEnd < dateTimeNow)
                        {
                            double minuts = +deliverySlot.Duration;
                            startTimeEnd = startTimeEnd.AddMinutes(minuts);
                        }
                    }

                    var startTimeStart = startTimeEnd;

                    try
                    {
                        // check Merchant start and close time 
                        TimeSpan openTimeSpan = DateTime.Parse(_workContext.CurrentMerchant.Opentime).TimeOfDay;
                        TimeSpan closetimeSpan = DateTime.Parse(_workContext.CurrentMerchant.Closetime).TimeOfDay;

                        var openTime = deliveryDateTime.Date.Add(openTimeSpan);
                        var closetime = deliveryDateTime.Date.Add(closetimeSpan);

                        if (startTimeEnd < openTime)
                        {
                            while (startTimeEnd < openTime)
                            {
                                double minuts = +deliverySlot.Duration;
                                startTimeEnd = startTimeEnd.AddMinutes(minuts);
                            }
                            startTimeStart = startTimeEnd;
                        }

                        if (endTime > closetime)
                        {
                            endTime = closetime;
                        }

                        //check cut off time
                        if (deliverySlot.CutOffTime > 0 && startTimeEnd.Date == dateTimeNow.Date)
                        {
                            var cutOffDateTime = dateTimeNow.AddHours(deliverySlot.CutOffTime);

                            while (startTimeEnd < cutOffDateTime)
                            {
                                double minuts = +deliverySlot.Duration;
                                startTimeEnd = startTimeEnd.AddMinutes(minuts);
                            }
                            startTimeStart = startTimeEnd;
                        }

                    }
                    catch (Exception ex) { }

                    while (startTimeEnd != endTime && startTimeEnd <= endTime)
                    {
                        double minuts = +deliverySlot.Duration;
                        startTimeEnd = startTimeEnd.AddMinutes(minuts);
                        if (bookedDeliverySlots.Where(x => x.DeliveryStartTime == startTimeStart && x.DeliveryEndTime == startTimeEnd && x.OrderId > 0).ToList().Count < deliverySlot.NumberOfOrders)
                        {

                            var deliverySlotObj = new DeliverySlotModel
                            {
                                Id = deliverySlot.Id,
                                StartDateUtc = startTimeStart,
                                StartDateString = startTimeStart.ToString("MM/dd/yyyy"),
                                StartTime = startTimeStart.ToString("hh:mm tt"),
                                EndTime = startTimeEnd.ToString("hh:mm tt"),
                                SurChargeFeeString = _priceFormatter.FormatPrice(deliverySlot.SurChargeFee, true, _workContext.WorkingCurrency)
                            };

                            deliverySlotlist.Add(deliverySlotObj);
                        }
                        startTimeStart = startTimeStart.AddMinutes(minuts);
                    }
                }
                deliverySlotlist = deliverySlotlist.OrderBy(x => x.StartDateUtc).ToList();
            }

            if (deliverySlotlist.Count > 0)
            {
                status = true;
            }
            else
            {
                status = false;
                message = _localizationService.GetResource("NB.CheckoutDelivery.NoDeliverySlotAvailable");
            }

            return Json(new { success = status, deliverySlotList = deliverySlotlist, message });
        }

        //delivery slot fun end

        public virtual IActionResult OpcSavePaymentMethod(string paymentmethod, CheckoutPaymentMethodModel model)
        {
            try
            {
                //Added By Mah
                int CurrStoreId = _workContext.GetCurrentStoreId;

                //validation
                if (_orderSettings.CheckoutDisabled)
                    throw new Exception(_localizationService.GetResource("Checkout.Disabled"));

                var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                //reward points
                if (_rewardPointsSettings.Enabled)
                {
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        NopCustomerDefaults.UseRewardPointsDuringCheckoutAttribute, model.UseRewardPoints,
                        CurrStoreId);
                }

                //CustomerWallet
                if (_settingService.GetSettingByKey<bool>("storesettings.store.walletenabled", false, CurrStoreId))
                {
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        NopCustomerDefaults.UseCustomerWalletDuringCheckoutAttribute, model.UseCustomerWallet,
                        CurrStoreId);
                }

                //Check whether payment workflow is required
                var isPaymentWorkflowRequired = _orderProcessingService.IsPaymentWorkflowRequired(cart);
                if (!isPaymentWorkflowRequired)
                {
                    //payment is not required
                    _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                        NopCustomerDefaults.SelectedPaymentMethodAttribute, null, CurrStoreId);

                    CheckoutPaymentInfoModel objpaymenInfoModel = new CheckoutPaymentInfoModel();
                    return OpcConfirmOrderCustom(objpaymenInfoModel);
                    //var confirmOrderModel = _checkoutModelFactory.PrepareConfirmOrderModel(cart);
                    //return Json(new
                    //{
                    //    update_section = new UpdateSectionJsonModel
                    //    {
                    //        name = "confirm-order",
                    //        html = RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                    //    },
                    //    goto_section = "confirm_order"
                    //});
                }

                //payment method 
                if (string.IsNullOrEmpty(paymentmethod))
                    throw new Exception("Selected payment method can't be parsed");

                var paymentMethodInst = _paymentPluginManager
                    .LoadPluginBySystemName(paymentmethod, _workContext.CurrentCustomer, CurrStoreId);
                if (!_paymentPluginManager.IsPluginActive(paymentMethodInst))
                    throw new Exception("Selected payment method can't be parsed");

                //save
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedPaymentMethodAttribute, paymentmethod, CurrStoreId);

                return OpcLoadStepAfterPaymentMethod(paymentMethodInst, cart);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public virtual IActionResult OpcSavePaymentInfo(IFormCollection form)
        {
            try
            {
                //Added By Mah
                int CurrStoreId = _workContext.GetCurrentStoreId;

                //validation
                if (_orderSettings.CheckoutDisabled)
                    throw new Exception(_localizationService.GetResource("Checkout.Disabled"));

                var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                var paymentMethodSystemName = _genericAttributeService.GetAttribute<string>(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedPaymentMethodAttribute, CurrStoreId);
                var paymentMethod = _paymentPluginManager.LoadPluginBySystemName(paymentMethodSystemName)
                    ?? throw new Exception("Payment method is not selected");

                var warnings = paymentMethod.ValidatePaymentForm(form);
                foreach (var warning in warnings)
                    ModelState.AddModelError("", warning);
                if (ModelState.IsValid)
                {
                    //get payment info
                    var paymentInfo = paymentMethod.GetPaymentInfo(form);
                    //set previous order GUID (if exists)
                    GenerateOrderGuid(paymentInfo);

                    //session save
                    HttpContext.Session.Set("OrderPaymentInfo", paymentInfo);

                    var confirmOrderModel = _checkoutModelFactory.PrepareConfirmOrderModel(cart);
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel
                        {
                            name = "confirm-order",
                            html = RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }

                //If we got this far, something failed, redisplay form
                var paymenInfoModel = _checkoutModelFactory.PreparePaymentInfoModel(paymentMethod);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "payment-info",
                        html = RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                    }
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public virtual IActionResult OpcConfirmOrder()
        {
            try
            {
                //Added By Mah
                int CurrStoreId = _workContext.GetCurrentStoreId;

                //validation
                if (_orderSettings.CheckoutDisabled)
                    throw new Exception(_localizationService.GetResource("Checkout.Disabled"));

                var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //place order
                var processPaymentRequest = HttpContext.Session.Get<ProcessPaymentRequest>("OrderPaymentInfo");
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    if (_orderProcessingService.IsPaymentWorkflowRequired(cart))
                    {
                        throw new Exception("Payment information is not entered");
                    }

                    processPaymentRequest = new ProcessPaymentRequest();
                }
                GenerateOrderGuid(processPaymentRequest);
                processPaymentRequest.StoreId = CurrStoreId;
                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = _genericAttributeService.GetAttribute<string>(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedPaymentMethodAttribute, CurrStoreId);
                HttpContext.Session.Set<ProcessPaymentRequest>("OrderPaymentInfo", processPaymentRequest);
                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
                if (placeOrderResult.Success)
                {
                    HttpContext.Session.Set<ProcessPaymentRequest>("OrderPaymentInfo", null);
                    var postProcessPaymentRequest = new PostProcessPaymentRequest
                    {
                        Order = placeOrderResult.PlacedOrder
                    };

                    var paymentMethod = _paymentPluginManager.LoadPluginBySystemName(placeOrderResult.PlacedOrder.PaymentMethodSystemName);
                    if (paymentMethod == null)
                        //payment method could be null if order total is 0
                        //success
                        return Json(new { success = 1 });

                    if (paymentMethod.PaymentMethodType == PaymentMethodType.Redirection)
                    {
                        //Redirection will not work because it's AJAX request.
                        //That's why we don't process it here (we redirect a user to another page where he'll be redirected)

                        //redirect
                        return Json(new
                        {
                            redirect = $"{_webHelper.GetStoreLocation()}checkout/OpcCompleteRedirectionPayment"
                        });
                    }

                    _paymentService.PostProcessPayment(postProcessPaymentRequest);
                    //success
                    return Json(new { success = 1 });
                }

                //error
                var confirmOrderModel = new CheckoutConfirmModel();
                foreach (var error in placeOrderResult.Errors)
                    confirmOrderModel.Warnings.Add(error);

                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "confirm-order",
                        html = RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                    },
                    goto_section = "confirm_order"
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public virtual IActionResult OpcCompleteRedirectionPayment()
        {
            try
            {
                //Added By Mah
                int CurrStoreId = _workContext.GetCurrentStoreId;

                //validation
                if (!_orderSettings.OnePageCheckoutEnabled)
                    return RedirectToRoute("Homepage");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    return Challenge();

                //get the order
                var order = _orderService.SearchOrders(storeId: CurrStoreId,
                customerId: _workContext.CurrentCustomer.Id, pageSize: 1)
                    .FirstOrDefault();
                if (order == null)
                    return RedirectToRoute("Homepage");

                var paymentMethod = _paymentPluginManager.LoadPluginBySystemName(order.PaymentMethodSystemName);
                if (paymentMethod == null)
                    return RedirectToRoute("Homepage");
                if (paymentMethod.PaymentMethodType != PaymentMethodType.Redirection)
                    return RedirectToRoute("Homepage");

                //ensure that order has been just placed
                if ((DateTime.UtcNow - order.CreatedOnUtc).TotalMinutes > 3)
                    return RedirectToRoute("Homepage");

                //Redirection will not work on one page checkout page because it's AJAX request.
                //That's why we process it here
                var postProcessPaymentRequest = new PostProcessPaymentRequest
                {
                    Order = order
                };

                _paymentService.PostProcessPayment(postProcessPaymentRequest);

                if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                {
                    //redirection or POST has been done in PostProcessPayment
                    return Content("Redirected");
                }

                //if no redirection has been done (to a third-party payment page)
                //theoretically it's not possible
                return RedirectToRoute("CheckoutCompleted", new { orderId = order.Id });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Content(exc.Message);
            }
        }

        #endregion

        #region Custom code from v4.0
        void sendMessage(int orderId, int storeId)
        {
            try
            {
                SMSModel sMSModel = new SMSModel();
                var orderVendor = _orderService.GetOrderVendorDetails(orderId);
                var orderCustomer = _orderService.GetOrderCustomerDetails(orderId);

                var ISDCode = _workContext.CurrentStoreISDCode; //_dbContext.QueryFromSql<string>("select ISDCode from store(nolock) As A,Country(nolock) As B where A.CountryId=B.Id and A.Id=" + storeId.ToString()).ToList().First();

                string accountSid = _configuration.GetSection("SMSSetting").GetSection("accountSid").Value;
                string authToken = _configuration.GetSection("SMSSetting").GetSection("authToken").Value;
                string senderId = _configuration.GetSection("SMSSetting").GetSection("senderId").Value;
                //string NewOrderMessage = _configuration.GetSection("SMSSetting").GetSection("newOrderMessage").Value;
                string NewOrderMessage = _localizationService.GetResource("Sales.ChangeOrderStatus.NewOrderMessageMerchant").ToString();

                //For sending sms
                //var accountSid = "AC3f8c08a8e69488d9b4f0197241bdaa6d";
                //var authToken = "821ee18399986acfd3736ac5ca8480fa";
                TwilioClient.Init(accountSid, authToken);
                var to = new PhoneNumber("+" + ISDCode.ToString() + orderVendor.PickupMobileNo);
                var from = new PhoneNumber(senderId);
                var body = NewOrderMessage;
                var message = MessageResource.Create(
                    to: to,
                    from: from,
                    body: body);
                Console.WriteLine(message.Sid);
                //For saving sms data
                var sms = new SMSModel
                {
                    To = sMSModel.To,
                    From = from.ToString(),
                    CreatedOn = DateTime.Now,
                    Message = sMSModel.Message
                };
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
            }
        }
        void sendMessageCustomer(int orderId, int storeId)
        {
            try
            {
                SMSModel sMSModel = new SMSModel();
                var orderVendor = _orderService.GetOrderVendorDetails(orderId);
                var orderCustomer = _orderService.GetOrderCustomerDetails(orderId);

                var ISDCode = _workContext.CurrentStoreISDCode;//_dbContext.QueryFromSql<string>("select ISDCode from store(nolock) As A,Country(nolock) As B where A.CountryId=B.Id and A.Id=" + storeId.ToString()).ToList().First();

                string accountSid = _configuration.GetSection("SMSSetting").GetSection("accountSid").Value;
                string authToken = _configuration.GetSection("SMSSetting").GetSection("authToken").Value;
                string senderId = _configuration.GetSection("SMSSetting").GetSection("senderId").Value;
                //string NewOrderMessage = _configuration.GetSection("SMSSetting").GetSection("newOrderMessageCustomer").Value;
                string NewOrderMessage = _localizationService.GetResource("Sales.ChangeOrderStatus.NewOrderMessageCustomer").ToString();
                //For sending sms
                //var accountSid = "AC3f8c08a8e69488d9b4f0197241bdaa6d";
                //var authToken = "821ee18399986acfd3736ac5ca8480fa";
                TwilioClient.Init(accountSid, authToken);
                var to = new PhoneNumber("+" + ISDCode.ToString() + orderCustomer.CustomerMobileNo);
                var from = new PhoneNumber(senderId);
                var body = NewOrderMessage;
                var message = MessageResource.Create(
                    to: to,
                    from: from,
                    body: body);
                Console.WriteLine(message.Sid);
                //For saving sms data
                var sms = new SMSModel
                {
                    To = sMSModel.To,
                    From = from.ToString(),
                    CreatedOn = DateTime.Now,
                    Message = sMSModel.Message
                };
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
            }
        }

        public IActionResult GetCrossSellProducts()
        {

            return ViewComponent("CrossSellProducts", new { productThumbPictureSize = 300 });
        }
        public IActionResult GetUserDetails()
        {
            var customer = _workContext.CurrentCustomer;
            string userDetails = string.Empty;
            if (customer.IsRegistered())
            {
                userDetails = customer.GetFullName();
                var phone = _genericAttributeService.GetAttribute<string>(customer, "Phone",
                      _workContext.GetCurrentStoreId);
                userDetails += " | " + phone;
                if (phone == null)
                {
                    userDetails += " | " + customer.Email;
                }
            }
            else
            {
                userDetails = _localizationService.GetResource("Checkout.CheckoutAsGuest.Text", _workContext.WorkingLanguage.Id);
            }
            return PartialView("OpcLogedIn", userDetails);
        }

        public IActionResult OpcSendOTP(CheckoutLoginSignUpModel model)
        {
            if (string.IsNullOrEmpty(model.PhoneOrEmail))
            {
                model.IsValid = false;
                model.Message = _localizationService.GetResource("Checkout.LoginSignup.PhoneOrEmail.Validate");
            }
            else
            {
                Customer customer = new Customer();
                customer = _customerService.GetCustomerByEmail(model.PhoneOrEmail);
                if (customer != null)
                {
                    model.IsEmail = true;
                    model.IsValid = true;
                }
                else
                {
                    try
                    {
                        var ISDCode = _workContext.CurrentStoreISDCode;//_dbContext.QueryFromSql<string>("select ISDCode from store(nolock) As A,Country(nolock) As B where A.CountryId=B.Id and A.Id=" + _workContext.GetCurrentStoreId.ToString()).ToList().First();
                        customer = _customerService.GetCustomerByPhone(ISDCode.ToString() + " " + model.PhoneOrEmail);
                        if (customer == null)
                        {
                            model.IsValid = false;
                            model.Message = _localizationService.GetResource("Checkout.LoginSignup.IncorrectPhoneOrEmail.Validate");
                        }
                        else
                        {
                            model.IsEmail = true;
                            model.IsValid = true;
                            //SMSModel sMSModel = new SMSModel();
                            //string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
                            //string sRandomOTP = GenerateRandomOTP(6, saAllowedCharacters);


                            //string accountSid = _configuration.GetSection("SMSSetting").GetSection("accountSid").Value;
                            //string authToken = _configuration.GetSection("SMSSetting").GetSection("authToken").Value;
                            //string senderId = _configuration.GetSection("SMSSetting").GetSection("senderId").Value;
                            //string NewOrderMessage = "Your OTP code is " + sRandomOTP;
                            ////For sending sms
                            //TwilioClient.Init(accountSid, authToken);
                            //var to = new PhoneNumber("+" + ISDCode.ToString() + model.PhoneOrEmail);
                            //var from = new PhoneNumber(senderId.Replace("+", ""));
                            //var body = NewOrderMessage;
                            //var message = MessageResource.Create(
                            //    to: to,
                            //    from: from,
                            //    body: body);
                            ////For saving sms data
                            //var sms = new SMSModel
                            //{
                            //    To = sMSModel.To,
                            //    From = from.ToString(),
                            //    CreatedOn = DateTime.Now,
                            //    Message = sMSModel.Message
                            //};
                            //model.IsValid = true;
                            //model.Message = _localizationService.GetResource("Checkout.LoginSignup.OTPSent.Message");
                            //model.ShowOTP = true;
                            ////save otp
                            //_genericAttributeService.SaveAttribute(customer,
                            //"CheckoutOTP", sRandomOTP, _workContext.GetCurrentStoreId);

                        }
                    }
                    catch (Exception ex)
                    {
                        model.Message = _localizationService.GetResource("Checkout.LoginSignup.OTPError.Message");
                    }
                }
            }

            return Json(new
            {
                update_section = new UpdateSectionJsonModel
                {
                    name = "checkout-login",
                    html = RenderPartialViewToString("OpcLogin", model)
                },
                goto_section = "checkout-login"
            });

        }

        public IActionResult OpcLogin(CheckoutLoginSignUpModel model)
        {
            Customer customer = new Customer();
            if (string.IsNullOrEmpty(model.PhoneOrEmail))
            {
                model.IsValid = false;
                model.Message = _localizationService.GetResource("Checkout.LoginSignup.PhoneOrEmail.Validate");
            }
            else if (model.IsEmail && string.IsNullOrEmpty(model.Password))
            {
                model.IsValid = false;
                model.Message = _localizationService.GetResource("Checkout.LoginSignup.PasswordEmpty.Validate");
            }
            else if (!model.IsEmail && string.IsNullOrEmpty(model.OTP))
            {
                model.IsValid = false;
                model.Message = _localizationService.GetResource("Checkout.LoginSignup.InvalidOTP.Validate");
            }
            else
            {
                if (model.IsEmail)
                {
                    customer = _customerService.GetCustomerByEmail(model.PhoneOrEmail);
                    if (customer == null)
                    {
                        customer = _customerService.GetCustomerByPhone(_workContext.CurrentStoreISDCode + " " + model.PhoneOrEmail);
                    }
                }
                else
                {
                    var isdCode = _workContext.CurrentStoreISDCode;
                    customer = _customerService.GetCustomerByPhone(isdCode + " " + model.PhoneOrEmail);
                }
                if (customer == null)
                {
                    model.IsValid = false;
                    model.Message = _localizationService.GetResource("Checkout.LoginSignup.UserNotExists.Message");
                }
                else
                {
                    bool isValidUser = false;
                    if (model.IsEmail)
                    {
                        var loginResult = _customerRegistrationService.ValidateCustomer(model.PhoneOrEmail, model.Password);
                        switch (loginResult)
                        {
                            case CustomerLoginResults.Successful:
                                isValidUser = true;
                                model.IsValid = true;
                                break;
                            case CustomerLoginResults.CustomerNotExist:
                                model.Message = _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist");
                                break;
                            case CustomerLoginResults.Deleted:
                                model.Message = _localizationService.GetResource("Account.Login.WrongCredentials.Deleted");
                                break;
                            case CustomerLoginResults.NotActive:
                                model.Message = _localizationService.GetResource("Account.Login.WrongCredentials.NotActive");
                                break;
                            case CustomerLoginResults.NotRegistered:
                                model.Message = _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered");
                                break;
                            case CustomerLoginResults.LockedOut:
                                model.Message = _localizationService.GetResource("Account.Login.WrongCredentials.LockedOut");
                                break;
                            case CustomerLoginResults.WrongPassword:
                            default:
                                model.Message = _localizationService.GetResource("Account.Login.WrongCredentials");
                                break;
                        }
                    }
                    else
                    {
                        var otp = _genericAttributeService.GetAttribute<string>(customer, "CheckoutOTP",
                        _workContext.GetCurrentStoreId);
                        if (otp == model.OTP)
                        {
                            isValidUser = true;
                            model.IsValid = true;
                            model.IsLogedIn = true;
                        }
                        else
                        {
                            model.IsValid = false;
                            model.Message = _localizationService.GetResource("Checkout.LoginSignup.InvalidOTP.Validate");
                        }
                    }

                    if (isValidUser)
                    {
                        _shoppingCartService.MigrateShoppingCart(_workContext.CurrentCustomer, customer, true);

                        //Custom code by KP 25Apr19
                        var orderDetails = _workContext.CurrentOrderDetails;
                        orderDetails.CustomerId = customer.Id;
                        _workContext.CurrentOrderDetails = orderDetails;


                        //sign in new customer
                        _authenticationService.SignIn(customer, true);

                        //raise event       
                        _eventPublisher.Publish(new CustomerLoggedinEvent(customer));

                        //activity log
                        _customerActivityService.InsertActivity(customer, "PublicStore.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"));
                        _genericAttributeService.SaveAttribute(customer,
                      "CheckoutOTP", "", _workContext.GetCurrentStoreId);
                    }
                }
            }

            if (model.IsValid)
            {
                var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, _workContext.GetCurrentStoreId);
                _workContext.CurrentCustomer = customer;
                var opcModel = _checkoutModelFactory.PrepareOnePageCheckoutModel(cart);
                var billingAddressModel = opcModel.BillingAddress;

                var headerLinkModel = _commonModelFactory.PrepareHeaderLinksModel();

                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "billing",
                        html = RenderPartialViewToString("OpcBillingAddress", billingAddressModel),
                    },
                    goto_section = "billing",
                    changeHeaderLInk = true,
                    headerLink = RenderPartialViewToString("_headerLinks", headerLinkModel)
                });
            }
            else
            {
                model.ShowOTP = !model.IsEmail;
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "checkout-login",
                        html = RenderPartialViewToString("OpcLogin", model)
                    },
                    goto_section = "checkout-login"
                });
            }

        }

        [HttpPost]
        public IActionResult OpcCheckoutAsGuest(CheckoutLoginSignUpModel model)
        {
            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, _workContext.GetCurrentStoreId);
            if (!cart.Any())
                throw new Exception("Your cart is empty");

            if (!_orderSettings.OnePageCheckoutEnabled)
                throw new Exception("One page checkout is disabled");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                throw new Exception("Anonymous checkout is not allowed");

            var opcModel = _checkoutModelFactory.PrepareOnePageCheckoutModel(cart);
            var billingAddressModel = opcModel.BillingAddress;

            return Json(new
            {
                update_section = new UpdateSectionJsonModel
                {
                    name = "billing",
                    html = RenderPartialViewToString("OpcBillingAddress", billingAddressModel)
                },
                goto_section = "billing"
            });
        }

        [HttpPost]
        public IActionResult OpcSignUp(SignUpModel model)
        {
            var customer = new Customer();
            if (ModelState.IsValid)
            {
                customer = _customerService.GetCustomerByEmail(model.Email);
                if (customer == null)
                {
                    customer = _customerService.GetCustomerByPhone(_workContext.CurrentStoreISDCode + " " + model.Phone);
                }
                if (customer != null)
                {
                    model.Message = _localizationService.GetResource("Checkout.LoginSignup.SignUp.AlreadyExist");
                }
                else
                {
                    if (_workContext.CurrentCustomer.IsRegistered())
                    {
                        //Already registered customer. 
                        _authenticationService.SignOut();

                        //raise logged out event       
                        _eventPublisher.Publish(new CustomerLoggedOutEvent(_workContext.CurrentCustomer));

                        //Save a new record
                        _workContext.CurrentCustomer = _customerService.InsertGuestCustomer();
                    }
                    customer = _workContext.CurrentCustomer;
                    customer.RegisteredInStoreId = _workContext.GetCurrentStoreId;

                    var isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard;
                    if (_storeInformationSettings.EnableOTPAuthentication)
                        isApproved = false;
                    var registrationRequest = new CustomerRegistrationRequest(customer,
                        model.Email,
                        model.Email,
                        model.Password,
                        _customerSettings.DefaultPasswordFormat,
                        _workContext.GetCurrentStoreId,
                        isApproved);
                    var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);
                    if (registrationResult.Success)
                    {
                        model.IsValid = true;
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FirstNameAttribute, model.FirstName, _workContext.GetCurrentStoreId);
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.LastNameAttribute, model.LastName, _workContext.GetCurrentStoreId);
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, _workContext.CurrentStoreISDCode + " " + model.Phone, _workContext.GetCurrentStoreId);

                        //Custom code by KP 25Apr19
                        var orderDetails = _workContext.CurrentOrderDetails;
                        orderDetails.CustomerId = customer.Id;
                        _workContext.CurrentOrderDetails = orderDetails;

                        //raise event       
                        _eventPublisher.Publish(new CustomerLoggedinEvent(customer));

                        //activity log
                        _customerActivityService.InsertActivity(customer, "PublicStore.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"));

                        if (_storeInformationSettings.EnableOTPAuthentication)
                        {
                            RegistrationOTP(customer);
                            var otpModel = new OTPVerificationModel();
                            otpModel.CustomerId = customer.Id;
                            return Json(new
                            {
                                update_section = new UpdateSectionJsonModel
                                {
                                    name = "checkout-signup",
                                    html = RenderPartialViewToString("OpcOTPVerification", otpModel)
                                },
                                goto_section = "checkout-signup"
                            });
                        }

                        //sign in new customer
                        _authenticationService.SignIn(customer, true);

                    }
                    else
                    {
                        if (registrationResult.Errors != null && registrationResult.Errors.Count > 0)
                        {
                            model.Message = registrationResult.Errors.FirstOrDefault();
                        }
                    }
                }
            }

            if (model.IsValid)
            {
                return LoadStepAfterSignUp(customer);
            }
            else
            {
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "checkout-signup",
                        html = RenderPartialViewToString("OpcSignup", model)
                    },
                    goto_section = "checkout-signup"
                });
            }

        }

        [HttpPost]
        public virtual IActionResult OpcOTPVerification(OTPVerificationModel model)
        {
            var customer = _customerService.GetCustomerById(model.CustomerId);
            if (customer == null)
                return RedirectToRoute("Homepage");

            var phone = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute, customer.RegisteredInStoreId, "");
            if (!phone.Contains(" "))
                phone = _workContext.CurrentStoreISDCode + " " + phone;
            var otpPhone = _genericAttributeService.GetAttribute<string>(customer, "RegistrationOTP", _workContext.GetCurrentStoreId);
            if (string.IsNullOrEmpty(model.OTP))
            {
                ModelState.AddModelError("", _localizationService.GetResource("Registration.LoginSignup.EnterOTP"));
            }
            else if (otpPhone != model.OTP)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Checkout.LoginSignup.InvalidOTP.Validate"));
            }

            //return model if errors
            if (ModelState.ErrorCount > 0)
            {
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "checkout-signup",
                        html = RenderPartialViewToString("OpcOTPVerification", model)
                    },
                    goto_section = "checkout-signup"
                });
            }

            //activate customer
            customer.Active = true;
            _customerService.UpdateCustomer(customer);

            //send customer welcome message
            _workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);
            _workflowMessageService.SendCustomerWelcomeMessageOnPhone(customer, phone, _workContext.WorkingLanguage.Id);
            //sign in new customer
            _authenticationService.SignIn(customer, true);
            return LoadStepAfterSignUp(customer);
        }

        public virtual IActionResult OpcSaveBillingCustom(CheckoutBillingAddressModel model, IFormCollection form)
        {
            int storeId = _workContext.GetCurrentStoreId;
            var customer = _workContext.CurrentCustomer;
            bool isNewAddress = false;
            try
            {
                //validation
                var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, storeId);
                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                int.TryParse(form["billing_address_id"], out int billingAddressId);

                if (billingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == billingAddressId);
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    _workContext.CurrentCustomer.BillingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                else
                {
                    //new address
                    //Added By Mah 26022019
                    var newAddress = model.BillingNewAddress;

                    //for vonzuu store - check limited addres fields only, for other store check with reguler scenario
                    var checkoutAddressHasLimitedFields = _settingService.GetSettingByKey<bool>("NB.Checkout.AddnewAddress.Contains.LimitedFields", false, _storeContext.CurrentStore.Id);
                    var addressHasMissingFields = false;
                    if (checkoutAddressHasLimitedFields)
                    {
                        //currently check with limited fields for vonzuu store.
                        if ((string.IsNullOrEmpty(newAddress.Address1) || string.IsNullOrEmpty(newAddress.Address2)) || (!customer.IsRegistered() && string.IsNullOrEmpty(newAddress.PhoneNumber)))
                            addressHasMissingFields = true;
                    }
                    else
                    {
                        //reguler address checking for all other store.
                        if ((string.IsNullOrEmpty(newAddress.Address1) || string.IsNullOrEmpty(newAddress.Address2) || string.IsNullOrEmpty(newAddress.Landmark)) || (!customer.IsRegistered() && (string.IsNullOrEmpty(newAddress.FirstName) || string.IsNullOrEmpty(newAddress.Email) || string.IsNullOrEmpty(newAddress.PhoneNumber))))
                            addressHasMissingFields = true;
                    }

                    if (addressHasMissingFields)
                    {
                        var billingAddressModel = _checkoutModelFactory.PrepareBillingAddressModel(cart,
                        selectedCountryId: newAddress.CountryId,
                        overrideAttributesXml: "");
                        billingAddressModel.NewAddressPreselected = true;
                        billingAddressModel.BillingNewAddress.Message = _localizationService.GetResource("Checkout.BillingNewAddress.Validation.Message");
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel
                            {
                                name = "billing",
                                html = RenderPartialViewToString("OpcBillingAddress", billingAddressModel)
                            },
                            wrong_billing_address = true,
                        });
                    }

                    if (customer.IsRegistered())
                    {
                        newAddress.FirstName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute, (customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0));
                        newAddress.LastName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute, (customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0));
                        newAddress.PhoneNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute, (customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0));
                        newAddress.Email = _workContext.CurrentCustomer.Email;
                    }
                    else
                    {
                        newAddress.FirstName = model.BillingNewAddress.FirstName;
                        newAddress.LastName = model.BillingNewAddress.LastName;
                        newAddress.PhoneNumber = model.BillingNewAddress.PhoneNumber;
                        newAddress.Email = model.BillingNewAddress.Email;
                    }
                    if (_workContext.GetCurrentStoreId == 36)
                    {
                        newAddress.PhoneNumber = newAddress.Landmark;
                    }

                    //custom address attributes
                    var customAttributes = _addressAttributeParser.ParseCustomAddressAttributes(form); //model.Form.ParseCustomAddressAttributes(_addressAttributeParser, _addressAttributeService);
                    var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
                    foreach (var error in customAttributeWarnings)
                    {
                        ModelState.AddModelError("", error);
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _addressService.FindAddress(_workContext.CurrentCustomer.Addresses.ToList(),
                    newAddress.FirstName, newAddress.LastName, newAddress.PhoneNumber,
                    newAddress.Email, newAddress.FaxNumber, newAddress.Company,
                    newAddress.Address1, newAddress.Address2, newAddress.City,
                    newAddress.County, newAddress.StateProvinceId, newAddress.ZipPostalCode,
                    newAddress.CountryId, customAttributes);
                    if (address == null)
                    {
                        //address is not found. let's create a new one
                        address = newAddress.ToEntity();
                        //address.Landmark = newAddress.Landmark;
                        address.CustomAttributes = customAttributes;
                        address.CountryId = newAddress.CountryId;
                        address.CreatedOnUtc = DateTime.UtcNow;
                        address.Latitude = newAddress.Latitude;
                        address.Longitude = newAddress.Longitude;
                        address.ZipPostalCode = newAddress.ZipPostalCode;
                        address.County = newAddress.Landmark;
                        address.FlatNo = newAddress.HouseNumber;
                        if (Convert.ToInt32(model.BillingNewAddress.LocationType) == (int)LocationAddressType.Home)
                        {
                            address.Landmark = Convert.ToString(LocationAddressType.Home);
                            address.AddressTypeId = (int)LocationAddressType.Home;
                        }
                        else if (Convert.ToInt32(model.BillingNewAddress.LocationType) == (int)LocationAddressType.Office)
                        {
                            address.Landmark = Convert.ToString(LocationAddressType.Office);
                            address.AddressTypeId = (int)LocationAddressType.Office;
                        }
                        else if (Convert.ToInt32(model.BillingNewAddress.LocationType) == (int)LocationAddressType.Other)
                        {
                            address.Landmark = Convert.ToString(LocationAddressType.Other);
                            address.AddressTypeId = (int)LocationAddressType.Other;
                        }
                        //some validation
                        if (address.CountryId == 0)
                        {
                            if (_storeContext.CurrentStore.CountryId > 0)
                                address.CountryId = _storeContext.CurrentStore.CountryId;
                            else
                                address.CountryId = null;
                        }
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        if (address.CountryId.HasValue && address.CountryId.Value > 0)
                        {
                            address.Country = _countryService.GetCountryById(address.CountryId.Value);
                        }

                        address.StateProvinceId = newAddress.StateProvinceId;
                        address.City = newAddress.City;
                        _workContext.CurrentCustomer.Addresses.Add(address);
                        //New                    
                        _workContext.CurrentCustomer.CustomerAddressMappings.Add(new CustomerAddressMapping { Address = address });
                        //
                        isNewAddress = true;
                    }
                    if (address.CountryId == null || address.CountryId == 0)
                    {
                        var storeCountry = _commonService.GetStoreCountryStateMapping(storeId).FirstOrDefault();
                        if (storeCountry != null)
                        {
                            address.CountryId = storeCountry.CountryId;
                        }
                    }
                    _workContext.CurrentCustomer.BillingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }

                if (_shoppingCartService.ShoppingCartRequiresShipping(cart))
                {
                    //shipping is required
                    if (_shippingSettings.ShipToSameAddress)
                    {
                        //ship to the same address
                        _workContext.CurrentCustomer.ShippingAddress = _workContext.CurrentCustomer.BillingAddress;
                        _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                        //reset selected shipping method (in case if "pick up in store" was selected)
                        _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedShippingOptionAttribute, null, storeId);
                        _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedPickupPointAttribute, null, storeId);
                        //limitation - "Ship to the same address" doesn't properly work in "pick up in store only" case (when no shipping plugins are available) 
                        return OpcLoadStepAfterShippingAddress(cart);
                    }

                    //do not ship to the same address
                    var shippingAddressModel = _checkoutModelFactory.PrepareShippingAddressModel(prePopulateNewAddressWithCustomerFields: true);

                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel
                        {
                            name = "shipping",
                            html = RenderPartialViewToString("OpcShippingAddress", shippingAddressModel)
                        },
                        goto_section = "shipping"
                    });
                }

                //shipping is not required
                _workContext.CurrentCustomer.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedShippingOptionAttribute, null, storeId);

                //load next step
                return OpcLoadStepAfterShippingMethod(cart, isNewAddress);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public IActionResult GetSelectedAddress()
        {
            var selectedAddress = _workContext.CurrentCustomer.BillingAddress;
            var model = _checkoutModelFactory.PrepareSelectedBillingAddressModel(selectedAddress);
            return PartialView("OpcSelectedAddress", model);
        }

        public IActionResult GetBillingAddress()
        {
            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, _workContext.GetCurrentStoreId);
            var opcModel = _checkoutModelFactory.PrepareOnePageCheckoutModel(cart);
            var billingAddressModel = opcModel.BillingAddress;
            billingAddressModel.NewAddressPreselected = false;
            return PartialView("OpcBillingAddress", billingAddressModel);

        }

        public virtual IActionResult OpcSavePaymentInfoCustom(IFormCollection form)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;
            try
            {
                //validation
                var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, _workContext.GetCurrentStoreId);
                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                var paymentMethodSystemName = _genericAttributeService.GetAttribute<string>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedPaymentMethodAttribute, CurrStoreId);
                var paymentMethod = _paymentPluginManager.LoadPluginBySystemName(paymentMethodSystemName);
                if (paymentMethod == null)
                    throw new Exception("Payment method is not selected");

                //check minimum order amount condition

                // get order type 
                var currentOrderDetails = _workContext.CurrentOrderDetails.OrderType;
                var ordType = 2;
                if (_workContext.CurrentOrderDetails.OrderType != null)
                {
                    ordType = _workContext.CurrentOrderDetails.OrderType.Value;
                }

                // get store setting
                bool enableMinOrderAmount = _settingService.GetSettingByKey<bool>("setting.storesetup.EnableMinOrderAmount", false, _storeContext.CurrentStore.Id);
                if (enableMinOrderAmount && ordType == 1)
                {
                    //subtotal
                    var subTotalIncludingTax = _workContext.TaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal;
                    _orderTotalCalculationService.GetShoppingCartSubTotal(cart, subTotalIncludingTax, out var orderSubTotalDiscountAmountBase, out var _, out var subTotalWithoutDiscountBase, out var _);
                    var subtotalBase = subTotalWithoutDiscountBase;
                    var subtotal = _currencyService.ConvertFromPrimaryStoreCurrency(subtotalBase, _workContext.WorkingCurrency);

                    // get minimum order value
                    var minOrderAmount = _workContext.CurrentMerchant.MinOrderAmount;
                    minOrderAmount = _currencyService.ConvertFromPrimaryStoreCurrency(minOrderAmount, _workContext.WorkingCurrency);

                    //check Minorder amount is not greater then of subtotal
                    if (minOrderAmount > subtotal)
                        throw new Exception(string.Format(_localizationService.GetResource("NB.Checkout.MinOrderAmount"), _priceFormatter.FormatPrice(minOrderAmount, true, _workContext.WorkingCurrency)));
                }
                var warnings = paymentMethod.ValidatePaymentForm(form);
                foreach (var warning in warnings)
                    ModelState.AddModelError("", warning);
                if (ModelState.IsValid)
                {
                    //get payment info
                    var paymentInfo = paymentMethod.GetPaymentInfo(form);

                    //session save
                    HttpContext.Session.Set("OrderPaymentInfo", paymentInfo);

                    var objpaymenInfoModel = _checkoutModelFactory.PreparePaymentInfoModel(paymentMethod);
                    return OpcConfirmOrderCustom(objpaymenInfoModel);
                }

                //If we got this far, something failed, redisplay form
                var paymenInfoModel = _checkoutModelFactory.PreparePaymentInfoModel(paymentMethod);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "payment-info",
                        html = RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                    }
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public virtual IActionResult OpcConfirmOrderCustom(CheckoutPaymentInfoModel paymenInfoModel)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);

            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");


            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            //model
            var model = _checkoutModelFactory.PrepareConfirmOrderModel(cart);
            try
            {
                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //place order
                var processPaymentRequest = HttpContext.Session.Get<ProcessPaymentRequest>("OrderPaymentInfo");
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    if (_orderProcessingService.IsPaymentWorkflowRequired(cart))
                        return RedirectToRoute("CheckoutPaymentInfo");

                    processPaymentRequest = new ProcessPaymentRequest();
                }
                GenerateOrderGuid(processPaymentRequest);
                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = _genericAttributeService.GetAttribute<string>(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedPaymentMethodAttribute, CurrStoreId);
                HttpContext.Session.Set<ProcessPaymentRequest>("OrderPaymentInfo", processPaymentRequest);

                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
                if (placeOrderResult.Success)
                {
                    HttpContext.Session.Set<ProcessPaymentRequest>("OrderPaymentInfo", null);
                    var postProcessPaymentRequest = new PostProcessPaymentRequest
                    {
                        Order = placeOrderResult.PlacedOrder
                    };

                    var paymentMethod = _paymentPluginManager.LoadPluginBySystemName(placeOrderResult.PlacedOrder.PaymentMethodSystemName);
                    if (paymentMethod == null)
                        //payment method could be null if order total is 0
                        //success
                        return Json(new { success = 1 });

                    if (paymentMethod.PaymentMethodType == PaymentMethodType.Redirection)
                    {
                        //Redirection will not work because it's AJAX request.
                        //That's why we don't process it here (we redirect a user to another page where he'll be redirected)

                        //redirect
                        return Json(new
                        {
                            redirect = $"{_webHelper.GetStoreLocation()}checkout/OpcCompleteRedirectionPayment"
                        });
                    }

                    _paymentService.PostProcessPayment(postProcessPaymentRequest);
                    //success
                    return Json(new { success = 1 });
                }

                //error
                foreach (var error in placeOrderResult.Errors)
                    paymenInfoModel.Warnings.Add(error);

                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "payment-info",
                        html = RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                    }
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        private string GenerateRandomOTP(int iOTPLength, string[] saAllowedCharacters)
        {
            string sOTP = String.Empty;
            string sTempChars = String.Empty;
            Random rand = new Random();

            for (int i = 0; i < iOTPLength; i++)
            {
                int p = rand.Next(0, saAllowedCharacters.Length);
                sTempChars = saAllowedCharacters[rand.Next(0, saAllowedCharacters.Length)];
                sOTP += sTempChars;
            }

            return sOTP;
        }

        public IActionResult SaveOrderNote(string orderNote)
        {
            var orderDetails = _workContext.CurrentOrderDetails;
            orderDetails.OrderNote = orderNote;
            _workContext.CurrentOrderDetails = orderDetails;

            return Json(new { success = true });
        }

        //For Geo Location Check
        public JsonResult CheckGeoArea(string latlong)
        {
            bool result = false;
            if (_workContext.CurrentOrderDetails.OrderType != 1)
            {
                result = true;
                return Json(result);
            }

            latlong = latlong.Replace("\"", "");
            if (latlong != null)
            {
                var retValue = _commonService.CheckGeoArea(_workContext.CurrentMerchant.Id.ToString(), latlong);
                result = (retValue == 1);
            }

            return Json(result);
        }

        #endregion

        #region wallet

        public virtual IActionResult OnePageWalletCheckout(Decimal walletAmount)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            var model = _checkoutModelFactory.PrepareOnePageWalletCheckoutModel(walletAmount);
            return View(model);
        }

        public virtual IActionResult OpcWalletSaveShippingMethod(string shippingoption)
        {
            try
            {
                //Added By Mah
                int CurrStoreId = _workContext.GetCurrentStoreId;

                //load next step
                return OpcWalletLoadStepAfterShippingMethod();
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        protected virtual JsonResult OpcWalletLoadStepAfterShippingMethod()
        {
            int CurrStoreId = _workContext.GetCurrentStoreId;
            if (1 == 1)
            {
                //payment is required
                var paymentMethodModel = _checkoutModelFactory.PrepareWalletPaymentMethodModel();

                if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                    paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
                {
                    //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                    //so customer doesn't have to choose a payment method

                    var selectedPaymentMethodSystemName = paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName;
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        NopCustomerDefaults.SelectedPaymentMethodAttribute,
                        selectedPaymentMethodSystemName, CurrStoreId);

                    var paymentMethodInst = _paymentPluginManager
                        .LoadPluginBySystemName(selectedPaymentMethodSystemName, _workContext.CurrentCustomer, CurrStoreId);
                    if (!_paymentPluginManager.IsPluginActive(paymentMethodInst))
                        throw new Exception("Selected payment method can't be parsed");

                    return OpcWalletLoadStepAfterPaymentMethod(paymentMethodInst);
                }

                var model = new MiniShoppingCartModel();
                model = _shoppingCartModelFactory.PrepareMiniShoppingCartModel(model);

                //customer have to choose a payment method
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "payment-method",
                        html = RenderPartialViewToString("OpcPaymentMethods", paymentMethodModel)
                    },
                    goto_section = "payment_method",
                    updateFlyoutShoppingCart = true,
                    updateFlyoutShoppingCartHtml = RenderPartialViewToString("~/Themes/" + _themeContext.WorkingThemeName.ToString() + "/Views/Shared/Components/FlyoutShoppingCart/Default.cshtml", model)
                });
            }

            //payment is not required
            _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                NopCustomerDefaults.SelectedPaymentMethodAttribute, null, CurrStoreId);

            var confirmOrderModel = _checkoutModelFactory.PrepareConfirmOrderModel(null);
            return Json(new
            {
                update_section = new UpdateSectionJsonModel
                {
                    name = "confirm-order",
                    html = RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                },
                goto_section = "confirm_order"
            });
        }

        protected virtual JsonResult OpcWalletLoadStepAfterPaymentMethod(IPaymentMethod paymentMethod)
        {
            if (paymentMethod.SkipPaymentInfo ||
                (paymentMethod.PaymentMethodType == PaymentMethodType.Redirection && _paymentSettings.SkipPaymentInfoStepForRedirectionPaymentMethods))
            {
                //skip payment info page
                var paymentInfo = new ProcessPaymentRequest();

                //session save
                HttpContext.Session.Set("OrderPaymentInfo", paymentInfo);

                var confirmOrderModel = _checkoutModelFactory.PrepareConfirmOrderModel(null);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "confirm-order",
                        html = RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                    },
                    goto_section = "confirm_order"
                });
            }

            //return payment info page
            var paymenInfoModel = _checkoutModelFactory.PreparePaymentInfoModel(paymentMethod);
            return Json(new
            {
                update_section = new UpdateSectionJsonModel
                {
                    name = "payment-info",
                    html = RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                },
                goto_section = "payment_info"
            });
        }

        public virtual IActionResult OpcWalletSaveBillingCustom(CheckoutBillingAddressModel model, IFormCollection form)
        {
            int storeId = _workContext.GetCurrentStoreId;
            var customer = _workContext.CurrentCustomer;
            bool isNewAddress = false;
            try
            {
                //validation


                //shipping is not required
                _workContext.CurrentCustomer.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedShippingOptionAttribute, null, storeId);

                //load next step
                return OpcWalletLoadStepAfterShippingMethod();
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }


        public virtual IActionResult OpcWalletSavePaymentMethod(string paymentmethod, CheckoutPaymentMethodModel model)
        {
            try
            {
                //Added By Mah
                int CurrStoreId = _workContext.GetCurrentStoreId;

                //validation
                if (_orderSettings.CheckoutDisabled)
                    throw new Exception(_localizationService.GetResource("Checkout.Disabled"));


                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                //payment method 
                if (string.IsNullOrEmpty(paymentmethod))
                    throw new Exception("Selected payment method can't be parsed");

                //reward points
                if (_rewardPointsSettings.Enabled)
                {
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        NopCustomerDefaults.UseRewardPointsDuringCheckoutAttribute, model.UseRewardPoints,
                        CurrStoreId);
                }

                //CustomerWallet
                if (_settingService.GetSettingByKey<bool>("storesettings.store.walletenabled", false, CurrStoreId))
                {
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        NopCustomerDefaults.UseCustomerWalletDuringCheckoutAttribute, model.UseCustomerWallet,
                        CurrStoreId);
                }

                var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault()
                       ?? throw new Exception("Address can't be loaded");

                _workContext.CurrentCustomer.BillingAddress = address;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                var paymentMethodInst = _paymentPluginManager
                    .LoadPluginBySystemName(paymentmethod, _workContext.CurrentCustomer, CurrStoreId);
                if (!_paymentPluginManager.IsPluginActive(paymentMethodInst))
                    throw new Exception("Selected payment method can't be parsed");

                //save
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedPaymentMethodAttribute, paymentmethod, CurrStoreId);

                return OpcWalletLoadStepAfterPaymentMethod(paymentMethodInst);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public virtual IActionResult OpcWalletSavePaymentInfo(IFormCollection form)
        {
            try
            {
                //Added By Mah
                int CurrStoreId = _workContext.GetCurrentStoreId;

                //validation
                if (_orderSettings.CheckoutDisabled)
                    throw new Exception(_localizationService.GetResource("Checkout.Disabled"));

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                var paymentMethodSystemName = _genericAttributeService.GetAttribute<string>(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedPaymentMethodAttribute, CurrStoreId);
                var paymentMethod = _paymentPluginManager.LoadPluginBySystemName(paymentMethodSystemName)
                    ?? throw new Exception("Payment method is not selected");

                var warnings = paymentMethod.ValidatePaymentForm(form);
                foreach (var warning in warnings)
                    ModelState.AddModelError("", warning);
                if (ModelState.IsValid)
                {
                    //get payment info
                    var paymentInfo = paymentMethod.GetPaymentInfo(form);
                    //set previous order GUID (if exists)
                    GenerateOrderGuid(paymentInfo);

                    //session save
                    HttpContext.Session.Set("OrderPaymentInfo", paymentInfo);

                    var confirmOrderModel = _checkoutModelFactory.PrepareConfirmOrderModel(null);
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel
                        {
                            name = "confirm-order",
                            html = RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }

                //If we got this far, something failed, redisplay form
                var paymenInfoModel = _checkoutModelFactory.PreparePaymentInfoModel(paymentMethod);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "payment-info",
                        html = RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                    }
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public virtual IActionResult OpcWalletSavePaymentInfoCustom(IFormCollection form)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;
            try
            {
                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                var paymentMethodSystemName = _genericAttributeService.GetAttribute<string>(_workContext.CurrentCustomer, NopCustomerDefaults.SelectedPaymentMethodAttribute, CurrStoreId);
                var paymentMethod = _paymentPluginManager.LoadPluginBySystemName(paymentMethodSystemName);
                if (paymentMethod == null)
                    throw new Exception("Payment method is not selected");

                //check minimum order amount condition

                // get order type 
                var currentOrderDetails = _workContext.CurrentOrderDetails.OrderType;
                var ordType = 2;
                if (_workContext.CurrentOrderDetails.OrderType != null)
                {
                    ordType = _workContext.CurrentOrderDetails.OrderType.Value;
                }

                // get store setting
                bool enableMinOrderAmount = _settingService.GetSettingByKey<bool>("setting.storesetup.EnableMinOrderAmount", false, _storeContext.CurrentStore.Id);
                if (enableMinOrderAmount && ordType == 1)
                {
                    string subtotalBase = form["walletamount"];
                    decimal amount = 1000;
                    var subtotal = _currencyService.ConvertFromPrimaryStoreCurrency(amount, _workContext.WorkingCurrency);

                }
                var warnings = paymentMethod.ValidatePaymentForm(form);
                foreach (var warning in warnings)
                    ModelState.AddModelError("", warning);
                if (ModelState.IsValid)
                {
                    //get payment info
                    var paymentInfo = paymentMethod.GetPaymentInfo(form);

                    //session save
                    HttpContext.Session.Set("OrderPaymentInfo", paymentInfo);

                    var objpaymenInfoModel = _checkoutModelFactory.PreparePaymentInfoModel(paymentMethod);
                    return OpcWalletConfirmOrderCustom(objpaymenInfoModel);
                }

                //If we got this far, something failed, redisplay form
                var paymenInfoModel = _checkoutModelFactory.PreparePaymentInfoModel(paymentMethod);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "payment-info",
                        html = RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                    }
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public virtual IActionResult OpcWalletConfirmOrder()
        {
            try
            {
                //Added By Mah
                int CurrStoreId = _workContext.GetCurrentStoreId;

                //validation
                if (_orderSettings.CheckoutDisabled)
                    throw new Exception(_localizationService.GetResource("Checkout.Disabled"));

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //place order
                var processPaymentRequest = HttpContext.Session.Get<ProcessPaymentRequest>("OrderPaymentInfo");
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    throw new Exception("Payment information is not entered");
                }
                GenerateOrderGuid(processPaymentRequest);
                processPaymentRequest.StoreId = CurrStoreId;
                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = _genericAttributeService.GetAttribute<string>(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedPaymentMethodAttribute, CurrStoreId);
                HttpContext.Session.Set<ProcessPaymentRequest>("OrderPaymentInfo", processPaymentRequest);
                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
                if (placeOrderResult.Success)
                {
                    HttpContext.Session.Set<ProcessPaymentRequest>("OrderPaymentInfo", null);
                    var postProcessPaymentRequest = new PostProcessPaymentRequest
                    {
                        Order = placeOrderResult.PlacedOrder
                    };

                    var paymentMethod = _paymentPluginManager.LoadPluginBySystemName(placeOrderResult.PlacedOrder.PaymentMethodSystemName);
                    if (paymentMethod == null)
                        //payment method could be null if order total is 0
                        //success
                        return Json(new { success = 1 });

                    if (paymentMethod.PaymentMethodType == PaymentMethodType.Redirection)
                    {
                        //Redirection will not work because it's AJAX request.
                        //That's why we don't process it here (we redirect a user to another page where he'll be redirected)

                        //redirect
                        return Json(new
                        {
                            redirect = $"{_webHelper.GetStoreLocation()}checkout/OpcCompleteRedirectionPayment"
                        });
                    }

                    _paymentService.PostProcessPayment(postProcessPaymentRequest);
                    //success
                    return Json(new { success = 1 });
                }

                //error
                var confirmOrderModel = new CheckoutConfirmModel();
                foreach (var error in placeOrderResult.Errors)
                    confirmOrderModel.Warnings.Add(error);

                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "confirm-order",
                        html = RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                    },
                    goto_section = "confirm_order"
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public virtual IActionResult OpcWalletConfirmOrderCustom(CheckoutPaymentInfoModel paymenInfoModel)
        {
            //Added By Mah
            int CurrStoreId = _workContext.GetCurrentStoreId;

            //validation
            if (_orderSettings.CheckoutDisabled)
                return RedirectToRoute("ShoppingCart");

            var vcart = HttpContext.Session.Get<WalletShoppingCart>("cart"); //_shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, CurrStoreId);
            var acart = vcart.CartItems;
            var cart = new List<ShoppingCartItem>();
            cart.Add(new ShoppingCartItem
            {
                Customer = _workContext.CurrentCustomer,
                Product = _productService.GetProductBySku("Subscriptionfeature"),
                CreatedOnUtc = acart[0].CreatedOnUtc,
                CustomerEnteredPrice = acart[0].CustomerEnteredPrice,
                CustomerId = acart[0].CustomerId,
                Id = acart[0].Id,
                ProductId = acart[0].ProductId,
                Quantity = acart[0].Quantity,
                ShoppingCartType = acart[0].ShoppingCartType,
                StoreId = acart[0].StoreId
            });
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");


            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return Challenge();

            //model
            var model = _checkoutModelFactory.PrepareWalletConfirmOrderModel();
            try
            {
                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //place order
                var processPaymentRequest = HttpContext.Session.Get<ProcessPaymentRequest>("OrderPaymentInfo");
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    if (_orderProcessingService.IsPaymentWorkflowRequired(cart))
                        return RedirectToRoute("CheckoutPaymentInfo");

                    processPaymentRequest = new ProcessPaymentRequest();
                }
                GenerateOrderGuid(processPaymentRequest);
                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = _genericAttributeService.GetAttribute<string>(_workContext.CurrentCustomer,
                    NopCustomerDefaults.SelectedPaymentMethodAttribute, CurrStoreId);
                HttpContext.Session.Set<ProcessPaymentRequest>("OrderPaymentInfo", processPaymentRequest);

                var placeOrderResult = _orderProcessingService.PlaceWalletOrder(processPaymentRequest, cart);
                if (placeOrderResult.Success)
                {
                    HttpContext.Session.Set<ProcessPaymentRequest>("OrderPaymentInfo", null);
                    var postProcessPaymentRequest = new PostProcessPaymentRequest
                    {
                        Order = placeOrderResult.PlacedOrder
                    };

                    var paymentMethod = _paymentPluginManager.LoadPluginBySystemName(placeOrderResult.PlacedOrder.PaymentMethodSystemName);
                    if (paymentMethod == null)
                        //payment method could be null if order total is 0
                        //success
                        return Json(new { success = 1 });

                    if (paymentMethod.PaymentMethodType == PaymentMethodType.Redirection)
                    {
                        //Redirection will not work because it's AJAX request.
                        //That's why we don't process it here (we redirect a user to another page where he'll be redirected)

                        //redirect
                        return Json(new
                        {
                            redirect = $"{_webHelper.GetStoreLocation()}checkout/OpcCompleteRedirectionPayment"
                        });
                    }

                    _paymentService.PostProcessPayment(postProcessPaymentRequest);
                    //success
                    return Json(new { success = 1 });
                }

                //error
                foreach (var error in placeOrderResult.Errors)
                    paymenInfoModel.Warnings.Add(error);

                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "payment-info",
                        html = RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                    }
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }
        #endregion
    }
}