﻿using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Models.RatingReview
{
    public partial class RatingReviewMainModel : BaseNopEntityModel
    {
        public int OrderId { get; set; }
        public bool SkipNotInterestedCheck { get; set; }
        public int DirectItemId { get; set; }
    }
}