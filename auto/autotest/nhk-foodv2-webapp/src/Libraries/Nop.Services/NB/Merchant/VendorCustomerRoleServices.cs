﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.NB.Merchant;
using Nop.Services.Catalog;

namespace Nop.Services.NB.Merchant
{
    /// <summary>
    /// VendorCustomerRole service
    /// </summary>
    public partial class VendorCustomerRoleServices : IVendorCustomerRoleServices
    {
        #region Fields

        private readonly IRepository<VendorCustomerRoleMapping> _vendorCustomerRoleMapRepository;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        public VendorCustomerRoleServices(IRepository<VendorCustomerRoleMapping> vendorCustomerRoleMapRepository,
            ICacheManager cacheManager)
        {
            _vendorCustomerRoleMapRepository = vendorCustomerRoleMapRepository;
            _cacheManager = cacheManager;
        }

        #endregion

        /// <summary>
        /// Gets all vendor vendor customer role mapping mappings
        /// </summary>
        /// <returns>Vendor vendor customer role mapping mappings</returns>
        public virtual IList<VendorCustomerRoleMapping> GetAllVendorCustomerRoleMappings()
        {
            var query = _vendorCustomerRoleMapRepository.Table;
            var mappings = query.OrderBy(m => m.Id).ToList();
            return mappings;
        }

        /// <summary>
        /// Inserts a vendor customer role mapping
        /// </summary>
        /// <param name="VendorCustomerRoleMapping">vendor customer role mapping</param>
        public virtual void InsertVendorCustomerRoleMapping(VendorCustomerRoleMapping vendorCustomerRoleMapping)
        {
            if (vendorCustomerRoleMapping == null)
                throw new ArgumentNullException(nameof(vendorCustomerRoleMapping));

            _vendorCustomerRoleMapRepository.Insert(vendorCustomerRoleMapping);

            //clear workcontext user sub admin cache
            _cacheManager.RemoveByPrefix(NopNBDefaults.UserSubAdminPrefixCacheKey);
        }

        /// <summary>
        /// Updates the vendor customer role mapping
        /// </summary>
        /// <param name="VendorCustomerRoleMapping">vendor customer role mapping</param>
        public virtual void UpdateVendorCustomerRoleMapping(VendorCustomerRoleMapping vendorCustomerRoleMapping)
        {
            if (vendorCustomerRoleMapping == null)
                throw new ArgumentNullException(nameof(vendorCustomerRoleMapping));

            _vendorCustomerRoleMapRepository.Update(vendorCustomerRoleMapping);

            //clear workcontext user sub admin cache
            _cacheManager.RemoveByPrefix(NopNBDefaults.UserSubAdminPrefixCacheKey);
        }

        /// <summary>
        /// Delete a vendor customer role mapping
        /// </summary>
        /// <param name="VendorCustomerRoleMapping">vendor customer role mapping</param>
        public virtual void DeleteVendorCustomerRoleMapping(VendorCustomerRoleMapping vendorCustomerRoleMapping)
        {
            if (vendorCustomerRoleMapping == null)
                throw new ArgumentNullException(nameof(vendorCustomerRoleMapping));

            _vendorCustomerRoleMapRepository.Delete(vendorCustomerRoleMapping);

            //clear workcontext user sub admin cache
            _cacheManager.RemoveByPrefix(NopNBDefaults.UserSubAdminPrefixCacheKey);
        }

        /// <summary>
        /// Gets all vendor vendor customer role mappings by customer role identifier
        /// </summary>
        /// <param name="id">identifier</param>
        /// <returns>Vendor vendor customer role mapping mappings</returns>
        public virtual VendorCustomerRoleMapping GetAllMappingsById(int id)
        {
            if (id == 0)
                return null;

            return _vendorCustomerRoleMapRepository.GetById(id);
        }

        /// <summary>
        /// Gets all vendor vendor customer role mappings by customer role identifier
        /// </summary>
        /// <param name="customerRoleId">customer role identifier</param>
        /// <returns>Vendor vendor customer role mapping mappings</returns>
        public virtual IList<VendorCustomerRoleMapping> GetAllMappingsByCustomerRoleId(int customerRoleId)
        {
            if (customerRoleId == 0)
                return null;

            var query = from c in _vendorCustomerRoleMapRepository.Table
                        where c.CustomerRoleId == customerRoleId
                        orderby c.Id
                        select c;

            var mapiings = query;
            return mapiings.ToList();
        }
    }
}
