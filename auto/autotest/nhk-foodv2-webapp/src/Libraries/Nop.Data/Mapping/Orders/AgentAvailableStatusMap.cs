﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Orders;

namespace Nop.Data.Mapping.Orders
{
    public partial class AgentAvailableStatusMap : NopEntityTypeConfiguration<AgentAvailableStatus>
    {
        public override void Configure(EntityTypeBuilder<AgentAvailableStatus> builder)
        {
            builder.ToTable(nameof(AgentAvailableStatus));
            builder.HasKey(sci => sci.Id);

            base.Configure(builder);
        }
    }
}