﻿using FluentValidation;
using Nop.Web.Areas.Admin.Models.Vendors;
using Nop.Core.Domain.Vendors;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Services.Seo;
using Nop.Web.Framework.Validators;
using System;
using Nop.Core.Data;
using System.Data;
using Nop.Services.Vendors;

namespace Nop.Web.Areas.Admin.Validators.Vendors
{
    public partial class VendorValidator : BaseNopValidator<VendorModel>
    {
        public VendorValidator(ILocalizationService localizationService, IDbContext dbContext,IVendorService vendorService)
        {
            RuleFor(x => x.Geolocation).NotEmpty().WithMessage(localizationService.GetResource("Admin.Vendors.Fields.Geolocation.Required"));
            RuleFor(x => x.Geofancing).NotEmpty().WithMessage(localizationService.GetResource("Admin.Vendors.Fields.Geofancing.Required"));
            RuleFor(x => x.Geofancing).Must((x, context) =>
            {
                if (string.IsNullOrEmpty(x.Geofancing))
                    return true;

                return vendorService.CheckGeofancingFormat(x.Geofancing);


            }).WithMessage(localizationService.GetResource("Admin.Vendors.Fields.Geofancing.IncorrectFormat"));
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Vendors.Fields.Name.Required"));

            RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Admin.Vendors.Fields.Email.Required"));
            RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Admin.Common.WrongEmail"));
            RuleFor(x => x.PageSizeOptions).Must(ValidatorUtilities.PageSizeOptionsValidator).WithMessage(localizationService.GetResource("Admin.Vendors.Fields.PageSizeOptions.ShouldHaveUniqueItems"));
            RuleFor(x => x.PageSize).Must((x, context) =>
            {
                if (!x.AllowCustomersToSelectPageSize && x.PageSize <= 0)
                    return false;

                return true;
            }).WithMessage(localizationService.GetResource("Admin.Vendors.Fields.PageSize.Positive"));
            RuleFor(x => x.SeName).Length(0, NopSeoDefaults.SearchEngineNameLength)
                .WithMessage(string.Format(localizationService.GetResource("Admin.SEO.SeName.MaxLengthValidation"), NopSeoDefaults.SearchEngineNameLength));

            //Custom code from v4.0
            RuleFor(x => x.StoreId).NotEmpty().WithMessage(localizationService.GetResource("Admin.Vendors.Fields.StoreId.Required"));

            SetDatabaseValidationRules<Vendor>(dbContext);
        }
    }
}