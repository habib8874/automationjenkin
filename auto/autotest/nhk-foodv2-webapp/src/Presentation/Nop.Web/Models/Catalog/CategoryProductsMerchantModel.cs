﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Catalog
{
    public partial class CategoryProductsMerchantModel : BaseNopEntityModel
    {
        public CategoryProductsMerchantModel()
        {
            Products = new List<ProductOverviewModelMerchant>();
            Categories = new List<CategoryModel>();
        }
        public IList<ProductOverviewModelMerchant> Products { get; set; }
        public IList<CategoryModel> Categories { get; set; }
        public int CategoryId { get; set; }

    }
}