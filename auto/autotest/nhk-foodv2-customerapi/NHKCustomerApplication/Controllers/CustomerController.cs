using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Services;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CustomerDetails = NHKCustomerApplication.Models.CustomerDetails;

namespace NHKCustomerApplication.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class CustomerController : ControllerBase
	{
		private Dictionary<string, IEnumerable<string>> _allowedTokens;
		//private readonly string ChefBaseUrl = "http://nhkdev.nestorhawk.com/images/thumbs/";
		private ecuadordevContext dbcontext;
		private readonly ICommonService _commonService;
		private readonly string PasswordFormat = "SHA512";
		private readonly IConfiguration _configuration;
		public CustomerController(ecuadordevContext context,
								  ICommonService commonService)
		{
			dbcontext = context;
			_commonService = commonService;
		}
		/// <summary>
		///    Customer Registation
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/Signup")]
		[HttpPost]
		public async System.Threading.Tasks.Task<CustomerAPIResponses> RegisterCustomerAsync([FromBody] CustomerModel customer)
		{

			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.DeviceToken))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.DeviceTokenMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			Customer customerObj = new Customer();
			try
			{
				if (string.IsNullOrEmpty(customer.Email))
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EmailRequired", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
				if (!string.IsNullOrEmpty(customer.Email))
				{
					try
					{
						customer.Email = customer.Email.ToLower().ToString();
						var addr = Regex.IsMatch(customer.Email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
						//addr.Address == customer.Email;
						if (!addr)
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ProvideValidEmail", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
					}
					catch (Exception ex)
					{
						Helper.SentryLogs(ex);
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ProvideValidEmail", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}

				}
				if (string.IsNullOrEmpty(customer.MobileNo))
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.MobilenumberRequired", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
				if (!string.IsNullOrEmpty(customer.MobileNo))
				{
					string Number = customer.MobileNo.Split(' ')[1];
					if (!string.IsNullOrEmpty(Number))
					{

						foreach (char c in Number)
						{
							if (c < '0' || c > '9')
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ProvideValidPhoneNumber", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
					}
				}
				if (string.IsNullOrEmpty(customer.Pswd))
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.PasswordRequired", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}

				if (customer.StoreId == 0)
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.StoreRequired", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}

				var checkPhoneNumberExistList = dbcontext.GenericAttribute.AsEnumerable().Where(x => x.Key == "Phone" && x.StoreId == customer.StoreId).ToList();
				if (checkPhoneNumberExistList.Any())
				{

					if (checkPhoneNumberExistList.Select(x => x.Value.Replace(" ", "")).Equals(customer.MobileNo.Replace(" ", "")))
					{
						var number = checkPhoneNumberExistList.AsEnumerable().Where(x => x.Value.Replace(" ", "").Equals(customer.MobileNo.Replace(" ", ""))).FirstOrDefault();
						if (dbcontext.Customer.Where(x => x.Id == number.EntityId && x.Active == true).Any())
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.MobileNumberAlreadyRegistered", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.Ambiguous;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
						else
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.MobileNumberAlreadyRegistered", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.Ambiguous;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
					}
				}

				string passwordFormat = PasswordFormat;
				string saltKey = Helper.CreateSaltKey(5);
				CustomerSignUpResponseModel customerSignUpResponse = new CustomerSignUpResponseModel();
				int _min = 1000;
				int _max = 9999;
				Random _rdm = new Random();

				string otp = _rdm.Next(_min, _max).ToString();

				var connectionString = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("ConnectionStrings").GetSection("ecuadordatabase").Value;
				using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
				{
					string result = db.Query<string>("sp_Customer_SignUp", new
					{
						customer.ApiKey,
						CustomerGuid = Guid.NewGuid(),
						Username = customer.Email,
						customer.Email,
						LastIpAddress = "127.0.0.1",
						RegisteredInStoreId = customer.StoreId,
						Phone = customer.MobileNo,
						ProfilePic = "default-image_450.png",
						customer.LastName,
						customer.FirstName,
						Password = Helper.CreatePasswordHash(customer.Pswd, saltKey, passwordFormat),
						PasswordSalt = saltKey,
						customer.DeviceNo,
						customer.DeviceToken,
						customer.Imei1,
						customer.Imei2,
						customer.LatPos,
						customer.LongPos,
						customer.DeviceType,
						Otp = otp
					}, commandType: CommandType.StoredProcedure).FirstOrDefault();
					try
					{
						int userid = Convert.ToInt32(result);
						customerSignUpResponse.UserID = userid;
					}
					catch (Exception ex)
					{
						if (result == "Email is already registered with us.")
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EmailAlreadyRegistered", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.Ambiguous;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
						else if (result == "Email is already registered with us!!Please login with same email.")
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EmailAlreadyRegistered", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.Ambiguous;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
						else if (result == "Invalid Authentication key")
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
						else if (result == "Phone number is already registered with us.")
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.MobileNumberAlreadyRegistered", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.Ambiguous;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
					}
				}

				customerSignUpResponse.FirstName = customer.FirstName;
				customerSignUpResponse.LastName = customer.LastName;
				customerSignUpResponse.UserStatus = customerObj.Active == true ? "Active" : "InActive";
				customerSignUpResponse.DeviceToken = customer.DeviceToken;
				customerSignUpResponse.OTP = otp;
				customerSignUpResponse.IsFCMToken = (customer.DeviceToken != "" && customer.DeviceToken != null) ? true : false;

				var store = dbcontext.Store.AsEnumerable().Where(x => x.Id == customer.StoreId).FirstOrDefault();
				//var messageTemplate = dbcontext.MessageTemplate.AsEnumerable().Where(x => x.Name.ToString().Contains("CustomerApp.Email.WelcomeMessage")).FirstOrDefault();
				var messageTemplate = dbcontext.MessageTemplate.Where(x => x.Name.Contains("CustomerApp.Email.WelcomeMessage")
													&& (dbcontext.StoreMapping.Any(y => y.StoreId == store.Id && y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
													).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();
				if (messageTemplate != null)
				{
					//welcome message for mobile app as SMS
					string body = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Body", languageId, messageTemplate.Body, dbcontext);
					string subject = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Subject", languageId, messageTemplate.Subject, dbcontext);
					subject = subject.Replace("%Store.Name%", store.Name);
					body = body.Replace("%Store.Name%", store.Name);
					body = body.Replace("%Store.Email%", "");
					body = body.Replace("%Store.URL%", store.Url);
					body = body.Replace("%Customer.OTP%", customerSignUpResponse.OTP);
					body = body.Replace("%Customer.FullName%", customer.FirstName + " " + customer.LastName);
					var emailAccount = (from b in dbcontext.EmailAccount
										where b.StoreId == store.Id
										select new
										{
											b
										}).FirstOrDefault()?.b;
					if (emailAccount != null)
						Helper.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, customer.Email, null);

				}

				customer.MobileNo = customer.MobileNo.Replace(" ", "");
				customer.MobileNo = "+" + customer.MobileNo;
				customerSignUpResponse.Email = customer.Email;
				customerSignUpResponse.MobileNo = customer.MobileNo;

				//var welcomeMessage = dbcontext.MessageTemplate.AsEnumerable().Where(x => x.Name.ToString().Contains("CustomerApp.SMS.WelcomeMessage")).FirstOrDefault();
				var welcomeMessage = dbcontext.MessageTemplate.Where(x => x.Name.Contains("CustomerApp.SMS.WelcomeMessage")
													&& (dbcontext.StoreMapping.Any(y => y.StoreId == store.Id && y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
													).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();
				if (welcomeMessage != null)
				{
					string bodyForSMS = LanguageHelper.GetLocalizedValueMessageTemplate(welcomeMessage.Id, "Body", languageId, welcomeMessage.Body, dbcontext);
					bodyForSMS = bodyForSMS.Replace("%Store.Name%", store.Name);
					bodyForSMS = bodyForSMS.Replace("%Store.Email%", "");
					bodyForSMS = bodyForSMS.Replace("%Store.URL%", store.Url);
					bodyForSMS = bodyForSMS.Replace("%Customer.OTP%", customerSignUpResponse.OTP);
					bodyForSMS = bodyForSMS.Replace("%Customer.FullName%", customer.FirstName + " " + customer.LastName);

					//bodyForSMS=HttpUtility.HtmlEncode(bodyForSMS);
					var plainText = Helper.HtmlToPlainText(bodyForSMS);
					var isSMSSend = await Helper.SendMessage(customer.MobileNo, plainText, dbcontext);
				}
				customerAPIResponses.ErrorMessageTitle = "Success!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerRegisteredSuccessfully", languageId, dbcontext);
				customerAPIResponses.Status = true;
				customerAPIResponses.StatusCode = 200;
				customerAPIResponses.ResponseObj = customerSignUpResponse;
				return customerAPIResponses;

			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				if (customerObj != null)
				{
					var customertoDelet = dbcontext.Customer.AsEnumerable().Where(x => x.Id == customerObj.Id).FirstOrDefault();
					dbcontext.Entry(customertoDelet).State = EntityState.Deleted;
					dbcontext.SaveChanges();
					var genericattributes = dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == customerObj.Id).ToList();
					if (genericattributes.Any())
					{
						foreach (var item in genericattributes)
						{
							dbcontext.Entry(item).State = EntityState.Deleted;
							dbcontext.SaveChanges();
						}
					}
				}

				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.InternalServerError", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		/// <summary>
		///    Update Customer
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/UpdateCustomer")]
		[HttpPost]
		public async Task<CustomerAPIResponses> UpdateCustomer(ViewModels.CustomerModel customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 204;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.Id == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 204;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 204;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{

				if (!string.IsNullOrEmpty(customer.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == customer.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (customer.Id != 0)
				{
					var CustomerExist = dbcontext.Customer.AsEnumerable().Where(x => x.Id == customer.Id).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var customerProfileTOUpdate = dbcontext.GenericAttribute.AsEnumerable().Where(c => c.EntityId == customer.Id && c.StoreId == customer.StoreId).ToList();
				bool LastName = false, FirstName = false;
				bool ProfilePic = false;
				if (!string.IsNullOrEmpty(customer.ProfilePic))
				{
					var url = dbcontext.Setting.AsEnumerable().Where(x => x.Name.ToLower().Equals("setting.customer.notificationurl")).FirstOrDefault().Value;
					//String path = projectRootPath + "/CustomerProfileImages"; //Path
					//Check if directory exist
					// if (!System.IO.Directory.Exists(path))
					//{
					//  System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
					//}

					string imageName = customer.Id + "_" + DateTime.Now.ToString("dd_MM_yyyy") + "_" + DateTime.Now.ToString("HH_mm") + ".jpg";

					//set the image path
					//
					//string imgPath = Path.Combine(path, imageName);

					//string imgPath = string.Empty;
					//foreach (string s in imgPath1.Replace("http://", "$#$").Split('/').Distinct())
					//{
					//    imgPath += s;
					//}
					//imgPath = imgPath.Replace("$#$", "http://");

					byte[] imageBytes = Convert.FromBase64String(customer.ProfilePic);
					String strUrl = url;
					//System.IO.File.WriteAllBytes(imgPath, imageBytes);

					var data = await Helper.UploadObject(imageBytes, "Images/Jpeg", imageName, dbcontext);
					customer.ProfilePic = "https://nhkprofileimages.s3.ap-south-1.amazonaws.com/" + data.FileName;

				}
				if (customerProfileTOUpdate.Any())
				{
					foreach (var item in customerProfileTOUpdate)
					{
						//if (item.Key == "Phone")
						//{
						//   item.Value= customer.MobileNo;
						//}
						if (item.Key == "LastName" && !string.IsNullOrEmpty(customer.LastName))
						{
							LastName = true;
							item.Value = customer.LastName;
						}
						if (item.Key == "FirstName" && !string.IsNullOrEmpty(customer.FirstName))
						{
							FirstName = true;
							item.Value = customer.FirstName;
						}
						if (item.Key == "ProfilePic" && !string.IsNullOrEmpty(customer.ProfilePic))
						{
							ProfilePic = true;
							item.Value = customer.ProfilePic;
						}
						//if (item.Key == "City" && customer.City > 0)
						//{
						//    City = true;
						//    item.Value = customer.City.ToString();
						//}
						//if (item.Key == "Gender" && customer.Gender > 0)
						//{
						//    Gender = true;
						//    item.Value = customer.Gender.ToString();
						//}
						dbcontext.Entry(item).State = EntityState.Modified;

					}

					dbcontext.SaveChanges();
				}
				if (!LastName && !string.IsNullOrEmpty(customer.LastName))
				{
					///Generic Attribute to save Gender
					GenericAttribute genericAttributeGender = new GenericAttribute();
					genericAttributeGender.Key = "LastName";
					genericAttributeGender.EntityId = customer.Id;
					genericAttributeGender.Value = customer.LastName;
					genericAttributeGender.KeyGroup = "Customer";
					genericAttributeGender.StoreId = customer.StoreId;
					dbcontext.GenericAttribute.Add(genericAttributeGender);
				}
				if (!FirstName && !string.IsNullOrEmpty(customer.FirstName))
				{
					///Generic Attribute to save Gender
					GenericAttribute genericAttributeGender = new GenericAttribute();
					genericAttributeGender.Key = "FirstName";
					genericAttributeGender.EntityId = customer.Id;
					genericAttributeGender.Value = customer.FirstName;
					genericAttributeGender.KeyGroup = "Customer";
					genericAttributeGender.StoreId = customer.StoreId;
					dbcontext.GenericAttribute.Add(genericAttributeGender);
				}
				if (!ProfilePic && !string.IsNullOrEmpty(customer.ProfilePic))
				{
					///Generic Attribute to save Gender
					GenericAttribute genericAttributeGender = new GenericAttribute();
					genericAttributeGender.Key = "ProfilePic";
					genericAttributeGender.EntityId = customer.Id;
					genericAttributeGender.Value = customer.ProfilePic;
					genericAttributeGender.KeyGroup = "Customer";
					genericAttributeGender.StoreId = customer.StoreId;
					dbcontext.GenericAttribute.Add(genericAttributeGender);
				}
				dbcontext.SaveChanges();
				customerAPIResponses.ErrorMessageTitle = "Success!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerUpdatedSuccessfully", languageId, dbcontext);
				customerAPIResponses.Status = true;
				customerAPIResponses.StatusCode = 200;
				customerAPIResponses.ResponseObj = customer;
				return customerAPIResponses;

			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}
		/// <summary>
		/// Customer Details By Customer Id
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>forget      
		[Route("~/api/v1/CustomerDetails")]
		[HttpPost]
		public CustomerAPIResponses CustomerDetails(CustomerDetailsModel detailsModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(detailsModel.StoreId, detailsModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (detailsModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(detailsModel.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 204;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(detailsModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(detailsModel.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				var connectionString = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("ConnectionStrings").GetSection("ecuadordatabase").Value;
				using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
				{
					string result = db.Query<string>("GetCustomerDetailsByID", new
					{
						detailsModel.ApiKey,
						detailsModel.CustomerId,
						detailsModel.StoreId,
					}, commandType: CommandType.StoredProcedure).FirstOrDefault();
					try
					{
						ViewModels.CustomerDetails customer = Newtonsoft.Json.JsonConvert.DeserializeObject<ViewModels.CustomerDetails>(result);
						customerAPIResponses.ErrorMessageTitle = "Success!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(detailsModel.StoreId, "API.ErrorMesaage.CustomerFetchedSuccessfully", languageId, dbcontext);
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = 200;
						customerAPIResponses.ResponseObj = customer;
						return customerAPIResponses;
					}
					catch (Exception ex)
					{
						if (result == "Customer Not Found")
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(detailsModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = 204;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
						else if (result == "Store Not Found")
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(detailsModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.Ambiguous;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
						else if (result == "Invalid Authentication key")
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(detailsModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
						else
						{
							Helper.SentryLogs(ex);
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = ex.Message;
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = 400;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
					}
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		///<summary>
		/// Customer Login
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/Login")]
		[HttpPost]
		public CustomerAPIResponses Login(CustomerLogin customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 204;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.DeviceToken))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.DeviceTokenMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{

				if (!string.IsNullOrEmpty(customer.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == customer.ApiKey).Any();
					if (!keyExist)
					{

						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				bool loginFlag = false;
				string passwordFormat = PasswordFormat;
				string saltKey = Helper.CreateSaltKey(5);
				string Password = Helper.CreatePasswordHash(customer.Pswd, saltKey, passwordFormat);
				if (!string.IsNullOrEmpty(customer.Email))
				{
					customer.Email = customer.Email.ToLower().ToString();
					var customerObj = dbcontext.Customer.Include("CustomerPassword").Where(x => x.Email == customer.Email && x.RegisteredInStoreId == customer.StoreId).FirstOrDefault();
					if (customerObj == null)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.InvalidUsername", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = 404;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
					else
					{
						//var customerObj = loginFlag; //dbcontext.Customer.Where(x => x.Email == customer.Email && x.RegisteredInStoreId == customer.StoreId).FirstOrDefault();
						var customerPassword = dbcontext.CustomerPassword.Where(x => x.CustomerId == customerObj.Id).OrderByDescending(x => x.Id).FirstOrDefault();

						//  var customerPassword = customerObj.CustomerPassword.Any() ? customerObj.CustomerPassword.First() : null;
						Password = Helper.CreatePasswordHash(customer.Pswd, customerPassword.PasswordSalt, passwordFormat);
						if (customerPassword.Password == Password)
						{
							CustomerSignUpResponseModel customerSignUpResponse = new CustomerSignUpResponseModel();

							var customerGenericAttributes = dbcontext.GenericAttribute.Where(x => x.EntityId == customerObj.Id && x.StoreId == customer.StoreId && (x.Key == "FirstName" || x.Key == "LastName" || x.Key == "Phone")).ToList();

							if (customerGenericAttributes.Count() > 0)
							{
								customerSignUpResponse.FirstName = customerGenericAttributes.Where(x => x.Key == "FirstName").Any() ?
									customerGenericAttributes.Where(x => x.Key == "FirstName").First().Value : "";
								customerSignUpResponse.LastName = customerGenericAttributes.Where(x => x.Key == "LastName").Any() ?
								   customerGenericAttributes.Where(x => x.Key == "LastName").First().Value : "";
								customerSignUpResponse.MobileNo = customerGenericAttributes.Where(x => x.Key == "Phone").Any() ?
								   customerGenericAttributes.Where(x => x.Key == "Phone").First().Value : "";
							}
							//customerSignUpResponse.FirstName = dbcontext.GenericAttribute.Where(x => x.EntityId == customerObj.Id && x.Key == "FirstName").Any() ?
							//    dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == customerObj.Id && x.Key == "FirstName").First().Value : "";
							//customerSignUpResponse.LastName = dbcontext.GenericAttribute.Where(x => x.EntityId == customerObj.Id && x.Key.Contains("LastName")).Any() ?
							//    dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == customerObj.Id && x.Key.Contains("LastName")).First().Value : "";
							//customerSignUpResponse.MobileNo = dbcontext.GenericAttribute.Where(x => x.EntityId == customerObj.Id && x.Key.Contains("Phone")).Any() ?
							//    dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == customerObj.Id && x.Key.Contains("Phone")).First().Value : "";


							customerSignUpResponse.UserID = customerObj.Id;
							customerSignUpResponse.UserStatus = customerObj.Active == true ? "Active" : "InActive";
							customerSignUpResponse.DeviceToken = customer.DeviceToken;
							if (customerSignUpResponse.UserStatus.Contains("InActive"))
							{
								var store = dbcontext.Store.AsEnumerable().Where(x => x.Id == customer.StoreId).FirstOrDefault();
								CustomerDetails customerDetail = new CustomerDetails();
								customerDetail.CrDt = DateTime.UtcNow;
								customerDetail.CustomerId = customerObj.Id;
								customerDetail.DeviceNo = customer.DeviceNo;
								customerDetail.DeviceToken = customer.DeviceToken;
								customerDetail.Imei1 = customer.Imei1;
								customerDetail.Imei2 = customer.Imei2;
								customerDetail.LatPos = customer.LatPos;
								customerDetail.LongPos = customer.LongPos;
								customerDetail.Otpfailed = 0;
								customerDetail.OtpresendCount = 0;
								int _min = 1000;
								int _max = 9999;
								Random _rdm = new Random();
								customerDetail.Otp = _rdm.Next(_min, _max).ToString();
								customerDetail.CrDt = DateTime.UtcNow;
								customerDetail.DeviceType = string.IsNullOrEmpty(customer.DeviceType) ? "A" : customer.DeviceType;
								dbcontext.CustomerDetails.Add(customerDetail);
								dbcontext.SaveChanges();

								var welcomeMessage = dbcontext.MessageTemplate.Where(x => x.Name.Contains("CustomerApp.SMS.WelcomeMessage")
													  && (dbcontext.StoreMapping.Any(y => y.StoreId == store.Id && y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
													  ).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();

								if (welcomeMessage != null)
								{
									string bodyForSMS = LanguageHelper.GetLocalizedValueMessageTemplate(welcomeMessage.Id, "Body", languageId, welcomeMessage.Body, dbcontext);
									bodyForSMS = bodyForSMS.Replace("%Store.Name%", store.Name);
									bodyForSMS = bodyForSMS.Replace("%Store.Email%", "");
									bodyForSMS = bodyForSMS.Replace("%Store.URL%", store.Url);
									bodyForSMS = bodyForSMS.Replace("%Customer.OTP%", customerDetail.Otp);
									bodyForSMS = bodyForSMS.Replace("%Customer.FullName%", customerSignUpResponse.FirstName + " " + customerSignUpResponse.LastName);
									customerSignUpResponse.MobileNo = customerSignUpResponse.MobileNo.Replace(" ", "");
									customerSignUpResponse.MobileNo = "+" + customerSignUpResponse.MobileNo;

									var plainText = Helper.HtmlToPlainText(bodyForSMS);
									var isSMSSend = Helper.SendMessage(customerSignUpResponse.MobileNo, plainText, dbcontext);
									//var messageTemplate = dbcontext.MessageTemplate.AsEnumerable().Where(x => x.Name.ToString().Contains("CustomerApp.Email.WelcomeMessage")).FirstOrDefault();
									var messageTemplate = dbcontext.MessageTemplate.Where(x => x.Name.Contains("CustomerApp.Email.WelcomeMessage")
													  && (dbcontext.StoreMapping.Any(y => y.StoreId == store.Id && y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
													  ).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();
									if (messageTemplate != null)
									{
										//welcome message for mobile app as SMS
										string body = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Body", languageId, messageTemplate.Body, dbcontext);
										string subject = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Subject", languageId, messageTemplate.Subject, dbcontext);
										customerSignUpResponse.Email = customer.Email;
										subject = subject.Replace("%Store.Name%", store.Name);
										body = body.Replace("%Store.Name%", store.Name);
										body = body.Replace("%Store.Email%", "");
										body = body.Replace("%Store.URL%", store.Url);
										body = body.Replace("%Customer.OTP%", customerDetail.Otp);
										body = body.Replace("%Customer.FullName%", customerSignUpResponse.FirstName + " " + customerSignUpResponse.LastName);
										var emailAccount = (from b in dbcontext.EmailAccount
															where b.StoreId == store.Id
															select new
															{
																b
															}).FirstOrDefault()?.b;
										if (emailAccount != null)
											Helper.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, customer.Email, null);
									}
								}
							}
							customerSignUpResponse.IsFCMToken = (customer.DeviceToken != "" || customer.DeviceToken != null) ? true : false;
							customerAPIResponses.Status = true;
							customerAPIResponses.StatusCode = 200;
							customerAPIResponses.ResponseObj = customerSignUpResponse;
							return customerAPIResponses;
						}
						else
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.PasswordIncorrect", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = 400;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;

						}
					}
				}
				else if (!string.IsNullOrEmpty(customer.Phone))
				{
					var checkPhoneNumberExistList = dbcontext.GenericAttribute.AsEnumerable().Where(x => x.Key == "Phone" && x.StoreId == customer.StoreId).ToList();
					if (checkPhoneNumberExistList.Any())
					{
						var record = checkPhoneNumberExistList.AsEnumerable().Where(y => y.Value.Replace(" ", "").Equals(customer.Phone.Replace(" ", "")));
						if (record.Any())
						{
							loginFlag = true;
							var customerExist = record.FirstOrDefault();
							var customerObj = dbcontext.Customer.Include("CustomerPassword").Where(y => y.Id == customerExist.EntityId).FirstOrDefault();
							var customerPassword = customerObj.CustomerPassword.OrderByDescending(x => x.Id).FirstOrDefault(); //dbcontext.CustomerPassword.AsEnumerable().Where(x => x.CustomerId == customerObj.Id).OrderByDescending(x => x.Id).FirstOrDefault();
							Password = Helper.CreatePasswordHash(customer.Pswd, customerPassword.PasswordSalt, passwordFormat);
							if (customerPassword.Password == Password)
							{

								CustomerSignUpResponseModel customerSignUpResponse = new CustomerSignUpResponseModel();
								var customerGenericAttributes = dbcontext.GenericAttribute.Where(x => x.EntityId == customerObj.Id && x.StoreId == customer.StoreId && (x.Key == "FirstName" || x.Key == "LastName" || x.Key == "Phone")).ToList();

								if (customerGenericAttributes.Count() > 0)
								{
									customerSignUpResponse.FirstName = customerGenericAttributes.Where(x => x.Key == "FirstName").Any() ?
										customerGenericAttributes.Where(x => x.Key == "FirstName").First().Value : "";
									customerSignUpResponse.LastName = customerGenericAttributes.Where(x => x.Key == "LastName").Any() ?
									   customerGenericAttributes.Where(x => x.Key == "LastName").First().Value : "";
									customerSignUpResponse.MobileNo = customerGenericAttributes.Where(x => x.Key == "Phone").Any() ?
									   customerGenericAttributes.Where(x => x.Key == "Phone").First().Value : "";
								}

								//customerSignUpResponse.FirstName = dbcontext.GenericAttribute.Where(x => x.EntityId == customerObj.Id && x.Key == "FirstName").Any() ?
								//    dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == customerObj.Id && x.Key == "FirstName").First().Value : "";
								//customerSignUpResponse.LastName = dbcontext.GenericAttribute.Where(x => x.EntityId == customerObj.Id && x.Key.Contains("LastName")).Any() ?
								//    dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == customerObj.Id && x.Key.Contains("LastName")).First().Value : "";
								//  customerSignUpResponse.MobileNo = dbcontext.GenericAttribute.Where(x => x.EntityId == customerObj.Id && x.Key.Contains("Phone")).Any() ?
								//dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == customerObj.Id && x.Key.Contains("Phone")).First().Value : "";

								customerSignUpResponse.UserID = customerObj.Id;
								customerSignUpResponse.UserStatus = customerObj.Active == true ? "Active" : "InActive";

								if (customerSignUpResponse.UserStatus.Contains("InActive"))
								{
									var store = dbcontext.Store.AsEnumerable().Where(x => x.Id == customer.StoreId).FirstOrDefault();

									CustomerDetails customerDetail = new CustomerDetails
									{
										CrDt = DateTime.UtcNow,
										CustomerId = customerObj.Id,
										DeviceNo = customer.DeviceNo,
										DeviceToken = customer.DeviceToken,
										Imei1 = customer.Imei1,
										Imei2 = customer.Imei2,
										LatPos = customer.LatPos,
										LongPos = customer.LongPos
									};
									int _min = 1000;
									int _max = 9999;
									Random _rdm = new Random();
									customerDetail.Otp = _rdm.Next(_min, _max).ToString();
									customerDetail.Otpfailed = 0;
									customerDetail.OtpresendCount = 0;
									customerDetail.CrDt = DateTime.UtcNow;
									customerDetail.DeviceType = string.IsNullOrEmpty(customer.DeviceType) ? "A" : customer.DeviceType;
									dbcontext.CustomerDetails.Add(customerDetail);
									dbcontext.SaveChanges();

									customer.Email = dbcontext.Customer.AsEnumerable().Where(x => x.Id == customerSignUpResponse.UserID).FirstOrDefault().Email;
									//var welcomeMessage = dbcontext.MessageTemplate.AsEnumerable().Where(x => x.Name.ToString().Contains("CustomerApp.SMS.WelcomeMessage")).FirstOrDefault();
									var welcomeMessage = dbcontext.MessageTemplate.Where(x => x.Name.Contains("CustomerApp.SMS.WelcomeMessage")
													 && (dbcontext.StoreMapping.Any(y => y.StoreId == store.Id && y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
													 ).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();
									if (welcomeMessage != null)
									{
										string bodyForSMS = LanguageHelper.GetLocalizedValueMessageTemplate(welcomeMessage.Id, "Body", languageId, welcomeMessage.Body, dbcontext);
										bodyForSMS = bodyForSMS.Replace("%Store.Name%", store.Name);
										bodyForSMS = bodyForSMS.Replace("%Store.Email%", "");
										bodyForSMS = bodyForSMS.Replace("%Store.URL%", store.Url);
										bodyForSMS = bodyForSMS.Replace("%Customer.OTP%", customerDetail.Otp);
										bodyForSMS = bodyForSMS.Replace("%Customer.FullName%", customerSignUpResponse.FirstName + " " + customerSignUpResponse.LastName);
										customerSignUpResponse.MobileNo = customerSignUpResponse.MobileNo.Replace(" ", "");
										customerSignUpResponse.MobileNo = "+" + customerSignUpResponse.MobileNo;
										//bodyForSMS=HttpUtility.HtmlEncode(bodyForSMS);
										var plainText = Helper.HtmlToPlainText(bodyForSMS);
										var isSMSSend = Helper.SendMessage(customerSignUpResponse.MobileNo, plainText, dbcontext);
									}
									//var messageTemplate = dbcontext.MessageTemplate.AsEnumerable().Where(x => x.Name.ToString().Contains("CustomerApp.Email.WelcomeMessage")).FirstOrDefault();
									var messageTemplate = dbcontext.MessageTemplate.Where(x => x.Name.Contains("CustomerApp.Email.WelcomeMessage")
													  && (dbcontext.StoreMapping.Any(y => y.StoreId == store.Id && y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
													  ).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();
									if (messageTemplate != null)
									{
										//welcome message for mobile app as SMS
										string body = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Body", languageId, messageTemplate.Body, dbcontext);
										string subject = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Subject", languageId, messageTemplate.Subject, dbcontext);

										customerSignUpResponse.Email = customer.Email;
										subject = subject.Replace("%Store.Name%", store.Name);
										body = body.Replace("%Store.Name%", store.Name);
										body = body.Replace("%Store.Email%", "");
										body = body.Replace("%Store.URL%", store.Url);
										body = body.Replace("%Customer.OTP%", customerDetail.Otp);
										body = body.Replace("%Customer.FullName%", customerSignUpResponse.FirstName + " " + customerSignUpResponse.LastName);
										var emailAccount = (from b in dbcontext.EmailAccount
															where b.StoreId == store.Id
															select new
															{
																b
															}).FirstOrDefault()?.b;
										if (emailAccount != null)
											Helper.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, customer.Email, null);

									}
								}
								customerSignUpResponse.DeviceToken = customer.DeviceToken;
								customerSignUpResponse.IsFCMToken = (customer.DeviceToken != "" && customer.DeviceToken != null) ? true : false;
								customerSignUpResponse.Gender = Enum.GetValues(typeof(GenderEnum)).Cast<GenderEnum>().Where(x => (int)x == 1).Select(x => x.ToString()).ToList()[0];
								customerAPIResponses.Status = true;
								customerAPIResponses.StatusCode = 200;

								customerAPIResponses.ResponseObj = customerSignUpResponse;
								return customerAPIResponses;
							}
							else
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.PasswordIncorrect", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = 400;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
						else
						{
							loginFlag = false;
						}
						if (!loginFlag)
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.InvalidUsername", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = 404;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
					}
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EnterMobileOrEmail", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 204;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		/// <summary>
		/// Customer Reset Password
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/ResetPassword")]
		[HttpPost]
		public CustomerAPIResponses ResetPassword(CustomerResetPassword customerResetPassword)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customerResetPassword.StoreId, customerResetPassword.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customerResetPassword == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 204;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customerResetPassword.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 204;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			if (string.IsNullOrEmpty(customerResetPassword.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customerResetPassword.OldPassword) && !customerResetPassword.IsForgetPassword)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.OldPasswordMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customerResetPassword.NewPassword))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.NewPasswordMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (!customerResetPassword.IsForgetPassword && customerResetPassword.OldPassword.Equals(customerResetPassword.NewPassword))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.NewPasswordNotSameAsOldPassword", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(customerResetPassword.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == customerResetPassword.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (customerResetPassword.CustomerId != 0)
				{
					var CustomerExist = dbcontext.Customer.AsEnumerable().Where(x => x.Id == customerResetPassword.CustomerId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				string passwordFormat = PasswordFormat;
				string saltKey = Helper.CreateSaltKey(5);
				string Password = string.Empty;
				var customerPassword = dbcontext.CustomerPassword.AsEnumerable().Where(x => x.CustomerId == customerResetPassword.CustomerId).OrderByDescending(x => x.Id).FirstOrDefault();
				if (customerPassword != null)
				{
					if (!customerResetPassword.IsForgetPassword)
					{
						string OldPassword = Helper.CreatePasswordHash(customerResetPassword.OldPassword, customerPassword.PasswordSalt, passwordFormat);
						if (!OldPassword.Equals(customerPassword.Password))
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.OldPasswordMismatch", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = 400;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
					}
					Password = Helper.CreatePasswordHash(customerResetPassword.NewPassword, saltKey, passwordFormat);
					CustomerPassword customerPasswordtoupdate = new CustomerPassword();
					customerPasswordtoupdate.CustomerId = customerResetPassword.CustomerId;
					customerPasswordtoupdate.CreatedOnUtc = DateTime.UtcNow;
					customerPasswordtoupdate.Password = Password;
					customerPasswordtoupdate.PasswordFormatId = 1;
					customerPasswordtoupdate.PasswordSalt = saltKey;
					dbcontext.CustomerPassword.Add(customerPasswordtoupdate);
					//saltKey = Helper.CreateSaltKey(5);
					//Password = Helper.CreatePasswordHash(customerResetPassword.NewPassword, saltKey, passwordFormat);
					//customerPassword.PasswordSalt = saltKey;
					//customerPassword.Password = Password;
					//dbcontext.Entry(customerPassword).State = System.Data.Entity.EntityState.Modified;
					dbcontext.SaveChanges();
					customerAPIResponses.ErrorMessageTitle = "Success!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.PasswordChangedSuccessfully", languageId, dbcontext);
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = 200;
					customerAPIResponses.ResponseObj = customerResetPassword.CustomerId;
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 400;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}


				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}


		/// <summary>
		/// Create New Reset Password
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/CreatePassword")]
		[HttpPost]
		public CustomerAPIResponses CreatePassword(CustomerCreateNewPassword customerResetPassword)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customerResetPassword.StoreId, customerResetPassword.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customerResetPassword == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 204;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customerResetPassword.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 204;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customerResetPassword.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 204;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customerResetPassword.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customerResetPassword.OTP))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.OtpRequired", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(customerResetPassword.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == customerResetPassword.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (!string.IsNullOrEmpty(customerResetPassword.OTP))
				{
					var OTPVerifiedMOdel = dbcontext.CustomerDetails.AsEnumerable().Where(x => x.CustomerId == customerResetPassword.CustomerId && x.Otp.Equals(customerResetPassword.OTP)).Any();
					if (!OTPVerifiedMOdel)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.OtpMismatch", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}


				}
				string passwordFormat = PasswordFormat;
				string saltKey = Helper.CreateSaltKey(5);
				string Password = string.Empty;
				var customerPassword = dbcontext.CustomerPassword.AsEnumerable().Where(x => x.CustomerId == customerResetPassword.CustomerId).OrderByDescending(x => x.Id).FirstOrDefault();
				if (customerPassword != null)
				{
					Password = Helper.CreatePasswordHash(customerResetPassword.NewPassword, saltKey, passwordFormat);
					CustomerPassword customerPasswordtoupdate = new CustomerPassword();
					customerPasswordtoupdate.CustomerId = customerResetPassword.CustomerId;
					customerPasswordtoupdate.CreatedOnUtc = DateTime.UtcNow;
					customerPasswordtoupdate.Password = Password;
					customerPasswordtoupdate.PasswordFormatId = 1;
					customerPasswordtoupdate.PasswordSalt = saltKey;
					dbcontext.CustomerPassword.Add(customerPasswordtoupdate);
					//saltKey = Helper.CreateSaltKey(5);
					//Password = Helper.CreatePasswordHash(customerResetPassword.NewPassword, saltKey, passwordFormat);
					//customerPassword.PasswordSalt = saltKey;
					//customerPassword.Password = Password;
					//dbcontext.Entry(customerPassword).State = System.Data.Entity.EntityState.Modified;
					dbcontext.SaveChanges();
					customerAPIResponses.ErrorMessageTitle = "Success!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.PasswordChangedSuccessfully", languageId, dbcontext);
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = 200;
					customerAPIResponses.ResponseObj = customerPasswordtoupdate.CustomerId;
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerResetPassword.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 400;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}


				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}
		///<summary>
		/// Forget Password
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/ForgetPassword")]
		[HttpPost]
		public async Task<CustomerAPIResponses> ForgetPasswordAsync(CustomerForgetPassword customer)
		{

			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Errror!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 204;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			try
			{

				if (!string.IsNullOrEmpty(customer.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == customer.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (!string.IsNullOrEmpty(customer.Email) && customer.Email != "string")
				{
					customer.Email = customer.Email.ToLower().ToString();
					var customerObj = dbcontext.Customer.AsEnumerable().Where(x => (x.Email ?? "").ToLower() == customer.Email && x.RegisteredInStoreId == customer.StoreId).FirstOrDefault();
					var isCustomerExist = customerObj != null;
					if (isCustomerExist)
					{
						var store = dbcontext.Store.AsEnumerable().Where(x => x.Id == customer.StoreId).FirstOrDefault();
						var CustomerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == customerObj.Id).ToList();
						CustomerDetails customerDetail = new CustomerDetails();
						customerDetail.CrDt = DateTime.UtcNow;
						customerDetail.CustomerId = customerObj.Id;
						customerDetail.DeviceNo = customer.DeviceNo;
						customerDetail.DeviceToken = customer.DeviceToken;
						customerDetail.Imei1 = customer.Imei1;
						customerDetail.Imei2 = customer.Imei2;
						customerDetail.LatPos = customer.LatPos;
						customerDetail.LongPos = customer.LongPos;
						int _min = 1000;
						int _max = 9999;
						Random _rdm = new Random();
						customerDetail.Otp = _rdm.Next(_min, _max).ToString();
						customerDetail.Otpfailed = 0;
						customerDetail.OtpresendCount = 0;
						customerDetail.CrDt = DateTime.UtcNow;
						customerDetail.DeviceType = string.IsNullOrEmpty(customer.DeviceType) ? "A" : customer.DeviceType;
						dbcontext.CustomerDetails.Add(customerDetail);
						dbcontext.SaveChanges();
						//var messageTemplate = dbcontext.MessageTemplate.AsEnumerable().Where(x => x.Name.ToString().Contains("CustomerApp.Email.ForgetPassword")).FirstOrDefault();
						var messageTemplate = dbcontext.MessageTemplate.Where(x => x.Name.Contains("CustomerApp.Email.ForgetPassword")
													&& (dbcontext.StoreMapping.Any(y => y.StoreId == store.Id && y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
													).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();
						var customerName = (CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).FirstOrDefault()?.Value??"") + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).FirstOrDefault()?.Value ?? "");
						if (messageTemplate != null)
						{
							//welcome message for mobile app as SMS
							string body = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Body", languageId, messageTemplate.Body, dbcontext);
							string subject = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Subject", languageId, messageTemplate.Subject, dbcontext);

							subject = subject.Replace("%Store.Name%", store.Name);
							body = body.Replace("%Store.Name%", store.Name);
							body = body.Replace("%Store.Email%", "");
							body = body.Replace("%Store.URL%", store.Url);
							body = body.Replace("%Customer.OTP%", customerDetail.Otp);
							body = body.Replace("%Customer.FullName%", customerName);
							var emailAccount = (from b in dbcontext.EmailAccount
												where b.StoreId == store.Id
												select new
												{
													b
												}).FirstOrDefault()?.b;
							if (emailAccount != null)
							{
								try
								{
									Helper.InsertQueuedEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, customer.Email, customerName, messageTemplate, dbcontext);
								}
								catch (Exception ex) { }
							}

						}
						CustomerSignUpResponseModel customerSignUpResponse = new CustomerSignUpResponseModel();
						customerSignUpResponse.UserID = customerObj.Id;
						customerSignUpResponse.OTP = customerDetail.Otp;
						customerSignUpResponse.FirstName = CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).Any() ?
							   CustomerAddressDetails.AsEnumerable().Where(x => x.Key.Contains("FirstName")).FirstOrDefault().Value : "";
						customerSignUpResponse.LastName = CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ?
							CustomerAddressDetails.AsEnumerable().Where(x => x.Key.Contains("LastName")).FirstOrDefault().Value : "";
						customerSignUpResponse.MobileNo = CustomerAddressDetails.Where(x => x.Key.Contains("Phone")).Any() ?
							CustomerAddressDetails.AsEnumerable().Where(x => x.Key.Contains("Phone")).FirstOrDefault().Value : "";
						customerSignUpResponse.UserID = customerDetail.CustomerId;
						customerSignUpResponse.Email = customer.Email;
						customerSignUpResponse.UserStatus = customerObj.Active == true ? "Active" : "InActive";
						customerSignUpResponse.DeviceToken = customer.DeviceToken;
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = customerSignUpResponse;

						return customerAPIResponses;
					}
					else
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EnterValidEmailOrPhoneNumber", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = 204;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				else if (!string.IsNullOrEmpty(customer.Phone))
				{
					var checkPhoneNumberExistList = dbcontext.GenericAttribute.Where(x => x.Key == "Phone" && x.StoreId == customer.StoreId).ToList();
					if (checkPhoneNumberExistList.Any())
					{
						var matchingvalues = checkPhoneNumberExistList.Where(x => x.Value.Replace(" ", "").Equals(customer.Phone.Replace(" ", "")) && x.StoreId == customer.StoreId).Any();
						//customer.Phone = customer.Phone.Replace(" ", "");
						//checkPhoneNumberExistList.Contains(customer.Phone.ToString().ToList());

						if (matchingvalues)
						{
							var store = dbcontext.Store.AsEnumerable().Where(x => x.Id == customer.StoreId).FirstOrDefault();
							var Customer = checkPhoneNumberExistList.AsEnumerable().Where(x => x.Value.Replace(" ", "").Equals(customer.Phone.Replace(" ", "")) && x.StoreId == customer.StoreId).FirstOrDefault();
							var customerInfo = (from a in dbcontext.Customer.AsEnumerable().Where(x => x.Id == Customer.EntityId)
												select new
												{
													a.Id,
													a.Email,
													a.Active
												}).FirstOrDefault();
							var CustomerAddressDetails = dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == customerInfo.Id).ToList();
							CustomerDetails customerDetail = new CustomerDetails();
							customerDetail.CrDt = DateTime.UtcNow;
							customerDetail.CustomerId = customerInfo.Id;
							customerDetail.DeviceNo = customer.DeviceNo;
							customerDetail.DeviceToken = customer.DeviceToken;
							customerDetail.Imei1 = customer.Imei1;
							customerDetail.Imei2 = customer.Imei2;
							customerDetail.LatPos = customer.LatPos;
							customerDetail.LongPos = customer.LongPos;
							int _min = 1000;
							int _max = 9999;
							Random _rdm = new Random();
							customerDetail.Otp = _rdm.Next(_min, _max).ToString();
							customerDetail.Otpfailed = 0;
							customerDetail.OtpresendCount = 0;
							customerDetail.CrDt = DateTime.UtcNow;
							customerDetail.DeviceType = string.IsNullOrEmpty(customer.DeviceType) ? "A" : customer.DeviceType;

							dbcontext.CustomerDetails.Add(customerDetail);
							dbcontext.SaveChanges();

							//Helper.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, customerInfo.Email, null);
							CustomerSignUpResponseModel customerSignUpResponse = new CustomerSignUpResponseModel();
							customerSignUpResponse.FirstName = CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).Any() ?
							CustomerAddressDetails.AsEnumerable().Where(x => x.Key.Contains("FirstName")).FirstOrDefault().Value : "";
							customerSignUpResponse.LastName = CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ?
								CustomerAddressDetails.AsEnumerable().Where(x => x.Key.Contains("LastName")).FirstOrDefault().Value : "";
							customerSignUpResponse.MobileNo = CustomerAddressDetails.Where(x => x.Key.Contains("Phone")).Any() ?
								CustomerAddressDetails.AsEnumerable().Where(x => x.Key.Contains("Phone")).FirstOrDefault().Value : "";
							customerSignUpResponse.UserID = customerDetail.CustomerId;
							customerSignUpResponse.UserStatus = customerInfo.Active == true ? "Active" : "InActive";
							customerSignUpResponse.DeviceToken = customer.DeviceToken;
							customerSignUpResponse.Email = customer.Email;
							customerSignUpResponse.OTP = customerDetail.Otp;
							customerAPIResponses.Status = true;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
							customerAPIResponses.ResponseObj = customerSignUpResponse;
							//var welcomeMessage = dbcontext.MessageTemplate.AsEnumerable().Where(x => x.Name.ToString().Contains("CustomerApp.SMS.ForgetPassword")).FirstOrDefault();
							var welcomeMessage = dbcontext.MessageTemplate.Where(x => x.Name.Contains("CustomerApp.SMS.ForgetPassword")
													&& (dbcontext.StoreMapping.Any(y => y.StoreId == store.Id && y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
													).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();
							var customerName = (CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).FirstOrDefault()?.Value??"") + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).FirstOrDefault()?.Value ?? "");
							if (welcomeMessage != null)
							{
								string bodyForSMS = LanguageHelper.GetLocalizedValueMessageTemplate(welcomeMessage.Id, "Body", languageId, welcomeMessage.Body, dbcontext);
								bodyForSMS = bodyForSMS.Replace("%Store.Name%", store.Name);
								bodyForSMS = bodyForSMS.Replace("%Store.Email%", "");
								bodyForSMS = bodyForSMS.Replace("%Store.URL%", store.Url);
								bodyForSMS = bodyForSMS.Replace("%Customer.OTP%", customerDetail.Otp);
								bodyForSMS = bodyForSMS.Replace("%Customer.FullName%", customerName);
								string Mobile = CustomerAddressDetails.AsEnumerable().Where(x => x.Key.Contains("Phone")).FirstOrDefault().Value.Replace(" ", "");
								Mobile = Mobile.Replace(" ", "");
								Mobile = "+" + Mobile;
								//bodyForSMS=HttpUtility.HtmlEncode(bodyForSMS);
								var plainText = Helper.HtmlToPlainText(bodyForSMS);
								try
								{
									var isSMSSend = await Helper.SendMessage(Mobile, plainText, dbcontext);
								}
								catch (Exception ex) { }
							}
							return customerAPIResponses;
						}
						else
						{

							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EnterValidEmailOrPhoneNumber", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = 204;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}

					}
					else
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EnterValidEmailOrPhoneNumber", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = 204;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EnterValidEmailOrPhoneNumber", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 204;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		/// <summary>
		///    Customer Registation
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/GetCustomerAdditionalInfo")]
		[HttpPost]
		public CustomerAPIResponses GetCustomerAdditionalInfo([FromBody] ViewModels.CustomerDetailsModel customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(customer.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == customer.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (customer.CustomerId != 0)
				{
					var CustomerExist = dbcontext.Customer.AsEnumerable().Where(x => x.Id == customer.CustomerId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (customer.StoreId != 0)
				{
					var CustomerExist = dbcontext.Store.AsEnumerable().Where(x => x.Id == customer.StoreId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var customerAdditionalInfos = (from a in dbcontext.CustomerDashboardMenu.AsEnumerable()
											   where a.StoreId == customer.StoreId
											   select new
											   {
												   Id = a.MenuId,
												   Name = a.Name
											   }).ToList();


				if (customerAdditionalInfos.Count > 0)
				{
					customerAPIResponses.ErrorMessageTitle = "Success!!";
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = 200;
					customerAPIResponses.ResponseObj = customerAdditionalInfos;
					return customerAPIResponses;
				}
				else
				{
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.Status = false;
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.NoPermissionAssignedyet", languageId, dbcontext);
					return customerAPIResponses;
				}

			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}



		/// <summary>
		///    SaveCustomerAddress
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/SaveCustomerAddress")]
		[HttpPost]
		public CustomerAPIResponses SaveCustomerAddressV2([FromBody] ViewModels.CustomerAddressModelV2 customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.CustId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}


			try
			{
				var connectionString = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("ConnectionStrings").GetSection("ecuadordatabase").Value;
				using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
				{
					string result = string.Empty;
					if (!Helper.IsUserRegistered(customer.CustId, dbcontext))
					{
						result = db.Query<string>("SaveGuestUserAddress", new
						{
							FirstName = customer.Name,
							Email = customer.Email,
							PhoneNumber = customer.PhoneNumber,
							Landmark = customer.Label,
							AddressTypeId = customer.LabelId,
							customer.Address1,
							customer.Address2,
							customer.City,
							customer.ZipPostalCode,
							apiKey = customer.ApiKey,
							custId = customer.CustId,
							Latitude = Convert.ToDecimal(customer.LatPos),
							Longitude = Convert.ToDecimal(customer.LongPos),
							LandMarktxt = customer.Landmarktext,
							DoorNo = customer.FlatNo,
							customer.@SetAsDefault
						}, commandType: CommandType.StoredProcedure).FirstOrDefault();
					}
					else
					{
						result = db.Query<string>("SaveCustomerAddress", new
						{
							FirstName = customer.Name,
							Email = customer.Email,
							PhoneNumber = customer.PhoneNumber,
							Landmark = customer.Label,
							AddressTypeId = customer.LabelId,
							customer.Address1,
							customer.Address2,
							customer.City,
							customer.ZipPostalCode,
							apiKey = customer.ApiKey,
							custId = customer.CustId,
							Latitude = Convert.ToDecimal(customer.LatPos),
							Longitude = Convert.ToDecimal(customer.LongPos),
							LandMarktxt = customer.Landmarktext,
							DoorNo = customer.FlatNo,
							customer.@SetAsDefault
						}, commandType: CommandType.StoredProcedure).FirstOrDefault();
					}
					try
					{
						if (result.Length > 0)
						{
							if (result == "Invalid Authentication key")
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
							else if (result == "Customer Not Found")
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
							else if (result == "Name is empty")
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.NameEmpty", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
							else if (result == "Email is empty")
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EmailEmpty", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
							else if (result == "PhoneNumber is empty")
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.PhoneNumberEmpty", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
							else if (result == "All Good")
							{
								customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
								customerAPIResponses.Status = true;
								customerAPIResponses.ErrorMessageTitle = "Success!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerAddressAddedSuccessfully", languageId, dbcontext);
								return customerAPIResponses;
							}
						}
					}
					catch (Exception ex)
					{
						Helper.SentryLogs(ex);
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = ex.Message;
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = 400;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}


			customerAPIResponses.ErrorMessageTitle = "Error!!";
			customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ErrorOccuredPleasetryOther", languageId, dbcontext);
			customerAPIResponses.Status = false;
			customerAPIResponses.StatusCode = 400;
			customerAPIResponses.ResponseObj = null;
			return customerAPIResponses;
		}
		/// <summary>
		///    UpdateCustomerAddress
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/UpdateCustomerAddress")]
		[HttpPost]
		public CustomerAPIResponses UpdateCustomerAddressV2([FromBody] CustomerAddressModelV2 customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.CustId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.Id == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerAddressIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			try
			{
				if (!string.IsNullOrEmpty(customer.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == customer.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (customer.CustId != 0)
				{
					var CustomerExist = dbcontext.Customer.AsEnumerable().Where(x => x.Id == customer.CustId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var isRegisteredUser = Helper.IsUserRegistered(customer.CustId, dbcontext);
				if (!isRegisteredUser)
				{
					if (string.IsNullOrEmpty(customer.Name))
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.NameEmpty", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
					else if (string.IsNullOrEmpty(customer.Email))
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.EmailEmpty", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
					else if (string.IsNullOrEmpty(customer.PhoneNumber))
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.PhoneNumberEmpty", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var addrssToUpdate = dbcontext.Address.AsEnumerable().Where(x => x.Id == customer.Id).FirstOrDefault();
				if (addrssToUpdate != null)
				{

					addrssToUpdate.FirstName = customer.Name;
					addrssToUpdate.Email = customer.Email;
					addrssToUpdate.PhoneNumber = customer.PhoneNumber;
					addrssToUpdate.Landmark = customer.Label;
					addrssToUpdate.Address1 = customer.Address1;
					addrssToUpdate.Address2 = customer.Address2;
					addrssToUpdate.City = customer.City;
					addrssToUpdate.ZipPostalCode = customer.ZipPostalCode;
					addrssToUpdate.AddressTypeId = customer.LabelId;
					addrssToUpdate.Latitude = Convert.ToDecimal(customer.LatPos);
					addrssToUpdate.Longitude = Convert.ToDecimal(customer.LongPos);
					addrssToUpdate.County = customer.Landmarktext;
					addrssToUpdate.FlatNo = customer.FlatNo;


					dbcontext.Entry(addrssToUpdate).State = EntityState.Modified;
					dbcontext.SaveChanges();
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.Status = true;
					customerAPIResponses.ErrorMessageTitle = "Success!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerAddressUpdatedSuccessfully", languageId, dbcontext);
					return customerAPIResponses;
				}
				else
				{
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.Status = false;
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.NoCustomerAddressFound", languageId, dbcontext);
					return customerAPIResponses;
				}
				//customerSignUpResponse.Gender = Enum.GetValues(typeof(GenderEnum)).Cast<GenderEnum>().Where(x => (int)x == customer.Gender).Select(x=>x.ToString()).ToList()[0];
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}
		/// <summary>
		///    GetCustomerAddress
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/GetCustomerAddress")]
		[HttpPost]
		public CustomerAPIResponses GetCustomerAddressV2([FromBody] CustomerDetailsModel customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();

			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}


			//try
			//{
			//    //if (!string.IsNullOrEmpty(customer.ApiKey))
			//    //{
			//    //    var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == customer.ApiKey).Any();
			//    //    if (!keyExist)
			//    //    {
			//    //        customerAPIResponses.ErrorMessageTitle = "Error!!";
			//    //        customerAPIResponses.ErrorMessage = "Invalid Authentication key";
			//    //        customerAPIResponses.Status = false;
			//    //        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
			//    //        customerAPIResponses.ResponseObj = null;
			//    //        return customerAPIResponses;
			//    //    }
			//    //}
			//    //if (customer.CustomerId != 0)
			//    //{
			//    //    var CustomerExist = dbcontext.Customer.AsEnumerable().Where(x => x.Id == customer.CustomerId).Any();
			//    //    if (!CustomerExist)
			//    //    {
			//    //        customerAPIResponses.ErrorMessageTitle = "Error!!";
			//    //        customerAPIResponses.ErrorMessage = "Customer Not Found";
			//    //        customerAPIResponses.Status = false;
			//    //        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
			//    //        customerAPIResponses.ResponseObj = null;
			//    //        return customerAPIResponses;
			//    //    }
			//    //}
			var connectionString = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("ConnectionStrings").GetSection("ecuadordatabase").Value;

			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				string result = db.Query<string>("CustomerAddresses_Validation", new
				{
					customer.ApiKey,
					customer.CustomerId,
					customer.StoreId,
				}, commandType: CommandType.StoredProcedure).FirstOrDefault();
				try
				{
					if (result.Length > 0)
					{
						if (result == "Invalid Authentication key")
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
						else if (result == "Customer Not Found")
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
						else if (result == "Store Not Found")
						{
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
						else if (result == "All Good")
						{
							var result1 = db.Query<CustomerAddressViewModel>("GETCustomerAddresses", new
							{
								customer.CustomerId
							}, commandType: CommandType.StoredProcedure).ToList();
							try
							{
								if (result1.Count > 0)
								{
									customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
									customerAPIResponses.Status = true;
									customerAPIResponses.ErrorMessageTitle = "Success!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetCustomerAddress.AddressFound", languageId, dbcontext);
									customerAPIResponses.ResponseObj = result1;
									return customerAPIResponses;
								}
								else
								{
									customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
									customerAPIResponses.Status = false;
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetCustomerAddress.NoAddress", languageId, dbcontext);
									return customerAPIResponses;
								}
							}
							catch (Exception ex)
							{
								Helper.SentryLogs(ex);
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = ex.Message;
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = 400;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
						}
					}
				}
				catch (Exception ex)
				{
					Helper.SentryLogs(ex);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = ex.Message;
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 400;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}

			customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
			customerAPIResponses.Status = false;
			customerAPIResponses.ErrorMessageTitle = "Error!!";
			customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetCustomerAddress.NoAddress", languageId, dbcontext);
			return customerAPIResponses;

		}
		/// <summary>
		///    GetCustomerAddress
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/GetCustomerAddressById")]
		[HttpPost]
		public CustomerAPIResponses GetCustomerAddressByIdV2([FromBody] CustomerAddressDetailsModel customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.CustId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.AddressId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerAddressIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(customer.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == customer.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (customer.CustId != 0)
				{
					var CustomerExist = dbcontext.Customer.AsEnumerable().Where(x => x.Id == customer.CustId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				// string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString;
				List<int> customerAddressId = new List<int>();
				var customerAddress = dbcontext.CustomerAddresses.AsEnumerable().Where(x => x.CustomerId == customer.CustId).ToList();
				if (customerAddress.Any())
				{
					foreach (var item in customerAddress)
					{
						customerAddressId.Add(item.AddressId);
					}
				}
				//customerSignUpResponse.Gender = Enum.GetValues(typeof(GenderEnum)).Cast<GenderEnum>().Where(x => (int)x == customer.Gender).Select(x=>x.ToString()).ToList()[0];
				if (customerAddressId.Count > 0)
				{
					var customerAddressobj = (from address in dbcontext.Address.AsEnumerable().Where(x => customerAddressId.Contains(x.Id) && !x.IsDeleted)
											  select new
											  {
												  Id = address.Id,
												  Label = address.Landmark,
												  LabelId = address.AddressTypeId,
												  FirstName = address.FirstName,
												  LastName = address.LastName,
												  PhoneNumber = address.PhoneNumber,
												  AddressLine1 = address.Address1,
												  AddressLine2 = address.Address2,
												  ZipCode = address.ZipPostalCode,
												  City = address.City,
												  LatPos = address.Latitude,
												  LongPos = address.Longitude
											  }).ToList();
					if (customerAddressobj.Any())
					{
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.AddressFound", languageId, dbcontext);
						customerAPIResponses.ErrorMessageTitle = "Success!!";
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.Status = true;
						customerAPIResponses.ResponseObj = customerAddress;
						return customerAPIResponses;
					}
					else
					{
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.Status = true;
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.NoAddressFound", languageId, dbcontext);
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						return customerAPIResponses;
					}
				}
				else
				{
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.Status = true;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.NoAddressFound", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}
		/// <summary>
		///    Delete Customer Address
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/DeleteCustomerAddress")]
		[HttpPost]
		public CustomerAPIResponses DeleteCustomerAddress([FromBody] CustomerAddressDetailsModel customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.CustId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.AddressId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerAddressIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(customer.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == customer.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (customer.CustId != 0)
				{
					var CustomerExist = dbcontext.Customer.AsEnumerable().Where(x => x.Id == customer.CustId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				//customerSignUpResponse.Gender = Enum.GetValues(typeof(GenderEnum)).Cast<GenderEnum>().Where(x => (int)x == customer.Gender).Select(x=>x.ToString()).ToList()[0];

				var customerAddress = dbcontext.Address.AsEnumerable().Where(x => x.Id == customer.AddressId).FirstOrDefault();
				if (customerAddress != null)
				{

					customerAddress.IsDeleted = true;
					dbcontext.Entry(customerAddress).State = EntityState.Modified;
					dbcontext.SaveChanges();

					//delete Customer address mapping
					var customerAddressMappings = dbcontext.CustomerAddresses.AsEnumerable().Where(x => x.CustomerId == customer.CustId && x.AddressId == customer.AddressId).FirstOrDefault();
					if (customerAddressMappings != null)
					{
						dbcontext.Entry(customerAddressMappings).State = EntityState.Deleted;
						dbcontext.SaveChanges();
					}
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.Status = true;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.AdressDeletedSuccessfully", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Success!!";
					return customerAPIResponses;
				}
				else
				{
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.Status = false;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.NoAddressFound", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}
		/// <summary>
		/// Customer Details By Customer Id
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/GetCustomerAddressType")]
		[HttpPost]
		public CustomerAPIResponses GetCustomerAddressType(CountryModel detailsModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(detailsModel.StoreId, detailsModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();

			if (detailsModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(detailsModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(detailsModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(detailsModel.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (detailsModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(detailsModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(detailsModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == detailsModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(detailsModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				Models.Customer customer = new Models.Customer();
				var customerBasicDetails = Enum.GetValues(typeof(AddressTypeEnum)).Cast<AddressTypeEnum>().ToList();
				if (customerBasicDetails.Any())
				{
					var BasicDetails = (from a in customerBasicDetails.AsEnumerable()
										select new
										{
											Name = a.ToString(),
											Id = (int)a,
											ImageURL = ""
										}).ToList();
					customerAPIResponses.ErrorMessage = null;
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = 200;
					customerAPIResponses.ResponseObj = BasicDetails;
					return customerAPIResponses;
				}
				else
				{
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(detailsModel.StoreId, "API.ErrorMesaage.NoAddressTypeFound", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 204;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}
		/// <summary>
		///    Registaionverification
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/RegistrationVerification")]
		[HttpPost]
		public CustomerAPIResponses RegistaionverificationCustomer([FromBody] RegistationverificationCustomer RegistationverificationCustomer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(RegistationverificationCustomer.StoreId, RegistationverificationCustomer.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (RegistationverificationCustomer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(RegistationverificationCustomer.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (RegistationverificationCustomer.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(RegistationverificationCustomer.OTP))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.ErrorMesaage.OtpMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			try
			{
				if (!string.IsNullOrEmpty(RegistationverificationCustomer.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == RegistationverificationCustomer.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (!string.IsNullOrEmpty(RegistationverificationCustomer.OTP))
				{
					var OTPVerifiedMOdel = dbcontext.CustomerDetails.AsEnumerable().Where(x => x.CustomerId == RegistationverificationCustomer.CustomerId && x.Otp != null).OrderByDescending(x => x.Id).FirstOrDefault();
					if (OTPVerifiedMOdel != null)
					{
						var IsOTPVerified = OTPVerifiedMOdel.Otp == RegistationverificationCustomer.OTP;
						if (IsOTPVerified)
						{
							var customer = dbcontext.Customer.AsEnumerable().Where(x => x.Id == RegistationverificationCustomer.CustomerId).FirstOrDefault();
							customer.Active = true;
							customer.LastActivityDateUtc = DateTime.UtcNow;
							dbcontext.Entry(customer).State = EntityState.Modified;
							dbcontext.SaveChanges();
							customerAPIResponses.ErrorMessageTitle = "Success!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.ErrorMesaage.OtpVerifiedSuccessfully", languageId, dbcontext);
							customerAPIResponses.Status = true;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
							customerAPIResponses.ResponseObj = customer.Id;
						}
						else
						{

							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.ErrorMesaage.OtpMismatch", languageId, dbcontext);
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
							customerAPIResponses.ResponseObj = null;

						}
					}
					else
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.ErrorMesaage.ServerError", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
					}

				}
				return customerAPIResponses;


			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}
		[Route("~/api/v1/ResendOTP")]
		[HttpPost]
		public async Task<CustomerAPIResponses> ResendOTPAsync([FromBody] ResendOTPModel RegistationverificationCustomer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(RegistationverificationCustomer.StoreId, RegistationverificationCustomer.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (RegistationverificationCustomer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(RegistationverificationCustomer.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (RegistationverificationCustomer.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (RegistationverificationCustomer.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(RegistationverificationCustomer.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == RegistationverificationCustomer.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				CustomerDetails customerDetail = new CustomerDetails();
				customerDetail.CrDt = DateTime.UtcNow;
				customerDetail.CustomerId = RegistationverificationCustomer.CustomerId;
				customerDetail.DeviceNo = RegistationverificationCustomer.DeviceNo;
				customerDetail.DeviceToken = RegistationverificationCustomer.DeviceToken;
				customerDetail.Imei1 = RegistationverificationCustomer.Imei1;
				customerDetail.Imei2 = RegistationverificationCustomer.Imei2;
				customerDetail.LatPos = RegistationverificationCustomer.LatPos;
				customerDetail.LongPos = RegistationverificationCustomer.LongPos;
				int _min = 1000;
				int _max = 9999;
				Random _rdm = new Random();
				customerDetail.Otp = _rdm.Next(_min, _max).ToString();
				customerDetail.Otpfailed = 0;
				customerDetail.OtpresendCount = 0;
				customerDetail.CrDt = DateTime.UtcNow;
				customerDetail.DeviceType = string.IsNullOrEmpty(RegistationverificationCustomer.DeviceType) ? "A" : RegistationverificationCustomer.DeviceType;
				dbcontext.CustomerDetails.Add(customerDetail);
				dbcontext.SaveChanges();
				var response = new ResendOtpTimerSecond();


				var customer = dbcontext.Customer.AsEnumerable().Where(x => x.Id == RegistationverificationCustomer.CustomerId).FirstOrDefault();
				var minutes = dbcontext.Setting.Where(x => x.Name.Contains("adminarea.login.resendOtp") && x.StoreId == RegistationverificationCustomer.StoreId).FirstOrDefault()?.Value ?? "5";
				var genericAttributes = dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == RegistationverificationCustomer.CustomerId && x.StoreId == RegistationverificationCustomer.StoreId && x.KeyGroup == ("Customer")).ToList();
				var customerMobileNumber = genericAttributes.Where(x => x.Key == ("Phone")).FirstOrDefault();
				var firstName = genericAttributes.Where(x => x.Key == ("FirstName")).FirstOrDefault()?.Value ?? "";
				var lastName = genericAttributes.Where(x => x.Key == ("LastName")).FirstOrDefault()?.Value ?? "";

				if (customerMobileNumber != null)
				{
					response.CustomerID = RegistationverificationCustomer.CustomerId;
					response.Minutes = minutes;
					string MobileNo = customerMobileNumber.Value.Replace(" ", "");
					var connectionString = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("ConnectionStrings").GetSection("ecuadordatabase").Value;
					using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
					{
						string result = db.Query<string>("sp_User_SendEmail", new
						{
							customer.Email,
							RegisteredInStoreId = RegistationverificationCustomer.StoreId,
							Phone = MobileNo,
							LastName = lastName,
							FirstName = firstName,
							customerDetail.Otp,
							LanguageId = languageId
						}, commandType: CommandType.StoredProcedure).FirstOrDefault();
					}
					customerAPIResponses.ErrorMessageTitle = "Success!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.ErrorMesaage.OtpSentSuccessfully", languageId, dbcontext);
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = response;
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(RegistationverificationCustomer.StoreId, "API.ErrorMesaage.UserWithMobileNotRegisteredYet", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
					customerAPIResponses.ResponseObj = customerDetail;
				}

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}
		/// <summary>
		///    Delete Customer Phone Number 
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/DeleteCustomerPhone")]
		[HttpGet]
		public CustomerAPIResponses DeleteCustomerPhone(string phone)
		{
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			try
			{
				var customers = dbcontext.GenericAttribute.Where(x => x.Value.Contains(phone)).ToList();
				if (customers.Any())
				{
					foreach (var item in customers)
					{
						var customerDelete = dbcontext.Customer.AsEnumerable().Where(x => x.Id == item.EntityId).FirstOrDefault();
						if (customerDelete != null)
						{
							dbcontext.Entry(customerDelete).State = EntityState.Deleted;
							dbcontext.SaveChanges();
						}
						dbcontext.Entry(item).State = EntityState.Deleted;
						dbcontext.SaveChanges();

					}
					customerAPIResponses.ErrorMessageTitle = "Success!!";
					customerAPIResponses.ErrorMessage = "Deleted Successfully";
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = 200;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Errro!!";
					customerAPIResponses.ErrorMessage = "Not Found";
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = 200;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		/// <summary>
		/// Add or Reomve merchant from favorite list
		/// </summary>
		/// <param name="addRemoveFavoriteMerchantModel"></param>
		/// <returns>CustomerAPIResponses</returns>
		[Route("~/api/v1/AddRemoveMerchantToFavorite")]
		[HttpPost]
		public CustomerAPIResponses AddRemoveMerchantToFavorite([FromBody] AddRemoveFavoriteMerchant addRemoveFavoriteMerchantModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(addRemoveFavoriteMerchantModel.StoreId, addRemoveFavoriteMerchantModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();

			if (addRemoveFavoriteMerchantModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addRemoveFavoriteMerchantModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(addRemoveFavoriteMerchantModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addRemoveFavoriteMerchantModel.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (addRemoveFavoriteMerchantModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addRemoveFavoriteMerchantModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (addRemoveFavoriteMerchantModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addRemoveFavoriteMerchantModel.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (addRemoveFavoriteMerchantModel.MerchantId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addRemoveFavoriteMerchantModel.StoreId, "API.ErrorMesaage.MerchantMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(addRemoveFavoriteMerchantModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == addRemoveFavoriteMerchantModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addRemoveFavoriteMerchantModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var favoriteMerchantResponce = Helper.AddREmoveFavoriteMerchant(addRemoveFavoriteMerchantModel, languageId, dbcontext);
				if (favoriteMerchantResponce != null)
				{
					if (favoriteMerchantResponce.Success)
					{
						customerAPIResponses.ErrorMessage = null;
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = 200;
						customerAPIResponses.ResponseObj = favoriteMerchantResponce;
						return customerAPIResponses;
					}
					else
					{
						customerAPIResponses.ErrorMessage = favoriteMerchantResponce.Message;
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = 204;
						customerAPIResponses.ResponseObj = favoriteMerchantResponce;
						return customerAPIResponses;
					}
				}
				else
				{
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addRemoveFavoriteMerchantModel.StoreId, "API.ErrorMesaage.FavoriteMerchantFail", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 204;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		/// <summary>
		/// Get favorite merchant list based on customer
		/// </summary>
		/// <param name="customerFavoriteMerchant"></param>
		/// <returns>CustomerAPIResponses</returns>
		[Route("~/api/v1/GetCustomrFavoriteMerchants")]
		[HttpPost]
		public CustomerAPIResponses GetCustomrFavoriteMerchants([FromBody] CustomerFavoriteMerchant customerFavoriteMerchant)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customerFavoriteMerchant.StoreId, customerFavoriteMerchant.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();

			if (customerFavoriteMerchant == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerFavoriteMerchant.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customerFavoriteMerchant.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerFavoriteMerchant.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customerFavoriteMerchant.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerFavoriteMerchant.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customerFavoriteMerchant.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerFavoriteMerchant.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(customerFavoriteMerchant.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == customerFavoriteMerchant.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customerFavoriteMerchant.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				var favoriteMerchantsrespnce = _commonService.GetFavoriteMerchantsByCustomerId(customerFavoriteMerchant);
				if (favoriteMerchantsrespnce != null && favoriteMerchantsrespnce.ValidData)
				{
					customerAPIResponses.ErrorMessage = null;
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = 200;
					customerAPIResponses.ResponseObj = favoriteMerchantsrespnce;
					return customerAPIResponses;
				}
				else
				{
					customerAPIResponses.ErrorMessage = favoriteMerchantsrespnce?.ResultData;
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 204;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		#region
		protected Dictionary<string, IEnumerable<string>> AllowedTokens
		{
			get
			{
				if (_allowedTokens != null)
					return _allowedTokens;

				_allowedTokens = new Dictionary<string, IEnumerable<string>>();

				//store tokens
				_allowedTokens.Add(TokenGroupNames.StoreTokens, new[]
				{
					"%Store.Name%",
					"%Store.URL%",
					"%Store.Email%",
					"%Store.CompanyName%",
					"%Store.CompanyAddress%",
					"%Store.CompanyPhoneNumber%",
					"%Store.CompanyVat%",
					"%Facebook.URL%",
					"%Twitter.URL%",
					"%YouTube.URL%",
					"%GooglePlus.URL%"
				});

				//customer tokens
				_allowedTokens.Add(TokenGroupNames.CustomerTokens, new[]
				{
					"%Customer.Email%",
					"%Customer.Username%",
					"%Customer.FullName%",
					"%Customer.FirstName%",
					"%Customer.LastName%",
					"%Customer.VatNumber%",
					"%Customer.VatNumberStatus%",
					"%Customer.CustomAttributes%",
					"%Customer.PasswordRecoveryURL%",
					"%Customer.AccountActivationURL%",
					"%Customer.EmailRevalidationURL%",
					"%Wishlist.URLForCustomer%"
				});

				//order tokens
				_allowedTokens.Add(TokenGroupNames.OrderTokens, new[]
				{
					"%Order.OrderNumber%",
					"%Order.CustomerFullName%",
					"%Order.CustomerEmail%",
					"%Order.BillingFirstName%",
					"%Order.BillingLastName%",
					"%Order.BillingPhoneNumber%",
					"%Order.BillingEmail%",
					"%Order.BillingFaxNumber%",
					"%Order.BillingCompany%",
					"%Order.BillingAddress1%",
					"%Order.BillingAddress2%",
					"%Order.BillingCity%",
					"%Order.BillingStateProvince%",
					"%Order.BillingZipPostalCode%",
					"%Order.BillingCountry%",
					"%Order.BillingCustomAttributes%",
					"%Order.Shippable%",
					"%Order.ShippingMethod%",
					"%Order.ShippingFirstName%",
					"%Order.ShippingLastName%",
					"%Order.ShippingPhoneNumber%",
					"%Order.ShippingEmail%",
					"%Order.ShippingFaxNumber%",
					"%Order.ShippingCompany%",
					"%Order.ShippingAddress1%",
					"%Order.ShippingAddress2%",
					"%Order.ShippingCity%",
					"%Order.ShippingStateProvince%",
					"%Order.ShippingZipPostalCode%",
					"%Order.ShippingCountry%",
					"%Order.ShippingCustomAttributes%",
					"%Order.PaymentMethod%",
					"%Order.VatNumber%",
					"%Order.CustomValues%",
					"%Order.Product(s)%",
					"%Order.CreatedOn%",
					"%Order.OrderURLForCustomer%"
				});

				//shipment tokens
				_allowedTokens.Add(TokenGroupNames.ShipmentTokens, new[]
				{
					"%Shipment.ShipmentNumber%",
					"%Shipment.TrackingNumber%",
					"%Shipment.TrackingNumberURL%",
					"%Shipment.Product(s)%",
					"%Shipment.URLForCustomer%"
				});

				//refunded order tokens
				_allowedTokens.Add(TokenGroupNames.RefundedOrderTokens, new[]
				{
					"%Order.AmountRefunded%"
				});

				//order note tokens
				_allowedTokens.Add(TokenGroupNames.OrderNoteTokens, new[]
				{
					"%Order.NewNoteText%",
					"%Order.OrderNoteAttachmentUrl%"
				});

				//recurring payment tokens
				_allowedTokens.Add(TokenGroupNames.RecurringPaymentTokens, new[]
				{
					"%RecurringPayment.ID%",
					"%RecurringPayment.CancelAfterFailedPayment%",
					"%RecurringPayment.RecurringPaymentType%"
				});

				//newsletter subscription tokens
				_allowedTokens.Add(TokenGroupNames.SubscriptionTokens, new[]
				{
					"%NewsLetterSubscription.Email%",
					"%NewsLetterSubscription.ActivationUrl%",
					"%NewsLetterSubscription.DeactivationUrl%"
				});

				//product tokens
				_allowedTokens.Add(TokenGroupNames.ProductTokens, new[]
				{
					"%Product.ID%",
					"%Product.Name%",
					"%Product.ShortDescription%",
					"%Product.ProductURLForCustomer%",
					"%Product.SKU%",
					"%Product.StockQuantity%"
				});

				//return request tokens
				_allowedTokens.Add(TokenGroupNames.ReturnRequestTokens, new[]
				{
					"%ReturnRequest.CustomNumber%",
					"%ReturnRequest.OrderId%",
					"%ReturnRequest.Product.Quantity%",
					"%ReturnRequest.Product.Name%",
					"%ReturnRequest.Reason%",
					"%ReturnRequest.RequestedAction%",
					"%ReturnRequest.CustomerComment%",
					"%ReturnRequest.StaffNotes%",
					"%ReturnRequest.Status%"
				});

				//forum tokens
				_allowedTokens.Add(TokenGroupNames.ForumTokens, new[]
				{
					"%Forums.ForumURL%",
					"%Forums.ForumName%"
				});

				//forum topic tokens
				_allowedTokens.Add(TokenGroupNames.ForumTopicTokens, new[]
				{
					"%Forums.TopicURL%",
					"%Forums.TopicName%"
				});

				//forum post tokens
				_allowedTokens.Add(TokenGroupNames.ForumPostTokens, new[]
				{
					"%Forums.PostAuthor%",
					"%Forums.PostBody%"
				});

				//private message tokens
				_allowedTokens.Add(TokenGroupNames.PrivateMessageTokens, new[]
				{
					"%PrivateMessage.Subject%",
					"%PrivateMessage.Text%"
				});

				//vendor tokens
				_allowedTokens.Add(TokenGroupNames.VendorTokens, new[]
				{
					"%Vendor.Name%",
					"%Vendor.Email%"
				});

				//gift card tokens
				_allowedTokens.Add(TokenGroupNames.GiftCardTokens, new[]
				{
					"%GiftCard.SenderName%",
					"%GiftCard.SenderEmail%",
					"%GiftCard.RecipientName%",
					"%GiftCard.RecipientEmail%",
					"%GiftCard.Amount%",
					"%GiftCard.CouponCode%",
					"%GiftCard.Message%"
				});

				//product review tokens
				_allowedTokens.Add(TokenGroupNames.ProductReviewTokens, new[]
				{
					"%ProductReview.ProductName%"
				});

				//attribute combination tokens
				_allowedTokens.Add(TokenGroupNames.AttributeCombinationTokens, new[]
				{
					"%AttributeCombination.Formatted%",
					"%AttributeCombination.SKU%",
					"%AttributeCombination.StockQuantity%"
				});

				//blog comment tokens
				_allowedTokens.Add(TokenGroupNames.BlogCommentTokens, new[]
				{
					"%BlogComment.BlogPostTitle%"
				});

				//news comment tokens
				_allowedTokens.Add(TokenGroupNames.NewsCommentTokens, new[]
				{
					"%NewsComment.NewsTitle%"
				});

				//product back in stock tokens
				_allowedTokens.Add(TokenGroupNames.ProductBackInStockTokens, new[]
				{
					"%BackInStockSubscription.ProductName%",
					"%BackInStockSubscription.ProductUrl%"
				});

				//email a friend tokens
				_allowedTokens.Add(TokenGroupNames.EmailAFriendTokens, new[]
				{
					"%EmailAFriend.PersonalMessage%",
					"%EmailAFriend.Email%"
				});

				//wishlist to friend tokens
				_allowedTokens.Add(TokenGroupNames.WishlistToFriendTokens, new[]
				{
					"%Wishlist.PersonalMessage%",
					"%Wishlist.Email%"
				});

				//VAT validation tokens
				_allowedTokens.Add(TokenGroupNames.VatValidation, new[]
				{
					"%VatValidationResult.Name%",
					"%VatValidationResult.Address%"
				});

				//contact us tokens
				_allowedTokens.Add(TokenGroupNames.ContactUs, new[]
				{
					"%ContactUs.SenderEmail%",
					"%ContactUs.SenderName%",
					"%ContactUs.Body%"
				});

				//contact vendor tokens
				_allowedTokens.Add(TokenGroupNames.ContactVendor, new[]
				{
					"%ContactUs.SenderEmail%",
					"%ContactUs.SenderName%",
					"%ContactUs.Body%"
				});

				return _allowedTokens;
			}
		}

		#endregion

	}
	public class CustomerAdditionalInfo
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
