﻿using Microsoft.AspNetCore.Mvc;
using NHKCustomerApplication.Caching;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace NHKCustomerApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VersionController : ControllerBase
    {
        private ecuadordevContext _context;
        private readonly ICacheManager _cacheManager;

        public VersionController(ecuadordevContext context, ICacheManager cacheManager)
        {
            _context = context;
            this._cacheManager = cacheManager;
        }
        [HttpPost]
        [Route("~/api/Version")]
        public APIVersionV2 PostVersionV1([FromBody] PublicKey pubkey)
        {
            int languageId = LanguageHelper.GetIdByLangCode(pubkey.StoreId, pubkey.UniqueSeoCode, _context);

            APIVersionV2 customerAPIResponses = new APIVersionV2();
            if (pubkey == null && pubkey.PublicKeyAuth == null && pubkey.PublicKeyAuth == "")
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, _context);
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NotAcceptable;
                customerAPIResponses.Status = false;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(pubkey.AppName))
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.ErrorMessage.AppNameMissing", languageId, _context);
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NotAcceptable;
                customerAPIResponses.Status = false;
                return customerAPIResponses;
            }
            try
            {
                if (pubkey.PublicKeyAuth != "4ED0BC8A-238F-4A90-BCEF-C76BEB9735D8")
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, _context);
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                    customerAPIResponses.Status = false;
                    return customerAPIResponses;
                }
                if (!string.IsNullOrEmpty(pubkey.AppName))
                {
                    var appname = pubkey.AppName.ToLower().ToString();
                    var storeNameExist = _context.VersionInfo.AsEnumerable().Where(x => x.AppName.ToLower().ToString().Equals(appname)).Any();
                    if (!storeNameExist)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.ErrorMessage.StoreNotFound", languageId, _context);
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                        customerAPIResponses.Status = false;
                        return customerAPIResponses;
                    }

                }
                int count = _context.VersionInfo.AsEnumerable().Count();
                if (count == 0)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.ErrorMesaage.NoVersionRecordFound", languageId, _context);
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                    customerAPIResponses.Status = false;
                    return customerAPIResponses;
                }
                
                var key = string.Format(CacheKeys.VersionKey, pubkey.StoreId, pubkey.AppName,pubkey.APKVersion,pubkey.DeviceOStype,languageId);
                var versiondata = _cacheManager.Get(key, () =>
                {
                    var entity = _context.VersionInfo.AsEnumerable().Where(x => x.DeviceOstype == pubkey.DeviceOStype && x.AppName.ToLower().ToString() == pubkey.AppName.ToLower().ToString()).FirstOrDefault();
                    if (entity != null)
                    {
                        var version = _context.Setting.FirstOrDefault(s => s.StoreId == pubkey.StoreId && ((pubkey.DeviceOStype == "A") ? s.Name == "apisettings.customerapi.version.android" : s.Name == "apisettings.customerapi.version.ios"))?.Value ?? "1.0.0";
                        var seconds = _context.Setting.Where(x => x.Name.Contains("adminareasettings.loginotptimer") && x.StoreId == pubkey.StoreId).FirstOrDefault()?.Value ?? "60";
                        var minutes = _context.Setting.Where(x => x.Name.Contains("adminarea.login.resendOtp") && x.StoreId == pubkey.StoreId).FirstOrDefault()?.Value ?? "5";

                        if (Convert.ToInt32(pubkey.APKVersion.Replace(".", "")) < Convert.ToInt32(version.Replace(".", "")))
                        {
                            customerAPIResponses.ErrorMessageTitle = "Error!!";
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.ErrorMesaage.VersionRecordNotFound", languageId, _context);
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                            return customerAPIResponses;
                        }
                        customerAPIResponses.ApiVersion = version;
                        customerAPIResponses.ApiKey = entity.Apikey;
                        customerAPIResponses.ResourceStringsVersion = Convert.ToDecimal(_context.Setting.FirstOrDefault(x => x.Name.ToLower() == "apisettings.resourcestringsversion.customer" && x.StoreId == entity.StoreId.GetValueOrDefault())?.Value ?? "1.0");
                        customerAPIResponses.TokenDispStatus = entity.TokenDispStatus;
                        customerAPIResponses.IsBaseSplashNeeded = entity.IsBaseSplashNeeded ?? false;
                        customerAPIResponses.IsMultiVendorSupported = entity.IsMultiVendorSupported;
                        customerAPIResponses.SplashURL = entity.SplashUrl;
                        customerAPIResponses.IsTutorialNeeded = entity.IsTutorialNeeded;
                        customerAPIResponses.TutorialVersion = entity.TutorialVersion;
                        customerAPIResponses.IsMultiLanguage = entity.IsMultiLanguage;
                        customerAPIResponses.DataBaseVersion = entity.DataBaseVersion;
                        customerAPIResponses.IsDispGoogMap = entity.IsDispGoogMap ?? false;
                        customerAPIResponses.DispGoogMapURL = entity.DispGoogMapUrl;
                        customerAPIResponses.IsSocketAvailable = entity.IsSocketAvailable ?? false;
                        customerAPIResponses.SocketPort = entity.SocketPort;
                        customerAPIResponses.SocketURL = entity.SocketUrl;
                        customerAPIResponses.StoreId = entity.StoreId.GetValueOrDefault();
                        customerAPIResponses.CheckStoreFront = BaseMethodsHelper.CheckStoreFront(entity.StoreId.GetValueOrDefault(), _context);
                        customerAPIResponses.IsStoreFront = BaseMethodsHelper.IsStoreFront(entity.StoreId.GetValueOrDefault(), _context);
                        customerAPIResponses.Seconds = seconds;
                        customerAPIResponses.Minutes = minutes;
                        var store = _context.Store.FirstOrDefault(x => x.Id == customerAPIResponses.StoreId);
                        if (store != null)
                        {
                            customerAPIResponses.IsDelTakeAwayOnDashboard = store.IsTakeaway ?? false;
                            customerAPIResponses.IsDelDeliveryOnDashboard = store.IsDelivery ?? false;
                            customerAPIResponses.RestnCookId = _context.Vendor.FirstOrDefault(x => x.StoreId == store.Id && x.Active && !x.Deleted)?.Id ?? 0;
                        }
                        var storeCurrency = Helper.GetStoreCurrency(customerAPIResponses.StoreId, context: _context);
                        var storeCurrencyCode= Helper.GetStoreCodeCurrency(customerAPIResponses.StoreId, context: _context);
                        customerAPIResponses.StoreCurrency = !string.IsNullOrEmpty(storeCurrency) ? (storeCurrency) : "";
                        customerAPIResponses.StoreCurrencyCode = !string.IsNullOrEmpty(storeCurrencyCode) ? (storeCurrencyCode + " ") : "";
                        customerAPIResponses.GetHelp = entity.GetHelp;
                        customerAPIResponses.TermsCondition = entity.TermsCondition;
                        customerAPIResponses.Privacy = entity.Privacy;
                        customerAPIResponses.SupportChatUrl = entity.SupportChatUrl;
                        customerAPIResponses.SupportPhone = entity.SupportPhone;
                        customerAPIResponses.SupportEmail = entity.SupportEmail;
                        customerAPIResponses.WebUrl = Helper.GetBaseUrl(customerAPIResponses.StoreId, _context);
                        // Fetch Languages
                        var entityLang = _context.VersionLangs.AsEnumerable().Where(x => x.DeviceOstype == pubkey.DeviceOStype && x.Apkversion == pubkey.APKVersion).ToList();
                        customerAPIResponses.LanguageCount = entityLang.Count();
                        List<APIVersionLanguage> verLangs = new List<APIVersionLanguage>();
                        if (entityLang.Any())
                        {
                            foreach (var langs in entityLang)
                            {
                                APIVersionLanguage aPIVersionLanguage = new APIVersionLanguage();
                                aPIVersionLanguage.APIVerLangId = langs.Id;
                                aPIVersionLanguage.APIVerLang = langs.VerLang;
                                verLangs.Add(aPIVersionLanguage);
                            }
                            customerAPIResponses.APIVerLangs = verLangs;
                        }
                        // Fetch Tutorials
                        var tutorialLang = _context.TutorialUrl.AsEnumerable().ToList();
                        customerAPIResponses.TutorialCount = tutorialLang.Count();
                        List<TutorialURLS> tutorialLangVerLangs = new List<TutorialURLS>();
                        if (tutorialLang.Any())
                        {
                            foreach (var langs in tutorialLang)
                            {
                                TutorialURLS tutorialVersionLanguage = new TutorialURLS();
                                tutorialVersionLanguage.TutorialID = langs.Id;
                                tutorialVersionLanguage.TutorialURL = langs.Url;
                                tutorialLangVerLangs.Add(tutorialVersionLanguage);
                            }
                            customerAPIResponses.TutorialURLs = tutorialLangVerLangs;
                        }
                        if (customerAPIResponses.TutorialURLs == null)
                        {
                            customerAPIResponses.TutorialURLs = new List<TutorialURLS>();
                        }
                        customerAPIResponses.ErrorMessageTitle = "Success";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.ErrorMesaage.VersionFoundSuccessfully", languageId, _context);
                        customerAPIResponses.Status = true;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                        return customerAPIResponses;

                    }
                 
                    else
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.ErrorMesaage.VersionRecordNotFound", languageId, _context);
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                        return customerAPIResponses;
                    }

                });
                return versiondata;
               
               

            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
                return customerAPIResponses;

            }




        }

    }
}