﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Nop.Data.Mapping.NB.Suppliers
{
    public partial class SupplierMap : NopEntityTypeConfiguration<Nop.Core.Domain.NB.Suppliers.Supplier>
    {

        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<Nop.Core.Domain.NB.Suppliers.Supplier> builder)
        {
            builder.ToTable(NopMappingDefaults.Supplier);
            builder.HasKey(supplier => supplier.Id);

            builder.Property(supplier => supplier.SupplierName).HasMaxLength(400).IsRequired();
            builder.Property(supplier => supplier.Description);
            builder.Property(supplier => supplier.AddressId).IsRequired();
            builder.Property(supplier => supplier.Active).IsRequired();
            builder.Property(supplier => supplier.Deleted).IsRequired();
            builder.Property(supplier => supplier.LocationMap);
            builder.Property(supplier => supplier.Geofancing);
            builder.Property(supplier => supplier.CreatedOnUtc).IsRequired();
            builder.Property(supplier => supplier.DeliveryFee).IsRequired();
            builder.Property(supplier => supplier.StoreId).IsRequired();
            builder.Property(supplier => supplier.FixedOrDynamic);
            builder.Property(supplier => supplier.KmOrMilesForDynamic);
            builder.Property(supplier => supplier.DistanceForDynamic);
            builder.Property(supplier => supplier.DisplayOrder);
            builder.Property(supplier => supplier.Published);
            builder.Property(supplier => supplier.SubjectToAcl);
            builder.Property(supplier => supplier.ParentSupplierId);
            builder.Property(supplier => supplier.PriceForDynamic);
            builder.Property(supplier => supplier.PricePerUnitDistanceForDynamic);
            base.Configure(builder);

        }
    }

}
#endregion