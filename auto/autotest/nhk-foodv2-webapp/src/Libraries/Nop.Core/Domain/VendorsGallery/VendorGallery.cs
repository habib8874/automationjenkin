using System;
using System.Collections.Generic;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Seo;

namespace Nop.Core.Domain.VendorsGallery
{
    /// <summary>
    /// Represents a vendor Gallery
    /// </summary>
    public partial class VendorGallery : BaseEntity
    {

        /// <summary>
        /// Gets or sets the Store
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the vendor
        /// </summary>
        public int VendorId { get; set; }

        /// <summary>
        /// Gets or sets the Gallery Type
        /// </summary>
        public int GalleryType { get; set; }
        
        /// <summary>
        /// Gets or sets the picture identifier
        /// </summary>
        public Nullable<int> PictureId { get; set; }

        /// <summary>
        /// Gets or sets the GalleryURL
        /// </summary>
        public string GalleryURL { get; set; }

        /// <summary>
        /// Gets or sets the Gallery Caption
        /// </summary>
        public string GalleryCaption { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity is active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Gets or sets a value CreatedDt
        /// </summary>
        public DateTime CreatedDt { get; set; }
        /// <summary>
        /// Gets or sets a value UpdatedDt
        /// </summary>
        public Nullable<DateTime> UpdatedDt { get; set; }
        /// <summary>
        /// Gets or sets a value UpdatedDt
        /// </summary>
        public Nullable<int> DisplayOrder{ get; set; }

    }
}
