﻿using Dapper;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace NHKCustomerApplication.Repository
{
	/// <summary>
	/// UserRepository.
	/// </summary>
	public class UserRepository : IUserRepository
	{
		#region "Fields"
		private const string RESTPRODUCTATTRIBUTESSPNAME = "sp_Rest_Product_Attributes";
		private const string RESTPRODUCTSPNAME = "sp_Rest_Product";
		private const string USERREGISTRATIONSPNAME = "sp_User_SignUp";
		private const string RESTPRODUCTSNAMEBYFILTER = "NB_ProductsFilter";
		private const string EXTERNALAUTH = "sp_ExternalAuth_SignUp";
		private const string INSERTGUESTUSERSPNAME = "sp_Insert_Guest_User";
		private const string USERREGISTRATIONVERIFICATIONSPNAME = "sp_User_Verification";
		private const string USERREGISTRATIONVERIFICATIONV3SPNAME = "sp_User_VerficationV3";
		private const string USERLOGINSPNAME = "sp_User_Login";
		private const string USERLOGINV3SPNAME = "sp_User_LoginV3";
		private const string USERLOGININFOSPNAME = "sp_User_Login_Info";
		private const string PRODUCTBYCATEGORYID = "NB_ProductsByCategoryId";
		private const string PRODUCTBYCATEGORYIDV2 = "NB_GetProductByCategoryId_V2";
		private const string USERLOGININFOSPNAMEV2_1 = "sp_User_Login_Info_V2_1";
		private const string LASTIPADDRESS = "127.0.0.1";
		private const string PROFILEPIC = "default-image_450.png";
		private string connectionString = string.Empty;
		private const string PASSWORDFORMAT = "SHA512";

		#endregion

		#region "constructor"

		public UserRepository()
		{
			connectionString = Helper.GetConnectionString();
		}

		#endregion

		/// <summary>
		/// RegisterCustomer.
		/// </summary>
		/// <param name="customer"></param>
		/// <param name="saltKey"></param>
		/// <param name="password"></param>
		/// <param name="otp"></param>
		/// <returns></returns>
		public CustomerResponseModel RegisterCustomer(CustomerModel customer, string saltKey, string password, string otp, int languageId = 0)
		{
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					string result = db.Query<string>(USERREGISTRATIONSPNAME, new
					{
						customer.ApiKey,
						CustomerGuid = Guid.NewGuid(),
						Username = customer.Email,
						customer.Email,
						LastIpAddress = LASTIPADDRESS,
						RegisteredInStoreId = customer.StoreId,
						Phone = customer.MobileNo,
						ProfilePic = PROFILEPIC,
						customer.LastName,
						customer.FirstName,
						Password = password,
						PasswordSalt = saltKey,
						customer.DeviceNo,
						customer.DeviceToken,
						customer.Imei1,
						customer.Imei2,
						customer.LatPos,
						customer.LongPos,
						customer.DeviceType,
						Otp = otp,
						LanguageId = languageId
					}, commandType: CommandType.StoredProcedure).FirstOrDefault();
					return new CustomerResponseModel { ValidData = true, ResultData = result };
				}
				catch (SqlException e)
				{
					return new CustomerResponseModel { ValidData = false, ResultData = e.Message };
				}
			}
		}

		/// <summary>
		/// ExternalAuth.
		/// </summary>
		/// <param name="customer"></param>
		/// <param name="languageId">languageId</param>
		/// <returns></returns>
		public CustomerResponseModel ExternalAuth(ExternalAuthModel customer)
		{
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					string result = db.Query<string>(EXTERNALAUTH, new
					{
						customer.ApiKey,
						CustomerGuid = Guid.NewGuid(),
						Username = customer.Email,
						customer.Email,
						LastIpAddress = LASTIPADDRESS,
						RegisteredInStoreId = customer.StoreId,
						Phone = customer.MobileNo,
						customer.LastName,
						customer.FirstName,
						customer.DeviceToken,
						customer.LoginType,
						customer.AccountId,
						customer.IsGuestId,
						customer.GuestUserId
					}, commandType: CommandType.StoredProcedure).FirstOrDefault();
					return new CustomerResponseModel { ValidData = true, ResultData = result };
				}
				catch (SqlException e)
				{
					return new CustomerResponseModel { ValidData = false, ResultData = e.Message };
				}
			}
		}


		/// <summary>
		/// RegistaionVerificationCustomer.
		/// </summary>
		/// <param name="customer"></param>
		/// <returns></returns>
		public CustomerResponseModel RegistaionVerificationCustomerV3(RegistationverificationCustomer customer)
		{
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					string result = db.Query<string>(USERREGISTRATIONVERIFICATIONV3SPNAME, new
					{
						customer.ApiKey,
						customer.CustomerId,
						customer.OTP,
						customer.isGuestId,
						customer.GuestUserId
					}, commandType: CommandType.StoredProcedure).FirstOrDefault();
					return new CustomerResponseModel { ValidData = true, ResultData = result };
				}
				catch (SqlException e)
				{
					return new CustomerResponseModel { ValidData = false, ResultData = e.Message };
				}
			}
		}

		/// <summary>
		/// CustomerLogin.
		/// </summary>
		/// <param name="customer">CustomerLogin object.</param>
		/// <returns>CustomerLoginResponseModel object</returns>
		public CustomerLoginResponseModel CustomerLoginV3(CustomerLoginV3 customer)
		{
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					string result = db.Query<string>(USERLOGINV3SPNAME, new
					{
						customer.ApiKey,
						CustomerGuid = Guid.NewGuid(),
						Username = customer.Email,
						customer.Email,
						RegisteredInStoreId = customer.StoreId,
						Phone = customer.Phone
					}, commandType: CommandType.StoredProcedure).FirstOrDefault();
					return new CustomerLoginResponseModel { ValidData = true, ResultData = result };
				}
				catch (SqlException e)
				{
					return new CustomerLoginResponseModel { ValidData = false, ResultData = e.Message };
				}
			}
		}


		/// <summary>
		/// Insert Guest User.
		/// </summary>
		/// <param name="customer"></param>
		/// <param name="otp"></param>
		/// <returns></returns>
		public CustomerResponseModel InsertGuestUser(GuestUserModel customer, string otp)
		{
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					string result = db.Query<string>(INSERTGUESTUSERSPNAME, new
					{
						customer.ApiKey,
						CustomerGuid = Guid.NewGuid(),
						RegisteredInStoreId = customer.StoreId,
						customer.DeviceNo,
						customer.DeviceToken,
						customer.DeviceType,
						Otp = otp
					}, commandType: CommandType.StoredProcedure).FirstOrDefault();
					return new CustomerResponseModel { ValidData = true, ResultData = result };
				}
				catch (SqlException e)
				{
					return new CustomerResponseModel { ValidData = false, ResultData = e.Message };
				}
			}
		}


		/// <summary>
		/// RegistaionVerificationCustomer.
		/// </summary>
		/// <param name="customer"></param>
		/// <returns></returns>
		public CustomerResponseModel RegistaionVerificationCustomer(RegistationverificationCustomer customer)
		{
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					string result = db.Query<string>(USERREGISTRATIONVERIFICATIONSPNAME, new
					{
						customer.ApiKey,
						customer.CustomerId,
						customer.OTP
					}, commandType: CommandType.StoredProcedure).FirstOrDefault();
					return new CustomerResponseModel { ValidData = true, ResultData = result };
				}
				catch (SqlException e)
				{
					return new CustomerResponseModel { ValidData = false, ResultData = e.Message };
				}
			}
		}

		/// <summary>
		/// CustomerLogin.
		/// </summary>
		/// <param name="customer">CustomerLogin object.</param>
		/// <returns>CustomerLoginResponseModel object</returns>
		public CustomerLoginResponseModel CustomerLogin(CustomerLogin customer)
		{
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					PasswordModel result = db.Query<PasswordModel>(USERLOGINSPNAME, new
					{
						customer.ApiKey,
						customer.Email,
						customer.Phone,
						customer.StoreId
					}, commandType: CommandType.StoredProcedure).FirstOrDefault();

					if (result != null)
					{
						var UserPassword = Helper.CreatePasswordHash(customer.Pswd, result.PasswordSalt, PASSWORDFORMAT);
						if (UserPassword == result.Password)
						{
							CustomerLoginResponseModel result2 = db.Query<CustomerLoginResponseModel>(USERLOGININFOSPNAME, new
							{
								result.CustomerId,
								result.Active,
								result.Email,
								customer.StoreId
							}, commandType: CommandType.StoredProcedure).FirstOrDefault();
							result2.UserID = result.CustomerId;
							result2.UserStatus = result.Active == true ? "Active" : "InActive";
							result2.ValidData = true;
							result2.Email = result.Email;
							return result2;
						}
						else
						{
							return new CustomerLoginResponseModel { ValidData = false, ResultData = "Password is incorrect" };
						}
					}
					return new CustomerLoginResponseModel { ValidData = false, ResultData = "Internal server error" }; ;
				}
				catch (SqlException e)
				{
					return new CustomerLoginResponseModel { ValidData = false, ResultData = e.Message };
				}
			}
		}

		/// <summary>
		/// CustomerLoginV2_2
		/// </summary>
		/// <param name="customer">CustomerLogin object.</param>
		/// <returns>CustomerLoginResponseModel object</returns>
		public CustomerLoginResponseModel CustomerLoginV2_2(CustomerLogin customer)
		{
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					PasswordModel result = db.Query<PasswordModel>(USERLOGINSPNAME, new
					{
						customer.ApiKey,
						customer.Email,
						customer.Phone,
						customer.StoreId,
						customer.GuestUserId
					}, commandType: CommandType.StoredProcedure).FirstOrDefault();

					if (result != null)
					{
						var UserPassword = Helper.CreatePasswordHash(customer.Pswd, result.PasswordSalt, PASSWORDFORMAT);
						if (UserPassword == result.Password)
						{
							CustomerLoginResponseModel result2 = db.Query<CustomerLoginResponseModel>(USERLOGININFOSPNAME, new
							{
								result.CustomerId,
								result.Active,
								result.Email,
								customer.StoreId
							}, commandType: CommandType.StoredProcedure).FirstOrDefault();
							result2.UserID = result.CustomerId;
							result2.UserStatus = result.Active == true ? "Active" : "InActive";
							result2.ValidData = true;
							result2.Email = result.Email;
							return result2;
						}
						else
						{
							return new CustomerLoginResponseModel { ValidData = false, ResultData = "Password is incorrect" };
						}
					}
					return new CustomerLoginResponseModel { ValidData = false, ResultData = "Internal server error" }; ;
				}
				catch (SqlException e)
				{
					return new CustomerLoginResponseModel { ValidData = false, ResultData = e.Message };
				}
			}
		}

		/// <summary>
		/// CustomerLogin.
		/// </summary>
		/// <param name="customer">CustomerLogin object.</param>
		/// <returns>CustomerLoginResponseModel object</returns>
		public CustomerLoginResponseModel CustomerLoginV2_1(CustomerLoginV2_1 customer)
		{
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					PasswordModel result = db.Query<PasswordModel>(USERLOGINSPNAME, new
					{
						customer.ApiKey,
						customer.Email,
						customer.Phone,
						customer.StoreId
					}, commandType: CommandType.StoredProcedure).FirstOrDefault();

					if (result != null)
					{
						var UserPassword = Helper.CreatePasswordHash(customer.Pswd, result.PasswordSalt, PASSWORDFORMAT);
						if (UserPassword == result.Password)
						{
							CustomerLoginResponseModel result2 = db.Query<CustomerLoginResponseModel>(USERLOGININFOSPNAMEV2_1, new
							{
								result.CustomerId,
								result.Active,
								result.Email,
								customer.StoreId,
								customer.TimeZoneId
							}, commandType: CommandType.StoredProcedure).FirstOrDefault();
							result2.UserID = result.CustomerId;
							result2.UserStatus = result.Active == true ? "Active" : "InActive";
							result2.ValidData = true;
							result2.Email = result.Email;
							return result2;
						}
						else
						{
							return new CustomerLoginResponseModel { ValidData = false, ResultData = "Password is  incorrect" };
						}
					}
					return new CustomerLoginResponseModel { ValidData = false, ResultData = "Internal server error" }; ;
				}
				catch (SqlException e)
				{
					return new CustomerLoginResponseModel { ValidData = false, ResultData = e.Message };
				}
			}
		}

		public CustomerRestProductResponseModel GetCooknRestItemsByRestnCookId(CooknRestProductByID customer)
		{
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					var result = db.Query<RestProductModel>(RESTPRODUCTSPNAME, new
					{
						customer.APIKey,
						customer.CustId,
						customer.RestnCookId,
						customer.StoreId
					}, commandType: CommandType.StoredProcedure);
					return new CustomerRestProductResponseModel { ValidData = true, ResultData = "", Products = result.ToList() };
				}
				catch (SqlException e)
				{
					return new CustomerRestProductResponseModel { ValidData = false, ResultData = e.Message };
				}
			}
		}

		public List<ProductAttributesV2> GetProductProductAttributes(List<int> productIds)
		{
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					string ProductIds = string.Join(",", productIds);
					var result = db.Query<ProductAttributesV2>(RESTPRODUCTATTRIBUTESSPNAME, new
					{
						ProductIds
					}, commandType: CommandType.StoredProcedure);
					return result.ToList();
				}
				catch (SqlException e)
				{
					return null;
				}
			}
		}

		public CustomerRestProductResponseModels GetCooknRestItemsByCategoryId(CooknRestProductByCategoryIds requestModel)
		{
			// int stmt = SELECT COUNT(*) FROM PRODUCTBYCATEGORYID;
			//int count = 0;
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					var result = db.Query<RestProductResponseModel>(PRODUCTBYCATEGORYID, new
					{
						requestModel.merchantid,
						requestModel.keywords,
						requestModel.selectedImageTags,
						requestModel.CategoryId,
						requestModel.page,
						requestModel.perPage,
						requestModel.StoreId,
						requestModel.CustId

					}, commandType: CommandType.StoredProcedure);

					//remove duplicate products
					var distinctResult = result?.GroupBy(r => r.ItemId)
												 .Select(grp => grp.First())
												 .ToList();

					return new CustomerRestProductResponseModels { ValidData = true, ResultData = "", Products = distinctResult };
				}
				catch (SqlException e)
				{
					return new CustomerRestProductResponseModels { ValidData = false, ResultData = e.Message };
				}

			}
		}

		public CustomerRestProductResponseModels GetCooknRestItemsByCategoryId__V2(CooknRestProductByCategoryIdsV2 requestModel)
		{
			// int stmt = SELECT COUNT(*) FROM PRODUCTBYCATEGORYID;
			//int count = 0;
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					var result = db.Query<RestProductResponseModel>(PRODUCTBYCATEGORYIDV2, new
					{
						requestModel.MerchantId,
						requestModel.Keywords,
						requestModel.SelectedImageTags,
						requestModel.CategoryId,
						requestModel.StoreId,
						requestModel.CustId

					}, commandType: CommandType.StoredProcedure);

					//remove duplicate products
					var distinctResult = result?.GroupBy(r => r.ItemId)
												 .Select(grp => grp.First())
												 .ToList();

					return new CustomerRestProductResponseModels { ValidData = true, ResultData = "", Products = distinctResult };

				}
				catch (SqlException e)
				{
					return new CustomerRestProductResponseModels { ValidData = false, ResultData = e.Message };
				}

			}
		}
		public CustomerRestProductFilterResponseModel GetProductItemsFilternSort(CustomerProductFilter requestModel)
		{
			using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
			{
				try
				{
					var result = db.Query<RestProductFilterModel>(RESTPRODUCTSNAMEBYFILTER, new
					{
						requestModel.CategoryIds,
						requestModel.ManufacturerId,
						requestModel.StoreId,
						requestModel.VendorId,
						requestModel.CustId,
						requestModel.ProductTypeId,
						requestModel.FeaturedProducts,
						requestModel.PriceMin,
						requestModel.PriceMax,
						requestModel.Keywords,
						requestModel.SearchDescriptions,
						requestModel.UseFullTextSearch,
						requestModel.FullTextMode,
						requestModel.FilteredSpecs,
						requestModel.LanguageId,
						requestModel.OrderBy,
						requestModel.PageIndex,
						requestModel.PageSize,
						requestModel.LoadFilterableSpecificationAttributeOptionIds,
						requestModel.FilterableSpecificationAttributeOptionIds,
						requestModel.TotalRecords,
						requestModel.FilteredAttribute,
						requestModel.LoadFilterableProductAttributeOptionIds,
						requestModel.FilterableProductAttributeOptionIds

					}, commandType: CommandType.StoredProcedure);

					return new CustomerRestProductFilterResponseModel { ValidData = true, ResultData = "", Products = result.ToList() };
				}
				catch (SqlException e)
				{
					return new CustomerRestProductFilterResponseModel { ValidData = false, ResultData = e.Message };
				}
			}
		}
	}
}
