﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.NB.Agent;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Web.Factories;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.Themes;

namespace Nop.Web.Controllers
{
    public partial class OrderController : BasePublicController
    {
        #region Fields

        private readonly IOrderModelFactory _orderModelFactory;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly IPaymentService _paymentService;
        private readonly IPdfService _pdfService;
        private readonly IShipmentService _shipmentService;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IThemeContext _themeContext;
        private readonly IAgentService _agentService;
        private readonly ILocalizationService _localizationService;

        #endregion

        #region Ctor

        public OrderController(IOrderModelFactory orderModelFactory,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IPaymentService paymentService,
            IPdfService pdfService,
            IShipmentService shipmentService,
            IWebHelper webHelper,
            IWorkContext workContext,
            RewardPointsSettings rewardPointsSettings,
            IWorkflowMessageService workflowMessageService,
            IThemeContext themeContext,
            IAgentService agentService,
            ILocalizationService localizationService)
        {
            _orderModelFactory = orderModelFactory;
            _orderProcessingService = orderProcessingService;
            _orderService = orderService;
            _paymentService = paymentService;
            _pdfService = pdfService;
            _shipmentService = shipmentService;
            _webHelper = webHelper;
            _workContext = workContext;
            _rewardPointsSettings = rewardPointsSettings;
            _workflowMessageService = workflowMessageService;
            _themeContext = themeContext;
            _agentService = agentService;
            _localizationService = localizationService;
        }

        #endregion

        #region Methods

        //My account / Orders
        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult CustomerOrders()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var model = _orderModelFactory.PrepareCustomerOrderListModel();
            return View(model);
        }

        //My account / Orders / Cancel recurring order
        [HttpPost, ActionName("CustomerOrders")]
        [PublicAntiForgery]
        [FormValueRequired(FormValueRequirement.StartsWith, "cancelRecurringPayment")]
        public virtual IActionResult CancelRecurringPayment(IFormCollection form)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            //get recurring payment identifier
            var recurringPaymentId = 0;
            foreach (var formValue in form.Keys)
                if (formValue.StartsWith("cancelRecurringPayment", StringComparison.InvariantCultureIgnoreCase))
                    recurringPaymentId = Convert.ToInt32(formValue.Substring("cancelRecurringPayment".Length));

            var recurringPayment = _orderService.GetRecurringPaymentById(recurringPaymentId);
            if (recurringPayment == null)
            {
                return RedirectToRoute("CustomerOrders");
            }

            if (_orderProcessingService.CanCancelRecurringPayment(_workContext.CurrentCustomer, recurringPayment))
            {
                var errors = _orderProcessingService.CancelRecurringPayment(recurringPayment);

                var model = _orderModelFactory.PrepareCustomerOrderListModel();
                model.RecurringPaymentErrors = errors;

                return View(model);
            }

            return RedirectToRoute("CustomerOrders");
        }

        //My account / Orders / Retry last recurring order
        [HttpPost, ActionName("CustomerOrders")]
        [PublicAntiForgery]
        [FormValueRequired(FormValueRequirement.StartsWith, "retryLastPayment")]
        public virtual IActionResult RetryLastRecurringPayment(IFormCollection form)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            //get recurring payment identifier
            var recurringPaymentId = 0;
            if (!form.Keys.Any(formValue => formValue.StartsWith("retryLastPayment", StringComparison.InvariantCultureIgnoreCase) &&
                int.TryParse(formValue.Substring(formValue.IndexOf('_') + 1), out recurringPaymentId)))
            {
                return RedirectToRoute("CustomerOrders");
            }

            var recurringPayment = _orderService.GetRecurringPaymentById(recurringPaymentId);
            if (recurringPayment == null)
                return RedirectToRoute("CustomerOrders");

            if (!_orderProcessingService.CanRetryLastRecurringPayment(_workContext.CurrentCustomer, recurringPayment))
                return RedirectToRoute("CustomerOrders");

            var errors = _orderProcessingService.ProcessNextRecurringPayment(recurringPayment);
            var model = _orderModelFactory.PrepareCustomerOrderListModel();
            model.RecurringPaymentErrors = errors.ToList();

            return View(model);
        }

        //My account / Reward points
        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult CustomerRewardPoints(int? pageNumber)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            if (!_rewardPointsSettings.Enabled)
                return RedirectToRoute("CustomerInfo");

            var model = _orderModelFactory.PrepareCustomerRewardPoints(pageNumber);
            return View(model);
        }

        //My account / Order details page
        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult Details(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            var model = _orderModelFactory.PrepareOrderDetailsModel(order);
            return View(model);
        }

        //My account / Order details page / Print
        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult PrintOrderDetails(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            var model = _orderModelFactory.PrepareOrderDetailsModel(order);
            model.PrintMode = true;

            return View("Details", model);
        }

        //My account / Order details page / PDF invoice
        public virtual IActionResult GetPdfInvoice(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            var orders = new List<Order>();
            orders.Add(order);
            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintOrdersToPdf(stream, orders, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, MimeTypes.ApplicationPdf, $"order_{order.Id}.pdf");
        }

        //My account / Order details page / re-order
        public virtual IActionResult ReOrder(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            _orderProcessingService.ReOrder(order);
            return RedirectToRoute("CheckoutOnePage");
        }

        //My account / Order details page / Complete payment
        [HttpPost, ActionName("Details")]
        [PublicAntiForgery]
        [FormValueRequired("repost-payment")]
        public virtual IActionResult RePostPayment(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            if (!_paymentService.CanRePostProcessPayment(order))
                return RedirectToRoute("OrderDetails", new { orderId = orderId });

            var postProcessPaymentRequest = new PostProcessPaymentRequest
            {
                Order = order
            };
            _paymentService.PostProcessPayment(postProcessPaymentRequest);

            if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
            {
                //redirection or POST has been done in PostProcessPayment
                return Content("Redirected");
            }

            //if no redirection has been done (to a third-party payment page)
            //theoretically it's not possible
            return RedirectToRoute("OrderDetails", new { orderId = orderId });
        }

        //My account / Order details page / Shipment details page
        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult ShipmentDetails(int shipmentId)
        {
            var shipment = _shipmentService.GetShipmentById(shipmentId);
            if (shipment == null)
                return Challenge();

            var order = shipment.Order;
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            var model = _orderModelFactory.PrepareShipmentDetailsModel(shipment);
            return View(model);
        }

        #endregion

        #region Custom code from v4.0
        public virtual IActionResult CancelOrder(int orderId)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var order = _orderService.GetOrderById(orderId);
            if (order != null)
                _orderProcessingService.CancelOrder(order, true);

            _workflowMessageService.SendOrderStatusMessageToCustomer(order, _workContext.WorkingLanguage.Id, MessageTemplateSystemNames.SMSOrderCancelledMessage);

            return RedirectToRoute("CustomerOrders");
        }
        #endregion

        #region NB order summary

        public virtual string NBOrderSummary(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            //if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
            //    return "";

            var model = _orderModelFactory.PrepareOrderDetailsModel(order);
            var viewPath = "~/Themes/" + _themeContext.WorkingThemeName.ToString() + "/Views/Order/_NBOrderSummary.cshtml";

            return RenderPartialViewToString(viewPath, model);
        }

        #endregion


        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult CustomerWallets()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var model = _orderModelFactory.PrepareCustomerWalletListModel();
            return View(model);
        }
        [HttpPost]
        public virtual IActionResult AddWalletAmount(Decimal walletAmount)
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {

                return Json(false);
            }
            return Json(false);

        }

        #region Order Tracking

        //My account / Order details page
        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult Tracking(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            var model = _orderModelFactory.PrepareOrderDetailsModel(order);
            return View(model);
        }

        #region Rider Live tracking

        public virtual IActionResult GetRiderCurrentLocation(int riderId, int orderId)
        {
            if (riderId == 0)
            {
                return Json(new
                {
                    success = false,
                    message = "Rider identifier is 0"
                });
            }

            if (orderId == 0)
            {
                return Json(new
                {
                    success = false,
                    message = "Order identifier is 0"
                });
            }

            try
            {
                //get updated lat long detail
                var record = _agentService.GetAgentOrderGeoLocation(riderId, orderId);
                if (record == null)
                {
                    return Json(new
                    {
                        success = false,
                        message = _localizationService.GetResource("NB.Order.LiveTracking.Agent.CurrentLocation.NotFound")
                    });
                }

                //check if rider is reached to destination and he has delivered the order.
                var order = _orderService.GetOrderById(orderId);
                bool isOrderDelivered = order?.OrderStatus == OrderStatus.OrderDelivered;

                //finally return
                return Json(new
                {
                    success = true,
                    lattitude = record.AgentLat,
                    longitide = record.AgentLong,
                    isOrderDelivered,
                    message = "latlng recived!"
                });

            }
            catch (Exception exc)
            {
                return Json(new
                {
                    success = false,
                    message = exc.Message
                });
            }
        }

        #endregion

        #region Tracking Progress (Status and Rider info update)

        /// <summary>
        /// This Ajax call we will use to update Rider info panel and Status Progress panel
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public virtual IActionResult UpdateTrackingProgress(int orderId)
        {
            if (orderId == 0)
            {
                return Json(new
                {
                    success = false,
                    message = "Order identifier is 0"
                });
            }

            try
            {
                var order = _orderService.GetOrderById(orderId);
                bool isOrderDelivered = order?.OrderStatus == OrderStatus.OrderDelivered;

                //prepare ajax model
                var orderDetailAjaxModel = _orderModelFactory.PrepareOrderTrackingAjaxModel(order);

                //Working theme
                var themeName = _themeContext.WorkingThemeName.ToString();

                //Rider info
                var riderInfoModel = orderDetailAjaxModel.LiveTrackingModel.RiderTrakingInfo;
                var riderInfoHtml = RenderPartialViewToString("~/Themes/" + themeName + "/Views/Order/_TrackingRiderInfo.cshtml", riderInfoModel);

                //Order status progress info
                var statusInfoModel = orderDetailAjaxModel.OrderedTrackingStatuses;
                var orderStatusInfoHtml = RenderPartialViewToString("~/Themes/" + themeName + "/Views/Order/_TrackingOrderStatusInfo.cshtml", statusInfoModel);

                //return with updated html strings
                return Json(new
                {
                    success = true,
                    riderInfoHtml,
                    orderStatusInfoHtml,
                    isOrderDelivered,
                    orderStatus = orderDetailAjaxModel.OrderStatus,
                    message = ""
                });
            }
            catch (Exception exc)
            {
                return Json(new
                {
                    success = false,
                    message = exc.Message
                });
            }
        }

        #endregion

        #region TrackLatestOrderBlock

        /// <summary>
        /// This Ajax call will use to update Order status in TrackLatestOrderBlock in realtime 
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public virtual IActionResult GetLatestTrackingOrderDetail(int orderId)
        {
            if (orderId == 0)
            {
                return Json(new
                {
                    success = false,
                    message = "Order identifier is 0"
                });
            }

            try
            {
                var order = _orderService.GetOrderById(orderId);
                if (order == null)
                {
                    return Json(new
                    {
                        success = false,
                        message = "Order not found!"
                    });
                }

                //return with order status
                return Json(new
                {
                    success = true,
                    orderStatus = _localizationService.GetLocalizedEnum(order.OrderStatus),
                    message = ""
                });
            }
            catch (Exception exc)
            {
                return Json(new
                {
                    success = false,
                    message = exc.Message
                });
            }
        }

        #endregion

        #endregion
    }
}