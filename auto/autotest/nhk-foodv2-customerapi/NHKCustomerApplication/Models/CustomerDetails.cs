﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class CustomerDetails
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Imei1 { get; set; }
        public string Imei2 { get; set; }
        public string DeviceNo { get; set; }
        public string DeviceToken { get; set; }
        public string LatPos { get; set; }
        public string LongPos { get; set; }
        public string Otp { get; set; }
        public int? Otpfailed { get; set; }
        public DateTime? OtpfailedLastDt { get; set; }
        public DateTime? OtpresendLastDt { get; set; }
        public int? OtpresendCount { get; set; }
        public DateTime? CrDt { get; set; }
        public string DeviceType { get; set; }
    }
}
