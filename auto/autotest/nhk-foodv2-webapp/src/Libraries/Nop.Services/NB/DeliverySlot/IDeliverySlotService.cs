﻿using Nop.Core;
using Nop.Core.Domain.NB;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System;
using System.Collections.Generic;

namespace Nop.Services.NB
{
    /// <summary>
    /// DeliverySlot Property service interface
    /// </summary>
    public partial interface IDeliverySlotService
    {
        #region DeliverySlot

        /// <summary>
        /// Gets a DeliverySlot  
        /// </summary>
        /// <param name="DeliverySlotId">DeliverySlot  identifier</param>
        /// <returns>MerchantCategory </returns>
        DeliverySlot GetDeliverySlotById(int deliverySlotId);

        /// <summary>
        /// Gets a DeliverySlot 
        /// </summary>
        /// <param name="storeId">storeId  identifier</param>
        /// <param name="searchName">searchName  identifier</param>
        /// <param name="duration">duration  identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="merchantId">merchant identifier</param>
        /// <param name="isDefaultSlot">isDefaultSlot identifier</param>
        /// <returns>DeliverySlot </returns>
        IPagedList<DeliverySlot> GetAllDeliverySlot(int storeId = 0, string searchName = null, int duration = 0, int pageIndex = 0, int pageSize = int.MaxValue, int merchantId = 0, bool isDefaultSlot = true);

        /// <summary>
        /// Inserts a DeliverySlot
        /// </summary>
        /// <param name="DeliverySlot">DeliverySlot </param>
        void InsertDeliverySlot(DeliverySlot deliverySlot);

        /// <summary>
        /// Updates the DeliverySlot 
        /// </summary>
        /// <param name="DeliverySlot">DeliverySlot </param>
        void UpdateDeliverySlot(DeliverySlot deliverySlot);

        /// <summary>
        /// Delete a DeliverySlot
        /// </summary>
        /// <param name="DeliverySlot">DeliverySlot</param>
        void DeleteDeliverySlot(DeliverySlot deliverySlot);

        /// <summary>
        /// Gets a DeliverySlot  
        /// </summary>
        /// <param name="DeliverySlotIds">DeliverySlot  identifier</param>
        /// <returns>DeliverySlot list</returns>
        IList<DeliverySlot> GetDeliverySlotByIds(int[] deliverySlotIds);

        /// <summary>
        /// Check Same Slot Exists  
        /// </summary>
        /// <param name="startDateUtc">startDateUtc  identifier</param>
        /// <param name="endDateUtc">endDateUtc  identifier</param>
        /// <param name="storeId">storeId  identifier</param>
        /// <param name="merchantId">merchantId  identifier</param>
        /// <param name="id">id  identifier</param>
        /// <returns>DeliverySlot list</returns>
        bool CheckSameSlotExists(DateTime startDateUtc, DateTime endDateUtc, int storeId, int merchantId, int id);

        /// <summary>
        /// Gets a DeliverySlot  
        /// </summary>
        /// <param name="deliverySlotBookingId">deliverySlotBookingId  identifier</param>
        /// <returns>DeliverySlot </returns>
        DeliverySlot GetDeliverySlotByDeliverySlotBookingId(int deliverySlotBookingId);

        /// <summary>
        /// Gets a DeliverySlotBooking  
        /// </summary>
        /// <param name="DeliverySlotBookingId">DeliverySlotBooking  identifier</param>
        /// <returns>DeliverySlotBooking </returns>
        DeliverySlotBooking GetDeliverySlotBookingById(int deliverySlotBookingId);

        /// <summary>
        /// Gets a DeliverySlotBooking  
        /// </summary>
        /// <param name="orderId">orderId  identifier</param>
        /// <returns>DeliverySlotBooking </returns>
        DeliverySlotBooking GetDeliverySlotBookingByOrderId(int orderId);

        /// <summary>
        /// Gets a DeliverySlotBooking 
        /// </summary>
        /// <param name="slotId">slotId  identifier</param>
        /// <param name="orderId">orderId  identifier</param>
        /// <param name="storeId">storeId  identifier</param>
        /// <param name="customerId">customerId  identifier</param>
        /// <param name="deliveryDate">deliveryDate  identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>DeliverySlotBooking </returns>
        IPagedList<DeliverySlotBooking> GetAllDeliverySlotsBooking(int slotId = 0, int orderId = 0, int storeId = 0, int customerId = 0, DateTime? deliveryDate = null, int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Inserts a DeliverySlotBooking
        /// </summary>
        /// <param name="DeliverySlotBooking">DeliverySlotBooking </param>
        void InsertDeliverySlotBooking(DeliverySlotBooking deliverySlotBooking);

        /// <summary>
        /// Updates the DeliverySlotBooking 
        /// </summary>
        /// <param name="DeliverySlotBooking">DeliverySlotBooking </param>
        void UpdateDeliverySlotBooking(DeliverySlotBooking deliverySlotBooking);

        /// <summary>
        /// Inserts a product DeliverySlot mapping
        /// </summary>
        /// <param name="productDeliverySlot">>Product DeliverySlot mapping</param>
        void InsertProductDeliverySlot(ProductDeliverySlotMapping productDeliverySlotMapping);

        /// <summary>
        /// Deletes a product DeliverySlot mapping
        /// </summary>
        /// <param name="productDeliverySlot">Product deliverySlot</param>
        void DeleteProductDeliverySlot(ProductDeliverySlotMapping productDeliverySlotMapping);

        /// <summary>
        /// Gets a product DeliverySlot mapping collection
        /// </summary>
        /// <param name="productId">Product identifier</param>
        /// <returns>Product DeliverySlot mapping collection</returns>
        IList<ProductDeliverySlotMapping> GetProductDeliverySlotsByProductId(int productId);

        /// <summary>
        /// Returns a ProductDeliverySlotMapping that has the specified values
        /// </summary>
        /// <param name="source">Source</param>
        /// <param name="productId">Product identifier</param>
        /// <param name="deliverySlotId">deliverySlot identifier</param>
        /// <returns>A ProductDeliverySlotMapping that has the specified values; otherwise null</returns>
        ProductDeliverySlotMapping FindProductDeliverySlot(IList<ProductDeliverySlotMapping> source, int productId, int deliverySlotId);

        /// <summary>
        /// Gets a DeliverySlot mapping collection
        /// </summary>
        /// <param name="productId">deliverySlot identifier</param>
        /// <returns>Product DeliverySlot mapping collection</returns>
        IList<ProductDeliverySlotMapping> GetProductDeliverySlotsByDeliverySlotId(int deliverySlotId);

        /// <summary>
        /// Any Slot Availble To Merchant 
        /// </summary>
        /// <param name="storeId">storeId  identifier</param>
        /// <param name="merchantId">merchant identifier</param>
        /// <returns>status </returns>
        bool AnySlotAvailbleToMerchant(int storeId = 0, int merchantId = 0);

        #endregion
    }
}
