﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Areas.Admin.Models.Settings
{
    public partial class StoreSetupModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.StoreSetup.IsAgentInfoEnable")]
        public bool IsAssignAgentEnable { get; set; }
        public bool IsAssignAgentEnable_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.StoreSetup.AutofilterGoogleKey")]
        public string AutofilterGoogleKey { get; set; }
        public bool AutofilterGoogleKey_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.StoreSetup.autofiltergoogleflag")]
        public bool autofiltergoogleflag { get; set; }
        public bool autofiltergoogleflag_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.StoreSetup.autofiltergooglecntname")]
        public string autofiltergooglecntname { get; set; }
        public bool autofiltergooglecntname_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.StoreSetup.IsReportBugOnline")]
        public bool IsReportBugOnline { get; set; }
        public bool IsReportBugOnline_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.StoreSetup.ReportBugKey")]
        public string ReportBugKey { get; set; }
        public bool ReportBugKey_OverrideForStore{ get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.StoreSetup.NotificationIOSKey")]
        public string NotificationIOSKey { get; set; }
        public bool NotificationIOSKey_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.StoreSetup.NotificationWebKey")]
        public string NotificationWebKey { get; set; }
        public bool NotificationWebKey_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.StoreSetup.NotificationAndroidKey")]
        public string NotificationAndroidKey { get; set; }
        public bool NotificationAndroidKey_OverrideForStore { get; set; }

        public bool FlatAmount_OverrideForStore { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.ServiceChargePercentage")]
        public decimal ServiceChargePercentage { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.FlatAmount")]
        public decimal FlatAmount { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.MaximumServiceChargeAmount")]
        public decimal MaximumServiceChargeAmount { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.UsePercentage")]
        public bool UsePercentage { get; set; }
        public string PrimaryStoreCurrencyCode { get; set; }
        public bool StoreOwnerProfitFlatAmount_OverrideForStore { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.StoreOwnerProfitServiceChargePercentage")]
        public decimal StoreOwnerProfitServiceChargePercentage { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.StoreOwnerProfitFlatAmount")]
        public decimal StoreOwnerProfitFlatAmount { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.StoreOwnerProfitMaximumServiceChargeAmount")]
        public decimal StoreOwnerProfitMaximumServiceChargeAmount { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.StoreOwnerProfitUsePercentage")]
        public bool StoreOwnerProfitUsePercentage { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.AgentRegisterAllow")]
        public bool AgentRegisterAllow { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.EnableMinOrderAmount")]
        public bool EnableMinOrderAmount { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.UseTwilio")]
        public bool UseTwilio { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.TwilioSenderId")]
        public string TwilioSenderId { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.TwilioAuthtoken")]
        public string TwilioAuthtoken { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.TwilioAccountsId")]
        public string TwilioAccountsId { get; set; }
        
        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.MashastraUserId")]
        public string MashastraUserId { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.MashastraPassword")]
        public string MashastraPassword { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.MashastraSenderId")]
        public string MashastraSenderId { get; set; }
        
        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.GoogleAccountKey")]
        public string GoogleAccountKey { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.NotificationUrl")]
        public string NotificationUrl { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.WebUrl")]
        public string WebUrl { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.CustomerApiNotificationKey")]
        public string CustomerApiNotificationKey { get; set; }

        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.NotificationPushUrl")]
        public string NotificationPushUrl { get; set; }
        public bool IsAdmin { get; set; }
        public bool StoreFront { get; set; }

        //adding the stripe Id for the using of account and billing section 
        [NopResourceDisplayName("NB.Admin.Configuration.Settings.StoreSetup.AccountBillingStripeApiKey")]
        public string AccountBillingStripeApiKey { get; set; }
    }
}