﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nestorbird.Plugin.Api.RequestModel.Account;
using Nestorbird.Plugin.Api.ResponseModel.Account;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Vendors;

namespace Nestorbird.Plugin.Api.Controllers
{
    [Route("merchantapi/account")]
    public class MerchantAccountController : BaseAPIController
    {
        #region Fields

        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly ICustomerService _customerService;
        private readonly ILocalizationService _localizationService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IPictureService _pictureService;
        private readonly ILogger _logger;
        private readonly IVendorService _vendorService;

        private readonly CustomerSettings _customerSettings;

        #endregion

        #region Ctor

        public MerchantAccountController(ICustomerRegistrationService customerRegistrationService,
            ICustomerService customerService,
            ILocalizationService localizationService,
            IGenericAttributeService genericAttributeService,
            IStoreContext storeContext,
            IWorkflowMessageService workflowMessageService,
            ILanguageService languageService,
            IPictureService pictureService,
            CustomerSettings customerSettings,
            ILogger logger,
            IVendorService vendorService) : base(languageService)
        {
            _customerRegistrationService = customerRegistrationService;
            _customerService = customerService;
            _localizationService = localizationService;
            _genericAttributeService = genericAttributeService;
            _storeContext = storeContext;
            _workflowMessageService = workflowMessageService;
            _pictureService = pictureService;
            _logger = logger;
            _customerSettings = customerSettings;
            _vendorService = vendorService;
        }

        #endregion

        #region Methods

        [HttpPost]
        [Route("login")]
        [ProducesResponseType(typeof(LoginResponse), StatusCodes.Status200OK)]
        public IActionResult Login(LoginRequest request)
        {
            if (request == null)
            {
                return Error("Please provide proper request");
            }

            if (string.IsNullOrEmpty(request.Email))
            {
                return Error("Please provide proper email");
            }

            if (string.IsNullOrEmpty(request.Password))
            {
                return Error("Please provide proper password");
            }

            var loginResult = _customerRegistrationService.ValidateCustomer(request.Email, request.Password);
            switch (loginResult)
            {
                case CustomerLoginResults.Successful:
                    {
                        var customer = _customerSettings.UsernamesEnabled
                            ? _customerService.GetCustomerByUsername(request.Email)
                            : _customerService.GetCustomerByEmail(request.Email);

                        if (!customer.IsVendor())
                        {
                            return Error("Customer is not vendor");
                        }

                        if (!string.IsNullOrEmpty(request.DeviceToken) && customer.VendorId > 0)
                        {
                            var vendor = _vendorService.GetVendorById(customer.VendorId);
                            _genericAttributeService.SaveAttribute(customer, NestorbirdAPIPlugin.DeviceTokenAttribute, request.DeviceToken, vendor.StoreId);
                        }

                        var loginResponse = GetCustomerDetails(customer);
                        return Success(loginResponse, "You have successfully logged in");
                    }
                case CustomerLoginResults.CustomerNotExist:
                    return Error(_localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist", CurrentLanguageId));
                case CustomerLoginResults.Deleted:
                    return Error(_localizationService.GetResource("Account.Login.WrongCredentials.Deleted", CurrentLanguageId));
                case CustomerLoginResults.NotActive:
                    return Error(_localizationService.GetResource("Account.Login.WrongCredentials.NotActive", CurrentLanguageId));
                case CustomerLoginResults.NotRegistered:
                    return Error(_localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered", CurrentLanguageId));
                case CustomerLoginResults.LockedOut:
                    return Error(_localizationService.GetResource("Account.Login.WrongCredentials.LockedOut", CurrentLanguageId));
                case CustomerLoginResults.WrongPassword:
                default:
                    return Error(_localizationService.GetResource("Account.Login.WrongCredentials", CurrentLanguageId));
            }
        }

        private LoginResponse GetCustomerDetails(Customer customer)
        {
            var vendorLanguageId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.LanguageIdAttribute,
                            _storeContext.CurrentStore.Id);

            var customerAvatar = _pictureService.GetPictureUrl(
                        _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AvatarPictureIdAttribute),
                        500, false);

            var loginResponse = new LoginResponse
            {
                CustomerId = customer.Id,
                LanguageId = vendorLanguageId,
                FullName = _customerService.GetCustomerFullName(customer),
                FirstName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute, (customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0)),
                LastName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute, (customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0)),
                PhoneNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute, (customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0)),
                Email = customer.Email,
                VendorId = customer.VendorId,
                ImageUrl = customerAvatar
            };

            return loginResponse;
        }

        [HttpPost]
        [Route("passwordrecovery")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public IActionResult PasswordRecovery(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return Error("Please enter email address");
            }

            var customer = _customerService.GetCustomerByEmail(email);
            if (customer != null && customer.Active && !customer.Deleted)
            {
                //save token and current date
                var passwordRecoveryToken = Guid.NewGuid();
                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PasswordRecoveryTokenAttribute,
                    passwordRecoveryToken.ToString());
                DateTime? generatedDateTime = DateTime.UtcNow;
                _genericAttributeService.SaveAttribute(customer,
                    NopCustomerDefaults.PasswordRecoveryTokenDateGeneratedAttribute, generatedDateTime);

                //send email
                _workflowMessageService.SendCustomerPasswordRecoveryMessage(customer, CurrentLanguageId);
                return Success("", _localizationService.GetResource("Account.PasswordRecovery.EmailHasBeenSent", CurrentLanguageId));
            }
            else
            {
                return Error(_localizationService.GetResource("Account.PasswordRecovery.EmailNotFound", CurrentLanguageId));
            }
        }

        [HttpPost]
        [Route("changepassword")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public IActionResult ChangePassword(ChangePasswordApiRequest request)
        {
            if (request == null)
                return Error("Please provide proper request");

            if (request.CustomerId <= 0)
                return Error("Please provide customer id");

            if (string.IsNullOrEmpty(request.OldPassword))
                return Error("Please enter old password");

            if (string.IsNullOrEmpty(request.NewPassword))
                return Error("Please enter new password");

            var customer = _customerService.GetCustomerById(request.CustomerId);

            var changePasswordRequest = new ChangePasswordRequest(customer.Email,
                   true, _customerSettings.DefaultPasswordFormat, request.NewPassword, request.OldPassword);

            var changePasswordResult = _customerRegistrationService.ChangePassword(changePasswordRequest);
            if (changePasswordResult.Success)
            {
                return Success("", _localizationService.GetResource("Account.ChangePassword.Success", CurrentLanguageId));
            }

            var errorMessage = string.Join(",", changePasswordResult.Errors);
            return Error(errorMessage);
        }

        [HttpPost]
        [Route("updateprofile")]
        [ProducesResponseType(typeof(LoginResponse), StatusCodes.Status200OK)]
        public IActionResult UpdateProfile(ProfileUpdateRequest request)
        {
            if (request == null)
                return Error("Please provide proper request");

            if (request.CustomerId <= 0)
                return Error("Please provide customer id");

            var customer = _customerService.GetCustomerById(request.CustomerId);
            if (customer == null)
                return Error("Please provide proper customer id");

            if (!string.IsNullOrEmpty(request.Email))
                customer.Email = request.Email;

            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FirstNameAttribute, request.FirstName, (customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0));
            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.LastNameAttribute, request.LastName, (customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0));
            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, request.PhoneNumber, (customer.RegisteredInStoreId > 0 ? customer.RegisteredInStoreId : 0));

            _customerService.UpdateCustomer(customer);
            UpdatePicture(customer, request.ImageBase64, request.ImageName, request.RemoveImage);

            var customerAvatar = _pictureService.GetPictureUrl(
                        _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AvatarPictureIdAttribute),
                        500, false);

            var response = GetCustomerDetails(customer);
            return Success(response, "Customer updated successfully");
        }

        private void UpdatePicture(Customer customer, string imageBase64, string imageName, bool removeImage)
        {
            var pictureId = SavePicture(imageBase64, customer.Id + imageName);
            if (pictureId > 0)
            {
                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AvatarPictureIdAttribute, pictureId);
            }
            else if (removeImage)
            {
                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AvatarPictureIdAttribute, 0);
            }            
        }

        #endregion
    }
}
