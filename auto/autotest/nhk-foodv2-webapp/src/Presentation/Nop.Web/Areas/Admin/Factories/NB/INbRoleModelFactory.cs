﻿using Nop.Core.Domain.Customers;
using Nop.Web.Areas.Admin.Models.Customers;

namespace Nop.Web.Areas.Admin.Factories.NB
{
    /// <summary>
    /// Represents the nb role model factory
    /// </summary>
    public interface INbRoleModelFactory
    {
        /// <summary>
        /// Prepare nb role search model
        /// </summary>
        /// <param name="searchModel">nb role search model</param>
        /// <returns>nb role search model</returns>
        CustomerRoleSearchModel PrepareNBRoleSearchModel(CustomerRoleSearchModel searchModel);

        /// <summary>
        /// Prepare paged nb role list model
        /// </summary>
        /// <param name="searchModel">nb role search model</param>
        /// <returns>nb role list model</returns>
        CustomerRoleListModel PrepareNBRoleListModel(CustomerRoleSearchModel searchModel);

        /// <summary>
        /// Prepare nb role model
        /// </summary>
        /// <param name="model">nb role model</param>
        /// <param name="customerRole">nb role</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>nb role model</returns>
        CustomerRoleModel PrepareNBRoleModel(CustomerRoleModel model, CustomerRole customerRole, bool excludeProperties = false);
    }
}
