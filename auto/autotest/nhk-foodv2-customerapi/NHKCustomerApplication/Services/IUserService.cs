﻿using NHKCustomerApplication.ViewModels;

namespace NHKCustomerApplication.Services
{
    /// <summary>
    /// User service.
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// RegisterCustomer.
        /// </summary>
        /// <param name="customer">CustomerModel object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        CustomerAPIResponses RegisterCustomer(CustomerModel customer);

        /// <summary>
        /// ExternalAuthModel.
        /// </summary>
        /// <param name="customer">ExternalAuthModel object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        CustomerAPIResponses ExternalAuth(ExternalAuthModel customer);

        /// <summary>
        /// Create guest user.
        /// </summary>
        /// <param name="customer">GuestUserModel object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        CustomerAPIResponses CreateGuestUser(GuestUserModel customer);

        /// <summary>
        /// CustomerLogin.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerLoginResponseModelV3 CustomerLoginV3(CustomerLoginV3 customer);
        CustomerAPIResponses CustomerLoginV2_2(CustomerLogin customer);


        /// <summary>
        /// RegistaionVerificationCustomer.
        /// </summary>
        /// <param name="customer">RegistationverificationCustomer object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        CustomerAPIResponses RegistaionVerificationCustomerV3(RegistationverificationCustomer customer);


        /// <summary>
        /// RegistaionVerificationCustomer.
        /// </summary>
        /// <param name="customer">RegistationverificationCustomer object.</param>
        /// <returns>CustomerAPIResponses object.</returns>
        CustomerAPIResponses RegistaionVerificationCustomer(RegistationverificationCustomer customer);

        /// <summary>
        /// CustomerLogin.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerAPIResponses CustomerLogin(CustomerLogin customer);

        /// <summary>
        /// CustomerLogin.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerAPIResponses CustomerLoginV2_1(CustomerLoginV2_1 customer);

        /// <summary>
        /// GetCooknRestItemsByRestnCookId.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerAPIResponses GetCooknRestItemsByRestnCookId(CooknRestProductByID customer);
    }
}
