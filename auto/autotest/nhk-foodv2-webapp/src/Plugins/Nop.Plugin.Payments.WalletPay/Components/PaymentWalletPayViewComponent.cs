﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Payments.WalletPay.Models;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.NB;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Payments.WalletPay.Components
{
    [ViewComponent(Name = WalletPayDefaults.PAYMENT_INFO_VIEW_COMPONENT_NAME)]
    public class PaymentWalletPayViewComponent : NopViewComponent
    {
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly IWalletDetailsService _walletService;
        private readonly IPriceFormatter _priceFormatter;
        public PaymentWalletPayViewComponent(ISettingService settingService,
                        IWorkContext workContext,
            IWalletDetailsService walletService,
            IPriceFormatter priceFormatter,
            IStoreContext storeContext)
        {
            _priceFormatter = priceFormatter;
            _walletService = walletService;
            _workContext = workContext;
            _settingService = settingService;
            _storeContext = storeContext;
        }

        public IViewComponentResult Invoke()
        {
            var walletPayPaymentSettings = _settingService.LoadSetting<WalletPayPaymentSettings>(_storeContext.CurrentStore.Id);
            var userBalance = _walletService.GetWalletBalance(_workContext.CurrentCustomer.Id);           
           
            var model = new PaymentInfoModel
            {
                WalletBallance = _priceFormatter.FormatPrice(userBalance,true,false),
                
                DescriptionText = walletPayPaymentSettings.DescriptionText
            };

            return View("~/Plugins/Payments.WalletPay/Views/PaymentInfo.cshtml", model);
        }
    }
}
