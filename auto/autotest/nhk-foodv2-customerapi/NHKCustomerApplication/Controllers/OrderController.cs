﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NHKCustomerApplication.Caching;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Services;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace NHKCustomerApplication.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class OrderController : ControllerBase
	{

		private readonly string googleMapKey = "key=AIzaSyClQteKHAPw20u9I8FQZaHvnH_4yg0Lph8";
		//private  string ChefBaseUrl = string.Empty;
		private string connectionString = string.Empty;
		private ecuadordevContext dbcontext;
		private readonly IConfiguration _configuration;
		private readonly ICacheManager _cacheManager;
		private readonly IOrderServices _orderServices;
		public OrderController(ecuadordevContext context, ICacheManager cacheManager, IConfiguration configuration,
							   IOrderServices orderServices)
		{
			dbcontext = context;
			_configuration = configuration;
			connectionString = _configuration.GetSection("ConnectionStrings").GetSection("ecuadordatabase").Value;
			_cacheManager = cacheManager;
			_orderServices = orderServices;
		}
		[Route("~/api/v1/checkoutcartproducts")]
		[HttpPost]
		public CustomerAPIResponses CheckoutCartProductsV3([FromBody] ChekoutCartProductsModelV3 cartProductsModel)
		{

			int languageId = LanguageHelper.GetIdByLangCode(cartProductsModel.StoreId, cartProductsModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (cartProductsModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(cartProductsModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (cartProductsModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (cartProductsModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (cartProductsModel.IsDelivery)
			{
				if (cartProductsModel.DeliveryAddressId == 0)
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.DelieveryAddressMissing", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}



			try
			{
				CheckoutProductCartResponseModel checkoutProductCartResponseModel = new CheckoutProductCartResponseModel();
				if (!string.IsNullOrEmpty(cartProductsModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == cartProductsModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				var itemsinShoppingCartExist = dbcontext.ShoppingCartItem.Where(x => x.CustomerId == cartProductsModel.CustomerId && x.StoreId == cartProductsModel.StoreId && x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).Any();
				if (!itemsinShoppingCartExist)
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.ShoppingCartEmpty", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
				var isProductBelongTosameChef = dbcontext.ShoppingCartItem.Where(x => x.StoreId == cartProductsModel.StoreId && x.CustomerId == cartProductsModel.CustomerId && x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).FirstOrDefault();
				if (isProductBelongTosameChef != null)
				{
					var VendorBelongtoProduct = (from pro in dbcontext.Product
												 where pro.Id == isProductBelongTosameChef.ProductId
												 select new
												 {
													 pro.VendorId
												 }).FirstOrDefault();
					cartProductsModel.RestnCookId = VendorBelongtoProduct.VendorId;
				}
				if (cartProductsModel.IsDinning)
				{
					var isDiningProvided = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId && x.IsDining == true).Any();
					if (!isDiningProvided)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CheckOutNotPossibleForDinning", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (cartProductsModel.IsDelivery)
				{
					var isDiningProvided = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId && x.IsDelivery == true).Any();
					if (!isDiningProvided)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CheckOutNotPossibleForDelievery", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (cartProductsModel.IsTakeAway)
				{
					var isDiningProvided = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId && x.IsTakeAway == true).Any();
					if (!isDiningProvided)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CheckOutNotPossibleForTakeAway", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (cartProductsModel.IsDelivery)
				{
					//var ProductsToCheckout = dbcontext.ShoppingCartItems.Where(x=>x.CustomerId==cartProductsModel.CustomerId && x.StoreId==cartProductsModel.StoreId).ToList();

					var vendorOrder = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId).FirstOrDefault();
					if (vendorOrder != null)
					{
						var addressTobeDelivered = dbcontext.Address.Where(x => x.Id == cartProductsModel.DeliveryAddressId).FirstOrDefault();
						string strsql = "select id from vendor where Id=" + cartProductsModel.RestnCookId + " and storeid=" + cartProductsModel.StoreId + " and geofancing is not null and geofancing like '%poly%' and isdelivery=1";
						strsql += " and cast(geometry::STGeomFromText(cast((replace(replace(replace(replace(replace(replace(replace(geofancing,";
						strsql += "char(34) + 'coordinates' + char(34),''),";
						strsql += "char(34) + 'type'+ char(34) +':',''),'' + char(34) +',:[{'+ char(34) +'lat'+ char(34) +':','((') ,'{'+ char(34) +'P','P'),','+ char(34) +'lng'+ char(34) +':',' '),'},{'+ char(34) +'lat'+ char(34) +':',', '),'}]}','))') ) AS varchar(max)) ,0) as geometry)";
						strsql += ".STContains(cast(geometry::STGeomFromText('POINT(" + addressTobeDelivered.Latitude + " " + addressTobeDelivered.Longitude + ")', 0) As geometry)) = 1";
						using (SqlConnection connection = new SqlConnection(connectionString))
						{
							SqlCommand command = new SqlCommand(strsql, connection);
							connection.Open();
							SqlDataReader reader = command.ExecuteReader();
							try
							{
								if (!reader.HasRows)
								{
									connection.Close();
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.AddressToFar", languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							finally
							{
								// Always call Close when done reading.
								connection.Close();
							}
						}
						if (vendorOrder.AvailableType != null && vendorOrder.AvailableType != 0)
						{
							var itemFlag = false;
							if (vendorOrder.AvailableType == (int)AvailableTypeEnum.AllDay)
							{
								var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == vendorOrder.Id && x.AvailableType == (int)AvailableTypeEnum.AllDay).ToList();
								if (AllDaySchedulesTimimgs.Any())
								{
									//var CurrentTime = DateTime.Now.TOS;
									var CurrentTime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).TimeOfDay;
									foreach (var __item in AllDaySchedulesTimimgs)
									{
										if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
										{
											itemFlag = true;
										}


									}
								}
								else
								{
									itemFlag = true;
								}

							}
							else if (vendorOrder.AvailableType == (int)AvailableTypeEnum.Custom)
							{
								var DayOfWeek = (int)Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).DayOfWeek;
								var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == vendorOrder.Id && x.AvailableType == (int)AvailableTypeEnum.Custom && x.AvailableOn == DayOfWeek).ToList();
								if (AllDaySchedulesTimimgs.Any())
								{
									//var CurrentTime = DateTime.Now.TOS;
									var CurrentTime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).TimeOfDay;
									foreach (var __item in AllDaySchedulesTimimgs)
									{
										if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
										{
											itemFlag = true;
										}


									}
								}
								else
								{
									itemFlag = true;
								}
							}
							else
							{
								itemFlag = true;
							}
							if (!itemFlag)
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.RestaturantClosed", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}

						}


					}
					else
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.VendorAddressNotValid", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}

				}
				var ProductsToCheckout = dbcontext.ShoppingCartItem.Where(x => x.CustomerId == cartProductsModel.CustomerId && x.StoreId == cartProductsModel.StoreId && x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).ToList();
				Order order = new Order();
				order.OrderGuid = Guid.NewGuid();
				order.StoreId = cartProductsModel.StoreId;
				string CurrencyCode = Helper.GetStoreCurrencyCode(cartProductsModel.StoreId, dbcontext);
				order.CustomerId = cartProductsModel.CustomerId;
				if (cartProductsModel.IsDelivery)
				{
					order.ShippingAddressId = cartProductsModel.DeliveryAddressId;
					order.BillingAddressId = cartProductsModel.DeliveryAddressId;
					order.PickupInStore = false;

				}
				else if (cartProductsModel.IsTakeAway)
				{
					order.BillingAddressId = cartProductsModel.BillingAddressId;
					order.PickupInStore = true;
				}
				else if (cartProductsModel.IsDinning)
				{
					order.BillingAddressId = cartProductsModel.BillingAddressId;
				}
				order.OrderStatusId = (int)OrderHistoryEnum.Cancelled;
				order.ShippingStatusId = (int)ShippingStatus.NotYetShipped;
				order.PaymentStatusId = (int)PaymentStatus.Pending;
				order.PaymentMethodSystemName = "Payments.Verifone";
				//var vendorId = 
				order.PickupAddressId = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId).FirstOrDefault().AddressId;
				order.CustomerCurrencyCode = CurrencyCode;
				var currencyRate = dbcontext.Currency.Where(x => x.CurrencyCode.Contains(CurrencyCode)).FirstOrDefault();
				if (currencyRate != null)
				{
					order.CurrencyRate = currencyRate.Rate;
				}
				else
				{
					order.CurrencyRate = 1;
				}
				order.CustomerTaxDisplayTypeId = (int)TaxDisplayType.IncludingTax;
				order.OrderSubtotalExclTax = Convert.ToDecimal(cartProductsModel.Subtotal);
				order.OrderSubtotalInclTax = Convert.ToDecimal(cartProductsModel.Subtotal);
				order.OrderSubTotalDiscountInclTax = decimal.Zero;
				order.OrderSubTotalDiscountExclTax = decimal.Zero;
				order.OrderShippingInclTax = 0;
				order.OrderShippingExclTax = 0;
				order.PaymentMethodAdditionalFeeInclTax = decimal.Zero;
				order.PaymentMethodAdditionalFeeExclTax = decimal.Zero;
				order.TaxRates = "0:0;";
				order.OrderDiscount = Convert.ToDecimal(cartProductsModel.DiscountAmount);
				order.OrderTax = Convert.ToDecimal(cartProductsModel.Tax);
				order.OrderTotal = Convert.ToDecimal(cartProductsModel.Total);
				order.RefundedAmount = 0;
				order.CustomerLanguageId = 1;
				order.AffiliateId = 0;
				order.CustomerIp = "127.0.0.1";
				order.AllowStoringCreditCardNumber = false;
				order.ShippingMethod = "Ground";
				order.ShippingRateComputationMethodSystemName = "Shipping.FixedOrByWeight";
				order.Deleted = false;
				order.CreatedOnUtc = DateTime.UtcNow;
				order.Tip = Convert.ToDecimal(cartProductsModel.Tip);
				order.DeliveryAmount = Convert.ToDecimal(cartProductsModel.DeliveryCharges);
				order.CustomOrderNumber = (dbcontext.Order.Count() + 1).ToString();
				dbcontext.Order.Add(order);
				dbcontext.SaveChanges();
				if (!string.IsNullOrEmpty(cartProductsModel.CouponCode))
				{
					var isCouponCodeExist = (from x in dbcontext.Discount.AsEnumerable()
											 join jn in dbcontext.StoreMapping.AsEnumerable()
											 on x.Id equals jn.EntityId
											 where jn.StoreId == cartProductsModel.StoreId && jn.EntityName == "Discount"
											 && (x.CouponCode ?? "").ToUpper().ToString() == cartProductsModel.CouponCode.ToUpper().ToString()
											 && x.DiscountTypeId == (int)DiscountType.AssignedToOrderTotal && x.RequiresCouponCode == true
											 select x).FirstOrDefault();
					DiscountUsageHistory discountUsageHistory = new DiscountUsageHistory();
					discountUsageHistory.CreatedOnUtc = DateTime.UtcNow;
					discountUsageHistory.OrderId = order.Id;
					if (isCouponCodeExist != null)
					{
						discountUsageHistory.DiscountId = isCouponCodeExist != null ? isCouponCodeExist.Id : 0;
						dbcontext.DiscountUsageHistory.Add(discountUsageHistory);
						dbcontext.SaveChanges();
					}

				}
				//OrderDetail orderDetail = new OrderDetail();
				//orderDetail.OrderNote = cartProductsModel.AdditionalComments;
				//orderDetail.StoreId = cartProductsModel.StoreId;
				//orderDetail.CreatedOnUtc = DateTime.Now;
				//orderDetail.CustomerId = cartProductsModel.CustomerId;
				//dbcontext.OrderDetails.Add(orderDetail);
				//dbcontext.SaveChanges();
				//var orderDetails = dbcontext.Orders.Where(x => x.Id == order.Id).FirstOrDefault();

				//orderDetails.OrderDetailId = orderDetail.Id;
				//dbcontext.Entry(orderDetails).State = System.Data.Entity.EntityState.Modified;
				//dbcontext.SaveChanges();
				foreach (var item in ProductsToCheckout)
				{
					var itemPrice = (from p in dbcontext.Product.Where(x => x.Id == item.ProductId)
									 select new
									 {
										 Price = p.TaxCategoryId == 0
																? p.Price : (from priceCategoryTax in dbcontext.Product
																			 join categoryTax in dbcontext.TaxRate
																				 on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
																			 where categoryTax.StoreId == cartProductsModel.StoreId &&
																				 priceCategoryTax.Id == p.Id

																			 select new
																			 {
																				 PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
																			 }).FirstOrDefault().PriceCalculated,
									 }).FirstOrDefault();
					OrderItem orderItem = new OrderItem();

					orderItem.OrderItemGuid = Guid.NewGuid();
					orderItem.OrderId = order.Id;
					orderItem.ProductId = item.ProductId;
					orderItem.Quantity = item.Quantity;
					orderItem.UnitPriceExclTax = itemPrice.Price;
					orderItem.UnitPriceInclTax = itemPrice.Price;
					orderItem.PriceExclTax = (itemPrice.Price * item.Quantity);
					orderItem.PriceInclTax = (itemPrice.Price * item.Quantity);
					orderItem.DiscountAmountExclTax = 0;
					orderItem.DiscountAmountInclTax = 0;
					orderItem.OriginalProductCost = 0;
					orderItem.DownloadCount = 0;
					orderItem.IsDownloadActivated = false;
					orderItem.LicenseDownloadId = 0;
					orderItem.ItemWeight = 0;
					dbcontext.OrderItem.Add(orderItem);
					dbcontext.SaveChanges();
				}

				OrderNote orderNote = new OrderNote();
				orderNote.CreatedOnUtc = DateTime.UtcNow;
				orderNote.DisplayToCustomer = true;
				orderNote.DownloadId = 0;
				orderNote.Note = "Order is incomplete";
				orderNote.OrderStatus = (int)OrderHistoryEnum.Cancelled;
				orderNote.OrderId = order.Id;
				dbcontext.OrderNote.Add(orderNote);
				dbcontext.SaveChanges();
				OrderDetails orderDetailss = new OrderDetails();
				orderDetailss.CreatedOnUtc = DateTime.UtcNow;
				orderDetailss.CustomerId = order.CustomerId;
				orderDetailss.StoreId = order.StoreId;
				if (!string.IsNullOrEmpty(cartProductsModel.Tip))
					orderDetailss.Tip = Convert.ToDecimal(cartProductsModel.Tip);
				if (!string.IsNullOrEmpty(cartProductsModel.DeliveryCharges))
					orderDetailss.DeliveryCharges = Convert.ToDecimal(cartProductsModel.DeliveryCharges);
				orderDetailss.OrderNote = cartProductsModel.AdditionalComments;
				if (cartProductsModel.IsDelivery)
				{
					orderDetailss.OrderType = (int)RestType.Delivery;

				}
				else if (cartProductsModel.IsTakeAway)
				{
					orderDetailss.OrderType = (int)RestType.TakeAway;
				}
				else if (cartProductsModel.IsDinning)
				{
					orderDetailss.OrderType = (int)RestType.Dining;
				}
				//orderDetailss.PackingChargesAmount = cartProductsModel.PackingCharges != null ? Convert.ToDecimal(cartProductsModel.PackingCharges) : 0;
				orderDetailss.IsOrderComplete = true;
				dbcontext.OrderDetails.Add(orderDetailss);
				dbcontext.SaveChanges();

				//Agent notes
				if (!string.IsNullOrEmpty(cartProductsModel.AgentComments))
				{
					OrderNote agentComments = new OrderNote
					{
						Note = "Comments for Agent : " + cartProductsModel.AgentComments,
						OrderStatus = (int)OrderHistoryEnum.Received,
						OrderId = order.Id
					};
					dbcontext.OrderNote.Add(agentComments);
					dbcontext.SaveChanges();
				}

				// update order details id
				var orderToZUpdate = dbcontext.Order.Where(x => x.Id == order.Id).FirstOrDefault();
				orderToZUpdate.OrderDetailId = orderDetailss.Id;
				dbcontext.Entry(orderToZUpdate).State = EntityState.Modified;
				dbcontext.SaveChanges();
				checkoutProductCartResponseModel.OrderId = order.Id;
				checkoutProductCartResponseModel.OrderNumber = order.CustomOrderNumber;
				checkoutProductCartResponseModel.OrderTotal = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(cartProductsModel.Total));
				//String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
				//String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
				//checkoutProductCartResponseModel.CheckoutUrl = strUrl + "Verifone/details/" + order.Id;


				customerAPIResponses.ErrorMessageTitle = "Success!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.ProuctCheckoutSuccessfully", languageId, dbcontext);
				customerAPIResponses.Status = true;
				customerAPIResponses.StatusCode = 200;
				customerAPIResponses.ResponseObj = checkoutProductCartResponseModel;
				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v2.1/checkoutcartproducts")]
		[HttpPost]
		public CustomerAPIResponses CheckoutCartProductsV2_1([FromBody] ChekoutCartProductsModelV3 cartProductsModel)
		{

			int languageId = LanguageHelper.GetIdByLangCode(cartProductsModel.StoreId, cartProductsModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (cartProductsModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(cartProductsModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (cartProductsModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (cartProductsModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (cartProductsModel.IsDelivery)
			{
				if (cartProductsModel.DeliveryAddressId == 0)
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.DelieveryAddressMissing", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}



			try
			{
				CheckoutProductCartResponseModel checkoutProductCartResponseModel = new CheckoutProductCartResponseModel();
				if (!string.IsNullOrEmpty(cartProductsModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == cartProductsModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				var itemsinShoppingCartExist = dbcontext.ShoppingCartItem.Where(x => x.CustomerId == cartProductsModel.CustomerId && x.StoreId == cartProductsModel.StoreId).Any();
				if (!itemsinShoppingCartExist)
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.ShoppingCartEmpty", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
				var isProductBelongTosameChef = dbcontext.ShoppingCartItem.Where(x => x.StoreId == cartProductsModel.StoreId && x.CustomerId == cartProductsModel.CustomerId).FirstOrDefault();
				if (isProductBelongTosameChef != null)
				{
					var VendorBelongtoProduct = (from pro in dbcontext.Product
												 where pro.Id == isProductBelongTosameChef.ProductId
												 select new
												 {
													 pro.VendorId
												 }).FirstOrDefault();
					cartProductsModel.RestnCookId = VendorBelongtoProduct.VendorId;
				}
				if (cartProductsModel.IsDinning)
				{
					var isDiningProvided = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId && x.IsDining == true).Any();
					if (!isDiningProvided)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CheckOutNotPossibleForDinning", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (cartProductsModel.IsDelivery)
				{
					var isDiningProvided = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId && x.IsDelivery == true).Any();
					if (!isDiningProvided)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CheckOutNotPossibleForDelievery", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (cartProductsModel.IsTakeAway)
				{
					var isDiningProvided = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId && x.IsTakeAway == true).Any();
					if (!isDiningProvided)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CheckOutNotPossibleForTakeAway", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (cartProductsModel.IsDelivery)
				{
					//var ProductsToCheckout = dbcontext.ShoppingCartItems.Where(x=>x.CustomerId==cartProductsModel.CustomerId && x.StoreId==cartProductsModel.StoreId).ToList();

					var vendorOrder = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId).FirstOrDefault();
					if (vendorOrder != null)
					{
						var addressTobeDelivered = dbcontext.Address.Where(x => x.Id == cartProductsModel.DeliveryAddressId).FirstOrDefault();
						string strsql = "select id from vendor where Id=" + cartProductsModel.RestnCookId + " and storeid=" + cartProductsModel.StoreId + " and geofancing is not null and geofancing like '%poly%' and isdelivery=1";
						strsql += " and cast(geometry::STGeomFromText(cast((replace(replace(replace(replace(replace(replace(replace(geofancing,";
						strsql += "char(34) + 'coordinates' + char(34),''),";
						strsql += "char(34) + 'type'+ char(34) +':',''),'' + char(34) +',:[{'+ char(34) +'lat'+ char(34) +':','((') ,'{'+ char(34) +'P','P'),','+ char(34) +'lng'+ char(34) +':',' '),'},{'+ char(34) +'lat'+ char(34) +':',', '),'}]}','))') ) AS varchar(max)) ,0) as geometry)";
						strsql += ".STContains(cast(geometry::STGeomFromText('POINT(" + addressTobeDelivered.Latitude + " " + addressTobeDelivered.Longitude + ")', 0) As geometry)) = 1";
						using (SqlConnection connection = new SqlConnection(connectionString))
						{
							SqlCommand command = new SqlCommand(strsql, connection);
							connection.Open();
							SqlDataReader reader = command.ExecuteReader();
							try
							{
								if (!reader.HasRows)
								{
									connection.Close();
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.AddressToFar", languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							finally
							{
								// Always call Close when done reading.
								connection.Close();
							}
						}
						if (vendorOrder.AvailableType != null && vendorOrder.AvailableType != 0)
						{
							var itemFlag = false;
							if (vendorOrder.AvailableType == (int)AvailableTypeEnum.AllDay)
							{
								var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == vendorOrder.Id && x.AvailableType == (int)AvailableTypeEnum.AllDay).ToList();
								if (AllDaySchedulesTimimgs.Any())
								{
									//var CurrentTime = DateTime.Now.TOS;
									var CurrentTime = Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, cartProductsModel.CustomerId).TimeOfDay;
									foreach (var __item in AllDaySchedulesTimimgs)
									{
										if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
										{
											itemFlag = true;
										}


									}
								}
								else
								{
									itemFlag = true;
								}

							}
							else if (vendorOrder.AvailableType == (int)AvailableTypeEnum.Custom)
							{
								var DayOfWeek = (int)Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, cartProductsModel.CustomerId).DayOfWeek;
								var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == vendorOrder.Id && x.AvailableType == (int)AvailableTypeEnum.Custom && x.AvailableOn == DayOfWeek).ToList();
								if (AllDaySchedulesTimimgs.Any())
								{
									//var CurrentTime = DateTime.Now.TOS;
									var CurrentTime = Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, cartProductsModel.CustomerId).TimeOfDay;
									foreach (var __item in AllDaySchedulesTimimgs)
									{
										if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
										{
											itemFlag = true;
										}


									}
								}
								else
								{
									itemFlag = true;
								}
							}
							else
							{
								itemFlag = true;
							}
							if (!itemFlag)
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.RestaturantClosed", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}

						}


					}
					else
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.VendorAddressNotValid", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}

				}
				var ProductsToCheckout = dbcontext.ShoppingCartItem.Where(x => x.CustomerId == cartProductsModel.CustomerId && x.StoreId == cartProductsModel.StoreId).ToList();
				Order order = new Order();
				order.OrderGuid = Guid.NewGuid();
				order.StoreId = cartProductsModel.StoreId;
				string CurrencyCode = Helper.GetStoreCurrencyCode(cartProductsModel.StoreId, dbcontext);
				order.CustomerId = cartProductsModel.CustomerId;
				if (cartProductsModel.IsDelivery)
				{
					order.ShippingAddressId = cartProductsModel.DeliveryAddressId;
					order.BillingAddressId = cartProductsModel.DeliveryAddressId;
					order.PickupInStore = false;

				}
				else if (cartProductsModel.IsTakeAway)
				{
					order.BillingAddressId = cartProductsModel.BillingAddressId;
					order.PickupInStore = true;
				}
				else if (cartProductsModel.IsDinning)
				{
					order.BillingAddressId = cartProductsModel.BillingAddressId;
				}
				order.OrderStatusId = (int)OrderHistoryEnum.Cancelled;
				order.ShippingStatusId = (int)ShippingStatus.NotYetShipped;
				order.PaymentStatusId = (int)PaymentStatus.Pending;
				order.PaymentMethodSystemName = "Payments.Verifone";
				//var vendorId = 
				order.PickupAddressId = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId).FirstOrDefault().AddressId;
				order.CustomerCurrencyCode = CurrencyCode;
				var currencyRate = dbcontext.Currency.Where(x => x.CurrencyCode.Contains(CurrencyCode)).FirstOrDefault();
				if (currencyRate != null)
				{
					order.CurrencyRate = currencyRate.Rate;
				}
				else
				{
					order.CurrencyRate = 1;
				}
				order.CustomerTaxDisplayTypeId = (int)TaxDisplayType.IncludingTax;
				order.OrderSubtotalExclTax = Convert.ToDecimal(cartProductsModel.Subtotal);
				order.OrderSubtotalInclTax = Convert.ToDecimal(cartProductsModel.Subtotal);
				order.OrderSubTotalDiscountInclTax = decimal.Zero;
				order.OrderSubTotalDiscountExclTax = decimal.Zero;
				order.OrderShippingInclTax = 0;
				order.OrderShippingExclTax = 0;
				order.PaymentMethodAdditionalFeeInclTax = decimal.Zero;
				order.PaymentMethodAdditionalFeeExclTax = decimal.Zero;
				order.TaxRates = "0:0;";
				order.OrderDiscount = Convert.ToDecimal(cartProductsModel.DiscountAmount);
				order.OrderTax = Convert.ToDecimal(cartProductsModel.Tax);
				order.OrderTotal = Convert.ToDecimal(cartProductsModel.Total);
				order.RefundedAmount = 0;
				order.CustomerLanguageId = 1;
				order.AffiliateId = 0;
				order.CustomerIp = "127.0.0.1";
				order.AllowStoringCreditCardNumber = false;
				order.ShippingMethod = "Ground";
				order.ShippingRateComputationMethodSystemName = "Shipping.FixedOrByWeight";
				order.Deleted = false;
				order.CreatedOnUtc = DateTime.UtcNow;
				order.Tip = Convert.ToDecimal(cartProductsModel.Tip);
				order.DeliveryAmount = !string.IsNullOrEmpty(cartProductsModel.DeliveryCharges) ? Convert.ToDecimal(cartProductsModel.DeliveryCharges) : 0;
				order.CustomOrderNumber = (dbcontext.Order.Count() + 1).ToString();
				dbcontext.Order.Add(order);
				dbcontext.SaveChanges();
				if (!string.IsNullOrEmpty(cartProductsModel.CouponCode))
				{
					var isCouponCodeExist = (from x in dbcontext.Discount.AsEnumerable()
											 join jn in dbcontext.StoreMapping.AsEnumerable()
											 on x.Id equals jn.EntityId
											 where jn.StoreId == cartProductsModel.StoreId && jn.EntityName == "Discount"
											 && (x.CouponCode ?? "").ToUpper().ToString() == cartProductsModel.CouponCode.ToUpper().ToString()
											 && x.DiscountTypeId == (int)DiscountType.AssignedToOrderTotal && x.RequiresCouponCode == true
											 select x).FirstOrDefault();
					DiscountUsageHistory discountUsageHistory = new DiscountUsageHistory();
					discountUsageHistory.CreatedOnUtc = DateTime.UtcNow;
					discountUsageHistory.OrderId = order.Id;
					if (isCouponCodeExist != null)
					{
						discountUsageHistory.DiscountId = isCouponCodeExist != null ? isCouponCodeExist.Id : 0;
						dbcontext.DiscountUsageHistory.Add(discountUsageHistory);
						dbcontext.SaveChanges();
					}

				}
				//OrderDetail orderDetail = new OrderDetail();
				//orderDetail.OrderNote = cartProductsModel.AdditionalComments;
				//orderDetail.StoreId = cartProductsModel.StoreId;
				//orderDetail.CreatedOnUtc = DateTime.Now;
				//orderDetail.CustomerId = cartProductsModel.CustomerId;
				//dbcontext.OrderDetails.Add(orderDetail);
				//dbcontext.SaveChanges();
				//var orderDetails = dbcontext.Orders.Where(x => x.Id == order.Id).FirstOrDefault();

				//orderDetails.OrderDetailId = orderDetail.Id;
				//dbcontext.Entry(orderDetails).State = System.Data.Entity.EntityState.Modified;
				//dbcontext.SaveChanges();
				foreach (var item in ProductsToCheckout)
				{
					var itemPrice = (from p in dbcontext.Product.Where(x => x.Id == item.ProductId)
									 select new
									 {
										 Price = p.TaxCategoryId == 0
																? p.Price : (from priceCategoryTax in dbcontext.Product
																			 join categoryTax in dbcontext.TaxRate
																				 on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
																			 where categoryTax.StoreId == cartProductsModel.StoreId &&
																				 priceCategoryTax.Id == p.Id

																			 select new
																			 {
																				 PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
																			 }).FirstOrDefault().PriceCalculated,
									 }).FirstOrDefault();
					OrderItem orderItem = new OrderItem();

					orderItem.OrderItemGuid = Guid.NewGuid();
					orderItem.OrderId = order.Id;
					orderItem.ProductId = item.ProductId;
					orderItem.Quantity = item.Quantity;
					orderItem.UnitPriceExclTax = itemPrice.Price;
					orderItem.UnitPriceInclTax = itemPrice.Price;
					orderItem.PriceExclTax = (itemPrice.Price * item.Quantity);
					orderItem.PriceInclTax = (itemPrice.Price * item.Quantity);
					orderItem.DiscountAmountExclTax = 0;
					orderItem.DiscountAmountInclTax = 0;
					orderItem.OriginalProductCost = 0;
					orderItem.DownloadCount = 0;
					orderItem.IsDownloadActivated = false;
					orderItem.LicenseDownloadId = 0;
					orderItem.ItemWeight = 0;
					dbcontext.OrderItem.Add(orderItem);
					dbcontext.SaveChanges();
				}

				OrderNote orderNote = new OrderNote();
				orderNote.CreatedOnUtc = DateTime.UtcNow;
				orderNote.DisplayToCustomer = true;
				orderNote.DownloadId = 0;
				orderNote.Note = "Order is incomplete";
				orderNote.OrderStatus = (int)OrderHistoryEnum.Cancelled;
				orderNote.OrderId = order.Id;
				dbcontext.OrderNote.Add(orderNote);
				dbcontext.SaveChanges();
				OrderDetails orderDetailss = new OrderDetails();
				orderDetailss.CreatedOnUtc = DateTime.UtcNow;
				orderDetailss.CustomerId = order.CustomerId;
				orderDetailss.StoreId = order.StoreId;
				if (!string.IsNullOrEmpty(cartProductsModel.Tip))
					orderDetailss.Tip = Convert.ToDecimal(cartProductsModel.Tip);
				if (!string.IsNullOrEmpty(cartProductsModel.DeliveryCharges))
					orderDetailss.DeliveryCharges = Convert.ToDecimal(cartProductsModel.DeliveryCharges);
				orderDetailss.OrderNote = cartProductsModel.AdditionalComments;
				if (cartProductsModel.IsDelivery)
				{
					orderDetailss.OrderType = (int)RestType.Delivery;

				}
				else if (cartProductsModel.IsTakeAway)
				{
					orderDetailss.OrderType = (int)RestType.TakeAway;
				}
				else if (cartProductsModel.IsDinning)
				{
					orderDetailss.OrderType = (int)RestType.Dining;
				}
				//orderDetailss.PackingChargesAmount = cartProductsModel.PackingCharges != null ? Convert.ToDecimal(cartProductsModel.PackingCharges) : 0;
				orderDetailss.IsOrderComplete = true;
				dbcontext.OrderDetails.Add(orderDetailss);
				dbcontext.SaveChanges();

				//Agent notes
				if (!string.IsNullOrEmpty(cartProductsModel.AgentComments))
				{
					OrderNote agentComments = new OrderNote
					{
						Note = "Comments for Agent : " + cartProductsModel.AgentComments,
						OrderStatus = (int)OrderHistoryEnum.Received,
						OrderId = order.Id
					};
					dbcontext.OrderNote.Add(agentComments);
					dbcontext.SaveChanges();
				}

				// update order details id
				var orderToZUpdate = dbcontext.Order.Where(x => x.Id == order.Id).FirstOrDefault();
				orderToZUpdate.OrderDetailId = orderDetailss.Id;
				dbcontext.Entry(orderToZUpdate).State = EntityState.Modified;
				dbcontext.SaveChanges();
				checkoutProductCartResponseModel.OrderId = order.Id;
				checkoutProductCartResponseModel.OrderNumber = order.CustomOrderNumber;
				checkoutProductCartResponseModel.OrderTotal = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(cartProductsModel.Total));
				//String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
				//String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
				//checkoutProductCartResponseModel.CheckoutUrl = strUrl + "Verifone/details/" + order.Id;


				customerAPIResponses.ErrorMessageTitle = "Success!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.ProuctCheckoutSuccessfully", languageId, dbcontext);
				customerAPIResponses.Status = true;
				customerAPIResponses.StatusCode = 200;
				customerAPIResponses.ResponseObj = checkoutProductCartResponseModel;
				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v2.2/checkoutcartproducts")]
		[HttpPost]
		public CustomerAPIResponses CheckoutCartProductsV2_2([FromBody] ChekoutCartProductsModelV3 cartProductsModel)
		{

			int languageId = LanguageHelper.GetIdByLangCode(cartProductsModel.StoreId, cartProductsModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (cartProductsModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(cartProductsModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (cartProductsModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (cartProductsModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (cartProductsModel.IsDelivery)
			{
				if (cartProductsModel.DeliveryAddressId == 0)
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.DelieveryAddressMissing", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}



			try
			{
				CheckoutProductCartResponseModel checkoutProductCartResponseModel = new CheckoutProductCartResponseModel();
				if (!string.IsNullOrEmpty(cartProductsModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == cartProductsModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				var itemsinShoppingCartExist = dbcontext.ShoppingCartItem.Where(x => x.CustomerId == cartProductsModel.CustomerId && x.StoreId == cartProductsModel.StoreId && x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).Any();
				if (!itemsinShoppingCartExist)
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.ShoppingCartEmpty", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
				var isProductBelongTosameChef = dbcontext.ShoppingCartItem.Where(x => x.StoreId == cartProductsModel.StoreId && x.CustomerId == cartProductsModel.CustomerId && x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).FirstOrDefault();
				if (isProductBelongTosameChef != null)
				{
					var VendorBelongtoProduct = (from pro in dbcontext.Product
												 where pro.Id == isProductBelongTosameChef.ProductId
												 select new
												 {
													 pro.VendorId
												 }).FirstOrDefault();
					cartProductsModel.RestnCookId = VendorBelongtoProduct.VendorId;
				}

				if (cartProductsModel.RestnCookId > 0)
				{
					var vendor = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId).FirstOrDefault();
					bool isOpen = (bool)vendor.IsOpen;
					var currentDatetime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext);
					var openDateTime = currentDatetime;
					var closeDateTime = currentDatetime;
					if (!string.IsNullOrEmpty(vendor.Opentime))
					{
						openDateTime = Convert.ToDateTime(currentDatetime.ToShortDateString() + " " + vendor.Opentime);
					}

					if (!string.IsNullOrEmpty(vendor.Closetime))
					{

						closeDateTime = Convert.ToDateTime(currentDatetime.ToShortDateString() + " " + vendor.Closetime);
					}


					if (isOpen == false || currentDatetime < openDateTime || currentDatetime > closeDateTime)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage. SorryMerchantisclosedatthemoment", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}

				}
				if (cartProductsModel.IsDinning)
				{
					var isDiningProvided = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId && x.IsDining == true).Any();
					if (!isDiningProvided)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CheckOutNotPossibleForDinning", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (cartProductsModel.IsDelivery)
				{
					var isDiningProvided = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId && x.IsDelivery == true).Any();
					if (!isDiningProvided)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CheckOutNotPossibleForDelievery", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (cartProductsModel.IsTakeAway)
				{
					var isDiningProvided = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId && x.IsTakeAway == true).Any();
					if (!isDiningProvided)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CheckOutNotPossibleForTakeAway", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (cartProductsModel.IsDelivery)
				{

					var vendorOrder = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId).FirstOrDefault();
					if (vendorOrder != null)
					{
						var addressTobeDelivered = dbcontext.Address.Where(x => x.Id == cartProductsModel.DeliveryAddressId).FirstOrDefault();
						string strsql = "select id from vendor where Id=" + cartProductsModel.RestnCookId + " and storeid=" + cartProductsModel.StoreId + " and geofancing is not null and geofancing like '%poly%' and isdelivery=1";
						strsql += " and cast(geometry::STGeomFromText(cast((replace(replace(replace(replace(replace(replace(replace(geofancing,";
						strsql += "char(34) + 'coordinates' + char(34),''),";
						strsql += "char(34) + 'type'+ char(34) +':',''),'' + char(34) +',:[{'+ char(34) +'lat'+ char(34) +':','((') ,'{'+ char(34) +'P','P'),','+ char(34) +'lng'+ char(34) +':',' '),'},{'+ char(34) +'lat'+ char(34) +':',', '),'}]}','))') ) AS varchar(max)) ,0) as geometry)";
						strsql += ".STContains(cast(geometry::STGeomFromText('POINT(" + addressTobeDelivered.Latitude + " " + addressTobeDelivered.Longitude + ")', 0) As geometry)) = 1";
						using (SqlConnection connection = new SqlConnection(connectionString))
						{
							SqlCommand command = new SqlCommand(strsql, connection);
							connection.Open();
							SqlDataReader reader = command.ExecuteReader();
							try
							{
								if (!reader.HasRows)
								{
									connection.Close();
									customerAPIResponses.ErrorMessageTitle = "Error!!";
									customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.AddressToFar", languageId, dbcontext);
									customerAPIResponses.Status = false;
									customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
									customerAPIResponses.ResponseObj = null;
									return customerAPIResponses;
								}
							}
							finally
							{
								// Always call Close when done reading.
								connection.Close();
							}
						}
						if (vendorOrder.AvailableType != null && vendorOrder.AvailableType != 0)
						{
							var itemFlag = false;
							if (vendorOrder.AvailableType == (int)AvailableTypeEnum.AllDay)
							{
								var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == vendorOrder.Id && x.AvailableType == (int)AvailableTypeEnum.AllDay).ToList();
								if (AllDaySchedulesTimimgs.Any())
								{
									//var CurrentTime = DateTime.Now.TOS;
									var CurrentTime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).TimeOfDay;
									foreach (var __item in AllDaySchedulesTimimgs)
									{
										if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
										{
											itemFlag = true;
										}


									}
								}
								else
								{
									itemFlag = true;
								}

							}
							else if (vendorOrder.AvailableType == (int)AvailableTypeEnum.Custom)
							{
								var DayOfWeek = (int)Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).DayOfWeek;
								var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == vendorOrder.Id && x.AvailableType == (int)AvailableTypeEnum.Custom && x.AvailableOn == DayOfWeek).ToList();
								if (AllDaySchedulesTimimgs.Any())
								{
									//var CurrentTime = DateTime.Now.TOS;
									var CurrentTime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).TimeOfDay;
									foreach (var __item in AllDaySchedulesTimimgs)
									{
										if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
										{
											itemFlag = true;
										}


									}
								}
								else
								{
									itemFlag = true;
								}
							}
							else
							{
								itemFlag = true;
							}
							if (!itemFlag)
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.RestaturantClosed", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}

						}


					}
					else
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.VendorAddressNotValid", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}

				}
				var ProductsToCheckout = dbcontext.ShoppingCartItem.Where(x => x.CustomerId == cartProductsModel.CustomerId && x.StoreId == cartProductsModel.StoreId && x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).ToList();
				Order order = new Order();
				order.OrderGuid = Guid.NewGuid();
				order.StoreId = cartProductsModel.StoreId;
				string CurrencyCode = Helper.GetStoreCurrencyCode(cartProductsModel.StoreId, dbcontext);
				order.CustomerId = cartProductsModel.CustomerId;
				if (cartProductsModel.IsDelivery)
				{
					order.ShippingAddressId = cartProductsModel.DeliveryAddressId;
					order.BillingAddressId = cartProductsModel.DeliveryAddressId;
					order.PickupInStore = false;

				}
				else if (cartProductsModel.IsTakeAway)
				{
					order.BillingAddressId = cartProductsModel.BillingAddressId;
					order.PickupInStore = true;
				}
				else if (cartProductsModel.IsDinning)
				{
					order.BillingAddressId = cartProductsModel.BillingAddressId;
				}
				order.OrderStatusId = (int)OrderHistoryEnum.Received;
				order.ShippingStatusId = (int)ShippingStatus.NotYetShipped;
				order.PaymentStatusId = (int)PaymentStatus.Pending;
				order.PaymentMethodSystemName = "Payments.Verifone";
				//var vendorId = 
				order.PickupAddressId = dbcontext.Vendor.Where(x => x.Id == cartProductsModel.RestnCookId).FirstOrDefault().AddressId;
				order.CustomerCurrencyCode = CurrencyCode;
				var currencyRate = dbcontext.Currency.Where(x => x.CurrencyCode.Contains(CurrencyCode)).FirstOrDefault();
				if (currencyRate != null)
				{
					order.CurrencyRate = currencyRate.Rate;
				}
				else
				{
					order.CurrencyRate = 1;
				}
				order.CustomerTaxDisplayTypeId = (int)TaxDisplayType.IncludingTax;
				order.OrderSubtotalExclTax = Convert.ToDecimal(cartProductsModel.Subtotal);
				order.OrderSubtotalInclTax = Convert.ToDecimal(cartProductsModel.Subtotal);
				order.OrderSubTotalDiscountInclTax = decimal.Zero;
				order.OrderSubTotalDiscountExclTax = decimal.Zero;
				order.OrderShippingInclTax = 0;
				order.OrderShippingExclTax = 0;
				order.PaymentMethodAdditionalFeeInclTax = decimal.Zero;
				order.PaymentMethodAdditionalFeeExclTax = decimal.Zero;
				order.TaxRates = "0:0;";
				order.OrderDiscount = Convert.ToDecimal(cartProductsModel.DiscountAmount);
				order.OrderTax = Convert.ToDecimal(cartProductsModel.Tax);
				order.OrderTotal = Convert.ToDecimal(cartProductsModel.Total);
				order.RefundedAmount = 0;
				order.CustomerLanguageId = 1;
				order.AffiliateId = 0;
				order.CustomerIp = "127.0.0.1";
				order.AllowStoringCreditCardNumber = false;
				order.ShippingMethod = "Ground";
				order.ShippingRateComputationMethodSystemName = "Shipping.FixedOrByWeight";
				order.Deleted = false;
				order.CreatedOnUtc = DateTime.UtcNow;
				order.Tip = Convert.ToDecimal(cartProductsModel.Tip);
				order.ServiceChargeAmount = Helper.CalculateServiceChargeAmount(Convert.ToDecimal(cartProductsModel.Subtotal), cartProductsModel.StoreId, dbcontext);
				order.DeliveryAmount = cartProductsModel.IsDelivery ? Helper.CalculateDeliveryFee(cartProductsModel.RestnCookId, cartProductsModel.DeliveryAddressId, cartProductsModel.StoreId, dbcontext) : 0;
				order.CustomOrderNumber = (dbcontext.Order.Count() + 1).ToString();
				dbcontext.Order.Add(order);
				dbcontext.SaveChanges();

				if (!string.IsNullOrEmpty(cartProductsModel.CouponCode))
				{
					var isCouponCodeExist = (from x in dbcontext.Discount.AsEnumerable()
											 join jn in dbcontext.StoreMapping.AsEnumerable()
											 on x.Id equals jn.EntityId
											 where jn.StoreId == cartProductsModel.StoreId && jn.EntityName == "Discount"
											 && (x.CouponCode ?? "").ToUpper().ToString() == cartProductsModel.CouponCode.ToUpper().ToString()
											 && x.DiscountTypeId == (int)DiscountType.AssignedToOrderTotal && x.RequiresCouponCode == true
											 select x).FirstOrDefault();
					DiscountUsageHistory discountUsageHistory = new DiscountUsageHistory();
					discountUsageHistory.CreatedOnUtc = DateTime.UtcNow;
					discountUsageHistory.OrderId = order.Id;
					if (isCouponCodeExist != null)
					{
						discountUsageHistory.DiscountId = isCouponCodeExist != null ? isCouponCodeExist.Id : 0;
						dbcontext.DiscountUsageHistory.Add(discountUsageHistory);
						dbcontext.SaveChanges();
					}

				}

				foreach (var item in ProductsToCheckout)
				{
					var itemPrice = (from p in dbcontext.Product.Where(x => x.Id == item.ProductId)
									 select new
									 {
										 Price = p.TaxCategoryId == 0
																? p.Price : (from priceCategoryTax in dbcontext.Product
																			 join categoryTax in dbcontext.TaxRate
																				 on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
																			 where categoryTax.StoreId == cartProductsModel.StoreId &&
																				 priceCategoryTax.Id == p.Id

																			 select new
																			 {
																				 PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
																			 }).FirstOrDefault().PriceCalculated,
									 }).FirstOrDefault();
					OrderItem orderItem = new OrderItem();

					orderItem.OrderItemGuid = Guid.NewGuid();
					orderItem.OrderId = order.Id;
					orderItem.ProductId = item.ProductId;
					orderItem.Quantity = item.Quantity;
					orderItem.ItemInstruction = item.ItemInstruction;
					orderItem.UnitPriceExclTax = itemPrice.Price;
					orderItem.UnitPriceInclTax = itemPrice.Price;
					orderItem.PriceExclTax = (itemPrice.Price * item.Quantity);
					orderItem.PriceInclTax = (itemPrice.Price * item.Quantity);
					orderItem.DiscountAmountExclTax = 0;
					orderItem.DiscountAmountInclTax = 0;
					orderItem.OriginalProductCost = 0;
					orderItem.DownloadCount = 0;
					orderItem.IsDownloadActivated = false;
					orderItem.AttributesXml = item.AttributesXml;
					orderItem.LicenseDownloadId = 0;
					orderItem.ItemWeight = 0;
					dbcontext.OrderItem.Add(orderItem);
					dbcontext.SaveChanges();
				}

				OrderNote orderNote = new OrderNote();
				orderNote.CreatedOnUtc = DateTime.UtcNow;
				orderNote.DisplayToCustomer = true;
				orderNote.DownloadId = 0;
				orderNote.Note = "Order is incomplete";
				orderNote.OrderStatus = (int)OrderHistoryEnum.Cancelled;
				orderNote.OrderId = order.Id;
				dbcontext.OrderNote.Add(orderNote);
				dbcontext.SaveChanges();
				OrderDetails orderDetailss = new OrderDetails();
				orderDetailss.CreatedOnUtc = DateTime.UtcNow;
				orderDetailss.CustomerId = order.CustomerId;
				orderDetailss.StoreId = order.StoreId;
				if (!string.IsNullOrEmpty(cartProductsModel.Tip))
					orderDetailss.Tip = Convert.ToDecimal(cartProductsModel.Tip);
				if (!string.IsNullOrEmpty(cartProductsModel.DeliveryCharges))
					orderDetailss.DeliveryCharges = Convert.ToDecimal(cartProductsModel.DeliveryCharges);
				orderDetailss.OrderNote = cartProductsModel.AdditionalComments;
				if (cartProductsModel.IsDelivery)
				{
					orderDetailss.OrderType = (int)RestType.Delivery;

				}
				else if (cartProductsModel.IsTakeAway)
				{
					orderDetailss.OrderType = (int)RestType.TakeAway;
				}
				else if (cartProductsModel.IsDinning)
				{
					orderDetailss.OrderType = (int)RestType.Dining;
				}
				//orderDetailss.PackingChargesAmount = cartProductsModel.PackingCharges != null ? Convert.ToDecimal(cartProductsModel.PackingCharges) : 0;
				orderDetailss.IsOrderComplete = true;
				dbcontext.OrderDetails.Add(orderDetailss);
				dbcontext.SaveChanges();

				//Agent notes
				if (!string.IsNullOrEmpty(cartProductsModel.AgentComments))
				{
					OrderNote agentComments = new OrderNote
					{
						Note = "Comments for Agent : " + cartProductsModel.AgentComments,
						OrderStatus = (int)OrderHistoryEnum.Received,
						OrderId = order.Id
					};
					dbcontext.OrderNote.Add(agentComments);
					dbcontext.SaveChanges();
				}

				// update order details id
				var orderToZUpdate = dbcontext.Order.Where(x => x.Id == order.Id).FirstOrDefault();
				orderToZUpdate.OrderDetailId = orderDetailss.Id;
				dbcontext.Entry(orderToZUpdate).State = EntityState.Modified;
				dbcontext.SaveChanges();
				checkoutProductCartResponseModel.OrderId = order.Id;
				checkoutProductCartResponseModel.OrderNumber = order.CustomOrderNumber;
				checkoutProductCartResponseModel.OrderTotal = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(cartProductsModel.Total) + Convert.ToDecimal(order.ServiceChargeAmount));
				checkoutProductCartResponseModel.OrderGuid = order.OrderGuid.ToString();

				_cacheManager.RemoveByPrefix(string.Format("API.Cache.Order-{0}", cartProductsModel.StoreId));
				customerAPIResponses.ErrorMessageTitle = "Success!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.ProuctCheckoutSuccessfully", languageId, dbcontext);
				customerAPIResponses.Status = true;
				customerAPIResponses.StatusCode = 200;
				customerAPIResponses.ResponseObj = checkoutProductCartResponseModel;

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v1/GetOrderHistory")]
		[HttpPost]
		public CustomerAPIResponses GetOrderHistory([FromBody] OrderHistoryModel orderHistoryModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(orderHistoryModel.StoreId, orderHistoryModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (orderHistoryModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderHistoryModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(orderHistoryModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderHistoryModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderHistoryModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderHistoryModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(orderHistoryModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == orderHistoryModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Invalid Authentication key";
						customerAPIResponses.ErrorMessage = "Invalid Authentication key";
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var connectionString = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("ConnectionStrings").GetSection("ecuadordatabase").Value;
				var key = string.Format(CacheKeys.OrderHistroyListKey, orderHistoryModel.StoreId, orderHistoryModel.CustomerId, orderHistoryModel.OrderId, orderHistoryModel.OrderStatus, orderHistoryModel.OrderDate, languageId);
				var data = _cacheManager.Get(key, () =>
				{
					int orderStatusId = Helper.IsNumeric(orderHistoryModel.OrderStatus) ? Convert.ToInt32(orderHistoryModel.OrderStatus) : 0;
					int orderId = Helper.IsNumeric(orderHistoryModel.OrderId) ? Convert.ToInt32(orderHistoryModel.OrderId) : 0;


					using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
					{

						var result = db.Query<OrderHistoryResponseModel>("GetOrderHistory", new
						{
							orderHistoryModel.CustomerId,
							orderStatusId,
							OrderId = orderId,
							PageIndex = orderHistoryModel.PageIndex,
							PageSize = orderHistoryModel.PageSize
						}, commandType: CommandType.StoredProcedure).ToList();
						try
						{
							if (result.Count > 0)
							{
								Helper.ProductsAndMerchantName(result, dbcontext);
								var finalResult = new List<OrderHistoryResponseModel>();
								foreach (var item in result)
								{

									item.OrderTime = Helper.ConvertToUserTime(item.OrderTime, DateTimeKind.Utc, dbcontext);
									if (string.IsNullOrEmpty(orderHistoryModel.OrderDate) || item.OrderTime.ToString("MM/dd/yyyy") == orderHistoryModel.OrderDate)
									{
										var deliverySlotBooking = dbcontext.DeliverySlotBooking.Where(x => x.OrderId == item.OrderId && x.Deleted == false).FirstOrDefault();
										if (deliverySlotBooking != null)
										{
											var deliverydate = Helper.ConvertToUserTime(deliverySlotBooking.DeliveryDateUtc, DateTimeKind.Utc, dbcontext);

											item.DeliveredTimeString = string.Format("{0:ddd, dd MMM} {1} - {2}", deliverydate, deliverySlotBooking.DeliveryStartTime.ToString("hh:mm tt"), deliverySlotBooking.DeliveryEndTime.ToString("hh:mm tt"));
										}
										else
										{
											item.DeliveredTime = Helper.ConvertToUserTime(item.DeliveredTime, DateTimeKind.Utc, dbcontext);
										}
										item.Status = LanguageHelper.GetResourseValueByName(orderHistoryModel.StoreId, "App.OrderStatus." + item.Status + "Txt", languageId, dbcontext);
										item.ShowRatingReviewOption = Helper.ShowRatingReviewOption(item.OrderId, orderHistoryModel.StoreId, dbcontext);
										finalResult.Add(item);
									}
								}
								customerAPIResponses.Status = true;
								customerAPIResponses.TotalRecords = finalResult.Select(x => x.TotalRecords).FirstOrDefault();
								customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
								customerAPIResponses.ResponseObj = finalResult;
								return customerAPIResponses;
							}
							else
							{
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.Status = false;
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderHistoryModel.StoreId, "API.ErrorMesaage.OrderNotFound", languageId, dbcontext);
								return customerAPIResponses;
							}
						}
						catch (Exception ex)
						{
							Helper.SentryLogs(ex);
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = ex.Message;
							customerAPIResponses.Status = false;
							customerAPIResponses.StatusCode = 400;
							customerAPIResponses.ResponseObj = null;
							return customerAPIResponses;
						}
					}
				});

				return data;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

		[Route("~/api/v2.1/GetOrderHistory")]
		[HttpPost]
		public CustomerAPIResponses GetOrderHistoryV2_1([FromBody] OrderHistoryModel orderHistoryModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(orderHistoryModel.StoreId, orderHistoryModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (orderHistoryModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderHistoryModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(orderHistoryModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderHistoryModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderHistoryModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderHistoryModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(orderHistoryModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == orderHistoryModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Invalid Authentication key";
						customerAPIResponses.ErrorMessage = "Invalid Authentication key";
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var connectionString = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("ConnectionStrings").GetSection("ecuadordatabase").Value;
				int orderStatusId = Helper.IsNumeric(orderHistoryModel.OrderStatus) ? Convert.ToInt32(orderHistoryModel.OrderStatus) : 0;
				int orderId = Helper.IsNumeric(orderHistoryModel.OrderId) ? Convert.ToInt32(orderHistoryModel.OrderId) : 0;
				using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
				{

					var result = db.Query<OrderHistoryResponseModel>("GetOrderHistory", new
					{
						orderHistoryModel.CustomerId,
						orderStatusId,
						OrderId = orderId
					}, commandType: CommandType.StoredProcedure).ToList();
					try
					{
						if (result.Count > 0)
						{

							var finalResult = new List<OrderHistoryResponseModel>();
							foreach (var item in result)
							{
								item.OrderTime = Helper.ConvertToUserTimeV2_1(item.OrderTime, DateTimeKind.Utc, dbcontext, orderHistoryModel.CustomerId);
								if (string.IsNullOrEmpty(orderHistoryModel.OrderDate) || item.OrderTime.ToString("MM/dd/yyyy") == orderHistoryModel.OrderDate)
								{
									item.DeliveredTime = Helper.ConvertToUserTimeV2_1(item.DeliveredTime, DateTimeKind.Utc, dbcontext, orderHistoryModel.CustomerId);
									item.Status = LanguageHelper.GetResourseValueByName(orderHistoryModel.StoreId, "App.OrderStatus." + item.Status + "Txt", languageId, dbcontext);
									finalResult.Add(item);
								}
							}
							customerAPIResponses.Status = true;
							customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
							customerAPIResponses.ResponseObj = finalResult;
							return customerAPIResponses;
						}
						else
						{
							customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
							customerAPIResponses.Status = false;
							customerAPIResponses.ErrorMessageTitle = "Error!!";
							customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderHistoryModel.StoreId, "API.ErrorMesaage.OrderNotFound", languageId, dbcontext);
							return customerAPIResponses;
						}
					}
					catch (Exception ex)
					{
						Helper.SentryLogs(ex);
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = ex.Message;
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = 400;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}


				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			return customerAPIResponses;
		}

		[Route("~/api/v1/GetOrderedProdutsforReviewByID")]
		[HttpPost]
		public CustomerAPIResponses GetOrderforReviewByID([FromBody] OrderReviewModel orderReviewModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(orderReviewModel.StoreId, orderReviewModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (orderReviewModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(orderReviewModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.OrderId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.OrderIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(orderReviewModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == orderReviewModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (orderReviewModel.StoreId != 0)
				{
					var OrderExist = dbcontext.Store.AsEnumerable().Where(x => x.Id == orderReviewModel.StoreId).Any();
					if (!OrderExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (orderReviewModel.OrderId != 0)
				{
					var OrderExist = dbcontext.Order.AsEnumerable().Where(x => x.Id == orderReviewModel.OrderId && x.StoreId == orderReviewModel.StoreId).Any();
					if (!OrderExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.OrderNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				// string reviewoptions = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "ratingreviewsettings.merchant.dislikeoptions", languageId, dbcontext);
				if (orderReviewModel.CustomerId != 0)
				{
					var CustomerExist = dbcontext.Customer.AsEnumerable().Where(x => x.Id == orderReviewModel.CustomerId && x.RegisteredInStoreId == orderReviewModel.StoreId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var merchantId = (from a in dbcontext.Order.AsEnumerable()
								  join b in dbcontext.OrderItem.AsEnumerable()
								  on a.Id equals b.OrderId
								  join c in dbcontext.Product
								  on b.ProductId equals c.Id
								  where a.Id == orderReviewModel.OrderId
								  select new
								  {
									  c.VendorId
								  }).FirstOrDefault();
				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault().Value;
				var OrderHistory = (from a in dbcontext.Order.AsEnumerable()
									where a.CustomerId == orderReviewModel.CustomerId
									&& a.Id == orderReviewModel.OrderId
									select new ReviewsItems
									{
										OrderId = a.Id,
										OrderNumber = a.CustomOrderNumber,
										IsAgentReview = dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == orderReviewModel.StoreId && x.Name == "ratingreviewsettings.agent.isactive").Any() ?
										dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == orderReviewModel.StoreId && x.Name == "ratingreviewsettings.agent.isactive").FirstOrDefault().Value.ToLower().ToString() == "true" : false,
										IsMerchantReview = dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == orderReviewModel.StoreId && x.Name == "ratingreviewsettings.merchant.isactive").Any() ?
										dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == orderReviewModel.StoreId && x.Name == "ratingreviewsettings.merchant.isactive").FirstOrDefault().Value.ToLower().ToString() == "true" : false,
										IsItemReview = dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == orderReviewModel.StoreId && x.Name == "ratingreviewsettings.item.isactive").Any() ?
										dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == orderReviewModel.StoreId && x.Name == "ratingreviewsettings.item.isactive").FirstOrDefault().Value.ToLower().ToString() == "true" : false,
										MerchantReview = (from merchant in dbcontext.Vendor.AsEnumerable()
														  where merchant.Id == merchantId.VendorId
														  select new MerchantReview
														  {
															  MerchantId = merchant.Id,
															  PictureId = merchant.PictureId,
															  Title = merchant.Name,
															  Subtitle = string.Format(LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "RatingReview.ReviewText.Merchant.HeaderText", languageId, dbcontext), merchant.Name),
															  ReviewOptionsS = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "ratingreviewsettings.merchant.dislikeoptions", languageId, dbcontext)
														  }).FirstOrDefault(),
										AgentReview = (from agent in dbcontext.AgentOrderStatus.AsEnumerable()
													   where agent.OrderId == orderReviewModel.OrderId

													   select new AgentReview
													   {
														   AgentId = agent.AgentId.Value,
														   Picture = dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agent.AgentId.Value && x.Key == "ProfileURL").FirstOrDefault() != null ?
														   dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agent.AgentId.Value && x.Key == "ProfileURL").FirstOrDefault().Value :
														   ChefBaseUrl + "default-image_450.png",
														   Title = "How was " + (dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agent.AgentId.Value && x.Key == "FirstName").Any() ? dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agent.AgentId.Value && x.Key == "FirstName").FirstOrDefault().Value :
														   "") + " " + (dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agent.AgentId.Value && x.Key == "LastName").Any() ? dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agent.AgentId.Value && x.Key == "LastName").FirstOrDefault().Value :
														   "") + "\'s delivery",
														   Subtitle = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "RatingReview.ReviewText.Agent.HeaderText", languageId, dbcontext),
														   //ReviewOptionsS = dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == orderReviewModel.StoreId && x.Name == "ratingreviewsettings.agent.dislikeoptions").Any()
														   //   ? dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == orderReviewModel.StoreId && x.Name == "ratingreviewsettings.agent.dislikeoptions").FirstOrDefault().Value :
														   //   ""
														   ReviewOptionsS = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "ratingreviewsettings.agent.dislikeoptions", languageId, dbcontext)
													   }).FirstOrDefault(),

										Products = (from order in dbcontext.OrderItem.AsEnumerable()
													join products in dbcontext.Product.AsEnumerable()
													on order.ProductId equals products.Id
													where order.OrderId == orderReviewModel.OrderId
													select new ProductsDetails
													{
														ProductId = products.Id,
														PictureId = dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == products.Id).FirstOrDefault() != null ?
														dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == products.Id).FirstOrDefault().PictureId
														: 0,
														Subtitle = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "RatingReview.ReviewText.Product.HeaderText", languageId, dbcontext) + " " + products.Name,
														Title = products.Name
													}).ToList()

									}).ToList();
				if (OrderHistory.Any())
				{
					foreach (var item in OrderHistory)
					{
						if (item.MerchantReview != null)
						{
							//item.MerchantReview.ReviewOptions = item.MerchantReview.ReviewOptionsS.Split(',').Where(x => !string.IsNullOrEmpty(x)).ToList();
							int pictureSize = 100;
							var setting = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("mediasettings.vendorthumbpicturesize")).FirstOrDefault();
							if (setting != null)
							{
								pictureSize = Convert.ToInt32(setting.Value);
							}
							if (item.MerchantReview.PictureId != 0)
							{

								int pictureId = Convert.ToInt32(item.MerchantReview.PictureId);
								var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();
								string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
								string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
								? $"{pictureImage.Id:0000000}_{pictureImage.SeoFilename}.{lastPart}"
								: $"{pictureImage.Id:0000000}_{pictureSize}.{lastPart}";
								item.MerchantReview.Picture = ChefBaseUrl + thumbFileName;
							}
							else
							{
								item.MerchantReview.Picture = ChefBaseUrl + "default-image_" + pictureSize + ".png";
							}
						}

						if (item.AgentReview != null && !string.IsNullOrEmpty(item.AgentReview.ReviewOptionsS))
						{
							// item.AgentReview.ReviewOptions = item.AgentReview.ReviewOptionsS.Split(',').Where(x => !string.IsNullOrEmpty(x)).ToList();

						}
						//if(item.AgentReview!=null)
						//{
						//    //item.AgentReview.Picture=""
						//    item.AgentReview.Subtitle = "Please review our agent " +  dbcontext.GenericAttributes.Where(x => x.EntityId == item.AgentReview.AgentId && x.Key == "FirstName").FirstOrDefault().Value + " " +
						//                                   dbcontext.GenericAttributes.Where(x => x.EntityId == item.AgentReview.AgentId && x.Key == "LastName").FirstOrDefault() != null ?
						//                                   dbcontext.GenericAttributes.Where(x => x.EntityId == item.AgentReview.AgentId && x.Key == "LastName").FirstOrDefault().Value : "";
						//}
						if (item.Products.Any())
						{

							foreach (var _item in item.Products)
							{
								int pictureSize = 75;
								var setting = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
								if (setting != null)
								{
									pictureSize = Convert.ToInt32(setting.Value);
								}
								if (_item.PictureId != 0)
								{
									int pictureId = Convert.ToInt32(_item.PictureId);
									var pictureImage = dbcontext.Picture.AsEnumerable().Where(x => x.Id == pictureId).FirstOrDefault();

									string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
									string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
									? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}"
									: $"{pictureId:0000000}_{pictureSize}.{lastPart}";
									_item.ProductImage = ChefBaseUrl + thumbFileName;
								}
								else
								{
									_item.ProductImage = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
								}


							}



						}

					}
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = OrderHistory;
				}
				else
				{
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.NoOrderOrderedYet", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Order History";
				}

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}


		[Route("~/api/v2/GetOrderedProdutsforReviewByID")]
		[HttpPost]
		public CustomerAPIResponses GetOrderforReviewsByID([FromBody] OrderReviewModel orderReviewModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(orderReviewModel.StoreId, orderReviewModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (orderReviewModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(orderReviewModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.OrderId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.OrderIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(orderReviewModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == orderReviewModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (orderReviewModel.StoreId != 0)
				{
					var OrderExist = dbcontext.Store.AsEnumerable().Where(x => x.Id == orderReviewModel.StoreId).Any();
					if (!OrderExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (orderReviewModel.OrderId != 0)
				{
					var OrderExist = dbcontext.Order.AsEnumerable().Where(x => x.Id == orderReviewModel.OrderId && x.StoreId == orderReviewModel.StoreId).Any();
					if (!OrderExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.OrderNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				// string reviewoptions = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "ratingreviewsettings.merchant.dislikeoptions", languageId, dbcontext);
				if (orderReviewModel.CustomerId != 0)
				{
					var CustomerExist = dbcontext.Customer.AsEnumerable().Where(x => x.Id == orderReviewModel.CustomerId && x.RegisteredInStoreId == orderReviewModel.StoreId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var merchantId = (from a in dbcontext.Order.AsEnumerable()
								  join b in dbcontext.OrderItem.AsEnumerable()
								  on a.Id equals b.OrderId
								  join c in dbcontext.Product
								  on b.ProductId equals c.Id
								  where a.Id == orderReviewModel.OrderId
								  select new
								  {
									  c.VendorId
								  }).FirstOrDefault();
				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault().Value;
				var OrderHistory = (from a in dbcontext.Order.AsEnumerable()
									where a.CustomerId == orderReviewModel.CustomerId
									&& a.Id == orderReviewModel.OrderId
									select new ReviewsItems
									{
										OrderId = a.Id,
										OrderNumber = a.CustomOrderNumber,
										IsAgentReview = dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == orderReviewModel.StoreId && x.Name == "ratingreviewsettings.agent.isactive").Any() ?
										dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == orderReviewModel.StoreId && x.Name == "ratingreviewsettings.agent.isactive").FirstOrDefault().Value.ToLower().ToString() == "true" : false,
										IsMerchantReview = dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == orderReviewModel.StoreId && x.Name == "ratingreviewsettings.merchant.isactive").Any() ?
										dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == orderReviewModel.StoreId && x.Name == "ratingreviewsettings.merchant.isactive").FirstOrDefault().Value.ToLower().ToString() == "true" : false,
										IsItemReview = dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == orderReviewModel.StoreId && x.Name == "ratingreviewsettings.item.isactive").Any() ?
										dbcontext.Setting.AsEnumerable().Where(x => x.StoreId == orderReviewModel.StoreId && x.Name == "ratingreviewsettings.item.isactive").FirstOrDefault().Value.ToLower().ToString() == "true" : false,
										IsAgentReviewFound = Helper.IsAgentReviewEnabledAndFound(orderReviewModel.StoreId, orderReviewModel.OrderId, dbcontext),
										IsMerchantReviewFound = Helper.IsMerchantReviewEnabledAndFound(orderReviewModel.StoreId, orderReviewModel.OrderId, dbcontext),
										MerchantReview = (from merchant in dbcontext.Vendor.AsEnumerable()
														  where merchant.Id == merchantId.VendorId
														  select new MerchantReview
														  {
															  MerchantId = merchant.Id,
															  PictureId = merchant.PictureId,
															  Title = merchant.Name,
															  Subtitle = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "RatingReview.ReviewText.Merchant.HeaderText", languageId, dbcontext),
															  ReviewOptionsS = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("ratingreviewsettings.merchant.activeoptionsettingids") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault()?.Value ?? "",
														  }).FirstOrDefault(),
										AgentReview = (from agent in dbcontext.AgentOrderStatus.AsEnumerable()
													   where agent.OrderId == orderReviewModel.OrderId

													   select new AgentReview
													   {
														   AgentId = agent.AgentId.Value,
														   Picture = dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agent.AgentId.Value && x.Key == "ProfileURL").FirstOrDefault() != null ?
														   dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agent.AgentId.Value && x.Key == "ProfileURL").FirstOrDefault().Value :
														   ChefBaseUrl + "default-image_450.png",
														   Title = (dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agent.AgentId.Value && x.Key == "FirstName").Any() ? dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agent.AgentId.Value && x.Key == "FirstName").FirstOrDefault().Value :
														   "") + " " + (dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agent.AgentId.Value && x.Key == "LastName").Any() ? dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agent.AgentId.Value && x.Key == "LastName").FirstOrDefault().Value :
														   ""),
														   Subtitle = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "RatingReview.ReviewText.Agent.HeaderText", languageId, dbcontext),
														   ReviewOptionsS = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("ratingreviewsettings.merchant.activeoptionsettingids") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault()?.Value ?? "",
													   }).FirstOrDefault(),

										Products = (from order in dbcontext.OrderItem.AsEnumerable()
													join products in dbcontext.Product.AsEnumerable()
													on order.ProductId equals products.Id
													where order.OrderId == orderReviewModel.OrderId
													select new ProductsDetails
													{
														ProductId = products.Id,
														ProductDescription = products.ShortDescription,
														Price = Convert.ToString(Helper.ConvertdecimaltoUptotwoPlaces(products.Price)),
														OldPrice = Convert.ToString(Helper.ConvertdecimaltoUptotwoPlaces(products.OldPrice)),
														PictureId = dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == products.Id).FirstOrDefault() != null ?
														dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == products.Id).FirstOrDefault().PictureId
														: 0,
														Subtitle = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "RatingReview.ReviewText.Product.HeaderText", languageId, dbcontext),
														Title = products.Name,
														BrandName = (from sao in dbcontext.SpecificationAttributeOption
																	 join psm in dbcontext.ProductSpecificationAttributeMapping
																	 on sao.Id equals psm.SpecificationAttributeOptionId
																	 where psm.ProductId == products.Id
																	 select sao.Name).FirstOrDefault(),
														IsItemReviewFound = Helper.IsItemReviewEnabledAndFound(orderReviewModel.StoreId, orderReviewModel.OrderId, products.Id, dbcontext)
													}).ToList()

									}).ToList();
				if (OrderHistory.Any())
				{
					foreach (var item in OrderHistory)
					{
						if (item.MerchantReview != null)
						{
							var merchantReviewOption = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("ratingreviewsettings.merchant.activeoptionsettingids") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault().Value;
							foreach (var merchant in merchantReviewOption.Split(',').Where(x => !string.IsNullOrEmpty(x)).ToList())
							{
								var merchantOption = (from Ro in dbcontext.NB_ReviewOption.AsEnumerable()
													  where Ro.Id == Convert.ToInt32(merchant)
													  select new MerchantReviewModel
													  {
														  Id = Ro.Id,
														  Name = LanguageHelper.GetLocalizedValueMerchantReview(Ro.Id, "Name", languageId, Ro.Name, dbcontext),

													  }).ToList();
								item.MerchantReview.ReviewOptions.Add(merchantOption.FirstOrDefault());

							}
							int pictureSize = 100;
							var setting = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("mediasettings.vendorthumbpicturesize")).FirstOrDefault();
							if (setting != null)
							{
								pictureSize = Convert.ToInt32(setting.Value);
							}
							if (item.MerchantReview.PictureId != 0)
							{

								int pictureId = Convert.ToInt32(item.MerchantReview.PictureId);
								var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();
								string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
								string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
								? $"{pictureImage.Id:0000000}_{pictureImage.SeoFilename}.{lastPart}"
								: $"{pictureImage.Id:0000000}_{pictureSize}.{lastPart}";
								item.MerchantReview.Picture = ChefBaseUrl + thumbFileName;
							}
							else
							{
								item.MerchantReview.Picture = ChefBaseUrl + "default-image_" + pictureSize + ".png";
							}
						}

						if (item.AgentReview != null && !string.IsNullOrEmpty(item.AgentReview.ReviewOptionsS))
						{
							var AgentReviewOption = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("ratingreviewsettings.agent.activeoptionsettingids") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault()?.Value ?? "";
							foreach (var agent in AgentReviewOption.Split(',').Where(x => !string.IsNullOrEmpty(x)).ToList())
							{
								var agentOption = (from Ro in dbcontext.NB_ReviewOption.AsEnumerable()
												   where Ro.Id == Convert.ToInt32(agent)
												   select new AgentReviewModel
												   {
													   Id = Ro.Id,
													   Name = LanguageHelper.GetLocalizedValueAgentReview(Ro.Id, "Name", languageId, Ro.Name, dbcontext),

												   }).ToList();
								item.AgentReview.ReviewOptions.Add(agentOption.FirstOrDefault());

							}
						}
						if (item.Products.Any())
						{

							foreach (var _item in item.Products)
							{
								int pictureSize = 75;
								var setting = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
								if (setting != null)
								{
									pictureSize = Convert.ToInt32(setting.Value);
								}
								if (_item.PictureId != 0)
								{
									int pictureId = Convert.ToInt32(_item.PictureId);
									var pictureImage = dbcontext.Picture.AsEnumerable().Where(x => x.Id == pictureId).FirstOrDefault();

									string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
									string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
									? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}"
									: $"{pictureId:0000000}_{pictureSize}.{lastPart}";
									_item.ProductImage = ChefBaseUrl + thumbFileName;
								}
								else
								{
									_item.ProductImage = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
								}


							}



						}

					}
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = OrderHistory;
				}
				else
				{
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.NoOrderOrderedYet", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Order History";
				}

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}


		[Route("~/api/v1/OrderReviewByOrderID")]
		[HttpPost]
		public CustomerAPIResponses OrderReviewByOrderIDV2([FromBody] OrderReviewDetailsModelV2 orderReviewModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(orderReviewModel.StoreId, orderReviewModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (orderReviewModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(orderReviewModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Customer Id Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.OrderId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Order Id Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.OrderIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(orderReviewModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == orderReviewModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (orderReviewModel.vendorReviews != null)
				{
					RatingReviews restaurantNCooksProductsRating = new RatingReviews();
					restaurantNCooksProductsRating.EntityId = orderReviewModel.vendorReviews.MerchantId;
					restaurantNCooksProductsRating.Rating = orderReviewModel.vendorReviews.Rating;
					restaurantNCooksProductsRating.CustomerId = orderReviewModel.CustomerId;
					restaurantNCooksProductsRating.OrderId = orderReviewModel.OrderId;
					restaurantNCooksProductsRating.ReviewType = (int)NHKCustomerApplication.Utilities.ReviewType.Merchant;
					restaurantNCooksProductsRating.StoreId = orderReviewModel.StoreId;
					restaurantNCooksProductsRating.IsApproved = dbcontext.Setting.Where(x => x.Name.Contains("ratingreviewsettings.merchant.isapprovalrequired") && x.StoreId == orderReviewModel.StoreId).Any() ? Convert.ToBoolean(dbcontext.Setting.Where(x => x.Name.Contains("ratingreviewsettings.merchant.isapprovalrequired") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault().Value) : false;
					var ReviewTypeValue = Enum.GetValues(typeof(NHKCustomerApplication.Utilities.ReviewType)).Cast<NHKCustomerApplication.Utilities.ReviewType>().Where(x => (int)x == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant).Select(x => x.ToString()).FirstOrDefault();
					restaurantNCooksProductsRating.Title = ReviewTypeValue;
					restaurantNCooksProductsRating.ReviewText = orderReviewModel.vendorReviews.AdditionalComments;
					restaurantNCooksProductsRating.IsLiked = orderReviewModel.vendorReviews.IsLiked;
					restaurantNCooksProductsRating.IsNotInterested = orderReviewModel.vendorReviews.IsNotIntersted;
					restaurantNCooksProductsRating.IsSkipped = orderReviewModel.vendorReviews.IsSkip;
					restaurantNCooksProductsRating.ReviewOptions = orderReviewModel.vendorReviews.Reviews;
					restaurantNCooksProductsRating.DisLikedReviewOptions = orderReviewModel.vendorReviews.DisLikedReviews;
					restaurantNCooksProductsRating.CreatedOnUtc = DateTime.Now;
					dbcontext.RatingReviews.Add(restaurantNCooksProductsRating);
					dbcontext.SaveChanges();
				}
				if (orderReviewModel.agentReviews != null)
				{
					RatingReviews restaurantNCooksProductsRating = new RatingReviews();
					restaurantNCooksProductsRating.EntityId = orderReviewModel.agentReviews.AgentId;
					restaurantNCooksProductsRating.Rating = orderReviewModel.agentReviews.Rating;
					restaurantNCooksProductsRating.CustomerId = orderReviewModel.CustomerId;
					restaurantNCooksProductsRating.OrderId = orderReviewModel.OrderId;
					restaurantNCooksProductsRating.ReviewType = (int)NHKCustomerApplication.Utilities.ReviewType.Agent;
					restaurantNCooksProductsRating.StoreId = orderReviewModel.StoreId;
					restaurantNCooksProductsRating.IsApproved = dbcontext.Setting.Where(x => x.Name.Contains("ratingreviewsettings.agent.isapprovalrequired") && x.StoreId == orderReviewModel.StoreId).Any() ? Convert.ToBoolean(dbcontext.Setting.Where(x => x.Name.Contains("ratingreviewsettings.agent.isapprovalrequired") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault().Value) : false;
					var ReviewTypeValue = Enum.GetValues(typeof(NHKCustomerApplication.Utilities.ReviewType)).Cast<NHKCustomerApplication.Utilities.ReviewType>().Where(x => (int)x == (int)NHKCustomerApplication.Utilities.ReviewType.Agent).Select(x => x.ToString()).FirstOrDefault();
					restaurantNCooksProductsRating.Title = ReviewTypeValue;
					restaurantNCooksProductsRating.ReviewText = orderReviewModel.agentReviews.AdditionalComments;
					restaurantNCooksProductsRating.IsLiked = orderReviewModel.agentReviews.IsLiked;
					restaurantNCooksProductsRating.IsNotInterested = orderReviewModel.agentReviews.IsNotIntersted;
					restaurantNCooksProductsRating.IsSkipped = orderReviewModel.agentReviews.IsSkip;
					restaurantNCooksProductsRating.ReviewOptions = orderReviewModel.agentReviews.Reviews;
					restaurantNCooksProductsRating.DisLikedReviewOptions = orderReviewModel.agentReviews.DisLikedReviews;
					restaurantNCooksProductsRating.CreatedOnUtc = DateTime.Now;
					dbcontext.RatingReviews.Add(restaurantNCooksProductsRating);
					dbcontext.SaveChanges();
				}
				if (orderReviewModel.ReviewProducts != null && orderReviewModel.ReviewProducts.Any())
				{
					foreach (var item in orderReviewModel.ReviewProducts)
					{
						RatingReviews restaurantNCooksProductsRating = new RatingReviews();
						restaurantNCooksProductsRating.EntityId = item.ProductId;
						restaurantNCooksProductsRating.Rating = item.Rating;
						restaurantNCooksProductsRating.CustomerId = orderReviewModel.CustomerId;
						restaurantNCooksProductsRating.OrderId = orderReviewModel.OrderId;
						restaurantNCooksProductsRating.ReviewType = (int)NHKCustomerApplication.Utilities.ReviewType.Item;
						restaurantNCooksProductsRating.StoreId = orderReviewModel.StoreId;
						restaurantNCooksProductsRating.IsApproved = dbcontext.Setting.Where(x => x.Name.Contains("ratingreviewsettings.item.isapprovalrequired") && x.StoreId == orderReviewModel.StoreId).Any() ? Convert.ToBoolean(dbcontext.Setting.Where(x => x.Name.Contains("ratingreviewsettings.item.isapprovalrequired") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault().Value) : false;
						var ReviewTypeValue = string.Empty;
						if (!string.IsNullOrEmpty(item.ReviewTitle))
						{
							ReviewTypeValue = item.ReviewTitle;
						}
						else
						{
							ReviewTypeValue = Enum.GetValues(typeof(NHKCustomerApplication.Utilities.ReviewType)).Cast<NHKCustomerApplication.Utilities.ReviewType>().Where(x => (int)x == (int)NHKCustomerApplication.Utilities.ReviewType.Item).Select(x => x.ToString()).FirstOrDefault();
						}
						restaurantNCooksProductsRating.Title = ReviewTypeValue;
						restaurantNCooksProductsRating.ReviewText = item.AdditionalComments;
						restaurantNCooksProductsRating.IsLiked = item.IsLiked;
						restaurantNCooksProductsRating.IsNotInterested = item.IsNotIntersted;
						restaurantNCooksProductsRating.IsSkipped = item.IsSkip;
						//restaurantNCooksProductsRating.ReviewOptions = item.Rating;
						restaurantNCooksProductsRating.CreatedOnUtc = DateTime.Now;
						dbcontext.RatingReviews.Add(restaurantNCooksProductsRating);
						dbcontext.SaveChanges();
					}

				}
				_cacheManager.RemoveByPrefix(string.Format("API.Cache.Order-{0}", orderReviewModel.StoreId));
				customerAPIResponses.Status = true;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
				customerAPIResponses.ResponseObj = null;
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.ReviewedSuccessfully", languageId, dbcontext);
				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

		[Route("~/api/v1/GetOrderStatus")]
		[HttpPost]
		public CustomerAPIResponses GetOrderStatus([FromBody] CountryModel orderStatus)
		{
			int languageId = LanguageHelper.GetIdByLangCode(orderStatus.StoreId, orderStatus.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (orderStatus == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderStatus.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(orderStatus.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderStatus.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderStatus.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderStatus.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(orderStatus.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == orderStatus.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderStatus.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}


				var OrderStatus = from OrderHistoryEnum d in Enum.GetValues(typeof(OrderHistoryEnum))
								  select new { StatusId = (int)d, Status = LanguageHelper.GetResourseValueByName(orderStatus.StoreId, "App.OrderStatus." + d.ToString() + "Txt", languageId, dbcontext) };
				customerAPIResponses.Status = true;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
				customerAPIResponses.ResponseObj = OrderStatus;

				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderStatus.StoreId, "API.ErrorMesaage.OrderStatusFetchedSuccessfully", languageId, dbcontext);
				customerAPIResponses.ErrorMessageTitle = "Success!!";

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}


		[Route("~/api/v1/OrderTrackingDetailsByOrderID")]
		[HttpPost]
		public CustomerAPIResponses OrderDetailsByOrderID([FromBody] OrderReviewModel orderReviewModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(orderReviewModel.StoreId, orderReviewModel.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (orderReviewModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(orderReviewModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Customer Id Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.OrderId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Order Id Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.OrderIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(orderReviewModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == orderReviewModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Invalid Authentication key";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}




				string symbol = Helper.GetStoreCurrency(orderReviewModel.StoreId, dbcontext);
				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault().Value;
				var orderDetails = (from a in dbcontext.Order
									where a.Id == orderReviewModel.OrderId
									&& a.CustomerId == orderReviewModel.CustomerId
									select new OrderTrackingModel
									{
										OrderId = a.Id,
										OrderDate = a.CreatedOnUtc,
										OrderNumber = a.CustomOrderNumber,
										Amount = a.OrderTotal.ToString(),
										SubAmount = a.OrderSubtotalInclTax.ToString(),
										OrderStatus = a.OrderStatusId.ToString(),
										DiscountAmount = a.OrderDiscount.ToString(),
										MerchantAddressType = dbcontext.Vendor.Where(x => x.AddressId == a.PickupAddressId).FirstOrDefault().Name,
										BillingAddressType = (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault() != null ? ((dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Home) ? "Home" : ((dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Office) ? "Office" : "Other" : "Other"),
										ShippingAddressType = (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault() != null ? ((dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Home) ? "Home" : ((dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Office) ? "Office" : "Other" : "Other"),
										//PackingCharges = (from orderDetail in dbcontext.OrderDetails.Where(x => x.Id == a.OrderDetailId)
										//                  select new {
										//                      orderDetail
										//                  }).FirstOrDefault().orderDetail.PackingChargesAmount!=null? (from orderDetail in dbcontext.OrderDetails.Where(x => x.Id == a.OrderDetailId)
										//                                                                               select new
										//                                                                               {
										//                                                                                   orderDetail
										//                                                                               }).FirstOrDefault().orderDetail.PackingChargesAmount.Value.ToString():"0",
										Tax = a.OrderTax.ToString(),
										BillingAddress = (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().Address1) + " " + (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().Address2) : ""),
										ShippingAddress = (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().Address1) + " " + (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().Address2) : ""),
										CooknChefAddress = dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault().Address1) + " " + (dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault().Address2 != null ?
										dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault().Address2 : "") : "",
										OrderedItems = (from c in dbcontext.OrderItem
														join d in dbcontext.Product
														on c.ProductId equals d.Id
														where c.OrderId == a.Id
														select new OrderedItems
														{
															ProductName = d.Name,
															ProductQuantity = c.Quantity,
															ProductTotalPrice = c.PriceInclTax.ToString(),
															ProductPrice = c.UnitPriceInclTax.ToString(),
															ProductImage = dbcontext.ProductPictureMapping.Where(x => x.ProductId == d.Id).FirstOrDefault() != null ? dbcontext.ProductPictureMapping.Where(x => x.ProductId == d.Id).FirstOrDefault().PictureId.ToString() : "0"
														}).ToList(),
										OrderedStatusArray = (from c in dbcontext.Order
															  join d in dbcontext.OrderNote
															  on c.Id equals d.OrderId
															  where c.Id == a.Id
															  select new OrderedStatusArray
															  {
																  StatusId = d.OrderStatus != null ? d.OrderStatus.Value : 100,
																  StatusTitle = d.Note,
																  StatusDescription = d.Note,
																  StatusDate = d.CreatedOnUtc,
															  }).ToList(),
										OrderType = 3,
										IsOrderNotes = !string.IsNullOrEmpty((from _c in dbcontext.OrderDetails
																			  where _c.Id == a.OrderDetailId
																			  select new
																			  {
																				  _c
																			  }).FirstOrDefault()._c.OrderNote),
										OrderNotes = string.IsNullOrEmpty((from _c in dbcontext.OrderDetails
																		   where _c.Id == a.OrderDetailId
																		   select new
																		   {
																			   _c
																		   }).FirstOrDefault()._c.OrderNote) ? "" : (from _c in dbcontext.OrderDetails
																													 where _c.Id == a.OrderDetailId
																													 select new
																													 {
																														 _c
																													 }).FirstOrDefault()._c.OrderNote,
										AgentOrderNotes = (from c in dbcontext.Order
														   join d in dbcontext.OrderNote
														   on c.Id equals d.OrderId
														   where c.Id == a.Id && d.Note.Contains("Comments for Agent")
														   select d.Note).FirstOrDefault(),

										Tip = a.OrderDetailId.ToString(),
										DeliveryCharges = a.OrderDetailId.ToString()

									}).ToList();
				if (orderDetails.Any())
				{
					foreach (var item in orderDetails)
					{
						int OrderDetailId = Convert.ToInt32(item.Tip ?? "0");
						item.Tip = "0";
						item.DeliveryCharges = "0";
						if (OrderDetailId > 0)
						{
							item.Tip = (dbcontext.OrderDetails.Where(x => x.Id == OrderDetailId).AsEnumerable().FirstOrDefault().Tip.HasValue) ?
											dbcontext.OrderDetails.Where(x => x.Id == OrderDetailId).AsEnumerable().FirstOrDefault().Tip.Value.ToString() : "0";
							item.DeliveryCharges = (dbcontext.OrderDetails.Where(x => x.Id == OrderDetailId).AsEnumerable().FirstOrDefault().DeliveryCharges.HasValue) ?
											dbcontext.OrderDetails.Where(x => x.Id == OrderDetailId).AsEnumerable().FirstOrDefault().DeliveryCharges.Value.ToString() : "0";
						}
						item.OrderDate = Helper.ConvertToUserTime(item.OrderDate, DateTimeKind.Utc, dbcontext);
						item.SubAmount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.SubAmount));
						item.Tax = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Tax));
						item.Tip = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Tip));
						item.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.DeliveryCharges));
						item.PackingCharges = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.PackingCharges));
						item.Amount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Amount));
						item.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.DiscountAmount));
						foreach (var _item in item.OrderedItems)
						{
							_item.ProductTotalPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(_item.ProductTotalPrice));
							_item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(_item.ProductPrice));
							int pictureSize = 75;
							var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
							if (setting != null)
							{
								pictureSize = Convert.ToInt32(setting.Value);
							}
							if (!string.IsNullOrEmpty(_item.ProductImage))
							{
								if (_item.ProductImage != "0")
								{
									int pictureId = Convert.ToInt32(_item.ProductImage);
									var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();
									string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
									string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
									? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}"
									: $"{pictureId:0000000}_{pictureSize}.{lastPart}";
									_item.ProductImage = ChefBaseUrl + thumbFileName;
								}
								else
								{
									_item.ProductImage = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
								}
							}
							else
							{
								_item.ProductImage = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
							}

						}
						int OrderStatusId = Convert.ToInt32(item.OrderStatus);
						item.OrderStatus = Enum.GetValues(typeof(OrderHistoryEnum)).Cast<OrderHistoryEnum>().Where(x => (int)x == OrderStatusId).Select(x => x.ToString()).ToList()[0];
						item.OrderStatus = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.OrderStatus." + item.OrderStatus + "Txt", languageId, dbcontext);
						foreach (var item_ in item.OrderedStatusArray)
						{
							if (item_.StatusId == 100)
							{
								if (item_.StatusTitle.ToLower().Contains("confirmed"))
								{
									item_.StatusId = (int)OrderHistoryEnum.Confirmed;
								}
								else if (item_.StatusTitle.ToLower().Contains("prepar"))
								{
									item_.StatusId = (int)OrderHistoryEnum.Preparing;
								}
								else if (item_.StatusTitle.ToLower().Contains("pick"))
								{
									item_.StatusId = (int)OrderHistoryEnum.Picked;
								}
								else if (item_.StatusTitle.ToLower().Contains("deliver"))
								{
									item_.StatusId = (int)OrderHistoryEnum.OrderDelivered;
								}
								else if (item_.StatusTitle.ToLower().Contains("placed"))
								{
									item_.StatusId = (int)OrderHistoryEnum.OrderDelivered;
								}
								else if (item_.StatusTitle.ToLower().Contains("cancelled"))
								{
									item_.StatusId = (int)OrderHistoryEnum.Cancelled;
								}
								else if (item_.StatusTitle.ToLower().Contains("recieved"))
								{
									item_.StatusId = (int)OrderHistoryEnum.Received;
								}



							}
							item_.StatusDate = Helper.ConvertToUserTime(item_.StatusDate, DateTimeKind.Utc, dbcontext);
							if (item_.StatusId != (int)OrderHistoryEnum.Received)
							{
								DateTime iKnowThisIsUtc = item_.StatusDate;
								DateTime runtimeKnowsThisIsUtc = DateTime.SpecifyKind(
									iKnowThisIsUtc,
									DateTimeKind.Utc);
								DateTime localVersion = runtimeKnowsThisIsUtc.ToLocalTime();
								item_.StatusTime = localVersion.ToString("hh:mm tt");
							}
							else
							{
								item_.StatusTime = item_.StatusDate.ToString("hh:mm tt");
							}

							item_.StatusTitle = Enum.GetValues(typeof(OrderHistoryEnum)).Cast<OrderHistoryEnum>().Where(x => (int)x == item_.StatusId).Select(x => x.ToString()).ToList()[0];
							item_.StatusTitle = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.OrderStatus." + item_.StatusTitle + "Txt", languageId, dbcontext);
							// item_.StatusTime = localVersion.ToString("hh:mm tt");
						}
					}
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = orderDetails;

				}
				else
				{
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.NoOrderOrderedYet", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 400;
					customerAPIResponses.ResponseObj = null;
				}


				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

		[Route("~/api/v2.1/OrderTrackingDetailsByOrderID")]
		[HttpPost]
		public CustomerAPIResponses OrderDetailsByOrderIDV2_1([FromBody] OrderReviewModel orderReviewModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(orderReviewModel.StoreId, orderReviewModel.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (orderReviewModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(orderReviewModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Customer Id Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.OrderId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Order Id Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.OrderIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(orderReviewModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == orderReviewModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Invalid Authentication key";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				string symbol = Helper.GetStoreCurrency(orderReviewModel.StoreId, dbcontext);
				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault().Value;
				var orderDetails = (from a in dbcontext.Order
									where a.Id == orderReviewModel.OrderId
									&& a.CustomerId == orderReviewModel.CustomerId
									select new OrderTrackingModel
									{
										OrderId = a.Id,
										OrderDate = a.CreatedOnUtc,
										OrderNumber = a.CustomOrderNumber,
										Amount = a.OrderTotal.ToString(),
										SubAmount = a.OrderSubtotalInclTax.ToString(),
										OrderStatus = a.OrderStatusId.ToString(),
										DiscountAmount = a.OrderDiscount.ToString(),
										MerchantAddressType = dbcontext.Vendor.Where(x => x.AddressId == a.PickupAddressId).FirstOrDefault().Name,
										BillingAddressType = (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault() != null ? ((dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Home) ? "Home" : ((dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Office) ? "Office" : "Other" : "Other"),
										ShippingAddressType = (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault() != null ? ((dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Home) ? "Home" : ((dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Office) ? "Office" : "Other" : "Other"),
										Tax = a.OrderTax.ToString(),
										BillingAddress = (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().Address1) + " " + (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().Address2) : ""),
										ShippingAddress = (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().Address1) + " " + (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().Address2) : ""),
										CooknChefAddress = dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault().Address1) + " " + (dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault().Address2 != null ?
										dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault().Address2 : "") : "",
										OrderedItems = (from c in dbcontext.OrderItem
														join d in dbcontext.Product
														on c.ProductId equals d.Id
														where c.OrderId == a.Id
														select new OrderedItems
														{
															ProductName = d.Name,
															ProductQuantity = c.Quantity,
															ProductTotalPrice = c.PriceInclTax.ToString(),
															ProductPrice = c.UnitPriceInclTax.ToString(),
															ProductImage = dbcontext.ProductPictureMapping.Where(x => x.ProductId == d.Id).FirstOrDefault() != null ? dbcontext.ProductPictureMapping.Where(x => x.ProductId == d.Id).FirstOrDefault().PictureId.ToString() : "0"
														}).ToList(),
										OrderedStatusArray = (from c in dbcontext.Order
															  join d in dbcontext.OrderNote
															  on c.Id equals d.OrderId
															  where c.Id == a.Id
															  select new OrderedStatusArray
															  {
																  StatusId = d.OrderStatus != null ? d.OrderStatus.Value : 100,
																  StatusTitle = d.Note,
																  StatusDescription = d.Note,
																  StatusDate = d.CreatedOnUtc,
															  }).ToList(),
										OrderType = 3,
										IsOrderNotes = !string.IsNullOrEmpty((from _c in dbcontext.OrderDetails
																			  where _c.Id == a.OrderDetailId
																			  select new
																			  {
																				  _c
																			  }).FirstOrDefault()._c.OrderNote),
										OrderNotes = string.IsNullOrEmpty((from _c in dbcontext.OrderDetails
																		   where _c.Id == a.OrderDetailId
																		   select new
																		   {
																			   _c
																		   }).FirstOrDefault()._c.OrderNote) ? "" : (from _c in dbcontext.OrderDetails
																													 where _c.Id == a.OrderDetailId
																													 select new
																													 {
																														 _c
																													 }).FirstOrDefault()._c.OrderNote,

										Tip = a.OrderDetailId.ToString(),
										DeliveryCharges = a.OrderDetailId.ToString()

									}).ToList();
				if (orderDetails.Any())
				{
					foreach (var item in orderDetails)
					{
						int OrderDetailId = Convert.ToInt32(item.Tip ?? "0");
						item.Tip = "0";
						item.DeliveryCharges = "0";
						if (OrderDetailId > 0)
						{
							item.Tip = (dbcontext.OrderDetails.Where(x => x.Id == OrderDetailId).AsEnumerable().FirstOrDefault().Tip.HasValue) ?
											dbcontext.OrderDetails.Where(x => x.Id == OrderDetailId).AsEnumerable().FirstOrDefault().Tip.Value.ToString() : "0";
							item.DeliveryCharges = (dbcontext.OrderDetails.Where(x => x.Id == OrderDetailId).AsEnumerable().FirstOrDefault().DeliveryCharges.HasValue) ?
											dbcontext.OrderDetails.Where(x => x.Id == OrderDetailId).AsEnumerable().FirstOrDefault().DeliveryCharges.Value.ToString() : "0";
						}
						item.OrderDate = Helper.ConvertToUserTimeV2_1(item.OrderDate, DateTimeKind.Utc, dbcontext, orderReviewModel.CustomerId);
						item.SubAmount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.SubAmount));
						item.Tax = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Tax));
						item.Tip = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Tip));
						item.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.DeliveryCharges));
						item.PackingCharges = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.PackingCharges));
						item.Amount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Amount));
						item.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.DiscountAmount));
						foreach (var _item in item.OrderedItems)
						{
							_item.ProductTotalPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(_item.ProductTotalPrice));
							_item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(_item.ProductPrice));
							int pictureSize = 75;
							var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
							if (setting != null)
							{
								pictureSize = Convert.ToInt32(setting.Value);
							}
							if (!string.IsNullOrEmpty(_item.ProductImage))
							{
								if (_item.ProductImage != "0")
								{
									int pictureId = Convert.ToInt32(_item.ProductImage);
									var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();
									string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
									string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
									? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}"
									: $"{pictureId:0000000}_{pictureSize}.{lastPart}";
									_item.ProductImage = ChefBaseUrl + thumbFileName;
								}
								else
								{
									_item.ProductImage = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
								}
							}
							else
							{
								_item.ProductImage = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
							}

						}
						int OrderStatusId = Convert.ToInt32(item.OrderStatus);
						item.OrderStatus = Enum.GetValues(typeof(OrderHistoryEnum)).Cast<OrderHistoryEnum>().Where(x => (int)x == OrderStatusId).Select(x => x.ToString()).ToList()[0];
						item.OrderStatus = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.OrderStatus." + item.OrderStatus + "Txt", languageId, dbcontext);
						foreach (var item_ in item.OrderedStatusArray)
						{
							if (item_.StatusId == 100)
							{
								if (item_.StatusTitle.ToLower().Contains("confirmed"))
								{
									item_.StatusId = (int)OrderHistoryEnum.Confirmed;
								}
								else if (item_.StatusTitle.ToLower().Contains("prepar"))
								{
									item_.StatusId = (int)OrderHistoryEnum.Preparing;
								}
								else if (item_.StatusTitle.ToLower().Contains("pick"))
								{
									item_.StatusId = (int)OrderHistoryEnum.Picked;
								}
								else if (item_.StatusTitle.ToLower().Contains("deliver"))
								{
									item_.StatusId = (int)OrderHistoryEnum.OrderDelivered;
								}
								else if (item_.StatusTitle.ToLower().Contains("placed"))
								{
									item_.StatusId = (int)OrderHistoryEnum.OrderDelivered;
								}
							}
							item_.StatusDate = Helper.ConvertToUserTimeV2_1(item_.StatusDate, DateTimeKind.Utc, dbcontext, orderReviewModel.CustomerId);
							if (item_.StatusId != (int)OrderHistoryEnum.Received)
							{
								DateTime iKnowThisIsUtc = item_.StatusDate;
								DateTime runtimeKnowsThisIsUtc = DateTime.SpecifyKind(
									iKnowThisIsUtc,
									DateTimeKind.Utc);
								DateTime localVersion = runtimeKnowsThisIsUtc.ToLocalTime();
								item_.StatusTime = localVersion.ToString("hh:mm tt");
							}
							else
							{
								item_.StatusTime = item_.StatusDate.ToString("hh:mm tt");
							}

							item_.StatusTitle = Enum.GetValues(typeof(OrderHistoryEnum)).Cast<OrderHistoryEnum>().Where(x => (int)x == item_.StatusId).Select(x => x.ToString()).ToList()[0];
							item_.StatusTitle = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.OrderStatus." + item_.StatusTitle + "Txt", languageId, dbcontext);
							// item_.StatusTime = localVersion.ToString("hh:mm tt");
						}
					}
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = orderDetails;

				}
				else
				{
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.NoOrderOrderedYet", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 400;
					customerAPIResponses.ResponseObj = null;
				}
				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}


		[Route("~/api/v2.2/OrderTrackingDetailsByOrderID")]
		[HttpPost]
		public CustomerAPIResponses OrderDetailsByOrderIDV2_2([FromBody] OrderReviewModel orderReviewModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(orderReviewModel.StoreId, orderReviewModel.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (orderReviewModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(orderReviewModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Customer Id Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.OrderId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Order Id Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.OrderIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(orderReviewModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == orderReviewModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Invalid Authentication key";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var merchantId = (from a in dbcontext.Order.AsEnumerable()
								  join b in dbcontext.OrderItem.AsEnumerable()
								  on a.Id equals b.OrderId
								  join c in dbcontext.Product
								  on b.ProductId equals c.Id
								  where a.Id == orderReviewModel.OrderId
								  select new
								  {
									  c.VendorId
								  }).FirstOrDefault();
				var agentPicture = dbcontext.AgentOrderStatus.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId).FirstOrDefault();
				var vendorpicture = dbcontext.Vendor.Where(x => x.Id == merchantId.VendorId).FirstOrDefault()?.PictureId ?? 0;
				var openForReview = !dbcontext.RatingReviews.Where(x => x.CustomerId == orderReviewModel.CustomerId && x.OrderId == orderReviewModel.OrderId && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item).Any();
				string symbol = Helper.GetStoreCurrency(orderReviewModel.StoreId, dbcontext);
				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault().Value;
				var orderDetails = (from a in dbcontext.Order
									where a.Id == orderReviewModel.OrderId
									&& a.CustomerId == orderReviewModel.CustomerId
									select new OrderTrackingModel
									{
										OrderId = a.Id,
										OrderDate = a.CreatedOnUtc,
										OrderNumber = a.CustomOrderNumber,
										Amount = a.OrderTotal.ToString(),
										SubAmount = a.OrderSubtotalInclTax.ToString(),
										OrderStatus = a.OrderStatusId.ToString(),
										DiscountAmount = a.OrderDiscount.ToString(),
										MerchantAddressType = dbcontext.Vendor.Where(x => x.AddressId == a.PickupAddressId).FirstOrDefault().Name,
										BillingAddressType = (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault() != null ? ((dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Home) ? "Home" : ((dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Office) ? "Office" : "Other" : "Other"),
										ShippingAddressType = (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault() != null ? ((dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Home) ? "Home" : ((dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Office) ? "Office" : "Other" : "Other"),
										CustomerName = (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().FirstName) + " " + (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().LastName) : ""),
										PhoneNumber = (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().PhoneNumber) : ""),
										Tax = a.OrderTax.ToString(),
										BillingAddress = (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().Address1) + " " + (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().Address2) : ""),
										ShippingAddress = (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().Address1) + " " + (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().Address2) : ""),
										CooknChefAddress = dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault().Address1) + " " + (dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault().Address2 != null ?
										dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault().Address2 : "") : "",
										OrderedItems = (from c in dbcontext.OrderItem
														join d in dbcontext.Product
														on c.ProductId equals d.Id
														where c.OrderId == a.Id
														select new OrderedItems
														{
															ProductId = d.Id,
															RatingEnable = openForReview,
															ProductDescription = d.ShortDescription,
															ProductName = d.Name,
															ProductQuantity = c.Quantity,
															ProductTotalPrice = c.PriceInclTax.ToString(),
															ProductPrice = c.UnitPriceInclTax.ToString(),
															ProductImage = dbcontext.ProductPictureMapping.Where(x => x.ProductId == d.Id).FirstOrDefault() != null ? dbcontext.ProductPictureMapping.Where(x => x.ProductId == d.Id).FirstOrDefault().PictureId.ToString() : "0",
															BrandName = (from sao in dbcontext.SpecificationAttributeOption
																		 join psm in dbcontext.ProductSpecificationAttributeMapping
																		 on sao.Id equals psm.SpecificationAttributeOptionId
																		 where psm.ProductId == d.Id
																		 select sao.Name).FirstOrDefault(),
															IsItemReviewFound = Helper.IsItemReviewEnabledAndFound(orderReviewModel.StoreId, orderReviewModel.OrderId, d.Id, dbcontext)
														}).ToList(),
										OrderedStatusArray = (from c in dbcontext.Order
															  join d in dbcontext.OrderNote
															  on c.Id equals d.OrderId
															  where c.Id == a.Id && !d.Note.Contains("On the way")
															  select new OrderedStatusArray
															  {
																  StatusId = d.OrderStatus != null ? d.OrderStatus.Value : 100,
																  StatusTitle = d.Note,
																  StatusDescription = d.Note,
																  StatusDate = d.CreatedOnUtc,
															  }).ToList(),
										OrderType = 3,
										IsOrderNotes = !string.IsNullOrEmpty((from _c in dbcontext.OrderDetails
																			  where _c.Id == a.OrderDetailId
																			  select new
																			  {
																				  _c
																			  }).FirstOrDefault()._c.OrderNote),
										OrderNotes = string.IsNullOrEmpty((from _c in dbcontext.OrderDetails
																		   where _c.Id == a.OrderDetailId
																		   select new
																		   {
																			   _c
																		   }).FirstOrDefault()._c.OrderNote) ? "" : (from _c in dbcontext.OrderDetails
																													 where _c.Id == a.OrderDetailId
																													 select new
																													 {
																														 _c
																													 }).FirstOrDefault()._c.OrderNote,
										AgentOrderNotes = (from c in dbcontext.Order
														   join d in dbcontext.OrderNote
														   on c.Id equals d.OrderId
														   where c.Id == a.Id && d.Note.Contains("Comments for Agent")
														   select d.Note).FirstOrDefault(),

										Tip = a.OrderDetailId.ToString(),
										DeliveryCharges = Convert.ToString(a.DeliveryAmount),
										ServiceChargeAmount = Convert.ToString(a.ServiceChargeAmount),


									}).ToList();
				if (orderDetails.Any())
				{
					foreach (var item in orderDetails)
					{

						int OrderDetailId = Convert.ToInt32(item.Tip ?? "0");
						item.Tip = "0";

						if (OrderDetailId > 0)
						{
							item.Tip = (dbcontext.OrderDetails.Where(x => x.Id == OrderDetailId).AsEnumerable().FirstOrDefault().Tip.HasValue) ?
											dbcontext.OrderDetails.Where(x => x.Id == OrderDetailId).AsEnumerable().FirstOrDefault().Tip.Value.ToString() : "0";
						}
						item.OrderDate = Helper.ConvertToUserTime(item.OrderDate, DateTimeKind.Utc, dbcontext);
						item.SubAmount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.SubAmount));
						item.Tax = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Tax));
						item.Tip = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Tip));
						item.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.DeliveryCharges));
						item.ServiceChargeAmount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ServiceChargeAmount));
						item.PackingCharges = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.PackingCharges));
						item.Amount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Amount));
						item.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.DiscountAmount));
						#region Rating
						item.MerchantComment = dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant).FirstOrDefault()?.ReviewText ?? "";
						item.MerchantResource = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.Merchant.HeaderText", languageId, dbcontext);
						item.AgentResource = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.Agent.HeaderText", languageId, dbcontext);
						item.AgentComment = dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Agent).FirstOrDefault()?.ReviewText ?? "";
						item.MerchantRating = (!(from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant)

												 select new
												 {
													 rating.Rating
												 }).Any()) ? 0 :
													   (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant)
														select new
														{
															rating.Rating
														}).ToList().Sum(x => x.Rating);

						item.MerchantRatingCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant)
													select new
													{
														rating.Rating
													}).Count();

						item.AgentRating = (!(from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Agent)
											  select new
											  {
												  rating.Rating
											  }).Any()) ? 0 :
					   (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Agent)
						select new
						{
							rating.Rating
						}).ToList().Sum(x => x.Rating);

						item.AgentRatingCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Agent)
												 select new
												 {
													 rating.Rating
												 }).Count();

						if (item.MerchantRating != 0)
						{
							item.MerchantRating = Convert.ToDecimal(item.MerchantRating / item.MerchantRatingCount);
							item.MerchantRating = Math.Round(item.MerchantRating, 2);
						}
						if (item.AgentRating != 0)
						{
							item.MerchantRating = Convert.ToDecimal(item.AgentRating / item.AgentRatingCount);
							item.MerchantRating = Math.Round(item.AgentRating, 2);
						}
						int pictureSize = 100;
						var setting = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("mediasettings.vendorthumbpicturesize")).FirstOrDefault();
						if (setting != null)
						{
							pictureSize = Convert.ToInt32(setting.Value);
						}
						if (vendorpicture != 0)
						{
							int pictureId = Convert.ToInt32(vendorpicture);
							var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();
							string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
							string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
							? $"{pictureImage.Id:0000000}_{pictureImage.SeoFilename}.{lastPart}"
							: $"{pictureImage.Id:0000000}_{pictureSize}.{lastPart}";
							item.MerchantImage = ChefBaseUrl + thumbFileName;
						}
						else
						{
							item.MerchantImage = ChefBaseUrl + "default-image_" + pictureSize + ".png";
						}
						if (agentPicture != null)
						{
							item.AgentImage = dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agentPicture.AgentId.Value && x.Key == "ProfileURL").FirstOrDefault() != null ?
															  dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agentPicture.AgentId.Value && x.Key == "ProfileURL").FirstOrDefault().Value :
															  ChefBaseUrl + "default-image_450.png";
						}
						else
						{
							item.AgentImage = ChefBaseUrl + "default-image_450.png";
						}
						#endregion Rating
						foreach (var _item in item.OrderedItems)
						{
							_item.ProductTotalPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(_item.ProductTotalPrice));
							_item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(_item.ProductPrice));
							pictureSize = 75;
							setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
							if (setting != null)
							{
								pictureSize = Convert.ToInt32(setting.Value);
							}
							if (!string.IsNullOrEmpty(_item.ProductImage))
							{
								if (_item.ProductImage != "0")
								{
									int pictureId = Convert.ToInt32(_item.ProductImage);
									var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();
									string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
									string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
									? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}"
									: $"{pictureId:0000000}_{pictureSize}.{lastPart}";
									_item.ProductImage = ChefBaseUrl + thumbFileName;
								}
								else
								{
									_item.ProductImage = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
								}
							}
							else
							{
								_item.ProductImage = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
							}

						}


						int OrderStatusId = Convert.ToInt32(item.OrderStatus);
						item.OrderStatus = Enum.GetValues(typeof(OrderHistoryEnum)).Cast<OrderHistoryEnum>().Where(x => (int)x == OrderStatusId).Select(x => x.ToString()).ToList()[0];
						item.OrderStatus = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.OrderStatus." + item.OrderStatus + "Txt", languageId, dbcontext);
						foreach (var item_ in item.OrderedStatusArray)
						{
							if (item_.StatusId == 100)
							{
								if (item_.StatusTitle.ToLower().Contains("confirmed"))
								{
									item_.StatusId = (int)OrderHistoryEnum.Confirmed;
								}
								else if (item_.StatusTitle.ToLower().Contains("prepar"))
								{
									item_.StatusId = (int)OrderHistoryEnum.Preparing;
								}
								else if (item_.StatusTitle.ToLower().Contains("pick"))
								{
									item_.StatusId = (int)OrderHistoryEnum.Picked;
								}
								else if (item_.StatusTitle.ToLower().Contains("deliver"))
								{
									item_.StatusId = (int)OrderHistoryEnum.OrderDelivered;
								}
								else if (item_.StatusTitle.ToLower().Contains("placed"))
								{
									item_.StatusId = (int)OrderHistoryEnum.OrderDelivered;
								}
								else if (item_.StatusTitle.ToLower().Contains("cancelled"))
								{
									item_.StatusId = (int)OrderHistoryEnum.Cancelled;
								}
								else if (item_.StatusTitle.ToLower().Contains("received"))
								{
									item_.StatusId = (int)OrderHistoryEnum.Received;
								}

							}
							item_.StatusDate = Helper.ConvertToUserTime(item_.StatusDate, DateTimeKind.Utc, dbcontext);
							if (item_.StatusId != (int)OrderHistoryEnum.Received)
							{
								DateTime iKnowThisIsUtc = item_.StatusDate;
								DateTime runtimeKnowsThisIsUtc = DateTime.SpecifyKind(
									iKnowThisIsUtc,
									DateTimeKind.Utc);
								DateTime localVersion = runtimeKnowsThisIsUtc.ToLocalTime();
								item_.StatusTime = localVersion.ToString("hh:mm tt");
							}
							else
							{
								item_.StatusTime = item_.StatusDate.ToString("hh:mm tt");
							}

							item_.StatusTitle = Enum.GetValues(typeof(OrderHistoryEnum)).Cast<OrderHistoryEnum>().Where(x => (int)x == item_.StatusId).Select(x => x.ToString()).ToList()[0];
							item_.StatusTitle = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.OrderStatus." + item_.StatusTitle + "Txt", languageId, dbcontext);
							// item_.StatusTime = localVersion.ToString("hh:mm tt");
						}
					}
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = orderDetails;

				}
				else
				{
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.NoOrderOrderedYet", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 400;
					customerAPIResponses.ResponseObj = null;
				}
				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v2.3/OrderTrackingDetailsByOrderID")]
		[HttpPost]
		public CustomerAPIResponses OrderDetailsByOrderIDV2_3([FromBody] OrderReviewModel orderReviewModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(orderReviewModel.StoreId, orderReviewModel.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (orderReviewModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(orderReviewModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Customer Id Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.OrderId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Order Id Not Defined";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.OrderIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(orderReviewModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == orderReviewModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Invalid Authentication key";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var merchantId = (from a in dbcontext.Order.AsEnumerable()
								  join b in dbcontext.OrderItem.AsEnumerable()
								  on a.Id equals b.OrderId
								  join c in dbcontext.Product
								  on b.ProductId equals c.Id
								  where a.Id == orderReviewModel.OrderId
								  select new
								  {
									  c.VendorId
								  }).FirstOrDefault();
				var agentPicture = dbcontext.AgentOrderStatus.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId).FirstOrDefault();
				var vendorpicture = dbcontext.Vendor.Where(x => x.Id == merchantId.VendorId).FirstOrDefault()?.PictureId ?? 0;
				var openForReview = !dbcontext.RatingReviews.Where(x => x.CustomerId == orderReviewModel.CustomerId && x.OrderId == orderReviewModel.OrderId && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item).Any();
				string symbol = Helper.GetStoreCurrency(orderReviewModel.StoreId, dbcontext);
				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault().Value;
				var orderDetails = (from a in dbcontext.Order
									where a.Id == orderReviewModel.OrderId
									&& a.CustomerId == orderReviewModel.CustomerId
									select new OrderTrackingModelV1
									{
										OrderId = a.Id,
										OrderDate = a.CreatedOnUtc,
										OrderNumber = a.CustomOrderNumber,
										Amount = a.OrderTotal.ToString(),
										SubAmount = a.OrderSubtotalInclTax.ToString(),
										OrderStatus = a.OrderStatusId.ToString(),
										DiscountAmount = a.OrderDiscount.ToString(),
										MerchantAddressType = dbcontext.Vendor.Where(x => x.AddressId == a.PickupAddressId).FirstOrDefault().Name,
										BillingAddressType = (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault() != null ? ((dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Home) ? "Home" : ((dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Office) ? "Office" : "Other" : "Other"),
										ShippingAddressType = (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault() != null ? ((dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Home) ? "Home" : ((dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().AddressTypeId) == (int)AddressTypeEnum.Office) ? "Office" : "Other" : "Other"),
										CustomerName = (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().FirstName) + " " + (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().LastName) : ""),
										PhoneNumber = (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().PhoneNumber) : ""),
										Tax = a.OrderTax.ToString(),
										BillingAddress = (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().Address1) + " " + (dbcontext.Address.Where(x => x.Id == a.BillingAddressId).FirstOrDefault().Address2) : ""),
										ShippingAddress = (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().Address1) + " " + (dbcontext.Address.Where(x => x.Id == a.ShippingAddressId).FirstOrDefault().Address2) : ""),
										CooknChefAddress = dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault() != null ? (dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault().Address1) + " " + (dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault().Address2 != null ?
										dbcontext.Address.Where(x => x.Id == a.PickupAddressId).FirstOrDefault().Address2 : "") : "",
										OrderedItems = (from c in dbcontext.OrderItem
														join d in dbcontext.Product
														on c.ProductId equals d.Id
														where c.OrderId == a.Id
														select new OrderedItems
														{
															ProductId = d.Id,
															RatingEnable = openForReview,
															ProductDescription = d.ShortDescription,
															ProductName = d.Name,
															ProductQuantity = c.Quantity,
															ProductTotalPrice = c.PriceInclTax.ToString(),
															ProductPrice = c.UnitPriceInclTax.ToString(),
															ProductImage = dbcontext.ProductPictureMapping.Where(x => x.ProductId == d.Id).FirstOrDefault() != null ? dbcontext.ProductPictureMapping.Where(x => x.ProductId == d.Id).FirstOrDefault().PictureId.ToString() : "0",
															BrandName = (from sao in dbcontext.SpecificationAttributeOption
																		 join psm in dbcontext.ProductSpecificationAttributeMapping
																		 on sao.Id equals psm.SpecificationAttributeOptionId
																		 where psm.ProductId == d.Id
																		 select sao.Name).FirstOrDefault(),
															IsItemReviewFound = Helper.IsItemReviewEnabledAndFound(orderReviewModel.StoreId, orderReviewModel.OrderId, d.Id, dbcontext)
														}).ToList(),

										OrderType = Convert.ToInt32((from od in dbcontext.OrderDetails
																	 where a.OrderDetailId == od.Id
																	 select od.OrderType).FirstOrDefault()),
										IsOrderNotes = !string.IsNullOrEmpty((from _c in dbcontext.OrderDetails
																			  where _c.Id == a.OrderDetailId
																			  select new
																			  {
																				  _c
																			  }).FirstOrDefault()._c.OrderNote),
										OrderNotes = string.IsNullOrEmpty((from _c in dbcontext.OrderDetails
																		   where _c.Id == a.OrderDetailId
																		   select new
																		   {
																			   _c
																		   }).FirstOrDefault()._c.OrderNote) ? "" : (from _c in dbcontext.OrderDetails
																													 where _c.Id == a.OrderDetailId
																													 select new
																													 {
																														 _c
																													 }).FirstOrDefault()._c.OrderNote,
										AgentOrderNotes = (from c in dbcontext.Order
														   join d in dbcontext.OrderNote
														   on c.Id equals d.OrderId
														   where c.Id == a.Id && d.Note.Contains("Comments for Agent")
														   select d.Note).FirstOrDefault(),

										Tip = a.OrderDetailId.ToString(),
										DeliveryCharges = Convert.ToString(a.DeliveryAmount),
										ServiceChargeAmount = Convert.ToString(a.ServiceChargeAmount),


									}).ToList();
				if (orderDetails.Any())
				{
					foreach (var item in orderDetails)
					{
						//OrderType: 1 = "Delivery", 2 = "Takeaway"
						item.OrderType = item.OrderType == 1 ? 1 : 2;

						#region OrderStatusArray

						//enum sequence maintainer
						List<int> EnumSortedList = new List<int>() { 10, 20, 25, 30, 3, 4, 40 };
						var orderNotes = dbcontext.OrderNote.Where(x => x.OrderId == orderReviewModel.OrderId).ToList();
						var orderstatusArray = Enum.GetValues(typeof(OrderHistoryEnumV1)).Cast<int>().ToList();
						orderstatusArray = orderstatusArray.OrderBy(d => EnumSortedList.IndexOf(d)).ToList();

						//Need to exclude some order status for Takeaway ordertype
						if (item.OrderType == 2)
						{
							var takeAwayExcludeOdStatusIds = new List<int>()
							{
								(int)OrderHistoryEnum.RiderAssigned,
								(int)OrderHistoryEnum.Picked,
							};

							//exclude defined status
							orderstatusArray = orderstatusArray.Where(x => !takeAwayExcludeOdStatusIds.Contains(x)).ToList();
						}

						//get required objects to prepare minidescription 
						var vendorId = dbcontext.Product.FirstOrDefault(p => p.Id == item.OrderedItems.FirstOrDefault().ProductId)?.VendorId ?? 0;
						var vendor = dbcontext.Vendor.FirstOrDefault(v => v.Id == vendorId);
						var assignedAgent = dbcontext.AgentOrderStatus.FirstOrDefault(ag => ag.OrderId == item.OrderId);

						List<OrderedStatusArrayV1> orderedStatusArrays = new List<OrderedStatusArrayV1>();
						foreach (var status in orderstatusArray)
						{
							DateTime? dt = null;
							var statusName = Enum.GetValues(typeof(OrderHistoryEnumV1)).Cast<OrderHistoryEnumV1>().Where(x => (int)x == (int)status).Select(x => x.ToString()).ToList()[0];
							var ordernoterecord = orderNotes.FirstOrDefault(x => x.Note.Contains(statusName));

							var orderstatus = new OrderedStatusArrayV1()
							{
								StatusId = Convert.ToInt32(status),
								StatusTitle = statusName,
								StatusDescription = ordernoterecord != null ? ordernoterecord.Note : string.Empty,
								MiniDescription = _orderServices.GetStatusMiniDescription(status, orderReviewModel.StoreId, languageId, vendor, assignedAgent?.AgentId ?? 0),
								StatusDate = ordernoterecord != null ? Helper.ConvertToUserTime(ordernoterecord.CreatedOnUtc, DateTimeKind.Utc, dbcontext) : dt,
								IsUpdate = ordernoterecord != null ? true : false,
							};

							orderstatus.StatusTime = ordernoterecord != null ? orderstatus.StatusDate?.ToString("hh:mm tt") : string.Empty;
							orderedStatusArrays.Add(orderstatus);

						}
						item.OrderedStatusArray = orderedStatusArrays;
						#endregion End Of OrderStatusArray

						int OrderDetailId = Convert.ToInt32(item.Tip ?? "0");
						item.Tip = "0";

						if (OrderDetailId > 0)
						{
							item.Tip = (dbcontext.OrderDetails.Where(x => x.Id == OrderDetailId).AsEnumerable().FirstOrDefault().Tip.HasValue) ?
											dbcontext.OrderDetails.Where(x => x.Id == OrderDetailId).AsEnumerable().FirstOrDefault().Tip.Value.ToString() : "0";
						}
						item.OrderDate = Helper.ConvertToUserTime(item.OrderDate, DateTimeKind.Utc, dbcontext);
						item.SubAmount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.SubAmount));
						item.Tax = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Tax));
						item.Tip = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Tip));
						item.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.DeliveryCharges));
						item.ServiceChargeAmount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ServiceChargeAmount));
						item.PackingCharges = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.PackingCharges));
						item.Amount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Amount));
						item.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.DiscountAmount));

						#region Rating
						item.MerchantComment = dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant).FirstOrDefault()?.ReviewText ?? "";
						item.MerchantResource = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.Merchant.HeaderText", languageId, dbcontext);
						item.AgentResource = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.Agent.HeaderText", languageId, dbcontext);
						item.AgentComment = dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Agent).FirstOrDefault()?.ReviewText ?? "";
						item.MerchantRating = (!(from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant)

												 select new
												 {
													 rating.Rating
												 }).Any()) ? 0 :
													   (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant)
														select new
														{
															rating.Rating
														}).ToList().Sum(x => x.Rating);

						item.MerchantRatingCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant)
													select new
													{
														rating.Rating
													}).Count();

						item.AgentRating = (!(from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Agent)
											  select new
											  {
												  rating.Rating
											  }).Any()) ? 0 :
					   (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Agent)
						select new
						{
							rating.Rating
						}).ToList().Sum(x => x.Rating);

						item.AgentRatingCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.OrderId == orderReviewModel.OrderId && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Agent)
												 select new
												 {
													 rating.Rating
												 }).Count();

						if (item.MerchantRating != 0)
						{
							item.MerchantRating = Convert.ToDecimal(item.MerchantRating / item.MerchantRatingCount);
							item.MerchantRating = Math.Round(item.MerchantRating, 2);
						}
						if (item.AgentRating != 0)
						{
							item.AgentRating = Convert.ToDecimal(item.AgentRating / item.AgentRatingCount);
							item.AgentRating = Math.Round(item.AgentRating, 2);
						}
						int pictureSize = 100;
						var setting = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("mediasettings.vendorthumbpicturesize")).FirstOrDefault();
						if (setting != null)
						{
							pictureSize = Convert.ToInt32(setting.Value);
						}
						if (vendorpicture != 0)
						{
							int pictureId = Convert.ToInt32(vendorpicture);
							var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();
							string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
							string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
							? $"{pictureImage.Id:0000000}_{pictureImage.SeoFilename}.{lastPart}"
							: $"{pictureImage.Id:0000000}_{pictureSize}.{lastPart}";
							item.MerchantImage = ChefBaseUrl + thumbFileName;
						}
						else
						{
							item.MerchantImage = ChefBaseUrl + "default-image_" + pictureSize + ".png";
						}
						if (agentPicture != null)
						{
							item.AgentImage = dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agentPicture.AgentId.Value && x.Key == "ProfileURL").FirstOrDefault() != null ?
															  dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == agentPicture.AgentId.Value && x.Key == "ProfileURL").FirstOrDefault().Value :
															  ChefBaseUrl + "default-image_450.png";
						}
						else
						{
							item.AgentImage = ChefBaseUrl + "default-image_450.png";
						}
						#endregion Rating
						foreach (var _item in item.OrderedItems)
						{
							_item.ProductTotalPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(_item.ProductTotalPrice));
							_item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(_item.ProductPrice));
							pictureSize = 75;
							setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
							if (setting != null)
							{
								pictureSize = Convert.ToInt32(setting.Value);
							}
							if (!string.IsNullOrEmpty(_item.ProductImage))
							{
								if (_item.ProductImage != "0")
								{
									int pictureId = Convert.ToInt32(_item.ProductImage);
									var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();
									string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
									string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
									? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}"
									: $"{pictureId:0000000}_{pictureSize}.{lastPart}";
									_item.ProductImage = ChefBaseUrl + thumbFileName;
								}
								else
								{
									_item.ProductImage = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
								}
							}
							else
							{
								_item.ProductImage = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
							}

						}


						int OrderStatusId = Convert.ToInt32(item.OrderStatus);
						item.OrderStatus = Enum.GetValues(typeof(OrderHistoryEnumV1)).Cast<OrderHistoryEnumV1>().Where(x => (int)x == OrderStatusId).Select(x => x.ToString()).ToList()[0];
						item.OrderStatus = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.OrderStatus." + item.OrderStatus + "Txt", languageId, dbcontext);
						foreach (var item_ in item.OrderedStatusArray)
						{
							if (item_.StatusId == 100)
							{
								if (item_.StatusTitle.ToLower().Contains("confirmed"))
								{
									item_.StatusId = (int)OrderHistoryEnumV1.Confirmed;
								}
								else if (item_.StatusTitle.ToLower().Contains("prepar"))
								{
									item_.StatusId = (int)OrderHistoryEnumV1.Prepared;
								}
								else if (item_.StatusTitle.ToLower().Contains("pick"))
								{
									item_.StatusId = (int)OrderHistoryEnumV1.Picked;
								}
								else if (item_.StatusTitle.ToLower().Contains("deliver"))
								{
									item_.StatusId = (int)OrderHistoryEnumV1.Delivered;
								}
								else if (item_.StatusTitle.ToLower().Contains("placed"))
								{
									item_.StatusId = (int)OrderHistoryEnumV1.Delivered;
								}
								else if (item_.StatusTitle.ToLower().Contains("cancelled"))
								{
									item_.StatusId = (int)OrderHistoryEnumV1.Cancelled;
								}
								else if (item_.StatusTitle.ToLower().Contains("received"))
								{
									item_.StatusId = (int)OrderHistoryEnumV1.Received;
								}

							}
							//item_.StatusDate = Helper.ConvertToUserTime(item_.StatusDate, DateTimeKind.Utc, dbcontext);
							//if (item_.StatusId != (int)OrderHistoryEnum.Received)
							//{
							//	DateTime iKnowThisIsUtc = item_.StatusDate;
							//	DateTime runtimeKnowsThisIsUtc = DateTime.SpecifyKind(
							//		iKnowThisIsUtc,
							//		DateTimeKind.Utc);
							//	DateTime localVersion = runtimeKnowsThisIsUtc.ToLocalTime();
							//	item_.StatusTime = localVersion.ToString("hh:mm tt");
							//}
							//else
							//{
							//	item_.StatusTime = item_.StatusDate.ToString("hh:mm tt");
							//}

							item_.StatusTitle = Enum.GetValues(typeof(OrderHistoryEnumV1)).Cast<OrderHistoryEnumV1>().Where(x => (int)x == item_.StatusId).Select(x => x.ToString()).ToList()[0];
							item_.StatusTitle = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.OrderStatus." + item_.StatusTitle + "Txt", languageId, dbcontext);
							// item_.StatusTime = localVersion.ToString("hh:mm tt");
						}

						#region Live tracking model					

						//prepare live tracking model
						item.LiveTrackingModel = _orderServices.PrepareLiveTrackingModel(item, orderReviewModel.StoreId, languageId);

						#endregion

						#region Average rating and phone

						if (vendorId > 0 && vendor != null)
						{
							var avgMerchantRating = _orderServices.GetAverageRating(orderReviewModel.StoreId, vendorId, (int)NHKCustomerApplication.Utilities.ReviewType.Merchant, out int merchantReviewCount);
							item.MerchantAveragRating = avgMerchantRating;
							item.MerchantTotalRatingCount = merchantReviewCount;
							//phone
							item.MerhantPhone = dbcontext.Address.Where(x => x.Id == vendor.AddressId).FirstOrDefault()?.PhoneNumber ?? string.Empty;
						}
						if (assignedAgent != null)
						{
							var assignedAgentRating = _orderServices.GetAverageRating(orderReviewModel.StoreId, assignedAgent.Id, (int)NHKCustomerApplication.Utilities.ReviewType.Agent, out int agentReviewCount);
							item.AgentAveragRating = assignedAgentRating;
							item.AgentTotalRatingCount = agentReviewCount;
							//phone
							var phoneGenericAttr = dbcontext.GenericAttribute.FirstOrDefault(x => x.Key == "Phone" && x.EntityId == assignedAgent.AgentId);
							if (phoneGenericAttr != null)
							{
								item.AgentPhone = phoneGenericAttr.Value;
							}
						}

						#endregion
					}
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = orderDetails;

				}
				else
				{
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.NoOrderOrderedYet", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 400;
					customerAPIResponses.ResponseObj = null;
				}
				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}


		[Route("~/api/v1/GetNotifications")]
		[HttpPost]
		public CustomerAPIResponses GetNotifications([FromBody] NotificationModel orderReviewModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(orderReviewModel.StoreId, orderReviewModel.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (orderReviewModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(orderReviewModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(orderReviewModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == orderReviewModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Invalid Authentication key";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				string symbol = Helper.GetStoreCurrency(orderReviewModel.StoreId, dbcontext);
				var noOfNotifications = Convert.ToInt32(dbcontext.Setting.Where(x => x.Name.Contains("setting.customer.noOfNotifications") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault()?.Value ?? "20");

				var orderNotesNotifiactionDetails = (from order in dbcontext.Order
													 where order.CustomerId == orderReviewModel.CustomerId
													 && order.StoreId == orderReviewModel.StoreId
													 select new OrderNotesNotifiactionModel
													 {
														 OrderId = order.Id,
														 OrderStatus = order.OrderStatusId,
														 OrderNumber = order.CustomOrderNumber,
														 OrderDate = order.CreatedOnUtc,
														 OrderNotificationDateTime = order.CreatedOnUtc
													 }).OrderByDescending(x => x.OrderId).Take(noOfNotifications).ToList();
				if (orderNotesNotifiactionDetails.Any())
				{
					List<OrderNotesNotifiactionModel> List = new List<OrderNotesNotifiactionModel>();
					var orderResourceValue = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.Order.Txt", languageId, dbcontext);
					#region Delivery date and time in helper
					Helper.DeliveryDateTime(orderNotesNotifiactionDetails, orderReviewModel.CustomerId, orderReviewModel.StoreId, dbcontext);
					#endregion
					foreach (var item in orderNotesNotifiactionDetails)
					{
						item.OrderDate = Helper.ConvertToUserTime(item.OrderDate, DateTimeKind.Utc, dbcontext);
						item.OrderNotificationDateTime = Helper.ConvertToUserTime(item.OrderNotificationDateTime, DateTimeKind.Utc, dbcontext);
						item.OrderNotificationDate = item.OrderNotificationDateTime.ToString("dd MMM yyyy");
						item.OrderNotificationTime = item.OrderNotificationDateTime.ToString("hh:mm tt");
						item.Title = orderResourceValue + " #" + item.OrderNumber;
						var allOrderNotes = dbcontext.OrderNote.Where(x => x.OrderId == item.OrderId).ToList();
						if (item.OrderStatus == (int)OrderHistoryEnum.Received)
						{
							var CustomerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == orderReviewModel.CustomerId && x.StoreId == orderReviewModel.StoreId).ToList();
							var statusResourceValue = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.Template.ReceivedTxt", languageId, dbcontext);
							item.Description = statusResourceValue;
							item.Description = item.Description.Replace("%Customer.FullName%", CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).FirstOrDefault().Value + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).FirstOrDefault().Value : ""));
							item.Description = item.Description.Replace("%Customer.OrderNumber%", item.OrderNumber.ToString());
							var orderNotes = allOrderNotes.Where(x => x.Note.ToLower().Contains("placed") || x.Note.ToLower().Contains("received")).FirstOrDefault();
							if (orderNotes != null)
							{
								item.OrderNotificationDateTime = Helper.ConvertToUserTime(orderNotes.CreatedOnUtc, DateTimeKind.Utc, dbcontext);
								item.OrderNotificationDate = item.OrderNotificationDateTime.ToString("dd MMM yyyy");
								item.OrderNotificationTime = item.OrderNotificationDateTime.ToString("hh:mm tt");
							}

						}
						if (item.OrderStatus == (int)OrderHistoryEnum.Cancelled)
						{
							var CustomerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == orderReviewModel.CustomerId && x.StoreId == orderReviewModel.StoreId).ToList();
							var statusResourceValue = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.Template.CancelledTxt", languageId, dbcontext);
							item.Description = statusResourceValue;
							item.Description = item.Description.Replace("%Customer.FullName%", CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).FirstOrDefault().Value + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).FirstOrDefault().Value : ""));
							item.Description = item.Description.Replace("%Customer.OrderNumber%", item.OrderNumber.ToString());
							var orderNotes = allOrderNotes.Where(x => x.Note.ToLower().Contains("cancelled")).FirstOrDefault();
							if (orderNotes != null)
							{
								item.OrderNotificationDateTime = Helper.ConvertToUserTime(orderNotes.CreatedOnUtc, DateTimeKind.Utc, dbcontext);
								item.OrderNotificationDate = item.OrderNotificationDateTime.ToString("dd MMM yyyy");
								item.OrderNotificationTime = item.OrderNotificationDateTime.ToString("hh:mm tt");
							}
						}
						if (item.OrderStatus == (int)OrderHistoryEnum.Confirmed)
						{
							var CustomerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == orderReviewModel.CustomerId && x.StoreId == orderReviewModel.StoreId).ToList();
							var statusResourceValue = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.Template.ConfirmedTxt", languageId, dbcontext);
							item.Description = statusResourceValue;
							item.Description = item.Description.Replace("%Customer.FullName%", CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).FirstOrDefault().Value + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).FirstOrDefault().Value : ""));
							item.Description = item.Description.Replace("%Customer.OrderNumber%", item.OrderNumber.ToString());
							var orderNotes = allOrderNotes.Where(x => x.Note.ToLower().Contains("confirmed")).FirstOrDefault();
							if (orderNotes != null)
							{
								item.OrderNotificationDateTime = Helper.ConvertToUserTime(orderNotes.CreatedOnUtc, DateTimeKind.Utc, dbcontext);
								item.OrderNotificationDate = item.OrderNotificationDateTime.ToString("dd MMM yyyy");
								item.OrderNotificationTime = item.OrderNotificationDateTime.ToString("hh:mm tt");
							}

						}
						if (item.OrderStatus == (int)OrderHistoryEnum.Picked)
						{
							var CustomerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == orderReviewModel.CustomerId && x.StoreId == orderReviewModel.StoreId).ToList();
							var statusResourceValue = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.Template.PickedTxt", languageId, dbcontext);
							item.Description = statusResourceValue;
							item.Description = item.Description.Replace("%Customer.FullName%", CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).FirstOrDefault().Value + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).FirstOrDefault().Value : ""));
							item.Description = item.Description.Replace("%Customer.OrderNumber%", item.OrderNumber.ToString());
							var orderNotes = allOrderNotes.Where(x => x.Note.ToLower().Contains("picked")).FirstOrDefault();
							if (orderNotes != null)
							{
								item.OrderNotificationDateTime = Helper.ConvertToUserTime(orderNotes.CreatedOnUtc, DateTimeKind.Utc, dbcontext);
								item.OrderNotificationDate = item.OrderNotificationDateTime.ToString("dd MMM yyyy");
								item.OrderNotificationTime = item.OrderNotificationDateTime.ToString("hh:mm tt");
							}
						}
						if (item.OrderStatus == (int)OrderHistoryEnum.OrderDelivered)
						{
							var CustomerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == orderReviewModel.CustomerId && x.StoreId == orderReviewModel.StoreId).ToList();
							var statusResourceValue = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.Template.OrderDeliveredTxt", languageId, dbcontext);
							item.Description = statusResourceValue;
							item.Description = item.Description.Replace("%Customer.FullName%", CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).FirstOrDefault().Value + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).FirstOrDefault().Value : ""));
							item.Description = item.Description.Replace("%Customer.OrderNumber%", item.OrderNumber.ToString());
							var orderNotes = allOrderNotes.Where(x => x.Note.ToLower().Contains("delivered")).FirstOrDefault();
							if (orderNotes != null)
							{
								item.OrderNotificationDateTime = Helper.ConvertToUserTime(orderNotes.CreatedOnUtc, DateTimeKind.Utc, dbcontext);
								item.OrderNotificationDate = item.OrderNotificationDateTime.ToString("dd MMM yyyy");
								item.OrderNotificationTime = item.OrderNotificationDateTime.ToString("hh:mm tt");
							}
						}
						if (item.OrderStatus == (int)OrderHistoryEnum.Preparing)
						{
							var CustomerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == orderReviewModel.CustomerId && x.StoreId == orderReviewModel.StoreId).ToList();
							var statusResourceValue = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.Template.PreparingTxt", languageId, dbcontext);
							item.Description = statusResourceValue;
							item.Description = item.Description.Replace("%Customer.FullName%", CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).FirstOrDefault().Value + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).FirstOrDefault().Value : ""));
							item.Description = item.Description.Replace("%Customer.OrderNumber%", item.OrderNumber.ToString());
							var orderNotes = allOrderNotes.Where(x => x.Note.ToLower().Contains("prepar")).FirstOrDefault();
							if (orderNotes != null)
							{
								item.OrderNotificationDateTime = Helper.ConvertToUserTime(orderNotes.CreatedOnUtc, DateTimeKind.Utc, dbcontext);
								item.OrderNotificationDate = item.OrderNotificationDateTime.ToString("dd MMM yyyy");
								item.OrderNotificationTime = item.OrderNotificationDateTime.ToString("hh:mm tt");
							}
						}
						item.Description = Regex.Replace(item.Description, "<.*?>", String.Empty);
						List.Add(item);

					}
					if (List.Any())
					{
						customerAPIResponses.ErrorMessageTitle = "Success";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.OrderNotificationPushSuccessfully", languageId, dbcontext);
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = List;
					}
					else
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.NoNotificationYet", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = 400;
						customerAPIResponses.ResponseObj = null;
					}
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.NoNotificationYet", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 400;
					customerAPIResponses.ResponseObj = null;
				}


				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

		[Route("~/api/v2.1/GetNotifications")]
		[HttpPost]
		public CustomerAPIResponses GetNotificationsV2_1([FromBody] NotificationModel orderReviewModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(orderReviewModel.StoreId, orderReviewModel.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (orderReviewModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(orderReviewModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (orderReviewModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(orderReviewModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == orderReviewModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Invalid Authentication key";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				string symbol = Helper.GetStoreCurrency(orderReviewModel.StoreId, dbcontext);
				var noOfNotifications = Convert.ToInt32(dbcontext.Setting.Where(x => x.Name.Contains("setting.customer.noOfNotifications") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault()?.Value ?? "20");

				var orderNotesNotifiactionDetails = (from order in dbcontext.Order
														 //join note in dbcontext.OrderNotes
														 //on order.Id equals note.OrderId
													 where order.CustomerId == orderReviewModel.CustomerId
													 && order.StoreId == orderReviewModel.StoreId
													 select new OrderNotesNotifiactionModel
													 {
														 OrderId = order.Id,
														 OrderStatus = order.OrderStatusId,
														 OrderNumber = order.CustomOrderNumber,
														 OrderDate = order.CreatedOnUtc,
														 //Description= note.Note,
														 //OrderNotificationDateTime=note.CreatedOnUtc
													 }).OrderByDescending(x => x.OrderId).Take(noOfNotifications).ToList();
				if (orderNotesNotifiactionDetails.Any())
				{
					List<OrderNotesNotifiactionModel> List = new List<OrderNotesNotifiactionModel>();
					var orderResourceValue = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.Order.Txt", languageId, dbcontext);

					foreach (var item in orderNotesNotifiactionDetails)
					{
						item.OrderDate = Helper.ConvertToUserTimeV2_1(item.OrderDate, DateTimeKind.Utc, dbcontext, orderReviewModel.CustomerId);
						item.OrderNotificationDate = item.OrderNotificationDateTime.ToString("dd MMM yyyy");
						item.OrderNotificationTime = item.OrderNotificationDateTime.ToString("hh:mm tt");
						item.Title = orderResourceValue + " #" + item.OrderNumber;

						if (item.OrderStatus == (int)OrderHistoryEnum.Received)
						{
							var CustomerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == orderReviewModel.CustomerId && x.StoreId == orderReviewModel.StoreId).ToList();
							var statusResourceValue = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.Template.ReceivedTxt", languageId, dbcontext);
							item.Description = statusResourceValue;
							item.Description = item.Description.Replace("%Customer.FullName%", CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).FirstOrDefault().Value + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).FirstOrDefault().Value : ""));
							item.Description = item.Description.Replace("%Customer.OrderNumber%", item.OrderNumber.ToString());
							var orderNotes = dbcontext.OrderNote.Where(x => x.OrderId == item.OrderId && (item.Description.ToLower().ToString().Contains("placed") || item.Description.ToLower().ToString().Contains("received"))).FirstOrDefault();
							if (orderNotes != null)
							{
								orderNotes.CreatedOnUtc = Helper.ConvertToUserTimeV2_1(orderNotes.CreatedOnUtc, DateTimeKind.Utc, dbcontext, orderReviewModel.CustomerId);
								item.OrderNotificationDate = orderNotes.CreatedOnUtc.ToString("dd MMM yyyy");
								item.OrderNotificationTime = orderNotes.CreatedOnUtc.ToString("hh:mm tt");
							}

						}
						if (item.OrderStatus == (int)OrderHistoryEnum.Cancelled)
						{
							var CustomerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == orderReviewModel.CustomerId && x.StoreId == orderReviewModel.StoreId).ToList();
							var statusResourceValue = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.Template.CancelledTxt", languageId, dbcontext);
							item.Description = statusResourceValue;
							item.Description = item.Description.Replace("%Customer.FullName%", CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).FirstOrDefault().Value + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).FirstOrDefault().Value : ""));
							item.Description = item.Description.Replace("%Customer.OrderNumber%", item.OrderNumber.ToString());
							var orderNotes = dbcontext.OrderNote.Where(x => x.OrderId == item.OrderId && (item.Description.ToLower().ToString().Contains("cancelled"))).FirstOrDefault();
							if (orderNotes != null)
							{
								orderNotes.CreatedOnUtc = Helper.ConvertToUserTimeV2_1(orderNotes.CreatedOnUtc, DateTimeKind.Utc, dbcontext, orderReviewModel.CustomerId);
								item.OrderNotificationDate = orderNotes.CreatedOnUtc.ToString("dd MMM yyyy");
								item.OrderNotificationTime = orderNotes.CreatedOnUtc.ToString("hh:mm tt");
							}
						}
						if (item.OrderStatus == (int)OrderHistoryEnum.Confirmed)
						{
							var CustomerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == orderReviewModel.CustomerId && x.StoreId == orderReviewModel.StoreId).ToList();
							var statusResourceValue = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.Template.ConfirmedTxt", languageId, dbcontext);
							item.Description = statusResourceValue;
							item.Description = item.Description.Replace("%Customer.FullName%", CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).FirstOrDefault().Value + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).FirstOrDefault().Value : ""));
							item.Description = item.Description.Replace("%Customer.OrderNumber%", item.OrderNumber.ToString());
							var orderNotes = dbcontext.OrderNote.Where(x => x.OrderId == item.OrderId && (item.Description.ToLower().ToString().Contains("confirmed"))).FirstOrDefault();
							if (orderNotes != null)
							{
								orderNotes.CreatedOnUtc = Helper.ConvertToUserTimeV2_1(orderNotes.CreatedOnUtc, DateTimeKind.Utc, dbcontext, orderReviewModel.CustomerId);
								item.OrderNotificationDate = orderNotes.CreatedOnUtc.ToString("dd MMM yyyy");
								item.OrderNotificationTime = orderNotes.CreatedOnUtc.ToString("hh:mm tt");
							}

						}
						if (item.OrderStatus == (int)OrderHistoryEnum.Picked)
						{
							var CustomerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == orderReviewModel.CustomerId && x.StoreId == orderReviewModel.StoreId).ToList();
							var statusResourceValue = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.Template.PickedTxt", languageId, dbcontext);
							item.Description = statusResourceValue;
							item.Description = item.Description.Replace("%Customer.FullName%", CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).FirstOrDefault().Value + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).FirstOrDefault().Value : ""));
							item.Description = item.Description.Replace("%Customer.OrderNumber%", item.OrderNumber.ToString());
							var orderNotes = dbcontext.OrderNote.Where(x => x.OrderId == item.OrderId && (item.Description.ToLower().ToString().Contains("picked"))).FirstOrDefault();
							if (orderNotes != null)
							{
								orderNotes.CreatedOnUtc = Helper.ConvertToUserTimeV2_1(orderNotes.CreatedOnUtc, DateTimeKind.Utc, dbcontext, orderReviewModel.CustomerId);
								item.OrderNotificationDate = orderNotes.CreatedOnUtc.ToString("dd MMM yyyy");
								item.OrderNotificationTime = orderNotes.CreatedOnUtc.ToString("hh:mm tt");
							}
						}
						if (item.OrderStatus == (int)OrderHistoryEnum.OrderDelivered)
						{
							var CustomerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == orderReviewModel.CustomerId && x.StoreId == orderReviewModel.StoreId).ToList();
							var statusResourceValue = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.Template.OrderDeliveredTxt", languageId, dbcontext);
							item.Description = statusResourceValue;
							item.Description = item.Description.Replace("%Customer.FullName%", CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).FirstOrDefault().Value + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).FirstOrDefault().Value : ""));
							item.Description = item.Description.Replace("%Customer.OrderNumber%", item.OrderNumber.ToString());
							var orderNotes = dbcontext.OrderNote.Where(x => x.OrderId == item.OrderId && (item.Description.ToLower().ToString().Contains("delivered"))).FirstOrDefault();
							if (orderNotes != null)
							{
								orderNotes.CreatedOnUtc = Helper.ConvertToUserTimeV2_1(orderNotes.CreatedOnUtc, DateTimeKind.Utc, dbcontext, orderReviewModel.CustomerId);
								item.OrderNotificationDate = orderNotes.CreatedOnUtc.ToString("dd MMM yyyy");
								item.OrderNotificationTime = orderNotes.CreatedOnUtc.ToString("hh:mm tt");
							}
						}
						if (item.OrderStatus == (int)OrderHistoryEnum.Preparing)
						{
							var CustomerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == orderReviewModel.CustomerId && x.StoreId == orderReviewModel.StoreId).ToList();
							var statusResourceValue = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.Template.PreparingTxt", languageId, dbcontext);
							item.Description = statusResourceValue;
							item.Description = item.Description.Replace("%Customer.FullName%", CustomerAddressDetails.Where(x => x.Key.Contains("FirstName")).FirstOrDefault().Value + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).FirstOrDefault().Value : ""));
							item.Description = item.Description.Replace("%Customer.OrderNumber%", item.OrderNumber.ToString());
							var orderNotes = dbcontext.OrderNote.Where(x => x.OrderId == item.OrderId && (item.Description.ToLower().ToString().Contains("prepar"))).FirstOrDefault();
							if (orderNotes != null)
							{
								orderNotes.CreatedOnUtc = Helper.ConvertToUserTimeV2_1(orderNotes.CreatedOnUtc, DateTimeKind.Utc, dbcontext, orderReviewModel.CustomerId);
								item.OrderNotificationDate = orderNotes.CreatedOnUtc.ToString("dd MMM yyyy");
								item.OrderNotificationTime = orderNotes.CreatedOnUtc.ToString("hh:mm tt");
							}
						}
						item.Description = Regex.Replace(item.Description, "<.*?>", String.Empty);
						List.Add(item);

					}
					if (List.Any())
					{
						customerAPIResponses.ErrorMessageTitle = "Success";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.OrderNotificationPushSuccessfully", languageId, dbcontext);
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = List;
					}
					else
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.NoNotificationYet", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = 400;
						customerAPIResponses.ResponseObj = null;
					}
					//foreach (var item in orderDetails)
					//{
					//    foreach (var item_ in item.OrderedStatusArray)
					//    {
					//        item_.StatusTitle = Enum.GetValues(typeof(OrderHistoryEnum)).Cast<OrderHistoryEnum>().Where(x => (int)x == item_.StatusId).Select(x => x.ToString()).ToList()[0];
					//        item_.StatusTime = item_.StatusDate.ToString("hh:mm tt");
					//    }
					//}


				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "API.ErrorMesaage.NoNotificationYet", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 400;
					customerAPIResponses.ResponseObj = null;
				}


				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

		[Route("~/api/v1/GetFCSNotifications")]
		[HttpPost]
		public CustomerAPIResponses GetFCSNotification(FCSNotificationModelGetV2 fCSNotificationModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(fCSNotificationModel.StoreId, fCSNotificationModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (fCSNotificationModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(fCSNotificationModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(fCSNotificationModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(fCSNotificationModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (fCSNotificationModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(fCSNotificationModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (fCSNotificationModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(fCSNotificationModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				FcsNotificationRestTypeModel fcsNotificationRestTypeModel = new FcsNotificationRestTypeModel();
				List<FCSNotificationV2Model> fCSNotificationModelObj = new List<FCSNotificationV2Model>();
				var orders = dbcontext.Order.Where(x => x.CustomerId == fCSNotificationModel.CustomerId).OrderByDescending(x => x.Id).Take(1).ToList();

				foreach (var item in orders)
				{
					if (item.OrderStatusId != (int)OrderHistoryEnum.OrderDelivered || item.OrderStatusId != (int)OrderHistoryEnum.Cancelled)
					{
						FCSNotificationV2Model fCSNotificationV2 = new FCSNotificationV2Model();
						fCSNotificationV2.OrderId = item.Id.ToString();
						var ProductsIds = dbcontext.OrderItem.Where(x => x.OrderId == item.Id).ToList().Select(x => x.ProductId).ToList();
						int PrepareTime = 0;
						int ProductPrepareTime = 0;
						string VendorLatLong = string.Empty;

						foreach (var productId in ProductsIds)
						{
							var productPrepareTime = (from p in dbcontext.Product
													  where p.Id == productId
													  select new
													  {
														  p.PrepareTime
													  }) != null ? (from p in dbcontext.Product
																	where p.Id == productId
																	select new
																	{
																		p.PrepareTime
																	}).FirstOrDefault().PrepareTime : 0;
							if (productPrepareTime == null)
							{
								productPrepareTime = 0;
							}
							ProductPrepareTime = productPrepareTime.Value + ProductPrepareTime;
							var vendorId = dbcontext.Product.Where(x => x.Id == productId).Select(x => x.VendorId).FirstOrDefault();
							VendorLatLong = dbcontext.Vendor.Where(x => x.Id == vendorId).FirstOrDefault().Geolocation;
						}
						if (ProductPrepareTime != 0)
							PrepareTime = ProductPrepareTime / ProductsIds.Count();
						//int PId = ProductsIds.FirstOrDefault();
						//var VendorId = dbcontext.Products.Where(x => x.Id == PId).FirstOrDefault().VendorId;
						//var vendors = dbcontext.Vendors.Where(x => x.Id == VendorId).FirstOrDefault();

						if (!string.IsNullOrEmpty(VendorLatLong) && VendorLatLong.Contains("MARKER"))
						{
							string[] venLtLngStrArr = VendorLatLong.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
							if (venLtLngStrArr.Length > 0)
							{
								var AddressId = item.PickupInStore ? item.BillingAddressId : item.ShippingAddressId;
								if (AddressId == null)
								{
									AddressId = item.BillingAddressId;
								}
								var customerAddress = dbcontext.Address.Where(x => x.Id == AddressId).FirstOrDefault();
								double latRest = Convert.ToDouble(venLtLngStrArr[0]);
								double longRest = Convert.ToDouble(venLtLngStrArr[1]);
								double latCust = Convert.ToDouble(customerAddress.Latitude);
								double longCust = Convert.ToDouble(customerAddress.Longitude);
								string URl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + latRest + "," + longRest + "&destination=" + latCust + "," + longCust + "&mode=transit&" + googleMapKey;
								var request = (HttpWebRequest)WebRequest.Create(URl);
								WebResponse response = request.GetResponse();
								using (Stream dataStream = response.GetResponseStream())
								{
									// Open the stream using a StreamReader for easy access.  
									StreamReader reader = new StreamReader(dataStream);
									// Read the content.  
									string responseFromServer = reader.ReadToEnd();
									// Display the content.
									var result = JsonConvert.DeserializeObject<GoogleTimeAPIModel>(responseFromServer);
									//new JavaScriptSerializer().Deserialize<Friends>(result);
									if (result.routes.Any())
									{
										var TravelTime = (result.routes.FirstOrDefault().legs.FirstOrDefault().duration.value) / 60;
										PrepareTime = PrepareTime + TravelTime;
									}

									Console.WriteLine(responseFromServer);
								}

								//if (ret <= DistanceCovered)
								//{
								//    CooknRestlist.Add(item.Id);
								//}
							}
						}
						var ProductId = ProductsIds.FirstOrDefault();
						var VendorName = (from a in dbcontext.Product.Where(x => x.Id == ProductId)
										  join b in dbcontext.Vendor
										  on a.VendorId equals b.Id
										  select new
										  {
											  b
										  }).FirstOrDefault()?.b.Name;
						fCSNotificationV2.Status = (string)Enum.GetValues(typeof(OrderHistoryEnum)).Cast<OrderHistoryEnum>().Where(x => (int)x == item.OrderStatusId).Select(x => x.ToString()).ToList()[0];
						fCSNotificationV2.Status = LanguageHelper.GetResourseValueByName(fCSNotificationModel.StoreId, "App.OrderStatus." + fCSNotificationV2.Status + "Txt", languageId, dbcontext);
						//if (VendorName.ToLower().Contains("live"))
						//{
						item.CreatedOnUtc = Helper.ConvertToUserTime(item.CreatedOnUtc, DateTimeKind.Utc, dbcontext);
						var OrderFromTime = item.CreatedOnUtc.AddMinutes(PrepareTime).ToString("hh:mm tt");
						var arrivaltime = Convert.ToInt32(dbcontext.Setting.Where(x => x.Name.Contains("setting.customer.arrivaltime") && x.StoreId == fCSNotificationModel.StoreId).FirstOrDefault()?.Value ?? "10");
						var OrderToTime = item.CreatedOnUtc.AddMinutes(PrepareTime + arrivaltime).ToString("hh:mm tt");

						var deliverySlotBooking = dbcontext.DeliverySlotBooking.Where(x => x.OrderId == item.Id && x.Deleted == false).FirstOrDefault();
						if (deliverySlotBooking != null)
						{
							var deliverydate = Helper.ConvertToUserTime(deliverySlotBooking.DeliveryDateUtc, DateTimeKind.Utc, dbcontext);
							fCSNotificationV2.DeliveredTime = string.Format("{0:ddd, dd MMM} {1} - {2}", deliverydate.ToString("MM/dd/yyyy"), deliverySlotBooking.DeliveryStartTime.ToString("hh:mm tt"), deliverySlotBooking.DeliveryEndTime.ToString("hh:mm tt"));
						}
						else
						{
							fCSNotificationV2.DeliveredTime = OrderFromTime + "-" + OrderToTime;
						}

						//}
						//else if (VendorName.ToLower().Contains("pre"))
						//{
						//    var orderDetails = dbcontext.OrderDetails.Where(x => x.Id == item.OrderDetailId).FirstOrDefault();
						//    if(orderDetails!=null)
						//    {
						//        DateTime time = DateTime.Today.Add(orderDetails.ScheduleTime.Value);
						//        string displayTime = time.ToString("hh:mm tt"); // It will give "03:00 AM"
						//        if (orderDetails.ScheduleToTime == null)
						//        {
						//            orderDetails.ScheduleToTime = orderDetails.ScheduleTime;
						//        }
						//        DateTime time2 = DateTime.Today.Add(orderDetails.ScheduleToTime.Value);
						//        string displayTime2 = time2.ToString("hh:mm tt");
						//        var OrderFromTime = displayTime;
						//        var OrderToTime = displayTime2;
						//        fCSNotificationV2.DeliveredTime = OrderFromTime + "-" + OrderToTime;
						//    }

						//}
						//else
						//{
						//    fCSNotificationV2.DeliveredTime = null;
						//}

						fCSNotificationV2.OrderNumber = item.CustomOrderNumber;
						if (dbcontext.OrderDetails.Where(x => x.Id == (item.OrderDetailId ?? 0)).Any())
						{
							fCSNotificationV2.RestType = dbcontext.OrderDetails.Where(x => x.Id == item.OrderDetailId).FirstOrDefault().OrderType.Value;
						}
						fCSNotificationModelObj.Add(fCSNotificationV2);
					}
				}
				if (fCSNotificationModelObj.Any())
				{
					fcsNotificationRestTypeModel.delivery = fCSNotificationModelObj.Where(x => x.RestType == (int)RestType.Delivery).OrderByDescending(x => x.OrderId).Take(1).ToList();
					fcsNotificationRestTypeModel.takeaway = fCSNotificationModelObj.Where(x => x.RestType == (int)RestType.TakeAway).OrderByDescending(x => x.OrderId).Take(1).ToList();
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(fCSNotificationModel.StoreId, "API.ErrorMesaage.NotificationPushSuccessfully", languageId, dbcontext);
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = fcsNotificationRestTypeModel;
					return customerAPIResponses;
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(fCSNotificationModel.StoreId, "API.ErrorMesaage.NoNotificationYet", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = fCSNotificationModelObj;
					return customerAPIResponses;
				}

			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

		[Route("~/api/v2.1/GetFCSNotifications")]
		[HttpPost]
		public CustomerAPIResponses GetFCSNotificationV2_1(FCSNotificationModelGetV2 fCSNotificationModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(fCSNotificationModel.StoreId, fCSNotificationModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (fCSNotificationModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(fCSNotificationModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(fCSNotificationModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(fCSNotificationModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (fCSNotificationModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(fCSNotificationModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (fCSNotificationModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(fCSNotificationModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				FcsNotificationRestTypeModel fcsNotificationRestTypeModel = new FcsNotificationRestTypeModel();
				List<FCSNotificationV2Model> fCSNotificationModelObj = new List<FCSNotificationV2Model>();
				var orders = dbcontext.Order.Where(x => x.CustomerId == fCSNotificationModel.CustomerId).OrderByDescending(x => x.Id).Take(1).ToList();

				foreach (var item in orders)
				{
					if (item.OrderStatusId != (int)OrderHistoryEnum.OrderDelivered || item.OrderStatusId != (int)OrderHistoryEnum.Cancelled)
					{
						FCSNotificationV2Model fCSNotificationV2 = new FCSNotificationV2Model();
						fCSNotificationV2.OrderId = item.Id.ToString();
						var ProductsIds = dbcontext.OrderItem.Where(x => x.OrderId == item.Id).ToList().Select(x => x.ProductId).ToList();
						int PrepareTime = 0;
						int ProductPrepareTime = 0;
						string VendorLatLong = string.Empty;

						foreach (var productId in ProductsIds)
						{
							var productPrepareTime = (from p in dbcontext.Product
													  where p.Id == productId
													  select new
													  {
														  p.PrepareTime
													  }) != null ? (from p in dbcontext.Product
																	where p.Id == productId
																	select new
																	{
																		p.PrepareTime
																	}).FirstOrDefault().PrepareTime : 0;
							if (productPrepareTime == null)
							{
								productPrepareTime = 0;
							}
							ProductPrepareTime = productPrepareTime.Value + ProductPrepareTime;
							var vendorId = dbcontext.Product.Where(x => x.Id == productId).Select(x => x.VendorId).FirstOrDefault();
							VendorLatLong = dbcontext.Vendor.Where(x => x.Id == vendorId).FirstOrDefault().Geolocation;
						}
						if (ProductPrepareTime != 0)
							PrepareTime = ProductPrepareTime / ProductsIds.Count();

						if (!string.IsNullOrEmpty(VendorLatLong) && VendorLatLong.Contains("MARKER"))
						{
							string[] venLtLngStrArr = VendorLatLong.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
							if (venLtLngStrArr.Length > 0)
							{
								var AddressId = item.PickupInStore ? item.BillingAddressId : item.ShippingAddressId;
								if (AddressId == null)
								{
									AddressId = item.BillingAddressId;
								}
								var customerAddress = dbcontext.Address.Where(x => x.Id == AddressId).FirstOrDefault();
								double latRest = Convert.ToDouble(venLtLngStrArr[0]);
								double longRest = Convert.ToDouble(venLtLngStrArr[1]);
								double latCust = Convert.ToDouble(customerAddress.Latitude);
								double longCust = Convert.ToDouble(customerAddress.Longitude);
								string URl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + latRest + "," + longRest + "&destination=" + latCust + "," + longCust + "&mode=transit&" + googleMapKey;
								var request = (HttpWebRequest)WebRequest.Create(URl);
								WebResponse response = request.GetResponse();
								using (Stream dataStream = response.GetResponseStream())
								{
									// Open the stream using a StreamReader for easy access.  
									StreamReader reader = new StreamReader(dataStream);
									// Read the content.  
									string responseFromServer = reader.ReadToEnd();
									// Display the content.
									var result = JsonConvert.DeserializeObject<GoogleTimeAPIModel>(responseFromServer);
									//new JavaScriptSerializer().Deserialize<Friends>(result);
									if (result.routes.Any())
									{
										var TravelTime = (result.routes.FirstOrDefault().legs.FirstOrDefault().duration.value) / 60;
										PrepareTime = PrepareTime + TravelTime;
									}

									Console.WriteLine(responseFromServer);
								}
							}
						}
						var ProductId = ProductsIds.FirstOrDefault();
						var VendorName = (from a in dbcontext.Product.Where(x => x.Id == ProductId)
										  join b in dbcontext.Vendor
										  on a.VendorId equals b.Id
										  select new
										  {
											  b
										  }).FirstOrDefault()?.b.Name;
						fCSNotificationV2.Status = (string)Enum.GetValues(typeof(OrderHistoryEnum)).Cast<OrderHistoryEnum>().Where(x => (int)x == item.OrderStatusId).Select(x => x.ToString()).ToList()[0];
						fCSNotificationV2.Status = LanguageHelper.GetResourseValueByName(fCSNotificationModel.StoreId, "App.OrderStatus." + fCSNotificationV2.Status + "Txt", languageId, dbcontext);
						item.CreatedOnUtc = Helper.ConvertToUserTimeV2_1(item.CreatedOnUtc, DateTimeKind.Utc, dbcontext, fCSNotificationModel.CustomerId);
						var OrderFromTime = item.CreatedOnUtc.AddMinutes(PrepareTime).ToString("hh:mm tt");
						var OrderToTime = item.CreatedOnUtc.AddMinutes(PrepareTime + 10).ToString("hh:mm tt");
						fCSNotificationV2.DeliveredTime = OrderFromTime + "-" + OrderToTime;

						fCSNotificationV2.OrderNumber = item.CustomOrderNumber;
						if (dbcontext.OrderDetails.Where(x => x.Id == (item.OrderDetailId ?? 0)).Any())
						{
							fCSNotificationV2.RestType = dbcontext.OrderDetails.Where(x => x.Id == item.OrderDetailId).FirstOrDefault().OrderType.Value;
						}
						fCSNotificationModelObj.Add(fCSNotificationV2);
					}
				}
				if (fCSNotificationModelObj.Any())
				{
					fcsNotificationRestTypeModel.delivery = fCSNotificationModelObj.Where(x => x.RestType == (int)RestType.Delivery).OrderByDescending(x => x.OrderId).Take(1).ToList();
					fcsNotificationRestTypeModel.takeaway = fCSNotificationModelObj.Where(x => x.RestType == (int)RestType.TakeAway).OrderByDescending(x => x.OrderId).Take(1).ToList();
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(fCSNotificationModel.StoreId, "API.ErrorMesaage.NotificationPushSuccessfully", languageId, dbcontext);
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = fcsNotificationRestTypeModel;
					return customerAPIResponses;
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(fCSNotificationModel.StoreId, "API.ErrorMesaage.NoNotificationYet", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = fCSNotificationModelObj;
					return customerAPIResponses;
				}

			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

		[Route("~/api/v1/SendFCSNotifications")]
		[HttpPost]
		public CustomerAPIResponses SendFCSNotifications(FCSNotificationModel fCSNotificationModel)
		{

			var storeId = Helper.IsNumeric(fCSNotificationModel.StoreId) ? Convert.ToInt32(fCSNotificationModel.StoreId) : 0;
			int languageId = LanguageHelper.GetIdByLangCode(storeId, fCSNotificationModel.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			try
			{
				int OrderId = Convert.ToInt32(fCSNotificationModel.OrderId);

				var order = dbcontext.Order.Where(x => x.Id == OrderId).FirstOrDefault();

				string OrderNumber = string.Empty;
				var WebKey = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.customerapinotificationkey") && x.StoreId == order.StoreId).Any() ?
					dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.customerapinotificationkey") && x.StoreId == order.StoreId).FirstOrDefault().Value : "";
				var WebAddr = "https://fcm.googleapis.com/fcm/send";
				var httpWebRequest = (HttpWebRequest)WebRequest.Create(WebAddr);
				httpWebRequest.ContentType = "application/json";
				httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, WebKey);
				httpWebRequest.Method = "POST";



				var customer = dbcontext.CustomerDetails.Where(x => x.CustomerId == order.CustomerId && !string.IsNullOrEmpty(x.DeviceToken)).OrderByDescending(x => x.Id).FirstOrDefault();
				if (customer == null)
				{
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.SessionExpired", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 400;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
				fCSNotificationModel.To = customer.DeviceToken;
				if (string.IsNullOrEmpty(fCSNotificationModel.ApiKey))
				{
					var version = dbcontext.VersionInfo.Where(x => x.StoreId == order.StoreId).OrderByDescending(x => x.Id).FirstOrDefault();
					fCSNotificationModel.ApiKey = version.Apikey;
				}
				int orderId = Convert.ToInt32(fCSNotificationModel.OrderId);
				var OrderNumberExist = dbcontext.Order.Where(x => x.Id == orderId).FirstOrDefault();
				if (OrderNumberExist != null)
				{
					OrderNumber = OrderNumberExist.CustomOrderNumber;
				}
				if (OrderNumberExist.OrderStatusId != (int)OrderHistoryEnum.Received)
				{
					OrderNumberExist.CreatedOnUtc = Helper.ConvertToUserTime(OrderNumberExist.CreatedOnUtc, DateTimeKind.Utc, dbcontext);
					DateTime iKnowThisIsUtc = OrderNumberExist.CreatedOnUtc;
					DateTime runtimeKnowsThisIsUtc = DateTime.SpecifyKind(
						iKnowThisIsUtc,
						DateTimeKind.Utc);
					DateTime localVersion = runtimeKnowsThisIsUtc.ToLocalTime();
					var ProductsIds = dbcontext.OrderItem.Where(x => x.OrderId == OrderNumberExist.Id).ToList().Select(x => x.ProductId).ToList();
					int PrepareTime = 0;
					int ProductPrepareTime = 0;
					string VendorLatLong = string.Empty;

					foreach (var productId in ProductsIds)
					{
						var productPrepareTime = (from p in dbcontext.Product
												  where p.Id == productId
												  select new
												  {
													  p.PrepareTime
												  }) != null ? (from p in dbcontext.Product
																where p.Id == productId
																select new
																{
																	p.PrepareTime
																}).FirstOrDefault().PrepareTime : 0;
						if (productPrepareTime == null)
						{
							productPrepareTime = 0;
						}
						ProductPrepareTime = productPrepareTime.Value + ProductPrepareTime;
						var vendorId = dbcontext.Product.Where(x => x.Id == productId).Select(x => x.VendorId).FirstOrDefault();
						VendorLatLong = dbcontext.Vendor.Where(x => x.Id == vendorId).FirstOrDefault().Geolocation;
					}
					PrepareTime = ProductPrepareTime / ProductsIds.Count();
					//int PId = ProductsIds.FirstOrDefault();
					//var VendorId = dbcontext.Products.Where(x => x.Id == PId).FirstOrDefault().VendorId;
					//var vendors = dbcontext.Vendors.Where(x => x.Id == VendorId).FirstOrDefault();

					if (!string.IsNullOrEmpty(VendorLatLong) && VendorLatLong.Contains("MARKER"))
					{
						string[] venLtLngStrArr = VendorLatLong.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
						if (venLtLngStrArr.Length > 0)
						{
							var AddressId = order.PickupInStore ? order.BillingAddressId : order.ShippingAddressId;
							var customerAddress = dbcontext.Address.Where(x => x.Id == AddressId).FirstOrDefault();
							double latRest = Convert.ToDouble(venLtLngStrArr[0]);
							double longRest = Convert.ToDouble(venLtLngStrArr[1]);
							double latCust = Convert.ToDouble(customerAddress.Latitude);
							double longCust = Convert.ToDouble(customerAddress.Longitude);
							string URl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + latRest + "," + longRest + "&destination=" + latCust + "," + longCust + "&mode=transit&" + googleMapKey;
							var request = (HttpWebRequest)WebRequest.Create(URl);
							WebResponse response = request.GetResponse();
							using (Stream dataStream = response.GetResponseStream())
							{
								// Open the stream using a StreamReader for easy access.  
								StreamReader reader = new StreamReader(dataStream);
								// Read the content.  
								string responseFromServer = reader.ReadToEnd();
								// Display the content.
								var result = JsonConvert.DeserializeObject<GoogleTimeAPIModel>(responseFromServer);
								//new JavaScriptSerializer().Deserialize<Friends>(result);
								if (result.routes.Any())
								{
									var TravelTime = (result.routes.FirstOrDefault().legs.FirstOrDefault().duration.value) / 60;
									PrepareTime = PrepareTime + TravelTime;
								}

								Console.WriteLine(responseFromServer);
							}

							//if (ret <= DistanceCovered)
							//{
							//    CooknRestlist.Add(item.Id);
							//}
						}
					}

					var ProductId = ProductsIds.FirstOrDefault();
					var VendorName = (from a in dbcontext.Product.Where(x => x.Id == ProductId)
									  join b in dbcontext.Vendor
									  on a.VendorId equals b.Id
									  select new
									  {
										  b
									  }).FirstOrDefault().b.Name;


					//if (VendorName.ToLower().Contains("live"))
					//{
					order.CreatedOnUtc = Helper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc, dbcontext);
					var OrderFromTime = order.CreatedOnUtc.AddMinutes(PrepareTime).ToString("hh:mm tt");
					var OrderToTime = order.CreatedOnUtc.AddMinutes(PrepareTime + 10).ToString("hh:mm tt");
					fCSNotificationModel.DeliveredTime = OrderFromTime + "-" + OrderToTime;
					//}
					//else if (VendorName.ToLower().Contains("pre"))
					//{
					//    var orderDetails = db.OrderDetails.Where(x => x.Id == order.OrderDetailId).FirstOrDefault();
					//    DateTime time = DateTime.Today.Add(orderDetails.ScheduleTime.Value);
					//    string displayTime = time.ToString("hh:mm tt"); // It will give "03:00 AM"
					//    if (orderDetails.ScheduleToTime == null)
					//    {
					//        orderDetails.ScheduleToTime = orderDetails.ScheduleTime;
					//    }
					//    DateTime time2 = DateTime.Today.Add(orderDetails.ScheduleToTime.Value);
					//    string displayTime2 = time2.ToString("hh:mm tt");
					//    var OrderFromTime = displayTime;
					//    var OrderToTime = displayTime2;
					//    fCSNotificationModel.DeliveredTime = OrderFromTime + "-" + OrderToTime;
					//}
					//else
					//{
					//    fCSNotificationModel.DeliveredTime = null;
					//}
				}

				fCSNotificationModel.Status = (string)Enum.GetValues(typeof(OrderHistoryEnum)).Cast<OrderHistoryEnum>().Where(x => (int)x == order.OrderStatusId).Select(x => x.ToString()).ToList()[0];
				var statusResourceValue = LanguageHelper.GetResourseValueByName(storeId, "App.OrderStatus." + fCSNotificationModel.Status.Replace(" ", "") + "Txt", languageId, dbcontext);
				var orderResourceValue = LanguageHelper.GetResourseValueByName(storeId, "App.Order.Txt", languageId, dbcontext);
				fCSNotificationModel.Heading = fCSNotificationModel.Status.ToLower().ToString().Contains("order") ? statusResourceValue : orderResourceValue + " " + statusResourceValue;
				fCSNotificationModel.Status = statusResourceValue;
				if (dbcontext.OrderDetails.Where(x => x.Id == order.OrderDetailId).Any())
				{
					fCSNotificationModel.RestType = dbcontext.OrderDetails.Where(x => x.Id == order.OrderDetailId).FirstOrDefault().OrderType.Value;
				}


				using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
				{
					string strNJson = @"{
                    ""to"": """ + fCSNotificationModel.To + @""",
                    ""data"": {
                        ""Status"": """ + fCSNotificationModel.Status + @""",
                        ""DeliveryTime"": """ + fCSNotificationModel.DeliveredTime + @""",
                        ""OrderId"": """ + fCSNotificationModel.OrderId + @""",
                            ""OrderNumber"": """ + OrderNumber + @""",
                            ""RestType"": """ + fCSNotificationModel.RestType + @""",
                            },
                        ""notification"": {
                        ""title"": """ + fCSNotificationModel.Heading + @""",
                        ""text"": """ + fCSNotificationModel.Status + @""",
                            
                            }
                        }";
					streamWriter.Write(strNJson);
					streamWriter.Flush();
				}
				var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
				using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
				{

					string result = streamReader.ReadToEnd();
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.SuccessMessage", languageId, dbcontext);
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = 200;
					customerAPIResponses.ResponseObj = result;
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

		[Route("~/api/v2.1/SendFCSNotifications")]
		[HttpPost]
		public CustomerAPIResponses SendFCSNotificationsV2_1(FCSNotificationModel fCSNotificationModel)
		{

			var storeId = Helper.IsNumeric(fCSNotificationModel.StoreId) ? Convert.ToInt32(fCSNotificationModel.StoreId) : 0;
			int languageId = LanguageHelper.GetIdByLangCode(storeId, fCSNotificationModel.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			try
			{
				int OrderId = Convert.ToInt32(fCSNotificationModel.OrderId);

				var order = dbcontext.Order.Where(x => x.Id == OrderId).FirstOrDefault();

				string OrderNumber = string.Empty;
				var WebKey = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.customerapinotificationkey") && x.StoreId == order.StoreId).Any() ?
					dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.customerapinotificationkey") && x.StoreId == order.StoreId).FirstOrDefault().Value : "";
				var WebAddr = "https://fcm.googleapis.com/fcm/send";
				var httpWebRequest = (HttpWebRequest)WebRequest.Create(WebAddr);
				httpWebRequest.ContentType = "application/json";
				httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, WebKey);
				httpWebRequest.Method = "POST";



				var customer = dbcontext.CustomerDetails.Where(x => x.CustomerId == order.CustomerId && !string.IsNullOrEmpty(x.DeviceToken)).OrderByDescending(x => x.Id).FirstOrDefault();
				if (customer == null)
				{
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.SessionExpired", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 400;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
				fCSNotificationModel.To = customer.DeviceToken;
				if (string.IsNullOrEmpty(fCSNotificationModel.ApiKey))
				{
					var version = dbcontext.VersionInfo.Where(x => x.StoreId == order.StoreId).OrderByDescending(x => x.Id).FirstOrDefault();
					fCSNotificationModel.ApiKey = version.Apikey;
				}
				int orderId = Convert.ToInt32(fCSNotificationModel.OrderId);
				var OrderNumberExist = dbcontext.Order.Where(x => x.Id == orderId).FirstOrDefault();
				if (OrderNumberExist != null)
				{
					OrderNumber = OrderNumberExist.CustomOrderNumber;
				}
				if (OrderNumberExist.OrderStatusId != (int)OrderHistoryEnum.Received)
				{
					OrderNumberExist.CreatedOnUtc = Helper.ConvertToUserTimeV2_1(OrderNumberExist.CreatedOnUtc, DateTimeKind.Utc, dbcontext, Convert.ToInt32(fCSNotificationModel.CustomerId));
					DateTime iKnowThisIsUtc = OrderNumberExist.CreatedOnUtc;
					DateTime runtimeKnowsThisIsUtc = DateTime.SpecifyKind(
						iKnowThisIsUtc,
						DateTimeKind.Utc);
					DateTime localVersion = runtimeKnowsThisIsUtc.ToLocalTime();
					var ProductsIds = dbcontext.OrderItem.Where(x => x.OrderId == OrderNumberExist.Id).ToList().Select(x => x.ProductId).ToList();
					int PrepareTime = 0;
					int ProductPrepareTime = 0;
					string VendorLatLong = string.Empty;

					foreach (var productId in ProductsIds)
					{
						var productPrepareTime = (from p in dbcontext.Product
												  where p.Id == productId
												  select new
												  {
													  p.PrepareTime
												  }) != null ? (from p in dbcontext.Product
																where p.Id == productId
																select new
																{
																	p.PrepareTime
																}).FirstOrDefault().PrepareTime : 0;
						if (productPrepareTime == null)
						{
							productPrepareTime = 0;
						}
						ProductPrepareTime = productPrepareTime.Value + ProductPrepareTime;
						var vendorId = dbcontext.Product.Where(x => x.Id == productId).Select(x => x.VendorId).FirstOrDefault();
						VendorLatLong = dbcontext.Vendor.Where(x => x.Id == vendorId).FirstOrDefault().Geolocation;
					}
					PrepareTime = ProductPrepareTime / ProductsIds.Count();
					//int PId = ProductsIds.FirstOrDefault();
					//var VendorId = dbcontext.Products.Where(x => x.Id == PId).FirstOrDefault().VendorId;
					//var vendors = dbcontext.Vendors.Where(x => x.Id == VendorId).FirstOrDefault();

					if (!string.IsNullOrEmpty(VendorLatLong) && VendorLatLong.Contains("MARKER"))
					{
						string[] venLtLngStrArr = VendorLatLong.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
						if (venLtLngStrArr.Length > 0)
						{
							var AddressId = order.PickupInStore ? order.BillingAddressId : order.ShippingAddressId;
							var customerAddress = dbcontext.Address.Where(x => x.Id == AddressId).FirstOrDefault();
							double latRest = Convert.ToDouble(venLtLngStrArr[0]);
							double longRest = Convert.ToDouble(venLtLngStrArr[1]);
							double latCust = Convert.ToDouble(customerAddress.Latitude);
							double longCust = Convert.ToDouble(customerAddress.Longitude);
							string URl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + latRest + "," + longRest + "&destination=" + latCust + "," + longCust + "&mode=transit&" + googleMapKey;
							var request = (HttpWebRequest)WebRequest.Create(URl);
							WebResponse response = request.GetResponse();
							using (Stream dataStream = response.GetResponseStream())
							{
								// Open the stream using a StreamReader for easy access.  
								StreamReader reader = new StreamReader(dataStream);
								// Read the content.  
								string responseFromServer = reader.ReadToEnd();
								// Display the content.
								var result = JsonConvert.DeserializeObject<GoogleTimeAPIModel>(responseFromServer);
								//new JavaScriptSerializer().Deserialize<Friends>(result);
								if (result.routes.Any())
								{
									var TravelTime = (result.routes.FirstOrDefault().legs.FirstOrDefault().duration.value) / 60;
									PrepareTime = PrepareTime + TravelTime;
								}

								Console.WriteLine(responseFromServer);
							}

							//if (ret <= DistanceCovered)
							//{
							//    CooknRestlist.Add(item.Id);
							//}
						}
					}

					var ProductId = ProductsIds.FirstOrDefault();
					var VendorName = (from a in dbcontext.Product.Where(x => x.Id == ProductId)
									  join b in dbcontext.Vendor
									  on a.VendorId equals b.Id
									  select new
									  {
										  b
									  }).FirstOrDefault().b.Name;


					//if (VendorName.ToLower().Contains("live"))
					//{
					order.CreatedOnUtc = Helper.ConvertToUserTimeV2_1(order.CreatedOnUtc, DateTimeKind.Utc, dbcontext, Convert.ToInt32(fCSNotificationModel.CustomerId));
					var OrderFromTime = order.CreatedOnUtc.AddMinutes(PrepareTime).ToString("hh:mm tt");
					var OrderToTime = order.CreatedOnUtc.AddMinutes(PrepareTime + 10).ToString("hh:mm tt");
					fCSNotificationModel.DeliveredTime = OrderFromTime + "-" + OrderToTime;

				}

				fCSNotificationModel.Status = (string)Enum.GetValues(typeof(OrderHistoryEnum)).Cast<OrderHistoryEnum>().Where(x => (int)x == order.OrderStatusId).Select(x => x.ToString()).ToList()[0];
				var statusResourceValue = LanguageHelper.GetResourseValueByName(storeId, "App.OrderStatus." + fCSNotificationModel.Status.Replace(" ", "") + "Txt", languageId, dbcontext);
				var orderResourceValue = LanguageHelper.GetResourseValueByName(storeId, "App.Order.Txt", languageId, dbcontext);
				fCSNotificationModel.Heading = fCSNotificationModel.Status.ToLower().ToString().Contains("order") ? statusResourceValue : orderResourceValue + " " + statusResourceValue;
				fCSNotificationModel.Status = statusResourceValue;
				if (dbcontext.OrderDetails.Where(x => x.Id == order.OrderDetailId).Any())
				{
					fCSNotificationModel.RestType = dbcontext.OrderDetails.Where(x => x.Id == order.OrderDetailId).FirstOrDefault().OrderType.Value;
				}


				using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
				{
					string strNJson = @"{
                    ""to"": """ + fCSNotificationModel.To + @""",
                    ""data"": {
                        ""Status"": """ + fCSNotificationModel.Status + @""",
                        ""DeliveryTime"": """ + fCSNotificationModel.DeliveredTime + @""",
                        ""OrderId"": """ + fCSNotificationModel.OrderId + @""",
                            ""OrderNumber"": """ + OrderNumber + @""",
                            ""RestType"": """ + fCSNotificationModel.RestType + @""",
                            },
                        ""notification"": {
                        ""title"": """ + fCSNotificationModel.Heading + @""",
                        ""text"": """ + fCSNotificationModel.Status + @""",
                            
                            }
                        }";
					streamWriter.Write(strNJson);
					streamWriter.Flush();
				}
				var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
				using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
				{

					string result = streamReader.ReadToEnd();
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.SuccessMessage", languageId, dbcontext);
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = 200;
					customerAPIResponses.ResponseObj = result;
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

		[Route("~/api/v1/GetRiderCurrentLocation")]
		[HttpPost]
		public CustomerAPIResponses GetRiderCurrentLocation([FromBody] AgentLocationRequestModel agentLocationModel)
		{
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (agentLocationModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Model null";
				customerAPIResponses.ErrorMessage = "Model null";
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(agentLocationModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = "Api key Not Defined";
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (agentLocationModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Store Id Not Defined";
				customerAPIResponses.ErrorMessage = "Store Id Not Defined";
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (agentLocationModel.AgentId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Agent Id Not Defined";
				customerAPIResponses.ErrorMessage = "Agent Id Not Defined";
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (agentLocationModel.OrderId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Order Id Not Defined";
				customerAPIResponses.ErrorMessage = "Order Id Not Defined";
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (!string.IsNullOrEmpty(agentLocationModel.ApiKey))
			{
				var keyExist = dbcontext.VersionInfo.Any(x => x.Apikey == agentLocationModel.ApiKey);
				if (!keyExist)
				{
					customerAPIResponses.ErrorMessageTitle = "API key Error!!";
					customerAPIResponses.ErrorMessage = "Api key not authenticate";
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}
			try
			{
				//get updated lat long detail
				var record = _orderServices.GetAgentOrderGeoLocation(agentLocationModel.AgentId, agentLocationModel.OrderId);
				if (record == null)
				{
					customerAPIResponses.ErrorMessage = "Agent Current location not found";
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 400;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}

				//check if rider is reached to destination and he has delivered the order.
				var order = dbcontext.Order.FirstOrDefault(o => o.Id == agentLocationModel.OrderId);
				bool isOrderDelivered = order?.OrderStatusId == (int)OrderStatus.OrderDelivered;

				//prepare model
				var model = new AgentLocationResponceModel()
				{
					Latitude = record.AgentLat,
					Longitude = record.AgentLong,
					IsOrderDelivered = isOrderDelivered
				};

				customerAPIResponses.Status = true;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
				customerAPIResponses.ResponseObj = model;
				customerAPIResponses.ErrorMessage = "LatLng recived";
				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

	}

}
