﻿using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Models.RatingReview
{
    public partial class RatingReviewModel : BaseNopEntityModel
    {
        public RatingReviewModel()
        {
            Items = new List<ItemsModel>();
            NBReviewOptions = new List<NBReviewOptionModel>();
        }
        public int CustomerId { get; set; }

        public int EntityId { get; set; }

        public int OrderId { get; set; }

        public int ReviewType { get; set; }

        public int StoreId { get; set; }

        public bool IsApproved { get; set; }

        public string ReviewForName { get; set; }

        public string ReviewTitle { get; set; }

        public string ReviewText { get; set; }

        public string ReplyText { get; set; }

        public int Rating { get; set; }

        public string ReviewOptions { get; set; }

        public string SelectedReviewOptions { get; set; }

        public string DisLikedReviewOptions { get; set; }

        public string ProfilePicture { get; set; }

        public bool IsLiked { get; set; }

        public bool IsDisLiked { get; set; }

        public bool IsNotInterested { get; set; }

        public bool IsSkipped { get; set; }

        public bool ShowAdditionalNotes { get; set; }

        public bool IsReviewSubmitted { get; set; }

        public List<ItemsModel> Items { get; set; }

        public List<NBReviewOptionModel> NBReviewOptions { get; set; }


        public partial class ItemsModel : BaseNopEntityModel
        {
            public int ProductId { get; set; }
            public int Rating { get; set; }
            public string Name { get; set; }
            public string ProductPicture { get; set; }
        }
    }
}