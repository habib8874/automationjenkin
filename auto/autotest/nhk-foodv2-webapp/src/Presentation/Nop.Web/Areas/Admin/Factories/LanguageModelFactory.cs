﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Localization;
using Nop.Services.Localization;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Localization;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the language model factory implementation
    /// </summary>
    public partial class LanguageModelFactory : ILanguageModelFactory
    {
        #region Fields
        
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly IStoreMappingSupportedModelFactory _storeMappingSupportedModelFactory;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;
        private readonly IStoreMappingService _storeMappingService;

        #endregion

        #region Ctor

        public LanguageModelFactory(IBaseAdminModelFactory baseAdminModelFactory,
            ILanguageService languageService,
            ILocalizationService localizationService,
            IStoreMappingSupportedModelFactory storeMappingSupportedModelFactory,
            IStoreService storeService,
            IWorkContext workContext,
            IStoreMappingService storeMappingService)
        {
            _baseAdminModelFactory = baseAdminModelFactory;
            _languageService = languageService;
            _localizationService = localizationService;
            _storeMappingSupportedModelFactory = storeMappingSupportedModelFactory;
            _storeService = storeService;
            _workContext = workContext;
            _storeMappingService = storeMappingService;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Prepare locale resource search model
        /// </summary>
        /// <param name="searchModel">Locale resource search model</param>
        /// <param name="language">Language</param>
        /// <returns>Locale resource search model</returns>
        protected virtual LocaleResourceSearchModel PrepareLocaleResourceSearchModel(LocaleResourceSearchModel searchModel, Language language)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (language == null)
                throw new ArgumentNullException(nameof(language));

            searchModel.LanguageId = language.Id;

            //prepare page parameters
            searchModel.SetGridPageSize();

            //prepare available stores
            _baseAdminModelFactory.PrepareStores(searchModel.AddResourceString.AvailableStores);

            return searchModel;
        }
        
        #endregion

        #region Methods

        /// <summary>
        /// Prepare language search model
        /// </summary>
        /// <param name="searchModel">Language search model</param>
        /// <returns>Language search model</returns>
        public virtual LanguageSearchModel PrepareLanguageSearchModel(LanguageSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged language list model
        /// </summary>
        /// <param name="searchModel">Language search model</param>
        /// <returns>Language list model</returns>
        public virtual LanguageListModel PrepareLanguageListModel(LanguageSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get languages
            var languages = _languageService.GetAllLanguages(showHidden: true, storeId: _workContext.IsAdminRole ? 0 : _workContext.GetCurrentStoreId, loadCacheableCopy: false).ToPagedList(searchModel);

            //get stores
            var allStores = _storeService.GetAllStores();

            //prepare list model
            var model = new LanguageListModel().PrepareToGrid(searchModel, languages, () =>
            {
                return languages.Select(language =>
                {
                    var languageModel = language.ToModel<LanguageModel>();
                    if (_workContext.IsAdminRole)
                    {
                        var storeIds = _storeMappingService.GetStoresIdsWithAccess(language).ToList();
                        var stores = allStores.Where(x => storeIds.Contains(x.Id)).ToList();
                        languageModel.StoreName = stores.Count > 0 ? string.Join(", ", stores.Select(x => x.Name)) : _localizationService.GetResource("Admin.Configuration.Settings.AllSettings.Fields.StoreName.AllStores");
                    }
                    else
                    {
                        var store = allStores.Where(x => x.Id == _workContext.GetCurrentStoreId).FirstOrDefault();
                        languageModel.StoreName = store == null ? string.Empty : store.Name;
                    }
                    return languageModel;
                });
            });

            return model;
        }

        /// <summary>
        /// Prepare language model
        /// </summary>
        /// <param name="model">Language model</param>
        /// <param name="language">Language</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Language model</returns>
        public virtual LanguageModel PrepareLanguageModel(LanguageModel model, Language language, bool excludeProperties = false)
        {
            if (language != null)
            {
                //fill in model values from the entity
                model = model ?? language.ToModel<LanguageModel>();

                //prepare nested search model
                PrepareLocaleResourceSearchModel(model.LocaleResourceSearchModel, language);

                // check is store language
                var storeIds = _storeMappingService.GetStoresIdsWithAccess(language).ToList();
                model.IsStoreLanguage = storeIds.Count == 1 && storeIds.Any(x => x == _workContext.GetCurrentStoreId);

            }

            //set default values for the new model
            if (language == null)
            {
                model.DisplayOrder = _languageService.GetAllLanguages().Max(l => l.DisplayOrder) + 1;
                model.Published = true;
            }

            //prepare available currencies
            //TODO: add locale resource for "---"
            _baseAdminModelFactory.PrepareCurrencies(model.AvailableCurrencies, defaultItemText: "---");

            //prepare available stores
            _storeMappingSupportedModelFactory.PrepareModelStores(model, language, excludeProperties);

            return model;
        }

        /// <summary>
        /// Prepare paged locale resource list model
        /// </summary>
        /// <param name="searchModel">Locale resource search model</param>
        /// <param name="language">Language</param>
        /// <returns>Locale resource list model</returns>
        public virtual LocaleResourceListModel PrepareLocaleResourceListModel(LocaleResourceSearchModel searchModel, Language language)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (language == null)
                throw new ArgumentNullException(nameof(language));

            //get locale resources
            //var localeResources = _localizationService.GetAllResourceValues(language.Id, loadPublicLocales: null)
            //    .OrderBy(localeResource => localeResource.Key).AsQueryable();

            var localeResources = _localizationService.GetAllResources(language.Id).AsQueryable();

            //filter locale resources
            //TODO: move filter to language service
            if (!string.IsNullOrEmpty(searchModel.SearchResourceName))
                localeResources = localeResources.Where(l => l.ResourceName.ToLowerInvariant().Contains(searchModel.SearchResourceName.ToLowerInvariant()));
            if (!string.IsNullOrEmpty(searchModel.SearchResourceValue))
                localeResources = localeResources.Where(l => l.ResourceValue.ToLowerInvariant().Contains(searchModel.SearchResourceValue.ToLowerInvariant()));

            var pagedLocaleResources = localeResources.ToList().ToPagedList(searchModel);

            //prepare list model
            var model = new LocaleResourceListModel().PrepareToGrid(searchModel, pagedLocaleResources, () =>
            {
                var objModel = new List<LocaleResourceModel>();
                //fill in model values from the entity
                foreach (var localeResource in pagedLocaleResources)
                {
                    if (_workContext.IsAdminRole || !(pagedLocaleResources.Any(x=>x.ResourceName == localeResource.ResourceName && x.StoreId > 0 && localeResource.StoreId == 0)))
                    {
                        var m = new LocaleResourceModel
                        {
                            LanguageId = localeResource.LanguageId,
                            Id = localeResource.Id,
                            ResourceName = localeResource.ResourceName,
                            ResourceValue = localeResource.ResourceValue,
                            StoreId = localeResource.StoreId
                        };
                        //fill in additional values (not existing in the entity)
                        m.Store = localeResource.StoreId > 0
                            ? _storeService.GetStoreById(localeResource.StoreId)?.Name ?? "Deleted"
                            : _localizationService.GetResource("Admin.Configuration.Settings.AllSettings.Fields.StoreName.AllStores");

                        objModel.Add(m);
                    }
                }
                return objModel;

            });

            return model;
        }

        #endregion
    }
}