﻿using Microsoft.AspNetCore.Mvc;
using NHKCustomerApplication.Caching;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Services;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModel;
using NHKCustomerApplication.ViewModels;
using System.Data;
using System.Linq;

namespace NHKCustomerApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogController : ControllerBase
    {
        private ecuadordevContext dbcontext;
        private readonly ICatalogServices _catalogService;
        private readonly ICacheManager _cacheManager;
        public CatalogController(ICatalogServices catalogService, ICacheManager cacheManager, ecuadordevContext context)
        {
            dbcontext = context;
            _catalogService = catalogService;
            _cacheManager = cacheManager;
        }
        /// <summary>
        /// Get Bussiness Category of merchant
        /// </summary>
        /// <param name="merchantCategorysModelV2"></param>
        /// <returns></returns>
        [Route("~/api/v1/GetBussinessCategory")]
        [HttpPost]
        public BaseAPIResponseModel GetMerchantCategorys_v1(MerchantCategorysModelV2 merchantCategorysModelV2)
        {
            int languageId = LanguageHelper.GetIdByLangCode(merchantCategorysModelV2.StoreId, merchantCategorysModelV2.UniqueSeoCode, dbcontext);

            if (merchantCategorysModelV2 == null)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(merchantCategorysModelV2.StoreId, "NB.API.ErrorMesaage.ModelNotDefined", languageId, dbcontext));

            else if (string.IsNullOrEmpty(merchantCategorysModelV2.ApiKey) || string.IsNullOrWhiteSpace(merchantCategorysModelV2.ApiKey))
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(merchantCategorysModelV2.StoreId, "NB.API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext));


            else if (merchantCategorysModelV2.StoreId == 0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(merchantCategorysModelV2.StoreId, "NB.API.GetAllStoreLanguages.StoreNotFound", languageId, dbcontext));


            if (!string.IsNullOrEmpty(merchantCategorysModelV2.ApiKey) || !string.IsNullOrWhiteSpace(merchantCategorysModelV2.ApiKey))
            {
                var keyExist = Helper.AuthenticateKey(merchantCategorysModelV2.ApiKey, merchantCategorysModelV2.StoreId, dbcontext,_cacheManager);
                if (keyExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(merchantCategorysModelV2.StoreId, "NB.API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext));
            }
            var key = string.Format(CacheKeys.BusinessCategoryListKey,merchantCategorysModelV2.StoreId,merchantCategorysModelV2.PageIndex,merchantCategorysModelV2.PageSize,merchantCategorysModelV2.LatPos,merchantCategorysModelV2.LongPos,merchantCategorysModelV2.RestType,merchantCategorysModelV2.Keywords,languageId );
            var data = _cacheManager.Get(key, () =>
            {
                return _catalogService.GetMerchantCategory(merchantCategorysModelV2, languageId);
            });
            if (data.Count <= 0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(merchantCategorysModelV2.StoreId, "NB.API.GetAllStoreLanguages.NoMerchantCategoryFound", languageId, dbcontext));
            return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(merchantCategorysModelV2.StoreId, "NB.API.GetAllStoreLanguages.MerchantCategoryFound", languageId, dbcontext), data);

        }

        /// <summary>
        /// Get Merchant Category Listing
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        [Route("~/api/v1/GetTopCategorysListing")]
        [HttpPost]
        public BaseAPIResponseModel GetCategoryListing(Categorys category)
        {
            int languageId = LanguageHelper.GetIdByLangCode(category.StoreId, category.UniqueSeoCode, dbcontext);

            if (category == null)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(category.StoreId, "NB.API.ErrorMesaage.ModelNotDefined", languageId, dbcontext));

            else if (string.IsNullOrEmpty(category.ApiKey) || string.IsNullOrWhiteSpace(category.ApiKey))
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(category.StoreId, "NB.API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext));

            else if (category.StoreId == 0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(category.StoreId, "NB.API.ErrorMesaage.StoreIdMissing", languageId, dbcontext));

            if (!string.IsNullOrEmpty(category.ApiKey) || !string.IsNullOrWhiteSpace(category.ApiKey))
            {
                var keyExist = Helper.AuthenticateKey(category.ApiKey, category.StoreId, dbcontext, _cacheManager);
                if (keyExist == null)
                    return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(category.StoreId, "NB.API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext));
            }
            //Brings Categorylisting data
            var key = string.Format(CacheKeys.CategoryListKey, category.StoreId, languageId, category.RestncookId, category.ParentCategoryId, category.PageIndex, category.PageSize, category.Keywords);
            var categorydata = _cacheManager.Get(key, () =>
            {
                return _catalogService.GetCategorys(category, languageId);
            });
            if (categorydata.Count <= 0)
                return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(category.StoreId, "NB.API.ErrorMesaageChefNotProvidingAnyProductForSellYet", languageId, dbcontext));
            return BaseMethodsHelper.SuccessListResponse(LanguageHelper.GetResourseValueByName(category.StoreId, "NB.API.ErrorMesaage.CategoryFound", languageId, dbcontext), categorydata, categorydata.Select(x => x.TotalRecords).FirstOrDefault());
        }
    }
}
