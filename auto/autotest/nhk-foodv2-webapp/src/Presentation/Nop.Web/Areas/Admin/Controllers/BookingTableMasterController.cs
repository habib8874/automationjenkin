﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Primitives;
using Nop.Web.Areas.Admin.Helpers;
using Nop.Web.Areas.Admin.Infrastructure.Cache;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Areas.Admin.Models.Orders;

using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Vendors;
using Nop.Services;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.ExportImport;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Services.Shipping.Date;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Extensions;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Core.Data;
using Nop.Core.Domain.BookingTable;
using Nop.Web.Areas.Admin.Models.BookingTable;
using Nop.Services.BookingTable;
using Nop.Web.Framework.Models.Extensions;
using Nop.Services.Messages;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class BookingTableMasterController : BaseAdminController
    {
        #region Fields
        private readonly IBookingService _bookingService;
        private readonly ILanguageService _SMSLanguageModuleService;
        private readonly IRepository<BookingTableMaster> _BookingTableMasterRepository;
        private readonly IProductService _productService;
        private readonly IProductTemplateService _productTemplateService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly ICustomerService _customerService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IWorkContext _workContext;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly IPictureService _pictureService;
        private readonly ITaxCategoryService _taxCategoryService;
        private readonly IProductTagService _productTagService;
        private readonly ICopyProductService _copyProductService;
        private readonly IPdfService _pdfService;
        private readonly IExportManager _exportManager;
        private readonly IImportManager _importManager;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IPermissionService _permissionService;
        private readonly IAclService _aclService;
        private readonly IStoreService _storeService;
        private readonly IOrderService _orderService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IVendorService _vendorService;
        private readonly IDateRangeService _dateRangeService;
        private readonly IShippingService _shippingService;
        private readonly IShipmentService _shipmentService;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;
        private readonly IMeasureService _measureService;
        private readonly MeasureSettings _measureSettings;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IDiscountService _discountService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IBackInStockSubscriptionService _backInStockSubscriptionService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IDownloadService _downloadService;
        private readonly ISettingService _settingService;
        private readonly TaxSettings _taxSettings;
        private readonly VendorSettings _vendorSettings;
        private readonly INotificationService _notificationService;

        #endregion

        #region Ctor

        public BookingTableMasterController(
            IBookingService bookingService,
            ILanguageService SMSLanguageModuleService,
            IRepository<BookingTableMaster> BookingTableMasterRepository,        
            IProductService productService,
            IProductTemplateService productTemplateService,
            ICategoryService categoryService,
            IManufacturerService manufacturerService,
            ICustomerService customerService,
            IUrlRecordService urlRecordService,
            IWorkContext workContext,
            ILanguageService languageService,
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService,
            ISpecificationAttributeService specificationAttributeService,
            IPictureService pictureService,
            ITaxCategoryService taxCategoryService,
            IProductTagService productTagService,
            ICopyProductService copyProductService,
            IPdfService pdfService,
            IExportManager exportManager,
            IImportManager importManager,
            ICustomerActivityService customerActivityService,
            IPermissionService permissionService,
            IAclService aclService,
            IStoreService storeService,
            IOrderService orderService,
            IStoreMappingService storeMappingService,
            IVendorService vendorService,
            IDateRangeService dateRangeService,
            IShippingService shippingService,
            IShipmentService shipmentService,
            ICurrencyService currencyService,
            CurrencySettings currencySettings,
            IMeasureService measureService,
            MeasureSettings measureSettings,
            IStaticCacheManager cacheManager,
            IDateTimeHelper dateTimeHelper,
            IDiscountService discountService,
            IProductAttributeService productAttributeService,
            IBackInStockSubscriptionService backInStockSubscriptionService,
            IShoppingCartService shoppingCartService,
            IProductAttributeFormatter productAttributeFormatter,
            IProductAttributeParser productAttributeParser,
            IDownloadService downloadService,
            ISettingService settingService,
            TaxSettings taxSettings,
            VendorSettings vendorSettings,
            INotificationService notificationService)
        {
            _SMSLanguageModuleService = SMSLanguageModuleService;
            _BookingTableMasterRepository = BookingTableMasterRepository;
            _bookingService = bookingService;
            _productService = productService;
            _productTemplateService = productTemplateService;
            _categoryService = categoryService;
            _manufacturerService = manufacturerService;
            _customerService = customerService;
            _urlRecordService = urlRecordService;
            _workContext = workContext;
            _languageService = languageService;
            _localizationService = localizationService;
            _localizedEntityService = localizedEntityService;
            _specificationAttributeService = specificationAttributeService;
            _pictureService = pictureService;
            _taxCategoryService = taxCategoryService;
            _productTagService = productTagService;
            _copyProductService = copyProductService;
            _pdfService = pdfService;
            _exportManager = exportManager;
            _importManager = importManager;
            _customerActivityService = customerActivityService;
            _permissionService = permissionService;
            _aclService = aclService;
            _storeService = storeService;
            _orderService = orderService;
            _storeMappingService = storeMappingService;
            _vendorService = vendorService;
            _dateRangeService = dateRangeService;
            _shippingService = shippingService;
            _shipmentService = shipmentService;
            _currencyService = currencyService;
            _currencySettings = currencySettings;
            _measureService = measureService;
            _measureSettings = measureSettings;
            _cacheManager = cacheManager;
            _dateTimeHelper = dateTimeHelper;
            _discountService = discountService;
            _productAttributeService = productAttributeService;
            _backInStockSubscriptionService = backInStockSubscriptionService;
            _shoppingCartService = shoppingCartService;
            _productAttributeFormatter = productAttributeFormatter;
            _productAttributeParser = productAttributeParser;
            _downloadService = downloadService;
            _settingService = settingService;
            _taxSettings = taxSettings;
            _vendorSettings = vendorSettings;
            _notificationService = notificationService;
        }

        #endregion

        #region Utilities

        protected virtual void UpdateLocales(Product product, ProductModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(product,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                    x => x.ShortDescription,
                    localized.ShortDescription,
                    localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                    x => x.FullDescription,
                    localized.FullDescription,
                    localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                    x => x.MetaKeywords,
                    localized.MetaKeywords,
                    localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                    x => x.MetaDescription,
                    localized.MetaDescription,
                    localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                    x => x.MetaTitle,
                    localized.MetaTitle,
                    localized.LanguageId);

                //search engine name
                var seName = _urlRecordService.ValidateSeName(product, localized.SeName, localized.Name, false);
                _urlRecordService.SaveSlug(product, seName, localized.LanguageId);
            }
        }

        protected virtual void UpdateLocales(ProductTag productTag, ProductTagModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(productTag,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);
            }
        }

        protected virtual void UpdateLocales(ProductAttributeMapping pam, ProductAttributeMappingModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(pam,
                    x => x.TextPrompt,
                    localized.TextPrompt,
                    localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(pam,
                    x => x.DefaultValue,
                    localized.DefaultValue,
                    localized.LanguageId);
            }
        }

        protected virtual void UpdateLocales(ProductAttributeValue pav, ProductAttributeValueModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(pav,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);
            }
        }

        protected virtual void UpdatePictureSeoNames(Product product)
        {
            foreach (var pp in product.ProductPictures)
                _pictureService.SetSeoFilename(pp.PictureId, _pictureService.GetPictureSeName(product.Name));
        }

        protected virtual void PrepareAclModel(ProductModel model, Product product, bool excludeProperties)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (!excludeProperties && product != null)
                model.SelectedCustomerRoleIds = _aclService.GetCustomerRoleIdsWithAccess(product).ToList();

            var allRoles = _customerService.GetAllCustomerRoles(true);
            foreach (var role in allRoles)
            {
                model.AvailableCustomerRoles.Add(new SelectListItem
                {
                    Text = role.Name,
                    Value = role.Id.ToString(),
                    Selected = model.SelectedCustomerRoleIds.Contains(role.Id)
                });
            }
        }

        protected virtual void SaveProductAcl(Product product, ProductModel model)
        {
            product.SubjectToAcl = model.SelectedCustomerRoleIds.Any();

            var existingAclRecords = _aclService.GetAclRecords(product);
            var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
            foreach (var customerRole in allCustomerRoles)
            {
                if (model.SelectedCustomerRoleIds.Contains(customerRole.Id))
                {
                    //new role
                    if (existingAclRecords.Count(acl => acl.CustomerRoleId == customerRole.Id) == 0)
                        _aclService.InsertAclRecord(product, customerRole.Id);
                }
                else
                {
                    //remove role
                    var aclRecordToDelete = existingAclRecords.FirstOrDefault(acl => acl.CustomerRoleId == customerRole.Id);
                    if (aclRecordToDelete != null)
                        _aclService.DeleteAclRecord(aclRecordToDelete);
                }
            }
        }

        

        #endregion

        #region Methods

        #region Product list / create / edit / delete

        //list products
        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        //public virtual IActionResult List()
        //{
        //    if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingMaster))
        //        return AccessDeniedView();
        //    var model = new BookingTableMasterListModel();
        //    int vendorId = 0;
        //    var vendor = _workContext.CurrentVendor;
        //    if (vendor != null)
        //    {
        //        vendorId = _workContext.CurrentVendor.Id;
        //    }
        //    if (vendorId != 0)
        //    {
        //        model.IsCurrentVendor = true;
        //        model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
        //        var vandors = SelectListHelper.GetVendorList(_vendorService, _cacheManager, true);
        //        foreach (var c in vandors)
        //        {
        //            if (c.Value == vendorId.ToString())
        //                model.AvailableVendors.Add(c);
        //        }

        //    }
        //    else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
        //    {
        //        var customerId = _workContext.CurrentCustomer.Id;
        //        var customer = _customerService.GetAllCustomers().Where(x => x.Id == customerId).FirstOrDefault();

        //        //vendor
        //        model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
        //        model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "-1" });
        //        var vandors = SelectListHelper.GetVendorList(_vendorService, _cacheManager, true);
        //        foreach (var c in vandors)
        //            model.AvailableVendors.Add(c);
        //        model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
        //        var isAdmin = _workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any();
        //        if (isAdmin)
        //            model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "-1" });
        //        foreach (var s in _storeService.GetAllStores())
        //        {
        //            //if (s.Id == customer.RegisteredInStoreId) //Comment By Mah
        //                model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });
        //        }

        //    }
        //    else if (_workContext.CurrentCustomer.RegisteredInStoreId != 0)
        //    {
        //        List<int> vendors = new List<int>();
        //        var customer = _customerService.GetAllCustomers().Where(x => x.RegisteredInStoreId == _workContext.CurrentCustomer.RegisteredInStoreId).ToList();
        //        if (customer.Any())
        //        {
        //            vendors = customer.Select(x => x.VendorId).ToList();
        //        }
        //        int storeId = _workContext.CurrentCustomer.RegisteredInStoreId;
        //        model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
        //        foreach (var s in _storeService.GetAllStores())
        //        {
        //            if (s.Id == storeId)
        //                model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });
        //        }
        //        var vandorslist = SelectListHelper.GetVendorList(_vendorService, _cacheManager, true);
        //        if (vandorslist.Any())
        //        {
        //            foreach (var c in vandorslist)
        //            {
        //                if (vendors.Contains(Convert.ToInt32(c.Value)))
        //                    model.AvailableVendors.Add(c);
        //            }
        //        }

        //    }
        //    else
        //    {

        //    }


        //    return View(model);
        //}

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingMaster))
                return AccessDeniedView();
            var model = new BookingTableMasterListSearchModel();
            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
            {
                var customerId = _workContext.CurrentCustomer.Id;
                var customer = _customerService.GetAllCustomers().Where(x => x.Id == customerId).FirstOrDefault();

                //vendor
                model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                var vandorslist = _vendorService.GetAllVendors();
                if (vandorslist != null && vandorslist.Any())
                {
                    foreach (var c in vandorslist)
                    {
                            model.AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
                model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                foreach (var s in _storeService.GetAllStores())
                {
                    model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });
                }

            }
            else if(_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                var customer = _customerService.GetAllCustomers().Where(x => x.RegisteredInStoreId == _workContext.CurrentCustomer.RegisteredInStoreId).ToList();
                model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                var vandorslist = _vendorService.GetAllVendors();
                if (vandorslist != null && vandorslist.Any())
                {
                    foreach (var c in vandorslist)
                    {
                        if (c.StoreId == stores.Id)
                            model.AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
            }
            model.SetGridPageSize();

            return View(model);
        }

        


        [HttpPost]
        public virtual IActionResult BookingMasterList(BookingTableMasterListSearchModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingMaster))
                return AccessDeniedDataTablesJson();

            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
            {
                if (model.SearchStoreId <= 0)
                    model.SearchStoreId = 0;
                if (model.SearchVendorId <= 0)
                    model.SearchVendorId = 0;
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                if(model.SearchStoreId==0)
                {
                    var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                    model.SearchStoreId = stores.Id;
                }
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
            {
                if(model.SearchVendorId==0)
                {
                    var vendorId = _customerService.GetAllCustomers().FirstOrDefault(x => x.Id == _workContext.CurrentCustomer.Id).VendorId;
                    model.SearchVendorId = vendorId;
                }
            }

            var booking = _bookingService.SearchBooking(
                //categoryIds: categoryIds,
                //manufacturerId: model.SearchManufacturerId,
                storeId: model.SearchStoreId,
                vendorId: model.SearchVendorId,

                TableNumber: model.SearchTableNo,
                //warehouseId: model.SearchWarehouseId,
                //productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
                //keywords: model.SearchProductName,
                //pageIndex: command.Page - 1,
                pageSize: model.PageSize,
                showHidden: true
            //overridePublished: overridePublished
            );
            var modelList = new BookingTableMasterGridListModel().PrepareToGrid(model, booking, () =>
            {
                return booking.Select(x =>
                {
                    var booktable = new BookingTableMasterListModel
                    {
                        Id = x.Id,
                        TableNo = x.TableNo,
                        Status = x.Status,
                        Seats = x.Seats,
                        TableAvailableFrom = x.TableAvailableFrom,
                        TableAvailableTo = x.TableAvailableTo
                    };
                    booktable.Store = _storeService.GetStoreById(x.StoreId) == null ? "Na" : _storeService.GetStoreById(x.StoreId).Name;
                    booktable.Vendor = _vendorService.GetVendorById(x.VendorId) == null ? "Na" : _vendorService.GetVendorById(x.VendorId).Name;
                    return booktable;
                });
            });


            return Json(modelList);
        }

        [HttpPost]
        public virtual IActionResult BulkEditUpdate(IEnumerable<BookingTableMasterListModel> bookingTableMasterModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingMaster))
                return AccessDeniedView();

            if (bookingTableMasterModel != null)
            {
                foreach (var pModel in bookingTableMasterModel)
                {
                    //update
                    var bookingMaster = _BookingTableMasterRepository.Table.Where(x => x.Id == pModel.Id).FirstOrDefault();
                    if (_BookingTableMasterRepository.Table.Where(x => x.StoreId == bookingMaster.StoreId && x.VendorId == bookingMaster.VendorId && x.TableNo == pModel.TableNo && x.Id != pModel.Id).Any())
                    {
                        _notificationService.ErrorNotification("Table number already exist!");
                        return RedirectToAction("List");
                    }
                    var product = _BookingTableMasterRepository.GetById(pModel.Id);
                    if (product != null)
                    {
                        //a vendor should have access only to his products
                        if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                            continue;
                        product.TableNo = pModel.TableNo;
                        product.Seats = pModel.Seats;
                        product.Status = pModel.Status;
                        _BookingTableMasterRepository.Update(product);
                    }
                }
            }

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingMaster))
                return AccessDeniedView();
            if (id == 0)
            {
                return RedirectToAction("List");
            }
            var bookingMaster = _bookingService.GetMasterBookingById(id);
            if (bookingMaster == null)
                //No bookingMaster found with the specified id
                return RedirectToAction("List");



            var results = _bookingService.DeleteMasterBookingById(bookingMaster);

            //activity log
            _customerActivityService.InsertActivity("DeleteBookingTableMaster", _localizationService.GetResource("ActivityLog.DeleteProduct"), bookingMaster);

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Bookings.Table.Deleted"));
            return RedirectToAction("List");
        }

        [HttpPost]
        public virtual IActionResult DeleteSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingMaster))
                return AccessDeniedView();

            foreach (var item in selectedIds)
            {
                var bookingMaster = _bookingService.GetMasterBookingById(item);
                if(bookingMaster!=null)
                {
                var results = _bookingService.DeleteMasterBookingById(bookingMaster);
                }
                else
                {
                    _notificationService.ErrorNotification(_localizationService.GetResource("Booking Not found"));
                }

            }

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Bookings.Table.Deleted"));

            return Json(new { Result = true });
        }
        //Create prodScheduleuct
        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBookingMaster))
                return AccessDeniedView();
            var model = new BookingTableMasterListModel();

            try
            {
                if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
                {
                    var customerId = _workContext.CurrentCustomer.Id;
                    var customer = _customerService.GetAllCustomers().Where(x => x.Id == customerId).FirstOrDefault();

                    //vendor

                    model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                    foreach (var s in _storeService.GetAllStores())
                    {
                        model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });
                    }
                    model.SearchVendorId = 0;
                    model.SearchStoreId = 0;

                }
                else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
                {
                    var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                    var customer = _customerService.GetAllCustomers().Where(x => x.RegisteredInStoreId == _workContext.CurrentCustomer.RegisteredInStoreId).ToList();
                    model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                    var vandorslist = _vendorService.GetAllVendors();
                    if (vandorslist != null && vandorslist.Any())
                    {
                        foreach (var c in vandorslist)
                        {
                            if (c.StoreId == stores.Id)
                                model.AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                        }
                    }
                    model.SearchStoreId = stores.Id;
                    model.SearchVendorId = 0;
                }
                else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
                {
                    var vendor = _customerService.GetAllCustomers().FirstOrDefault(x => x.Id == _workContext.CurrentCustomer.Id);
                    ///*bookingTableTransaction.StoreId = vendor.Re*/gisteredInStoreId;
                    model.SearchVendorId = vendor.VendorId;
                    model.SearchStoreId = vendor.RegisteredInStoreId;
                }

                foreach (var s in _storeService.GetAllAvailableTiming())
                {
                    model.AvailableFromTiming.Add(new SelectListItem { Text = s.ToString(), Value = s.ToString() });
                }
                foreach (var s in _storeService.GetAllAvailableTiming())
                {
                    model.AvailableToTiming.Add(new SelectListItem { Text = s.ToString(), Value = s.ToString() });
                }
                return View(model);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public virtual IList<SelectListItem> GetAvailableVendors(string Store)
        {
            string select = "0";
            IList<SelectListItem> AvailableVendors = new List<SelectListItem>();
            if (Store == select)
            {

                AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
            }
            else
            {
                int storeId = Convert.ToInt32(Store);
                AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                var vandorslist = _vendorService.GetAllVendors();
                if (vandorslist != null && vandorslist.Any())
                {
                    foreach (var c in vandorslist)
                    {
                        if (c.StoreId == storeId)
                            AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
            }
            //var storeId = _workContext.CurrentCustomer.RegisteredInStoreId;

            return AvailableVendors;
        }

        [HttpPost]
        //Create prodScheduleuct
        public virtual IActionResult Create(BookingTableMasterListModel bookingTableMasterListModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSchedules))
                return AccessDeniedView();
            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
            {
                var customerId = _workContext.CurrentCustomer.Id;
                var customer = _customerService.GetAllCustomers().Where(x => x.Id == customerId).FirstOrDefault();

                //vendor

                bookingTableMasterListModel.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                foreach (var s in _storeService.GetAllStores())
                {
                    bookingTableMasterListModel.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });
                }

            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                var customer = _customerService.GetAllCustomers().Where(x => x.RegisteredInStoreId == _workContext.CurrentCustomer.RegisteredInStoreId).ToList();
                bookingTableMasterListModel.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });
                var vandorslist = _vendorService.GetAllVendors();
                if (vandorslist != null && vandorslist.Any())
                {
                    foreach (var c in vandorslist)
                    {
                        if (c.StoreId == stores.Id)
                            bookingTableMasterListModel.AvailableVendors.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
                bookingTableMasterListModel.SearchStoreId = stores.Id;
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
            {
                var vendor = _customerService.GetAllCustomers().FirstOrDefault(x => x.Id == _workContext.CurrentCustomer.Id);
                ///*bookingTableTransaction.StoreId = vendor.Re*/gisteredInStoreId;
                bookingTableMasterListModel.SearchVendorId = vendor.VendorId;
                bookingTableMasterListModel.SearchStoreId = vendor.RegisteredInStoreId;
            }
            if (bookingTableMasterListModel.SearchStoreId == 0)
            {
                
                _notificationService.ErrorNotification("Please Select Store");
                return View(bookingTableMasterListModel);
            }
            if (bookingTableMasterListModel.SearchVendorId == 0)
            {
                
                _notificationService.ErrorNotification("Please Select Merchant");
                return View(bookingTableMasterListModel);
            }
            //a vendor should have access only to his scheule
            if (bookingTableMasterListModel == null)
                return RedirectToAction("Create");
            BookingTableMaster bookingTable = new BookingTableMaster();
            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
            {
                bookingTable.StoreId = bookingTableMasterListModel.SearchStoreId;
                bookingTable.VendorId = bookingTableMasterListModel.SearchVendorId;

            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                bookingTable.StoreId = stores.Id;
                bookingTable.VendorId = bookingTableMasterListModel.SearchVendorId;
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
            {
                var vendor = _customerService.GetAllCustomers().FirstOrDefault(x => x.Id == _workContext.CurrentCustomer.Id);
                bookingTable.StoreId = vendor.RegisteredInStoreId;
                bookingTable.VendorId = vendor.VendorId;
            }
            bookingTable.CreatedAt = DateTime.UtcNow;
            bookingTable.Status = false;
            bookingTable.TableNo = bookingTableMasterListModel.TableNo;
            bookingTable.Seats = bookingTableMasterListModel.Seats;
            var result = _bookingService.CreateBookingMaster(bookingTable);
            if (!result)
            {
                
                _notificationService.ErrorNotification("Table number already exist");
                return View(bookingTableMasterListModel);
            }
            return RedirectToAction("List");

        }



        //edit product




        #endregion



        #endregion

    }
}