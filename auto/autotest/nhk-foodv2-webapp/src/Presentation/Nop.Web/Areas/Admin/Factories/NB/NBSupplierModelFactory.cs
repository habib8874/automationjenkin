﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.NB.Suppliers;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Services.NB.Suppliers;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Areas.Admin.Models.Common;
using Nop.Web.Areas.Admin.Models.NB.Suppliers;
using Nop.Web.Framework.Models.Extensions;
using DataSourceRequest = Nop.Web.Areas.Admin.Models.NB.Suppliers.DataSourceRequest;

namespace Nop.Web.Areas.Admin.Factories
{
    public partial class NBSupplierModelFactory : INbSupplierModelFactory
    {

        #region Fields

        private readonly IAddressAttributeModelFactory _addressAttributeModelFactory;
        private readonly IAddressService _addressService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ILocalizationService _localizationService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ISupplierService _supplierService;
        private readonly IStoreService _storeService;
        private readonly IProductService _productService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor
        public NBSupplierModelFactory(IAddressAttributeModelFactory addressAttributeModelFactory,
            IAddressService addressService,
            IBaseAdminModelFactory baseAdminModelFactory,
            ILocalizationService localizationService,
            IUrlRecordService urlRecordService,
            ISupplierService supplierService,
            IProductService productService,
            IStoreService storeService,
            IWorkContext workContext)
           
        {
            _addressAttributeModelFactory = addressAttributeModelFactory;
            _addressService = addressService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _localizationService = localizationService;
            _urlRecordService = urlRecordService;
            _supplierService = supplierService;
            _productService = productService;
            _storeService = storeService;
            _workContext = workContext;
        }

        #endregion

        /// <summary>
        /// Prepare address model
        /// </summary>
        /// <param name="model">Address model</param>
        /// <param name="address">Address</param>
        protected virtual void PrepareAddressModel(AddressModel model, Address address)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            //set some of address fields as enabled and required
            model.CountryEnabled = true;
            model.StateProvinceEnabled = true;
            model.CountyEnabled = true;
            model.CityEnabled = true;
            model.StreetAddressEnabled = true;
            model.StreetAddress2Enabled = true;
            model.ZipPostalCodeEnabled = true;
            model.PhoneEnabled = true;

            //prepare available countries
            _baseAdminModelFactory.PrepareCountries(model.AvailableCountries);

            //prepare available states
            _baseAdminModelFactory.PrepareStatesAndProvinces(model.AvailableStates, model.CountryId);

            //prepare custom address attributes
            _addressAttributeModelFactory.PrepareCustomAddressAttributes(model.CustomAddressAttributes, address);
        }



        /// <summary>
        /// Prepare Supplier search model
        /// </summary>
        /// <param name="searchModel">Supplier search model</param>
        /// <returns>Supplier search model</returns>
        public virtual SupplierSearchModel PrepareSupplierSearchModel(SupplierSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }


        /// <summary>
        /// Prepare paged Supplier list model
        /// </summary>
        /// <param name="searchModel">Supplier search model</param>
        /// <returns>Supplier list model</returns>
        /// 

        public virtual SupplierListModel PrepareSupplierListModel(SupplierSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));



            if (_workContext.IsAdminRole)
            {
                //get suppliers
                var suppliers = _supplierService.GetAllSupplier(showHidden: true,
                    name: searchModel.SupplierName,
                    pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

                //prepare list model
                var model = new SupplierListModel().PrepareToGrid(searchModel, suppliers, () =>
                {
                    //fill in model values from the entity
                    return suppliers.Select(supplier =>
                    {
                        var sup = new SupplierModel()
                        {
                            Id = supplier.Id,
                            SupplierName = supplier.SupplierName,
                            Active=supplier.Active,
                            //Active = true,

                        };
                        return sup;
                    });
                });

                return model;
            }
            else if (_workContext.IsStoreOwnerRole)
            {
                //get vendors
                var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).Select(x => x.Id).ToList();

                var suppliers = _supplierService.GetAllSupplierByStore(stores, searchModel.SupplierName, searchModel.Page - 1, searchModel.PageSize, true);

                //prepare list model
                var model = new SupplierListModel().PrepareToGrid(searchModel, suppliers, () =>
                {
                    //fill in model values from the entity
                    return suppliers.Select(supplier =>
                    {
                        var sup = new SupplierModel()
                        {
                            Id = supplier.Id,
                            SupplierName = supplier.SupplierName,
                            Active = supplier.Active,
                            //Active = true,

                        };
                        return sup;
                    });
                });

                return model;
            }
            else if (_workContext.IsVendorRole)
            {
                int[] ids = new int[1];
                ids[0] = _workContext.CurrentVendor.Id;

                var suppliers = _supplierService.GetSupplierByIds(ids).ToPagedList(searchModel);

                //prepare list model
                var model = new SupplierListModel().PrepareToGrid(searchModel, suppliers, () =>
                {
                    //fill in model values from the entity
                    return suppliers.Select(supplier =>
                    {

                        var sup = new SupplierModel()
                        {
                            Id = supplier.Id,
                            SupplierName = supplier.SupplierName,
                            Active = supplier.Active,
                            //Active = true,

                        };
                        return sup;
                    });
                });

                return model;
            }
            return new SupplierListModel();
        }




        /// <summary>
        /// Get Available Stores
        /// </summary>
        /// <param name="searchModel">AvailableStores search model</param>
        /// <returns>Available Stores</returns>
        /// 
        protected void GetAvailableStores(SupplierModel model)
        {
            var storeIdReg = 0;

            if (_workContext.IsStoreOwnerRole || _workContext.IsVendorRole)
            {
                storeIdReg = _workContext.GetCurrentStoreId;
            }

            //prepare available stores
            var availableStores = _storeService.GetAllStores();
            foreach (var store in availableStores)
            {
                if (store.Id == storeIdReg && storeIdReg > 0)
                {
                    model.AvailableStore.Add(new SelectListItem
                    {
                        Text = store.Name,
                        Value = store.Id.ToString(),
                    });

                }
                if (storeIdReg == 0)
                {
                    model.AvailableStore.Add(new SelectListItem
                    {
                        Text = store.Name,
                        Value = store.Id.ToString(),
                    });
                }
            }


            //insert special item for the default value
            PrepareDefaultItem(model.AvailableStore, true, null);

        }


        /// <summary>
        /// Prepare supplier model
        /// </summary>
        /// <param name="model">Supplier model</param>
        /// <param name="Supplier">Supplier</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Supplier model</returns>
        public virtual SupplierModel PrepareSupplierModel(SupplierModel model, Supplier supplier, bool excludeProperties = false)
        {
          
            if (supplier != null)
            {
                //fill in model values from the entity

                model = model ?? new SupplierModel();
                model.StoreId = supplier.StoreId;
                model.Id = supplier.Id;
                model.SupplierName = supplier.SupplierName;
                model.Active = supplier.Active;
                model.Description = supplier.Description;
                model.LocationMap = supplier.LocationMap;
                model.Geofancing = supplier.Geofancing;
                model.DeliveryFee = supplier.DeliveryFee;
                model.FixedOrDynamic = supplier.FixedOrDynamic;
                model.DistanceForDynamic = supplier.DistanceForDynamic;
                model.KmOrMilesForDynamic = supplier.KmOrMilesForDynamic;
                model.PriceForDynamic = supplier.PriceForDynamic;
                model.PricePerUnitDistanceForDynamic = supplier.PricePerUnitDistanceForDynamic;
               
              
                PrepareSupplierProductSearchModel(model.SupplierProductSearchModel, supplier);
            }

            //#region Supplier Distance fee
            if (supplier == null)
            {
                model.FixedOrDynamic = "f";
                model.KmOrMilesForDynamic = "k";
                model.PriceForDynamic = decimal.Zero;
                model.DistanceForDynamic = 0;
                model.PricePerUnitDistanceForDynamic = decimal.Zero;
            }
            // #endregion

            GetAvailableStores(model);

            //prepare address model
            var address = _addressService.GetAddressById(supplier?.AddressId ?? 0);
            if (!excludeProperties && address != null)
                model.Address = address.ToModel(model.Address);
            PrepareAddressModel(model.Address, address);

            // check is vendor or not
            model.IsSupplier = _workContext.IsVendorRole;



            return model;
        }


        /// <summary>
        /// Prepare Default Item
        /// </summary>
        /// <param name="model">Supplier model</param>
        /// <param name="Supplier">Supplier</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Supplier model</returns>
        protected virtual void PrepareDefaultItem(IList<SelectListItem> items, bool withSpecialDefaultItem, string defaultItemText = null)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));

            //whether to insert the first special item for the default value
            if (!withSpecialDefaultItem)
                return;

            //at now we use "0" as the default value
            const string value = "0";

            //prepare item text
            defaultItemText = defaultItemText ?? _localizationService.GetResource("Admin.Common.All");

            //insert this default item at first
            items.Insert(0, new SelectListItem { Text = defaultItemText, Value = value });
        }

        /// <summary>
        /// Prepare Supplier product search model
        /// </summary>
        /// <param name="searchModel">Supplier product search model</param>
        /// <param name="supplier">Supplier</param>
        /// <returns>Supplier product search model</returns>
        public virtual SupplierProductSearchModel PrepareSupplierProductSearchModel(SupplierProductSearchModel searchModel, Supplier supplier)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (supplier == null)
                throw new ArgumentNullException(nameof(supplier));

            searchModel.SupplierId = supplier.Id;

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged product list model to add to the supplier
        /// </summary>
        /// <param name="searchModel">Product search model to add to the supplier</param>
        /// <returns>Product list model to add to the category</returns>
        public virtual AddProductToSupplierListModel PrepareAddProductToSupplierListModel(AddProductToSupplierSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var customer = _workContext.CurrentCustomer;
            if (_workContext.IsStoreOwnerRole)
            {
                searchModel.SearchStoreId = _workContext.GetCurrentStoreId;
            }
            else if (_workContext.IsVendorRole)
            {
                searchModel.SearchStoreId = _workContext.GetCurrentStoreId;
                searchModel.SearchVendorId = _workContext.CurrentVendor.Id;
            }

            //get products
            var products = _productService.SearchProducts(showHidden: true,
                categoryIds: new List<int> { searchModel.SearchSupplierId },
                manufacturerId: searchModel.SearchManufacturerId,
                storeId: searchModel.SearchStoreId,
                vendorId: searchModel.SearchVendorId,
                productType: searchModel.SearchProductTypeId > 0 ? (ProductType?)searchModel.SearchProductTypeId : null,
                keywords: searchModel.SearchProductName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new AddProductToSupplierListModel().PrepareToGrid(searchModel, products, () =>
            {
                return products.Select(product =>
                {
                    var productModel = product.ToModel<ProductModel>();
                    productModel.SeName = _urlRecordService.GetSeName(product, 0, true, false);

                    return productModel;
                });
            });

            return model;
        }

        // <summary>
        /// Prepare paged product bulk edit list model
        /// </summary>
        /// <param name="searchModel">DataSourceRequest model</param>
        /// <returns>Product list model</returns>
        public virtual SupplierProductListModel PrepareProductBulkEditListModel(DataSourceRequest searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));
            var customer = _workContext.CurrentCustomer;
            if (_workContext.IsStoreOwnerRole)
            {
                searchModel.SearchStoreId = _workContext.GetCurrentStoreId;
            }
            else if (_workContext.IsVendorRole)
            {
                searchModel.SearchStoreId = _workContext.GetCurrentStoreId;
                searchModel.SearchVendorId = _workContext.CurrentVendor.Id;
            }


            //get product categories
            var productsuppliers = _supplierService.GetProductSuppliersBySupplierId(searchModel.SearchSupplierId,
                showHidden: true,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var supplierProductmodel = new SupplierProductListModel().PrepareToGrid(searchModel, productsuppliers, () =>
            {
                return productsuppliers.Select(productSupplier =>
                {

                    var supplierProductModel = new SupplierProductModel();
                    supplierProductModel.ProductId = productSupplier.ProductId;
                    supplierProductModel.Price = productSupplier.Price;
                    supplierProductModel.DisplayOrder = productSupplier.DisplayOrder;
                    supplierProductModel.SupplierId = productSupplier.SupplierId;
                    supplierProductModel.IsFeaturedProduct = productSupplier.IsFeaturedProduct;

                    supplierProductModel.ProductName = _productService.GetProductById(productSupplier.ProductId)?.Name;

                    return supplierProductModel;
                });
            });

            return supplierProductmodel;
        }

        /// <summary>
        /// Prepare product search model to add to the supplier
        /// </summary>
        /// <param name="searchModel">Product search model to add to the supplier</param>
        /// <returns>Product search model to add to the supplier</returns>
        public virtual AddProductToSupplierSearchModel PrepareAddProductToSupplierSearchModel(AddProductToSupplierSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare available categories
            _baseAdminModelFactory.PrepareSuppliers(searchModel.AvailableSuppliers);

            //prepare available manufacturers
            _baseAdminModelFactory.PrepareManufacturers(searchModel.AvailableManufacturers);

            //prepare available stores
            _baseAdminModelFactory.PrepareStores(searchModel.AvailableStores);

            //prepare available product types
            _baseAdminModelFactory.PrepareProductTypes(searchModel.AvailableProductTypes);

            //prepare page parameters
            searchModel.SetPopupGridPageSize();

            return searchModel;
        }
    }
}



