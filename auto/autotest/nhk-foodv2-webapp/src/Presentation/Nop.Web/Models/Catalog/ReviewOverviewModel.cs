﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Catalog
{
    public class ReviewOverviewModel : BaseNopModel
    {
        public ReviewOverviewModel()
        {
            Reviews = new List<ReviewEntityModel>();
        }

        public int TotalReviews { get; set; }
        public int RatingSum { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public IList<ReviewEntityModel> Reviews { get; set; }
    }

    public class ReviewEntityModel : BaseNopModel
    {
        public int ReviewType { get; set; }
        public int EntityId { get; set; }
        public string CustomerName { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string ReviewText { get; set; }
        public string ReplyText { get; set; }
        public int Rating { get; set; }
        public string WrittenOnStr { get; set; }
        public string ApprovalStatus { get; set; }
    }
}
