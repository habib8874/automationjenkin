﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.BookingTable
{
    public partial class BookingTableMasterDetail:BaseEntity
    {
        public int BookingMasterId { get; set; }
        public int ImageId { get; set; }
        public DateTime CreatedAt { get; set; }
        public TimeSpan TableAvailableFrom { get; set; }
        public TimeSpan TableAvailableTo { get; set; }
    }
}
