﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Web.Models.Catalog;

namespace Nop.Web.Components
{
    public class HomepageProductsMerchantViewComponent : NopViewComponent
    {
        #region Feilds

        private readonly IProductModelFactory _productModelFactory;
        private readonly IProductService _productService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly ICategoryService _categoryService;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;

        #endregion

        #region Ctor
        public HomepageProductsMerchantViewComponent(IProductModelFactory productModelFactory,
            IProductService productService,
            IWorkContext workContext,
            IStoreContext storeContext,
            ICategoryService categoryService,
            ILocalizationService localizationService,
            ISettingService settingService)
        {
            _productModelFactory = productModelFactory;
            _productService = productService;
            _workContext = workContext;
            _storeContext = storeContext;
            _categoryService = categoryService;
            _localizationService = localizationService;
            _settingService = settingService;
        }

        #endregion

        public IViewComponentResult Invoke(int pageNumber, bool isBestSeller, int categoryId = 0, int? productThumbPictureSize = 0, string keywords = "", string selectedImageTags = "", string brandNameIds = "", string packSizeIds = "", int minPrice = 0, int maxPrice = 0, int sortingOrder = 0 , string viewMode = "grid")
        {
            var merchantid = _workContext.CurrentMerchant.Id.ToString();
            var pageSize = _settingService.GetSettingByKey("catalogSettings.NumberOfProductsToDisplay", 9, storeId: _storeContext.CurrentStore.Id);

            var model = new ItemsListModel();
            model = _productModelFactory.PrepareSelectedItemFiltersModel(model, keywords, selectedImageTags);
            var storeId = _workContext.GetCurrentStoreId;
            if (isBestSeller || categoryId == -1)
            {
                model.CategoryName = _localizationService.GetResource("NB.Admin.BestSeller");
                isBestSeller = true;
            }
            else
            {
                model.CategoryName = _categoryService.GetCategoryById(categoryId)?.Name;
            }
            //load products
            var products = _productService.GetNBProductsDisplayedOnHomePageMerchant(
                merchantid: merchantid,
                isBestSeller: isBestSeller,
                keywords: keywords,
                selectedImageTags: selectedImageTags,
                categoryId: categoryId,
                pageSize: pageSize,
                pageIndex: pageNumber - 1,
                minPrice: minPrice,
                maxPrice: maxPrice,
                packSizeIds: packSizeIds,
                brandNameIds: brandNameIds,
                sortBy: sortingOrder);

            model.Items = _productModelFactory.PrepareProductOverviewModelsMerchant(products, storeId, true, true, productThumbPictureSize, prepareSpecificationAttributes: true).ToList();
            model.SortingList = _productModelFactory.PrepareSortingFilterList();
            model.PagingFilteringContext.LoadPagedList(products);
            model.PagingFilteringContext.ViewMode = viewMode;
            return View(model);
        }
    }
}
