﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Stores;
using Nop.Core.Domain.NB.Suppliers;
using Nop.Data;
using Nop.Services.Catalog;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Stores;

namespace Nop.Services.NB.Suppliers
{
    public partial class SupplierService : ISupplierService
    {

        #region Fields

        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<Supplier> _supplierRepository;
        private readonly IDbContext _dbContext;
        private readonly IDataProvider _dataProvider;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IStoreService _storeService;
        private readonly IStoreContext _storeContext;
        private readonly IAclService _aclService;
        private readonly ILocalizationService _localizationService;
        private readonly ICacheManager _cacheManager;
        private readonly IWorkContext _workContext;
        private readonly IRepository<Product> _productRepository;
        private readonly CatalogSettings _catalogSettings;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly IRepository<ProductSupplier> _productSupplierRepository;
        private readonly IRepository<AclRecord> _aclRepository;
        #endregion

        #region Ctor

        public SupplierService(IEventPublisher eventPublisher,
            IRepository<Supplier> supplierRepository,
            IRepository<AclRecord> aclRepository,
            IRepository<Product> productRepository,
            IDbContext dbContext,
            IStoreService storeService,
            ICacheManager cacheManager,
            ILocalizationService localizationService,
            IAclService aclService,

            IRepository<StoreMapping> storeMappingRepository,
            IRepository<ProductSupplier> productSupplierRepository,
            
            CatalogSettings catalogSettings,
            IWorkContext workContext,
            IStoreContext storeContext,
            IStoreMappingService storeMappingService,
            IDataProvider dataProvider)

        {
            _eventPublisher = eventPublisher;
            _supplierRepository = supplierRepository;
            _dbContext = dbContext;
            _dataProvider = dataProvider;
            _productRepository = productRepository;
            _aclService = aclService;
            _storeMappingRepository = storeMappingRepository;
            _aclRepository = aclRepository;
            _cacheManager = cacheManager;
            _localizationService = localizationService;
            _catalogSettings = catalogSettings;
            _storeContext = storeContext;
            _workContext = workContext;
            _storeMappingService = storeMappingService;
            _storeService = storeService;
            _productSupplierRepository = productSupplierRepository;

        }

        #endregion


        #region Methods






        /// <summary>
        /// Gets a supplier by Supplier identifier
        /// </summary>
        /// <param name="SupplierId">Supplier identifier</param>
        /// <returns>Supplier</returns>
        public virtual Supplier GetSupplierById(int id)
        {
            if (id == 0)
                return null;

            return _supplierRepository.GetById(id);
        }


        /// <summary>
        /// Gets suppliers
        /// </summary>
        /// <param name="SuppliersIds">Supplier identifiers</param>
        /// <returns>Suppliers</returns>
        public virtual IList<Supplier> GetSupplierByIds(int[] supplierid)
        {

            if (supplierid == null || supplierid.Length == 0)
                return new List<Supplier>();

            var query = from p in _supplierRepository.Table
                        where supplierid.Contains(p.Id) && !p.Deleted
                        select p;
            var suppliers = query.ToList();
            //sort by passed identifiers
            var sortedSuppliers = new List<Supplier>();
            foreach (var id in supplierid)
            {
                var supplier = suppliers.Find(x => x.Id == id);
                if (supplier != null)
                    sortedSuppliers.Add(supplier);
            }

            return sortedSuppliers;
        }



        /// <summary>
        /// Delete a Supplier
        /// </summary>
        /// <param name="supplier">Supplier</param>
        public virtual void DeleteSupplier(Supplier supplier)
        {
            if (supplier == null)
                throw new ArgumentNullException(nameof(supplier));

            supplier.Deleted = true;

            DeleteSupplier1(supplier);
           //UpdateSupplier(supplier);

            //event notification
            _eventPublisher.EntityDeleted(supplier);
        }


        /// <summary>
        /// Update Suppliers
        /// </summary>
        /// <param name="products">UpdateSuppliers</param>
        public virtual void UpdateSuppliers(IList<Supplier> suppliers)
        {
            if (suppliers == null)
                throw new ArgumentNullException(nameof(suppliers));

            //update
            _supplierRepository.Update(suppliers);

            ////cache
            //_cacheManager.RemoveByPrefix(NopCatalogDefaults.ProductsPrefixCacheKey);

            //event notification
            foreach (var supplier in suppliers)
            {
                _eventPublisher.EntityUpdated(supplier);
            }
        }



        /// <summary>
        /// Update Suppliers
        /// </summary>
        /// <param name="products">DeleteSuppliers</param>
        public virtual void DeleteSuppliers(ICollection<int> supplierIds)
        {
           if(supplierIds == null)
                throw new ArgumentNullException(nameof(supplierIds));

            foreach (var ids in supplierIds)
            {
                var supplier = GetSupplierById(ids);
                supplier.Deleted = true;
                DeleteSupplier1(supplier);
                //delete supplier
               // UpdateSupplier(supplier);
                //event notification
                _eventPublisher.EntityDeleted(supplier);
            }     
        }



        /// <summary>
        /// Gets all Supplier
        /// </summary>
        /// <param name="name">Supplier name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Suppliers</returns>
        public virtual IPagedList<Supplier> GetAllSupplier(string name = "", int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, int storeId = 0)
        {
            var query = _supplierRepository.Table;
            if (!string.IsNullOrWhiteSpace(name))
                query = query.Where(v => v.SupplierName.Contains(name));
            if (!showHidden)
                query = query.Where(v => v.Active);

            if (storeId > 0)
                query = query.Where(x => x.StoreId == storeId);

            query = query.Where(v => !v.Deleted);
            query = query.OrderBy(v => v.DisplayOrder).ThenBy(v => v.SupplierName);

            var suppliers = new PagedList<Supplier>(query, pageIndex, pageSize);
            return suppliers;
        }





        /// <summary>
        /// Gets all suppliers by store
        /// </summary>
        /// <param name="name">Vendor name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Suppliers</returns>
        public virtual IPagedList<Supplier> GetAllSupplierByStore(List<int> storeids, string name = "",
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {

            var query = _supplierRepository.Table;

            var suppliers = new PagedList<Supplier>(query, pageIndex, pageSize);
            return suppliers;
        }


        /// <summary>
        /// Inserts a supplier
        /// </summary>
        /// <param name="supplier">Supplier</param>
        public virtual void InsertSupplier(Supplier supplier)
        {
            if (supplier == null)
                throw new ArgumentNullException(nameof(supplier));
            _supplierRepository.Insert(supplier);

            //event notification
            _eventPublisher.EntityInserted(supplier);
        }

        /// <summary>
        /// Updates the supplier
        /// </summary>
        /// <param name="supplier">Supplier</param>
        public virtual void UpdateSupplier(Supplier supplier)
        {
            if (supplier == null)
                throw new ArgumentNullException(nameof(supplier));

            _supplierRepository.Update(supplier);

            //event notification
            _eventPublisher.EntityUpdated(supplier);
        }

        /// <summary>
        /// Deletes the supplier
        /// </summary>
        /// <param name="supplier">Supplier</param>

        public virtual void DeleteSupplier1(Supplier supplier)
        {
            if (supplier == null)
                throw new ArgumentNullException(nameof(supplier));

            _supplierRepository.Delete(supplier);

            //event notification
            _eventPublisher.EntityUpdated(supplier);
        }

        /// <summary>
        /// Check geofancing value
        /// </summary>
        /// <param name="geofancing"></param>
        /// <returns>bool</returns>
        public virtual bool CheckGeofancingFormat(string geofancing)
        {
            try
            {
                var pGeofancing = _dataProvider.GetParameter();
                pGeofancing.ParameterName = "Geofancing";
                pGeofancing.Value = geofancing;
                pGeofancing.DbType = DbType.String;

                _dbContext.ExecuteSqlCommand("EXEC Check_Geofancing @Geofancing", parameters: pGeofancing);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        /// <summary>
        /// Gets a supplier
        /// </summary>
        /// <param name="categoryId">Supplier identifier</param>
        /// <returns>Supplier</returns>
        public virtual Supplier GetSuppliersById(int supplierId)
        {
            if (supplierId == 0)
                return null;

            var key = string.Format(NopCatalogDefaults.CategoriesByIdCacheKey, supplierId);
            return _cacheManager.Get(key, () => _supplierRepository.GetById(supplierId));
        }






        /// <summary>
        /// Gets product supplier mapping collection
        /// </summary>
        /// <param name="supplierId">Category identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Product a supplier mapping collection</returns>
        public virtual IPagedList<ProductSupplier> GetProductSuppliersBySupplierId(int supplierId,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            if (supplierId == 0)
                return new PagedList<ProductSupplier>(new List<ProductSupplier>(), pageIndex, pageSize);

            var key = string.Format(NopCatalogDefaults.ProductCategoriesAllByCategoryIdCacheKey, showHidden, supplierId, pageIndex, pageSize, _workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id);
            return _cacheManager.Get(key, () =>
            {
                var query = from pc in _productSupplierRepository.Table
                            join p in _productRepository.Table on pc.ProductId equals p.Id
                            where pc.SupplierId == supplierId &&
                                  !p.Deleted &&
                                  (showHidden || p.Published)
                            orderby pc.DisplayOrder, pc.Id
                            select pc;

                if (!showHidden && (!_catalogSettings.IgnoreAcl || !_catalogSettings.IgnoreStoreLimitations))
                {
                    if (!_catalogSettings.IgnoreAcl)
                    {
                        //ACL (access control list)
                        var allowedCustomerRolesIds = _workContext.CurrentCustomer.GetCustomerRoleIds();
                        query = from pc in query
                                join c in _supplierRepository.Table on pc.SupplierId equals c.Id
                                join acl in _aclRepository.Table
                                on new { c1 = c.Id, c2 = nameof(Supplier) } equals new { c1 = acl.EntityId, c2 = acl.EntityName } into c_acl
                                from acl in c_acl.DefaultIfEmpty()
                                where !c.SubjectToAcl || allowedCustomerRolesIds.Contains(acl.CustomerRoleId)
                                select pc;
                    }

                    if (!_catalogSettings.IgnoreStoreLimitations)
                    {
                        //Store mapping
                        var currentStoreId = _storeContext.CurrentStore.Id;
                        query = from pc in query
                                join c in _supplierRepository.Table on pc.SupplierId equals c.Id
                                join sm in _storeMappingRepository.Table
                                on new { c1 = c.Id, c2 = nameof(Supplier) } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into c_sm
                                from sm in c_sm.DefaultIfEmpty()
                                where !c.AvailableStore || currentStoreId == sm.StoreId
                                select pc;
                    }

                    query = query.Distinct().OrderBy(pc => pc.DisplayOrder).ThenBy(pc => pc.Id);
                }

                var productSuppliers = new PagedList<ProductSupplier>(query, pageIndex, pageSize);
                return productSuppliers;
            });
        }


        /// <summary>
        /// Gets a product supplier mapping 
        /// </summary>
        /// <param name="productSupplierId">Product supplier mapping identifier</param>
        /// <returns>Product supplier mapping</returns>
        public virtual ProductSupplier GetProductSupplierById(int productSupplierId)
        {
            if (productSupplierId == 0)
                return null;

            return _productSupplierRepository.GetById(productSupplierId);
        }




        /// <summary>
        /// Updates the product supplier mapping 
        /// </summary>
        /// <param name="productSupplier">>Product supplier mapping</param>
        public virtual void UpdateProductSupplier(ProductSupplier productSupplier)
        {
            if (productSupplier == null)
                throw new ArgumentNullException(nameof(productSupplier));

            _productSupplierRepository.Update(productSupplier);

            //cache
            _cacheManager.RemoveByPrefix(NopCatalogDefaults.ProductCategoriesPrefixCacheKey);

            //event notification
            _eventPublisher.EntityUpdated(productSupplier);
        }



        /// <summary>
        /// Deletes a product supplier mapping
        /// </summary>
        /// <param name="productSupplier">Product supplier</param>
        public virtual void DeleteProductSupplier(ProductSupplier productSupplier)
        {
            if (productSupplier == null)
                throw new ArgumentNullException(nameof(productSupplier));

            _productSupplierRepository.Delete(productSupplier);

            //cache
            _cacheManager.RemoveByPrefix(NopCatalogDefaults.ProductCategoriesPrefixCacheKey);

            //event notification
            _eventPublisher.EntityDeleted(productSupplier);
        }


        /// <summary>
        /// GetAllSuppliersVendorwise
        /// </summary>
        /// <param name="productSupplier">Product supplier</param>

        public virtual IPagedList<Supplier> GetAllSuppliersVendorwise(string supplierName = "", int storeId = 0, int vendorId = 0,
           int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
           

            //stored procedures aren't supported. Use LINQ
            var query = _supplierRepository.Table;
            
            if ((storeId > 0 && !_catalogSettings.IgnoreStoreLimitations) || (!showHidden && !_catalogSettings.IgnoreAcl))
            {
                if (!showHidden && !_catalogSettings.IgnoreAcl)
                {
                    //ACL (access control list)
                    var allowedCustomerRolesIds = _workContext.CurrentCustomer.GetCustomerRoleIds();
                    query = from c in query
                            join acl in _aclRepository.Table
                                on new { c1 = c.Id, c2 = "Supplier" } equals new { c1 = acl.EntityId, c2 = acl.EntityName } into c_acl
                            from acl in c_acl.DefaultIfEmpty()
                            where !c.SubjectToAcl || allowedCustomerRolesIds.Contains(acl.CustomerRoleId)
                            select c;
                }
                if (storeId > 0 && !_catalogSettings.IgnoreStoreLimitations)
                {
                    //Store mapping
                    query = from c in query
                            join sm in _storeMappingRepository.Table
                                on new { c1 = c.Id, c2 = "Supplier" } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into c_sm
                            from sm in c_sm.DefaultIfEmpty()
                            where !c.AvailableStore || storeId == sm.StoreId
                            select c;
                }



                //only distinct categories (group by ID)
                query = from c in query
                        group c by c.Id
                    into cGroup
                        orderby cGroup.Key
                        select cGroup.FirstOrDefault();
                query = query.OrderBy(c => c.ParentSupplierId).ThenBy(c => c.DisplayOrder).ThenBy(c => c.Id);
            }

            if (storeId > 0 && vendorId == 0)
            {
                //Store mapping
                query = from c in query
                        join sm in _storeMappingRepository.Table
                            on new { c1 = c.Id, c2 = "Supplier" } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into c_sm
                        from sm in c_sm.DefaultIfEmpty()
                        where storeId == sm.StoreId
                        select c;
            }

            if (storeId > 0 && vendorId > 0)
            {
                //Store mapping
                query = from c in query
                        join sm in _storeMappingRepository.Table
                            on new { c1 = c.Id, c2 = "Supplier" } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into c_sm
                        from sm in c_sm.DefaultIfEmpty()
                        where vendorId == sm.VendorId
                        select c;
            }

            if (storeId == 0 && vendorId > 0)
            {
                //Store mapping
                query = from c in query
                        join sm in _storeMappingRepository.Table
                            on new { c1 = c.Id, c2 = "Supplier" } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into c_sm
                        from sm in c_sm.DefaultIfEmpty()
                        where vendorId == sm.VendorId
                        select c;
            }

            var unsortedSuppliers = query.ToList();

            //sort categories
            var sortedSuppliers = unsortedSuppliers.OrderBy(x => x.DisplayOrder).ToList();

            //paging
            return new PagedList<Supplier>(sortedSuppliers, pageIndex, pageSize);
        }




        /// <summary>
        /// Get formatted category breadcrumb 
        /// Note: ACL and store mapping is ignored
        /// </summary>
        /// <param name="supplier">Category</param>
        /// <param name="allSuppliers">All categories</param>
        /// <param name="separator">Separator</param>
        /// <param name="languageId">Language identifier for localization</param>
        /// <returns>Formatted breadcrumb</returns>
        public virtual string GetFormattedBreadCrumb(Supplier supplier, IList<Supplier> allSuppliers = null,
            string separator = ">>", int languageId = 0)
        {
            var result = string.Empty;

            var breadcrumb = GetSupplierBreadCrumb(supplier, allSuppliers, true);
            for (var i = 0; i <= breadcrumb.Count - 1; i++)
            {
                var supplierName = _localizationService.GetLocalized(breadcrumb[i], x => x.SupplierName, languageId);
                result = string.IsNullOrEmpty(result) ? supplierName : $"{result} {separator} {supplierName}";
            }

            return result;
        }

        /// <summary>
        /// Get category breadcrumb 
        /// </summary>
        /// <param name="supplier">Category</param>
        /// <param name="allSuppliers">All categories</param>
        /// <param name="showHidden">A value indicating whether to load hidden records</param>
        /// <returns>Supplier breadcrumb </returns>
        public virtual IList<Supplier> GetSupplierBreadCrumb(Supplier supplier, IList<Supplier> allSuppliers = null, bool showHidden = false)
        {
            if (supplier == null)
                throw new ArgumentNullException(nameof(supplier));

            var result = new List<Supplier>();

            //used to prevent circular references
            var alreadyProcessedSuppliersIds = new List<int>();

            while (supplier != null && //not null
                !supplier.Deleted && //not deleted
                (showHidden || supplier.Published) && //published
                //(showHidden || _aclService.Authorize(supplier)) && //ACL
                //(showHidden || _storeMappingService.Authorize(supplier)) && //Store mapping
                !alreadyProcessedSuppliersIds.Contains(supplier.Id)) //prevent circular references
            {
                result.Add(supplier);

                alreadyProcessedSuppliersIds.Add(supplier.Id);

                supplier = allSuppliers != null ? allSuppliers.FirstOrDefault(c => c.Id == supplier.ParentSupplierId)
                    : GetSupplierById(supplier.ParentSupplierId);
            }

            result.Reverse();
            return result;
        }


        /// <summary>
        /// Returns a ProductSupplier that has the specified values
        /// </summary>
        /// <param name="source">Source</param>
        /// <param name="productId">Product identifier</param>
        /// <param name="supplierId">Category identifier</param>
        /// <returns>A ProductSupplier that has the specified values; otherwise null</returns>
        public virtual ProductSupplier FindProductSupplier(IList<ProductSupplier> source, int productId, int supplierId)
        {
            foreach (var productSupplier in source)
                if (productSupplier.ProductId == productId && productSupplier.SupplierId == supplierId )
                    return productSupplier;

            return null;
        }


        /// <summary>
        /// Inserts a product supplier mapping
        /// </summary>
        /// <param name="productSupplier">>Product category mapping</param>
        public virtual void InsertProductSupplier(ProductSupplier productSupplier)
        {
            if (productSupplier == null)
                throw new ArgumentNullException(nameof(productSupplier));

            _productSupplierRepository.Insert(productSupplier);

            //cache
            _cacheManager.RemoveByPrefix(NopCatalogDefaults.ProductCategoriesPrefixCacheKey);

            //event notification
            _eventPublisher.EntityInserted(productSupplier);
        }

        public virtual IList<Supplier> GetSupplierByStoreId(int storeId, int languageId = 0, bool showHidden = false)
        {
            
                var query = from sp in _supplierRepository.Table
                            orderby sp.DisplayOrder, sp.SupplierName
                            where sp.StoreId == storeId &&
                            (showHidden || sp.Published)
                            select sp;
                var supplier = query.ToList();

                if (languageId > 0)
                {
                    //we should sort suppliers by stores
                    supplier = supplier
                        .OrderBy(c => c.DisplayOrder)
                        .ThenBy(c => _localizationService.GetLocalized(c, x => x.SupplierName))
                        .ToList();
                }

                return supplier;
            
        }

    }
}
#endregion