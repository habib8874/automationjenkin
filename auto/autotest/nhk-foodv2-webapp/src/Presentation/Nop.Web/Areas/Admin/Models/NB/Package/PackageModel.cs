﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.Package
{
    /// <summary>
    /// Represents a Package model
    /// </summary>
    public partial class PackageModel : BaseNopEntityModel
    {
        #region Ctor

        public PackageModel()
        {
            AvailableStores = new List<SelectListItem>();
            PackagePlanModel = new PackagePlanModel();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("NB.Admin.Package.Fields.Name")]
        public string Name { get; set; }

        [UIHint("Picture")]
        [NopResourceDisplayName("NB.Admin.Package.Fields.Picture")]
        public int PictureId { get; set; }
        public int? ProductId { get; set; }

        [NopResourceDisplayName("NB.Admin.Package.Fields.ShortDescription")]
        public string ShortDescription { get; set; }

        [NopResourceDisplayName("NB.Admin.Package.Fields.FullDescription")]
        public string FullDescription { get; set; }

        [NopResourceDisplayName("NB.Admin.Package.Fields.UnitLabel")]
        public string UnitLabel { get; set; }

        [NopResourceDisplayName("NB.Admin.Package.Fields.Store")]
        public int StoreId { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }

        public PackagePlanModel PackagePlanModel { get; set; }

        #endregion
    }

}