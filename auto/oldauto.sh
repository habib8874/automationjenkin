#!/bin/bash
PROJECT_TYPE=$1

#DEFINE PROJECT NAME FROM SECOND ARGUMENT
PROJECT_NAME=$2
mkdir $PROJECT_NAME

#DEFINE NEW PROJECT DATABASE NAME
newdb=$2dev

if [[ "$PROJECT_TYPE" == "ecommerce" ]]; then
    db="newV3nhkecommercedev"
    web_repo="nhk-ecommerce-web"
    customer_repo="nhk-customer-v3"
    agent_repo="nhk-agent-v3"
    new_web_repo="nhk-ecommerce-${PROJECT_NAME}-web"
    new_customer_repo="nhk-ecommerce-${PROJECT_NAME}-customerAPI"
    new_agent_repo="nhk-ecommerce-${PROJECT_NAME}-agentAPI"

elif [[ "$PROJECT_TYPE" == "food" ]]; then
    db=nhkfooddbr
    web_repo="nhk-foodsolution"
    customer_repo="nhk-food-${PROJECT_NAME}-customerAPI"
    agent_repo="nhk-food-${PROJECT_NAME}-agentAPI"
    new_web_repo="nhk-food-${PROJECT_NAME}-web"
    new_customer_repo="nhk-food-${PROJECT_NAME}-customerAPI"
    new_agent_repo="nhk-food-${PROJECT_NAME}-agentAPI"

elif [[ "$PROJECT_TYPE" == "grocery" ]]; then
    db=nhkgrocerydb
    web_repo="nhk-grocery-${PROJECT_NAME}-web"
    customer_repo="nhk-grocery-${PROJECT_NAME}-customerAPI"
    agent_repo="nhk-grocery-${PROJECT_NAME}-agentAPI"
    new_web_repo="nhk-grocery-${PROJECT_NAME}-web"
    new_customer_repo="nhk-grocery-${PROJECT_NAME}-customerAPI"
    new_agent_repo="nhk-grocery-${PROJECT_NAME}-agentAPI"


elif [[ "$PROJECT_TYPE" == "rental" ]]; then
    db=nhkrentaldb
    web_repo="nhk-rental-${PROJECT_NAME}-web"
    customer_repo="nhk-rental-${PROJECT_NAME}-customerAPI"
    agent_repo="nhk-rental-${PROJECT_NAME}-agentAPI"
    new_web_repo="nhk-rental-${PROJECT_NAME}-web"
    new_customer_repo="nhk-rental-${PROJECT_NAME}-customerAPI"
    new_agent_repo="nhk-rental-${PROJECT_NAME}-agentAPI"

elif [[ "$PROJECT_TYPE" == "handyman" ]]; then
    db=nhkhandymandb
    web_repo="nhk-handyman-${PROJECT_NAME}-web"
    customer_repo="nhk-handyman-${PROJECT_NAME}-customerAPI"
    agent_repo="nhk-handyman-${PROJECT_NAME}-agentAPI"
    new_web_repo="nhk-handyman-${PROJECT_NAME}-web"
    new_customer_repo="nhk-handyman-${PROJECT_NAME}-customerAPI"
    new_agent_repo="nhk-handyman-${PROJECT_NAME}-agentAPI"

elif [[ "$PROJECT_TYPE" == "fleaarketplace" ]]; then
    db=nhkfleaarketplacedb
    web_repo="nhk-fleaarketplace-${PROJECT_NAME}-web"
    customer_repo="nhk-fleaarketplace-${PROJECT_NAME}-customerAPI"
    agent_repo="nhk-fleaarketplace-${PROJECT_NAME}-agentAPI"
    new_web_repo="nhk-fleaarketplace-${PROJECT_NAME}-web"
    new_customer_repo="nhk-fleaarketplace-${PROJECT_NAME}-customerAPI"
    new_agent_repo="nhk-fleaarketplace-${PROJECT_NAME}-agentAPI"

elif [[ "$PROJECT_TYPE" == "foodkiosk" ]]; then
    db=nhkfoodkioskdb
    web_repo="nhk-foodkiosk-${PROJECT_NAME}-web"
    customer_repo="nhk-foodkiosk-${PROJECT_NAME}-customerAPI"
    agent_repo="nhk-foodkiosk-${PROJECT_NAME}-agentAPI"
    new_web_repo="nhk-foodkiosk-${PROJECT_NAME}-web"
    new_customer_repo="nhk-foodkiosk-${PROJECT_NAME}-customerAPI"
    new_agent_repo="nhk-foodkiosk-${PROJECT_NAME}-agentAPI"
else
    echo "invalid project type"
fi

LOCALPASS=z+EV/JW'!'4ms@8'$'L3

##################################################################
#DB SETUP
if [[ "$PROJECT_TYPE" == "ecommerce" ]] || [[ "$PROJECT_TYPE" == "food" ]] || [[ "$PROJECT_TYPE" == "grocery" ]] || [[ "$PROJECT_TYPE" == "rental" ]] || [[ "$PROJECT_TYPE" == "handyman" ]] || [[ "$PROJECT_TYPE" == "fleaarketplace" ]] || [[ "$PROJECT_TYPE" == "foodkiosk" ]]

then

#docker exec -it 3ea5cdf204a6 /bin/mkdir /var/opt/mssql/data/backups/

#BACKUP DATABASE
/opt/mssql-tools/bin/sqlcmd -S 122.186.73.186,1433 -U SA -P 'eiuN8$rv^*(&*HH' -Q "BACKUP DATABASE [$db] TO DISK = N'/var/opt/mssql/data/backups/$db.bak' WITH NOFORMAT, NOINIT, NAME = '$db', SKIP, NOREWIND, NOUNLOAD, STATS = 10"


/opt/mssql-tools/bin/sqlcmd -S 122.186.73.186,1433 -U SA -P 'eiuN8$rv^*(&*HH' -Q " RESTORE FILELISTONLY FROM DISK='/var/opt/mssql/data/backups/$db.bak'" > datalogfile
grep -ow '\bDB_\w*' datalogfile > datalogfile1
rm -rf datalogfile
filecount=0
for word in $(cat datalogfile1)
do
        declare -a filecount_array
        filecount_array[filecount]=`echo $word`
        filecount=+1
done

#RESTORE DATABASE
/opt/mssql-tools/bin/sqlcmd -S 122.186.73.186,1433 -U SA -P 'eiuN8$rv^*(&*HH'  -Q "
RESTORE DATABASE [$newdb] FROM DISK='/var/opt/mssql/data/backups/$db.bak'
WITH
   MOVE '${filecount_array[0]}' TO '/var/opt/mssql/data/${newdb}.mdf',
   MOVE '${filecount_array[1]}' TO '/var/opt/mssql/data/${newdb}_log.ldf' "



#set username and password
DBUSER=$2userdev
DBPASS=`echo $1 | base64`
#echo $DBPASS

/opt/mssql-tools/bin/sqlcmd -S 122.186.73.186,1433 -U SA -P 'eiuN8$rv^*(&*HH'  -Q "
use ${newdb}
CREATE LOGIN [${DBUSER}] WITH PASSWORD = '${DBPASS}', CHECK_POLICY = OFF;
CREATE USER [${DBUSER}] for login [${DBUSER}];
ALTER ROLE db_owner ADD MEMBER [${DBUSER}];
"


#docker exec -it 3ea5cdf204a6 /bin/rm -rf  /var/opt/mssql/data/backups

#UPDATE STORE URL AND HOSTS VALUE
/opt/mssql-tools/bin/sqlcmd -S 122.186.73.186,1433 -U SA -P 'eiuN8$rv^*(&*HH' -Q "
USE [$newdb]
GO
update store set Url='http://${2}dev.nestorhawk.com';
update store set Hosts='http://${2}dev.nestorhawk.com';
"
fi
#############################################################################################################################################################################################
#REPO CREATE
#REPO SETUP
sum=0
declare -a my_array
declare -a domain_array

if [[ "$3" == "web" ]]; then
     my_array+="\"${new_web_repo}\""
     domain_array+="\"${PROJECT_NAME}dev\""
     sum=$((sum + 1))
fi
if [[ "$4" == "customer" ]]; then
     my_array+=", \"${new_customer_repo}\""
     domain_array+=", \"${PROJECT_NAME}customerapi\""
     sum=$((sum + 1))
fi
if [[ "$5" == "agent" ]]; then
     my_array+=", \"${new_agent_repo}\""
     domain_array+=", \"${PROJECT_NAME}agentapi\""
     sum=$((sum + 1 ))
fi

mkdir $PROJECT_NAME/cloudflare
mkdir $PROJECT_NAME/repocreate

sed -e "s/value/$my_array/g" -e "s/num/$sum/g" test.tf > $PROJECT_NAME/repocreate/${PROJECT_NAME}.tf
sed -e "s/sub_domain/$domain_array/g" -e "s/num/$sum/g" cloudflare.tf > $PROJECT_NAME/cloudflare/${PROJECT_NAME}.tf

cp *.service $PROJECT_NAME
cp -r conf $PROJECT_NAME

cd /home/ubuntu/auto/$PROJECT_NAME/repocreate/

/var/lib/jenkins/bin/terraform init
/var/lib/jenkins/bin/terraform apply --auto-approve


cd /home/ubuntu/auto/$PROJECT_NAME/cloudflare

/var/lib/jenkins/bin/terraform init
/var/lib/jenkins/bin/terraform apply --auto-approve


cd /home/ubuntu/auto/$PROJECT_NAME/

if [[ "$3" == "web" ]]; then

   #git clone and create repo:
   git clone https://$USERNAME:$USERPASS@gitlab.com/nhkteam/${web_repo}.git
   git clone https://$USERNAME:$USERPASS@gitlab.com/ajit3/${new_web_repo}.git

   rm -rf ${web_repo}/.git

   WEB_PORT=9000
   VALUE="false"

   while [[ "$VALUE" == "false" ]]
   do
     if [[ $(grep -wir $WEB_PORT /etc/httpd/conf.d) ]]; then
         WEB_PORT=$((WEB_PORT + 1))
         VALUE="false"
     else
         echo $WEB_PORT
         VALUE="true"
     fi
   done

   echo "print value `pwd`" 
   
   sed -e "s/WEB_PORT/$WEB_PORT/g" -e "s/PROJECT_NAME/$PROJECT_NAME/g" conf/sampledev.nestorhawk.com.conf > ${PROJECT_NAME}dev.nestorhawk.com.conf
   #new_web_repo_small=`echo $new_web_repo | awk '{print tolower($0)}'`
   #web_repo_small=`echo $web_repo | awk '{print tolower($0)}'`

   mv ${web_repo}/* ${new_web_repo}
   cd ${new_web_repo}

   #update connection string
   #sed -i -e "s/DBUSER/$DBUSER/g" -e "s/DBPASS/$DBPASS/g" dataSettings.json

   #send conf to local server
   sshpass -p $PASS rsync --progress -avz ${PROJECT_NAME}dev.nestorhawk.com.conf root@122.186.73.186:/etc/httpd/conf.d/
   #rsync --progress -avz -e "ssh -i  key.pem" ${PROJECT_NAME}dev.nestorhawk.com.conf root@165.232.182.1:/etc/httpd/conf.d
   sed -e "s/DBUSER/$DBUSER/g" -e "s/DBPASS/$DBPASS/g" -e "s/DBNAME/$newdb/g" /home/ubuntu/auto/dataSettings.json > dataSettings.json
 
   git add --all
   git commit -am "initial commit"
   git branch -m deployment-development
   git push origin -u deployment-development

   GIT_CLONE_WEB_URL="https://gitlab.com/ajit3/${new_web_repo}.git"
   echo $GIT_CLONE_WEB_URL

   
   cp /home/ubuntu/auto/webconf.xml  /home/ubuntu/auto/${PROJECT_NAME}/${PROJECT_NAME}webconf.xml
   
   cd /home/ubuntu/auto/${PROJECT_NAME}/

   sed -i "s/WEB_PORT/$WEB_PORT/g" ${PROJECT_NAME}webconf.xml

   sed -i "s|GIT_CLONE_WEB_URL|$GIT_CLONE_WEB_URL|g" ${PROJECT_NAME}webconf.xml
   echo ${PROJECT_NAME}webconf.xml
   curl -s -XPOST 'http://jenkinsdev.nestorhawk.com/createItem?name='${PROJECT_NAME}'-webdev' -u ajit:1143e5e90f13206ea18be9558081795273 --data-binary @${PROJECT_NAME}webconf.xml -H "Content-Type:text/xml"
   curl -X POST http://ajit:1143e5e90f13206ea18be9558081795273@jenkinsdev.nestorhawk.com/job/${PROJECT_NAME}-webdev/build/

fi

cd /home/ubuntu/auto/$PROJECT_NAME/

if [[ "$4" == "customer" ]]; then

   #git clone and create repo:
   git clone https://$USERNAME:$USERPASS@gitlab.com/nhkteam/${customer_repo}.git
   git clone https://$USERNAME:$USERPASS@gitlab.com/ajit3/${new_customer_repo}.git

   rm -rf ${customer_repo}/.git
   new_customer_repo_small=`echo $new_customer_repo | awk '{print tolower($0)}'`
   customer_repo_small=`echo $customer_repo | awk '{print tolower($0)}'`
   pwd

   #port assigning
   CUSTOMER_PORT=5000
   VALUE="false"

   while [[ "$VALUE" == "false" ]]
   do
     if [[ $(grep -wir $CUSTOMER_PORT /etc/httpd/conf.d) ]]; then
         CUSTOMER_PORT=$((CUSTOMER_PORT + 1))
         VALUE="false"
     else
         echo $CUSTOMER_PORT
         VALUE="true"
     fi
   done

   sed -e "s/PROJECT_NAME/$PROJECT_NAME/g" -e "s/CUSTOMER_PORT/$CUSTOMER_PORT/g" conf/samplecustomerapidev.nestorhawk.com.conf > ${PROJECT_NAME}customerapidev.nestorhawk.com.conf
   sed "s/PROJECT_NAME/$PROJECT_NAME/g" kestrel-sampledevcustomerapi.service > kestrel-${PROJECT_NAME}customerdevapi.service



   mv ${customer_repo}/* ${new_customer_repo}
   cd ${new_customer_repo}
   #update connection string
   #sed -i -e "s/DBUSER/$DBUSER/g" -e "s/DBPASS/$DBPASS/g" NHKCustomerApplication/appsettings.json

   #send conf to local server
   sshpass -p $PASS rsync --progress -avz ${PROJECT_NAME}customerapidev.nestorhawk.com.conf root@122.186.73.186:/etc/httpd/conf.d/
   sshpass -p $PASS rsync --progress -avz kestrel-${PROJECT_NAME}customerdevapi.service root@122.186.73.186:/etc/systemd/system/

   #rsync --progress -avz -e "ssh -i  key.pem" ${PROJECT_NAME}customerapidev.nestorhawk.com.conf root@165.232.182.1:/etc/httpd/conf.d
   
   sed -i "s/localhost:[0-9][0-9][0-9][0-9]/localhost:$CUSTOMER_PORT/g" NHKCustomerApplication/Program.cs
   #left with updating connection striing
   sed -e "s/DBUSER/$DBUSER/g" -e "s/DBPASS/$DBPASS/g" -e "s/DBNAME/$newdb/g" /home/ubuntu/auto/appsettings.json > NHKCustomerApplication/appsettings.json
   git add --all
   git commit -am "initial commit"
   git branch -m deployment-development
   git push origin -u deployment-development
   echo $CUSTOMER_PORT >> /etc/httpd/conf.d/port
   
   GIT_CLONE_CUSTOMER_URL="https://gitlab.com/ajit3/${new_customer_repo}.git"
   echo $GIT_CLONE_CUSTOMER_URL


   cp /home/ubuntu/auto/customerapi.xml  /home/ubuntu/auto/${PROJECT_NAME}/${PROJECT_NAME}customerapi.xml

   cd /home/ubuntu/auto/${PROJECT_NAME}/

   sed -i "s/PROJECT_NAME/$PROJECT_NAME/g" ${PROJECT_NAME}customerapi.xml

   sed -i "s|GIT_CLONE_CUSTOMER_URL|$GIT_CLONE_CUSTOMER_URL|g" ${PROJECT_NAME}customerapi.xml
   echo ${PROJECT_NAME}customerapi.xml

   curl -s -XPOST 'http://jenkinsdev.nestorhawk.com/createItem?name='${PROJECT_NAME}'-customerdev' -u ajit:1143e5e90f13206ea18be9558081795273 --data-binary @${PROJECT_NAME}customerapi.xml -H "Content-Type:text/xml"
   curl -X POST http://ajit:1143e5e90f13206ea18be9558081795273@jenkinsdev.nestorhawk.com/job/${PROJECT_NAME}-customerdev/build/
fi

cd /home/ubuntu/auto/${PROJECT_NAME}

if [[ "$5" == "agent" ]]; then

   #git clone and create repo:
   git clone https://$USERNAME:$USERPASS@gitlab.com/nhkteam/${agent_repo}.git
   git clone https://$USERNAME:$USERPASS@gitlab.com/ajit3/${new_agent_repo}.git

   rm -rf ${agent_repo}/.git
   new_agent_repo_small=`echo $new_agent_repo | awk '{print tolower($0)}'`
   agent_repo_small=`echo $agent_repo | awk '{print tolower($0)}'`
   pwd

   #port assigning
   AGENT_PORT=5000
   VALUE="false"

   while [[ "$VALUE" == "false" ]]
   do
     if [[ $(grep -wir $AGENT_PORT /etc/httpd/conf.d) ]]; then
         AGENT_PORT=$((AGENT_PORT + 1))
         VALUE="false"
     else
         echo $AGENT_PORT
         VALUE="true"
     fi
   done

   sed -e "s/PROJECT_NAME/$PROJECT_NAME/g" -e "s/AGENT_PORT/$AGENT_PORT/g" conf/sampleagentapidev.nestorhawk.com.conf > ${PROJECT_NAME}agentapidev.nestorhawk.com.conf
   sed "s/PROJECT_NAME/$PROJECT_NAME/g" kestrel-sampledevagentapi.service > kestrel-${PROJECT_NAME}agentdevapi.service



   mv ${agent_repo}/* ${new_agent_repo}
   cd ${new_agent_repo}


   #update connection string
   #sed -i -e "s/DBUSER/$DBUSER/g" -e "s/DBPASS/$DBPASS/g" NHKAgentApplication/appsettings.json
   
   
   #rsync --progress -avz -e "ssh -i  key.pem" ${PROJECT_NAME}agentapidev.nestorhawk.com.conf root@165.232.182.1:/etc/httpd/conf.d
   sshpass -p $PASS rsync --progress -avz ${PROJECT_NAME}agentapidev.nestorhawk.com.conf root@122.186.73.186:/etc/httpd/conf.d/
   sshpass -p $PASS rsync --progress -avz kestrel-${PROJECT_NAME}agentdevapi.service root@122.186.73.186:/etc/systemd/system/

   sed -i "s/localhost:[0-9][0-9][0-9][0-9]/localhost:$AGENT_PORT/g" NHKAgentApplication/Program.cs

   #left with updating connection striing
   sed -e "s/DBUSER/$DBUSER/g" -e "s/DBPASS/$DBPASS/g" -e "s/DBNAME/$newdb/g" /home/ubuntu/auto/appsettings.json > NHKAgentApplication/appsettings.json

   git add --all
   git commit -am "initial commit"
   git branch -m deployment-development
   git push origin -u deployment-development

      echo $AGENT_PORT >> /etc/httpd/conf.d/port

   GIT_CLONE_AGENT_URL="https://gitlab.com/ajit3/${new_agent_repo}.git"
   echo $GIT_CLONE_AGENT_URL


   cp /home/ubuntu/auto/agentapi.xml  /home/ubuntu/auto/${PROJECT_NAME}/${PROJECT_NAME}agentapi.xml

   cd /home/ubuntu/auto/${PROJECT_NAME}/

   sed -i "s/PROJECT_NAME/$PROJECT_NAME/g" ${PROJECT_NAME}agentapi.xml

   sed -i "s|GIT_CLONE_CUSTOMER_URL|$GIT_CLONE_AGENT_URL|g" ${PROJECT_NAME}agentapi.xml
   echo ${PROJECT_NAME}agentapi.xml

   curl -s -XPOST 'http://jenkinsdev.nestorhawk.com/createItem?name='${PROJECT_NAME}'-agentdev' -u ajit:1143e5e90f13206ea18be9558081795273 --data-binary @${PROJECT_NAME}agentapi.xml -H "Content-Type:text/xml"
   curl -X POST http://ajit:1143e5e90f13206ea18be9558081795273@jenkinsdev.nestorhawk.com/job/${PROJECT_NAME}-agentdev/build/


fi

