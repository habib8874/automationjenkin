﻿namespace NHKCustomerApplication.ViewModel
{
    public class BaseAPIListResponseModel: BaseAPIResponseModel
    {
            public int TotalRecords { get; set; }
    }
}
