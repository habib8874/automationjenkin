using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class ProductImageTagMap : NopEntityTypeConfiguration<ProductImageTag>
    {
        public override void Configure(EntityTypeBuilder<ProductImageTag> builder)
        {
            builder.ToTable("ProductTag_Image_Mapping");
            builder.HasKey(pt => pt.Id);

            base.Configure(builder);
        }
    }
}