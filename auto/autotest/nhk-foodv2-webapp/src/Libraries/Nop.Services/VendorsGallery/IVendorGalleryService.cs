using Nop.Core;
using Nop.Core.Domain.VendorsGallery;
using Nop.Core.Domain.Vendors;
using System.Collections.Generic;

namespace Nop.Services.VendorsGallery
{
    /// <summary>
    /// Vendor service interface
    /// </summary>
    public partial interface IVendorGalleryService
    {
        /// <summary>
        /// Gets a vendor by vendor identifier
        /// </summary>
        /// <param name="vendorId">Vendor identifier</param>
        /// <returns>Vendor</returns>
        VendorGallery GetVendorGalleryById(int vendorId);

        /// <summary>
        /// Delete a vendor
        /// </summary>
        /// <param name="vendor">Vendor</param>
        void DeleteVendorGallery(VendorGallery vendor);

        /// <summary>
        /// Gets all vendors
        /// </summary>
        /// <param name="name">Vendor name</param>
        /// /// <param name="storeId">storeId</param>
        /// /// <param name="vendorId">vendorId</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>VendorGallery</returns>
        IPagedList<VendorGallery> GetAllVendorGallery(string name = "", int storeId=0,int vendorId=0,int GalleryTypeId=0,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);


        /// <summary>
        /// Gets all vendors by store
        /// </summary>
        /// <param name="name">Vendor name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Vendors</returns>
        IPagedList<Vendor> GetAllVendorsByStore(List<int> storeids,string name = "",
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        /// <summary>
        /// Inserts a vendor Gallery
        /// </summary>
        /// <param name="vendor">Vendor</param>
        void InsertVendorGallery(VendorGallery vendorGallery);

        /// <summary>
        /// Updates the vendor
        /// </summary>
        /// <param name="vendor">Vendor</param>
        void UpdateVendorGallery(VendorGallery vendorGallery);

        /// <summary>
        /// Gets a vendor note note
        /// </summary>
        /// <param name="vendorNoteId">The vendor note identifier</param>
        /// <returns>Vendor note</returns>
        VendorNote GetVendorNoteById(int vendorNoteId);

        /// <summary>
        /// Deletes a vendor note
        /// </summary>
        /// <param name="vendorNote">The vendor note</param>
        void DeleteVendorNote(VendorNote vendorNote);
    }
}