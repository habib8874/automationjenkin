﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Payments.Curlec.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Curlec.Fields.MerchantID")]
        public string MerchantID { get; set; }
        public bool MerchantID_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Curlec.Fields.EmployeeID")]
        public string EmployeeID { get; set; }
        public bool EmployeeID_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Curlec.Fields.MerchantURL")]
        public string MerchantURL { get; set; }
        public bool MerchantURL_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Curlec.Fields.MerchantCallbackURL")]
        public string MerchantCallbackURL { get; set; }
        public bool MerchantCallbackURL_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Curlec.Fields.CollectionCallbackURL")]
        public string CollectionCallbackURL { get; set; }
        public bool CollectionCallbackURL_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Curlec.Fields.EnrollmentCallbackURL")]
        public string EnrollmentCallbackURL { get; set; }
        public bool EnrollmentCallbackURL_OverrideForStore { get; set; }
    }
}