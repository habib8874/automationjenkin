﻿using Nop.Core.Domain.Stores;

namespace Nop.Core
{
    /// <summary>
    /// Store context
    /// </summary>
    public interface IStoreContext
    {
        /// <summary>
        /// Gets the current store
        /// </summary>
        Store CurrentStore { get; }

        /// <summary>
        /// Gets active store scope configuration
        /// </summary>
        int ActiveStoreScopeConfiguration { get; }

        //Custom code from v4.0
        Store CurrentStoreById(int storeId);

        /// <summary>
        /// Gets google map account key
        /// </summary>
        string GoogleMapAccountKey { get; }
    }
}
