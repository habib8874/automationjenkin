﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Catalog
{
    public partial class MagicMenuModel : BaseNopEntityModel
    {
        public MagicMenuModel()
        {
            Products = new List<MagicMenuProductModel>();
            SubCategories = new List<MagicMenuCategoryModel>();
        }
        public string Title { get; set; }
        public string CategoryBredCrumb { get; set; }
        public IList<MagicMenuCategoryModel> SubCategories { get; set; }
        public IList<MagicMenuProductModel> Products { get; set; }

		#region Nested Classes

        public partial class MagicMenuProductModel : BaseNopEntityModel
        {
            public string Name { get; set; }

            public string SeName { get; set; }

            public decimal Price { get; set; }
            public string PriceValue { get; set; }
        }

        public partial class MagicMenuCategoryModel : BaseNopEntityModel
        {
            public string Name { get; set; }

            public string Description { get; set; }

        }
        #endregion
    }
}