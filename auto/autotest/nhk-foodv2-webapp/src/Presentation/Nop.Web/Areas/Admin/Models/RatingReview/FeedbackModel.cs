﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.RatingReview
{
    public partial class FeedbackModel
    {
        public FeedbackModel()
        {
            FeedBackList = new List<Feedback>();
        }

        public List<Feedback> FeedBackList { get; set; }
    }

    public class Feedback
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
