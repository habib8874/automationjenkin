﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nestorbird.Plugin.Api.RequestModel.Categories
{
    public class CategoryListRequest : PagingRequest
    {
        public int VendorId { get; set; }
    }
}
