﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB.MerchantCategorys
{
    /// <summary>
    /// Represents a Merchant Category Merchant search model
    /// </summary>
    public partial class MerchantCategoryMerchantSearchModel : BaseSearchModel
    {
        #region Properties

        public int MerchantCategoryId { get; set; }

        public int StoreId { get; set; }

        #endregion
    }
}