﻿namespace Nop.Core.Domain.NB.VendorTokens
{
    public partial class NBVendorToken :  BaseEntity
    {
        public int CustomerId { get; set; }
        public string Token { get; set; }
        public bool Deleted { get; set; }
    }
}
