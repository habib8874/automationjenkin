﻿using System.Collections.Generic;
using Nop.Core.Domain.UserPushIdDetails;

namespace Nop.Services.UserPushIdDetails
{
    public partial interface IUserPushIdDetailService
    {
        /// <summary>
        /// Inserts a user push id details
        /// </summary>
        /// <param name="userPushIdDetail">UserPushIdDetail</param>
        void InsertUserPushIdDetails(UserPushIdDetail userPushIdDetail);

        /// <summary>
        /// Gets a store 
        /// </summary>
        /// <param name="userId">UserPushIdDetail identifier</param>
        /// <param name="loadCacheableCopy">A value indicating whether to load a copy that could be cached (workaround until Entity Framework supports 2-level caching)</param>
        /// <returns>Store</returns>
        UserPushIdDetail GetUserPushIdDetailById(int userId); //, bool loadCacheableCopy = true

        /// <summary>
        /// Gets all userpushiddetails
        /// </summary>
        /// <param name="loadCacheableCopy">A value indicating whether to load a copy that could be cached (workaround until Entity Framework supports 2-level caching)</param>
        /// <returns>UserPushIdDetails</returns>
        IList<UserPushIdDetail> GetAllUserPushIdDetails();
    }
}
