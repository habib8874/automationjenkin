﻿using System.Collections.Generic;

namespace Nop.Core.Domain.NB.Tip
{
    /// <summary>
    /// Represents a Tip
    /// </summary>
    public partial class Tip : BaseEntity
    {
        public decimal TipAmount { get; set; }
        public int DisplayOrder { get; set; }
        public bool Deleted { get; set; }
        public int StoreId { get; set; }
    }
}
