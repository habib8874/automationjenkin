﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.NB.SignalR;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Models.Orders;
using Nop.Services.Logging;
using static Nop.Web.Areas.Admin.Controllers.OrderController;

namespace Nop.Web.Areas.Admin.Controllers.NB
{
    public partial class NBDispatchManagementController : BaseAdminController
    {
        #region Fields

        private readonly string googleMapKey = "key=AIzaSyDDN0GJ503Fy6nQMysM9xT3OSC0CievcZI";
        private readonly IWorkContext _workContext;
        private readonly IPermissionService _permissionService;
        private readonly IHubContext<SignalRHub> _signalRHubContext;
        private readonly IOrderModelFactory _orderModelFactory;
        private readonly IOrderService _orderService;
        private readonly ICustomerService _customerService;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingservice;
        private readonly ICommonService _commonService;
        private readonly ILogger _loger;
        private readonly ISettingService _settingService;

        #endregion

        #region Ctor

        public NBDispatchManagementController(IWorkContext workContext,
            IPermissionService permissionService,
            IHubContext<SignalRHub> signalRHubContext,
            IOrderModelFactory orderModelFactory,
            IOrderService orderService,
            ICustomerService customerService,
            ILocalizationService localizationService,
            ISettingService settingservice,
            ICommonService commonService,
            ILogger loger,
            ISettingService settingService)
        {
            _workContext = workContext;
            _permissionService = permissionService;
            _signalRHubContext = signalRHubContext;
            _orderModelFactory = orderModelFactory;
            _orderService = orderService;
            _customerService = customerService;
            _localizationService = localizationService;
            _settingservice = settingservice;
            _commonService = commonService;
            _loger = loger;
            _settingService = settingService;
        }

        #endregion

        #region Utitlties

        #endregion

        public virtual IActionResult Index()
        {
            return RedirectToAction("Dispatch");
        }

        public virtual IActionResult Dispatch()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDispatchManagement))
                return AccessDeniedView();

            //prepare model
            var model = _orderModelFactory.PrepareDispatchModel();

            return View(model);
        }

        public virtual string LoadOrderDetails(int orderId)
        {
            var order = _orderService.GetOrderByCustomOrderNumber(orderId.ToString());
            //prepare model
            var model = _orderModelFactory.PrepareDispatchOrderModel(null, order);

            var html = RenderPartialViewToString("_Dispatch.OrderDetail", model);

            return html;
        }

        public virtual IActionResult AssignAgentToOrder(OrderModel model)
        {
            try
            {
                var order = _orderService.GetOrderByCustomOrderNumber(model.Id.ToString());
                if (model.Id == 0 || order == null)
                {
                    return Json(new
                    {
                        success = false,
                        message = "Order not found",
                    });
                }
                if (_customerService.CheckAgentAvailability(model.AssignedAgentId) != null)
                {
                    return Json(new
                    {
                        success = false,
                        message = _localizationService.GetResource("AssignAgent.Message.Error", _workContext.WorkingLanguage.Id),
                    });
                }

                var assignedAgent = _customerService.GetAssignedAgentByOrderId(order.Id);

                if (assignedAgent == null)
                {
                    assignedAgent = new AgentOrderStatus
                    {
                        OrderId = order.Id,
                        AgentId = model.AssignedAgentId,
                        OrderStatus = model.AgentStatusId,
                        SysDt = DateTime.UtcNow,
                        OrderDt = DateTime.UtcNow,
                        AgentStatusDt = DateTime.UtcNow,
                        CityId = model.CityId
                    };
                    _customerService.InsertAssignedAgent(assignedAgent);
                }
                else
                {
                    assignedAgent.AgentId = model.AssignedAgentId;
                    assignedAgent.OrderStatus = model.AgentStatusId;
                    assignedAgent.CityId = model.CityId;
                    _customerService.UpdateAssignedAgent(assignedAgent);

                }

                if (model.AgentStatusId == 2)
                    _orderModelFactory.SendNotificationToAgent(order.Id, model.AssignedAgentId);

                //signalR invoker
                _orderService.SendAgentAssignedSignalR(order);

                //sagrigate detail
                int storeIdReg = 0;
                int vendorIdReg = 0;
                var subAdminAccosiatedvendorIds = new List<int>();
                if (_workContext.IsStoreOwnerRole)
                {
                    storeIdReg = _workContext.GetCurrentStoreId;
                }
                else if (_workContext.IsVendorRole)
                {
                    storeIdReg = _workContext.GetCurrentStoreId;
                    vendorIdReg = _workContext.CurrentVendor.Id;
                }
                else if (_workContext.SubAdmin.IsSubAdminRole)
                {
                    storeIdReg = _workContext.GetCurrentStoreId;
                    subAdminAccosiatedvendorIds = _workContext.SubAdmin.AssociatedVenorIds?.ToList();
                }

                //prepare html sections
                var pendingOrderIds = _orderService.GetUnassignedOrderIds(storeId: storeIdReg, vendorId: vendorIdReg, vendorIds: subAdminAccosiatedvendorIds);
                var pendingOrderHtml = RenderPartialViewToString("_Dispatch.OrderPending", pendingOrderIds.ToList());

                var orderDetailHtml = RenderPartialViewToString("_Dispatch.OrderDetail", new OrderModel());

                var onGoinggOrderIds = _orderService.GetAssignedOrderIds(storeId: storeIdReg, vendorId: vendorIdReg, vendorIds: subAdminAccosiatedvendorIds);
                var onGoingOrderHtml = RenderPartialViewToString("_Dispatch.OrderOnGoing", onGoinggOrderIds.ToList());

                return Json(new
                {
                    success = true,
                    message = "Agent assigned",
                    pendingOrderHtml,
                    orderDetailHtml,
                    onGoingOrderHtml
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = ex.ToString(),
                });
            }
        }

        //update pending orders using SignalR
        public virtual string UpdatePendingOrders()
        {
            int storeIdReg = 0;
            int vendorIdReg = 0;
            var subAdminAccosiatedvendorIds = new List<int>();
            if (_workContext.IsStoreOwnerRole)
            {
                storeIdReg = _workContext.GetCurrentStoreId;
            }
            else if (_workContext.IsVendorRole)
            {
                storeIdReg = _workContext.GetCurrentStoreId;
                vendorIdReg = _workContext.CurrentVendor.Id;
            }
            else if (_workContext.SubAdmin.IsSubAdminRole)
            {
                storeIdReg = _workContext.GetCurrentStoreId;
                subAdminAccosiatedvendorIds = _workContext.SubAdmin.AssociatedVenorIds?.ToList();
            }

            //prepare html sections
            var pendingOrderIds = _orderService.GetUnassignedOrderIds(storeId: storeIdReg, vendorId: vendorIdReg, vendorIds: subAdminAccosiatedvendorIds);
            var pendingOrderHtml = RenderPartialViewToString("_Dispatch.OrderPending", pendingOrderIds.ToList());
            return pendingOrderHtml;
        }

        //update ongoing orders using SignalR
        public virtual string UpdateOngoingOrders()
        {
            int storeIdReg = 0;
            int vendorIdReg = 0;
            var subAdminAccosiatedvendorIds = new List<int>();
            if (_workContext.IsStoreOwnerRole)
            {
                storeIdReg = _workContext.GetCurrentStoreId;
            }
            else if (_workContext.IsVendorRole)
            {
                storeIdReg = _workContext.GetCurrentStoreId;
                vendorIdReg = _workContext.CurrentVendor.Id;
            }
            else if (_workContext.SubAdmin.IsSubAdminRole)
            {
                storeIdReg = _workContext.GetCurrentStoreId;
                subAdminAccosiatedvendorIds = _workContext.SubAdmin.AssociatedVenorIds?.ToList();
            }

            var onGoinggOrderIds = _orderService.GetAssignedOrderIds(storeId: storeIdReg, vendorId: vendorIdReg, vendorIds: subAdminAccosiatedvendorIds);
            var onGoingOrderHtml = RenderPartialViewToString("_Dispatch.OrderOnGoing", onGoinggOrderIds.ToList());
            return onGoingOrderHtml;
        }
    }
}
