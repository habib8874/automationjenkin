﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NHKCustomerApplication.Models
{
    public class CustomerWallet
    {
        public int ID { get; set; }
        public int CustomerId { get; set; }
        public int? OrderId { get; set; }
        public decimal Amount { get; set; }
        public string Note { get; set; }
        public bool Credited { get; set; }
        public DateTime TransactionOnUtc { get; set; }
        public int TransactionBy { get; set; }
        public bool FromWeb { get; set; }
    }
}
