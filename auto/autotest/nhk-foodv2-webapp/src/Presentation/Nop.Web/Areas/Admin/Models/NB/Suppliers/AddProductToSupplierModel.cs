﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB.Suppliers
{
    public partial class AddProductToSupplierModel:BaseNopModel
    {
        public AddProductToSupplierModel()
        {
            SelectedProductIds = new List<int>();

        }


        #region Properties

        public int SupplierId { get; set; }

        public IList<int> SelectedProductIds { get; set; }

        #endregion
    }
}
