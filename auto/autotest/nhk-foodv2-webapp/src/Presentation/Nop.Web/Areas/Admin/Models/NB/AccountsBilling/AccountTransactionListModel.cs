﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB.AccountsBilling
{
    /// <summary>
    /// Represents a Account Transaction log list model
    /// </summary>
    public partial class AccountTransactionListModel : BasePagedListModel<AccountTransactionModel>
    {
    }
}