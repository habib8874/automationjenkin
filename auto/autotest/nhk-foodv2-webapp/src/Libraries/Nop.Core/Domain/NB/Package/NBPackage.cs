﻿using System;

namespace Nop.Core.Domain.NB.Package
{
    /// <summary>
    /// Represents a Package
    /// </summary>
    public partial class NBPackage : BaseEntity
    {
        /// <summary>
        /// Gets or sets the Name 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the PictureId 
        /// </summary>
        public int PictureId { get; set; }


        /// <summary>
        /// Gets or sets the ShortDescription 
        /// </summary>
        public string ShortDescription { get; set; }


        /// <summary>
        /// Gets or sets the FullDescription 
        /// </summary>
        public string FullDescription { get; set; }

        /// <summary>
        /// Gets or sets the UnitLabel 
        /// </summary>
        public string UnitLabel { get; set; }

        /// <summary>
        /// Gets or sets the StoreId 
        /// </summary>
        public int? StoreId { get; set; }

        /// <summary>
        /// Gets or sets the ProductId 
        /// </summary>
        public int? ProductId { get; set; }

        /// <summary>
        /// Gets or sets the Deleted 
        /// </summary>
        public bool Deleted { get; set; }


        /// <summary>
        /// Gets or sets the CreatedOnUtc 
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedOnUtc
        /// </summary>
        public DateTime? ModifiedOnUtc { get; set; }

    }
}
