﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.WebsiteExtension;

namespace Nop.Data.Mapping.WebsiteExtensionMap
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class WebsiteExtensionMap : NopEntityTypeConfiguration<WebsiteExtension>
    {
        /// <summary>
        /// Ctor
        /// </summary>
        public override void Configure(EntityTypeBuilder<WebsiteExtension> builder)
        {
            builder.ToTable("WebsiteExtension");
            builder.HasKey(s => s.id);
            builder.Property(s => s.extension).IsRequired().HasMaxLength(40);
            base.Configure(builder);
        }
    }
}