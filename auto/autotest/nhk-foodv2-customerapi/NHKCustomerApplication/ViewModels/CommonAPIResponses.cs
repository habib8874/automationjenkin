﻿namespace NHKCustomerApplication.ViewModels
{
	public class CommonAPIResponses
	{
		public int StatusCode { get; set; }
		public string ErrorMessageTitle { get; set; }
		public string ErrorMessage { get; set; }
		public bool Status { get; set; }
		public object ResponseObj { get; set; }
	}
}
