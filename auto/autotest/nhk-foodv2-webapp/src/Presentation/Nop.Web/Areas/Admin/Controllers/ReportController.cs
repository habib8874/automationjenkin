﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Models.Reports;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class ReportController : BaseAdminController
    {
        #region Fields

        private readonly IPermissionService _permissionService;
        private readonly IReportModelFactory _reportModelFactory;
        private readonly IWorkContext _workContext;
        private readonly IOrderModelFactory _orderModelFactory;
        private readonly IOrderService _orderService;
        private readonly ICustomerService _customerService;
        private readonly IPriceFormatter _priceFormatter;
        #endregion

        #region Ctor

        public ReportController(IWorkContext workContext,
            IPermissionService permissionService,
            IReportModelFactory reportModelFactory,
            IOrderModelFactory orderModelFactory,
            IOrderService orderService,
            ICustomerService customerService,
            IPriceFormatter priceFormatter)
        {
            _workContext = workContext;
            _permissionService = permissionService;
            _reportModelFactory = reportModelFactory;
            _orderModelFactory = orderModelFactory;
            _orderService = orderService;
            _customerService = customerService;
            _priceFormatter = priceFormatter;
        }

        #endregion

        #region Methods

        #region Low stock

        public virtual IActionResult LowStock()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            //prepare model
            var model = _reportModelFactory.PrepareLowStockProductSearchModel(new LowStockProductSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult LowStockList(LowStockProductSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _reportModelFactory.PrepareLowStockProductListModel(searchModel);

            return Json(model);
        }

        #endregion

        #region Bestsellers

        public virtual IActionResult Bestsellers()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //prepare model
            var model = _reportModelFactory.PrepareBestsellerSearchModel(new BestsellerSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult BestsellersList(BestsellerSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedDataTablesJson();

            if (_workContext.CurrentCustomer.CustomerRoles.Any(x => x.Name == "Store Owner"))
            {
                searchModel.StoreId = _workContext.GetCurrentStoreId;
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Any(x => x.Name == "Vendor"))
            {
                searchModel.StoreId = _workContext.GetCurrentStoreId;
                // searchModel.SearchVendor = _workContext.GetCurrentStoreId;
            }
            else if (_workContext.SubAdmin.IsSubAdminRole)
            {
                searchModel.StoreId = _workContext.GetCurrentStoreId;
            }
            //prepare model
            var model = _reportModelFactory.PrepareBestsellerListModel(searchModel);

            return Json(model);
        }

        #endregion

        #region Never Sold

        public virtual IActionResult NeverSold()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //prepare model
            var model = _reportModelFactory.PrepareNeverSoldSearchModel(new NeverSoldReportSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult NeverSoldList(NeverSoldReportSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedDataTablesJson();
            if (_workContext.CurrentCustomer.CustomerRoles.Any(x => x.Name == "Store Owner"))
            {
                searchModel.SearchStoreId = _workContext.GetCurrentStoreId;
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Any(x => x.Name == "Vendor"))
            {
                searchModel.SearchStoreId = _workContext.GetCurrentStoreId;
                // searchModel.SearchVendor = _workContext.GetCurrentStoreId;
            }
            else if(_workContext.SubAdmin.IsSubAdminRole)
            {
                searchModel.SearchStoreId = _workContext.GetCurrentStoreId;
            }
            //prepare model
            var model = _reportModelFactory.PrepareNeverSoldListModel(searchModel);

            return Json(model);
        }

        #endregion

        #region Country sales

        public virtual IActionResult CountrySales()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.OrderCountryReport))
                return AccessDeniedView();

            //prepare model
            var model = _reportModelFactory.PrepareCountrySalesSearchModel(new CountryReportSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult CountrySalesList(CountryReportSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.OrderCountryReport))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _reportModelFactory.PrepareCountrySalesListModel(searchModel);

            return Json(model);
        }

        #endregion

        #region Customer reports

        public virtual IActionResult RegisteredCustomers()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            //prepare model
            var model = _reportModelFactory.PrepareCustomerReportsSearchModel(new CustomerReportsSearchModel());

            return View(model);
        }

        public virtual IActionResult BestCustomersByOrderTotal()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            //prepare model
            var model = _reportModelFactory.PrepareCustomerReportsSearchModel(new CustomerReportsSearchModel());

            return View(model);
        }

        public virtual IActionResult BestCustomersByNumberOfOrders()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            //prepare model
            var model = _reportModelFactory.PrepareCustomerReportsSearchModel(new CustomerReportsSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult ReportBestCustomersByOrderTotalList(BestCustomersReportSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _reportModelFactory.PrepareBestCustomersReportListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult ReportBestCustomersByNumberOfOrdersList(BestCustomersReportSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _reportModelFactory.PrepareBestCustomersReportListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult ReportRegisteredCustomersList(RegisteredCustomersReportSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _reportModelFactory.PrepareRegisteredCustomersReportListModel(searchModel);

            return Json(model);
        }

        #endregion

        #endregion

        #region Agent Earning

        public virtual IActionResult AgentEarningList(List<int> orderStatuses = null, List<int> paymentStatuses = null, List<int> shippingStatuses = null)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgentEarning))
                return AccessDeniedView();

            //prepare model
            var model = _orderModelFactory.PrepareAgentEarningSearchModel(new AgentEarningSearchModel
            {
                OrderStatusIds = orderStatuses,
                PaymentStatusIds = paymentStatuses,
                ShippingStatusIds = shippingStatuses
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult AgentEarningOrderList(AgentEarningSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgentEarning))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _orderModelFactory.PrepareAgentEarningListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult AgentEarningAmountDetails(AgentEarningSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgentEarning))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _orderModelFactory.GetAgentEarningDetails(searchModel);

            return Json(new { Orders = model.Item1, Amount = _priceFormatter.FormatPrice(model.Item2, true, false), ReceivedAmount = _priceFormatter.FormatPrice(model.Item3, true, false), PaidAmount = _priceFormatter.FormatPrice(model.Item4, true, false) });
        }

        [HttpPost]
        public virtual IActionResult AgentEarningUpdate(AgentEarningModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgentEarning))
                return AccessDeniedView();
            var assignedAgent = _customerService.GetAssignedAgentByOrderId(model.Id);
            if (assignedAgent != null)
            {
                model.AssignedAgentId = assignedAgent.AgentId;
            }

            var agentEarning = new AgentEarning();
            agentEarning.AgentId = model.AssignedAgentId;
            agentEarning.OrderId = model.Id;
            agentEarning.PayableAmount = model.PayableAmountValue;
            agentEarning.PaidAmount = model.PaidAmount;

            _orderService.InsertUpdateAgentEarning(agentEarning);

            return new NullJsonResult();
        }

        [HttpPost, ActionName("AgentEarningList")]
        [FormValueRequired("go-to-order-by-number")]
        public virtual IActionResult GoToOrderId(AgentEarningSearchModel model)
        {
            var order = _orderService.GetOrderByCustomOrderNumber(model.GoDirectlyToCustomOrderNumber);

            if (order == null)
                return AgentEarningList();

            return RedirectToAction("Edit", "Order", new { id = order.Id });
        }
        #endregion
    }
}
