﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Models.Common;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Nop.Web.Areas.Admin.Models.Vendors
{
    /// <summary>
    /// Represents a vendor model
    /// </summary>
    public partial class VendorModel : BaseNopEntityModel, ILocalizedModel<VendorLocalizedModel>, IDiscountSupportedModel
    {
        #region Ctor

        public VendorModel()
        {
            if (PageSize < 1)
                PageSize = 5;

            Address = new AddressModel();
            VendorAttributes = new List<VendorAttributeModel>();
            Locales = new List<VendorLocalizedModel>();
            AssociatedCustomers = new List<VendorAssociatedCustomerModel>();
            VendorNoteSearchModel = new VendorNoteSearchModel();
            AvailabeStoreId = new List<SelectListItem>();
            AvailabeWeekDays = new List<SelectListItem>();
            AvailableTimeZones = new List<SelectListItem>();
            VendorScheduleModels = new List<VendorScheduleModel>();
            AddPictureModel = new VendorSliderPictureModel();
            VendorSliderPictureModels = new List<VendorSliderPictureModel>();
            VendorSliderPictureSearchModel = new VendorSliderPictureSearchModel();
            SelectedDiscountIds = new List<int>();
            AvailableDiscounts = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Vendors.Fields.Name")]
        public string Name { get; set; }

        //time zone
        [NopResourceDisplayName("Admin.Customers.Customers.Fields.TimeZoneId")]
        public string TimeZoneId { get; set; }
        public IList<SelectListItem> AvailableTimeZones { get; set; }

        [DataType(DataType.EmailAddress)]
        [NopResourceDisplayName("Admin.Vendors.Fields.Email")]
        public string Email { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.Description")]
        public string Description { get; set; }

        [UIHint("Picture")]
        [NopResourceDisplayName("Admin.Vendors.Fields.Picture")]
        public int PictureId { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.AdminComment")]
        public string AdminComment { get; set; }

        public AddressModel Address { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.Active")]
        public bool Active { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }        

        [NopResourceDisplayName("Admin.Vendors.Fields.MetaKeywords")]
        public string MetaKeywords { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.MetaDescription")]
        public string MetaDescription { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.MetaTitle")]
        public string MetaTitle { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.SeName")]
        public string SeName { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.PageSize")]
        public int PageSize { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.AllowCustomersToSelectPageSize")]
        public bool AllowCustomersToSelectPageSize { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.PageSizeOptions")]
        public string PageSizeOptions { get; set; }

        public List<VendorAttributeModel> VendorAttributes { get; set; }

        public IList<VendorLocalizedModel> Locales { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.AssociatedCustomerEmails")]
        public IList<VendorAssociatedCustomerModel> AssociatedCustomers { get; set; }

        //vendor notes
        [NopResourceDisplayName("Admin.Vendors.VendorNotes.Fields.Note")]
        public string AddVendorNoteMessage { get; set; }

        public VendorNoteSearchModel VendorNoteSearchModel { get; set; }

        #region Custom cod from v4.0
        [NopResourceDisplayName("Admin.Vendors.Fields.StoreOwnerId")]
        public int StoreId { get; set; }
        public IList<SelectListItem> AvailabeStoreId { get; set; }

        //Add New Field By Mah
        [NopResourceDisplayName("Admin.Vendors.Fields.Geolocation")]
        public string Geolocation { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.Geofancing")]
        public string Geofancing { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.Weeklyoff")]
        public string Weeklyoff { get; set; }

        public IList<SelectListItem> AvailabeWeekDays { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.Opentime")]
        public string Opentime { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.Closetime")]
        public string Closetime { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.IsOpen")]
        public bool IsOpen { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.IsBookTableEnable")] // custom by Mohini
        public bool IsBookTableEnable { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.IsRatingPercentEnable")]
        public bool RatingPercent { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.IsAvailableSchedulesEnable")]
        public bool IsAvailableSchedulesEnable { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.IsCostfortwoEnable")]
        public bool IsCostForEnable { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.IsDeliveryTimeEnable")]
        public bool IsDeliveryTimeEnable { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.IsTakeAway")]
        public bool IsTakeAway { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.IsDelivery")]
        public bool IsDelivery { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.IsDining")]
        public bool IsDining { get; set; }

        //Opentime time, Closetime time,IsOpen bit
        // Created By Mah

        [NopResourceDisplayName("Admin.Vendors.Fields.ExpDeliveryTime")]
        public string ExpDeliveryTime { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.CostFor")]
        public string CostFor { get; set; }
        public VendorScheduleModel AddVendorSchedule { get; set; }
        public IList<VendorScheduleModel> VendorScheduleModels { get; set; }
        [NopResourceDisplayName("Admin.Vendors.Fields.IsCustom")]
        public bool IsCustom { get; set; }
        [NopResourceDisplayName("Admin.Vendors.Fields.IsAllDay")]
        public bool IsAllDay { get; set; }
        [NopResourceDisplayName("Admin.Vendors.Fields.DeliveryCharge")]
        public decimal DeliveryCharge { get; set; }

        [NopResourceDisplayName("NB.Admin.Vendors.Fields.MinOrderAmount")]
        public decimal MinOrderAmount { get; set; }

        public bool IsVendor { get; set; }

        //[Validator(typeof(VendorScheduleValidator))]
        public partial class VendorScheduleModel : BaseNopModel
        {
            public VendorScheduleModel()
            {
                AvailableDays = new List<SelectListItem>();
                AvailableFromTime = new List<SelectListItem>();
                AvailableToTime = new List<SelectListItem>();
            }
            public int Id { get; set; }
            [NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.ScheduleName")]
            public string ScheduleName { get; set; }
            [NopResourceDisplayName("admin.catalog.products.pictures.fields.scheduleday")]
            public int ScheduleDay { get; set; }
            public string ScheduleDayS { get; set; }
            [NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.ScheduleFromTime")]
            public string ScheduleFromTime { get; set; }
            public TimeSpan ScheduleFromTimeTS { get; set; }
            public TimeSpan ScheduleToTimeTS { get; set; }
            [NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.ScheduleToTime")]
            public string ScheduleToTime { get; set; }
            public int DisplayOrder { get; set; }
            public IList<SelectListItem> AvailableDays { get; set; }
            public IList<SelectListItem> AvailableFromTime { get; set; }
            public IList<SelectListItem> AvailableToTime { get; set; }
            public int VendorId { get; set; }
        }

        #endregion

        [NopResourceDisplayName("NB.Admin.Vendors.Fields.FixedOrDynamic")]
        public string FixedOrDynamic { get; set; }

        [NopResourceDisplayName("NB.Admin.Vendors.Fields.KmOrMilesForDynamic")]
        public string KmOrMilesForDynamic { get; set; }

        [NopResourceDisplayName("NB.Admin.Vendors.Fields.PriceForDynamic")]
        public decimal PriceForDynamic { get; set; }

        [NopResourceDisplayName("NB.Admin.Vendors.Fields.DistanceForDynamic")]
        public decimal DistanceForDynamic { get; set; }

        [NopResourceDisplayName("NB.Admin.Vendors.Fields.PricePerUnitDistanceForDynamic")]
        public decimal PricePerUnitDistanceForDynamic { get; set; }

        //pictures
        public VendorSliderPictureModel AddPictureModel { get; set; }
        public IList<VendorSliderPictureModel> VendorSliderPictureModels { get; set; }
        public VendorSliderPictureSearchModel VendorSliderPictureSearchModel { get; set; }
        public IList<int> SelectedDiscountIds { get; set; }
        public IList<SelectListItem> AvailableDiscounts { get; set; }

        [NopResourceDisplayName("NB.Admin.Vendors.Fields.Discounts")]
        public int DiscountId { get; set; }
        #endregion

        #region Nested classes

        public partial class VendorAttributeModel : BaseNopEntityModel
        {
            public VendorAttributeModel()
            {
                Values = new List<VendorAttributeValueModel>();
            }

            public string Name { get; set; }

            public bool IsRequired { get; set; }

            /// <summary>
            /// Default value for textboxes
            /// </summary>
            public string DefaultValue { get; set; }

            public AttributeControlType AttributeControlType { get; set; }

            public IList<VendorAttributeValueModel> Values { get; set; }
        }

        public partial class VendorAttributeValueModel : BaseNopEntityModel
        {
            public string Name { get; set; }

            public bool IsPreSelected { get; set; }
        }

        #endregion
    }

    public partial class VendorLocalizedModel : ILocalizedLocaleModel
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.Description")]
        public string Description { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.MetaKeywords")]
        public string MetaKeywords { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.MetaDescription")]
        public string MetaDescription { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.MetaTitle")]
        public string MetaTitle { get; set; }

        [NopResourceDisplayName("Admin.Vendors.Fields.SeName")]
        public string SeName { get; set; }
        

    }

    public partial class VendorScheduleListSearchModel : BaseSearchModel
    {
    }
    public partial class VendorScheduleListModel : BasePagedListModel<VendorModel.VendorScheduleModel>
    {
    }
}