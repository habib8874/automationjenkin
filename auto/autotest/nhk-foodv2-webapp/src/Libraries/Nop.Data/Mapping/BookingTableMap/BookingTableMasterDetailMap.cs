﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.BookingTable;

namespace Nop.Data.Mapping.BookingTableMap
{
    public partial class BookingTableMasterDetailMap : NopEntityTypeConfiguration<BookingTableMasterDetail>
    {
        public override void Configure(EntityTypeBuilder<BookingTableMasterDetail> builder)
        {
            builder.ToTable("BookingTableMasterDetail");
            builder.HasKey(p => p.Id);
            builder.Property(c => c.BookingMasterId).IsRequired();
            builder.Property(c => c.TableAvailableFrom).IsRequired();
            builder.Property(c => c.TableAvailableTo).IsRequired();
            base.Configure(builder);
        }
    }
}
