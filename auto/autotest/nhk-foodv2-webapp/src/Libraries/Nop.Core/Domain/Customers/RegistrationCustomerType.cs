﻿namespace Nop.Core.Domain.Customers
{
    /// <summary>
    /// Represents the customer registration type enumeration
    /// </summary>
    public enum RegistrationCustomerType
    {
        /// <summary>
        /// Customer type Customer
        /// </summary>
        Customer = 1,

        /// <summary>
        /// Store owner type Customer
        /// </summary>
        StoreOwner = 2,

        /// <summary>
        /// Agent type Customer
        /// </summary>
        Agent = 3,

        /// <summary>
        /// Restaurant Owner type Customer
        /// </summary>
        RestaurantOwner = 4
    }
}
