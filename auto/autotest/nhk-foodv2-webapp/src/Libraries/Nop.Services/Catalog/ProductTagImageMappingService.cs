﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Stores;
using Nop.Data;
using Nop.Services.Events;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Product tag service
    /// </summary>
    public partial class ProductTagImageMappingService : IProductTagImageMappingService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : store ID
        /// </remarks>
        private const string PRODUCTTAG_COUNT_KEY = "Nop.producttagimagemapping.count-{0}";

        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PRODUCTTAG_PATTERN_KEY = "Nop.producttagimagemapping.";

        #endregion

        #region Fields

        private readonly IRepository<ProductImageTag> _productTagImageMappingReporitory;
        private readonly IRepository<ProductImageTag> _tagImageMappingReporitory;
        private readonly IRepository<ProductTag> _productTagRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly IDataProvider _dataProvider;
        private readonly IDbContext _dbContext;
        private readonly CommonSettings _commonSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly IProductService _productService;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="productServiceTagImageMappingRepository">Product Tag Image Mapping</param>
        /// <param name="productTagRepository">Product tag repository</param>
        /// <param name="dataProvider">Data provider</param>
        /// <param name="dbContext">Database Context</param>
        /// <param name="commonSettings">Common settings</param>
        /// <param name="cacheManager">Static cache manager</param>
        /// <param name="eventPublisher">Event published</param>
        /// <param name="storeMappingRepository">Store mapping repository</param>
        /// <param name="catalogSettings">Catalog settings</param>
        /// <param name="productService">Product service</param>
        public ProductTagImageMappingService(IRepository<ProductImageTag> productTagImageMappingRepository,
            IRepository<ProductTag> productTagRepository,
            IRepository<StoreMapping> storeMappingRepository,
            IDataProvider dataProvider,
            IDbContext dbContext,
            CommonSettings commonSettings,
            CatalogSettings catalogSettings,
            IStaticCacheManager cacheManager,
            IEventPublisher eventPublisher,
            IProductService productService,
            IRepository<ProductImageTag> tagImageMappingReporitory)
        {
           _productTagImageMappingReporitory = productTagImageMappingRepository;
           _productTagRepository = productTagRepository;
           _storeMappingRepository = storeMappingRepository;
           _dataProvider = dataProvider;
           _dbContext = dbContext;
           _commonSettings = commonSettings;
           _catalogSettings = catalogSettings;
           _cacheManager = cacheManager;
           _eventPublisher = eventPublisher;
           _productService = productService;
           _tagImageMappingReporitory = tagImageMappingReporitory;
        }

        #endregion

        #region Nested classes

        private class ProductTagWithCount
        {
            public int ProductTagId { get; set; }
            public int ProductCount { get; set; }
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Get product count for each of existing product tag
        /// </summary>
        /// <param name="storeId">Store identifier</param>
        /// <returns>Dictionary of "product tag ID : product count"</returns>
        private Dictionary<int, int> GetProductCount(int storeId)
        {
            var key = string.Format(PRODUCTTAG_COUNT_KEY, storeId);
            return _cacheManager.Get(key, () =>
            {
                var query = _productTagRepository.Table.Select(pt => new
                {
                    pt.Id,
                    ProductCount = (storeId == 0 || _catalogSettings.IgnoreStoreLimitations) ?
                        pt.ProductProductTagMappings.Where(p=> !p.Product.Deleted && p.Product.Published).Count()
                        : (from p in pt.ProductProductTagMappings
                           join sm in _storeMappingRepository.Table
                               on new { p1 = p.ProductId, p2 = "Product" } equals new { p1 = sm.EntityId, p2 = sm.EntityName } into p_sm
                           from sm in p_sm.DefaultIfEmpty()
                           where (!p.Product.LimitedToStores || storeId == sm.StoreId) && !p.Product.Deleted && p.Product.Published
                           select p).Count()
                });
                var dictionary = new Dictionary<int, int>();
                foreach (var item in query)
                    dictionary.Add(item.Id, item.ProductCount);
                return dictionary;
            });
        }

        #endregion

        #region Methods

        /// <summary>
        /// Delete a product tag Image Mapping
        /// </summary>
        /// <param name="productTagImageMapping">Product tag Image Mapping</param>
        public virtual void DeleteProductTagImageMapping(ProductImageTag productTagImageMapping)
        {
            if (productTagImageMapping == null)
                throw new ArgumentNullException(nameof(productTagImageMapping));

            _productTagImageMappingReporitory.Delete(productTagImageMapping);

            //cache
            //_cacheManager.RemoveByPattern(PRODUCTTAG_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(productTagImageMapping);
        }

        /// <summary>
        /// Gets all product tags Image mapping
        /// </summary>
        /// <returns>Product tags Image mapping</returns>
        public virtual IList<ProductImageTag> GetAllProductTagsImageMapping()
        {
            var tags= _tagImageMappingReporitory.Table.ToList();
            return tags;
        }

        /// <summary>
        /// Gets product tag Image Mapping
        /// </summary>
        /// <param name="productTagId">Product tag identifier</param>
        /// <returns>Product tag Image Mapping</returns>
        public virtual ProductImageTag GetProductTagImageMappingById(int productTagImageMappingId)
        {
            if (productTagImageMappingId == 0)
                return null;

            return _productTagImageMappingReporitory.GetById(productTagImageMappingId);
        }


        /// <summary>
        /// Gets product tag Image Mapping
        /// </summary>
        /// <param name="productTagId">Product tag identifier</param>
        /// <returns>Product tag Image Mapping</returns>
        public virtual ProductImageTag GetProductTagImageMappingByTagId(int TagId)
        {
            if (TagId == 0)
                return null;
          
            return _productTagImageMappingReporitory.Table.FirstOrDefault(x => x.TagId == TagId);
        }

        /// <summary>
        /// Inserts a product tag Image Mapping
        /// </summary>
        /// <param name="productTagImageMapping">Product tag image mapping</param>
        public virtual void InsertProductTagImageMapping(ProductImageTag productTagImageMapping)
        {
            if (productTagImageMapping == null)
                throw new ArgumentNullException(nameof(productTagImageMapping));

            _productTagImageMappingReporitory.Insert(productTagImageMapping);

            //cache
            //_cacheManager.RemoveByPattern(PRODUCTTAG_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(productTagImageMapping);
        }

        /// <summary>
        /// Updates the product tag image mapping
        /// </summary>
        /// <param name="productTagImageMapping">Product tag image mapping</param>
        public virtual void UpdateProductTagImageMapping(ProductImageTag productTagImageMapping)
        {
            if (productTagImageMapping == null)
                throw new ArgumentNullException(nameof(productTagImageMapping));

            _productTagImageMappingReporitory.Update(productTagImageMapping);

            //event notification
            _eventPublisher.EntityUpdated(productTagImageMapping);
        }

        /// <summary>
        /// Get number of products
        /// </summary>
        /// <param name="productTagId">Product tag identifier</param>
        /// <param name="storeId">Store identifier</param>
        /// <returns>Number of products</returns>
        public virtual int GetProductCount(int productTagId, int storeId)
        {
            var dictionary = GetProductCount(storeId);
            if (dictionary.ContainsKey(productTagId))
                return dictionary[productTagId];

            return 0;
        }

        /// <summary>
        /// Update product tags
        /// </summary>
        /// <param name="product">Product for update</param>
        /// <param name="productTags">Product tags</param>
       //// public virtual void UpdateProductTagsImageMapping(Product product, string[] productTagsImageMapping)
       // {
       //     if (product == null)
       //         throw new ArgumentNullException(nameof(product));

       //     //product tags
       //     var existingProductTags = product.ProductTagImageMappings.ToList();
       //     var productTagsToRemove = new List<ProductImageTag>();
       //     foreach (var existingProductTag in existingProductTags)
       //     {
       //         var found = false;
       //         if (!found)
       //         {
       //             productTagsToRemove.Add(existingProductTag);
       //         }
       //         //}
       //         foreach (var productTagImageMapping in productTagsToRemove)
       //         {
       //             product.ProductTagImageMappings.Remove(productTagImageMapping);
       //             _productService.UpdateProduct(product);
       //         }
       //     }
       // }
        #endregion
    }
}