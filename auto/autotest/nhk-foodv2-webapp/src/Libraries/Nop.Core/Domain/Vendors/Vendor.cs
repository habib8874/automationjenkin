﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Seo;
using Nop.Core.Domain.NB.Discount;

namespace Nop.Core.Domain.Vendors
{
    /// <summary>
    /// Represents a vendor
    /// </summary>
    public partial class Vendor : BaseEntity, ILocalizedEntity, ISlugSupported, IDiscountSupported
    {
        private ICollection<VendorNote> _vendorNotes;
        private ICollection<VendorSliderPicture> _vendorSliderPictures;
        protected ICollection<DiscountVendorMapping> _discountVendorMapping;

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the picture identifier
        /// </summary>
        public int PictureId { get; set; }

        /// <summary>
        /// Gets or sets the address identifier
        /// </summary>
        public int AddressId { get; set; }

        /// <summary>
        /// Gets or sets the admin comment
        /// </summary>
        public string AdminComment { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity is active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets the meta keywords
        /// </summary>
        public string MetaKeywords { get; set; }

        /// <summary>
        /// Gets or sets the meta description
        /// </summary>
        public string MetaDescription { get; set; }

        /// <summary>
        /// Gets or sets the meta title
        /// </summary>
        public string MetaTitle { get; set; }

        /// <summary>
        /// Gets or sets the page size
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether customers can select the page size
        /// </summary>
        public bool AllowCustomersToSelectPageSize { get; set; }

        /// <summary>
        /// Gets or sets the available customer selectable page size options
        /// </summary>
        public string PageSizeOptions { get; set; }
        public decimal DeliveryCharge { get; set; }

        public decimal MinOrderAmount { get; set; }

        #region Custom code from v4.0
        public int StoreId { get; set; }
        public string Geolocation { get; set; }
        public string Geofancing { get; set; }
        public string WeeklyOff { get; set; }
        public string Opentime { get; set; }
        public string Closetime { get; set; }
        public bool? RatingPercent { get; set; }
        public bool? IsBookTableEnable { get; set; }
        public bool? IsCostForEnable { get; set; }
        public bool? IsDeliveryTimeEnable { get; set; }
        public bool? IsAvailableSchedulesEnable { get; set; }

        public bool IsOpen { get; set; }
        public bool IsTakeAway { get; set; }
        public bool IsDelivery { get; set; }
        public bool IsDining { get; set; }
        // [System.ComponentModel.DefaultValue("")]
        public string ExpDeliveryTime { get; set; }
        //[System.ComponentModel.DefaultValue("")]
        public string CostFor { get; set; }
        public int TakeAwayTime { get; set; }
        public int DeliveryTime { get; set; }
        public decimal MinimumOrderValue { get; set; }
        public TimeSpan? LunchFrom { get; set; }
        public TimeSpan? LunchTo { get; set; }
        public TimeSpan? DinnerFrom { get; set; }
        public TimeSpan? DinnerTo { get; set; }
        public int? AvailableType { get; set; }
        #endregion
        public string FixedOrDynamic { get; set; }

        public string KmOrMilesForDynamic { get; set; }

        public decimal PriceForDynamic { get; set; }

        public decimal DistanceForDynamic { get; set; }

        public decimal PricePerUnitDistanceForDynamic { get; set; }
        //public bool HasDiscountsApplied { get; set; }

        /// <summary>
        /// Gets or sets vendor notes
        /// </summary>
        public virtual ICollection<VendorNote> VendorNotes
        {
            get => _vendorNotes ?? (_vendorNotes = new List<VendorNote>());
            protected set => _vendorNotes = value;
        }

        public virtual ICollection<VendorSliderPicture> VendorSliderPictures
        {
            get => _vendorSliderPictures ?? (_vendorSliderPictures = new List<VendorSliderPicture>());
            protected set => _vendorSliderPictures = value;
        }

        /// <summary>
        /// Gets or sets the collection of applied discounts
        /// </summary>
        public IList<Discount> AppliedDiscounts => DiscountVendorMapping.Select(mapping => mapping.Discount).ToList();

        /// <summary>
        /// Gets or sets the discount-product mappings
        /// </summary>
        public virtual ICollection<DiscountVendorMapping> DiscountVendorMapping
        {
            
            get => _discountVendorMapping ?? (_discountVendorMapping = new List<DiscountVendorMapping>());
            set => _discountVendorMapping = value;
        }

    }
}
