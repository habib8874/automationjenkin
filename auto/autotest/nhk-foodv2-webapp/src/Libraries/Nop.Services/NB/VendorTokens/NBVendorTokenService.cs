﻿using Nop.Core.Data;
using System.Linq;
using Nop.Core.Domain.NB.VendorTokens;
using Nop.Services.Events;
using System.Collections.Generic;
using System;

namespace Nop.Services.NB.VendorTokens
{
    public partial class NBVendorTokenService : INBVendorTokenService
    {
        #region Fields

        private readonly IRepository<NBVendorToken> _nBVendorTokenRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        public NBVendorTokenService(IRepository<NBVendorToken> nBVendorTokenRepository,
            IEventPublisher eventPublisher)
        {
            _nBVendorTokenRepository = nBVendorTokenRepository;
            _eventPublisher = eventPublisher;
        }

        #endregion

        #region NBVendorToken


        /// <summary>
        /// GetNBVendorTokenByToken
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public virtual NBVendorToken GetNBVendorTokenByToken(string token)
        {
            if (string.IsNullOrEmpty(token))
                return null;

            var query = from vt in _nBVendorTokenRepository.Table
                        where !vt.Deleted
                        select vt;

                query = query.Where(vt => vt.Token.Contains(token));

            return query.FirstOrDefault();
        }

        /// <summary>
        /// GetNBVendorTokenByCustomerId
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public virtual IList<NBVendorToken> GetNBVendorTokenByCustomerId(int customerId)
        {

            var query = from eq in _nBVendorTokenRepository.Table
                        where !eq.Deleted && eq.CustomerId == customerId
                        select eq;

            return query.ToList();
        }

        /// <summary>
        /// InsertNBVendorToken
        /// </summary>
        /// <param name="vendorToken"></param>
        public virtual void InsertNBVendorToken(NBVendorToken vendorToken)
        {
            if (vendorToken == null)
                throw new ArgumentNullException("vendorToken");

            _nBVendorTokenRepository.Insert(vendorToken);

            //event notification
            _eventPublisher.EntityInserted(vendorToken);
        }

        /// <summary>
        /// UpdateNBVendorToken
        /// </summary>
        /// <param name="vendorToken"></param>
        public virtual void UpdateNBVendorToken(NBVendorToken vendorToken)
        {
            if (vendorToken == null)
                throw new ArgumentNullException("vendorToken");

            _nBVendorTokenRepository.Update(vendorToken);


            //event notification
            _eventPublisher.EntityUpdated(vendorToken);
        }

        #endregion
    }
}
