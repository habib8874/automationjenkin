﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.NB.Product
{
    public partial class ProductFiltrationAttributes : BaseNopEntityModel
    {
        public List<Attributes> BrandName { get; set; }
        public List<string> PackSize { get; set; }
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public string CurrencySymbol { get; set; }
    }
    public partial class Attributes
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
