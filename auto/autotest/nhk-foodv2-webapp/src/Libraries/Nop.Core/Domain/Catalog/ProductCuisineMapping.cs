using System.Collections.Generic;
using Nop.Core.Domain.Localization;

namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents a product tag
    /// </summary>
    public partial class ProductCuisineMapping: BaseEntity
    {

        // public string Name { get; set; }

        //private ICollection<ProductCuisine> _ProductCuisine;
        public int CuisineId { get; set; }
        public int ProductId { get; set; }
        //public virtual ICollection<ProductCuisine> ProductCuisine
        //{
        //    get { return _ProductCuisine ?? (_ProductCuisine = new List<ProductCuisine>()); }
        //    protected set { _ProductCuisine = value; }
        //}
    }
}
