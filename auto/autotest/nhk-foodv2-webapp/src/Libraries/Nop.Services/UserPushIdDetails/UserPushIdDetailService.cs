﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.UserPushIdDetails;
using Nop.Data;
using Nop.Services.Events;

namespace Nop.Services.UserPushIdDetails
{
    public partial class UserPushIdDetailService : IUserPushIdDetailService
    {
        #region Fields
        private readonly IDbContext _context;
        private readonly IRepository<UserPushIdDetail> _userPushIdDetailRepository ;        
        //private readonly IStaticCacheManager _cacheManager;

        #endregion

        
        public UserPushIdDetailService(IDbContext context, IRepository<UserPushIdDetail> userPushIdDetailRepository)
        {
            this._context = context;
            this._userPushIdDetailRepository = userPushIdDetailRepository;
        }
        
        public IList<UserPushIdDetail> GetAllUserPushIdDetails()
        {
            throw new NotImplementedException();
        }

        public UserPushIdDetail GetUserPushIdDetailById(int userId)
        {
            throw new NotImplementedException();
        }

        public void InsertUserPushIdDetails(UserPushIdDetail userPushIdDetail)
        {
            if (userPushIdDetail == null)
                throw new ArgumentNullException(nameof(userPushIdDetail));

            //if (userPushIdDetail is IEntityForCaching)
            //    throw new ArgumentException("Cacheable entities are not supported by Entity Framework");

            _userPushIdDetailRepository.Insert(userPushIdDetail);

            //_cacheManager.RemoveByPattern(STORES_PATTERN_KEY);

            //event notification
            //_eventPublisher.EntityInserted(store);
        }
    }
}
