﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB.Agent;

namespace Nop.Data.Mapping.NB.Agent
{
    /// <summary>
    /// Represents a AgentOrderGeoLocation configuration
    /// </summary>
    public partial class AgentOrderGeoLocationMap : NopEntityTypeConfiguration<AgentOrderGeoLocation>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<AgentOrderGeoLocation> builder)
        {
            builder.ToTable(NopMappingDefaults.AgentOrderGeoLocationTable);
            builder.HasKey(mapping => mapping.Id);

            base.Configure(builder);
        }

        #endregion
    }
}
