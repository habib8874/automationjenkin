﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Html;
using Nop.Core.Domain.Vendors;
using Nop.Services.Events;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Core.Infrastructure;
using Nop.Core.Domain.NB;
using Nop.Services.NB.SignalR;
using Microsoft.AspNetCore.SignalR;
using Nop.Core.Domain.Messages;
using Nop.Services.Messages;
using static Nop.Services.NB.CommonNotification;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Nop.Services.Localization;
using Nop.Services.Configuration;
using Nop.Services.Logging;
using Nop.Core.Domain.NB.VendorTokens;
using Nop.Core.Domain.NB.Common;
using Nop.Services.NB.VendorTokens;

namespace Nop.Services.Orders
{
    /// <summary>
    /// Order service
    /// </summary>
    public partial class OrderService : IOrderService
    {
        #region Fields

        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<OrderItem> _orderItemRepository;
        private readonly IRepository<OrderNote> _orderNoteRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<RecurringPayment> _recurringPaymentRepository;

        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IAddressService _addressService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IRepository<OrderDetails> _orderDetailsRepository;
        private readonly IRepository<AgentEarning> _agentEarningRepository;
        private readonly IRepository<MerchantEarning> _merchantEarningRepository;
        private readonly ICommonService _commonService;
        private readonly IRepository<AgentOrderStatus> _agentOrderStatusRepository;
        private readonly IHubContext<SignalRHub> _signalRHubContext;
        private readonly ICustomerService _customerService;
        private readonly IWorkContext _workContext;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly ILogger _loger;
        private readonly INBVendorTokenService _nBVendorTokenService;

        #endregion

        #region Ctor

        public OrderService(IEventPublisher eventPublisher,
            IRepository<Customer> customerRepository,
            IRepository<Order> orderRepository,
            IRepository<OrderItem> orderItemRepository,
            IRepository<OrderNote> orderNoteRepository,
            IRepository<Product> productRepository,
            IRepository<RecurringPayment> recurringPaymentRepository,
            IRepository<Vendor> vendorRepository,
            IAddressService addressService,
            IGenericAttributeService genericAttributeService,
            IRepository<OrderDetails> orderDetailsRepository,
            IRepository<AgentEarning> agentEarningRepository,
            IRepository<MerchantEarning> merchantEarningRepository,
            ICommonService commonService,
            IRepository<AgentOrderStatus> agentOrderStatusRepository,
            IHubContext<SignalRHub> signalRHubContext,
            ICustomerService customerService,
            IWorkContext workContext,
            ILocalizationService localizationService,
            ISettingService settingService,
            ILogger loger,
            INBVendorTokenService nBVendorTokenService)
        {
            _eventPublisher = eventPublisher;
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
            _orderItemRepository = orderItemRepository;
            _orderNoteRepository = orderNoteRepository;
            _productRepository = productRepository;
            _recurringPaymentRepository = recurringPaymentRepository;
            _vendorRepository = vendorRepository;
            _addressService = addressService;
            _genericAttributeService = genericAttributeService;
            _orderDetailsRepository = orderDetailsRepository;
            _agentEarningRepository = agentEarningRepository;
            _merchantEarningRepository = merchantEarningRepository;
            _commonService = commonService;
            _agentOrderStatusRepository = agentOrderStatusRepository;
            _signalRHubContext = signalRHubContext;
            _customerService = customerService;
            _workContext = workContext;
            _localizationService = localizationService;
            _settingService = settingService;
            _loger = loger;
            _nBVendorTokenService = nBVendorTokenService;
        }

        #endregion

        #region Methods

        #region Orders

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="orderId">The order identifier</param>
        /// <returns>Order</returns>
        public virtual Order GetOrderById(int orderId)
        {
            if (orderId == 0)
                return null;

            return _orderRepository.GetById(orderId);
        }

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="customOrderNumber">The custom order number</param>
        /// <returns>Order</returns>
        public virtual Order GetOrderByCustomOrderNumber(string customOrderNumber)
        {
            if (string.IsNullOrEmpty(customOrderNumber))
                return null;

            return _orderRepository.Table.FirstOrDefault(o => o.CustomOrderNumber == customOrderNumber);
        }

        /// <summary>
        /// Get orders by identifiers
        /// </summary>
        /// <param name="orderIds">Order identifiers</param>
        /// <returns>Order</returns>
        public virtual IList<Order> GetOrdersByIds(int[] orderIds)
        {
            if (orderIds == null || orderIds.Length == 0)
                return new List<Order>();

            var query = from o in _orderRepository.Table
                        where orderIds.Contains(o.Id) && !o.Deleted
                        select o;
            var orders = query.ToList();
            //sort by passed identifiers
            var sortedOrders = new List<Order>();
            foreach (var id in orderIds)
            {
                var order = orders.Find(x => x.Id == id);
                if (order != null)
                    sortedOrders.Add(order);
            }

            return sortedOrders;
        }

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="orderGuid">The order identifier</param>
        /// <returns>Order</returns>
        public virtual Order GetOrderByGuid(Guid orderGuid)
        {
            if (orderGuid == Guid.Empty)
                return null;

            var query = from o in _orderRepository.Table
                        where o.OrderGuid == orderGuid
                        select o;
            var order = query.FirstOrDefault();
            return order;
        }

        /// <summary>
        /// Deletes an order
        /// </summary>
        /// <param name="order">The order</param>
        public virtual void DeleteOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            order.Deleted = true;
            UpdateOrder(order);

            //event notification
            _eventPublisher.EntityDeleted(order);
        }

        /// <summary>
        /// Search orders
        /// </summary>
        /// <param name="storeId">Store identifier; 0 to load all orders</param>
        /// <param name="vendorId">Vendor identifier; null to load all orders</param>
        /// <param name="customerId">Customer identifier; 0 to load all orders</param>
        /// <param name="productId">Product identifier which was purchased in an order; 0 to load all orders</param>
        /// <param name="affiliateId">Affiliate identifier; 0 to load all orders</param>
        /// <param name="billingCountryId">Billing country identifier; 0 to load all orders</param>
        /// <param name="warehouseId">Warehouse identifier, only orders with products from a specified warehouse will be loaded; 0 to load all orders</param>
        /// <param name="paymentMethodSystemName">Payment method system name; null to load all records</param>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="osIds">Order status identifiers; null to load all orders</param>
        /// <param name="psIds">Payment status identifiers; null to load all orders</param>
        /// <param name="ssIds">Shipping status identifiers; null to load all orders</param>
        /// <param name="billingPhone">Billing phone. Leave empty to load all records.</param>
        /// <param name="billingEmail">Billing email. Leave empty to load all records.</param>
        /// <param name="billingLastName">Billing last name. Leave empty to load all records.</param>
        /// <param name="orderNotes">Search in order notes. Leave empty to load all records.</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="getOnlyTotalCount">A value in indicating whether you want to load only total number of records. Set to "true" if you don't want to load data from database</param>
        /// <returns>Orders</returns>
        public virtual IPagedList<Order> SearchOrders(int storeId = 0,
            int vendorId = 0, IList<int> vendorIds = null, int customerId = 0,
            int productId = 0, int affiliateId = 0, int warehouseId = 0,
            int billingCountryId = 0, string paymentMethodSystemName = null,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            List<int> osIds = null, List<int> psIds = null, List<int> ssIds = null,
            string billingPhone = null, string billingEmail = null, string billingLastName = "",
            string orderNotes = null, int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false, int agentId = 0, string billingFullName = "", bool isForAgentEarning = false, bool isDeliverySlot = false, List<int> slotIds = null)
        {
            var query = _orderRepository.Table;
            if (storeId > 0)
                query = query.Where(o => o.StoreId == storeId);
            if (vendorIds != null && vendorIds.Any())
                query = query.Where(o => o.OrderItems.Any(orderItem => vendorIds.Contains(orderItem.Product.VendorId)));
            if (vendorId > 0)
                query = query.Where(o => o.OrderItems.Any(orderItem => orderItem.Product.VendorId == vendorId));
            if (customerId > 0)
                query = query.Where(o => o.CustomerId == customerId);
            if (productId > 0)
                query = query.Where(o => o.OrderItems.Any(orderItem => orderItem.ProductId == productId));

            // get order detail by agent
            if (agentId > 0 || isForAgentEarning)
            {
                var agentOrderStatusRepository = EngineContext.Current.Resolve<IRepository<AgentOrderStatus>>();
                query = from q in query
                        join a in agentOrderStatusRepository.Table on q.Id equals a.OrderId
                        where agentId == 0 || a.AgentId == agentId
                        select q;
            }

            // get order detail by DeliverySlot
            if (isDeliverySlot)
            {
                var deliverySlotBookingRepository = EngineContext.Current.Resolve<IRepository<DeliverySlotBooking>>();

                if (slotIds != null && slotIds.Any())
                    query = from q in query
                            join ds in deliverySlotBookingRepository.Table on q.Id equals ds.OrderId
                            where slotIds.Contains(ds.SlotId) && !ds.Deleted
                            select q;
                else
                    query = from q in query
                            join ds in deliverySlotBookingRepository.Table on q.Id equals ds.OrderId
                            where !ds.Deleted
                            select q;
            }

            if (warehouseId > 0)
            {
                var manageStockInventoryMethodId = (int)ManageInventoryMethod.ManageStock;
                query = query
                    .Where(o => o.OrderItems
                    .Any(orderItem =>
                        //"Use multiple warehouses" enabled
                        //we search in each warehouse
                        (orderItem.Product.ManageInventoryMethodId == manageStockInventoryMethodId &&
                        orderItem.Product.UseMultipleWarehouses &&
                        orderItem.Product.ProductWarehouseInventory.Any(pwi => pwi.WarehouseId == warehouseId))
                        ||
                        //"Use multiple warehouses" disabled
                        //we use standard "warehouse" property
                        ((orderItem.Product.ManageInventoryMethodId != manageStockInventoryMethodId ||
                        !orderItem.Product.UseMultipleWarehouses) &&
                        orderItem.Product.WarehouseId == warehouseId)));
            }

            if (billingCountryId > 0)
                query = query.Where(o => o.BillingAddress != null && o.BillingAddress.CountryId == billingCountryId);
            if (!string.IsNullOrEmpty(paymentMethodSystemName))
                query = query.Where(o => o.PaymentMethodSystemName == paymentMethodSystemName);
            if (affiliateId > 0)
                query = query.Where(o => o.AffiliateId == affiliateId);
            if (createdFromUtc.HasValue)
                query = query.Where(o => createdFromUtc.Value <= o.CreatedOnUtc);
            if (createdToUtc.HasValue)
                query = query.Where(o => createdToUtc.Value >= o.CreatedOnUtc);
            if (osIds != null && osIds.Any())
                query = query.Where(o => osIds.Contains(o.OrderStatusId));
            if (psIds != null && psIds.Any())
                query = query.Where(o => psIds.Contains(o.PaymentStatusId));
            if (ssIds != null && ssIds.Any())
                query = query.Where(o => ssIds.Contains(o.ShippingStatusId));
            if (!string.IsNullOrEmpty(billingPhone))
                query = query.Where(o => o.BillingAddress != null && !string.IsNullOrEmpty(o.BillingAddress.PhoneNumber) && o.BillingAddress.PhoneNumber.Contains(billingPhone));
            if (!string.IsNullOrEmpty(billingEmail))
                query = query.Where(o => o.BillingAddress != null && !string.IsNullOrEmpty(o.BillingAddress.Email) && o.BillingAddress.Email.Contains(billingEmail));
            if (!string.IsNullOrEmpty(billingLastName))
                query = query.Where(o => o.BillingAddress != null && !string.IsNullOrEmpty(o.BillingAddress.LastName) && o.BillingAddress.LastName.Contains(billingLastName));
            if (!string.IsNullOrEmpty(billingFullName))
                query = query.Where(o => o.BillingAddress != null && !string.IsNullOrEmpty(o.BillingAddress.FirstName) && !string.IsNullOrEmpty(o.BillingAddress.LastName) && (o.BillingAddress.FirstName + o.BillingAddress.LastName).Contains(billingFullName));
            if (!string.IsNullOrEmpty(orderNotes))
                query = query.Where(o => o.OrderNotes.Any(on => on.Note.Contains(orderNotes)));
            query = query.Where(o => !o.Deleted);
            query = query.OrderByDescending(o => o.CreatedOnUtc);

            //database layer paging
            return new PagedList<Order>(query, pageIndex, pageSize, getOnlyTotalCount);
        }

        /// <summary>
        /// Inserts an order
        /// </summary>
        /// <param name="order">Order</param>
        public virtual void InsertOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            _orderRepository.Insert(order);
            _commonService.ClearCache(order.StoreId, string.Format("API.Cache.Order-{0}", order.StoreId));

            //event notification
            _eventPublisher.EntityInserted(order);
        }

        /// <summary>
        /// Updates the order
        /// </summary>
        /// <param name="order">The order</param>
        public virtual void UpdateOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            _orderRepository.Update(order);
            _commonService.ClearCache(order.StoreId, string.Format("API.Cache.Order-{0}", order.StoreId));


            //event notification
            _eventPublisher.EntityUpdated(order);
        }

        /// <summary>
        /// Get an order by authorization transaction ID and payment method system name
        /// </summary>
        /// <param name="authorizationTransactionId">Authorization transaction ID</param>
        /// <param name="paymentMethodSystemName">Payment method system name</param>
        /// <returns>Order</returns>
        public virtual Order GetOrderByAuthorizationTransactionIdAndPaymentMethod(string authorizationTransactionId,
            string paymentMethodSystemName)
        {
            var query = _orderRepository.Table;
            if (!string.IsNullOrWhiteSpace(authorizationTransactionId))
                query = query.Where(o => o.AuthorizationTransactionId == authorizationTransactionId);

            if (!string.IsNullOrWhiteSpace(paymentMethodSystemName))
                query = query.Where(o => o.PaymentMethodSystemName == paymentMethodSystemName);

            query = query.OrderByDescending(o => o.CreatedOnUtc);
            var order = query.FirstOrDefault();
            return order;
        }

        /// <summary>
        /// Parse tax rates
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="taxRatesStr"></param>
        /// <returns>Rates</returns>
        public virtual SortedDictionary<decimal, decimal> ParseTaxRates(Order order, string taxRatesStr)
        {
            var taxRatesDictionary = new SortedDictionary<decimal, decimal>();

            if (string.IsNullOrEmpty(taxRatesStr))
                return taxRatesDictionary;

            var lines = taxRatesStr.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var line in lines)
            {
                if (string.IsNullOrEmpty(line.Trim()))
                    continue;

                var taxes = line.Split(':');
                if (taxes.Length != 2)
                    continue;

                try
                {
                    var taxRate = decimal.Parse(taxes[0].Trim(), CultureInfo.InvariantCulture);
                    var taxValue = decimal.Parse(taxes[1].Trim(), CultureInfo.InvariantCulture);
                    taxRatesDictionary.Add(taxRate, taxValue);
                }
                catch
                {
                    // ignored
                }
            }

            //add at least one tax rate (0%)
            if (!taxRatesDictionary.Any())
                taxRatesDictionary.Add(decimal.Zero, decimal.Zero);

            return taxRatesDictionary;
        }

        /// <summary>
        /// Gets a value indicating whether an order has items to be added to a shipment
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether an order has items to be added to a shipment</returns>
        public virtual bool HasItemsToAddToShipment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            foreach (var orderItem in order.OrderItems)
            {
                //we can ship only shippable products
                if (!orderItem.Product.IsShipEnabled)
                    continue;

                var totalNumberOfItemsCanBeAddedToShipment = GetTotalNumberOfItemsCanBeAddedToShipment(orderItem);
                if (totalNumberOfItemsCanBeAddedToShipment <= 0)
                    continue;

                //yes, we have at least one item to create a new shipment
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets a value indicating whether an order has items to ship
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether an order has items to ship</returns>
        public virtual bool HasItemsToShip(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            foreach (var orderItem in order.OrderItems)
            {
                //we can ship only shippable products
                if (!orderItem.Product.IsShipEnabled)
                    continue;

                var totalNumberOfNotYetShippedItems = GetTotalNumberOfNotYetShippedItems(orderItem);
                if (totalNumberOfNotYetShippedItems <= 0)
                    continue;

                //yes, we have at least one item to ship
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets a value indicating whether an order has items to deliver
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether an order has items to deliver</returns>
        public virtual bool HasItemsToDeliver(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            foreach (var orderItem in order.OrderItems)
            {
                //we can ship only shippable products
                if (!orderItem.Product.IsShipEnabled)
                    continue;

                var totalNumberOfShippedItems = GetTotalNumberOfShippedItems(orderItem);
                var totalNumberOfDeliveredItems = GetTotalNumberOfDeliveredItems(orderItem);
                if (totalNumberOfShippedItems <= totalNumberOfDeliveredItems)
                    continue;

                //yes, we have at least one item to deliver
                return true;
            }

            return false;
        }

        #endregion

        #region Orders items

        /// <summary>
        /// Gets an order item
        /// </summary>
        /// <param name="orderItemId">Order item identifier</param>
        /// <returns>Order item</returns>
        public virtual OrderItem GetOrderItemById(int orderItemId)
        {
            if (orderItemId == 0)
                return null;

            return _orderItemRepository.GetById(orderItemId);
        }

        /// <summary>
        /// Gets an item
        /// </summary>
        /// <param name="orderItemGuid">Order identifier</param>
        /// <returns>Order item</returns>
        public virtual OrderItem GetOrderItemByGuid(Guid orderItemGuid)
        {
            if (orderItemGuid == Guid.Empty)
                return null;

            var query = from orderItem in _orderItemRepository.Table
                        where orderItem.OrderItemGuid == orderItemGuid
                        select orderItem;
            var item = query.FirstOrDefault();
            return item;
        }

        /// <summary>
        /// Gets all downloadable order items
        /// </summary>
        /// <param name="customerId">Customer identifier; null to load all records</param>
        /// <returns>Order items</returns>
        public virtual IList<OrderItem> GetDownloadableOrderItems(int customerId)
        {
            if (customerId == 0)
                throw new ArgumentOutOfRangeException(nameof(customerId));

            var query = from orderItem in _orderItemRepository.Table
                        join o in _orderRepository.Table on orderItem.OrderId equals o.Id
                        join p in _productRepository.Table on orderItem.ProductId equals p.Id
                        where customerId == o.CustomerId &&
                        p.IsDownload &&
                        !o.Deleted
                        orderby o.CreatedOnUtc descending, orderItem.Id
                        select orderItem;

            var orderItems = query.ToList();
            return orderItems;
        }

        /// <summary>
        /// Delete an order item
        /// </summary>
        /// <param name="orderItem">The order item</param>
        public virtual void DeleteOrderItem(OrderItem orderItem)
        {
            if (orderItem == null)
                throw new ArgumentNullException(nameof(orderItem));

            _orderItemRepository.Delete(orderItem);
            //event notification
            _eventPublisher.EntityDeleted(orderItem);
        }

        /// <summary>
        /// Gets a total number of items in all shipments
        /// </summary>
        /// <param name="orderItem">Order item</param>
        /// <returns>Total number of items in all shipments</returns>
        public virtual int GetTotalNumberOfItemsInAllShipment(OrderItem orderItem)
        {
            if (orderItem == null)
                throw new ArgumentNullException(nameof(orderItem));

            var totalInShipments = 0;
            var shipments = orderItem.Order.Shipments.ToList();
            for (var i = 0; i < shipments.Count; i++)
            {
                var shipment = shipments[i];
                var si = shipment.ShipmentItems
                    .FirstOrDefault(x => x.OrderItemId == orderItem.Id);
                if (si != null)
                {
                    totalInShipments += si.Quantity;
                }
            }

            return totalInShipments;
        }

        /// <summary>
        /// Gets a total number of already items which can be added to new shipments
        /// </summary>
        /// <param name="orderItem">Order item</param>
        /// <returns>Total number of already delivered items which can be added to new shipments</returns>
        public virtual int GetTotalNumberOfItemsCanBeAddedToShipment(OrderItem orderItem)
        {
            if (orderItem == null)
                throw new ArgumentNullException(nameof(orderItem));

            var totalInShipments = GetTotalNumberOfItemsInAllShipment(orderItem);

            var qtyOrdered = orderItem.Quantity;
            var qtyCanBeAddedToShipmentTotal = qtyOrdered - totalInShipments;
            if (qtyCanBeAddedToShipmentTotal < 0)
                qtyCanBeAddedToShipmentTotal = 0;

            return qtyCanBeAddedToShipmentTotal;
        }

        /// <summary>
        /// Gets a total number of not yet shipped items (but added to shipments)
        /// </summary>
        /// <param name="orderItem">Order item</param>
        /// <returns>Total number of not yet shipped items (but added to shipments)</returns>
        public virtual int GetTotalNumberOfNotYetShippedItems(OrderItem orderItem)
        {
            if (orderItem == null)
                throw new ArgumentNullException(nameof(orderItem));

            var result = 0;
            var shipments = orderItem.Order.Shipments.ToList();
            for (var i = 0; i < shipments.Count; i++)
            {
                var shipment = shipments[i];
                if (shipment.ShippedDateUtc.HasValue)
                    //already shipped
                    continue;

                var si = shipment.ShipmentItems
                    .FirstOrDefault(x => x.OrderItemId == orderItem.Id);
                if (si != null)
                {
                    result += si.Quantity;
                }
            }

            return result;
        }

        /// <summary>
        /// Gets a total number of already shipped items
        /// </summary>
        /// <param name="orderItem">Order item</param>
        /// <returns>Total number of already shipped items</returns>
        public virtual int GetTotalNumberOfShippedItems(OrderItem orderItem)
        {
            if (orderItem == null)
                throw new ArgumentNullException(nameof(orderItem));

            var result = 0;
            var shipments = orderItem.Order.Shipments.ToList();
            for (var i = 0; i < shipments.Count; i++)
            {
                var shipment = shipments[i];
                if (!shipment.ShippedDateUtc.HasValue)
                    //not shipped yet
                    continue;

                var si = shipment.ShipmentItems
                    .FirstOrDefault(x => x.OrderItemId == orderItem.Id);
                if (si != null)
                {
                    result += si.Quantity;
                }
            }

            return result;
        }

        /// <summary>
        /// Gets a total number of already delivered items
        /// </summary>
        /// <param name="orderItem">Order  item</param>
        /// <returns>Total number of already delivered items</returns>
        public virtual int GetTotalNumberOfDeliveredItems(OrderItem orderItem)
        {
            if (orderItem == null)
                throw new ArgumentNullException(nameof(orderItem));

            var result = 0;
            var shipments = orderItem.Order.Shipments.ToList();
            for (var i = 0; i < shipments.Count; i++)
            {
                var shipment = shipments[i];
                if (!shipment.DeliveryDateUtc.HasValue)
                    //not delivered yet
                    continue;

                var si = shipment.ShipmentItems
                    .FirstOrDefault(x => x.OrderItemId == orderItem.Id);
                if (si != null)
                {
                    result += si.Quantity;
                }
            }

            return result;
        }

        /// <summary>
        /// Gets all order items by orderid
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns>Order items</returns>
        public virtual IList<OrderItem> GetOrderItemsByOrderId(int orderId)
        {
            if (orderId == 0)
                return null;

            var query = from orderItem in _orderItemRepository.Table
                        where orderItem.OrderId == orderId
                        select orderItem;

            var orderItems = query.ToList();
            return orderItems;
        }

        #endregion

        #region Orders notes

        /// <summary>
        /// Gets an order note
        /// </summary>
        /// <param name="orderNoteId">The order note identifier</param>
        /// <returns>Order note</returns>
        public virtual OrderNote GetOrderNoteById(int orderNoteId)
        {
            if (orderNoteId == 0)
                return null;

            return _orderNoteRepository.GetById(orderNoteId);
        }

        /// <summary>
        /// Deletes an order note
        /// </summary>
        /// <param name="orderNote">The order note</param>
        public virtual void DeleteOrderNote(OrderNote orderNote)
        {
            if (orderNote == null)
                throw new ArgumentNullException(nameof(orderNote));

            _orderNoteRepository.Delete(orderNote);

            //event notification
            _eventPublisher.EntityDeleted(orderNote);
        }

        /// <summary>
        /// Formats the order note text
        /// </summary>
        /// <param name="orderNote">Order note</param>
        /// <returns>Formatted text</returns>
        public virtual string FormatOrderNoteText(OrderNote orderNote)
        {
            if (orderNote == null)
                throw new ArgumentNullException(nameof(orderNote));

            var text = orderNote.Note;

            if (string.IsNullOrEmpty(text))
                return string.Empty;

            text = HtmlHelper.FormatText(text, false, true, false, false, false, false);

            return text;
        }

        #endregion

        #region Recurring payments

        /// <summary>
        /// Deletes a recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        public virtual void DeleteRecurringPayment(RecurringPayment recurringPayment)
        {
            if (recurringPayment == null)
                throw new ArgumentNullException(nameof(recurringPayment));

            recurringPayment.Deleted = true;
            UpdateRecurringPayment(recurringPayment);

            //event notification
            _eventPublisher.EntityDeleted(recurringPayment);
        }

        /// <summary>
        /// Gets a recurring payment
        /// </summary>
        /// <param name="recurringPaymentId">The recurring payment identifier</param>
        /// <returns>Recurring payment</returns>
        public virtual RecurringPayment GetRecurringPaymentById(int recurringPaymentId)
        {
            if (recurringPaymentId == 0)
                return null;

            return _recurringPaymentRepository.GetById(recurringPaymentId);
        }

        /// <summary>
        /// Inserts a recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        public virtual void InsertRecurringPayment(RecurringPayment recurringPayment)
        {
            if (recurringPayment == null)
                throw new ArgumentNullException(nameof(recurringPayment));

            _recurringPaymentRepository.Insert(recurringPayment);

            //event notification
            _eventPublisher.EntityInserted(recurringPayment);
        }

        /// <summary>
        /// Updates the recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        public virtual void UpdateRecurringPayment(RecurringPayment recurringPayment)
        {
            if (recurringPayment == null)
                throw new ArgumentNullException(nameof(recurringPayment));

            _recurringPaymentRepository.Update(recurringPayment);

            //event notification
            _eventPublisher.EntityUpdated(recurringPayment);
        }

        /// <summary>
        /// Search recurring payments
        /// </summary>
        /// <param name="storeId">The store identifier; 0 to load all records</param>
        /// <param name="customerId">The customer identifier; 0 to load all records</param>
        /// <param name="initialOrderId">The initial order identifier; 0 to load all records</param>
        /// <param name="initialOrderStatus">Initial order status identifier; null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Recurring payments</returns>
        public virtual IPagedList<RecurringPayment> SearchRecurringPayments(int storeId = 0,
            int customerId = 0, int initialOrderId = 0, OrderStatus? initialOrderStatus = null,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            int? initialOrderStatusId = null;
            if (initialOrderStatus.HasValue)
                initialOrderStatusId = (int)initialOrderStatus.Value;

            var query1 = from rp in _recurringPaymentRepository.Table
                         join c in _customerRepository.Table on rp.InitialOrder.CustomerId equals c.Id
                         where
                         !rp.Deleted &&
                         (showHidden || !rp.InitialOrder.Deleted) &&
                         (showHidden || !c.Deleted) &&
                         (showHidden || rp.IsActive) &&
                         (customerId == 0 || rp.InitialOrder.CustomerId == customerId) &&
                         (storeId == 0 || rp.InitialOrder.StoreId == storeId) &&
                         (initialOrderId == 0 || rp.InitialOrder.Id == initialOrderId) &&
                         (!initialOrderStatusId.HasValue || initialOrderStatusId.Value == 0 ||
                          rp.InitialOrder.OrderStatusId == initialOrderStatusId.Value)
                         select rp.Id;

            var query2 = from rp in _recurringPaymentRepository.Table
                         where query1.Contains(rp.Id)
                         orderby rp.StartDateUtc, rp.Id
                         select rp;

            var recurringPayments = new PagedList<RecurringPayment>(query2, pageIndex, pageSize);
            return recurringPayments;
        }

        #endregion

        #endregion

        #region Custom code from v4.0
        /// <summary>
        /// Gets an order vendor details
        /// </summary>
        /// <param name="orderId">The order identifier</param>
        /// <returns>Order</returns>
        public virtual OrderVendorDetails GetOrderVendorDetails(int orderId)
        {
            if (orderId == 0)
                return null;

            OrderVendorDetails orderVendorDetails = new OrderVendorDetails();

            var queryOrder = _orderRepository.Table;
            if (orderId > 0)
                queryOrder = queryOrder.Where(o => o.Id == orderId);

            int vendorId = queryOrder.Select(x => x.OrderItems.FirstOrDefault().Product.VendorId).FirstOrDefault();


            var vendor = _vendorRepository.Table.FirstOrDefault(x => x.Id == vendorId);
            if (vendor != null)
            {
                orderVendorDetails.MerchantName = vendor.Name;
                string venLtLngStr = vendor.Geolocation;

                if (venLtLngStr.Any())
                {
                    string[] venLtLngStrArr = venLtLngStr.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
                    if (venLtLngStrArr.Length > 0)
                    {
                        orderVendorDetails.PickupLat = venLtLngStrArr[0].ToString();
                        orderVendorDetails.PickupLong = venLtLngStrArr[1].ToString();
                    }
                }
                var venAdd = _addressService.GetAddressById(vendor.AddressId);
                if (venAdd != null)
                {
                    orderVendorDetails.PickupLocation = (venAdd.Address1 + "," + venAdd.Address2 + "," + venAdd.City).ToString().Replace(",,", ",");
                    orderVendorDetails.PickupMobileNo = venAdd.PhoneNumber;
                    orderVendorDetails.ISDCode = venAdd.Country?.ISDCode;
                }
            }
            var orderTotal = queryOrder.Where(o => o.Id == orderId).Select(s => s.OrderTotal).First();

            orderVendorDetails.OrderTotal = orderTotal;

            return orderVendorDetails;
        }

        /// <summary>
        /// get vendors and customer details of order
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public virtual Tuple<OrderVendorDetails, OrderCustomerDetails> GetOrderVendorAndCustomerDetails(Order order)
        {
            var orderVendorDetails = new OrderVendorDetails();
            int vendorId = order.OrderItems.FirstOrDefault().Product.VendorId;
            var vendor = _vendorRepository.Table.FirstOrDefault(x => x.Id == vendorId);
            if (vendor != null)
            {
                orderVendorDetails.MerchantName = vendor.Name;
                string venLtLngStr = vendor.Geolocation;

                if (venLtLngStr.Any())
                {
                    string[] venLtLngStrArr = venLtLngStr.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
                    if (venLtLngStrArr.Length > 0)
                    {
                        orderVendorDetails.PickupLat = venLtLngStrArr[0].ToString();
                        orderVendorDetails.PickupLong = venLtLngStrArr[1].ToString();
                    }
                }
                var venAdd = _addressService.GetAddressById(vendor.AddressId);
                if (venAdd != null)
                {
                    orderVendorDetails.PickupLocation = (venAdd.Address1 + "," + venAdd.Address2 + "," + venAdd.City).ToString().Replace(",,", ",");
                    orderVendorDetails.PickupMobileNo = venAdd.PhoneNumber;
                    orderVendorDetails.ISDCode = venAdd.Country?.ISDCode;
                }
            }

            orderVendorDetails.OrderTotal = order.OrderTotal;

            //customer
            var orderCustomerDetails = new OrderCustomerDetails();
            var custShipping = order.Customer.ShippingAddress;
            if (custShipping == null)
            {
                custShipping = order.Customer.BillingAddress;
            }
            if (custShipping == null)
            {
                custShipping = order.ShippingAddress;
            }
            orderCustomerDetails.CustomerName = order.Customer.GetFullName();
            orderCustomerDetails.CustomerMobileNo = _genericAttributeService.GetAttribute<string>(order.Customer, NopCustomerDefaults.PhoneAttribute, order.StoreId, "");
            ;
            if (custShipping.Address1 != null)
                orderCustomerDetails.CustomerLocation = custShipping.Address1;
            if (custShipping.Address2 != null)
                orderCustomerDetails.CustomerLocation = orderCustomerDetails.CustomerLocation + "," + custShipping.Address2;
            if (custShipping.City != null)
                orderCustomerDetails.CustomerLocation = orderCustomerDetails.CustomerLocation + "," + custShipping.City;
            if (custShipping.StateProvince != null)
            {
                if (custShipping.StateProvince.Name != null)
                    orderCustomerDetails.CustomerLocation = orderCustomerDetails.CustomerLocation + "," + custShipping.StateProvince.Name;
            }
            if (custShipping.Country != null)
            {
                if (custShipping.Country.Name != null)
                    orderCustomerDetails.CustomerLocation = orderCustomerDetails.CustomerLocation + "," + custShipping.Country.Name;
            }
            orderCustomerDetails.DropLat = custShipping.Latitude.HasValue ? custShipping.Latitude.Value.ToString() : "0";
            orderCustomerDetails.DropLong = custShipping.Longitude.HasValue ? custShipping.Longitude.Value.ToString() : "0";

            return new Tuple<OrderVendorDetails, OrderCustomerDetails>(orderVendorDetails, orderCustomerDetails);
        }


        /// <summary>
        /// Get Order Quantity and Product Name
        /// </summary>
        /// <param name="orderId">The order identifier</param>
        /// <returns>Order</returns>
        public List<PushOrderItems> GetOrdersDetailsNameandQty(int orderId)
        {
            if (orderId == 0)
                return null;
            List<PushOrderItems> listPushOrderItems = new List<PushOrderItems>();
            listPushOrderItems = (from order in _orderItemRepository.Table
                                  join Product in _productRepository.Table
                                  on order.ProductId equals Product.Id
                                  where order.OrderId == orderId
                                  select new PushOrderItems
                                  {
                                      ItemQty = order.Quantity.ToString(),
                                      ItemName = Product.Name
                                  }).ToList();

            return listPushOrderItems;


        }

        public virtual OrderCustomerDetails GetOrderCustomerDetails(int orderId)
        {
            if (orderId == 0)
                return null;

            OrderCustomerDetails orderCustomerDetails = new OrderCustomerDetails();
            var queryOrder = _orderRepository.Table;
            if (orderId > 0)
                queryOrder = queryOrder.Where(o => o.Id == orderId);

            int customerId = queryOrder.Select(s => s.CustomerId).ToList().First();
            string custName = string.Empty;
            var customerInfo = _genericAttributeService.GetAttributesForEntity(customerId, "Customer").ToList();
            foreach (var item in customerInfo)
            {
                if (item.Key == "FirstName")
                {
                    custName = item.Value;
                }
            }
            foreach (var item in customerInfo)
            {
                if (item.Key == "LastName")
                {
                    custName = custName + " " + item.Value;
                }
            }
            //string custName = _customerRepository.Table.Where(c => c.Id == customerId).Select(s => s.).First();

            string custPhNo = _genericAttributeService.GetAttributesForEntity(customerId, "Customer").Where(a => a.Key == "Phone").Select(s => s.Value).FirstOrDefault();

            var custShipping = _customerRepository.Table.Where(c => c.Id == customerId).Select(s => s.ShippingAddress).FirstOrDefault();

            if (custShipping == null)
            {
                custShipping = _customerRepository.Table.Where(c => c.Id == customerId).Select(s => s.BillingAddress).FirstOrDefault();
            }
            if (custShipping == null)
            {
                var addressId = queryOrder.Where(o => o.Id == orderId).FirstOrDefault().ShippingAddressId;
                custShipping = _addressService.GetAddressById(addressId.Value);
            }
            orderCustomerDetails.CustomerName = custName;
            orderCustomerDetails.CustomerMobileNo = custPhNo;
            if (custShipping.Address1 != null)
                orderCustomerDetails.CustomerLocation = custShipping.Address1;
            if (custShipping.Address2 != null)
                orderCustomerDetails.CustomerLocation = orderCustomerDetails.CustomerLocation + "," + custShipping.Address2;
            if (custShipping.City != null)
                orderCustomerDetails.CustomerLocation = orderCustomerDetails.CustomerLocation + "," + custShipping.City;
            if (custShipping.StateProvince != null)
            {
                if (custShipping.StateProvince.Name != null)
                    orderCustomerDetails.CustomerLocation = orderCustomerDetails.CustomerLocation + "," + custShipping.StateProvince.Name;
            }
            if (custShipping.Country != null)
            {
                if (custShipping.Country.Name != null)
                    orderCustomerDetails.CustomerLocation = orderCustomerDetails.CustomerLocation + "," + custShipping.Country.Name;
            }
            orderCustomerDetails.DropLat = custShipping.Latitude.HasValue ? custShipping.Latitude.Value.ToString() : "0";
            orderCustomerDetails.DropLong = custShipping.Longitude.HasValue ? custShipping.Longitude.Value.ToString() : "0";


            return orderCustomerDetails;
        }

        public virtual List<Order> PendingOrderList(int storeId = 0,
            int vendorId = 0, IList<int> vendorIds = null)
        {
            var query = _orderRepository.Table;
            if (storeId > 0)
                query = query.Where(o => o.StoreId == storeId);
            if (vendorIds != null && vendorIds.Any())
            {
                query = query
                    .Where(o => o.OrderItems
                    .Any(orderItem => vendorIds.Contains(orderItem.Product.VendorId)));
            }
            if (vendorId > 0)
            {
                query = query
                    .Where(o => o.OrderItems
                    .Any(orderItem => orderItem.Product.VendorId == vendorId));
            }
            query = query.Where(o => !o.Deleted && o.OrderStatusId == 10);
            query = query.OrderByDescending(o => o.CreatedOnUtc);

            //database layer paging
            return query.ToList();
        }

        /// <summary>
        /// insert orderDetails
        /// </summary>
        /// <param name="orderDetails"></param>
        public virtual void InsertOrderDetails(OrderDetails orderDetails)
        {
            if (orderDetails == null)
                throw new ArgumentNullException(nameof(orderDetails));
            var obj = _orderDetailsRepository.Table.Where(x => x.CustomerId == orderDetails.CustomerId && !x.IsOrderComplete).FirstOrDefault();
            if (obj != null)
            {
                obj.OrderType = orderDetails.OrderType;
                obj.OrderNote = orderDetails.OrderNote;
                obj.EnteredLocation = orderDetails.EnteredLocation;
                obj.IsOrderComplete = orderDetails.IsOrderComplete;
                UpdateOrderDetails(obj);
            }
            else
            {
                _orderDetailsRepository.Insert(orderDetails);
                //event notification
                _eventPublisher.EntityInserted(orderDetails);
            }
            _commonService.ClearCache(Convert.ToInt32(orderDetails.StoreId), string.Format("API.Cache.Order-{0}", orderDetails.StoreId));

        }

        /// <summary>
        /// Updates the orderDetails
        /// </summary>
        /// <param name="orderDetails">The order details</param>
        public virtual void UpdateOrderDetails(OrderDetails orderDetails)
        {
            if (orderDetails == null)
                throw new ArgumentNullException(nameof(orderDetails));

            _orderDetailsRepository.Update(orderDetails);
            _commonService.ClearCache(Convert.ToInt32(orderDetails.StoreId), string.Format("API.Cache.Order-{0}", orderDetails.StoreId));
            //event notification
            _eventPublisher.EntityUpdated(orderDetails);
        }

        public virtual OrderDetails GetOrderDetails(int orderId = 0, int customerId = 0, int storeId = 0, bool? isOrderComplete = null)
        {
            var query = _orderDetailsRepository.Table;
            if (orderId > 0)
            {
                query = from od in query
                        join o in _orderRepository.Table
                        on od.Id equals o.OrderDetailId
                        where o.Id == orderId
                        select od;
            }

            if (customerId > 0)
                query = query.Where(x => x.CustomerId == customerId);
            if (storeId > 0)
                query = query.Where(x => x.StoreId == storeId);
            if (isOrderComplete != null)
                query = query.Where(x => x.IsOrderComplete == isOrderComplete);

            return query.OrderByDescending(x => x.Id).FirstOrDefault();
        }
        #endregion

        #region Agent Earning
        /// <summary>
        /// get agent earning
        /// </summary>
        /// <param name="id"></param>
        /// <param name="agentId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public virtual AgentEarning GetAgentEarning(int id = 0, int agentId = 0, int orderId = 0)
        {
            if (id == 0 && agentId == 0 && orderId == 0)
                return null;

            var objAgentEarning = from ag in _agentEarningRepository.Table
                                  where (id == 0 || ag.Id == id)
                                  && (agentId == 0 || ag.AgentId == agentId)
                                  && (orderId == 0 || ag.OrderId == orderId)
                                  select ag;
            return objAgentEarning.FirstOrDefault();
        }

        /// <summary>
        /// insert or update agent earning
        /// </summary>
        /// <param name="agentEarning"></param>
        public virtual void InsertUpdateAgentEarning(AgentEarning agentEarning)
        {
            if (agentEarning == null)
                throw new ArgumentNullException(nameof(agentEarning));
            var obj = _agentEarningRepository.Table.Where(x => x.AgentId == agentEarning.AgentId && x.OrderId == agentEarning.OrderId).FirstOrDefault();
            if (obj != null)
            {
                obj.PayableAmount = agentEarning.PayableAmount;
                obj.PaidAmount = agentEarning.PaidAmount;

                _agentEarningRepository.Update(obj);
                var storeId = (from cus in _customerRepository.Table
                               where cus.Id == agentEarning.AgentId
                               select cus.RegisteredInStoreId).FirstOrDefault();
                _commonService.ClearCacheAgent(storeId, string.Format("API.Cache.AgentEarning-{0}", storeId));

                //event notification
                _eventPublisher.EntityUpdated(obj);
            }
            else
            {
                _agentEarningRepository.Insert(agentEarning);
                var storeId = (from cus in _customerRepository.Table
                               where cus.Id == agentEarning.AgentId
                               select cus.RegisteredInStoreId).FirstOrDefault();
                _commonService.ClearCacheAgent(storeId, string.Format("API.Cache.AgentEarning-{0}", storeId));
                //event notification
                _eventPublisher.EntityInserted(agentEarning);
            }
        }

        /// <summary>
        /// get agent earning amount details
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="vendorId"></param>
        /// <param name="customerId"></param>
        /// <param name="createdFromUtc"></param>
        /// <param name="createdToUtc"></param>
        /// <param name="osIds"></param>
        /// <param name="billingEmail"></param>
        /// <param name="agentId"></param>
        /// <returns></returns>
        public virtual Tuple<decimal, decimal, decimal, decimal> GetAgentEarningDetails(int storeId = 0,
            int vendorId = 0, IList<int> vendorIds = null, int customerId = 0,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            List<int> osIds = null, string billingEmail = null, int agentId = 0)
        {
            var query = _orderRepository.Table;
            if (storeId > 0)
                query = query.Where(o => o.StoreId == storeId);
            if (vendorIds != null && vendorIds.Any())
                query = query.Where(o => o.OrderItems.Any(orderItem => vendorIds.Contains(orderItem.Product.VendorId)));
            if (vendorId > 0)
                query = query.Where(o => o.OrderItems.Any(orderItem => orderItem.Product.VendorId == vendorId));
            if (customerId > 0)
                query = query.Where(o => o.CustomerId == customerId);

            // get order detail by agent
            var agentOrderStatusRepository = EngineContext.Current.Resolve<IRepository<AgentOrderStatus>>();
            query = from q in query
                    join a in agentOrderStatusRepository.Table on q.Id equals a.OrderId
                    where agentId == 0 || a.AgentId == agentId
                    select q;

            if (createdFromUtc.HasValue)
                query = query.Where(o => createdFromUtc.Value <= o.CreatedOnUtc);
            if (createdToUtc.HasValue)
                query = query.Where(o => createdToUtc.Value >= o.CreatedOnUtc);
            if (osIds != null && osIds.Any())
                query = query.Where(o => osIds.Contains(o.OrderStatusId));
            if (!string.IsNullOrEmpty(billingEmail))
                query = query.Where(o => o.BillingAddress != null && !string.IsNullOrEmpty(o.BillingAddress.Email) && o.BillingAddress.Email.Contains(billingEmail));
            query = query.Where(o => !o.Deleted);
            query = query.OrderByDescending(o => o.CreatedOnUtc);

            var totalOrders = query.Count();
            var totalAmount = (from q in query
                               join a in _orderDetailsRepository.Table on q.OrderDetailId equals a.Id
                               select new { Amount = (q.DeliveryAmount + (a.Tip ?? 0)) }).Sum(x => x.Amount);
            var totalPaidAmount = (from q in query
                                   join a in _agentEarningRepository.Table on q.Id equals a.OrderId
                                   select a).Sum(x => x.PaidAmount);
            var totalPendingAmount = totalAmount - totalPaidAmount;

            var objData = new Tuple<decimal, decimal, decimal, decimal>(totalOrders, totalAmount, totalPaidAmount, totalPendingAmount);

            return objData;
        }
        #endregion

        #region Merchant Earning

        /// <summary>
        /// get merchant earning
        /// </summary>
        /// <param name="id"></param>
        /// <param name="merchantId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public virtual MerchantEarning GetMerchantEarning(int id = 0, int merchantId = 0, int orderId = 0)
        {
            if (id == 0 && merchantId == 0 && orderId == 0)
                return null;

            var objMerchantEarning = from ma in _merchantEarningRepository.Table
                                     where (id == 0 || ma.Id == id)
                                     && (merchantId == 0 || ma.MerchantId == merchantId)
                                     && (orderId == 0 || ma.OrderId == orderId)
                                     select ma;
            return objMerchantEarning.FirstOrDefault();
        }

        /// <summary>
        /// insert or update merchant earning
        /// </summary>
        /// <param name="merchantEarning"></param>
        public virtual void InsertUpdateMerchantEarning(MerchantEarning merchantEarning)
        {
            if (merchantEarning == null)
                throw new ArgumentNullException(nameof(merchantEarning));
            var obj = _merchantEarningRepository.Table.Where(x => x.MerchantId == merchantEarning.MerchantId && x.OrderId == merchantEarning.OrderId).FirstOrDefault();
            if (obj != null)
            {
                obj.PayableAmount = merchantEarning.PayableAmount;
                obj.PaidAmount = merchantEarning.PaidAmount;

                _merchantEarningRepository.Update(obj);

                //event notification
                _eventPublisher.EntityUpdated(obj);
            }
            else
            {
                _merchantEarningRepository.Insert(merchantEarning);
                //event notification
                _eventPublisher.EntityInserted(merchantEarning);
            }
        }
        #endregion

        #region Dispatch management

        /// <summary>
        /// Get un assigned order id
        /// </summary>
        /// <returns>ids</returns>
        public virtual IList<int> GetUnassignedOrderIds(int storeId = 0, int vendorId = 0, IList<int> vendorIds = null)
        {
            //filtered orders (sagridated data)
            var orders = SearchOrders(storeId: storeId, vendorId: vendorId, vendorIds: vendorIds);

            //only "Delivery Type" orders.
            var deliveryTypeOrders = orders.Where(o => o.GetOrderDetails().OrderType == 1).ToList();
            var orderids = deliveryTypeOrders?.Where(x => x.OrderStatus == OrderStatus.Confirmed)?.OrderByDescending(x => x.Id)?.Select(o => new { o.Id, o.CustomOrderNumber });

            var assignedOrderIds = _agentOrderStatusRepository.Table.Where(x => x.OrderStatus != 9)?.Select(a => a.OrderId); //T7806: date:24-5-21 (OrderStatus 9 = Rider Rejects the order)
            //sagrigated assignedOrderIds only
            assignedOrderIds = assignedOrderIds.Where(a => orderids.Select(o => o.Id).Contains(a));
            var unassignedOrderIds = orderids.Where(x => !assignedOrderIds.Contains(x.Id))?.Select(x => Convert.ToInt32(x.CustomOrderNumber));

            return unassignedOrderIds.ToList();
        }

        /// <summary>
        /// Get un assigned order id
        /// </summary>
        /// <returns>ids</returns>
        public virtual IList<int> GetAssignedOrderIds(int storeId = 0, int vendorId = 0, IList<int> vendorIds = null)
        {
            //filtered orders (sagridated data)
            var orders = SearchOrders(storeId: storeId, vendorId: vendorId, vendorIds: vendorIds);

            //only "Delivery Type" orders.
            var deliveryTypeOrders = orders.Where(o => o.GetOrderDetails().OrderType == 1).ToList();
            var orderids = deliveryTypeOrders?.Where(x => x.OrderStatus == OrderStatus.Confirmed || x.OrderStatus == OrderStatus.RiderAssigned || x.OrderStatus == OrderStatus.OrderPickedUp)
                           ?.OrderByDescending(x => x.Id)?.Select(o => new { o.Id, o.CustomOrderNumber });

            var assignedOrderIds = _agentOrderStatusRepository.Table.Where(x => x.OrderStatus != 9)?.Select(a => a.OrderId);
            //sagrigated assignedOrderIds only
            assignedOrderIds = assignedOrderIds.Where(a => orderids.Select(o => o.Id).Contains(a));
            var assignedOngoingOrderIds = orderids.Where(x => assignedOrderIds.Contains(x.Id))?.Select(x => Convert.ToInt32(x.CustomOrderNumber));

            return assignedOngoingOrderIds.ToList();
        }

        #endregion

        #region Order Tracking

        /// <summary>
        /// Value idicates wether to show order tracking for order or not
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>True/False</returns>
        public virtual bool ShowOrderTracking(Order order)
        {
            if (order == null)
                return false;

            //once order confirmed, show tracking button
            if (order.OrderStatus == OrderStatus.Cancelled)
                return false;

            return true;
        }

        #endregion

        #region SignalR Invokers

        /// <summary>
        /// Invoke SignalR when order status changed 
        /// </summary>
        /// <param name="order"></param>
        public virtual void SendOrderStatusSignalR(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            #region Admin side

            //For Dispatch management
            if (order.OrderStatus == OrderStatus.Confirmed)
                _signalRHubContext.Clients.All.SendAsync("orderConfirmedRequest", order.Id);
            else if (order.OrderStatus == OrderStatus.OrderDelivered)
                _signalRHubContext.Clients.All.SendAsync("orderDeliveredRequest", order.Id);

            #endregion

            #region Public side

            //For Order Live Tracking 
            _signalRHubContext.Clients.All.SendAsync("updateTrackingProgress", order.Id);

            #endregion
        }

        /// <summary>
        /// Invoke SignalR when Agent asigned to order.
        /// </summary>
        /// <param name="order"></param>
        public virtual void SendAgentAssignedSignalR(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            #region Public side

            //For Order Live Tracking 
            _signalRHubContext.Clients.All.SendAsync("updateTrackingProgress", order.Id);

            #endregion
        }

        #endregion

        #region Customer/Merchant Notification & SMS

        /// <summary>
        /// Send SMS to customer
        /// </summary>
        /// <param name="order"></param>
        /// <param name="orderstatus"></param>
        public void SendMessage(Order order, int orderstatus)
        {
            var workflowMessageService = EngineContext.Current.Resolve<IWorkflowMessageService>();

            //set language
            var language = _customerService.GetDefaultLang(order.Customer);
            if (language == null)
                language = _workContext.WorkingLanguage;

            if (orderstatus == (int)OrderStatus.Received)
            {
                workflowMessageService.SendOrderStatusMessageToCustomer(order, language.Id, MessageTemplateSystemNames.SMSOrderPlacedCustomerMessage);
            }
            else if (orderstatus == (int)OrderStatus.Confirmed)
            {
                workflowMessageService.SendOrderStatusMessageToCustomer(order, language.Id, MessageTemplateSystemNames.SMSOrderConfirmedMessage);
            }
            else if (orderstatus == (int)OrderStatus.Prepared)
            {
                workflowMessageService.SendOrderStatusMessageToCustomer(order, language.Id, MessageTemplateSystemNames.SMSOrderPreparingMessage);
            }
            else if (orderstatus == (int)OrderStatus.OrderPickedUp)
            {
                workflowMessageService.SendOrderStatusMessageToCustomer(order, language.Id, MessageTemplateSystemNames.SMSOrderPickedMessage);
            }
            else if (orderstatus == (int)OrderStatus.OrderDelivered)
            {
                workflowMessageService.SendOrderStatusMessageToCustomer(order, language.Id, MessageTemplateSystemNames.SMSOrderDeliveredMessage);
            }
            else if (orderstatus == (int)OrderStatus.Cancelled)
            {
                workflowMessageService.SendOrderStatusMessageToCustomer(order, language.Id, MessageTemplateSystemNames.SMSOrderCancelledMessage);
            }
        }

        /// <summary>
        /// Send Mobile notification to customer
        /// </summary>
        /// <param name="order"></param>
        public void SendNotificationtoCustomer(Order order)
        {
            //set language
            var language = _customerService.GetDefaultLang(order.Customer);
            if (language == null)
                language = _workContext.WorkingLanguage;

            try
            {
                FCSNotificationModel fCSNotificationModel = new FCSNotificationModel();
                fCSNotificationModel.OrderId = order.Id.ToString();
                fCSNotificationModel.CustomerId = order.CustomerId.ToString();
                fCSNotificationModel.StoreId = order.StoreId.ToString();
                fCSNotificationModel.UniqueSeoCode = language.UniqueSeoCode;
                fCSNotificationModel.To = "";
                fCSNotificationModel.ApiKey = "";
                if (order.OrderStatusId == (int)OrderStatus.OrderDelivered)
                {
                    fCSNotificationModel.Heading = _localizationService.GetResource("nb.customer.notification.orderdelivered", language.Id, false, "Order Delivered");
                }
                else
                {
                    fCSNotificationModel.Heading = _localizationService.GetResource("nb.customer.notification.orderongoing", language.Id, false, "On-Going Order");
                }
                fCSNotificationModel.Status = _localizationService.GetLocalizedEnum(order.OrderStatus);

                var orderFromTime = order.CreatedOnUtc.AddMinutes(30).ToString("hh:mm tt");
                var orderToTime = order.CreatedOnUtc.AddMinutes(40).ToString("hh:mm tt");
                fCSNotificationModel.DeliveredTime = orderFromTime + "-" + orderToTime;
                int currStoreId = _workContext.GetCurrentStoreId;
                //Condition for get setting true
                String strUrl = string.Empty;
                string customersetting = _settingService.GetSettingByKey<string>("setting.customer.notificationurl", "", currStoreId);
                bool useV2 = _settingService.GetSettingByKey<bool>("setting.customer.notification.isversion2", false, currStoreId);
                if (!string.IsNullOrEmpty(customersetting))
                {
                    strUrl = customersetting;
                }
                if (string.IsNullOrEmpty(strUrl))
                {
                    throw new Exception("Customer Notification Key not Found");
                }
                //Mah Close

                var sendNotifcationURl = strUrl + "/api/v1/SendFCSNotifications";

                if (useV2)
                {
                    sendNotifcationURl = strUrl + "/api/v2.1/SendFCSNotifications";
                }
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(sendNotifcationURl);

                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                string json = JsonConvert.SerializeObject(fCSNotificationModel);
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = JsonConvert.DeserializeObject<FCSResultModel>(streamReader.ReadToEnd());

                }
            }
            catch (Exception ex)
            {
                _loger.Error("Customer notification error: OrderId=" + order.Id, ex);
            }
        }

        /// <summary>
        /// Send new order placed Mobile notification to merchant
        /// </summary>
        /// <param name="order"></param>
        public void OrderPlacedNotificationtoMerchant(Order order)
        {
            try
            {
                //Firebase notification   
                var tokenList = new List<NBVendorToken>();
                var orderVendorId = order.OrderItems.FirstOrDefault(o => o.Product.VendorId != 0)?.Product?.VendorId ?? 0;
                if (orderVendorId > 0)
                {
                    var vendorCustomer = _customerService.GetCustomerByVendorId(orderVendorId);
                    if (vendorCustomer != null)
                    {
                        var tokens = _nBVendorTokenService.GetNBVendorTokenByCustomerId(vendorCustomer.Id).ToList();
                        tokenList = tokens;
                    }
                }

                var nbFCMNotification = EngineContext.Current.Resolve<NBWebFCMNotificationSetting>();
                var fcmWebAddress = nbFCMNotification.WebAddress;
                var fcmWebKey = nbFCMNotification.WebKey;
                var newOrderTitle = _localizationService.GetResource("NB.WebFCMNotification.Fields.NewOrderTitle");
                var newOrderBody = _localizationService.GetResource("NB.WebFCMNotification.Fields.NewOrderBody");

                if (tokenList.Count > 0)
                {
                    foreach (var token in tokenList)
                    {
                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(fcmWebAddress);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, fcmWebKey);
                        httpWebRequest.Method = "POST";

                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            string strNJson = @"{
                                ""to"": """ + token.Token + @""",
                                ""data"": {
                                ""id"": """ + order.Id + @""",
                                    },
                                ""notification"":{
                                ""title"": """ + newOrderTitle + @""",
                                ""body"": """ + newOrderBody + @""",
                                ""icon"": """ + "/logo.png" + @""",
                                ""sound"": """ + "default" + @""",
                                }
                            }";
                            streamWriter.Write(strNJson);
                            System.Diagnostics.Debug.WriteLine(strNJson);
                            streamWriter.Flush();
                        }
                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            var result = streamReader.ReadToEnd();
                            _loger.Information("Order Placed firebase notification API: " + result);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                _loger.Error("Order placed firebase notification error: OrderId=" + order.Id, ex);
            }
        }

        #endregion
    }

    //Custom code from v4.0
    #region extension class
    public static class CustomOrderExtensions
    {
        public static OrderDetails GetOrderDetails(this Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            var orderDetails = new OrderDetails();
            var orderService = EngineContext.Current.Resolve<IOrderService>();
            var objorderDetails = orderService.GetOrderDetails(orderId: order.Id, isOrderComplete: true);
            if (objorderDetails != null)
                orderDetails = objorderDetails;

            return orderDetails;
        }
    }
    #endregion
}