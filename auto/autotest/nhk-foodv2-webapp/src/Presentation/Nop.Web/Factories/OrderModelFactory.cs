﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Vendors;
using Nop.Services.Catalog;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Services.Vendors;
using Nop.Web.Models.Common;
using Nop.Web.Models.Order;
using Nop.Core.Domain.RatingReview;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using ReviewType = Nop.Core.Domain.RatingReview.ReviewType;
using Nop.Services.Tax;
using Nop.Services.RatingReview;
using Nop.Web.Models.Media;
using Nop.Web.Infrastructure.Cache;
using Nop.Core.Domain.Media;
using Nop.Core.Caching;
using Nop.Services.NB;
using Nop.Services.Common;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Services.NB.Agent;

namespace Nop.Web.Factories
{
    /// <summary>
    /// Represents the order model factory
    /// </summary>
    public partial class OrderModelFactory : IOrderModelFactory
    {
        #region Fields

        private readonly AddressSettings _addressSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly IAddressModelFactory _addressModelFactory;
        private readonly ICountryService _countryService;
        private readonly ICurrencyService _currencyService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IDownloadService _downloadService;
        private readonly ILocalizationService _localizationService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IPaymentPluginManager _paymentPluginManager;
        private readonly IPaymentService _paymentService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IProductService _productService;
        private readonly IRewardPointService _rewardPointService;
        private readonly IShipmentService _shipmentService;
        private readonly IStoreContext _storeContext;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IVendorService _vendorService;
        private readonly IWorkContext _workContext;
        private readonly OrderSettings _orderSettings;
        private readonly PdfSettings _pdfSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly TaxSettings _taxSettings;
        private readonly VendorSettings _vendorSettings;
        private readonly ISettingService _settingService; //Custom code from v4.0
        private readonly ITaxService _taxService;
        private readonly IRatingReviewService _ratingReviewService;
        private readonly MediaSettings _mediaSettings;
        private readonly IWebHelper _webHelper;
        private readonly ICacheManager _cacheManager;
        private readonly IPictureService _pictureService;
        private readonly IWalletDetailsService _walletService;
        private readonly IDeliverySlotService _deliverySlotService;
        private readonly IAddressService _addressService;
        private readonly ICustomerService _customerService;
        private readonly IAgentService _agentService;
        private readonly IGenericAttributeService _genericAttributeService;

        #endregion

        #region Ctor

        public OrderModelFactory(AddressSettings addressSettings,
            CatalogSettings catalogSettings,
            IAddressModelFactory addressModelFactory,
            ICountryService countryService,
            ICurrencyService currencyService,
            IDateTimeHelper dateTimeHelper,
            IDownloadService downloadService,
            ILocalizationService localizationService,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IOrderTotalCalculationService orderTotalCalculationService,
            IPaymentPluginManager paymentPluginManager,
            IPaymentService paymentService,
            IPriceFormatter priceFormatter,
            IProductService productService,
            IRewardPointService rewardPointService,
            IShipmentService shipmentService,
            IStoreContext storeContext,
            IUrlRecordService urlRecordService,
            IVendorService vendorService,
            IWorkContext workContext,
            OrderSettings orderSettings,
            PdfSettings pdfSettings,
            RewardPointsSettings rewardPointsSettings,
            ShippingSettings shippingSettings,
            TaxSettings taxSettings,
            VendorSettings vendorSettings,
            ISettingService settingService,
            ITaxService taxService,
            IRatingReviewService ratingReviewService,
            MediaSettings mediaSettings,
            IWebHelper webHelper,
            ICacheManager cacheManager,
            IPictureService pictureService,
            IWalletDetailsService walletService,
            IDeliverySlotService deliverySlotService,
            IAddressService addressService,
            ICustomerService customerService,
            IAgentService agentService,
            IGenericAttributeService genericAttributeService)
        {
            _addressSettings = addressSettings;
            _catalogSettings = catalogSettings;
            _addressModelFactory = addressModelFactory;
            _countryService = countryService;
            _currencyService = currencyService;
            _dateTimeHelper = dateTimeHelper;
            _downloadService = downloadService;
            _localizationService = localizationService;
            _orderProcessingService = orderProcessingService;
            _orderService = orderService;
            _orderTotalCalculationService = orderTotalCalculationService;
            _paymentPluginManager = paymentPluginManager;
            _paymentService = paymentService;
            _priceFormatter = priceFormatter;
            _productService = productService;
            _rewardPointService = rewardPointService;
            _shipmentService = shipmentService;
            _storeContext = storeContext;
            _urlRecordService = urlRecordService;
            _vendorService = vendorService;
            _workContext = workContext;
            _orderSettings = orderSettings;
            _pdfSettings = pdfSettings;
            _rewardPointsSettings = rewardPointsSettings;
            _shippingSettings = shippingSettings;
            _taxSettings = taxSettings;
            _vendorSettings = vendorSettings;
            _settingService = settingService;
            _taxService = taxService;
            _ratingReviewService = ratingReviewService;
            _mediaSettings = mediaSettings;
            _webHelper = webHelper;
            _cacheManager = cacheManager;
            _pictureService = pictureService;
            _walletService = walletService;
            _deliverySlotService = deliverySlotService;
            _addressService = addressService;
            _customerService = customerService;
            _agentService = agentService;
            _genericAttributeService = genericAttributeService;
        }

        #endregion

        #region Utitlities

        /// <summary>
        /// Prepare review model
        /// </summary>
        /// <param name="reviewType"></param>
        /// <param name="entityId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        protected virtual OrderDetailsModel.ReviewModel PrepareReviewModel(ReviewType reviewType, int? entityId, int orderId)
        {
            //get ratingreview record
            var reviews = _ratingReviewService.GetAllRatingReviews(_workContext.GetCurrentStoreId);
            var review = new RatingReviews();
            if (reviewType == ReviewType.Item && entityId != null)
            {
                //item review
                review = reviews?.FirstOrDefault(r => r.OrderId == orderId && r.ReviewType == (int)reviewType && r.EntityId == entityId);
            }
            else
            {
                //agent, merchant review
                review = reviews?.FirstOrDefault(r => r.OrderId == orderId && r.ReviewType == (int)reviewType);
            }

            if (review != null)
            {
                return new OrderDetailsModel.ReviewModel
                {
                    Id = review.Id,
                    IsRiviewFound = true,
                    RatingStar = review.Rating,
                    Title = review.Title,
                    AdditionalNote = review.ReviewText
                };
            }
            else
                return null;
        }

        /// <summary>
        /// Prepare the product overview picture model
        /// </summary>
        /// <param name="product">Product</param>
        /// <param name="productThumbPictureSize">Product thumb picture size (longest side); pass null to use the default value of media settings</param>
        /// <returns>Picture model</returns>
        protected virtual PictureModel PrepareProductOverviewPictureModel(Product product, int? productThumbPictureSize = null)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product));

            var productName = _localizationService.GetLocalized(product, x => x.Name);
            //If a size has been set in the view, we use it in priority
            var pictureSize = productThumbPictureSize ?? _mediaSettings.ProductThumbPictureSize;

            //prepare picture model
            var cacheKey = string.Format(NopModelCacheDefaults.ProductDefaultPictureModelKey,
                product.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(),
                _storeContext.CurrentStore.Id);

            var defaultPictureModel = _cacheManager.Get(cacheKey, () =>
            {
                var picture = _pictureService.GetPicturesByProductId(product.Id, 1).FirstOrDefault();
                var pictureModel = new PictureModel
                {
                    ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                    FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                    //"title" attribute
                    Title = (picture != null && !string.IsNullOrEmpty(picture.TitleAttribute))
                        ? picture.TitleAttribute
                        : string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"),
                            productName),
                    //"alt" attribute
                    AlternateText = (picture != null && !string.IsNullOrEmpty(picture.AltAttribute))
                        ? picture.AltAttribute
                        : string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"),
                            productName)
                };

                return pictureModel;
            });

            return defaultPictureModel;
        }

        /// <summary>
        /// Prepare LiveTracking model
        /// </summary>
        /// <param name="orderDetailmodel">OrderDetailsModel</param>
        /// <returns>LiveTrackingModel</returns>
        protected virtual LiveTrackingModel PrepareLiveTrackingModel(OrderDetailsModel orderDetailmodel, Order order)
        {
            var model = orderDetailmodel.LiveTrackingModel;
            model.OrderId = orderDetailmodel.Id;
            model.TrackingPossible = false;

            if (order == null)
                return null;

            //restaurant(vendor/merchant) info
            if (model.RestauratTrakingInfo.RestaurantId == 0)
                model.ErrorMessage.Add(_localizationService.GetResource("NB.Order.LiveTracking.Restaurant.Address.NotFound"));
            //restaurant coordiinates
            if (model.RestauratTrakingInfo.Latitude == null || model.RestauratTrakingInfo.Longitude == null)
                model.ErrorMessage.Add(_localizationService.GetResource("NB.Order.LiveTracking.Restaurant.Address.Coordinates.NotFound"));

            //customer info
            if (order.BillingAddress == null)
                model.ErrorMessage.Add(_localizationService.GetResource("NB.Order.LiveTracking.Customer.Address.NotFound"));
            else
            {
                var customerAddress = order.BillingAddress;
                model.CustomerTrakingInfo.CustomertId = order.CustomerId;
                model.CustomerTrakingInfo.CustomerName = _customerService.GetCustomerFullName(order.Customer) ?? string.Empty;
                model.CustomerTrakingInfo.Latitude = customerAddress.Latitude;
                model.CustomerTrakingInfo.Longitude = customerAddress.Longitude;
                if (customerAddress.Latitude == null || customerAddress.Longitude == null)
                    model.ErrorMessage.Add(_localizationService.GetResource("NB.Order.LiveTracking.Customer.Address.Coordinates.NotFound"));
            }

            //rider info
            //Delivery type order will only have rider assigned.
            if (orderDetailmodel.OrderTypeId == 1)
            {
                var assignedAgent = _customerService.GetAssignedAgentByOrderId(order.Id);
                if (assignedAgent == null)
                    model.ErrorMessage.Add(_localizationService.GetResource("NB.Order.LiveTracking.Agent.NotAssigned"));
                else
                {
                    var agentCustomer = _customerService.GetCustomerById(assignedAgent.AgentId);
                    if (agentCustomer == null)
                        model.ErrorMessage.Add(_localizationService.GetResource("NB.Order.LiveTracking.Agent.NotAssigned"));
                    else
                    {
                        model.RiderTrakingInfo.RidertId = assignedAgent.AgentId;
                        model.RiderTrakingInfo.RiderName = _customerService.GetCustomerFullName(agentCustomer) ?? string.Empty;

                        //get rider latest coordinates if available, else assign restaurant coordinates 
                        var agentCoordinatesrecord = _agentService.GetAgentOrderGeoLocation(agentCustomer.Id, order.Id);
                        if (agentCoordinatesrecord != null)
                        {
                            decimal.TryParse(agentCoordinatesrecord.AgentLat, out decimal agentLat);
                            decimal.TryParse(agentCoordinatesrecord.AgentLong, out decimal agentLng);
                            model.RiderTrakingInfo.Latitude = agentLat;
                            model.RiderTrakingInfo.Longitude = agentLng;
                        }
                        else
                        {
                            //now here. initially in agent latlong, assign restaurant latlong detail, because rider starting point will be restaurant address.
                            //Further we will update agent latlong as rider moves on track
                            model.RiderTrakingInfo.Latitude = model.RestauratTrakingInfo.Latitude;
                            model.RiderTrakingInfo.Longitude = model.RestauratTrakingInfo.Longitude;
                        }

                        //phone
                        model.RiderTrakingInfo.RiderPhone = _genericAttributeService.GetAttribute<string>(agentCustomer, NopCustomerDefaults.PhoneAttribute);
                        //Rider avg rating
                        var riderAvgRating = _ratingReviewService.GetAverageRating(_workContext.GetCurrentStoreId, agentCustomer.Id, (int)ReviewType.Agent, out int reviewCount);
                        model.RiderTrakingInfo.RiderRatingCount = reviewCount;
                        model.RiderTrakingInfo.RiderAverageRating = riderAvgRating;
                        //Rider picture url
                        var avatarPictureId = _genericAttributeService.GetAttribute<int>(agentCustomer, NopCustomerDefaults.AvatarPictureIdAttribute);
                        model.RiderTrakingInfo.PictureUrl = _pictureService.GetPictureUrl(avatarPictureId, _mediaSettings.AvatarPictureSize,
                                                                                          defaultPictureType: PictureType.Avatar);
                    }
                }
            }

            //check for error
            if (!model.ErrorMessage.Any())
                model.TrackingPossible = true;

            return model;
        }

        /// <summary>
        /// Prepare order status tracking list 
        /// </summary>
        /// <param name="orderDetailmodel"></param>
        /// <returns></returns>
        protected virtual IList<OrderedStatusTracking> PrepareOrderedTrackingStatusList(OrderDetailsModel orderDetailmodel, Order order)
        {
            var model = new List<OrderedStatusTracking>();

            //sort enum in sequence in which it displayed on view side (code from API )
            List<int> enumSortedList = new List<int>() { 10, 20, 25, 30, 3, 4, 40 }; //contain orderStatus enum values
            var orderstatusIds = Enum.GetValues(typeof(OrderHistoryEnum)).Cast<int>().ToList();

            //Need to exclude some order status for Takeaway ordertype
            if (orderDetailmodel.OrderTypeId == 2)
            {
                var takeAwayExcludeOdStatusIds = new List<int>()
                {
                    (int)OrderHistoryEnum.RiderAssigned,
                    (int)OrderHistoryEnum.Picked,
                };

                //exclude defined status
                orderstatusIds = orderstatusIds.Where(x => !takeAwayExcludeOdStatusIds.Contains(x)).ToList();
            }

            //sort in specified order
            orderstatusIds = orderstatusIds.OrderBy(d => enumSortedList.IndexOf(d)).ToList();

            //rider info
            var riderName = orderDetailmodel.LiveTrackingModel.RiderTrakingInfo.RiderName;

            //we will use order notes to track status is accomplished or not. (API logic)
            var orderNotes = order.OrderNotes.OrderByDescending(o => o.Id).ToList();
            DateTime? dt = null;

            foreach (var id in orderstatusIds)
            {
                var statusEnum = (OrderStatus)id;
                var statusName = _localizationService.GetLocalizedEnum(statusEnum);
                var statusOrderNote = orderNotes.FirstOrDefault(x => x.OrderStatus == id); //record found mean status accomlished
                //reviced will be acctive anyway
                var active = id == (int)OrderHistoryEnum.Received ? true : (statusOrderNote != null ? true : false);

                var orderstatus = new OrderedStatusTracking()
                {
                    StatusId = id,
                    StatusTitle = _localizationService.GetLocalizedEnum(statusEnum),
                    MiniDescription = GetStatusMiniDescription(id, orderDetailmodel.MerchantName, riderName),
                    StatusTime = statusOrderNote != null ? _dateTimeHelper.ConvertToUserTime(statusOrderNote.CreatedOnUtc, DateTimeKind.Utc) : dt,
                    IsUpdate = active,
                };
                model.Add(orderstatus);
            }

            return model;
        }

        /// <summary>
        /// Prepare order status mini description
        /// </summary>
        /// <param name="orderStatusId"></param>
        /// <param name="merchantName"></param>
        /// <param name="riderName"></param>
        /// <returns></returns>
        private string GetStatusMiniDescription(int orderStatusId, string merchantName, string riderName)
        {
            var miniDescription = string.Empty;
            if (orderStatusId == (int)OrderHistoryEnum.Received)
            {
                miniDescription = _localizationService.GetResource("API.Order.OrderDetail.Status.MiniDescription.Recived");
            }
            else if (orderStatusId == (int)OrderHistoryEnum.Confirmed)
            {
                miniDescription = _localizationService.GetResource("API.Order.OrderDetail.Status.MiniDescription.Confirmed");
            }
            else if (orderStatusId == (int)OrderHistoryEnum.RiderAssigned)
            {
                miniDescription = string.Format(_localizationService.GetResource("API.Order.OrderDetail.Status.MiniDescription.RiderAssigned"), riderName);
            }
            else if (orderStatusId == (int)OrderHistoryEnum.Prepared)
            {
                miniDescription = string.Format(_localizationService.GetResource("API.Order.OrderDetail.Status.MiniDescription.Prepared"), merchantName);
            }
            else if (orderStatusId == (int)OrderHistoryEnum.Picked)
            {
                miniDescription = string.Format(_localizationService.GetResource("API.Order.OrderDetail.Status.MiniDescription.Picked"), riderName);
            }
            else if (orderStatusId == (int)OrderHistoryEnum.Delivered)
            {
                miniDescription = _localizationService.GetResource("API.Order.OrderDetail.Status.MiniDescription.Delivered");
            }
            else if (orderStatusId == (int)OrderHistoryEnum.Cancelled)
            {
                miniDescription = _localizationService.GetResource("API.Order.OrderDetail.Status.MiniDescription.Cancelled");
            }

            return miniDescription;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare the customer order list model
        /// </summary>
        /// <returns>Customer order list model</returns>
        public virtual CustomerOrderListModel PrepareCustomerOrderListModel()
        {
            var model = new CustomerOrderListModel();
            //var orders = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,
            //    customerId: _workContext.CurrentCustomer.Id);

            #region Custom code from v4.0
            int storeId = _workContext.GetCurrentStoreId;

            var orders = _orderService.SearchOrders(storeId: storeId,
                customerId: _workContext.CurrentCustomer.Id);

            var isEnabled = _settingService.GetSettingByKey<bool>("ratingreviewsettings.isenabled", true, _workContext.GetCurrentStoreId, true);

            #endregion

            foreach (var order in orders)
            {
                #region Custom code Added by KP

                var isAllReviewFound = _ratingReviewService.AllReviewFound(order.Id);
                var showRtingReviewButton = (isEnabled && order.OrderStatus == OrderStatus.OrderDelivered && !isAllReviewFound);

                //if (showRtingReviewButton)
                //{
                //    int reviewType = (int)ReviewType.Agent;
                //    int review_status = _workContext.CurrentCustomer.CheckReviewForCustomer(reviewType, order.Id, storeId, true);

                //    if (review_status == 0 || review_status == 3)
                //        showRtingReviewButton = false;

                //    if (review_status == 2 || review_status == 4)
                //    {
                //        reviewType = (int)ReviewType.Merchant;
                //        review_status = _workContext.CurrentCustomer.CheckReviewForCustomer(reviewType, order.Id, storeId, true);

                //        if (review_status == 0 || review_status == 3)
                //            showRtingReviewButton = false;

                //        if (review_status == 2 || review_status == 4)
                //        {
                //            reviewType = (int)ReviewType.Item;
                //            review_status = _workContext.CurrentCustomer.CheckReviewForCustomer(reviewType, order.Id, storeId, true);

                //            if (review_status != 1)
                //                showRtingReviewButton = false;
                //        }
                //    }
                //}
                #endregion

                var details = order.GetOrderDetails();
                var orderModel = new CustomerOrderListModel.OrderDetailsModel
                {
                    Id = order.Id,
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc),
                    OrderStatusEnum = order.OrderStatus,
                    OrderStatus = _localizationService.GetLocalizedEnum(order.OrderStatus),
                    PaymentStatus = _localizationService.GetLocalizedEnum(order.PaymentStatus),
                    ShippingStatus = _localizationService.GetLocalizedEnum(order.ShippingStatus),
                    IsReturnRequestAllowed = _orderProcessingService.IsReturnRequestAllowed(order),
                    CustomOrderNumber = order.CustomOrderNumber,
                    ShowRtingReviewButton = showRtingReviewButton,//Custom code from v4.0
                    OrderGuId = order.OrderGuid.ToString(),
                    OrderScheduleDate = (details.ScheduleDate != null && details.ScheduleTime != null) ? details.ScheduleDate.Value.Add(details.ScheduleTime.Value).ToString() : "N/A",
                    ShowOrderTrackingButton = _orderService.ShowOrderTracking(order)
                };
                _addressModelFactory.PrepareAddressModel(orderModel.Address,
                    address: order.BillingAddress,
                    excludeProperties: false,
                    addressSettings: _addressSettings);

                var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
                orderModel.OrderTotal = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

                //Restaurant name/ Vendor name
                var vendors = _vendorService.GetVendorsByIds(order.OrderItems.Select(item => item.Product.VendorId).ToArray());
                orderModel.VendorName = vendors.FirstOrDefault()?.Name ?? string.Empty;
                model.Orders.Add(orderModel);
            }

            var recurringPayments = _orderService.SearchRecurringPayments(_storeContext.CurrentStore.Id,
                _workContext.CurrentCustomer.Id);
            foreach (var recurringPayment in recurringPayments)
            {
                var recurringPaymentModel = new CustomerOrderListModel.RecurringOrderModel
                {
                    Id = recurringPayment.Id,
                    StartDate = _dateTimeHelper.ConvertToUserTime(recurringPayment.StartDateUtc, DateTimeKind.Utc).ToString(),
                    CycleInfo = $"{recurringPayment.CycleLength} {_localizationService.GetLocalizedEnum(recurringPayment.CyclePeriod)}",
                    NextPayment = recurringPayment.NextPaymentDate.HasValue ? _dateTimeHelper.ConvertToUserTime(recurringPayment.NextPaymentDate.Value, DateTimeKind.Utc).ToString() : "",
                    TotalCycles = recurringPayment.TotalCycles,
                    CyclesRemaining = recurringPayment.CyclesRemaining,
                    InitialOrderId = recurringPayment.InitialOrder.Id,
                    InitialOrderNumber = recurringPayment.InitialOrder.CustomOrderNumber,
                    CanCancel = _orderProcessingService.CanCancelRecurringPayment(_workContext.CurrentCustomer, recurringPayment),
                    CanRetryLastPayment = _orderProcessingService.CanRetryLastRecurringPayment(_workContext.CurrentCustomer, recurringPayment)
                };

                model.RecurringOrders.Add(recurringPaymentModel);
            }

            return model;
        }

        /// <summary>
        /// Prepare the order details model
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Order details model</returns>
        public virtual OrderDetailsModel PrepareOrderDetailsModel(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));
            var model = new OrderDetailsModel
            {
                Id = order.Id,
                CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc),
                OrderStatus = _localizationService.GetLocalizedEnum(order.OrderStatus),
                OrderStatusEnum = order.OrderStatus,
                IsReOrderAllowed = _orderSettings.IsReOrderAllowed,
                IsReturnRequestAllowed = _orderProcessingService.IsReturnRequestAllowed(order),
                PdfInvoiceDisabled = _pdfSettings.DisablePdfInvoicesForPendingOrders && order.OrderStatus == OrderStatus.Received,
                CustomOrderNumber = order.CustomOrderNumber,

                //shipping info
                ShippingStatus = _localizationService.GetLocalizedEnum(order.ShippingStatus)
            };
            if (order.ShippingStatus != ShippingStatus.ShippingNotRequired)
            {
                model.IsShippable = true;
                model.PickupInStore = order.PickupInStore;
                if (!order.PickupInStore)
                {
                    _addressModelFactory.PrepareAddressModel(model.ShippingAddress,
                        address: order.ShippingAddress,
                        excludeProperties: false,
                        addressSettings: _addressSettings);
                }
                else
                    if (order.PickupAddress != null)
                    model.PickupAddress = new AddressModel
                    {
                        Address1 = order.PickupAddress.Address1,
                        City = order.PickupAddress.City,
                        County = order.PickupAddress.County,
                        CountryName = order.PickupAddress.Country != null ? order.PickupAddress.Country.Name : string.Empty,
                        ZipPostalCode = order.PickupAddress.ZipPostalCode
                    };
                model.ShippingMethod = order.ShippingMethod;

                //shipments (only already shipped)
                var shipments = order.Shipments.Where(x => x.ShippedDateUtc.HasValue).OrderBy(x => x.CreatedOnUtc).ToList();
                foreach (var shipment in shipments)
                {
                    var shipmentModel = new OrderDetailsModel.ShipmentBriefModel
                    {
                        Id = shipment.Id,
                        TrackingNumber = shipment.TrackingNumber,
                    };
                    if (shipment.ShippedDateUtc.HasValue)
                        shipmentModel.ShippedDate = _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc);
                    if (shipment.DeliveryDateUtc.HasValue)
                        shipmentModel.DeliveryDate = _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc);
                    model.Shipments.Add(shipmentModel);
                }
            }

            //billing info
            _addressModelFactory.PrepareAddressModel(model.BillingAddress,
                address: order.BillingAddress,
                excludeProperties: false,
                addressSettings: _addressSettings);

            //VAT number
            model.VatNumber = order.VatNumber;

            //payment method
            var paymentMethod = _paymentPluginManager.LoadPluginBySystemName(order.PaymentMethodSystemName);
            model.PaymentMethod = paymentMethod != null ? _localizationService.GetLocalizedFriendlyName(paymentMethod, _workContext.WorkingLanguage.Id) : order.PaymentMethodSystemName;
            model.PaymentMethodStatus = _localizationService.GetLocalizedEnum(order.PaymentStatus);
            model.CanRePostProcessPayment = _paymentService.CanRePostProcessPayment(order);
            //custom values
            model.CustomValues = _paymentService.DeserializeCustomValues(order);

            //order subtotal
            if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal)
            {
                //including tax

                //order subtotal
                var orderSubtotalInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalInclTax, order.CurrencyRate);
                model.OrderSubtotal = _priceFormatter.FormatPrice(orderSubtotalInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                //discount (applied to order subtotal)
                var orderSubTotalDiscountInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountInclTax, order.CurrencyRate);
                if (orderSubTotalDiscountInclTaxInCustomerCurrency > decimal.Zero)
                    model.OrderSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
            }
            else
            {
                //excluding tax

                //order subtotal
                var orderSubtotalExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalExclTax, order.CurrencyRate);
                model.OrderSubtotal = _priceFormatter.FormatPrice(orderSubtotalExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                //discount (applied to order subtotal)
                var orderSubTotalDiscountExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountExclTax, order.CurrencyRate);
                if (orderSubTotalDiscountExclTaxInCustomerCurrency > decimal.Zero)
                    model.OrderSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
            }

            if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
            {
                //including tax

                //order shipping
                var orderShippingInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingInclTax, order.CurrencyRate);
                model.OrderShipping = _priceFormatter.FormatShippingPrice(orderShippingInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                //payment method additional fee
                var paymentMethodAdditionalFeeInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeInclTax, order.CurrencyRate);
                if (paymentMethodAdditionalFeeInclTaxInCustomerCurrency > decimal.Zero)
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
            }
            else
            {
                //excluding tax

                //order shipping
                var orderShippingExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingExclTax, order.CurrencyRate);
                model.OrderShipping = _priceFormatter.FormatShippingPrice(orderShippingExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                //payment method additional fee
                var paymentMethodAdditionalFeeExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeExclTax, order.CurrencyRate);
                if (paymentMethodAdditionalFeeExclTaxInCustomerCurrency > decimal.Zero)
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
            }

            //tax
            var displayTax = true;
            var displayTaxRates = true;
            if (_taxSettings.HideTaxInOrderSummary && order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
            {
                displayTax = false;
                displayTaxRates = false;
            }
            else
            {
                if (order.OrderTax == 0 && _taxSettings.HideZeroTax)
                {
                    displayTax = false;
                    displayTaxRates = false;
                }
                else
                {
                    var taxRates = _orderService.ParseTaxRates(order, order.TaxRates);
                    displayTaxRates = _taxSettings.DisplayTaxRates && taxRates.Any();
                    displayTax = !displayTaxRates;

                    var orderTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTax, order.CurrencyRate);
                    //TODO pass languageId to _priceFormatter.FormatPrice
                    model.Tax = _priceFormatter.FormatPrice(orderTaxInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

                    foreach (var tr in taxRates)
                    {
                        model.TaxRates.Add(new OrderDetailsModel.TaxRate
                        {
                            Rate = _priceFormatter.FormatTaxRate(tr.Key),
                            //TODO pass languageId to _priceFormatter.FormatPrice
                            Value = _priceFormatter.FormatPrice(_currencyService.ConvertCurrency(tr.Value, order.CurrencyRate), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage),
                        });
                    }
                }
            }
            model.AppliedTaxRates = _taxService.GetAppliedTaxRates(order.Customer, order.OrderItems.Select(x => x.Product).ToList(), order.StoreId);
            model.DisplayTaxRates = displayTaxRates;
            model.DisplayTax = displayTax;
            model.DisplayTaxShippingInfo = _catalogSettings.DisplayTaxShippingInfoOrderDetailsPage;
            model.PricesIncludeTax = order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax;

            //discount (applied to order total)
            var orderDiscountInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderDiscount, order.CurrencyRate);
            if (orderDiscountInCustomerCurrency > decimal.Zero)
                model.OrderTotalDiscount = _priceFormatter.FormatPrice(-orderDiscountInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

            //gift cards
            foreach (var gcuh in order.GiftCardUsageHistory)
            {
                model.GiftCards.Add(new OrderDetailsModel.GiftCard
                {
                    CouponCode = gcuh.GiftCard.GiftCardCouponCode,
                    Amount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(gcuh.UsedValue, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage),
                });
            }

            //reward points           
            if (order.RedeemedRewardPointsEntry != null)
            {
                model.RedeemedRewardPoints = -order.RedeemedRewardPointsEntry.Points;
                model.RedeemedRewardPointsAmount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(order.RedeemedRewardPointsEntry.UsedAmount, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);
            }

            //delivery Amount Start
            var deliveryAmountInCustomerCurrency = _currencyService.ConvertCurrency(order.DeliveryAmount, order.CurrencyRate);
            model.DeliveryAmount = _priceFormatter.FormatPrice(deliveryAmountInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);
            //delivery Amount End

            //serviceCharge Amount Start
            var serviceChargeAmountInCustomerCurrency = _currencyService.ConvertCurrency(order.ServiceChargeAmount, order.CurrencyRate);
            model.ServiceChargeAmount = _priceFormatter.FormatPrice(serviceChargeAmountInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);
            //serviceCharge Amount End

            //tip Amount Start
            var tipAmountInCustomerCurrency = _currencyService.ConvertCurrency(order.Tip, order.CurrencyRate);
            model.TipAmount = _priceFormatter.FormatPrice(tipAmountInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);
            //tip Amount End

            //total
            var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
            model.OrderTotal = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

            //checkout attributes
            model.CheckoutAttributeInfo = order.CheckoutAttributeDescription;

            //order notes
            foreach (var orderNote in order.OrderNotes
                .Where(on => on.DisplayToCustomer)
                .OrderByDescending(on => on.CreatedOnUtc)
                .ToList())
            {
                model.OrderNotes.Add(new OrderDetailsModel.OrderNote
                {
                    Id = orderNote.Id,
                    HasDownload = orderNote.DownloadId > 0,
                    Note = _orderService.FormatOrderNoteText(orderNote),
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(orderNote.CreatedOnUtc, DateTimeKind.Utc)
                });
            }

            //purchased products
            model.ShowSku = _catalogSettings.ShowSkuOnProductDetailsPage;
            model.ShowVendorName = _vendorSettings.ShowVendorOnOrderDetailsPage;

            var orderItems = order.OrderItems;

            var vendors = _vendorSettings.ShowVendorOnOrderDetailsPage ? _vendorService.GetVendorsByIds(orderItems.Select(item => item.Product.VendorId).ToArray()) : new List<Vendor>();

            foreach (var orderItem in orderItems)
            {
                var orderItemModel = new OrderDetailsModel.OrderItemModel
                {
                    Id = orderItem.Id,
                    OrderItemGuid = orderItem.OrderItemGuid,
                    Sku = _productService.FormatSku(orderItem.Product, orderItem.AttributesXml),
                    VendorName = vendors.FirstOrDefault(v => v.Id == orderItem.Product.VendorId)?.Name ?? string.Empty,
                    ProductId = orderItem.Product.Id,
                    ProductName = _localizationService.GetLocalized(orderItem.Product, x => x.Name),
                    ProductSeName = _urlRecordService.GetSeName(orderItem.Product),
                    Quantity = orderItem.Quantity,
                    AttributeInfo = orderItem.AttributeDescription,
                };
                //rental info
                if (orderItem.Product.IsRental)
                {
                    var rentalStartDate = orderItem.RentalStartDateUtc.HasValue
                        ? _productService.FormatRentalDate(orderItem.Product, orderItem.RentalStartDateUtc.Value) : "";
                    var rentalEndDate = orderItem.RentalEndDateUtc.HasValue
                        ? _productService.FormatRentalDate(orderItem.Product, orderItem.RentalEndDateUtc.Value) : "";
                    orderItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
                        rentalStartDate, rentalEndDate);
                }
                model.Items.Add(orderItemModel);

                //unit price, subtotal
                if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                {
                    //including tax
                    var unitPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceInclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);

                    var priceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceInclTax, order.CurrencyRate);
                    orderItemModel.SubTotal = _priceFormatter.FormatPrice(priceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                }
                else
                {
                    //excluding tax
                    var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);

                    var priceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceExclTax, order.CurrencyRate);
                    orderItemModel.SubTotal = _priceFormatter.FormatPrice(priceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                }

                //downloadable products
                if (_downloadService.IsDownloadAllowed(orderItem))
                    orderItemModel.DownloadId = orderItem.Product.DownloadId;
                if (_downloadService.IsLicenseDownloadAllowed(orderItem))
                    orderItemModel.LicenseId = orderItem.LicenseDownloadId.HasValue ? orderItem.LicenseDownloadId.Value : 0;

                //prepare item review model
                orderItemModel.ItemReviewModel = PrepareReviewModel(ReviewType.Item, orderItem.ProductId, order.Id);

                //item picture
                orderItemModel.Picture = PrepareProductOverviewPictureModel(orderItem.Product);
            }

            //prepare agent review model
            model.AgentReviewModel = PrepareReviewModel(ReviewType.Agent, null, order.Id);
            //prepare merchant review model
            model.MerchantReviewModel = PrepareReviewModel(ReviewType.Merchant, null, order.Id);

            //delivery slot
            var deliverySlot = _deliverySlotService.GetDeliverySlotBookingByOrderId(order.Id);
            if (deliverySlot != null)
                model.DeliveryInfo = string.Format("{0} {1} - {2}", deliverySlot.DeliveryDateUtc.Date.ToString("MM/dd/yyyy"), deliverySlot.DeliveryStartTime.ToString("hh:mm tt"), deliverySlot.DeliveryEndTime.ToString("hh:mm tt"));

            //vendor address
            if (orderItems.Any())
            {
                var merchantAddress = "";
                var productId = orderItems.Select(x => x.ProductId).FirstOrDefault();
                var vendorId = _productService.GetProductById(productId)?.VendorId ?? 0;
                if (vendorId > 0)
                {
                    var vendor = _vendorService.GetVendorById(vendorId);
                    model.MerchantName = vendor?.Name ?? " ";
                    var vendorAddressId = vendor?.AddressId ?? 0;
                    if (vendorAddressId > 0)
                    {
                        var vendorAddress = _addressService.GetAddressById(vendorAddressId);
                        if (vendorAddress != null)
                        {

                            if (!string.IsNullOrEmpty(vendorAddress.Address1))
                            {
                                merchantAddress += vendorAddress.Address1;
                            }
                            if (!string.IsNullOrEmpty(vendorAddress.Address2))
                            {
                                merchantAddress += ", " + vendorAddress.Address2;
                            }
                            if (!string.IsNullOrEmpty(vendorAddress.City))
                            {
                                merchantAddress += ", " + vendorAddress.City;
                            }
                            if (vendorAddress.StateProvince != null && !string.IsNullOrEmpty(vendorAddress.StateProvince.Name))
                            {
                                merchantAddress += ", " + vendorAddress.StateProvince.Name;
                            }
                            if (!string.IsNullOrEmpty(vendorAddress.ZipPostalCode))
                            {
                                merchantAddress += ", " + vendorAddress.ZipPostalCode;
                            }
                            if (vendorAddress.CountryId > 0)
                            {
                                var country = _countryService.GetCountryById(vendorAddress.CountryId ?? 0);
                                if (country != null)
                                    merchantAddress += ", " + country.Name;
                            }

                            //assign value in tracking model
                            model.LiveTrackingModel.RestauratTrakingInfo.RestaurantId = vendorId;
                            model.LiveTrackingModel.RestauratTrakingInfo.RestauratName = vendor?.Name ?? string.Empty;
                            model.LiveTrackingModel.RestauratTrakingInfo.Latitude = vendorAddress.Latitude;
                            model.LiveTrackingModel.RestauratTrakingInfo.Longitude = vendorAddress.Longitude;
                            model.LiveTrackingModel.RestauratTrakingInfo.RestHelpLink = _settingService.GetSettingByKey<string>("OrderTracking.RestaurantInfo.Helplink", string.Empty, _workContext.GetCurrentStoreId, true);
                            ;
                            model.LiveTrackingModel.RestauratTrakingInfo.RestPhone = vendorAddress.PhoneNumber;
                            //Merchant rating
                            var restAvgRating = _ratingReviewService.GetAverageRating(_workContext.GetCurrentStoreId, vendorId, (int)ReviewType.Merchant, out int reviewCount);
                            model.LiveTrackingModel.RestauratTrakingInfo.RestRatingCount = reviewCount;
                            model.LiveTrackingModel.RestauratTrakingInfo.RestAverageRating = restAvgRating;

                            //prepare picture model
                            var pictureSize = _mediaSettings.VendorThumbPictureSize;
                            var pictureCacheKey = string.Format(NopModelCacheDefaults.VendorPictureModelKey, vendor.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
                            model.LiveTrackingModel.RestauratTrakingInfo.PictureModel = _cacheManager.Get(pictureCacheKey, () =>
                            {
                                var picture = _pictureService.GetPictureById(vendor.PictureId);
                                var pictureModel = new PictureModel
                                {
                                    FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                                    ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                                    Title = string.Format(_localizationService.GetResource("Media.Vendor.ImageLinkTitleFormat"), vendor.Name),
                                    AlternateText = string.Format(_localizationService.GetResource("Media.Vendor.ImageAlternateTextFormat"), vendor.Name)
                                };
                                return pictureModel;
                            });
                        }
                    }
                }
                model.MerchantAddress = merchantAddress;
            }

            //orderStatusProgressive bar
            var details = order.GetOrderDetails();
            var orderType = 0;
            if (details != null && details.OrderType != null)
            {
                orderType = details.OrderType == 1 ? 1 : 2;
            }
            if (orderType > 0)
            {
                if (orderType == 1)
                {
                    model.OrderStatusDetail.OrderStatus.Add(OrderStatus.Received);
                    model.OrderStatusDetail.OrderStatus.Add(OrderStatus.Confirmed);
                    model.OrderStatusDetail.OrderStatus.Add(OrderStatus.Prepared);
                    model.OrderStatusDetail.OrderStatus.Add(OrderStatus.OrderPickedUp);
                    model.OrderStatusDetail.OrderStatus.Add(OrderStatus.OrderDelivered);
                }
                else
                {
                    model.OrderStatusDetail.OrderStatus.Add(OrderStatus.Received);
                    model.OrderStatusDetail.OrderStatus.Add(OrderStatus.Confirmed);
                    model.OrderStatusDetail.OrderStatus.Add(OrderStatus.Prepared);
                    model.OrderStatusDetail.OrderStatus.Add(OrderStatus.OrderDelivered);
                }

                var orderNotes11 = order.OrderNotes;
                var orderSatus = new List<string>() { "New status: Confirmed", "New status: Order Delivered", "New status: Order Picked Up", "New status: Received", "New status: Prepared" };
                model.OrderStatusDetail.Notes = order.OrderNotes.Where(x => orderSatus.Any(y => x.Note.Contains(y)) || x.Note == "Order placed").OrderBy(x => x.CreatedOnUtc).Select(x => new SelectListItem { Text = x.Note, Value = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc).ToString("HH:mm tt") }).ToList();

            }
            var scheduledOrder = _deliverySlotService.GetDeliverySlotBookingByOrderId(order.Id);
            if (scheduledOrder != null)
            {
                model.ScheduledOrderDate = _dateTimeHelper.ConvertToUserTime(scheduledOrder.DeliveryStartTime, DateTimeKind.Utc).ToString("ddd, dd MMM");
                model.ScheduledOrderTime = _dateTimeHelper.ConvertToUserTime(scheduledOrder.DeliveryStartTime, DateTimeKind.Utc).ToString("hh: mm tt") + "-" + _dateTimeHelper.ConvertToUserTime(scheduledOrder.DeliveryEndTime, DateTimeKind.Utc).ToString("hh: mm tt");
            }

            //prepare Rider tracking model and required details
            model.OrderTypeId = orderType;
            model.ShowOrderTracking = _orderService.ShowOrderTracking(order);
            model.LiveTrackingModel = PrepareLiveTrackingModel(model, order);
            model.OrderedTrackingStatuses = PrepareOrderedTrackingStatusList(model, order);

            return model;
        }

        /// <summary>
        /// Prepare the shipment details model
        /// </summary>
        /// <param name="shipment">Shipment</param>
        /// <returns>Shipment details model</returns>
        public virtual ShipmentDetailsModel PrepareShipmentDetailsModel(Shipment shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException(nameof(shipment));

            var order = shipment.Order;
            if (order == null)
                throw new Exception("order cannot be loaded");
            var model = new ShipmentDetailsModel
            {
                Id = shipment.Id
            };
            if (shipment.ShippedDateUtc.HasValue)
                model.ShippedDate = _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc);
            if (shipment.DeliveryDateUtc.HasValue)
                model.DeliveryDate = _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc);

            //tracking number and shipment information
            if (!string.IsNullOrEmpty(shipment.TrackingNumber))
            {
                model.TrackingNumber = shipment.TrackingNumber;
                var shipmentTracker = _shipmentService.GetShipmentTracker(shipment);
                if (shipmentTracker != null)
                {
                    model.TrackingNumberUrl = shipmentTracker.GetUrl(shipment.TrackingNumber);
                    if (_shippingSettings.DisplayShipmentEventsToCustomers)
                    {
                        var shipmentEvents = shipmentTracker.GetShipmentEvents(shipment.TrackingNumber);
                        if (shipmentEvents != null)
                            foreach (var shipmentEvent in shipmentEvents)
                            {
                                var shipmentStatusEventModel = new ShipmentDetailsModel.ShipmentStatusEventModel();
                                var shipmentEventCountry = _countryService.GetCountryByTwoLetterIsoCode(shipmentEvent.CountryCode);
                                shipmentStatusEventModel.Country = shipmentEventCountry != null
                                    ? _localizationService.GetLocalized(shipmentEventCountry, x => x.Name) : shipmentEvent.CountryCode;
                                shipmentStatusEventModel.Date = shipmentEvent.Date;
                                shipmentStatusEventModel.EventName = shipmentEvent.EventName;
                                shipmentStatusEventModel.Location = shipmentEvent.Location;
                                model.ShipmentStatusEvents.Add(shipmentStatusEventModel);
                            }
                    }
                }
            }

            //products in this shipment
            model.ShowSku = _catalogSettings.ShowSkuOnProductDetailsPage;
            foreach (var shipmentItem in shipment.ShipmentItems)
            {
                var orderItem = _orderService.GetOrderItemById(shipmentItem.OrderItemId);
                if (orderItem == null)
                    continue;

                var shipmentItemModel = new ShipmentDetailsModel.ShipmentItemModel
                {
                    Id = shipmentItem.Id,
                    Sku = _productService.FormatSku(orderItem.Product, orderItem.AttributesXml),
                    ProductId = orderItem.Product.Id,
                    ProductName = _localizationService.GetLocalized(orderItem.Product, x => x.Name),
                    ProductSeName = _urlRecordService.GetSeName(orderItem.Product),
                    AttributeInfo = orderItem.AttributeDescription,
                    QuantityOrdered = orderItem.Quantity,
                    QuantityShipped = shipmentItem.Quantity,
                };
                //rental info
                if (orderItem.Product.IsRental)
                {
                    var rentalStartDate = orderItem.RentalStartDateUtc.HasValue
                        ? _productService.FormatRentalDate(orderItem.Product, orderItem.RentalStartDateUtc.Value) : "";
                    var rentalEndDate = orderItem.RentalEndDateUtc.HasValue
                        ? _productService.FormatRentalDate(orderItem.Product, orderItem.RentalEndDateUtc.Value) : "";
                    shipmentItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
                        rentalStartDate, rentalEndDate);
                }
                model.Items.Add(shipmentItemModel);
            }

            //order details model
            model.Order = PrepareOrderDetailsModel(order);

            return model;
        }

        /// <summary>
        /// Prepare the customer reward points model
        /// </summary>
        /// <param name="page">Number of items page; pass null to load the first page</param>
        /// <returns>Customer reward points model</returns>
        public virtual CustomerRewardPointsModel PrepareCustomerRewardPoints(int? page)
        {
            //get reward points history
            var customer = _workContext.CurrentCustomer;
            var store = _storeContext.CurrentStore;
            var pageSize = _rewardPointsSettings.PageSize;
            var rewardPoints = _rewardPointService.GetRewardPointsHistory(customer.Id, store.Id, true, pageIndex: --page ?? 0, pageSize: pageSize);

            //prepare model
            var model = new CustomerRewardPointsModel();
            model.RewardPoints = rewardPoints.Select(historyEntry =>
            {
                var activatingDate = _dateTimeHelper.ConvertToUserTime(historyEntry.CreatedOnUtc, DateTimeKind.Utc);
                return new CustomerRewardPointsModel.RewardPointsHistoryModel
                {
                    Points = historyEntry.Points,
                    PointsBalance = historyEntry.PointsBalance.HasValue ? historyEntry.PointsBalance.ToString()
                        : string.Format(_localizationService.GetResource("RewardPoints.ActivatedLater"), activatingDate),
                    Message = historyEntry.Message,
                    CreatedOn = activatingDate,
                    EndDate = !historyEntry.EndDateUtc.HasValue ? null :
                        (DateTime?)_dateTimeHelper.ConvertToUserTime(historyEntry.EndDateUtc.Value, DateTimeKind.Utc)
                };
            }).ToList();

            model.PagerModel = new PagerModel
            {
                PageSize = rewardPoints.PageSize,
                TotalRecords = rewardPoints.TotalCount,
                PageIndex = rewardPoints.PageIndex,
                ShowTotalSummary = true,
                RouteActionName = "CustomerRewardPointsPaged",
                UseRouteLinks = true,
                RouteValues = new RewardPointsRouteValues { pageNumber = page ?? 0 }
            };

            //current amount/balance
            var rewardPointsBalance = _rewardPointService.GetRewardPointsBalance(customer.Id, _storeContext.CurrentStore.Id);
            var rewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(rewardPointsBalance);
            var rewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(rewardPointsAmountBase, _workContext.WorkingCurrency);
            model.RewardPointsBalance = rewardPointsBalance;
            model.RewardPointsAmount = _priceFormatter.FormatPrice(rewardPointsAmount, true, false);

            //minimum amount/balance
            var minimumRewardPointsBalance = _rewardPointsSettings.MinimumRewardPointsToUse;
            var minimumRewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(minimumRewardPointsBalance);
            var minimumRewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(minimumRewardPointsAmountBase, _workContext.WorkingCurrency);
            model.MinimumRewardPointsBalance = minimumRewardPointsBalance;
            model.MinimumRewardPointsAmount = _priceFormatter.FormatPrice(minimumRewardPointsAmount, true, false);

            return model;
        }


        /// <summary>
        /// Prepare the customer wallet list model
        /// </summary>
        /// <returns>Customer wallet list model</returns>
        public virtual CustomerWalletListModel PrepareCustomerWalletListModel()
        {
            var model = new CustomerWalletListModel();
            var wallets = _walletService.SearchWallets(customerId: _workContext.CurrentCustomer.Id);
            decimal totalAmount = 0;
            foreach (var wallet in wallets)
            {
                var walletModel = new CustomerWalletListModel.WalletDetailsModel
                {
                    Id = wallet.Id,
                    Amount = wallet.Amount,
                    Credited = wallet.Credited,
                    CustomerId = wallet.CustomerId,
                    FromWeb = wallet.FromWeb,
                    Note = wallet.Note,
                    OrderId = wallet.OrderId,
                    TransactionOnUtc = _dateTimeHelper.ConvertToUserTime(wallet.TransactionOnUtc, DateTimeKind.Utc),
                };
                if (walletModel.Credited)
                {
                    totalAmount += wallet.Amount;
                    walletModel.DisplayAmount = "+ " + _priceFormatter.FormatPrice(wallet.Amount, true, _workContext.WorkingCurrency);
                }
                else
                {
                    totalAmount -= wallet.Amount;
                    walletModel.DisplayAmount = "- " + _priceFormatter.FormatPrice(wallet.Amount, true, _workContext.WorkingCurrency);

                }
                model.Wallets.Add(walletModel);
            }
            model.BalanceAmount = _priceFormatter.FormatPrice(totalAmount, true, _workContext.WorkingCurrency);
            return model;
        }

        /// <summary>
        /// Prepare the order details model for order tracking. We prepare only required values for Tracking in this method.
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Order details model</returns>
        public virtual OrderDetailsModel PrepareOrderTrackingAjaxModel(Order order)
        {
            //Note: We'll prepare only required value for tracking ajax call 

            if (order == null)
                throw new ArgumentNullException(nameof(order));
            var model = new OrderDetailsModel
            {
                Id = order.Id,
                CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc),
                OrderStatus = _localizationService.GetLocalizedEnum(order.OrderStatus),
                OrderStatusEnum = order.OrderStatus,
                CustomOrderNumber = order.CustomOrderNumber,
            };

            var orderItems = order.OrderItems;

            //vendor address
            if (orderItems.Any())
            {
                var merchantAddress = "";
                var productId = orderItems.Select(x => x.ProductId).FirstOrDefault();
                var vendorId = _productService.GetProductById(productId)?.VendorId ?? 0;
                if (vendorId > 0)
                {
                    var vendor = _vendorService.GetVendorById(vendorId);
                    model.MerchantName = vendor?.Name ?? " ";
                    var vendorAddressId = vendor?.AddressId ?? 0;
                    if (vendorAddressId > 0)
                    {
                        var vendorAddress = _addressService.GetAddressById(vendorAddressId);
                        if (vendorAddress != null)
                        {

                            if (!string.IsNullOrEmpty(vendorAddress.Address1))
                            {
                                merchantAddress += vendorAddress.Address1;
                            }
                            if (!string.IsNullOrEmpty(vendorAddress.Address2))
                            {
                                merchantAddress += ", " + vendorAddress.Address2;
                            }
                            if (!string.IsNullOrEmpty(vendorAddress.City))
                            {
                                merchantAddress += ", " + vendorAddress.City;
                            }
                            if (vendorAddress.StateProvince != null && !string.IsNullOrEmpty(vendorAddress.StateProvince.Name))
                            {
                                merchantAddress += ", " + vendorAddress.StateProvince.Name;
                            }
                            if (!string.IsNullOrEmpty(vendorAddress.ZipPostalCode))
                            {
                                merchantAddress += ", " + vendorAddress.ZipPostalCode;
                            }
                            if (vendorAddress.CountryId > 0)
                            {
                                var country = _countryService.GetCountryById(vendorAddress.CountryId ?? 0);
                                if (country != null)
                                    merchantAddress += ", " + country.Name;
                            }

                            //assign value in tracking model
                            model.LiveTrackingModel.RestauratTrakingInfo.RestaurantId = vendorId;
                            model.LiveTrackingModel.RestauratTrakingInfo.RestauratName = vendor?.Name ?? string.Empty;
                            model.LiveTrackingModel.RestauratTrakingInfo.Latitude = vendorAddress.Latitude;
                            model.LiveTrackingModel.RestauratTrakingInfo.Longitude = vendorAddress.Longitude;
                            model.LiveTrackingModel.RestauratTrakingInfo.RestHelpLink = _settingService.GetSettingByKey<string>("OrderTracking.RestaurantInfo.Helplink", string.Empty, _workContext.GetCurrentStoreId, true);
                            ;
                            model.LiveTrackingModel.RestauratTrakingInfo.RestPhone = vendorAddress.PhoneNumber;
                            //Merchant rating
                            var restAvgRating = _ratingReviewService.GetAverageRating(_workContext.GetCurrentStoreId, vendorId, (int)ReviewType.Merchant, out int reviewCount);
                            model.LiveTrackingModel.RestauratTrakingInfo.RestRatingCount = reviewCount;
                            model.LiveTrackingModel.RestauratTrakingInfo.RestAverageRating = restAvgRating;

                            //prepare picture model
                            var pictureSize = _mediaSettings.VendorThumbPictureSize;
                            var pictureCacheKey = string.Format(NopModelCacheDefaults.VendorPictureModelKey, vendor.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
                            model.LiveTrackingModel.RestauratTrakingInfo.PictureModel = _cacheManager.Get(pictureCacheKey, () =>
                            {
                                var picture = _pictureService.GetPictureById(vendor.PictureId);
                                var pictureModel = new PictureModel
                                {
                                    FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                                    ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                                    Title = string.Format(_localizationService.GetResource("Media.Vendor.ImageLinkTitleFormat"), vendor.Name),
                                    AlternateText = string.Format(_localizationService.GetResource("Media.Vendor.ImageAlternateTextFormat"), vendor.Name)
                                };
                                return pictureModel;
                            });
                        }
                    }
                }
                model.MerchantAddress = merchantAddress;
            }

            //OrderType: 1 = "Delivery", 2 = "Takeaway"
            var details = order.GetOrderDetails();
            var orderType = 0;
            if (details != null && details.OrderType != null)
                orderType = details.OrderType == 1 ? 1 : 2;

            //prepare Rider tracking model and required details
            model.OrderTypeId = orderType;
            model.ShowOrderTracking = _orderService.ShowOrderTracking(order);
            model.LiveTrackingModel = PrepareLiveTrackingModel(model, order);
            model.OrderedTrackingStatuses = PrepareOrderedTrackingStatusList(model, order);

            return model;
        }

        #endregion
    }
}