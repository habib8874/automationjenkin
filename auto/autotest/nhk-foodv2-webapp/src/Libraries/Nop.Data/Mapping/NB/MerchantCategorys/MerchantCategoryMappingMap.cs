﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB.MerchantCategorys;

namespace Nop.Data.Mapping.NB.MerchantCategorys
{
    /// <summary>
    /// Represents a Merchant Category mapping configuration
    /// </summary>
    public partial class MerchantCategoryMappingMap : NopEntityTypeConfiguration<MerchantCategoryMapping>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<MerchantCategoryMapping> builder)
        {
            builder.ToTable(NopMappingDefaults.MerchantCategoryMappingTable);
            builder.HasKey(merchantCategory => merchantCategory.Id);

            builder.HasOne(merchantCategory => merchantCategory.MerchantCategory)
                .WithMany()
                .HasForeignKey(merchantCategory => merchantCategory.MerchantCategoryId)
                .IsRequired();

            builder.HasOne(merchantCategory => merchantCategory.Vendor)
                .WithMany()
                .HasForeignKey(merchantCategory => merchantCategory.VendorId)
                .IsRequired();

            base.Configure(builder);
        }

        #endregion
    }
}