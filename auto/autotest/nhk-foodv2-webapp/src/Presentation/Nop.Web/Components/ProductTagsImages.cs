﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.RatingReview;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Orders;
using Nop.Services.RatingReview;
using Nop.Services.Security;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.RatingReview;
using Nop.Web.Models.ShoppingCart;
using System.Collections.Generic;
using System.Linq;
using Nop.Services.Localization;
using Nop.Services.Media;

namespace Nop.Web.Components
{
    public class ProductTagsImagesViewComponent : NopViewComponent
    {
        private readonly IProductTagImageMappingService _productTagImageMappingService;
        private readonly IProductTagService _producttagService;
        private readonly IProductService _productService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IPictureService _pictureService;
        private readonly ILocalizationService _localizationService;

        public ProductTagsImagesViewComponent(IProductTagImageMappingService productTagImageMappingService, IProductTagService producttagService,
            IWorkContext workContext, IStoreContext storeContext,
            IPictureService pictureService,
            ILocalizationService localizationService)
        {
            this._productTagImageMappingService = productTagImageMappingService;
            this._producttagService = producttagService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._pictureService = pictureService;
            this._localizationService = localizationService;
        }

        public IViewComponentResult Invoke(bool showAll = true, int productId = 0)
        {            
            var storeId = _workContext.GetCurrentStoreId;
            var tags = _producttagService.GetAllProductTagsByStoreId(storeId);
            var model = new ProductTagsImagesModel();
            model.ShowAll = showAll;
            if (!showAll)
            {
                tags = tags.Where(x => x.ProductProductTagMappings.Any(p => p.ProductId == productId)).ToList();
            }
                        

            foreach (var tag in tags)
            {
                string tagImage = string.Empty;
                var productTagImage = _productTagImageMappingService.GetProductTagImageMappingByTagId(tag.Id);
                if (productTagImage != null && productTagImage.Picture_Id > 0)
                {
                    var tagPicture = _pictureService.GetPictureById(productTagImage.Picture_Id);
                    tagImage = _pictureService.GetPictureUrl(tagPicture, 75, true);
                }
                model.TagImages.Add(new ProductTagsImagesModel.ProductTagsImagesListModel { Id = tag.Id, TagName = _localizationService.GetLocalized(tag, x => x.Name), ImageUrl = tagImage });
            }
            return View(model);
        }
    }
}
