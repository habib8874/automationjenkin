﻿using System.Collections.Generic;
using Nop.Web.Models.Directory;

namespace Nop.Web.Factories
{
    /// <summary>
    /// Represents the interface of the country model factory
    /// </summary>
    public partial interface ICountryModelFactory
    {
        /// <summary>
        /// Get states and provinces by country identifier
        /// </summary>
        /// <param name="countryId">Country identifier</param>
        /// <param name="addSelectStateItem">Whether to add "Select state" item to list of states</param>
        /// <returns>List of identifiers and names of states and provinces</returns>
        IList<StateProvinceModel> GetStatesByCountryId(string countryId, bool addSelectStateItem);

        /// <summary>
        /// Get city by states and provinces identifier
        /// </summary>
        /// <param name="stateId">state identifier</param>
        /// <param name="addSelectStateItem">Whether to add "Select city" item to list of citys</param>
        /// <returns>List of identifiers and names of city</returns>
        IList<CityModel> GetCityByStateId(string stateId, bool addSelectStateItem);
    }
}
