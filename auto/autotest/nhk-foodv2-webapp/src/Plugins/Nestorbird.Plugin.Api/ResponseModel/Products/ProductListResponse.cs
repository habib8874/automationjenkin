﻿using System.Collections.Generic;

namespace Nestorbird.Plugin.Api.ResponseModel.Products
{
    public class ProductListResponse : PagingResponse
    {
        public ProductListResponse()
        {
            Products = new List<ProductItemResponse>();
        }

        public IList<ProductItemResponse> Products { get; set; }
    }

    public class ProductItemResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ShortDescription { get; set; }

        public string FullDescription { get; set; }

        public string ImageUrl { get; set; }

        public int StockQuantity { get; set; }

        public string Price { get; set; }
    }
}
