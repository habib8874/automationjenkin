﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Localization
{
    /// <summary>
    /// Represents a locale resource model
    /// </summary>
    public partial class LocaleResourceModel : BaseNopEntityModel
    {
        public LocaleResourceModel()
        {
            AvailableStores = new List<SelectListItem>();
        }
        #region Properties

        [NopResourceDisplayName("Admin.Configuration.Languages.Resources.Fields.Name")]
        public string ResourceName { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Languages.Resources.Fields.Value")]
        public string ResourceValue { get; set; }

        public int LanguageId { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Languages.Resources.Fields.StoreName")]
        public string Store { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Languages.Resources.Fields.Store")]
        public int StoreId { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }

        #endregion
    }
}