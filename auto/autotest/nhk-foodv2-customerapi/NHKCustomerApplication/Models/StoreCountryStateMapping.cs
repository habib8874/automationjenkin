﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class StoreCountryStateMapping
    {
        public int Id { get; set; }
        public int? StoreId { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public int? StateId { get; set; }
        public string StateName { get; set; }
    }
}
