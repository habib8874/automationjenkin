namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents an agent status enumeration
    /// </summary>
    public enum AgentStatus
    {
        /// <summary>
        /// Agent is assigned
        /// </summary>
        Assigned = 2,
        /// <summary>
        /// Agent is unassigned
        /// </summary>
        Unassigned = 0
    }
}
