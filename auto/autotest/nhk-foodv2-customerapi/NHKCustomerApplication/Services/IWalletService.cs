﻿using NHKCustomerApplication.Models;
using NHKCustomerApplication.ViewModels;
using System.Collections.Generic;

namespace NHKCustomerApplication.Services
{
    public interface IWalletService
    {
        decimal GetCurrentBalance(int customerId);
        List<WalletRequestModel> GetHistory(int customerId);

        void AddMoney(WalletRequestModel requestModel);

    }
}
