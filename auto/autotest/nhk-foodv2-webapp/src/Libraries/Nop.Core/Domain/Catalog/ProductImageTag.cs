using System.Collections.Generic;
using Nop.Core.Domain.Localization;

namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents a product tag
    /// </summary>
    public partial class ProductImageTag : BaseEntity
    {
        /// <summary>
        /// Gets or sets the TagId
        /// </summary>
        public int TagId { get; set; }

        /// <summary>
        /// Gets or sets the Url
        /// </summary>
        public int Picture_Id { get; set; }
    }
}
