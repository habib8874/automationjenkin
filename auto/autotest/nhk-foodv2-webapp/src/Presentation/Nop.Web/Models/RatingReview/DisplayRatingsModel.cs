﻿using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Models.RatingReview
{
    public partial class DisplayRatingsModel : BaseNopEntityModel
    {
        public int ReviewType { get; set; }

        public decimal RatingPercent { get; set; }
    }
}