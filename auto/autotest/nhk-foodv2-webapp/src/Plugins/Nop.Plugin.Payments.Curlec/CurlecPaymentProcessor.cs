﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Plugins;
using Nop.Services.Tax;

namespace Nop.Plugin.Payments.Curlec
{
    /// <summary>
    /// Curlec payment processor
    /// </summary>
    public class CurlecPaymentProcessor : BasePlugin, IPaymentMethod
    {
        #region Fields

        private readonly CurrencySettings _currencySettings;
        private readonly ICheckoutAttributeParser _checkoutAttributeParser;
        private readonly ICurrencyService _currencyService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILocalizationService _localizationService;
        private readonly IPaymentService _paymentService;
        private readonly ISettingService _settingService;
        private readonly ITaxService _taxService;
        private readonly IWebHelper _webHelper;
        private readonly CurlecPaymentSettings _curlecPaymentSettings;

        #endregion

        #region Ctor

        public CurlecPaymentProcessor(CurrencySettings currencySettings,
            ICheckoutAttributeParser checkoutAttributeParser,
            ICurrencyService currencyService,
            IGenericAttributeService genericAttributeService,
            IHttpContextAccessor httpContextAccessor,
            ILocalizationService localizationService,
            IPaymentService paymentService,
            ISettingService settingService,
            ITaxService taxService,
            IWebHelper webHelper,
            CurlecPaymentSettings curlecPaymentSettings)
        {
            _currencySettings = currencySettings;
            _checkoutAttributeParser = checkoutAttributeParser;
            _currencyService = currencyService;
            _genericAttributeService = genericAttributeService;
            _httpContextAccessor = httpContextAccessor;
            _localizationService = localizationService;
            _paymentService = paymentService;
            _settingService = settingService;
            _taxService = taxService;
            _webHelper = webHelper;
            _curlecPaymentSettings = curlecPaymentSettings;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Create common query parameters for the request
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Created query parameters</returns>
        private IDictionary<string, string> CreateQueryParameters(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            //get store location
            var storeLocation = _webHelper.GetStoreLocation();

            //choosing correct order address
            var orderAddress = postProcessPaymentRequest.Order.PickupInStore
                    ? postProcessPaymentRequest.Order.PickupAddress
                    : postProcessPaymentRequest.Order.ShippingAddress;

            var roundedOrderTotal = Math.Round(postProcessPaymentRequest.Order.OrderTotal, 2);

            //create query parameters
            return new Dictionary<string, string>
            {
                ["orderNo"] = Convert.ToString(postProcessPaymentRequest.Order.Id),
                ["email"] = orderAddress?.Email,
                ["description"] = "New-Instant-Pay-Order-" + Convert.ToString(postProcessPaymentRequest.Order.Id),
                ["amount"] = roundedOrderTotal.ToString("0.00", CultureInfo.InvariantCulture),
                ["bankCode"] = "TEST0021",
                ["merchantId"] = _curlecPaymentSettings.MerchantID,
                ["employeeId"] = _curlecPaymentSettings.EmployeeID,
                ["method"] = "03",
            };
        }

        #endregion

        #region Methods

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            return new ProcessPaymentResult();
        }

        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            var baseUrl = _curlecPaymentSettings.MerchantURL + "/new-instant-pay";

            //create common query parameters for the request
            var queryParameters = CreateQueryParameters(postProcessPaymentRequest);

            //remove null values from parameters
            queryParameters = queryParameters.Where(parameter => !string.IsNullOrEmpty(parameter.Value))
                .ToDictionary(parameter => parameter.Key, parameter => parameter.Value);

            var url = QueryHelpers.AddQueryString(baseUrl, queryParameters);
            _httpContextAccessor.HttpContext.Response.Redirect(url);
        }

        /// <summary>
        /// Returns a value indicating whether payment method should be hidden during checkout
        /// </summary>
        /// <param name="cart">Shopping cart</param>
        /// <returns>true - hide; false - display.</returns>
        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            //you can put any logic here
            //for example, hide this payment method if all products in the cart are downloadable
            //or hide this payment method if current customer is from certain country
            return false;
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <param name="cart">Shopping cart</param>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            return 0;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            return new CapturePaymentResult { Errors = new[] { "Capture method not supported" } };
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            return new RefundPaymentResult { Errors = new[] { "Refund method not supported" } };
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            return new VoidPaymentResult { Errors = new[] { "Void method not supported" } };
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            return new ProcessPaymentResult { Errors = new[] { "Recurring payment not supported" } };
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            return new CancelRecurringPaymentResult { Errors = new[] { "Recurring payment not supported" } };
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            //let's ensure that at least 5 seconds passed after order is placed
            //P.S. there's no any particular reason for that. we just do it
            if ((DateTime.UtcNow - order.CreatedOnUtc).TotalSeconds < 5)
                return false;

            return true;
        }

        /// <summary>
        /// Validate payment form
        /// </summary>
        /// <param name="form">The parsed form values</param>
        /// <returns>List of validating errors</returns>
        public IList<string> ValidatePaymentForm(IFormCollection form)
        {
            return new List<string>();
        }

        /// <summary>
        /// Get payment information
        /// </summary>
        /// <param name="form">The parsed form values</param>
        /// <returns>Payment info holder</returns>
        public ProcessPaymentRequest GetPaymentInfo(IFormCollection form)
        {
            return new ProcessPaymentRequest();
        }

        /// <summary>
        /// Gets a configuration page URL
        /// </summary>
        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/PaymentCurlec/Configure";
        }

        /// <summary>
        /// Gets a name of a view component for displaying plugin in public store ("payment info" checkout step)
        /// </summary>
        /// <returns>View component name</returns>
        public string GetPublicViewComponentName()
        {
            return "PaymentCurlec";
        }

        /// <summary>
        /// Install the plugin
        /// </summary>
        public override void Install()
        {
            //locales
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.Fields.MerchantID", "Merchant ID");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.Fields.MerchantID.Hint", "Specify your Merchant ID.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.Fields.EmployeeID", "Employee ID");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.Fields.EmployeeID.Hint", "Specify your Employee ID.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.Fields.MerchantURL", "Merchant URL");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.Fields.MerchantURL.Hint", "Specify your Merchant URL.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.Fields.MerchantCallbackURL", "Merchant Callback URL");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.Fields.MerchantCallbackURL.Hint", "Specify your Merchant Callback URL.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.Fields.CollectionCallbackURL", "Collection Callback URL");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.Fields.CollectionCallbackURL.Hint", "Specify your Collection Callback URL.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.Fields.EnrollmentCallbackURL", "Enrollment Callback URL");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.Fields.EnrollmentCallbackURL.Hint", "Specify your Enrollment Callback URL.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.Fields.RedirectionTip", "You will be redirected to Curlec site to complete the order.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.PaymentMethodDescription", "You will be redirected to Curlec site to complete the payment");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Curlec.RoundingWarning", "It looks like you have \"ShoppingCartSettings.RoundPricesDuringCalculation\" setting disabled. Keep in mind that this can lead to a discrepancy of the order total amount, as Curlec only rounds to two decimals.");

            base.Install();
        }

        /// <summary>
        /// Uninstall the plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<CurlecPaymentSettings>();

            //locales
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.Fields.MerchantID");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.Fields.MerchantID.Hint");

            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.Fields.EmployeeID");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.Fields.EmployeeID.Hint");

            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.Fields.MerchantURL");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.Fields.MerchantURL.Hint");

            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.Fields.MerchantCallbackURL");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.Fields.MerchantCallbackURL.Hint");

            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.Fields.CollectionCallbackURL");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.Fields.CollectionCallbackURL.Hint");

            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.Fields.EnrollmentCallbackURL");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.Fields.EnrollmentCallbackURL.Hint");

            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.Fields.RedirectionTip");

            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.PaymentMethodDescription");

            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Curlec.RoundingWarning");

            base.Uninstall();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture => false;

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund => false;

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund => false;

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid => false;

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType => RecurringPaymentType.NotSupported;

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType => PaymentMethodType.Redirection;

        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo => false;

        /// <summary>
        /// Gets a payment method description that will be displayed on checkout pages in the public store
        /// </summary>
        public string PaymentMethodDescription => _localizationService.GetResource("Plugins.Payments.Curlec.PaymentMethodDescription");

        #endregion
    }
}