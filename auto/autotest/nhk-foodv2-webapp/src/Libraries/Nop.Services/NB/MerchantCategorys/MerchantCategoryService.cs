﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.NB.MerchantCategorys;
using Nop.Core.Domain.Vendors;
using Nop.Services.Common;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.NB.MerchantCategorys
{
    /// <summary>
    /// MerchantCategory property service
    /// </summary>
    public partial class MerchantCategoryService : IMerchantCategoryService
    {

        #region Fields

        private readonly IRepository<MerchantCategory> _merchantCategoryRepository;
        private readonly IRepository<MerchantCategoryMapping> _merchantCategoryMappingRepository;
        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        private readonly ICommonService _commonService;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="workContext">Work context</param>
        /// <param name="storeContext">Store context</param>
        /// <param name="eventPublisher">Event publisher</param>
        public MerchantCategoryService(IRepository<MerchantCategory> merchantCategoryRepository,
            IRepository<MerchantCategoryMapping> merchantCategoryMappingRepository,
            IRepository<Vendor> vendorRepository,
            IWorkContext workContext,
            IStoreContext storeContext,
            IEventPublisher eventPublisher,
            ICacheManager cacheManager,
            ICommonService commonService
)
        {
            _merchantCategoryRepository = merchantCategoryRepository;
            _merchantCategoryMappingRepository = merchantCategoryMappingRepository;
            _workContext = workContext;
            _storeContext = storeContext;
            _eventPublisher = eventPublisher;
            _cacheManager = cacheManager;
            _vendorRepository = vendorRepository;
            _commonService = commonService;
        }

        #endregion

        #region MerchantCategory

        /// <summary>
        /// Gets a MerchantCategory  
        /// </summary>
        /// <param name="MerchantCategoryId">MerchantCategory  identifier</param>
        /// <returns>MerchantCategory </returns>
        public virtual MerchantCategory GetMerchantCategoryById(int merchantCategoryId)
        {
            if (merchantCategoryId == 0)
                return null;

            return _merchantCategoryRepository.GetById(merchantCategoryId);
        }

        /// <summary>
        /// Gets a MerchantCategory 
        /// </summary>
        /// <param name="storeId">storeId  identifier</param>
        /// <param name="searchName">searchName  identifier</param>
        /// <returns>MerchantCategory </returns>
        public virtual IList<MerchantCategory> GetAllMerchantCategory(int storeId = 0, string searchName = null, IList<int> vendorIds = null)
        {
            var query = from eq in _merchantCategoryRepository.Table
                        where !eq.Deleted
                        select eq;

            if (storeId > 0)
                query = query.Where(x => x.StoreId == storeId);
            //filter by sub admin
            if (vendorIds != null && vendorIds.Any())
            {
                query = from m in query
                        join mm in _merchantCategoryMappingRepository.Table
                            on m.Id equals mm.MerchantCategoryId
                        where vendorIds.Contains(mm.VendorId)
                        select m;
                query = query.GroupBy(x => x.Id)
                             .Select(g => g.First());
            }

            if (!string.IsNullOrWhiteSpace(searchName))
                query = query.Where(x => x.Name.Contains(searchName));

            return query.OrderBy(x => x.DisplayOrder).ToList();
        }

        /// <summary>
        /// Inserts a MerchantCategory
        /// </summary>
        /// <param name="MerchantCategory">MerchantCategory </param>
        public virtual void InsertMerchantCategory(MerchantCategory merchantCategory)
        {
            if (merchantCategory == null)
                throw new ArgumentNullException("merchantCategory");

            _merchantCategoryRepository.Insert(merchantCategory);
            _commonService.ClearCache(_workContext.GetCurrentStoreId, string.Format("API.Cache.Category-{0}",_workContext.GetCurrentStoreId));

            //event notification
            _eventPublisher.EntityInserted(merchantCategory);
        }

        /// <summary>
        /// Updates the MerchantCategory 
        /// </summary>
        /// <param name="MerchantCategory">MerchantCategory </param>
        public virtual void UpdateMerchantCategory(MerchantCategory merchantCategory)
        {
            if (merchantCategory == null)
                throw new ArgumentNullException("merchantCategory");

            _merchantCategoryRepository.Update(merchantCategory);
            _commonService.ClearCache(_workContext.GetCurrentStoreId, string.Format("API.Cache.Category-{0}", _workContext.GetCurrentStoreId));


            //event notification
            _eventPublisher.EntityUpdated(merchantCategory);
        }

        /// <summary>
        /// Delete a MerchantCategory
        /// </summary>
        /// <param name="MerchantCategory">MerchantCategory</param>
        public virtual void DeleteMerchantCategory(MerchantCategory merchantCategory)
        {
            if (merchantCategory == null)
                throw new ArgumentNullException("merchantCategory");

            _merchantCategoryRepository.Delete(merchantCategory);
            _commonService.ClearCache(_workContext.GetCurrentStoreId, string.Format("API.Cache.Category-{0}", _workContext.GetCurrentStoreId));


            //event notification
            _eventPublisher.EntityDeleted(merchantCategory);
        }

        /// <summary>
        /// Gets a MerchantCategory  
        /// </summary>
        /// <param name="MerchantCategoryIds">MerchantCategory  identifier</param>
        /// <returns>MerchantCategory list</returns>
        public virtual IList<MerchantCategory> GetMerchantCategorysByIds(int[] merchantCategoryIds)
        {
            if (merchantCategoryIds == null || merchantCategoryIds.Length == 0)
                return new List<MerchantCategory>();

            var query = from p in _merchantCategoryRepository.Table
                        where merchantCategoryIds.Contains(p.Id) && !p.Deleted && p.Published
                        orderby p.DisplayOrder
                        select p;

            return query.ToList();
        }
        #endregion

        #region MerchantCategoryMapping


        /// <summary>
        /// Inserts a MerchantCategoryMapping
        /// </summary>
        /// <param name="MerchantCategoryMapping">MerchantCategoryMapping</param>
        public virtual void InsertMerchantCategoryMapping(MerchantCategoryMapping merchantCategoryMapping)
        {
            if (merchantCategoryMapping == null)
                throw new ArgumentNullException(nameof(merchantCategoryMapping));

            _merchantCategoryMappingRepository.Insert(merchantCategoryMapping);
            _commonService.ClearCache(_workContext.GetCurrentStoreId, string.Format("API.Cache.Category-{0}", _workContext.GetCurrentStoreId));


            //event notification
            _eventPublisher.EntityInserted(merchantCategoryMapping);
        }


        /// <summary>
        /// Gets all MerchantCategoryMapping
        /// </summary>
        /// <returns>MerchantCategoryMapping list</returns>
        public virtual IList<MerchantCategoryMapping> GetAllMerchantCategoryMapping()
        {
            var query = _merchantCategoryMappingRepository.Table;
            var questionTags = query.ToList();
            return questionTags;
        }


        /// <summary>
        /// Gets Merchant Category mapping collection
        /// </summary>
        /// <param name="merchantCategoryId">MerchantCategory identifier</param>
        /// <returns>Merchant Category mapping collection</returns>
        public virtual IList<MerchantCategoryMapping> GetMerchantCategoryMerchantsByMerchantCategoryId(int merchantCategoryId)
        {
            if (merchantCategoryId == 0)
                return new List<MerchantCategoryMapping>();

            var query = from mcm in _merchantCategoryMappingRepository.Table
                        join v in _vendorRepository.Table on mcm.VendorId equals v.Id
                        where mcm.MerchantCategoryId == merchantCategoryId &&
                              !v.Deleted && v.Active
                        select mcm;

            var questionTags = query.ToList();
            return questionTags;
        }

        /// <summary>
        /// Gets a MerchantCategoryMapping  
        /// </summary>
        /// <param name="MerchantCategoryMappingId">MerchantCategoryMapping  identifier</param>
        /// <returns>MerchantCategory </returns>
        public virtual MerchantCategoryMapping GetMerchantCategoryMappingById(int merchantCategoryMappingId)
        {
            if (merchantCategoryMappingId == 0)
                return null;

            return _merchantCategoryMappingRepository.GetById(merchantCategoryMappingId);
        }

        /// <summary>
        /// Delete a merchant category mapping
        /// </summary>
        /// <param name="MerchantCategoryMapping">MerchantCategoryMapping</param>
        public virtual void DeleteMerchantCategoryMapping(MerchantCategoryMapping merchantCategoryMapping)
        {
            if (merchantCategoryMapping == null)
                throw new ArgumentNullException("merchantCategoryMapping");

            _merchantCategoryMappingRepository.Delete(merchantCategoryMapping);
            _commonService.ClearCache(_workContext.GetCurrentStoreId, string.Format("API.Cache.Category-{0}", _workContext.GetCurrentStoreId));

            //event notification
            _eventPublisher.EntityDeleted(merchantCategoryMapping);
        }


        /// <summary>
        /// Returns a MerchantCategoryMapping that has the specified values
        /// </summary>
        /// <param name="source">Source</param>
        /// <param name="vendorId">Vendor identifier</param>
        /// <param name="merchantCategoryId">merchantCategory identifier</param>
        /// <returns>A MerchantCategoryMapping that has the specified values; otherwise null</returns>
        public virtual MerchantCategoryMapping FindMerchantCategoryMapping(IList<MerchantCategoryMapping> source, int vendorId, int merchantCategoryId)
        {
            foreach (var merchantCategory in source)
                if (merchantCategory.VendorId == vendorId && merchantCategory.MerchantCategoryId == merchantCategoryId)
                    return merchantCategory;

            return null;
        }
        #endregion
    }
}
