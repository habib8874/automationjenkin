﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.Suppliers
{
    public partial class SupplierProductModel:BaseNopEntityModel
    {

        #region Properties

        [NopResourceDisplayName("Admin.Supplier.Products.Fields.SupplierId")]
        public int SupplierId { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Products.Fields.ProductId")]
        public int ProductId { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Products.Fields.ProductName")]
        public string ProductName { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Products.Fields.Price")]
        public decimal Price { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Products.Fields.IsFeaturedProduct")]
        public bool IsFeaturedProduct { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Products.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        #endregion
    }
}
