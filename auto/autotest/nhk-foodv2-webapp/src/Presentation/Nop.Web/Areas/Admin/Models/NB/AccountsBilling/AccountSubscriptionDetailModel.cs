﻿using System;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.AccountsBilling
{
    /// <summary>
    /// Represents a Account  SubscriptionDetail model
    /// </summary>
    public partial class AccountSubscriptionDetailModel : BaseNopEntityModel
    {

        #region Properties

        public int PackagePlanId { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.AccountSubscriptionDetail.Field.PackageName")]
        public string PackageName { get; set; }

        public string PackageId { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.AccountSubscriptionDetail.Field.PackagePlanName")]
        public string PackagePlanName { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.AccountSubscriptionDetail.Field.Validaty")]
        public DateTime Validaty { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.AccountSubscriptionDetail.Field.NextDueDate")]
        public DateTime NextDueDate { get; set; }

        [NopResourceDisplayName("NB.Admin.AccountBillings.AccountSubscriptionDetail.Field.Amount")]
        public decimal Amount { get; set; }
        public string PlanPrice { get; set; }
        #endregion
    }
}
