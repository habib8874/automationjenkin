﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class BookingTableMasterDetail
    {
        public int Id { get; set; }
        public TimeSpan? TableAvailableFrom { get; set; }
        public TimeSpan? TableAvailableTo { get; set; }
        public int? ImageId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int BookingMasterId { get; set; }
        public string ImageUrl { get; set; }
    }
}
