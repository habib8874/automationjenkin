﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Models.NB
{
    /// <summary>
    /// Represents a DeliverySlot model
    /// </summary>
    public partial class DeliverySlotModel : BaseNopEntityModel
    {
        public DeliverySlotModel() 
        {
           
        }

        public string Name { get; set; }
        public bool IsDefaultSlot { get; set; }

        public int Duration { get; set; }
        public DateTime StartDateUtc { get; set; }
        public DateTime EndDateUtc { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int NumberOfOrders { get; set; }
        public decimal SurChargeFee { get; set; }
        public int DisplayOrder { get; set; }
        public int StoreId { get; set; }
        public int MerchantId { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the entity is published
        /// </summary>
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }

        public string StartDateString { get; set; }

        public string SurChargeFeeString { get; set; }
    }
}