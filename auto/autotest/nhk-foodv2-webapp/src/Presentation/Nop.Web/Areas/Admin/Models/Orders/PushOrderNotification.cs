﻿using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.Orders
{
    public class PushOrderNotification
    {
        public int Id { get; set; }
        public string ApiKey { get; set; }
        public string WebAddr { get; set; }
        public string WebKey { get; set; }
        public string To { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Heading { get; set; }
        public string OrderId { get; set; }
        public string AgentId { get; set; }
        public string CustomerName { get; set; }
        public string MerchantName { get; set; }
        public string PickupLocation { get; set; }
        public string PickupLat { get; set; }
        public string PickupLong { get; set; }
        public string PickupMobileNo { get; set; }
        public string DeliveryLocation { get; set; }
        public string DeliveryLat { get; set; }
        public string DeliveryLong { get; set; }
        public string DeliveryMobileNo { get; set; }
        public List<PushOrderItems> Items { get; set; }
        public string TotalCost { get; set; }
        public string Action { get; set; }
        public string DeliveryTime { get; set; }
    }

    
    }
