﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Areas.Admin.Models.Common;
using Nop.Web.Areas.Admin.Validators.Vendors;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.VendorGallery
{
   // [Validator(typeof(VendorValidator))]
    public partial class VendorGalleryModel : BaseNopEntityModel
    {
        public VendorGalleryModel()
        {

            AvailableStores = new List<SelectListItem>();
            AvailableVendors = new List<SelectListItem>();
            AvailableGalleryType = new List<SelectListItem>();
        }
        public int Id { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.Store")]
        public int StoreId { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.Vendor")]
        public int VendorId { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.GalleryType")]
        public int GalleryType { get; set; }

        public IList<SelectListItem> AvailableStores { get; set; }
        public IList<SelectListItem> AvailableVendors { get; set; }
        public IList<SelectListItem> AvailableGalleryType { get; set; }
        [UIHint("Picture")]
        [NopResourceDisplayName("Admin.VendorGallery.Fields.Picture")]
        public int PictureId { get; set; }

        [NopResourceDisplayName("Admin.VendorGallery.Fields.GalleryURL")]
        public string GalleryURL { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.GalleryCaption")]
        public string GalleryCaption { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.Active")]
        public bool Active { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.Deleted")]
        public bool Deleted { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }
    }
    
  
}