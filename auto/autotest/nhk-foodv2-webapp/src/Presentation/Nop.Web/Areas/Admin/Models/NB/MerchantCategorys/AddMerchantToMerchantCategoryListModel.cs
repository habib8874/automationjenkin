﻿using Nop.Web.Areas.Admin.Models.Vendors;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB.MerchantCategorys
{
    /// <summary>
    /// Represents a Merchant list model to add to the Merchant category
    /// </summary>
    public partial class AddMerchantToMerchantCategoryListModel : BasePagedListModel<VendorModel>
    {
    }
}