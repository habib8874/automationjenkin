﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB
{
    /// <summary>
    /// Represents a tip list model
    /// </summary>
    public partial class TipListModel : BasePagedListModel<TipModel>
    {
    }
}