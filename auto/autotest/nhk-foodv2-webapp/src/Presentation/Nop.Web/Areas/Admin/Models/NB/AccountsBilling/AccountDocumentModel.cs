﻿using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.AccountsBilling
{
    /// <summary>
    /// Represents a Account Billing model
    /// </summary>
    public partial class AccountDocumentModel : BaseNopEntityModel
    {

        #region Properties

        [UIHint("Picture")]
        [NopResourceDisplayName("NB.Admin.AccountBillings.MerchantBilling.Field.IdentityProofFront")]
        public int IdentityProofFront { get; set; }

        [UIHint("Picture")]
        [NopResourceDisplayName("NB.Admin.AccountBillings.MerchantBilling.Field.IdentityProofBack")]
        public int IdentityProofBack { get; set; }

        #endregion
    }
}
