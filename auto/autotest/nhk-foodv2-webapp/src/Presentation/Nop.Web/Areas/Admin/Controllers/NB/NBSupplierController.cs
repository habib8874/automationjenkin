﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.NB.Suppliers;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.NB.Suppliers;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.NB.Suppliers;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers.NB
{
    public partial class NBSupplierController : BaseAdminController
    {

        #region Fields

        private readonly IAddressService _addressService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ICustomerService _customerService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly INbSupplierModelFactory _supplierModelFactory;
        private readonly ISupplierService _supplierService;
        private readonly ISettingService _settingService;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IProductService _productService;


        #endregion

        #region Ctor

        public NBSupplierController(IAddressService addressService,
            ICustomerActivityService customerActivityService,
            ICustomerService customerService,
            IProductService productService,
            IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService,
            INotificationService notificationService,
            IPermissionService permissionService,
            ISupplierService supplierService,
            INbSupplierModelFactory supplierModelFactory,
            ISettingService settingService,
            IWorkContext workContext,
            IStoreContext storeContext,
            IStoreService storeService)
        {
            _storeContext = storeContext;
            _addressService = addressService;
            _customerActivityService = customerActivityService;
            _customerService = customerService;
            _genericAttributeService = genericAttributeService;
            _localizationService = localizationService;
            _localizedEntityService = localizedEntityService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _productService = productService;
            _supplierModelFactory = supplierModelFactory;
            _supplierService = supplierService;
            _workContext = workContext;
            _storeService = storeService;
            _settingService = settingService;
        }

        #endregion



        #region Suppliers

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSuppliers))
                return AccessDeniedView();

            //prepare model
            var model = _supplierModelFactory.PrepareSupplierSearchModel(new SupplierSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(SupplierSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSuppliers))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _supplierModelFactory.PrepareSupplierListModel(searchModel);

            return Json(model);
        }

        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSuppliers))
                return AccessDeniedView();


            //prepare model
            var model = _supplierModelFactory.PrepareSupplierModel(new SupplierModel(), null);


            //StoreId  
            int storeId = 0;
            if (_workContext.IsAdminRole)
            {
                storeId = _storeContext.CurrentStore.Id;
            }
            else if (_workContext.IsStoreOwnerRole)
            {
                storeId = _storeService.GetAllStores().FirstOrDefault(s => s.StoreOwnerId == _workContext.CurrentCustomer.Id).Id;
            }

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Create(SupplierModel model, bool continueEditing, IFormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSuppliers))
                return AccessDeniedView();


            if (ModelState.IsValid)
            {
                var supplier = new Supplier();
                supplier.SupplierName = model.SupplierName;
                supplier.Description = model.Description;
                supplier.Geofancing = model.Geofancing;
                supplier.LocationMap = model.LocationMap;
                supplier.StoreId = model.StoreId;
                supplier.DeliveryFee = model.DeliveryFee;
                supplier.FixedOrDynamic = model.FixedOrDynamic;
                supplier.KmOrMilesForDynamic = model.KmOrMilesForDynamic;
                supplier.PriceForDynamic = model.PriceForDynamic;
                supplier.DistanceForDynamic = model.DistanceForDynamic;
                supplier.PricePerUnitDistanceForDynamic = model.PricePerUnitDistanceForDynamic;


                _supplierService.InsertSupplier(supplier);

                //activity log
                _customerActivityService.InsertActivity("AddNewSupplier",
                    string.Format(_localizationService.GetResource("ActivityLog.AddNewSupplier"), supplier.Id), supplier);

                //address
                var address = model.Address.ToEntity<Address>();
                address.CreatedOnUtc = DateTime.UtcNow;

                //some validation
                if (address.CountryId == 0)
                    address.CountryId = null;
                if (address.StateProvinceId == 0)
                    address.StateProvinceId = null;
                _addressService.InsertAddress(address);
                supplier.AddressId = address.Id;
                _supplierService.UpdateSupplier(supplier);

                if (!continueEditing)
                    return RedirectToAction("List");

                return RedirectToAction("Edit", new { id = supplier.Id });
            }

            //prepare model
            model = _supplierModelFactory.PrepareSupplierModel(model, null, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSuppliers))
                return AccessDeniedView();

            //try to get a vendor with the specified id
            var supplier = _supplierService.GetSupplierById(id);
            if (supplier == null || supplier.Deleted)
                return RedirectToAction("List");

            //prepare model
            var model = _supplierModelFactory.PrepareSupplierModel(null, supplier);

            return View(model);
        }


        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(SupplierModel model, bool continueEditing, IFormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSuppliers))
                return AccessDeniedView();

            //try to get a vendor with the specified id
            var supplier = _supplierService.GetSupplierById(model.Id);
            if (supplier == null || supplier.Deleted)
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                supplier.Id = model.Id;
                supplier.SupplierName = model.SupplierName;
                supplier.Description = model.Description;
                supplier.StoreId = model.StoreId;
                supplier.DeliveryFee = model.DeliveryFee;
                supplier.Active = model.Active;
                supplier.FixedOrDynamic = model.FixedOrDynamic;
                supplier.KmOrMilesForDynamic = model.KmOrMilesForDynamic;
                supplier.PriceForDynamic = model.PriceForDynamic;
                supplier.DistanceForDynamic = model.DistanceForDynamic;
                supplier.PricePerUnitDistanceForDynamic = model.PricePerUnitDistanceForDynamic;

                _supplierService.UpdateSupplier(supplier);

                //address
                var address = _addressService.GetAddressById(supplier.AddressId);
                if (address == null)
                {
                    address = model.Address.ToEntity<Address>();
                    address.CreatedOnUtc = DateTime.UtcNow;

                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;

                    _addressService.InsertAddress(address);
                    supplier.AddressId = address.Id;
                    _supplierService.UpdateSupplier(supplier);
                }
                else
                {
                    address = model.Address.ToEntity(address);

                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;

                    _addressService.UpdateAddress(address);
                }

                if (!continueEditing)
                    return RedirectToAction("List");

                return RedirectToAction("Edit", new { id = supplier.Id });

            }
            //prepare model
            model = _supplierModelFactory.PrepareSupplierModel(model, supplier, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSuppliers))
                return AccessDeniedView();

            //try to get a vendor with the specified id
            var supplier = _supplierService.GetSupplierById(id);
            if (supplier == null)
                return RedirectToAction("List");

            //delete a vendor
            _supplierService.DeleteSupplier(supplier);

            //activity log
            _customerActivityService.InsertActivity("DeleteSupplier",
                string.Format(_localizationService.GetResource("ActivityLog.DeleteSupplier"), supplier.Id), supplier);


            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Supplier.Deleted"));

            return RedirectToAction("List");
        }


        [HttpPost]
        public virtual IActionResult DeleteSelected(ICollection<int> selectedIds)
        {


            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSuppliers))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                _supplierService.DeleteSuppliers(selectedIds);
            }

            return Json(new { Result = true });

        }



        #region Products

        #region Bulk Edit
        public virtual IActionResult BulkEdit()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSuppliers))
                return AccessDeniedView();

            //prepare model
            var model = _supplierModelFactory.PrepareSupplierProductSearchModel(new SupplierProductSearchModel(), null);

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult BulkEditSelect(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSuppliers))
                return AccessDeniedDataTablesJson();

            var products = _supplierModelFactory.PrepareProductBulkEditListModel(command);

            var gridModel = new
            {
                Data = products.Data,
                Total = products.RecordsTotal
            };

            return Json(gridModel);
        }

        [HttpPost]
        public virtual IActionResult BulkEditUpdate(IEnumerable<SupplierProductModel> products)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSuppliers))
                return AccessDeniedView();

            if (products != null)
            {
                foreach (var model in products)
                {
                    //update
                    var product = _supplierService.GetProductSupplierById(model.Id);

                    if (product != null)
                    {
                        //a vendor should have access only to his products
                        if (_workContext.CurrentVendor != null && product.SupplierId != _workContext.CurrentVendor.Id)
                            continue;
                        product.Price = model.Price;
                        _supplierService.UpdateProductSupplier(product);
                    }
                }
            }

            return new NullJsonResult();
        }

        public virtual IActionResult ProductAddPopup(int supplierId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSuppliers))
                return AccessDeniedView();

            //prepare model
            var model = _supplierModelFactory.PrepareAddProductToSupplierSearchModel(new AddProductToSupplierSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult ProductAddPopupList(AddProductToSupplierSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSuppliers))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _supplierModelFactory.PrepareAddProductToSupplierListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult ProductAddPopup(AddProductToSupplierModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSuppliers))
                return AccessDeniedView();

            //get selected products
            var selectedProducts = _productService.GetProductsByIds(model.SelectedProductIds.ToArray());
            if (selectedProducts.Any())
            {
                var existingProductCategories = _supplierService.GetProductSuppliersBySupplierId(model.SupplierId, showHidden: true);
                foreach (var product in selectedProducts)
                {
                    //whether product category with such parameters already exists
                    if (_supplierService.FindProductSupplier(existingProductCategories, product.Id, model.SupplierId) != null)
                        continue;

                    //insert the new product category mapping
                    _supplierService.InsertProductSupplier(new ProductSupplier
                    {
                        SupplierId = model.SupplierId,
                        ProductId = product.Id,
                        Price = product.Price,
                        IsFeaturedProduct = false,
                        DisplayOrder = 1
                    });
                }
            }

            ViewBag.RefreshPage = true;

            return View(new AddProductToSupplierSearchModel());
        }



        public virtual IActionResult GetAllSuppliersByStore(string supplierId, string storeId, bool? addAsterisk)
        {
            //permission validation is not required here

            // This action method gets called via an ajax request
            if (string.IsNullOrEmpty(storeId))
                throw new ArgumentNullException(nameof(storeId));

            var stores = _storeService.GetStoreById(Convert.ToInt32(storeId));
            var supplier = storeId != null ? _supplierService.GetSupplierByStoreId(stores.Id).ToList() : new List<Supplier>();
            var result = (from s in supplier
                          select new { id = s.Id, name = s.SupplierName }).ToList();
            if (addAsterisk.HasValue && addAsterisk.Value)
            {
                //asterisk
                result.Insert(0, new { id = 0, name = "*" });
            }
            else
            {
                if (storeId == null)
                {
                    result.Insert(0, new { id = 0, name = _localizationService.GetResource("Admin.Address.OtherNonUS") });
       
                }
                else
                {
                    //some country is selected
                    if (!result.Any())
                    {
                        //country does not have states
                        result.Insert(0, new { id = 0, name = _localizationService.GetResource("Admin.Address.OtherNonUS") });
                    }
                    else
                    {
                        //country has some states
                        result.Insert(0, new { id = 0, name = _localizationService.GetResource("Admin.Address.OtherNonUS") });

                    }
                }

                return Json(result);
            }
            return null;
        }

            #endregion

        }
    }

#endregion
#endregion