﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class AgentAvailibiltyForPushNotification
    {
        public int? AgentId { get; set; }
        public long? MobileNo { get; set; }
        public string DeviceToken { get; set; }
    }
}
