﻿using System;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Seo;

namespace Nop.Core.Domain.NB.Suppliers
{
    public partial class Supplier:BaseEntity, ILocalizedEntity, ISlugSupported
    {

        /// <summary>
        /// Gets or sets the SupplierName
        /// </summary>
        public string SupplierName { get; set; }

        /// <summary>
        /// Gets or sets the Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Address 
        /// </summary>
        public int AddressId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity is active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }


        /// <summary>
        /// Gets or sets the LocationMap
        /// </summary>
        public string LocationMap { get; set; }

        /// <summary>
        /// Gets or sets the Geofancing
        /// </summary>
        public string Geofancing { get; set; }

        /// <summary>
        /// Gets or sets the DeliveryFee
        /// </summary>
        public decimal DeliveryFee { get; set; }

        /// <summary>
        /// Gets or sets the date and time of entity creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the FixedOrDynamic
        /// </summary>
        public string FixedOrDynamic { get; set; }

        /// <summary>
        /// Gets or sets the KmOrMilesForDynamic
        /// </summary>
        public string KmOrMilesForDynamic { get; set; }
        /// <summary>
        /// Gets or sets the PriceForDynamic
        /// </summary>
        public decimal PriceForDynamic { get; set; }
        /// <summary>
        /// Gets or sets the DistanceForDynamic
        /// </summary>
        public decimal DistanceForDynamic { get; set; }
        /// <summary>
        /// Gets or sets the PricePerUnitDistanceForDynamic
        /// </summary>
        public decimal PricePerUnitDistanceForDynamic { get; set; }

        /// <summary>
        /// Gets or sets the DisplayOrder
        /// </summary>
        public int DisplayOrder { get; set; }
        /// <summary>
        /// Gets or sets the StoreId
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the SubjectToAcl
        /// </summary>
        public bool SubjectToAcl { get; set; }

        /// <summary>
        /// Gets or sets the Published
        /// </summary>
        public bool Published { get; set; }
        /// <summary>
        /// Gets or sets the AvailableStore
        /// </summary>
        public bool AvailableStore { get; set; }
        /// <summary>
        /// Gets or sets the ParentSupplierId
        /// </summary>
        public int ParentSupplierId { get; set; }
    }
}
