﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Media;
using static Nop.Web.Models.ShoppingCart.ShoppingCartModel;

namespace Nop.Web.Models.ShoppingCart
{
    public partial class MiniShoppingCartModel : BaseNopModel
    {
        public MiniShoppingCartModel()
        {
            Items = new List<ShoppingCartItemModel>();
            #region Custom code from v4.0
            DiscountBox = new DiscountBoxModel();
            GiftCardBox = new GiftCardBoxModel();
            #endregion
        }

        public IList<ShoppingCartItemModel> Items { get; set; }
        public int TotalProducts { get; set; }
        public string SubTotal { get; set; }
        public bool DisplayShoppingCartButton { get; set; }
        public bool DisplayCheckoutButton { get; set; }
        public bool CurrentCustomerIsGuest { get; set; }
        public bool AnonymousCheckoutAllowed { get; set; }
        public bool ShowProductImages { get; set; }

        #region custom code from v4.0
        public bool IsDisplayCheckoutButton { get; set; }
        public bool TermsOfServiceOnShoppingCartPage { get; set; }
        public bool TermsOfServiceOnOrderConfirmPage { get; set; }
        public bool TermsOfServicePopup { get; set; }
        public bool IsForCheckout { get; set; } //custom code by KP

        public DiscountBoxModel DiscountBox { get; set; }
        public GiftCardBoxModel GiftCardBox { get; set; }

        public string DeliveryAmountString { get; set; }
        public decimal DeliveryAmount { get; set; }

        public string ServiceChargeAmount { get; set; }
        public decimal ServiceChargeAmountValue { get; set; }

        public string TipAmount { get; set; }
        public decimal TipAmountValue { get; set; }

        public decimal ItemTotal { get; set; }
        public string ItemTotalAmount { get; set; }
        #endregion
        #region Nested Classes

        public partial class ShoppingCartItemModel : BaseNopEntityModel
        {
            public ShoppingCartItemModel()
            {
                Picture = new PictureModel();
            }

            public int ProductId { get; set; }

            public string ProductName { get; set; }

            public string ProductSeName { get; set; }

            public int Quantity { get; set; }

            public string UnitPrice { get; set; }
            public string AttributeInfo { get; set; }

            public PictureModel Picture { get; set; }

            //Custom code from v4.0
            public string UnitPriceWithQuantity { get; set; }
            public string BrandName { get; set; }
            public string Description { get; set; }
        }

        #endregion
    }
}