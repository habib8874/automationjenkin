﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NHKCustomerApplication.Models
{
    public partial class NB_Vendor_SliderPicture_Mapping
    {
        public int Id { get; set; }
        public int VendorId { get; set; }
        public int PictureId { get; set; }
        public int DisplayOrder { get; set; }

    }
}
