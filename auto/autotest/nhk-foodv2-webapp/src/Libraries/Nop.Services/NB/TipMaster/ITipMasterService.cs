﻿using Nop.Core;
using Nop.Core.Domain.NB.Tip;
using System.Collections.Generic;

namespace Nop.Services.NB.TipMaster
{
    /// <summary>
    /// TipMaster Property service interface
    /// </summary>
    public partial interface ITipMasterService
    {
        #region TipMaster

        /// <summary>
        /// Gets a Tip  
        /// </summary>
        /// <param name="tipId">Tip  identifier</param>
        /// <returns>Tip </returns>
        Tip GetTipById(int tipId);

        /// <summary>
        /// Gets a Tips 
        /// </summary>
        /// <returns>Tips </returns>
        IList<Tip> GetAllTips(int storeId = 0);

        /// <summary>
        /// Inserts a Tip
        /// </summary>
        /// <param name="Tip">Tip </param>
        void InsertTip(Tip tip);

        /// <summary>
        /// Updates the Tip 
        /// </summary>
        /// <param name="Tip">Tip </param>
        void UpdateTip(Tip tip);

        /// <summary>
        /// Deletes a Tip
        /// </summary>
        /// <param name="Tip">Tip</param>
        void DeleteTip(Tip tip);

        /// <summary>
        /// Gets a Tips  
        /// </summary>
        /// <param name="TipsIds">Tips  identifier</param>
        /// <returns>Tips list</returns>
        IList<Tip> GetTipsByIds(int[] tipsIds);
        #endregion
    }
}
