using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.VendorsGallery;

namespace Nop.Data.Mapping.VendorGalleryMap
{
    /// <summary>
    /// Vendor Gallery Mapping class
    /// </summary>
    public partial class VendorGalleryMap : NopEntityTypeConfiguration<VendorGallery>
    {
        /// <summary>
        /// Ctor
        /// </summary>
        public override void Configure(EntityTypeBuilder<VendorGallery> builder)
        {
            builder.ToTable("VendorGallery");
            builder.HasKey(v => v.Id);

            builder.Property(v => v.GalleryType).IsRequired();
            builder.Property(v => v.Active).IsRequired();
            builder.Property(v => v.Deleted).IsRequired();
            builder.Property(v => v.CreatedDt).IsRequired();
            builder.Property(s => s.StoreId).IsRequired();
            builder.Property(s => s.VendorId).IsRequired();
            base.Configure(builder);

        }
    }
}