﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Services;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModel;
using NHKCustomerApplication.ViewModels;
using NHKCustomerApplication.Caching;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;

namespace NHKCustomerApplication.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class CooknRestaurantController : ControllerBase
	{
		// private readonly string ChefBaseUrl = "http://nhkdev.nestorhawk.com/images/thumbs/";
		private string connectionString = string.Empty;
		private ecuadordevContext dbcontext;
		private string CurrencyCode = string.Empty;
		private readonly IConfiguration _configuration;
		private readonly ICacheManager _cacheManager;

		#region "fields"

		private readonly ICommonService _commonService;

		#endregion


		public CooknRestaurantController(ICommonService commonService, ICacheManager cacheManager, ecuadordevContext context, IConfiguration configuration)
		{
			dbcontext = context;
			_configuration = configuration;
			connectionString = _configuration.GetSection("ConnectionStrings").GetSection("ecuadordatabase").Value;
			this._commonService = commonService;
			this._cacheManager = cacheManager;

		}
		//KP Comments//Please add comment for each api
		[Route("~/api/v1/SearchCookndDish")]
		[HttpPost]
		public CustomerAPIResponses SearchCookndDish([FromBody] SearchCooknChefDishesModel searchCooknChefDishesModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(searchCooknChefDishesModel.StoreId, searchCooknChefDishesModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (searchCooknChefDishesModel == null)
			{
				//KP Comments//Please use a function for response

				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(searchCooknChefDishesModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (searchCooknChefDishesModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (searchCooknChefDishesModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(searchCooknChefDishesModel.SearchValue))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.SearchValueEmpty", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(searchCooknChefDishesModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == searchCooknChefDishesModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				List<int> CooknRestlist = new List<int>();
				var vendors = dbcontext.Vendor.AsEnumerable().Where(x => x.StoreId == searchCooknChefDishesModel.StoreId).ToList();
				foreach (var item in vendors)
				{
					CooknRestlist.Add(item.Id);
				}
				if (!CooknRestlist.Any())
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.SorryNoDataAvailable", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
				if (searchCooknChefDishesModel.StoreId != 0)
				{
					var CustomerExist = dbcontext.Store.AsEnumerable().Where(x => x.Id == searchCooknChefDishesModel.StoreId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (searchCooknChefDishesModel.CustomerId != 0)
				{
					var CustomerExist = dbcontext.Customer.AsEnumerable().Where(x => x.Id == searchCooknChefDishesModel.CustomerId && x.Active == true).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var GetRestaurantnCookById = (from b in dbcontext.Product.AsEnumerable().Where(x => x.Published == true && x.Deleted == false && x.Name.ToLower().ToString().Contains(searchCooknChefDishesModel.SearchValue.ToLower().ToString()))
											  join pc in dbcontext.ProductCategoryMapping.AsEnumerable()
											  on b.Id equals pc.ProductId
											  join cat in dbcontext.Category.AsEnumerable()
											  on pc.CategoryId equals cat.Id
											  join c in dbcontext.Vendor.AsEnumerable().Where(x => x.StoreId == searchCooknChefDishesModel.StoreId)
											  on b.VendorId equals c.Id
											  select new CooknChefProducts
											  {
												  RestnCookId = c.Id,

											  }).AsEnumerable().ToList();


				var isApproved = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("ratingreviewsettings.merchant.isapprovalrequired") && x.StoreId == searchCooknChefDishesModel.StoreId).Any() ? Convert.ToBoolean(dbcontext.Setting.Where(x => x.Name.Contains("ratingreviewsettings.merchant.isapprovalrequired") && x.StoreId == searchCooknChefDishesModel.StoreId).FirstOrDefault().Value) : false;


				var TopRatedChefnRestaurant = (from a in dbcontext.Vendor.AsEnumerable()
											   where a.StoreId == searchCooknChefDishesModel.StoreId
											   && a.Active == true && a.Deleted == false
											   select new CooknRestV2
											   {
												   RestnCookId = a.Id,
												   CooknRestName = a.Name,
												   PictureId = a.PictureId,
												   IsOpen = a.IsOpen == null ? false : a.IsOpen.Value,
												   RestnCookAddress = a.AddressId.ToString(),
												   Distance = "0 Km",
												   OpenTime = a.Opentime,
												   CloseTime = a.Closetime

											   }).ToList();

				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == searchCooknChefDishesModel.StoreId).FirstOrDefault().Value;

				if (TopRatedChefnRestaurant.Any())
				{
					List<CooknRestV2> TopRatedChefnRestaurantlist = new List<CooknRestV2>();
					if (CooknRestlist.Any())
					{
						foreach (var item in TopRatedChefnRestaurant)
						{
							item.CooknRestName = item.CooknRestName.ToUpper().ToString();
							if (!string.IsNullOrEmpty(item.RestnCookAddress))
							{
								var address = Convert.ToInt32(item.RestnCookAddress);
								var Addreess2 = dbcontext.Address.Where(x => x.Id == address).FirstOrDefault();
								if (Addreess2 != null)
								{
									item.RestnCookAddress = Addreess2.Address1 + " " + Addreess2.Address2;
								}

							}
							item.Rating = isApproved ? (!(from rating in dbcontext.RatingReviews.Where(x => x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == searchCooknChefDishesModel.StoreId
																	 && x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved && x.IsLiked)
														  where rating.EntityId == item.RestnCookId
														  select new
														  {
															  rating
														  }).AsEnumerable().Any() ? 0 :
									(from rating in dbcontext.RatingReviews.Where(x => x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == searchCooknChefDishesModel.StoreId
																   && x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved && x.IsLiked)
									 where rating.EntityId == item.RestnCookId
									 select new
									 {
										 rating
									 }).AsEnumerable().ToList().Count(x => x.rating.IsLiked)) : (!(from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == searchCooknChefDishesModel.StoreId
																   && x.IsNotInterested == false && x.IsSkipped == false && x.IsLiked)
																								   where rating.EntityId == item.RestnCookId
																								   select new
																								   {
																									   rating
																								   }).AsEnumerable().Any() ? 0 :
									(from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == searchCooknChefDishesModel.StoreId
																   && x.IsNotInterested == false && x.IsSkipped == false && x.IsLiked)
									 where rating.EntityId == item.RestnCookId
									 select new
									 {
										 rating
									 }).AsEnumerable().ToList().Count(x => x.rating.IsLiked));
							item.RatingCount = isApproved ? (from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == searchCooknChefDishesModel.StoreId
										 && x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved)
															 where rating.EntityId == item.RestnCookId
															 select new
															 {
																 rating.Rating
															 }).AsEnumerable().Count() : (from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == searchCooknChefDishesModel.StoreId
															&& x.IsNotInterested == false && x.IsSkipped == false)
																						  where rating.EntityId == item.RestnCookId
																						  select new
																						  {
																							  rating.Rating
																						  }).AsEnumerable().Count();

							if (CooknRestlist.Contains(item.RestnCookId) && item.CooknRestName.ToLower().ToString().Contains(searchCooknChefDishesModel.SearchValue.ToLower().ToString()))
							{
								var restDetail = dbcontext.Vendor.Where(x => x.Id == item.RestnCookId).FirstOrDefault();
								string[] venLtLngStrArr = restDetail.Geolocation.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');

								//item.Cuisine = Regex.Replace(item.Cuisine, "<.*?>", String.Empty);
								double latRest = Convert.ToDouble(venLtLngStrArr[0]);
								double longRest = Convert.ToDouble(venLtLngStrArr[1]);
								item.Distance = String.Format("{0:0.00}", Helper.distance(Convert.ToDouble(searchCooknChefDishesModel.LatPos), Convert.ToDouble(searchCooknChefDishesModel.LongPos), latRest, longRest, "K"));
								int pictureSize = 100;
								var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.vendorthumbpicturesize")).FirstOrDefault();
								if (setting != null)
								{
									pictureSize = Convert.ToInt32(setting.Value);
								}
								if (item.PictureId != 0 && item.PictureId != null)
								{

									var picture = dbcontext.Picture.Where(x => x.Id == item.PictureId).FirstOrDefault();
									string lastPart = Helper.GetFileExtensionFromMimeType(picture.MimeType);
									string thumbFileName = !string.IsNullOrEmpty(picture.SeoFilename)
									? $"{picture.Id:0000000}_{picture.SeoFilename}_{pictureSize}.{lastPart}" + "? " + DateTime.Now
									: $"{picture.Id:0000000}_{pictureSize}.{lastPart}" + "?" + DateTime.Now;


									item.CooknRestImageURL = ChefBaseUrl + thumbFileName;
								}
								else
								{
									item.CooknRestImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
								}
								item.IsRestAvailable = false;
								if (item.CooknRestName.ToLower().Contains("live"))
								{
									var isVendorSchedule = dbcontext.Vendor.Where(x => x.Id == item.RestnCookId).FirstOrDefault().AvailableType;
									if (isVendorSchedule != null && isVendorSchedule.Value != 0)
									{
										if (isVendorSchedule == (int)AvailableTypeEnum.AllDay)
										{
											var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.AllDay).ToList();
											if (AllDaySchedulesTimimgs.Any())
											{
												//var CurrentTime = DateTime.Now.TOS;
												var CurrentTime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).TimeOfDay;
												foreach (var __item in AllDaySchedulesTimimgs)
												{
													if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
													{
														item.IsRestAvailable = true;
													}
													DateTime fromTime = DateTime.Today.Add(__item.ScheduleFromTime);
													string displayFromTime = fromTime.ToString("hh:mm tt");
													DateTime toTime = DateTime.Today.Add(__item.ScheduleToTime);
													string displayToTime = toTime.ToString("hh:mm tt");
													string fromToTime = displayFromTime + " - " + displayToTime;
													item.OpenCloseTime.Add(fromToTime);

												}
											}
											else
											{
												item.IsRestAvailable = true;
											}

										}
										else if (isVendorSchedule == (int)AvailableTypeEnum.Custom)
										{
											var dayofWeek = (int)Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).DayOfWeek;
											var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.Custom && x.AvailableOn == dayofWeek).ToList();
											if (AllDaySchedulesTimimgs.Any())
											{
												//var CurrentTime = DateTime.Now.TOS;
												var CurrentTime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).TimeOfDay;
												foreach (var __item in AllDaySchedulesTimimgs)
												{
													if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
													{
														item.IsRestAvailable = true;
													}
													DateTime fromTime = DateTime.Today.Add(__item.ScheduleFromTime);
													string displayFromTime = fromTime.ToString("hh:mm tt");
													DateTime toTime = DateTime.Today.Add(__item.ScheduleToTime);
													string displayToTime = toTime.ToString("hh:mm tt");
													string fromToTime = displayFromTime + " - " + displayToTime;
													item.OpenCloseTime.Add(fromToTime);

												}
											}
											else
											{
												item.IsRestAvailable = true;
											}
										}
										else
										{
											item.IsRestAvailable = true;
										}
									}
									else
									{
										item.IsRestAvailable = true;
									}
								}
								else
								{
									item.IsRestAvailable = true;
								}
								if (item.Rating != 0)
								{
									item.Rating = Convert.ToDecimal((item.Rating / item.RatingCount));
									item.Rating = Math.Round(item.Rating * 5, 2);
								}
								TopRatedChefnRestaurantlist.Add(item);
							}
						}

					}
					if (GetRestaurantnCookById.Any() && CooknRestlist.Any())
					{
						foreach (var item in GetRestaurantnCookById)
						{
							if (item.Rating != 0)
							{
								item.Rating = Convert.ToDecimal((item.Rating / item.RatingCount));
								item.Rating = Math.Round(item.Rating * 5, 2);
							}
							if (CooknRestlist.Contains(item.RestnCookId))
							{
								var chef = TopRatedChefnRestaurant.Where(x => x.RestnCookId == item.RestnCookId).FirstOrDefault();
								var restDetail = dbcontext.Vendor.Where(x => x.Id == item.RestnCookId).FirstOrDefault();
								string[] venLtLngStrArr = restDetail.Geolocation.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
								//chef.Cuisine = Regex.Replace(chef.Cuisine, "<.*?>", String.Empty);
								double latRest = Convert.ToDouble(venLtLngStrArr[0]);
								double longRest = Convert.ToDouble(venLtLngStrArr[1]);
								chef.Distance = String.Format("{0:0.00}", Helper.distance(Convert.ToDouble(searchCooknChefDishesModel.LatPos), Convert.ToDouble(searchCooknChefDishesModel.LongPos), latRest, longRest, "K"));
								int pictureSize = 100;
								var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.vendorthumbpicturesize")).FirstOrDefault();
								if (setting != null)
								{
									pictureSize = Convert.ToInt32(setting.Value);
								}
								if (chef.PictureId != 0 && chef.PictureId != null)
								{

									var picture = dbcontext.Picture.Where(x => x.Id == chef.PictureId).FirstOrDefault();
									string lastPart = Helper.GetFileExtensionFromMimeType(picture.MimeType);
									string thumbFileName = !string.IsNullOrEmpty(picture.SeoFilename)
									? $"{picture.Id:0000000}_{picture.SeoFilename}_{pictureSize}.{lastPart}" + "? " + DateTime.Now
									: $"{picture.Id:0000000}_{pictureSize}.{lastPart}" + "? " + DateTime.Now;

									chef.CooknRestImageURL = ChefBaseUrl + thumbFileName + "?" + DateTime.Now;
								}
								else
								{
									chef.CooknRestImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
								}
								chef.IsRestAvailable = false;
								if (chef.CooknRestName.ToLower().Contains("live"))
								{
									var isVendorSchedule = dbcontext.Vendor.Where(x => x.Id == item.RestnCookId).FirstOrDefault().AvailableType;
									if (isVendorSchedule != null && isVendorSchedule.Value != 0)
									{
										if (isVendorSchedule == (int)AvailableTypeEnum.AllDay)
										{
											var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.AllDay).ToList();
											if (AllDaySchedulesTimimgs.Any())
											{
												//var CurrentTime = DateTime.Now.TOS;
												var CurrentTime = Helper.ConvertToUserTime(DateTime.Now, DateTimeKind.Utc, dbcontext).TimeOfDay;
												foreach (var __item in AllDaySchedulesTimimgs)
												{
													if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
													{
														chef.IsRestAvailable = true;
													}


												}
											}
											else
											{
												chef.IsRestAvailable = true;
											}

										}
										else if (isVendorSchedule == (int)AvailableTypeEnum.Custom)
										{
											var dayofWeek = (int)Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).DayOfWeek;
											var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.Custom && x.AvailableOn == dayofWeek).ToList();
											if (AllDaySchedulesTimimgs.Any())
											{
												//var CurrentTime = DateTime.Now.TOS;
												var CurrentTime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).TimeOfDay;
												foreach (var __item in AllDaySchedulesTimimgs)
												{
													if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
													{
														chef.IsRestAvailable = true;
													}


												}
											}
											else
											{
												chef.IsRestAvailable = true;
											}
										}
										else
										{
											chef.IsRestAvailable = true;
										}
									}
									else
									{
										chef.IsRestAvailable = true;
									}
								}
								else
								{
									chef.IsRestAvailable = true;
								}

								TopRatedChefnRestaurantlist.Add(chef);
							}

						}
					}
					if (TopRatedChefnRestaurantlist.Any())
					{
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = TopRatedChefnRestaurantlist.Distinct().ToList();
					}
					else
					{
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.SorryNoDataAvailable", languageId, dbcontext);
						customerAPIResponses.ErrorMessageTitle = "Error!!";
					}

				}
				else
				{
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.SorryNoDataAvailable", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
				}

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v2.1/SearchCookndDish")]
		[HttpPost]
		public CustomerAPIResponses SearchCookndDishV2_1([FromBody] SearchCooknChefDishesModel searchCooknChefDishesModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(searchCooknChefDishesModel.StoreId, searchCooknChefDishesModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (searchCooknChefDishesModel == null)
			{
				//KP Comments//Please use a function for response
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(searchCooknChefDishesModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (searchCooknChefDishesModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (searchCooknChefDishesModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(searchCooknChefDishesModel.SearchValue))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.SearchValueEmpty", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(searchCooknChefDishesModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == searchCooknChefDishesModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				List<int> CooknRestlist = new List<int>();
				var vendors = dbcontext.Vendor.AsEnumerable().Where(x => x.StoreId == searchCooknChefDishesModel.StoreId).ToList();
				foreach (var item in vendors)
				{
					CooknRestlist.Add(item.Id);
				}
				if (!CooknRestlist.Any())
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.SorryNoDataAvailable", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
				if (searchCooknChefDishesModel.StoreId != 0)
				{
					var CustomerExist = dbcontext.Store.AsEnumerable().Where(x => x.Id == searchCooknChefDishesModel.StoreId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (searchCooknChefDishesModel.CustomerId != 0)
				{
					var CustomerExist = dbcontext.Customer.AsEnumerable().Where(x => x.Id == searchCooknChefDishesModel.CustomerId && x.Active == true).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var GetRestaurantnCookById = (from b in dbcontext.Product.AsEnumerable().Where(x => x.Published == true && x.Deleted == false && x.Name.ToLower().ToString().Contains(searchCooknChefDishesModel.SearchValue.ToLower().ToString()))
											  join pc in dbcontext.ProductCategoryMapping.AsEnumerable()
											  on b.Id equals pc.ProductId
											  join cat in dbcontext.Category.AsEnumerable()
											  on pc.CategoryId equals cat.Id
											  join c in dbcontext.Vendor.AsEnumerable().Where(x => x.StoreId == searchCooknChefDishesModel.StoreId)
											  on b.VendorId equals c.Id
											  select new CooknChefProducts
											  {
												  RestnCookId = c.Id,

											  }).AsEnumerable().ToList();


				var isApproved = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("ratingreviewsettings.merchant.isapprovalrequired") && x.StoreId == searchCooknChefDishesModel.StoreId).Any() ? Convert.ToBoolean(dbcontext.Setting.Where(x => x.Name.Contains("ratingreviewsettings.merchant.isapprovalrequired") && x.StoreId == searchCooknChefDishesModel.StoreId).FirstOrDefault().Value) : false;


				var TopRatedChefnRestaurant = (from a in dbcontext.Vendor.AsEnumerable()
											   where a.StoreId == searchCooknChefDishesModel.StoreId
											   && a.Active == true && a.Deleted == false
											   select new CooknRestV2
											   {
												   RestnCookId = a.Id,
												   CooknRestName = a.Name,
												   PictureId = a.PictureId,
												   IsOpen = a.IsOpen == null ? false : a.IsOpen.Value,
												   RestnCookAddress = a.AddressId.ToString(),
												   Distance = "0 Km",
												   OpenTime = a.Opentime,
												   CloseTime = a.Closetime

											   }).ToList();

				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == searchCooknChefDishesModel.StoreId).FirstOrDefault().Value;

				if (TopRatedChefnRestaurant.Any())
				{
					List<CooknRestV2> TopRatedChefnRestaurantlist = new List<CooknRestV2>();
					if (CooknRestlist.Any())
					{
						foreach (var item in TopRatedChefnRestaurant)
						{
							item.CooknRestName = item.CooknRestName.ToUpper().ToString();
							if (!string.IsNullOrEmpty(item.RestnCookAddress))
							{
								var address = Convert.ToInt32(item.RestnCookAddress);
								var Addreess2 = dbcontext.Address.Where(x => x.Id == address).FirstOrDefault();
								if (Addreess2 != null)
								{
									item.RestnCookAddress = Addreess2.Address1 + " " + Addreess2.Address2;
								}

							}
							item.Rating = isApproved ? (!(from rating in dbcontext.RatingReviews.Where(x => x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == searchCooknChefDishesModel.StoreId
																	 && x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved && x.IsLiked)
														  where rating.EntityId == item.RestnCookId
														  select new
														  {
															  rating
														  }).AsEnumerable().Any() ? 0 :
									(from rating in dbcontext.RatingReviews.Where(x => x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == searchCooknChefDishesModel.StoreId
																   && x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved && x.IsLiked)
									 where rating.EntityId == item.RestnCookId
									 select new
									 {
										 rating
									 }).AsEnumerable().ToList().Count(x => x.rating.IsLiked)) : (!(from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == searchCooknChefDishesModel.StoreId
																   && x.IsNotInterested == false && x.IsSkipped == false && x.IsLiked)
																								   where rating.EntityId == item.RestnCookId
																								   select new
																								   {
																									   rating
																								   }).AsEnumerable().Any() ? 0 :
									(from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == searchCooknChefDishesModel.StoreId
																   && x.IsNotInterested == false && x.IsSkipped == false && x.IsLiked)
									 where rating.EntityId == item.RestnCookId
									 select new
									 {
										 rating
									 }).AsEnumerable().ToList().Count(x => x.rating.IsLiked));
							item.RatingCount = isApproved ? (from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == searchCooknChefDishesModel.StoreId
										 && x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved)
															 where rating.EntityId == item.RestnCookId
															 select new
															 {
																 rating.Rating
															 }).AsEnumerable().Count() : (from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == searchCooknChefDishesModel.StoreId
															&& x.IsNotInterested == false && x.IsSkipped == false)
																						  where rating.EntityId == item.RestnCookId
																						  select new
																						  {
																							  rating.Rating
																						  }).AsEnumerable().Count();

							if (CooknRestlist.Contains(item.RestnCookId) && item.CooknRestName.ToLower().ToString().Contains(searchCooknChefDishesModel.SearchValue.ToLower().ToString()))
							{
								var restDetail = dbcontext.Vendor.Where(x => x.Id == item.RestnCookId).FirstOrDefault();
								string[] venLtLngStrArr = restDetail.Geolocation.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');

								//item.Cuisine = Regex.Replace(item.Cuisine, "<.*?>", String.Empty);
								double latRest = Convert.ToDouble(venLtLngStrArr[0]);
								double longRest = Convert.ToDouble(venLtLngStrArr[1]);
								item.Distance = String.Format("{0:0.00}", Helper.distance(Convert.ToDouble(searchCooknChefDishesModel.LatPos), Convert.ToDouble(searchCooknChefDishesModel.LongPos), latRest, longRest, "K"));
								int pictureSize = 100;
								var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.vendorthumbpicturesize")).FirstOrDefault();
								if (setting != null)
								{
									pictureSize = Convert.ToInt32(setting.Value);
								}
								if (item.PictureId != 0 && item.PictureId != null)
								{

									var picture = dbcontext.Picture.Where(x => x.Id == item.PictureId).FirstOrDefault();
									string lastPart = Helper.GetFileExtensionFromMimeType(picture.MimeType);
									string thumbFileName = !string.IsNullOrEmpty(picture.SeoFilename)
									? $"{picture.Id:0000000}_{picture.SeoFilename}_{pictureSize}.{lastPart}" + "? " + DateTime.Now
									: $"{picture.Id:0000000}_{pictureSize}.{lastPart}" + "?" + DateTime.Now;


									item.CooknRestImageURL = ChefBaseUrl + thumbFileName;
								}
								else
								{
									item.CooknRestImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
								}
								item.IsRestAvailable = false;
								if (item.CooknRestName.ToLower().Contains("live"))
								{
									var isVendorSchedule = dbcontext.Vendor.Where(x => x.Id == item.RestnCookId).FirstOrDefault().AvailableType;
									if (isVendorSchedule != null && isVendorSchedule.Value != 0)
									{
										if (isVendorSchedule == (int)AvailableTypeEnum.AllDay)
										{
											var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.AllDay).ToList();
											if (AllDaySchedulesTimimgs.Any())
											{
												//var CurrentTime = DateTime.Now.TOS;
												var CurrentTime = Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, searchCooknChefDishesModel.CustomerId).TimeOfDay;
												foreach (var __item in AllDaySchedulesTimimgs)
												{
													if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
													{
														item.IsRestAvailable = true;
													}
													DateTime fromTime = DateTime.Today.Add(__item.ScheduleFromTime);
													string displayFromTime = fromTime.ToString("hh:mm tt");
													DateTime toTime = DateTime.Today.Add(__item.ScheduleToTime);
													string displayToTime = toTime.ToString("hh:mm tt");
													string fromToTime = displayFromTime + " - " + displayToTime;
													item.OpenCloseTime.Add(fromToTime);

												}
											}
											else
											{
												item.IsRestAvailable = true;
											}

										}
										else if (isVendorSchedule == (int)AvailableTypeEnum.Custom)
										{
											var dayofWeek = (int)Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, searchCooknChefDishesModel.CustomerId).DayOfWeek;
											var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.Custom && x.AvailableOn == dayofWeek).ToList();
											if (AllDaySchedulesTimimgs.Any())
											{
												//var CurrentTime = DateTime.Now.TOS;
												var CurrentTime = Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, searchCooknChefDishesModel.CustomerId).TimeOfDay;
												foreach (var __item in AllDaySchedulesTimimgs)
												{
													if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
													{
														item.IsRestAvailable = true;
													}
													DateTime fromTime = DateTime.Today.Add(__item.ScheduleFromTime);
													string displayFromTime = fromTime.ToString("hh:mm tt");
													DateTime toTime = DateTime.Today.Add(__item.ScheduleToTime);
													string displayToTime = toTime.ToString("hh:mm tt");
													string fromToTime = displayFromTime + " - " + displayToTime;
													item.OpenCloseTime.Add(fromToTime);

												}
											}
											else
											{
												item.IsRestAvailable = true;
											}
										}
										else
										{
											item.IsRestAvailable = true;
										}
									}
									else
									{
										item.IsRestAvailable = true;
									}
								}
								else
								{
									item.IsRestAvailable = true;
								}
								if (item.Rating != 0)
								{
									item.Rating = Convert.ToDecimal((item.Rating / item.RatingCount));
									item.Rating = Math.Round(item.Rating * 5, 2);
								}
								TopRatedChefnRestaurantlist.Add(item);
							}
						}

					}
					if (GetRestaurantnCookById.Any() && CooknRestlist.Any())
					{
						foreach (var item in GetRestaurantnCookById)
						{
							if (item.Rating != 0)
							{
								item.Rating = Convert.ToDecimal((item.Rating / item.RatingCount));
								item.Rating = Math.Round(item.Rating * 5, 2);
							}
							if (CooknRestlist.Contains(item.RestnCookId))
							{
								var chef = TopRatedChefnRestaurant.Where(x => x.RestnCookId == item.RestnCookId).FirstOrDefault();
								var restDetail = dbcontext.Vendor.Where(x => x.Id == item.RestnCookId).FirstOrDefault();
								string[] venLtLngStrArr = restDetail.Geolocation.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
								//chef.Cuisine = Regex.Replace(chef.Cuisine, "<.*?>", String.Empty);
								double latRest = Convert.ToDouble(venLtLngStrArr[0]);
								double longRest = Convert.ToDouble(venLtLngStrArr[1]);
								chef.Distance = String.Format("{0:0.00}", Helper.distance(Convert.ToDouble(searchCooknChefDishesModel.LatPos), Convert.ToDouble(searchCooknChefDishesModel.LongPos), latRest, longRest, "K"));
								int pictureSize = 100;
								var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.vendorthumbpicturesize")).FirstOrDefault();
								if (setting != null)
								{
									pictureSize = Convert.ToInt32(setting.Value);
								}
								if (chef.PictureId != 0 && chef.PictureId != null)
								{

									var picture = dbcontext.Picture.Where(x => x.Id == chef.PictureId).FirstOrDefault();
									string lastPart = Helper.GetFileExtensionFromMimeType(picture.MimeType);
									string thumbFileName = !string.IsNullOrEmpty(picture.SeoFilename)
									? $"{picture.Id:0000000}_{picture.SeoFilename}_{pictureSize}.{lastPart}" + "? " + DateTime.Now
									: $"{picture.Id:0000000}_{pictureSize}.{lastPart}" + "? " + DateTime.Now;

									chef.CooknRestImageURL = ChefBaseUrl + thumbFileName + "?" + DateTime.Now;
								}
								else
								{
									chef.CooknRestImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
								}
								chef.IsRestAvailable = false;
								if (chef.CooknRestName.ToLower().Contains("live"))
								{
									var isVendorSchedule = dbcontext.Vendor.Where(x => x.Id == item.RestnCookId).FirstOrDefault().AvailableType;
									if (isVendorSchedule != null && isVendorSchedule.Value != 0)
									{
										if (isVendorSchedule == (int)AvailableTypeEnum.AllDay)
										{
											var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.AllDay).ToList();
											if (AllDaySchedulesTimimgs.Any())
											{
												//var CurrentTime = DateTime.Now.TOS;
												var CurrentTime = Helper.ConvertToUserTimeV2_1(DateTime.Now, DateTimeKind.Utc, dbcontext, searchCooknChefDishesModel.CustomerId).TimeOfDay;
												foreach (var __item in AllDaySchedulesTimimgs)
												{
													if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
													{
														chef.IsRestAvailable = true;
													}


												}
											}
											else
											{
												chef.IsRestAvailable = true;
											}

										}
										else if (isVendorSchedule == (int)AvailableTypeEnum.Custom)
										{
											var dayofWeek = (int)Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, searchCooknChefDishesModel.CustomerId).DayOfWeek;
											var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.Custom && x.AvailableOn == dayofWeek).ToList();
											if (AllDaySchedulesTimimgs.Any())
											{
												//var CurrentTime = DateTime.Now.TOS;
												var CurrentTime = Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, searchCooknChefDishesModel.CustomerId).TimeOfDay;
												foreach (var __item in AllDaySchedulesTimimgs)
												{
													if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
													{
														chef.IsRestAvailable = true;
													}


												}
											}
											else
											{
												chef.IsRestAvailable = true;
											}
										}
										else
										{
											chef.IsRestAvailable = true;
										}
									}
									else
									{
										chef.IsRestAvailable = true;
									}
								}
								else
								{
									chef.IsRestAvailable = true;
								}

								TopRatedChefnRestaurantlist.Add(chef);
							}

						}
					}
					if (TopRatedChefnRestaurantlist.Any())
					{
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = TopRatedChefnRestaurantlist.Distinct().ToList();
					}
					else
					{
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.SorryNoDataAvailable", languageId, dbcontext);
						customerAPIResponses.ErrorMessageTitle = "Error!!";
					}

				}
				else
				{
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.SorryNoDataAvailable", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
				}

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}


		/// <summary>
		/// Get store slider pictures
		/// </summary>
		/// <param name="requestModel"></param>
		/// <returns></returns>
		[Route("~/api/v1/ProductSlider")]
		[HttpPost]
		public BaseAPIResponseModel GetStoreSliderPictures([FromBody] StoreSliderPicturesReqModel requestModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(requestModel.StoreId, requestModel.UniqueSeoCode, dbcontext);
			if (requestModel == null)
				return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext));
			if (string.IsNullOrEmpty(requestModel.ApiKey))
				return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext));
			if (requestModel.StoreId == 0)
				return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext));
			try
			{
				if (!string.IsNullOrEmpty(requestModel.ApiKey) || !string.IsNullOrWhiteSpace(requestModel.ApiKey))
				{
					var keyExist = Helper.AuthenticateKey(requestModel.ApiKey, requestModel.StoreId, dbcontext, _cacheManager);
					if (keyExist == null)
						return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext));
				}
				if (requestModel.StoreId != 0)
				{
					var CustomerExist = dbcontext.Store.Where(x => x.Id == requestModel.StoreId).Any();
					if (!CustomerExist)
						return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext));
				}
				var key = string.Format(CacheKeys.StoreBannerListKey, requestModel.StoreId, requestModel.Latitude, requestModel.Longitude,  languageId);
				var responceData = _cacheManager.Get(key, () =>
				{
					return _commonService.GetStoreSliderPictures(requestModel.StoreId);
				});
				if (responceData.Count <= 0)
					return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMessage.NoBannerFound", languageId, dbcontext));
				return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMessage.BannerFound", languageId, dbcontext), responceData);
			}

			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				return BaseMethodsHelper.ErrorResponse(ex.Message, HttpStatusCode.BadRequest);
			}
		}

		/// <summary>
		/// Get slider pictures by geo location
		/// </summary>
		/// <param name="requestModel"></param>
		/// <returns></returns>
		[Route("~/api/v2/ProductSlider")]
		[HttpPost]
		public BaseAPIResponseModel GetStoreSliderPicturesV2([FromBody] StoreSliderPicturesReqModel requestModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(requestModel.StoreId, requestModel.UniqueSeoCode, dbcontext);
			try
			{
				if (requestModel == null)
					return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext));
				if (string.IsNullOrEmpty(requestModel.ApiKey))
					return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext));
				if (requestModel.StoreId == 0)
					return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext));
				if (string.IsNullOrEmpty(requestModel.Latitude))
					return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.BannerSlider.ErrorMesaage.Lat.Missing", languageId, dbcontext));
				if (string.IsNullOrEmpty(requestModel.Longitude))
					return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.BannerSlider.ErrorMesaage.Long.Missing", languageId, dbcontext));
				if (!string.IsNullOrEmpty(requestModel.ApiKey) || !string.IsNullOrWhiteSpace(requestModel.ApiKey))
				{
					var keyExist = Helper.AuthenticateKey(requestModel.ApiKey, requestModel.StoreId, dbcontext, _cacheManager);
					if (keyExist == null)
						return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "NB.API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext));
				}
				//check store exist
				if (requestModel.StoreId != 0)
				{
					var storeExist = dbcontext.Store.Where(x => x.Id == requestModel.StoreId).Any();
					if (!storeExist)
						return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext));
				}
				var key = string.Format(CacheKeys.StoreBannerListKey, requestModel.StoreId, requestModel.Latitude,requestModel.Longitude, languageId);
				var responceData = _cacheManager.Get(key, () =>
				{
					return _commonService.GetSliderPicturesByGeoLocation(requestModel.Latitude, requestModel.Longitude, requestModel.StoreId);
				});
				if(responceData.Count<=0)
					return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMessage.NoBannerFound", languageId, dbcontext));
				return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMessage.BannerFound", languageId, dbcontext), responceData);


			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				return BaseMethodsHelper.ErrorResponse(ex.Message, HttpStatusCode.BadRequest);
			}
		}

		[Route("~/api/v1/GetCooknRestFiltersTitlenValues")]
		[HttpPost]
		public CustomerAPIResponses GetCooknRestFiltersTitlenValues([FromBody] CountryModel customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(customer.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == customer.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (customer.StoreId != 0)
				{
					var CustomerExist = dbcontext.Store.Where(x => x.Id == customer.StoreId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var Filter = Enum.GetValues(typeof(FiltersEnum)).Cast<FiltersEnum>().Where(x => (int)x == (int)FiltersEnum.RestnCook).Select(x => x.ToString()).FirstOrDefault();
				var CategoryTitleFilters = (from a in dbcontext.FilterCategory
											join b in dbcontext.Type
											on a.Type equals b.Id
											where a.FilterFor == Filter
					  && a.StoreId == customer.StoreId
											select new
											{
												TitleId = a.Id,
												TitleName = a.Name,
												TitleType = b.Name,
												CategoryValueFilter = dbcontext.FilterCategoryValues.Where(x => x.FilterCategoryId == a.Id && x.StoreId == customer.StoreId).Select(x => new
												{
													TitleValueId = x.Id,
													TitleValueName = x.Name,
												}).ToList()

											}).OrderBy(x => x.TitleId).ToList();
				if (CategoryTitleFilters.Any())
				{

					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = CategoryTitleFilters;
				}
				else
				{
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.Datanotavailable", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
				}

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v1/GetTopRatedCooknRest")]
		[HttpPost]
		public CustomerAPIResponses GetTopRatedCooknRestV3([FromBody] TopRatedRestnChefModel topRatedRestnChefModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(topRatedRestnChefModel.StoreId, topRatedRestnChefModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (topRatedRestnChefModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(topRatedRestnChefModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(topRatedRestnChefModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(topRatedRestnChefModel.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (topRatedRestnChefModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(topRatedRestnChefModel.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (topRatedRestnChefModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(topRatedRestnChefModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			try
			{
				List<int> CooknRestlist = new List<int>();
				using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
				{
					string result = db.Query<string>("GetTopRatedCooknRest_Validation", new
					{
						topRatedRestnChefModel.ApiKey,
						topRatedRestnChefModel.CustomerId,
						topRatedRestnChefModel.StoreId,
						topRatedRestnChefModel.RestType,
						latLongPos = topRatedRestnChefModel.LatPos + " " + topRatedRestnChefModel.LongPos,
						topRatedRestnChefModel.SkipLocationSearch,
						merchantCategoryIds = string.IsNullOrWhiteSpace(topRatedRestnChefModel.MerchantCategoryIds) ? "" : topRatedRestnChefModel.MerchantCategoryIds
					}, commandType: CommandType.StoredProcedure).FirstOrDefault();
					try
					{
						if (result != null && result.Length > 0)
						{
							if (result == "Invalid Authentication key")
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(topRatedRestnChefModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
							else if (result == "Customer Not Found")
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(topRatedRestnChefModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
							else if (result == "Store Not Found")
							{
								customerAPIResponses.ErrorMessageTitle = "Error!!";
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(topRatedRestnChefModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
								customerAPIResponses.ResponseObj = null;
								return customerAPIResponses;
							}
							else
							{
								foreach (string item in result.Split(','))
								{
									int addressid = Convert.ToInt32(item.Trim());
									CooknRestlist.Add(addressid);
								}
							}
						}
					}
					catch (Exception ex)
					{

					}
				}

				if (!CooknRestlist.Any())
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(topRatedRestnChefModel.StoreId, "API.ErrorMesaage.RestaurentNotAvailableAtYourLocation", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}

				List<CooknRestV2> TopRatedChefnRestaurant = new List<CooknRestV2>();
				using (SqlConnection connection = new SqlConnection(connectionString))
				{
					SqlCommand command = new SqlCommand("getvendorList", connection);
					command.Parameters.Add(new SqlParameter("@StoreId", topRatedRestnChefModel.StoreId));
					command.CommandType = CommandType.StoredProcedure;
					connection.Open();
					SqlDataReader reader = command.ExecuteReader();
					try
					{
						while (reader.Read())
						{
							CooknRestV2 cooknRestV2 = new CooknRestV2()
							{
								RestnCookId = Convert.ToInt32(reader["RestnCookId"]),
								CooknRestName = Convert.ToString(reader["CooknRestName"]),
								PictureId = Convert.ToInt32(reader["PictureId"]),
								IsOpen = Convert.ToBoolean(reader["IsOpen"]),
								Geolocation = Convert.ToString(reader["Geolocation"]),
								RestnCookAddress = Convert.ToString(reader["RestnCookAddress"]),
								Distance = Convert.ToString(reader["Distance"]),
								OpenTime = Convert.ToString(reader["Opentime"]),
								CloseTime = Convert.ToString(reader["Closetime"]),
								CooknRestImageURL = Convert.ToString(reader["CooknRestImageURL"]),
								AvailableType = Convert.ToInt32(reader["AvailableType"])
							};
							TopRatedChefnRestaurant.Add(cooknRestV2);
						}
					}
					finally
					{
						// Always call Close when done reading.
						connection.Close();
					}
				}
				if (TopRatedChefnRestaurant.Any())
				{
					List<CooknRestV2> TopRatedChefnRestaurantlist = new List<CooknRestV2>();
					if (CooknRestlist.Any())
					{
						foreach (var item in TopRatedChefnRestaurant)
						{
							if (CooknRestlist.Contains(item.RestnCookId))
							{
								string[] venLtLngStrArr = item.Geolocation.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
								double latRest = Convert.ToDouble(venLtLngStrArr[0]);
								double longRest = Convert.ToDouble(venLtLngStrArr[1]);
								var distancetype = dbcontext.Setting.Where(x => x.Name.Contains("setting.customer.distancetype") && x.StoreId == topRatedRestnChefModel.StoreId).FirstOrDefault()?.Value ?? "";
								item.Distance = String.Format("{0:0.00}" + " " + distancetype, Helper.distance(topRatedRestnChefModel.LatPos, topRatedRestnChefModel.LongPos, latRest, longRest, "K"));

								item.IsRestAvailable = false;

								var isVendorSchedule = item.AvailableType;
								if (isVendorSchedule != null && isVendorSchedule != 0)
								{
									if (isVendorSchedule == (int)AvailableTypeEnum.AllDay)
									{
										var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.AllDay).ToList();
										if (AllDaySchedulesTimimgs.Any())
										{
											//var CurrentTime = DateTime.Now.TOS;
											var CurrentTime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).TimeOfDay;
											foreach (var __item in AllDaySchedulesTimimgs)
											{
												if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
												{
													item.IsRestAvailable = true;
												}
												DateTime fromTime = DateTime.Today.Add(__item.ScheduleFromTime);
												string displayFromTime = fromTime.ToString("hh:mm tt");
												DateTime toTime = DateTime.Today.Add(__item.ScheduleToTime);
												string displayToTime = toTime.ToString("hh:mm tt");
												string fromToTime = displayFromTime + " - " + displayToTime;

												item.OpenCloseTime.Add(fromToTime);
											}
										}
										else
										{
											item.IsRestAvailable = true;
										}
									}
									else if (isVendorSchedule == (int)AvailableTypeEnum.Custom)
									{
										var dayofWeek = (int)Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).DayOfWeek;
										var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.Custom && x.AvailableOn == dayofWeek).ToList();
										if (AllDaySchedulesTimimgs.Any())
										{
											//var CurrentTime = DateTime.Now.TOS;
											var CurrentTime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).TimeOfDay;
											foreach (var __item in AllDaySchedulesTimimgs)
											{
												if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
												{
													item.IsRestAvailable = true;
												}
												DateTime fromTime = DateTime.Today.Add(__item.ScheduleFromTime);
												string displayFromTime = fromTime.ToString("hh:mm tt");
												DateTime toTime = DateTime.Today.Add(__item.ScheduleToTime);
												string displayToTime = toTime.ToString("hh:mm tt");
												string fromToTime = displayFromTime + " - " + displayToTime;
												item.OpenCloseTime.Add(fromToTime);

											}
										}
										else
										{
											item.IsRestAvailable = true;
										}
									}
									else
									{
										item.IsRestAvailable = true;
									}
								}
								else
								{
									item.IsRestAvailable = true;
								}

								TopRatedChefnRestaurantlist.Add(item);
							}
						}
					}
					if (TopRatedChefnRestaurantlist.Any())
					{
						TopRatedChefnRestaurantlist = TopRatedChefnRestaurantlist.OrderByDescending(x => x.Rating).Take(topRatedRestnChefModel.SplitSize).Skip(topRatedRestnChefModel.EndItemCount).ToList();
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = TopRatedChefnRestaurantlist.Distinct().ToList();
					}
					else
					{
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(topRatedRestnChefModel.StoreId, "API.ErrorMesaage.RestaurentNotAvailableAtYourLocation", languageId, dbcontext);
						customerAPIResponses.ErrorMessageTitle = "Error!!";
					}
				}
				else
				{
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(topRatedRestnChefModel.StoreId, "API.ErrorMesaage.RestaurentNotAvailableAtYourLocation", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
				}
				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v2/GetTopRatedCooknRest")]
		[HttpPost]
		public BaseAPIResponseModel GetTopRatedCooknRest([FromBody] TopRatedRestnChefModel requestModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(requestModel.StoreId, requestModel.UniqueSeoCode, dbcontext);
			if (requestModel == null)
				return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext));
			if (string.IsNullOrEmpty(requestModel.ApiKey))
				return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMesaage.AuthenticationKey", languageId, dbcontext));
			if (requestModel.StoreId == 0)
				return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext));
			try
			{
				var lstMerchantIds = new List<int>();
				var result = _commonService.GetTopRatedCooknRestDbResult(requestModel);
				if (result != null && result.Length > 0)
				{
					if (result == "Invalid Authentication key")
						return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext));
					else if (result == "Store Not Found")
						return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMesaage.StoreNotFound", languageId, dbcontext));
				}
				lstMerchantIds = result.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(x => int.Parse(x)).ToList();
				var key = string.Format(CacheKeys.MerchantListKey, requestModel.StoreId, requestModel.LatPos,requestModel.LongPos,requestModel.RestType,requestModel.SearchValue,requestModel.MerchantCategoryIds, requestModel.GetMerchantOffer, requestModel.SplitSize,requestModel.SkipLocationSearch,  languageId);
				var restData = _cacheManager.Get(key, () =>
				{
					return _commonService.GetTopRatedCooknRest(requestModel, lstMerchantIds);
				});

				if (restData.Count<=0)
					return BaseMethodsHelper.ErrorResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMesaage.RestaurentNotAvailableAtYourLocation", languageId, dbcontext));
				return BaseMethodsHelper.SuccessResponse(LanguageHelper.GetResourseValueByName(requestModel.StoreId, "API.ErrorMessage.MerchantFound", languageId, dbcontext), restData);
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				return BaseMethodsHelper.ErrorResponse(ex.Message, HttpStatusCode.BadRequest);
			}
		}

		/// <summary>
		///   GetCooknRestDishesFiltersCategoryValues
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/GetCooknRestDishesFiltersCategoryValues")]
		[HttpPost]
		public CustomerAPIResponses GetCooknRestDishesFiltersCategoryValuesv2([FromBody] CountryModel2 customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(customer.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == customer.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (customer.StoreId != 0)
				{
					var CustomerExist = dbcontext.Store.Where(x => x.Id == customer.StoreId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var Filter = Enum.GetValues(typeof(FiltersEnum)).Cast<FiltersEnum>().Where(x => (int)x == 2).Select(x => x.ToString()).FirstOrDefault();
				var CategoryTitleFilters = (from a in dbcontext.FilterCategory
											join b in dbcontext.Type
											on a.Type equals b.Id
											where a.FilterFor == Filter
											&& a.StoreId == customer.StoreId
											select new SearchFiltersMain
											{
												TitleId = a.Id,
												TitleName = a.Name,
												TitleType = b.Name,
												CategoryValueFilter = dbcontext.FilterCategoryValues.Where(x => x.FilterCategoryId == a.Id && x.StoreId == customer.StoreId).Select(x => new SearchFilters
												{
													TitleValueId = x.Id,
													TitleValueName = x.Name,
												}).ToList()
											}).OrderBy(x => x.TitleId).ToList();
				if (CategoryTitleFilters.Any())
				{
					foreach (var item in CategoryTitleFilters)
					{
						if (item.TitleName.ToLower().Contains("preferences"))
						{
							item.CategoryValueFilter = new List<SearchFilters>();
							var Tags = dbcontext.ProductTag.Select(x => new SearchFilters
							{
								TitleValueId = x.Id,
								TitleValueName = x.Name.ToUpper().ToString(),
							}).ToList();
							item.CategoryValueFilter = Tags;
						}
						if (item.TitleName.ToLower().Contains("price"))
						{
							item.CategoryValueFilter = new List<SearchFilters>();
							SearchFilters searchFilters = new SearchFilters();
							searchFilters.TitleValueId = 35;
							searchFilters.TitleValueName = "0";
							item.CategoryValueFilter.Add(searchFilters);
							SearchFilters searchFilters2 = new SearchFilters();
							searchFilters2.TitleValueId = 36;

							var price = (from pro in dbcontext.Product.Where(x => x.Published == true && x.Deleted == false && x.VendorId == customer.MId)
										 select new
										 {
											 price = pro.Price
										 }).OrderByDescending(x => x.price).FirstOrDefault().price;
							searchFilters2.TitleValueName = Convert.ToInt32(price).ToString();
							item.CategoryValueFilter.Add(searchFilters2);
						}
					}
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = CategoryTitleFilters;
				}
				else
				{
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.Datanotavailable", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
				}

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}
		[Route("~/api/v1/GetStoreoptions")]
		[HttpPost]
		public CustomerAPIResponses GetStoreoptions([FromBody] CountryModel searchCooknChefDishesModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(searchCooknChefDishesModel.StoreId, searchCooknChefDishesModel.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (searchCooknChefDishesModel == null)
			{
				//KP Comments//Please use a function for response
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(searchCooknChefDishesModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			if (searchCooknChefDishesModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(searchCooknChefDishesModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == searchCooknChefDishesModel.ApiKey && x.StoreId == searchCooknChefDishesModel.StoreId).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (searchCooknChefDishesModel.StoreId != 0)
				{
					var CustomerExist = dbcontext.Store.Where(x => x.Id == searchCooknChefDishesModel.StoreId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}


				var StoreOptions = (from a in dbcontext.Store
									where a.Id == searchCooknChefDishesModel.StoreId
									select new StoreOptions
									{
										IsDelivery = a.IsDelivery != null ? a.IsDelivery.Value : false,
										IsDinning = a.IsDinning != null ? a.IsDinning.Value : false,
										IsTakeaway = a.IsTakeaway != null ? a.IsTakeaway.Value : false

									}).ToList();

				if (StoreOptions.Any())
				{
					List<RestTypeModel> list = new List<RestTypeModel>();
					foreach (var item in StoreOptions)
					{

						if (item.IsDelivery)
						{
							RestTypeModel typeModel = new RestTypeModel();
							typeModel.IsAvailable = item.IsDelivery;
							typeModel.Id = (int)RestType.Delivery;
							typeModel.Name = Enum.GetValues(typeof(RestType)).Cast<RestType>().Where(x => (int)x == (int)RestType.Delivery).Select(x => x.ToString()).FirstOrDefault();
							list.Add(typeModel);
						}
						if (!item.IsDelivery)
						{
							RestTypeModel typeModel = new RestTypeModel();
							typeModel.IsAvailable = item.IsDelivery;
							typeModel.Id = (int)RestType.Delivery;
							typeModel.Name = Enum.GetValues(typeof(RestType)).Cast<RestType>().Where(x => (int)x == (int)RestType.Delivery).Select(x => x.ToString()).FirstOrDefault();
							list.Add(typeModel);
						}
						if (item.IsDinning)
						{
							RestTypeModel typeModel = new RestTypeModel();
							typeModel.IsAvailable = item.IsDinning;
							typeModel.Id = (int)RestType.Dining;
							typeModel.Name = Enum.GetValues(typeof(RestType)).Cast<RestType>().Where(x => (int)x == (int)RestType.Dining).Select(x => x.ToString()).FirstOrDefault();
							list.Add(typeModel);
						}
						if (!item.IsDinning)
						{
							RestTypeModel typeModel = new RestTypeModel();
							typeModel.IsAvailable = item.IsDinning;
							typeModel.Id = (int)RestType.Dining;
							typeModel.Name = Enum.GetValues(typeof(RestType)).Cast<RestType>().Where(x => (int)x == (int)RestType.Dining).Select(x => x.ToString()).FirstOrDefault();
							list.Add(typeModel);
						}
						if (item.IsTakeaway)
						{
							RestTypeModel typeModel = new RestTypeModel();
							typeModel.IsAvailable = item.IsTakeaway;
							typeModel.Id = (int)RestType.TakeAway;
							typeModel.Name = Enum.GetValues(typeof(RestType)).Cast<RestType>().Where(x => (int)x == (int)RestType.TakeAway).Select(x => x.ToString()).FirstOrDefault();
							list.Add(typeModel);
						}
						if (!item.IsTakeaway)
						{
							RestTypeModel typeModel = new RestTypeModel();
							typeModel.IsAvailable = item.IsTakeaway;
							typeModel.Id = (int)RestType.TakeAway;
							typeModel.Name = Enum.GetValues(typeof(RestType)).Cast<RestType>().Where(x => (int)x == (int)RestType.TakeAway).Select(x => x.ToString()).FirstOrDefault();
							list.Add(typeModel);
						}
						//list.Add(typeModel);
					}
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.DataAvailable", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Success!!";
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = list.ToList();

				}
				else
				{
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(searchCooknChefDishesModel.StoreId, "API.ErrorMesaage.SorryNoDataAvailable", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
				}

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		/// <summary>
		///   GetCooknRestDetailsByID
		/// <param name="customer"></param>
		/// <returns></returns>
		/// </summary>
		[Route("~/api/v1/GetCooknRestDetailsByID")]
		[HttpPost]
		public CustomerAPIResponses GetCooknRestDetailsByIDV2([FromBody] CooknRestDetailsByID customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.APIKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.CustId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.RestnCookId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.RestaurantAndCookIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(customer.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == customer.APIKey && x.StoreId == customer.StoreId).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (customer.CustId != 0)
				{
					var CustomerExist = dbcontext.Customer.Where(x => x.Id == customer.CustId && x.RegisteredInStoreId == customer.StoreId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (customer.RestnCookId != 0)
				{
					var CustomerExist = dbcontext.Vendor.Where(x => x.Id == customer.RestnCookId && x.StoreId == customer.StoreId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.RestaurantNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				if (customer.StoreId != 0)
				{
					var CustomerExist = dbcontext.Store.Where(x => x.Id == customer.StoreId).Any();
					if (!CustomerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == customer.StoreId).FirstOrDefault().Value;
				var isApproved = dbcontext.Setting.Where(x => x.Name.Contains("ratingreviewsettings.merchant.isapprovalrequired") && x.StoreId == customer.StoreId).Any() ? Convert.ToBoolean(dbcontext.Setting.Where(x => x.Name.Contains("ratingreviewsettings.merchant.isapprovalrequired") && x.StoreId == customer.StoreId).FirstOrDefault().Value) : false;
				var GetRestaurantnCookById = (from a in dbcontext.Vendor.Where(x => x.Id == customer.RestnCookId)
											  select new CooknRestV2
											  {
												  RestnCookId = a.Id,
												  CooknRestName = a.Name.ToUpper(),
												  Geolocation=a.Geolocation,
												  Description=a.Description,
												  PictureId = a.PictureId,
												  RestnCookAddress = a.AddressId.ToString(),
												  IsOpen = a.IsOpen == null ? false : a.IsOpen.Value,
												  OpenTime = a.Opentime,
												  CloseTime = a.Closetime
											  }).ToList();
				if (GetRestaurantnCookById.Any())
				{
					foreach (var item in GetRestaurantnCookById)
					{
						item.Cuisine = (from products in dbcontext.Product.AsEnumerable()
										join product_cuisine_map in dbcontext.ProductProductCuisineMapping.AsEnumerable()
										on products.Id equals product_cuisine_map.ProductId
										join cuisineName in dbcontext.ProductCuisine.AsEnumerable()
										on product_cuisine_map.CuisineId equals cuisineName.Id
										where products.VendorId == item.RestnCookId && products.Deleted == false && products.Published == true
										select new
										{
											cuisineName
										}).Distinct().Any() ?
																		   (from products in dbcontext.Product.AsEnumerable()
																			join product_cuisine_map in dbcontext.ProductProductCuisineMapping.AsEnumerable()
																			on products.Id equals product_cuisine_map.ProductId
																			join cuisineName in dbcontext.ProductCuisine.AsEnumerable()
																			on product_cuisine_map.CuisineId equals cuisineName.Id
																			where products.VendorId == item.RestnCookId && products.Deleted == false && products.Published == true
																			select new Items
																			{
																				Name = cuisineName.Name
																			}).Distinct().ToList() : new List<Items>();
						var address = Convert.ToInt32(item.RestnCookAddress);
						var Addreess2 = dbcontext.Address.Where(x => x.Id == address).FirstOrDefault();
						if (Addreess2 != null)
						{
							item.RestnCookAddress = Addreess2.Address1 + " " + Addreess2.Address2;
						}
						item.PhoneNumber = dbcontext.Address.Where(x => x.Id == address).FirstOrDefault()?.PhoneNumber??"";
						string[] venLtLngStrArr = item.Geolocation.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
						double latRest = Convert.ToDouble(venLtLngStrArr[0]);
						double longRest = Convert.ToDouble(venLtLngStrArr[1]);
						var distancetype = dbcontext.Setting.Where(x => x.Name.Contains("setting.customer.distancetype") && x.StoreId == customer.StoreId).FirstOrDefault()?.Value ?? "";
						item.Distance = String.Format("{0:0.00}" + " " + distancetype, Helper.distance(Convert.ToDouble(customer.CustLat), Convert.ToDouble(customer.CustLong), latRest, longRest, "K"));
						item.Latitude = Convert.ToDouble(Addreess2.Latitude);
						item.Longitude =Convert.ToDouble(Addreess2.Longitude);
						item.MerchantOffer= Helper.GetMerchantOffers(customer.StoreId, item.RestnCookId, dbcontext);
						item.IsInFavorite = Helper.IsMerchantInFavorite(item.RestnCookId, customer.CustId, dbcontext);
						item.Rating = isApproved ? (!(from rating in dbcontext.RatingReviews.Where(x => x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == customer.StoreId
																  && x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved && x.IsLiked)
													  where rating.EntityId == item.RestnCookId
													  select new
													  {
														  rating
													  }).Any() ? 0 :
								 (from rating in dbcontext.RatingReviews.Where(x => x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == customer.StoreId
																&& x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved && x.IsLiked)
								  where rating.EntityId == item.RestnCookId
								  select new
								  {
									  rating
								  }).ToList().Count(x => x.rating.IsLiked)) : (!(from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == customer.StoreId
																&& x.IsNotInterested == false && x.IsSkipped == false && x.IsLiked)
																				 where rating.EntityId == item.RestnCookId
																				 select new
																				 {
																					 rating
																				 }).Any() ? 0 :
								 (from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == customer.StoreId
																&& x.IsNotInterested == false && x.IsSkipped == false && x.IsLiked)
								  where rating.EntityId == item.RestnCookId
								  select new
								  {
									  rating
								  }).ToList().Count(x => x.rating.IsLiked));
						item.RatingCount = isApproved ? (from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == customer.StoreId
									 && x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved)
														 where rating.EntityId == item.RestnCookId
														 select new
														 {
															 rating.Rating
														 }).Count() : (from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == customer.StoreId
														&& x.IsNotInterested == false && x.IsSkipped == false)
																	   where rating.EntityId == item.RestnCookId
																	   select new
																	   {
																		   rating.Rating
																	   }).Count();
						if (item.Rating != 0)
						{
							item.Rating = Convert.ToDecimal((item.Rating / item.RatingCount));
							item.Rating = Math.Round(item.Rating * 5, 2);
						}
						int pictureSize = 100;
						var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.vendorthumbpicturesize") && x.StoreId==customer.StoreId).FirstOrDefault();
						if (setting != null)
						{
							pictureSize = Convert.ToInt32(setting.Value);
						}
						if (item.PictureId != 0)
						{

							var picture = dbcontext.Picture.Where(x => x.Id == item.PictureId).FirstOrDefault();
							string lastPart = Helper.GetFileExtensionFromMimeType(picture.MimeType);
							string thumbFileName = !string.IsNullOrEmpty(picture.SeoFilename)
							? $"{picture.Id:0000000}_{picture.SeoFilename}_{pictureSize}.{lastPart}" + "? " + DateTime.Now
							: $"{picture.Id:0000000}_{pictureSize}.{lastPart}" + "? " + DateTime.Now;
							item.CooknRestImageURL = ChefBaseUrl + thumbFileName;
						}
						else
						{
							item.CooknRestImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;

						}
						var topRatedChefnRestaurant = dbcontext.Vendor.AsEnumerable().Where(x => x.StoreId == customer.StoreId && x.Id == customer.RestnCookId).FirstOrDefault();
						if (topRatedChefnRestaurant != null && topRatedChefnRestaurant.IsOpen != null && topRatedChefnRestaurant.IsOpen.Value)
						{
							try
							{
								var dateTimeNow = DateTime.Now;
								var startTime = dateTimeNow.Date.Add(DateTime.Parse(topRatedChefnRestaurant.Opentime).TimeOfDay);
								var closeTime = dateTimeNow.Date.Add(DateTime.Parse(topRatedChefnRestaurant.Closetime).TimeOfDay);

								if (dateTimeNow >= startTime && dateTimeNow <= closeTime)
								{
									item.IsDelivery = topRatedChefnRestaurant.IsDelivery == null ? false : topRatedChefnRestaurant.IsDelivery.Value;
									item.IsTakeAway = topRatedChefnRestaurant.IsTakeAway == null ? false : topRatedChefnRestaurant.IsTakeAway.Value;
								}
							}
							catch { }
						}
					}
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = GetRestaurantnCookById;
				}
				else
				{
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.RestaurantDetailsNotAvailable", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
				}

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v1/GetCooknRestProductsByRestnCookId")]
		[HttpPost]
		public CustomerAPIResponses GetCooknRestItemsByRestnCookIdv4([FromBody] CooknRestProductByID customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.APIKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.CustId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext); LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.RestnCookId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.RestaurantIdNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(customer.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == customer.APIKey && x.StoreId == customer.StoreId).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}



				string CurrencySymbol = Helper.GetStoreCurrency(customer.StoreId, dbcontext);
				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == customer.StoreId).FirstOrDefault()?.Value;
				var GetRestaurantnCookById = (from a in dbcontext.Category.AsEnumerable().Where(x => x.Deleted == false && x.Published == true && x.ParentCategoryId == 0)
											  join b in dbcontext.StoreMapping.AsEnumerable().Where(x => x.VendorId == customer.RestnCookId && x.EntityName == "Category")
											  on a.Id equals b.EntityId
											  select new CooknRestProductsV2
											  {
												  CategoryId = a?.Id ?? 0,
												  CategoryName = LanguageHelper.GetLocalizedValueCategory(a.Id, "Name", languageId, a.Name, dbcontext),
												  Subcategories = (from _a in dbcontext.Category.AsEnumerable().Where(x => x.Deleted == false && x.Published == true && x.ParentCategoryId == (a?.Id ?? 0))
																   join _b in dbcontext.StoreMapping.AsEnumerable().Where(x => x.VendorId == customer.RestnCookId && x.EntityName == "Category")
																   on _a.Id equals _b.EntityId
																   select new CooknRestProductsV3
																   {
																	   CategoryId = _a?.Id ?? 0,
																	   CategoryName = LanguageHelper.GetLocalizedValueCategory(_a.Id, "Name", languageId, _a.Name, dbcontext),
																	   CategoryItems = (from _c in dbcontext.Product.AsEnumerable().Where(x => x.VendorId == customer.RestnCookId && x.Published == true && x.Price != 0)
																						join _d in dbcontext.ProductCategoryMapping.AsEnumerable().Where(x => x.CategoryId == (_a?.Id ?? 0))
																						on _c.Id equals _d.ProductId
																						where _c.VendorId == customer.RestnCookId
																						select new CooknChefCatProductsV2
																						{
																							ItemId = _c.Id,
																							ItemName = LanguageHelper.GetLocalizedValueProduct(_c.Id, "Name", languageId, _c.Name, dbcontext),
																							Quantity = dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.ProductId == _c.Id && x.CustomerId == customer.CustId).Any() ? dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.ProductId == _c.Id && x.CustomerId == customer.CustId).Sum(x => x.Quantity) : 0,
																							Description = LanguageHelper.GetLocalizedValueProduct(_c.Id, "ShortDescription", languageId, _c.ShortDescription ?? "", dbcontext),
																							Available = _c.Published,
																							Cuisine = (from productcuisine in dbcontext.ProductProductCuisineMapping.AsEnumerable()
																									   join product in dbcontext.ProductCuisine.AsEnumerable()
																									   on productcuisine.CuisineId equals product.Id
																									   where productcuisine.ProductId == _c.Id

																									   select new
																									   {
																										   product.Name
																									   }).FirstOrDefault() != null ? (from productcuisine in dbcontext.ProductProductCuisineMapping.AsEnumerable()
																																	  join product in dbcontext.ProductCuisine.AsEnumerable()
																																	  on productcuisine.CuisineId equals product.Id
																																	  where productcuisine.ProductId == _c.Id
																																	  select new
																																	  {
																																		  product.Name
																																	  }).FirstOrDefault()?.Name : "",
																							Preferences = (from ProductTag in dbcontext.ProductProductTagMapping.AsEnumerable().Where(x => x.ProductId == _c.Id)
																										   join ProductTagss in dbcontext.ProductTag.AsEnumerable()
																										   on ProductTag.ProductTagId equals ProductTagss.Id
																										   join ProductTagImage in dbcontext.ProductTagImageMapping.AsEnumerable()
																										   on ProductTagss.Id equals ProductTagImage.TagId
																										   join ProductTagImageDetails in dbcontext.Picture.AsEnumerable()
																										   on ProductTagImage.PictureId equals ProductTagImageDetails.Id
																										   select new PreferencesV2
																										   {
																											   Name = ProductTagss?.Name,
																											   Image =
																												   ProductTagImageDetails?.Id.ToString(),
																											   ImageType = ProductTagImageDetails?.MimeType,


																										   }
																								).Distinct().ToList(),
																							ImageUrl = (from product in dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == _c.Id)
																										join picture in dbcontext.Picture.AsEnumerable()
																										on product.PictureId equals picture.Id
																										select new CooknChefProductsImagesV2
																										{
																											ProductImageURL = picture?.Id.ToString(),
																											MimeType = picture?.MimeType,
																											FileSeo = picture.SeoFilename

																										}).ToList(),
																							Price = dbcontext.Product.AsEnumerable().Where(x => x.Id == _c.Id && x.TaxCategoryId == 0).FirstOrDefault() != null
																									   ? dbcontext.Product.AsEnumerable().Where(x => x.Id == _c.Id && x.TaxCategoryId == 0).FirstOrDefault().Price.ToString() : "" + (from priceCategoryTax in dbcontext.Product.AsEnumerable()
																																																									  join categoryTax in dbcontext.TaxRate.AsEnumerable()
																																																							  on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
																																																									  where categoryTax.StoreId == customer.StoreId &&
																																																									  priceCategoryTax.Id == _c.Id

																																																									  select new
																																																									  {
																																																										  PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
																																																									  }).FirstOrDefault().PriceCalculated != null ? (from priceCategoryTax in dbcontext.Product.AsEnumerable()
																																																																					 join categoryTax in dbcontext.TaxRate.AsEnumerable()
																																																																									 on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
																																																																					 where categoryTax.StoreId == customer.StoreId &&
																																																																					 priceCategoryTax.Id == _c.Id

																																																																					 select new
																																																																					 {
																																																																						 PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
																																																																					 }).FirstOrDefault().PriceCalculated.ToString() : "0.0",
																							Rating = (!(from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == _c.Id && x.IsApproved && x.ReviewType == 3)
																										select new
																										{
																											rating.Rating
																										}).Any()) ? 0 :
																							(from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == _c.Id && x.IsApproved && x.ReviewType == 3)

																							 select new RatingModel
																							 {
																								 Rating = rating.Rating
																							 }).ToList().Sum(x => x.Rating),
																							ProductProductAttributes = (from _d in dbcontext.Product.AsEnumerable().Where(x => x.Id == _c.Id)
																														join _e in dbcontext.ProductProductAttributeMapping.AsEnumerable()
																														on _d.Id equals _e.ProductId
																														join _f in dbcontext.ProductAttribute.AsEnumerable()
																														on _e.ProductAttributeId equals _f.Id
																														select new CooknRestProductProductAttributesResponse
																														{
																															AttributeName = _f.Name,
																															IsRequired = _e.IsRequired,
																															AttributeTypeId = _e.AttributeControlTypeId,
																															ProductId = _d.Id,
																															ProductAttributeId = _f.Id,
																															ProductAttributeMappingId = _e.Id,
																															ProductAttribute = (from __d in dbcontext.ProductAttributeValue.AsEnumerable()
																																				where __d.ProductAttributeMappingId == _e.Id
																																				select new ProductAttributes
																																				{
																																					Name = __d.Name,
																																					Price = __d.PriceAdjustment.ToString(),
																																					ProductAttributeValueId = __d.Id,
																																					IsPreSelected = __d.IsPreSelected,
																																					Currency = CurrencySymbol
																																				}).ToList()
																														}).ToList(),
																							IsProductAttributesExist = (from _d in dbcontext.Product.AsEnumerable().Where(x => x.Id == _c.Id)
																														join _e in dbcontext.ProductProductAttributeMapping.AsEnumerable()
																														on _d.Id equals _e.ProductId
																														join _f in dbcontext.ProductAttribute.AsEnumerable()
																														on _e.ProductAttributeId equals _f.Id
																														select new CooknRestProductProductAttributesResponse
																														{
																															AttributeName = _f.Name,
																															IsRequired = _e.IsRequired,
																															AttributeTypeId = _e.AttributeControlTypeId,
																															ProductId = _d.Id,
																															ProductAttributeId = _f.Id,
																															ProductAttributeMappingId = _e.Id,
																															ProductAttribute = (from __d in dbcontext.ProductAttributeValue.AsEnumerable()
																																				where __d.ProductAttributeMappingId == _e.Id
																																				select new ProductAttributes
																																				{
																																					Name = __d.Name,
																																					Price = __d.PriceAdjustment.ToString(),
																																					ProductAttributeValueId = __d.Id,
																																					IsPreSelected = __d.IsPreSelected,
																																					Currency = CurrencySymbol
																																				}).ToList()
																														}).Any(),
																							RatingCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == _c.Id && x.IsApproved && x.ReviewType == 3)

																										   select new
																										   {
																											   rating.Rating
																										   }).Count(),
																							ReviewCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == _c.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item && !string.IsNullOrEmpty(x.ReviewText))

																										   select new
																										   {
																											   rating.ReviewText
																										   }).Count(),
																							CustomerImages = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == _c.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item && !string.IsNullOrEmpty(x.ReviewText))
																											  join customerImage in dbcontext.GenericAttribute.AsEnumerable().Where(x => x.Key == "ProfilePic")
																											  on rating.CustomerId equals customerImage.EntityId
																											  select new CustomerImage
																											  {
																												  Image = customerImage.Value
																											  }).ToList(),
																							////Cuisine = dbcontext.FilterCategoryValues.Where(x => x.Id == c.cuisine.Value).Any() ? (dbcontext.FilterCategoryValues.Where(x => x.Id == c.cuisine.Value).FirstOrDefault().Name) : "NA",
																						}).OrderBy(x => x.ItemId).ToList(),
																   }).ToList(),
												  CategoryItems = (from c in dbcontext.Product.AsEnumerable()
																   join d in dbcontext.ProductCategoryMapping.AsEnumerable()
																   on c.Id equals d.ProductId
																   where c.VendorId == customer.RestnCookId
																   && d.CategoryId == a.Id && c.Deleted == false && c.Published == true
																   && c.Price != 0
																   select new CooknChefCatProductsV2
																   {
																	   ItemId = c.Id,
																	   ItemName = LanguageHelper.GetLocalizedValueProduct(c.Id, "Name", languageId, c.Name, dbcontext),
																	   Quantity = dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.ProductId == c.Id && x.CustomerId == customer.CustId).Any() ? dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.ProductId == c.Id && x.CustomerId == customer.CustId).Sum(x => x.Quantity) : 0,
																	   Description = LanguageHelper.GetLocalizedValueProduct(c.Id, "ShortDescription", languageId, c.ShortDescription ?? "", dbcontext),
																	   Available = c.Published,
																	   Cuisine = (from productcuisine in dbcontext.ProductProductCuisineMapping.AsEnumerable()
																				  join product in dbcontext.ProductCuisine.AsEnumerable()
																				  on productcuisine.CuisineId equals product.Id
																				  where productcuisine.ProductId == c.Id

																				  select new
																				  {
																					  product.Name
																				  }).FirstOrDefault() != null ? (from productcuisine in dbcontext.ProductProductCuisineMapping.AsEnumerable()
																												 join product in dbcontext.ProductCuisine.AsEnumerable()
																												 on productcuisine.CuisineId equals product.Id
																												 where productcuisine.ProductId == c.Id
																												 select new
																												 {
																													 product.Name
																												 }).FirstOrDefault().Name : "",
																	   Preferences = (from ProductTag in dbcontext.ProductProductTagMapping.AsEnumerable().Where(x => x.ProductId == c.Id)
																					  join ProductTagss in dbcontext.ProductTag.AsEnumerable()
																					  on ProductTag.ProductTagId equals ProductTagss.Id
																					  join ProductTagImage in dbcontext.ProductTagImageMapping.AsEnumerable()
																					  on ProductTagss.Id equals ProductTagImage.TagId
																					  join ProductTagImageDetails in dbcontext.Picture.AsEnumerable()
																					  on ProductTagImage.PictureId equals ProductTagImageDetails.Id
																					  select new PreferencesV2
																					  {
																						  Name = ProductTagss.Name,
																						  Image =
																							  ProductTagImageDetails.Id.ToString(),
																						  ImageType = ProductTagImageDetails.MimeType,


																					  }
																 ).Distinct().ToList(),
																	   ImageUrl = (from product in dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == c.Id)
																				   join picture in dbcontext.Picture.AsEnumerable()
																				   on product.PictureId equals picture.Id
																				   select new CooknChefProductsImagesV2
																				   {
																					   ProductImageURL = picture.Id.ToString(),
																					   MimeType = picture.MimeType,
																					   FileSeo = picture.SeoFilename

																				   }).ToList(),
																	   Price = dbcontext.Product.AsEnumerable().Where(x => x.Id == c.Id && x.TaxCategoryId == 0).FirstOrDefault() != null
																		? (dbcontext.Product.AsEnumerable().Where(x => x.Id == c.Id && x.TaxCategoryId == 0).FirstOrDefault()?.Price ?? 0).ToString() : "" + (from priceCategoryTax in dbcontext.Product.AsEnumerable()
																																																			  join categoryTax in dbcontext.TaxRate.AsEnumerable()
																																																	   on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
																																																			  where categoryTax.StoreId == customer.StoreId &&
																																																			  priceCategoryTax.Id == c.Id

																																																			  select new
																																																			  {
																																																				  PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
																																																			  }).FirstOrDefault()?.PriceCalculated != null ? (from priceCategoryTax in dbcontext.Product.AsEnumerable()
																																																															  join categoryTax in dbcontext.TaxRate.AsEnumerable()
																																																																			  on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
																																																															  where categoryTax.StoreId == customer.StoreId &&
																																																															  priceCategoryTax.Id == c.Id

																																																															  select new
																																																															  {
																																																																  PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
																																																															  }).FirstOrDefault()?.PriceCalculated.ToString() : "0.0",
																	   Rating = (!(from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == c.Id && x.IsApproved && x.ReviewType == 3)
																				   select new
																				   {
																					   rating.Rating
																				   }).Any()) ? 0 :
																	   (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == c.Id && x.IsApproved && x.ReviewType == 3)

																		select new RatingModel
																		{
																			Rating = rating.Rating
																		}).ToList().Sum(x => x.Rating),
																	   ProductProductAttributes = (from d in dbcontext.Product.AsEnumerable().Where(x => x.Id == c.Id)
																								   join e in dbcontext.ProductProductAttributeMapping.AsEnumerable()
																								   on d.Id equals e.ProductId
																								   join f in dbcontext.ProductAttribute.AsEnumerable()
																								   on e.ProductAttributeId equals f.Id
																								   select new CooknRestProductProductAttributesResponse
																								   {
																									   AttributeName = f.Name,
																									   IsRequired = e.IsRequired,
																									   AttributeTypeId = e.AttributeControlTypeId,
																									   ProductId = d.Id,
																									   ProductAttributeId = f.Id,
																									   ProductAttributeMappingId = e.Id,
																									   ProductAttribute = (from _d in dbcontext.ProductAttributeValue.AsEnumerable()
																														   where _d.ProductAttributeMappingId == e.Id
																														   select new ProductAttributes
																														   {
																															   Name = _d.Name,
																															   Price = _d.PriceAdjustment.ToString(),
																															   ProductAttributeValueId = _d.Id,
																															   IsPreSelected = _d.IsPreSelected,
																															   Currency = CurrencySymbol
																														   }).ToList()
																								   }).ToList(),
																	   IsProductAttributesExist = (from d in dbcontext.Product.AsEnumerable().Where(x => x.Id == c.Id)
																								   join e in dbcontext.ProductProductAttributeMapping.AsEnumerable()
																								   on d.Id equals e.ProductId
																								   join f in dbcontext.ProductAttribute.AsEnumerable()
																								   on e.ProductAttributeId equals f.Id
																								   select new CooknRestProductProductAttributesResponse
																								   {
																									   AttributeName = f.Name,
																									   IsRequired = e.IsRequired,
																									   AttributeTypeId = e.AttributeControlTypeId,
																									   ProductId = d.Id,
																									   ProductAttributeId = f.Id,
																									   ProductAttributeMappingId = e.Id,
																									   ProductAttribute = (from _d in dbcontext.ProductAttributeValue.AsEnumerable()
																														   where _d.ProductAttributeMappingId == e.Id
																														   select new ProductAttributes
																														   {
																															   Name = _d.Name,
																															   Price = _d.PriceAdjustment.ToString(),
																															   ProductAttributeValueId = _d.Id,
																															   IsPreSelected = _d.IsPreSelected,
																															   Currency = CurrencySymbol
																														   }).ToList()
																								   }).Any(),
																	   RatingCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == c.Id && x.IsApproved && x.ReviewType == 3)

																					  select new
																					  {
																						  rating.Rating
																					  }).Count(),
																	   ReviewCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == c.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item && !string.IsNullOrEmpty(x.ReviewText))

																					  select new
																					  {
																						  rating.ReviewText
																					  }).Count(),
																	   CustomerImages = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == c.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item && !string.IsNullOrEmpty(x.ReviewText))
																						 join customerImage in dbcontext.GenericAttribute.AsEnumerable().Where(x => x.Key == "ProfilePic")
																						 on rating.CustomerId equals customerImage.EntityId
																						 select new CustomerImage
																						 {
																							 Image = customerImage.Value
																						 }).ToList(),
																	   ////Cuisine = dbcontext.FilterCategoryValues.Where(x => x.Id == c.cuisine.Value).Any() ? (dbcontext.FilterCategoryValues.Where(x => x.Id == c.cuisine.Value).FirstOrDefault().Name) : "NA",
																   }).OrderBy(x => x.ItemId).ToList(),

											  }).OrderBy(x => x.CategoryId).ToList();
				if (GetRestaurantnCookById.Any())
				{
					List<CooknRestProductsV2> list = new List<CooknRestProductsV2>();
					foreach (var item in GetRestaurantnCookById)
					{

						if (item.CategoryItems.Any() || item.Subcategories.Any())
						{
							foreach (var __item in item.Subcategories)
							{
								foreach (var ___item in __item.CategoryItems)
								{
									foreach (var pref in ___item.ProductProductAttributes)
									{
										foreach (var ______item in pref.ProductAttribute)
										{
											______item.Price = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(______item.Price));
										}
									}
									___item.Price = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(___item.Price));
									foreach (var pref in ___item.Preferences)
									{
										int pictureId = Convert.ToInt32(pref.Image);
										string lastPart = Helper.GetFileExtensionFromMimeType(pref.ImageType);
										var prefdetail = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();
										var fileName = !string.IsNullOrEmpty(prefdetail.SeoFilename)
													? $"{pictureId:0000000}_{prefdetail.SeoFilename}_{75}.{lastPart}" + "? " + DateTime.Now
													: $"{pictureId:0000000}_{75}.{lastPart}" + "? " + DateTime.Now;
										pref.Image = ChefBaseUrl + fileName;
									}
									int pictureSize = 75;
									var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
									if (setting != null)
									{
										pictureSize = Convert.ToInt32(setting.Value);
									}
									if (___item.ImageUrl.Any())
										foreach (var pref in ___item.ImageUrl)
										{
											if (!string.IsNullOrEmpty(pref.ProductImageURL))
											{
												int pictureId = Convert.ToInt32(pref.ProductImageURL);
												if (pictureId != 0)
												{

													string lastPart = Helper.GetFileExtensionFromMimeType(pref.MimeType);
													string thumbFileName = !string.IsNullOrEmpty(pref.FileSeo)
													? $"{pictureId:0000000}_{pref.FileSeo}.{lastPart}" + "? " + DateTime.Now
													: $"{pictureId:0000000}_{pictureSize}.{lastPart}" + "? " + DateTime.Now;

													pref.ProductImageURL = ChefBaseUrl + thumbFileName + "?" + DateTime.Now;
												}
												else
												{
													pref.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
												}
											}
											else
											{
												pref.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
											}
											//int pictureId = Convert.ToInt32(pref.ProductImageURL);
											//var picture = dbcontext.Pictures.Where(x => x.Id == pictureId).FirstOrDefault();
											//string lastPart = Helper.GetFileExtensionFromMimeType(picture.MimeType);
											//var fileName = $"{picture.Id:0000000}_{75}.{lastPart}";
											//pref.ProductImageURL = ImageBaseUrl + fileName;
										}
									else
									{
										CooknChefProductsImagesV2 cooknChefProductsImagesV2 = new CooknChefProductsImagesV2();
										cooknChefProductsImagesV2.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
										___item.ImageUrl.Add(cooknChefProductsImagesV2);
									}
									if (___item.Rating != 0)
									{
										___item.Rating = Convert.ToDouble(___item.Rating / ___item.RatingCount);
										___item.Rating = Math.Round(___item.Rating, 2);
									}
								}
							}

							foreach (var category in item.CategoryItems)
							{
								category.Price = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(category.Price));
								//CurrencySymbol + "" + Math.Round(Convert.ToDecimal(category.Price), 2);
								foreach (var pref in category.ProductProductAttributes)
								{
									foreach (var ______item in pref.ProductAttribute)
									{
										______item.Price = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(______item.Price));
									}
								}
								foreach (var pref in category.Preferences)
								{

									int pictureId = Convert.ToInt32(pref.Image);
									string lastPart = Helper.GetFileExtensionFromMimeType(pref.ImageType);
									var prefdetail = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();
									var fileName = !string.IsNullOrEmpty(prefdetail.SeoFilename)
												? $"{pictureId:0000000}_{prefdetail.SeoFilename}_{75}.{lastPart}" + "? " + DateTime.Now
												: $"{pictureId:0000000}_{75}.{lastPart}" + "? " + DateTime.Now;
									pref.Image = ChefBaseUrl + fileName;
								}

								int pictureSize = 75;
								var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
								if (setting != null)
								{
									pictureSize = Convert.ToInt32(setting.Value);
								}
								if (category.ImageUrl.Any())
									foreach (var pref in category.ImageUrl)
									{
										if (!string.IsNullOrEmpty(pref.ProductImageURL))
										{
											int pictureId = Convert.ToInt32(pref.ProductImageURL);
											if (pictureId != 0)
											{
												string lastPart = Helper.GetFileExtensionFromMimeType(pref.MimeType);
												string thumbFileName = !string.IsNullOrEmpty(pref.FileSeo)
												? $"{pictureId:0000000}_{pref.FileSeo}.{lastPart}" + "? " + DateTime.Now
												: $"{pictureId:0000000}_{pictureSize}.{lastPart}" + "? " + DateTime.Now;
												pref.ProductImageURL = ChefBaseUrl + thumbFileName;
											}
											else
											{
												pref.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
											}
										}
										else
										{
											pref.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
										}

									}
								else
								{
									CooknChefProductsImagesV2 cooknChefProductsImagesV2 = new CooknChefProductsImagesV2();
									cooknChefProductsImagesV2.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
									category.ImageUrl.Add(cooknChefProductsImagesV2);
								}
								if (category.Rating != 0)
								{
									category.Rating = Convert.ToDouble(category.Rating / category.RatingCount);
									category.Rating = Math.Round(category.Rating, 2);
								}
							}
							list.Add(item);
						}
					}
					if (list.Any() && customer.Filters.Any())
					{
						List<int> RatingList = new List<int>();
						List<int> CuisineList = new List<int>();
						List<int> PreferenceList = new List<int>();
						List<int> PriceList = new List<int>();
						List<int> SortList = new List<int>();
						if (customer.Filters.Any())
						{
							customer.Filters = customer.Filters.OrderBy(x => x.OrderIndex).ToList();
							foreach (var item in customer.Filters)
							{
								foreach (var _item in list)
								{
									item.CategoryName = _item.CategoryName;
									switch (item.Title.ToLower().ToString())
									{
										case "sort":
											{

												switch (item.TitleValueName.ToLower().ToString())
												{
													case "most popular":
														{
															var popularChef = list.Where(x => x.CategoryName == item.CategoryName).FirstOrDefault();
															foreach (var popular in popularChef.CategoryItems)
															{
																SortList.Add(popular.ItemId);
															}
															var orderedProducts = dbcontext.OrderItem.Where(x => SortList.Contains(x.ProductId)).GroupBy(x => x.ProductId).Count();
															break;
														}
													case "price":
														{
															var priceCHef = list.Where(x => x.CategoryName == item.CategoryName).FirstOrDefault();
															List<SortPrice> sortPrice = new List<SortPrice>();
															foreach (var price in priceCHef.CategoryItems)
															{
																SortPrice sort = new SortPrice();
																sort.Price = Convert.ToDecimal(price.Price.Replace(CurrencySymbol, ""));
																sort.ItemId = price.ItemId;
																sortPrice.Add(sort);
															}

															var priceSort = sortPrice.OrderBy(x => x.Price).ToList();
															foreach (var rating in priceSort)
															{
																SortList.Add(rating.ItemId);
															}
															break;

														}
													case "rating":
														{
															var rateOneCHef = list.Where(x => x.CategoryName == item.CategoryName).FirstOrDefault();
															var ratingSort = rateOneCHef.CategoryItems.OrderByDescending(x => x.Rating).ThenByDescending(x => x.RatingCount).ToList();
															foreach (var rating in ratingSort)
															{
																SortList.Add(rating.ItemId);
															}
															break;
														}


												}

												break;

											}
										case "price":
											{
												var rateOneCHef = list.Where(x => x.CategoryName == item.CategoryName).FirstOrDefault();
												foreach (var cooknRest in rateOneCHef.CategoryItems)
												{
													var price = cooknRest.Price.Replace("" + CurrencySymbol, "");
													price = price.Replace(" ", "").ToString();
													if (Convert.ToDecimal(price) <= Convert.ToDecimal(item.TitleValueName))
													{
														PriceList.Add(cooknRest.ItemId);
													}
												}
												break;

											}

										case "rating":
											{
												switch (item.TitleValueName)
												{

													case "1":
														{
															var rateOneCHef = list.Where(x => x.CategoryName == item.CategoryName).FirstOrDefault();
															if (rateOneCHef != null)
															{
																foreach (var itemOne in rateOneCHef.CategoryItems)
																{
																	if ((int)itemOne.Rating == 1)
																		RatingList.Add(itemOne.ItemId);
																}
															}
															break;
														}
													case "2":
														{
															var rateTwoCHef = list.Where(x => x.CategoryName == item.CategoryName).FirstOrDefault();
															if (rateTwoCHef != null)
															{
																foreach (var itemOne in rateTwoCHef.CategoryItems)
																{
																	if ((int)itemOne.Rating == 2)
																		RatingList.Add(itemOne.ItemId);
																}
															}
															break;
														}
													case "3":
														{
															var rateThreeCHef = list.Where(x => x.CategoryName == item.CategoryName).FirstOrDefault();
															if (rateThreeCHef != null)
															{
																foreach (var itemOne in rateThreeCHef.CategoryItems)
																{
																	if ((int)itemOne.Rating == 3)
																		RatingList.Add(itemOne.ItemId);
																}
															}
															break;
														}
													case "4":
														{
															var rateFourCHef = list.Where(x => x.CategoryName == item.CategoryName).FirstOrDefault();
															if (rateFourCHef != null)
															{
																foreach (var itemOne in rateFourCHef.CategoryItems)
																{
																	if ((int)itemOne.Rating == 4)
																		RatingList.Add(itemOne.ItemId);
																}
															}
															break;
														}
													case "5":
														{
															var rateFiveCHef = list.Where(x => x.CategoryName == item.CategoryName).FirstOrDefault();
															if (rateFiveCHef != null)
															{
																foreach (var itemOne in rateFiveCHef.CategoryItems)
																{
																	if ((int)itemOne.Rating == 5)
																		RatingList.Add(itemOne.ItemId);
																}
															}
															break;
														}
												}
												break;

											}
										case "preferences":
											{
												var preference = list.Where(x => x.CategoryName == item.CategoryName).FirstOrDefault();
												foreach (var cooknRest in preference.CategoryItems)
												{


													var prefExist = (from a in dbcontext.ProductTag.AsEnumerable()
																	 join b in dbcontext.ProductProductTagMapping.AsEnumerable()
																	 on a.Id equals b.ProductTagId
																	 where b.ProductId == cooknRest.ItemId
																	 && a.Name.ToLower().ToString().Equals(item.TitleValueName.ToLower().ToString())
																	 select new
																	 {

																	 }
																	).Any();
													if (prefExist)
													{
														PreferenceList.Add(cooknRest.ItemId);
													}




												}
												break;
											}
									}
								}

							}
						}
						if (RatingList.Any())
						{
							RatingList = RatingList.Distinct().ToList();
							foreach (var item in list)
							{

								foreach (var filter in customer.Filters)
								{
									filter.CategoryName = item.CategoryName;
									if (item.CategoryName == filter.CategoryName)
										item.CategoryItems = item.CategoryItems.Intersect(item.CategoryItems.Where(x => RatingList.Contains(x.ItemId)).ToList()).ToList();

								}
							}
							//list = list.Union(list.Where(x => RatingList.Contains(x.))).ToList();
						}
						else
						{
							if (customer.Filters.Any())
							{
								foreach (var item in customer.Filters)
								{
									if (item.Title.ToLower().ToString() == "rating")
									{
										foreach (var _item in list)
										{
											foreach (var filter in customer.Filters)
											{
												filter.CategoryName = _item.CategoryName;
												if (_item.CategoryName == filter.CategoryName)
													_item.CategoryItems = new List<CooknChefCatProductsV2>();

											}
										}
										customerAPIResponses.Status = true;
										customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
										customerAPIResponses.ResponseObj = list.ToList();
										return customerAPIResponses;
									}
								}

							}

						}
						if (PreferenceList.Any())
						{
							PreferenceList = PreferenceList.Distinct().ToList();
							foreach (var item in list)
							{
								foreach (var filter in customer.Filters)
								{
									filter.CategoryName = item.CategoryName;
									if (item.CategoryName == filter.CategoryName)
										item.CategoryItems = item.CategoryItems.Intersect(item.CategoryItems.Where(x => PreferenceList.Contains(x.ItemId)).ToList()).ToList();

								}
							}
						}
						else
						{
							if (customer.Filters.Any())
							{
								foreach (var item in customer.Filters)
								{
									if (item.Title.ToLower().ToString() == "preferences")
									{
										foreach (var _item in list)
										{
											foreach (var filter in customer.Filters)
											{
												filter.CategoryName = _item.CategoryName;
												if (_item.CategoryName == filter.CategoryName)
													_item.CategoryItems = new List<CooknChefCatProductsV2>();

											}
										}
										customerAPIResponses.Status = true;
										customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
										customerAPIResponses.ResponseObj = list.ToList();
										return customerAPIResponses;
									}
								}

							}

						}
						if (PriceList.Any())
						{
							PriceList = PriceList.Distinct().ToList();
							foreach (var item in list)
							{
								foreach (var filter in customer.Filters)
								{
									filter.CategoryName = item.CategoryName;
									if (item.CategoryName == filter.CategoryName)
										item.CategoryItems = item.CategoryItems.Intersect(item.CategoryItems.Where(x => PriceList.Contains(x.ItemId)).ToList()).ToList();

								}
							}
						}
						else
						{
							if (customer.Filters.Any())
							{
								foreach (var item in customer.Filters)
								{
									if (item.Title.ToLower().ToString() == "price")
									{
										foreach (var _item in list)
										{
											foreach (var filter in customer.Filters)
											{
												filter.CategoryName = _item.CategoryName;
												if (_item.CategoryName == filter.CategoryName)
													_item.CategoryItems = new List<CooknChefCatProductsV2>();

											}
										}
										customerAPIResponses.Status = true;
										customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
										customerAPIResponses.ResponseObj = list.ToList();
										return customerAPIResponses;
									}
								}

							}

						}
						if (SortList.Any())
						{
							SortList = SortList.Distinct().ToList();
							var sortChef = new List<CooknChefCatProductsV2>();
							foreach (var item in list)
							{
								foreach (var filter in customer.Filters)
								{
									filter.CategoryName = item.CategoryName;
									if (item.CategoryName == filter.CategoryName)
										if (filter.TitleValueName.ToLower().ToString() == "rating" || filter.TitleValueName.ToLower().ToString() == "most popular")
										{
											item.CategoryItems = item.CategoryItems.Intersect(item.CategoryItems.Where(x => SortList.Contains(x.ItemId)).ToList()).OrderByDescending(x => x.Rating).ThenByDescending(x => x.RatingCount).ToList();
										}
										else
										{
											if (item.CategoryItems.Any())
											{
												var list_ = new List<CooknChefCatProductsV2>();

												foreach (var ___item in SortList)
												{
													foreach (var __item in item.CategoryItems)
													{
														if (___item == __item.ItemId)
														{
															list_.Add(__item);
															break;
														}
													}
												}
												item.CategoryItems = new List<CooknChefCatProductsV2>();
												item.CategoryItems = list_;
											}
										}

								}
							}

						}
						List<CooknRestProductsV2> listMain = new List<CooknRestProductsV2>();
						foreach (var ___item in list)
						{
							listMain.Add(___item);
						}
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = listMain.Distinct().ToList();
					}
					else if (list.Any())
					{
						List<CooknRestProductsV2> listMain = new List<CooknRestProductsV2>();
						foreach (var item in list)
						{
							if (item.CategoryItems.Count > 0)
							{
								listMain.Add(item);
							}
						}
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = listMain.Distinct().ToList();
					}
					else
					{
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaageChefNotProvidingAnyProductForSellYet", languageId, dbcontext);
						customerAPIResponses.ErrorMessageTitle = "Error!!";
					}

				}
				else
				{
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaageChefNotProvidingAnyProductForSellYet", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
				}

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v1/GetCooknRestProductDetailsByProductId")]
		[HttpPost]
		public CustomerAPIResponses GetCooknRestProductDetailsByProductIdV2([FromBody] CooknRestProductDetailsByID customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.APIKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.CustId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.RestnCookId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.RestaurantAndCookIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.ProductId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(customer.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == customer.APIKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				string CurrencySymbol = Helper.GetStoreCurrency(customer.StoreId, dbcontext);
				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == customer.StoreId).FirstOrDefault().Value;
				var GetRestaurantnCookProductsDetailsById = (from a in dbcontext.Product.AsEnumerable()
																 //join b in dbcontext.FilterCategoryValues
																 //on a.cuisine equals b.Id
															 where a.Id == customer.ProductId
															 && a.Deleted == false && a.Published == true
															 select new ProductDetailResponseModel
															 {
																 Id = a.Id,
																 Name = LanguageHelper.GetLocalizedValueProduct(a.Id, "Name", languageId, a.Name, dbcontext),
																 Description = LanguageHelper.GetLocalizedValueProduct(a.Id, "ShortDescription", languageId, a.ShortDescription ?? "", dbcontext),
																 Quantity = dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.ProductId == a.Id && x.CustomerId == customer.CustId).Any() ? dbcontext.ShoppingCartItem.Where(x => x.ProductId == a.Id && x.CustomerId == customer.CustId).Sum(x => x.Quantity) : 0,
																 Price = dbcontext.Product.AsEnumerable().Where(x => x.Id == a.Id && x.TaxCategoryId == 0).FirstOrDefault() != null
																		? dbcontext.Product.AsEnumerable().Where(x => x.Id == a.Id && x.TaxCategoryId == 0).FirstOrDefault().Price.ToString() : (from priceCategoryTax in dbcontext.Product.AsEnumerable()
																																																 join categoryTax in dbcontext.TaxRate.AsEnumerable()
																																													  on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
																																																 where categoryTax.StoreId == customer.StoreId &&
																																																	 priceCategoryTax.Id == a.Id

																																																 select new
																																																 {
																																																	 PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
																																																 }).FirstOrDefault().PriceCalculated.ToString(),
																 Rating = (!(from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == 3)

																			 select new
																			 {
																				 rating.Rating
																			 }).Any()) ? 0 :
															 (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == 3)

															  select new
															  {
																  rating.Rating
															  }).ToList().Sum(x => x.Rating),
																 RatingCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == 3)

																				select new
																				{
																					rating.Rating
																				}).Count(),
																 ReviewCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item && !string.IsNullOrEmpty(x.ReviewText))

																				select new
																				{
																					rating.ReviewText
																				}).Count(),
																 CustomerImages = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item && !string.IsNullOrEmpty(x.ReviewText))
																				   join customerImage in dbcontext.GenericAttribute.AsEnumerable().Where(x => x.Key == "ProfilePic")
																				   on rating.CustomerId equals customerImage.EntityId
																				   select new CustomerImage
																				   {
																					   Image = customerImage.Value
																				   }).ToList(),

																 Comments = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item)

																			 select new ProductCommentsModels
																			 {
																				 Id = rating.Id,
																				 CustomerName = dbcontext.GenericAttribute.AsEnumerable().Where(c => c.EntityId == rating.CustomerId && c.Key.Contains("FirstName")).FirstOrDefault().Value + " " +
																				 dbcontext.GenericAttribute.AsEnumerable().Where(c => c.EntityId == rating.CustomerId && c.Key.Contains("LastName")).FirstOrDefault().Value,
																				 Comment = rating.ReviewText,
																				 CustomerImage = dbcontext.GenericAttribute.AsEnumerable().Where(c => c.EntityId == rating.CustomerId && c.Key.Contains("ProfilePic")).FirstOrDefault() != null ? dbcontext.GenericAttribute.Where(c => c.EntityId == rating.CustomerId && c.Key.Contains("ProfilePic")).FirstOrDefault().Value : " "
																			 }).OrderByDescending(x => x.Id).Take(10).ToList(),
																 Preferences = (from ProductTag in dbcontext.ProductProductTagMapping.AsEnumerable().Where(x => x.ProductId == a.Id)
																				join ProductTagss in dbcontext.ProductTag.AsEnumerable()
																				on ProductTag.ProductTagId equals ProductTagss.Id
																				join ProductTagImage in dbcontext.ProductTagImageMapping.AsEnumerable()
																				on ProductTagss.Id equals ProductTagImage.TagId
																				join ProductTagImageDetails in dbcontext.Picture.AsEnumerable()
																				on ProductTagImage.PictureId equals ProductTagImageDetails.Id
																				select new PreferencesV2
																				{
																					Name = ProductTagss.Name,
																					Image = ProductTagImageDetails.Id.ToString(),


																				}
																 ).Distinct().ToList(),
																 ImageUrl = (from product in dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == a.Id)
																			 join picture in dbcontext.Picture.AsEnumerable()
																			 on product.PictureId equals picture.Id
																			 select new CooknChefProductsImagesV2
																			 {
																				 ProductImageURL = picture.Id.ToString()
																			 }).ToList(),
																 Specifications = (from Product in dbcontext.Product.AsEnumerable()
																				   join sam in dbcontext.ProductSpecificationAttributeMapping.AsEnumerable()
																				   on Product.Id equals sam.ProductId
																				   join sao in dbcontext.SpecificationAttributeOption.AsEnumerable()
																				   on sam.SpecificationAttributeOptionId equals sao.Id
																				   join sa in dbcontext.SpecificationAttribute.AsEnumerable()
																				   on sao.SpecificationAttributeId equals sa.Id
																				   where Product.Id == a.Id
																				   select new SpecificationModel
																				   {
																					   Name = sa.Name,
																					   Value = sao.Name
																				   }).Distinct().ToList(),

																 Cuisine = (from productcuisine in dbcontext.ProductProductCuisineMapping.AsEnumerable()
																			join product in dbcontext.ProductCuisine.AsEnumerable()
																			on productcuisine.CuisineId equals product.Id
																			where productcuisine.ProductId == a.Id
																			select new
																			{
																				product.Name
																			}).FirstOrDefault() != null ? (from productcuisine in dbcontext.ProductProductCuisineMapping.AsEnumerable()
																										   join product in dbcontext.ProductCuisine.AsEnumerable()
																										   on productcuisine.CuisineId equals product.Id
																										   where productcuisine.ProductId == a.Id
																										   select new
																										   {
																											   product.Name
																										   }).FirstOrDefault().Name : "",
																 DishPrepareTime = a.PrepareTime != 0 ? a.PrepareTime + " min" : "0 min",
																 ProductProductAttributes = (from _a in dbcontext.Product.AsEnumerable().Where(x => x.Id == a.Id)
																							 join b in dbcontext.ProductProductAttributeMapping.AsEnumerable()
																							 on _a.Id equals b.ProductId
																							 join c in dbcontext.ProductAttribute.AsEnumerable()
																							 on b.ProductAttributeId equals c.Id
																							 select new CooknRestProductProductAttributesResponse
																							 {
																								 AttributeName = c.Name,
																								 IsRequired = b.IsRequired,
																								 AttributeTypeId = b.AttributeControlTypeId,
																								 ProductId = _a.Id,
																								 ProductAttributeId = c.Id,
																								 ProductAttributeMappingId = b.Id,
																								 ProductAttribute = (from d in dbcontext.ProductAttributeValue.AsEnumerable()
																													 where d.ProductAttributeMappingId == b.Id
																													 select new ProductAttributes
																													 {
																														 Name = d.Name,
																														 Price = d.PriceAdjustment.ToString(),
																														 ProductAttributeValueId = d.Id,
																														 IsPreSelected = d.IsPreSelected,
																														 Currency = CurrencySymbol
																													 }).ToList()
																							 }).ToList(),
																 IsProductAttributesExist = (from _a in dbcontext.Product.Where(x => x.Id == a.Id)
																							 join b in dbcontext.ProductProductAttributeMapping.AsEnumerable()
																							 on _a.Id equals b.ProductId
																							 join c in dbcontext.ProductAttribute.AsEnumerable()
																							 on b.ProductAttributeId equals c.Id
																							 select new CooknRestProductProductAttributesResponse
																							 {
																								 AttributeName = c.Name,
																								 IsRequired = b.IsRequired,
																								 AttributeTypeId = b.AttributeControlTypeId,
																								 ProductId = _a.Id,
																								 ProductAttributeId = c.Id,
																								 ProductAttributeMappingId = b.Id,
																								 ProductAttribute = (from d in dbcontext.ProductAttributeValue.AsEnumerable()
																													 where d.ProductAttributeMappingId == b.Id
																													 select new ProductAttributes
																													 {
																														 Name = d.Name,
																														 Price = d.PriceAdjustment.ToString(),
																														 ProductAttributeValueId = d.Id,
																														 IsPreSelected = d.IsPreSelected,
																														 Currency = CurrencySymbol
																													 }).ToList()
																							 }).Any()
															 }).AsEnumerable().ToList();
				if (GetRestaurantnCookProductsDetailsById != null)
				{
					foreach (var item in GetRestaurantnCookProductsDetailsById)
					{
						item.Price = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Price));
						//CurrencySymbol + " " + Math.Round(Convert.ToDecimal(item.Price), 2);
						foreach (var pref in item.Preferences)
						{

							int pictureId = Convert.ToInt32(pref.Image);
							string lastPart = Helper.GetFileExtensionFromMimeType(pref.ImageType);
							var prefdetail = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();
							var fileName = !string.IsNullOrEmpty(prefdetail.SeoFilename)
										? $"{pictureId:0000000}_{prefdetail.SeoFilename}_{75}.{lastPart}" + "? " + DateTime.Now
										: $"{pictureId:0000000}_{75}.{lastPart}" + "? " + DateTime.Now;
							pref.Image = ChefBaseUrl + fileName;
						}
						int pictureSize = 75;
						var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
						if (setting != null)
						{
							pictureSize = Convert.ToInt32(setting.Value);
						}
						if (item.ImageUrl.Any())
						{
							foreach (var image in item.ImageUrl)
							{
								if (!string.IsNullOrEmpty(image.ProductImageURL))
								{
									int pictureId = Convert.ToInt32(image.ProductImageURL);
									var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();

									string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
									string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
									? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}" + "? " + DateTime.Now
									: $"{pictureId:0000000}_{pictureSize}.{lastPart}" + "? " + DateTime.Now;
									image.ProductImageURL = ChefBaseUrl + thumbFileName;
								}
								else
								{
									image.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
								}
							}
						}
						else
						{
							CooknChefProductsImagesV2 cooknChefProductsImagesV2 = new CooknChefProductsImagesV2();
							cooknChefProductsImagesV2.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
							item.ImageUrl.Add(cooknChefProductsImagesV2);
						}
						if (item.Rating != 0)
						{
							item.Rating = Convert.ToDouble(item.Rating / item.RatingCount);
							item.Rating = Math.Round(item.Rating, 2);
						}


					}
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = GetRestaurantnCookProductsDetailsById;
				}
				else
				{
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.Datanotavailable", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
				}

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v1/GetCooknRestCrossSellProducts")]
		[HttpPost]
		public CustomerAPIResponses GetCooknRestCrossSellProducts([FromBody] CooknRestCrossSellProducts customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(customer.APIKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (customer.CustId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			try
			{
				if (!string.IsNullOrEmpty(customer.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == customer.APIKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				string CurrencySymbol = Helper.GetStoreCurrency(customer.StoreId, dbcontext);
				var shoppingCartItems = dbcontext.ShoppingCartItem.Where(x => x.CustomerId == customer.CustId && x.StoreId == customer.StoreId).ToList();
				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == customer.StoreId).FirstOrDefault().Value;
				List<ProductDetailResponseModel> listCrossProducts = new List<ProductDetailResponseModel>();
				if (shoppingCartItems.Any())
				{

					foreach (var _item in shoppingCartItems)
					{
						var crossProducts = dbcontext.CrossSellProduct.Where(x => x.ProductId1 == _item.ProductId).ToList();
						if (crossProducts.Any())
						{
							foreach (var __item in crossProducts)
							{

								var GetRestaurantnCookProductsDetailsById = (from a in dbcontext.Product.AsEnumerable()
																				 //join b in dbcontext.FilterCategoryValues.AsEnumerable()
																				 //on a.cuisine equals b.Id
																			 where a.Id == __item.ProductId2
																			 && a.Deleted == false && a.Published == true
																			 select new ProductDetailResponseModel
																			 {
																				 Id = a.Id,
																				 Name = LanguageHelper.GetLocalizedValueProduct(a.Id, "Name", languageId, a.Name, dbcontext),
																				 Description = LanguageHelper.GetLocalizedValueProduct(a.Id, "ShortDescription", languageId, a.ShortDescription ?? "", dbcontext),
																				 Quantity = dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.ProductId == a.Id && x.CustomerId == customer.CustId).Any() ? dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.ProductId == a.Id && x.CustomerId == customer.CustId).Sum(x => x.Quantity) : 0,
																				 Price = dbcontext.Product.AsEnumerable().Where(x => x.Id == a.Id && x.TaxCategoryId == 0).FirstOrDefault() != null
																						? dbcontext.Product.AsEnumerable().Where(x => x.Id == a.Id && x.TaxCategoryId == 0).FirstOrDefault().Price.ToString() : (from priceCategoryTax in dbcontext.Product.AsEnumerable()
																																																				 join categoryTax in dbcontext.TaxRate.AsEnumerable()
																																																   on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
																																																				 where categoryTax.StoreId == customer.StoreId &&
																																																				 priceCategoryTax.Id == a.Id

																																																				 select new
																																																				 {
																																																					 PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
																																																				 }).FirstOrDefault().PriceCalculated.ToString(),
																				 //    Rating = (!(from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == 3)

																				 //                select new
																				 //                {
																				 //                    rating.Rating
																				 //                }).Any()) ? 0 :
																				 //(from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == 3)

																				 // select new RatingModel
																				 // {
																				 //    Rating= rating.Rating
																				 // }).ToList().Sum(x => x.Rating),
																				 //    RatingCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == 3)

																				 //                   select new
																				 //                   {
																				 //                       rating.Rating
																				 //                   }).Count(),
																				 //    ReviewCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item && !string.IsNullOrEmpty(x.ReviewText))

																				 //                   select new
																				 //                   {
																				 //                       rating.ReviewText
																				 //                   }).Count(),
																				 //    CustomerImages = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item && !string.IsNullOrEmpty(x.ReviewText))
																				 //                      join customerImage in dbcontext.GenericAttribute.AsEnumerable().Where(x => x.Key == "ProfilePic")
																				 //                      on rating.CustomerId equals customerImage.EntityId
																				 //                      select new CustomerImage
																				 //                      {
																				 //                          Image = customerImage.Value
																				 //                      }).ToList(),

																				 //    Comments = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item)

																				 //                select new ProductCommentsModels
																				 //                {
																				 //                    Id = rating.Id,
																				 //                    CustomerName = dbcontext.GenericAttribute.AsEnumerable().Where(c => c.EntityId == rating.CustomerId && c.Key.Contains("FirstName")).FirstOrDefault().Value + " " +
																				 //                    dbcontext.GenericAttribute.AsEnumerable().Where(c => c.EntityId == rating.CustomerId && c.Key.Contains("LastName")).FirstOrDefault().Value,
																				 //                    Comment = rating.ReviewText,
																				 //                    CustomerImage = dbcontext.GenericAttribute.AsEnumerable().Where(c => c.EntityId == rating.CustomerId && c.Key.Contains("ProfilePic")).FirstOrDefault() != null ? dbcontext.GenericAttribute.AsEnumerable().Where(c => c.EntityId == rating.CustomerId && c.Key.Contains("ProfilePic")).FirstOrDefault().Value : " "
																				 //                }).OrderByDescending(x => x.Id).Take(10).ToList(),
																				 //    Preferences = (from ProductTag in dbcontext.ProductProductTagMapping.AsEnumerable().Where(x => x.ProductId == a.Id)
																				 //                   join ProductTagss in dbcontext.ProductTag.AsEnumerable()
																				 //                   on ProductTag.ProductTagId equals ProductTagss.Id
																				 //                   join ProductTagImage in dbcontext.ProductTagImageMapping.AsEnumerable()
																				 //                   on ProductTagss.Id equals ProductTagImage.TagId
																				 //                   join ProductTagImageDetails in dbcontext.Picture.AsEnumerable()
																				 //                   on ProductTagImage.PictureId equals ProductTagImageDetails.Id
																				 //                   select new PreferencesV2
																				 //                   {
																				 //                       Name = ProductTagss.Name,
																				 //                       Image = ProductTagImageDetails.Id.ToString(),


																				 //                   }
																				 //    ).Distinct().ToList(),
																				 //    ImageUrl = (from product in dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == a.Id)
																				 //                join picture in dbcontext.Picture.AsEnumerable()
																				 //                on product.PictureId equals picture.Id
																				 //                select new CooknChefProductsImagesV2
																				 //                {
																				 //                    ProductImageURL = picture.Id.ToString()
																				 //                }).ToList(),
																				 //    Specifications = (from Product in dbcontext.Product.AsEnumerable()
																				 //                      join sam in dbcontext.ProductSpecificationAttributeMapping.AsEnumerable()
																				 //                      on Product.Id equals sam.ProductId
																				 //                      join sao in dbcontext.SpecificationAttributeOption.AsEnumerable()
																				 //                      on sam.SpecificationAttributeOptionId equals sao.Id
																				 //                      join sa in dbcontext.SpecificationAttribute.AsEnumerable()
																				 //                      on sao.SpecificationAttributeId equals sa.Id
																				 //                      where Product.Id == a.Id
																				 //                      select new SpecificationModel
																				 //                      {
																				 //                          Name = sa.Name,
																				 //                          Value = sao.Name
																				 //                      }).Distinct().ToList(),

																				 //    Cuisine = (from productcuisine in dbcontext.ProductProductCuisineMapping.AsEnumerable()
																				 //               join product in dbcontext.ProductCuisine.AsEnumerable()
																				 //               on productcuisine.CuisineId equals product.Id
																				 //               where productcuisine.ProductId == a.Id
																				 //               select new
																				 //               {
																				 //                   product.Name
																				 //               }).FirstOrDefault() != null ? (from productcuisine in dbcontext.ProductProductCuisineMapping.AsEnumerable()
																				 //                                              join product in dbcontext.ProductCuisine.AsEnumerable()
																				 //                                              on productcuisine.CuisineId equals product.Id
																				 //                                              where productcuisine.ProductId == a.Id
																				 //                                              select new
																				 //                                              {
																				 //                                                  product.Name
																				 //                                              }).FirstOrDefault().Name : "",
																				 DishPrepareTime = a.PrepareTime != 0 ? a.PrepareTime + " min" : "0 min",
																				 //    ProductProductAttributes = (from _a in dbcontext.Product.Where(x => x.Id == a.Id)
																				 //                                join b in dbcontext.ProductProductAttributeMapping.AsEnumerable()
																				 //                                on _a.Id equals b.ProductId
																				 //                                join c in dbcontext.ProductAttribute.AsEnumerable()
																				 //                                on b.ProductAttributeId equals c.Id
																				 //                                select new CooknRestProductProductAttributesResponse
																				 //                                {
																				 //                                    AttributeName = c.Name,
																				 //                                    IsRequired = b.IsRequired,
																				 //                                    AttributeTypeId = b.AttributeControlTypeId,
																				 //                                    ProductId = _a.Id,
																				 //                                    ProductAttributeId = c.Id,
																				 //                                    ProductAttributeMappingId = b.Id,
																				 //                                    ProductAttribute = (from d in dbcontext.ProductAttributeValue.AsEnumerable()
																				 //                                                        where d.ProductAttributeMappingId == b.Id
																				 //                                                        select new ProductAttributes
																				 //                                                        {
																				 //                                                            Name = d.Name,
																				 //                                                            Price = d.PriceAdjustment,
																				 //                                                            ProductAttributeValueId = d.Id,
																				 //                                                            IsPreSelected = d.IsPreSelected,
																				 //                                                            Currency = CurrencySymbol
																				 //                                                        }).ToList()
																				 //                                }).ToList(),
																				 //    IsProductAttributesExist = (from _a in dbcontext.Product.Where(x => x.Id == a.Id)
																				 //                                join b in dbcontext.ProductProductAttributeMapping.AsEnumerable()
																				 //                                on _a.Id equals b.ProductId
																				 //                                join c in dbcontext.ProductAttribute
																				 //                                on b.ProductAttributeId equals c.Id
																				 //                                select new CooknRestProductProductAttributesResponse
																				 //                                {
																				 //                                    AttributeName = c.Name,
																				 //                                    IsRequired = b.IsRequired,
																				 //                                    AttributeTypeId = b.AttributeControlTypeId,
																				 //                                    ProductId = _a.Id,
																				 //                                    ProductAttributeId = c.Id,
																				 //                                    ProductAttributeMappingId = b.Id,
																				 //                                    ProductAttribute = (from d in dbcontext.ProductAttributeValue.AsEnumerable()
																				 //                                                        where d.ProductAttributeMappingId == b.Id
																				 //                                                        select new ProductAttributes
																				 //                                                        {
																				 //                                                            Name = d.Name,
																				 //                                                            Price = d.PriceAdjustment,
																				 //                                                            ProductAttributeValueId = d.Id,
																				 //                                                            IsPreSelected = d.IsPreSelected,
																				 //                                                            Currency = CurrencySymbol
																				 //                                                        }).ToList()
																				 //                                }).Any()
																			 }).FirstOrDefault();
								if (!dbcontext.ShoppingCartItem.Where(x => x.ProductId == __item.ProductId2).Any())
									listCrossProducts.Add(GetRestaurantnCookProductsDetailsById);
							}
							if (listCrossProducts.Any())
							{
								foreach (var item in listCrossProducts)
								{
									item.Price = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Price));
									//CurrencySymbol + " " + Math.Round(Convert.ToDecimal(item.Price), 2);
									foreach (var pref in item.Preferences)
									{

										int pictureId = Convert.ToInt32(pref.Image);
										var picture = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();
										string lastPart = Helper.GetFileExtensionFromMimeType(picture.MimeType);
										var fileName = $"{picture.Id:0000000}_{75}.{lastPart}" + "? " + DateTime.Now;
										pref.Image = ChefBaseUrl + fileName;
									}
									int pictureSize = 75;
									var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
									if (setting != null)
									{
										pictureSize = Convert.ToInt32(setting.Value);
									}
									if (item.ImageUrl.Any())
									{
										foreach (var image in item.ImageUrl)
										{
											if (!string.IsNullOrEmpty(image.ProductImageURL))
											{
												int pictureId = Convert.ToInt32(image.ProductImageURL);
												var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();

												string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
												string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
												? $"{pictureId:0000000}_{pictureImage.SeoFilename}_{pictureSize}.{lastPart}" + "? " + DateTime.Now
												: $"{pictureId:0000000}_{pictureSize}.{lastPart}" + "? " + DateTime.Now;
												image.ProductImageURL = ChefBaseUrl + thumbFileName;
											}
											else
											{
												image.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
											}
										}
									}
									else
									{
										CooknChefProductsImagesV2 cooknChefProductsImagesV2 = new CooknChefProductsImagesV2();
										cooknChefProductsImagesV2.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
										item.ImageUrl.Add(cooknChefProductsImagesV2);
									}
									if (item.Rating != 0)
									{
										item.Rating = Convert.ToDouble(item.Rating / item.RatingCount);
										item.Rating = Math.Round(item.Rating, 2);
									}


								}
								customerAPIResponses.Status = true;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
								customerAPIResponses.ResponseObj = listCrossProducts;
							}
							else
							{
								customerAPIResponses.Status = false;
								customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
								customerAPIResponses.ResponseObj = null;
								customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.Datanotavailable", languageId, dbcontext);
								customerAPIResponses.ErrorMessageTitle = "Error!!";
							}
						}



					}


				}
				else
				{
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CartEmpty", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
				}

				if (!listCrossProducts.Any())
				{
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.NoCrossProductFound", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
				}



				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v1/Markaslike")]
		[HttpPost]
		public CustomerAPIResponses Markaslike([FromBody] MarkaslikeModel markaslikeModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(markaslikeModel.StoreId, markaslikeModel.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (markaslikeModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(markaslikeModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(markaslikeModel.APIKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(markaslikeModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			if (markaslikeModel.RestnCookId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(markaslikeModel.StoreId, "API.ErrorMesaage.RestaurantAndCookIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (markaslikeModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(markaslikeModel.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(markaslikeModel.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == markaslikeModel.APIKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(markaslikeModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				var existRatenReview = dbcontext.RatingReviews.Where(x => x.CustomerId == markaslikeModel.CustomerId &&
				 x.EntityId == markaslikeModel.RestnCookId && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == markaslikeModel.StoreId).FirstOrDefault();
				if (existRatenReview != null)
				{
					existRatenReview.IsLiked = markaslikeModel.IsLike;
					dbcontext.Entry(existRatenReview).State = EntityState.Modified;
					dbcontext.SaveChanges();
				}
				else
				{
					RatingReviews restaurantNCooksProductsRating = new RatingReviews();
					restaurantNCooksProductsRating.EntityId = markaslikeModel.RestnCookId;
					restaurantNCooksProductsRating.Rating = 0;
					restaurantNCooksProductsRating.CustomerId = markaslikeModel.CustomerId;
					restaurantNCooksProductsRating.OrderId = 0;
					restaurantNCooksProductsRating.ReviewType = (int)NHKCustomerApplication.Utilities.ReviewType.Merchant;
					restaurantNCooksProductsRating.StoreId = markaslikeModel.StoreId;
					restaurantNCooksProductsRating.IsApproved = true;

					restaurantNCooksProductsRating.IsLiked = markaslikeModel.IsLike;
					restaurantNCooksProductsRating.IsNotInterested = false;
					restaurantNCooksProductsRating.IsSkipped = false;
					restaurantNCooksProductsRating.CreatedOnUtc = DateTime.UtcNow;
					dbcontext.RatingReviews.Add(restaurantNCooksProductsRating);
					dbcontext.SaveChanges();
				}


				customerAPIResponses.Status = true;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
				customerAPIResponses.ResponseObj = null;
				if (markaslikeModel.IsLike)
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(markaslikeModel.StoreId, "API.ErrorMesaage.MarkedLikedSuccessfully", languageId, dbcontext);
				else
				{
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(markaslikeModel.StoreId, "API.ErrorMesaage.MarkedUnlikedSuccessfully", languageId, dbcontext);
				}
				customerAPIResponses.ErrorMessageTitle = "Success!!";



				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v1/GetAvailableFromTimings")]
		[HttpPost]
		public virtual CustomerAPIResponses GetAvailableTimings(AvailableFromTimingModel availableFromTiming)
		{
			int languageId = LanguageHelper.GetIdByLangCode(availableFromTiming.StoreId, availableFromTiming.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (availableFromTiming == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(availableFromTiming.APIKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			if (availableFromTiming.RestnCookId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.RestaurantAndCookIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (availableFromTiming.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (availableFromTiming.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(availableFromTiming.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == availableFromTiming.APIKey && x.StoreId == availableFromTiming.StoreId).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				List<string> AvailableTimings = new List<string>();
				var timingsList = Helper.GetAllAvailableTiming();

				var booked = dbcontext.BookingTableMaster.Where(x => x.StoreId == availableFromTiming.StoreId && x.VendorId == availableFromTiming.RestnCookId).ToList();
				if (booked.Any())
				{
					DateTime dateTime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext);
					TimeSpan fromTime = dateTime.TimeOfDay;
					TimeSpan toTime = dateTime.TimeOfDay;
					foreach (var item in booked)
					{
						fromTime = item.TableAvailableFrom.Value;
						toTime = item.TableAvailableTo.Value;
					}
					foreach (var s in timingsList)
					{
						DateTime dateTimetoConvert = DateTime.ParseExact(s,
											"h:mm tt", CultureInfo.InvariantCulture);
						TimeSpan span = dateTimetoConvert.TimeOfDay;
						if (span >= fromTime && span <= toTime)
							AvailableTimings.Add(s);
					}
					if (AvailableTimings.Any())
					{
						customerAPIResponses.ErrorMessageTitle = "Success!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.Available", languageId, dbcontext);
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = AvailableTimings;
						return customerAPIResponses;
					}
					else
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.SorryNoRestaurantisnotAvailableAtThismoment", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.SorryNoTimingsAvailableforthisRestaurant", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}

			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.ServerError", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

		[Route("~/api/v2.1/GetAvailableFromTimings")]
		[HttpPost]
		public virtual CustomerAPIResponses GetAvailableTimingsV2_1(AvailableFromTimingModel availableFromTiming)
		{
			int languageId = LanguageHelper.GetIdByLangCode(availableFromTiming.StoreId, availableFromTiming.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (availableFromTiming == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(availableFromTiming.APIKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			if (availableFromTiming.RestnCookId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.RestaurantAndCookIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (availableFromTiming.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (availableFromTiming.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(availableFromTiming.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == availableFromTiming.APIKey && x.StoreId == availableFromTiming.StoreId).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				List<string> AvailableTimings = new List<string>();
				var timingsList = Helper.GetAllAvailableTiming();

				var booked = dbcontext.BookingTableMaster.Where(x => x.StoreId == availableFromTiming.StoreId && x.VendorId == availableFromTiming.RestnCookId).ToList();
				if (booked.Any())
				{
					DateTime dateTime = Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, availableFromTiming.CustomerId);
					TimeSpan fromTime = dateTime.TimeOfDay;
					TimeSpan toTime = dateTime.TimeOfDay;
					foreach (var item in booked)
					{
						fromTime = item.TableAvailableFrom.Value;
						toTime = item.TableAvailableTo.Value;
					}
					foreach (var s in timingsList)
					{
						DateTime dateTimetoConvert = DateTime.ParseExact(s,
											"h:mm tt", CultureInfo.InvariantCulture);
						TimeSpan span = dateTimetoConvert.TimeOfDay;
						if (span >= fromTime && span <= toTime)
							AvailableTimings.Add(s);
					}
					if (AvailableTimings.Any())
					{
						customerAPIResponses.ErrorMessageTitle = "Success!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.Available", languageId, dbcontext);
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = AvailableTimings;
						return customerAPIResponses;
					}
					else
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.SorryNoRestaurantisnotAvailableAtThismoment", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.SorryNoTimingsAvailableforthisRestaurant", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}

			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.ServerError", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

		[Route("~/api/v1/GetAvailableToTimings")]
		[HttpPost]
		public virtual CustomerAPIResponses GetAvailableToTimings(AvailableToTimingModel availableFromTiming)
		{
			int languageId = LanguageHelper.GetIdByLangCode(availableFromTiming.StoreId, availableFromTiming.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (availableFromTiming == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(availableFromTiming.APIKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			if (availableFromTiming.RestnCookId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.RestaurantAndCookIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (availableFromTiming.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (availableFromTiming.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(availableFromTiming.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == availableFromTiming.APIKey && x.StoreId == availableFromTiming.StoreId).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				List<string> AvailableTimings = new List<string>();
				var timingsList = Helper.GetAllAvailableTiming();

				var booked = dbcontext.BookingTableMaster.Where(x => x.StoreId == availableFromTiming.StoreId && x.VendorId == availableFromTiming.RestnCookId).ToList();
				if (booked.Any())
				{
					DateTime dateTime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext);
					TimeSpan fromTime = dateTime.TimeOfDay;
					TimeSpan toTime = dateTime.TimeOfDay;
					foreach (var item in booked)
					{
						fromTime = item.TableAvailableFrom.Value;
						toTime = item.TableAvailableTo.Value;
					}
					foreach (var s in timingsList)
					{
						DateTime dateTimetoConvert = DateTime.ParseExact(s,
								 "h:mm tt", CultureInfo.InvariantCulture);
						DateTime ConvertFromTiming = DateTime.ParseExact(availableFromTiming.Fromtime,
											"h:mm tt", CultureInfo.InvariantCulture);
						TimeSpan span = dateTimetoConvert.TimeOfDay;
						TimeSpan spanFrom = ConvertFromTiming.TimeOfDay;
						if (span > spanFrom && span <= toTime)
							AvailableTimings.Add(s);
					}
					if (AvailableTimings.Any())
					{
						customerAPIResponses.ErrorMessageTitle = "Success!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.Available", languageId, dbcontext);
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = AvailableTimings;
						return customerAPIResponses;
					}
					else
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.SorryNoRestaurantisnotAvailableAtThismoment", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.SorryNoTimingsAvailableforthisRestaurant", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}

			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.ServerError", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}


		}

		[Route("~/api/v2.1/GetAvailableToTimings")]
		[HttpPost]
		public virtual CustomerAPIResponses GetAvailableToTimingsV2_1(AvailableToTimingModel availableFromTiming)
		{
			int languageId = LanguageHelper.GetIdByLangCode(availableFromTiming.StoreId, availableFromTiming.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (availableFromTiming == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(availableFromTiming.APIKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			if (availableFromTiming.RestnCookId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.RestaurantAndCookIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (availableFromTiming.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (availableFromTiming.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(availableFromTiming.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == availableFromTiming.APIKey && x.StoreId == availableFromTiming.StoreId).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				List<string> AvailableTimings = new List<string>();
				var timingsList = Helper.GetAllAvailableTiming();

				var booked = dbcontext.BookingTableMaster.Where(x => x.StoreId == availableFromTiming.StoreId && x.VendorId == availableFromTiming.RestnCookId).ToList();
				if (booked.Any())
				{
					DateTime dateTime = Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, availableFromTiming.RestnCookId);
					TimeSpan fromTime = dateTime.TimeOfDay;
					TimeSpan toTime = dateTime.TimeOfDay;
					foreach (var item in booked)
					{
						fromTime = item.TableAvailableFrom.Value;
						toTime = item.TableAvailableTo.Value;
					}
					foreach (var s in timingsList)
					{
						DateTime dateTimetoConvert = DateTime.ParseExact(s,
								 "h:mm tt", CultureInfo.InvariantCulture);
						DateTime ConvertFromTiming = DateTime.ParseExact(availableFromTiming.Fromtime,
											"h:mm tt", CultureInfo.InvariantCulture);
						TimeSpan span = dateTimetoConvert.TimeOfDay;
						TimeSpan spanFrom = ConvertFromTiming.TimeOfDay;
						if (span > spanFrom && span <= toTime)
							AvailableTimings.Add(s);
					}
					if (AvailableTimings.Any())
					{
						customerAPIResponses.ErrorMessageTitle = "Success!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.Available", languageId, dbcontext);
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = AvailableTimings;
						return customerAPIResponses;
					}
					else
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.SorryNoRestaurantisnotAvailableAtThismoment", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.SorryNoTimingsAvailableforthisRestaurant", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}

			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.ServerError", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}


		}

		[Route("~/api/v1/GetAvailableTables")]
		[HttpPost]
		public virtual CustomerAPIResponses GetAvailableTables(AvailableTablesModel availableFromTiming)
		{
			int languageId = LanguageHelper.GetIdByLangCode(availableFromTiming.StoreId, availableFromTiming.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (availableFromTiming == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(availableFromTiming.APIKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			if (availableFromTiming.RestnCookId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.RestaurantAndCookIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (availableFromTiming.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (availableFromTiming.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(availableFromTiming.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == availableFromTiming.APIKey && x.StoreId == availableFromTiming.StoreId).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				List<string> AvailableTableforBookings = new List<string>();
				var timingsList = Helper.GetAllAvailableTiming();

				var TableAvailableforbooking = dbcontext.BookingTableMaster.Where(x => x.StoreId == availableFromTiming.StoreId && x.VendorId == availableFromTiming.RestnCookId).ToList();
				if (TableAvailableforbooking.Any())
				{
					DateTime dateTimeDate = DateTime.Parse(availableFromTiming.Date);
					DateTime dateTimetoConvert = DateTime.ParseExact(availableFromTiming.Fromtime,
											"h:mm tt", CultureInfo.InvariantCulture);
					TimeSpan spanFrom = dateTimetoConvert.TimeOfDay;
					DateTime dateTimetoConvertTo = DateTime.ParseExact(availableFromTiming.ToTime,
											"h:mm tt", CultureInfo.InvariantCulture);
					TimeSpan spanTo = dateTimetoConvertTo.TimeOfDay;
					//spanFrom.Subtract(new TimeSpan(0, 30, 0));
					//spanFrom=spanFrom.Add(new TimeSpan(0, 30, 0));
					//spanTo = spanTo.Subtract(new TimeSpan(0, 30, 0));

					DateTime dateTime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext);
					TimeSpan fromTime = dateTime.TimeOfDay;
					TimeSpan toTime = dateTime.TimeOfDay;
					var BookedTables = dbcontext.BookingTableTransaction.Where(x => x.StoreId == availableFromTiming.StoreId && x.VendorId == availableFromTiming.RestnCookId && x.BookingDate == dateTimeDate
					&& ((x.BookingTimeTo >= spanFrom && x.BookingTimeFrom <= spanFrom)) && x.Status == (int)BookingTableStatus.Booked).ToList();
					var BookedTablesto = dbcontext.BookingTableTransaction.Where(x => x.StoreId == availableFromTiming.StoreId && x.VendorId == availableFromTiming.RestnCookId && x.BookingDate == dateTimeDate
					&& ((x.BookingTimeTo >= spanTo && x.BookingTimeFrom <= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
					var BookedTables3 = dbcontext.BookingTableTransaction.Where(x => x.StoreId == availableFromTiming.StoreId && x.VendorId == availableFromTiming.RestnCookId && x.BookingDate == dateTimeDate
					&& ((x.BookingTimeFrom >= spanFrom && x.BookingTimeFrom <= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
					//BookedTables = _bookingTransactionrepository.Table.Where(x => x.StoreId == storeId && x.VendorId == vendorId && x.BookingDate == dateTimeDate
					//&& ((x.BookingTimeTo >= spanTo && x.BookingTimeFrom <= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
					var BookedTablesto3 = dbcontext.BookingTableTransaction.Where(x => x.StoreId == availableFromTiming.StoreId && x.VendorId == availableFromTiming.RestnCookId && x.BookingDate == dateTimeDate
					&& ((x.BookingTimeTo <= spanFrom && x.BookingTimeTo >= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
					var lstBooksTable = BookedTables.Select(x => x.TableNumber).Union(BookedTablesto.Select(x => x.TableNumber))
						.Union(BookedTables3.Select(x => x.TableNumber)).Union(BookedTablesto3.Select(x => x.TableNumber));
					//List<int> lstBooksTables = new List<int>();
					//if (lstBooksTable.Any())
					//{
					//     lstBooksTables = BookedTables.Select(x => x.TableNumber).Except(lstBooksTable.Select(x=>x.TableNumber)).ToList();
					//}
					//var lstTables = BookedTables.Where(x => x.Id == id).FirstOrDefault();
					//foreach (var item in TableAvailableforbooking)
					//{
					//    fromTime = item.TableAvailableFrom;
					//    toTime = item.TableAvailableTo;
					//}

					//select new
					//{
					//    Tablenumber = a.TableNo,
					//    IsAvailable = !BookedTables.Where(x => x.TableNumber == a.TableNo && x.StoreId == storeId && (x.BookingTimeTo <= spanFrom && x.BookingTimeTo >= spanTo)).Any()

					//}
					var TableAvailableAccordingToSelectedTiming = TableAvailableforbooking.Where(x => !lstBooksTable.Contains(x.TableNo.Value)).ToList();

					foreach (var item in TableAvailableAccordingToSelectedTiming)
					{
						//if (item.IsAvailable)
						AvailableTableforBookings.Add(item.TableNo.Value.ToString());

					}
					//if (lstTables != null)
					//    AvailableTableforBookings.Add(new SelectListItem { Text = lstTables.TableNumber.ToString(), Value = lstTables.TableNumber.ToString() });


					if (AvailableTableforBookings.Any())
					{
						customerAPIResponses.ErrorMessageTitle = "Success!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.Available", languageId, dbcontext);
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = AvailableTableforBookings;
						return customerAPIResponses;
					}
					else
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.SorryNoRestaurantisnotAvailableAtThismoment", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.SorryNoTableAvailableforthisRestaurant", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.ServerError", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

		[Route("~/api/v2.1/GetAvailableTables")]
		[HttpPost]
		public virtual CustomerAPIResponses GetAvailableTablesV2_1(AvailableTablesModel availableFromTiming)
		{
			int languageId = LanguageHelper.GetIdByLangCode(availableFromTiming.StoreId, availableFromTiming.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (availableFromTiming == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(availableFromTiming.APIKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			if (availableFromTiming.RestnCookId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.RestaurantAndCookIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (availableFromTiming.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (availableFromTiming.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(availableFromTiming.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == availableFromTiming.APIKey && x.StoreId == availableFromTiming.StoreId).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				List<string> AvailableTableforBookings = new List<string>();
				var timingsList = Helper.GetAllAvailableTiming();

				var TableAvailableforbooking = dbcontext.BookingTableMaster.Where(x => x.StoreId == availableFromTiming.StoreId && x.VendorId == availableFromTiming.RestnCookId).ToList();
				if (TableAvailableforbooking.Any())
				{
					DateTime dateTimeDate = DateTime.Parse(availableFromTiming.Date);
					DateTime dateTimetoConvert = DateTime.ParseExact(availableFromTiming.Fromtime,
											"h:mm tt", CultureInfo.InvariantCulture);
					TimeSpan spanFrom = dateTimetoConvert.TimeOfDay;
					DateTime dateTimetoConvertTo = DateTime.ParseExact(availableFromTiming.ToTime,
											"h:mm tt", CultureInfo.InvariantCulture);
					TimeSpan spanTo = dateTimetoConvertTo.TimeOfDay;
					//spanFrom.Subtract(new TimeSpan(0, 30, 0));
					//spanFrom=spanFrom.Add(new TimeSpan(0, 30, 0));
					//spanTo = spanTo.Subtract(new TimeSpan(0, 30, 0));

					DateTime dateTime = Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, availableFromTiming.CustomerId);
					TimeSpan fromTime = dateTime.TimeOfDay;
					TimeSpan toTime = dateTime.TimeOfDay;
					var BookedTables = dbcontext.BookingTableTransaction.Where(x => x.StoreId == availableFromTiming.StoreId && x.VendorId == availableFromTiming.RestnCookId && x.BookingDate == dateTimeDate
					&& ((x.BookingTimeTo >= spanFrom && x.BookingTimeFrom <= spanFrom)) && x.Status == (int)BookingTableStatus.Booked).ToList();
					var BookedTablesto = dbcontext.BookingTableTransaction.Where(x => x.StoreId == availableFromTiming.StoreId && x.VendorId == availableFromTiming.RestnCookId && x.BookingDate == dateTimeDate
					&& ((x.BookingTimeTo >= spanTo && x.BookingTimeFrom <= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
					var BookedTables3 = dbcontext.BookingTableTransaction.Where(x => x.StoreId == availableFromTiming.StoreId && x.VendorId == availableFromTiming.RestnCookId && x.BookingDate == dateTimeDate
					&& ((x.BookingTimeFrom >= spanFrom && x.BookingTimeFrom <= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
					//BookedTables = _bookingTransactionrepository.Table.Where(x => x.StoreId == storeId && x.VendorId == vendorId && x.BookingDate == dateTimeDate
					//&& ((x.BookingTimeTo >= spanTo && x.BookingTimeFrom <= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
					var BookedTablesto3 = dbcontext.BookingTableTransaction.Where(x => x.StoreId == availableFromTiming.StoreId && x.VendorId == availableFromTiming.RestnCookId && x.BookingDate == dateTimeDate
					&& ((x.BookingTimeTo <= spanFrom && x.BookingTimeTo >= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
					var lstBooksTable = BookedTables.Select(x => x.TableNumber).Union(BookedTablesto.Select(x => x.TableNumber))
						.Union(BookedTables3.Select(x => x.TableNumber)).Union(BookedTablesto3.Select(x => x.TableNumber));
					//List<int> lstBooksTables = new List<int>();
					//if (lstBooksTable.Any())
					//{
					//     lstBooksTables = BookedTables.Select(x => x.TableNumber).Except(lstBooksTable.Select(x=>x.TableNumber)).ToList();
					//}
					//var lstTables = BookedTables.Where(x => x.Id == id).FirstOrDefault();
					//foreach (var item in TableAvailableforbooking)
					//{
					//    fromTime = item.TableAvailableFrom;
					//    toTime = item.TableAvailableTo;
					//}

					//select new
					//{
					//    Tablenumber = a.TableNo,
					//    IsAvailable = !BookedTables.Where(x => x.TableNumber == a.TableNo && x.StoreId == storeId && (x.BookingTimeTo <= spanFrom && x.BookingTimeTo >= spanTo)).Any()

					//}
					var TableAvailableAccordingToSelectedTiming = TableAvailableforbooking.Where(x => !lstBooksTable.Contains(x.TableNo.Value)).ToList();

					foreach (var item in TableAvailableAccordingToSelectedTiming)
					{
						//if (item.IsAvailable)
						AvailableTableforBookings.Add(item.TableNo.Value.ToString());

					}
					//if (lstTables != null)
					//    AvailableTableforBookings.Add(new SelectListItem { Text = lstTables.TableNumber.ToString(), Value = lstTables.TableNumber.ToString() });


					if (AvailableTableforBookings.Any())
					{
						customerAPIResponses.ErrorMessageTitle = "Success!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.Available", languageId, dbcontext);
						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = AvailableTableforBookings;
						return customerAPIResponses;
					}
					else
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.SorryNoRestaurantisnotAvailableAtThismoment", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.SorryNoTableAvailableforthisRestaurant", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.ServerError", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

		[Route("~/api/v1/SubmitTableDetails")]
		[HttpPost]
		public virtual CustomerAPIResponses SubmitTableDetails(BookingTableTransactionListModel availableFromTiming)
		{
			int languageId = LanguageHelper.GetIdByLangCode(availableFromTiming.StoreId, availableFromTiming.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (availableFromTiming == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(availableFromTiming.APIKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			if (availableFromTiming.RestnCookId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.RestaurantAndCookIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (availableFromTiming.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(availableFromTiming.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == availableFromTiming.APIKey && x.StoreId == availableFromTiming.StoreId).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				BookingTableTransaction bookingTableTransaction = new BookingTableTransaction();
				bookingTableTransaction.StoreId = availableFromTiming.StoreId; //_workContext.GetCurrentStoreId();  //_workContext.CurrentCustomer.RegisteredInStoreId;
				bookingTableTransaction.VendorId = availableFromTiming.RestnCookId; //_workContext.CurrentCustomer.VendorId;
				DateTime dateTimetoConvert = DateTime.ParseExact(availableFromTiming.BookingTimeFrom,
										"h:mm tt", CultureInfo.InvariantCulture);
				TimeSpan spanFrom = dateTimetoConvert.TimeOfDay;
				DateTime dateTimetoConvertTo = DateTime.ParseExact(availableFromTiming.BookingTimeTo,
										"h:mm tt", CultureInfo.InvariantCulture);
				TimeSpan spanTo = dateTimetoConvertTo.TimeOfDay;
				bookingTableTransaction.BookingTimeFrom = spanFrom;
				bookingTableTransaction.BookingTimeTo = spanTo;
				bookingTableTransaction.CreatedDt = DateTime.UtcNow;
				bookingTableTransaction.BookingDate = DateTime.Parse(availableFromTiming.BookingDate);
				bookingTableTransaction.CustomerName = availableFromTiming.CustomerName;
				bookingTableTransaction.Email = availableFromTiming.CustomerEmail;
				bookingTableTransaction.MobileNumber = availableFromTiming.MobileNumber;
				bookingTableTransaction.TableNumber = availableFromTiming.TableNumber;
				bookingTableTransaction.Status = (int)BookingTableStatus.Booked;
				bookingTableTransaction.SeatingCapacity = availableFromTiming.Seats.ToString();
				bookingTableTransaction.SpecialNotes = availableFromTiming.SpecialNotes;
				dbcontext.BookingTableTransaction.Add(bookingTableTransaction);

				dbcontext.SaveChanges();
				var vendorInfo = (from a in dbcontext.Vendor
								  join b in dbcontext.Address
								  on a.AddressId equals b.Id
								  where a.Id == availableFromTiming.RestnCookId
								  select new
								  {
									  Name = a.Name,
									  Address = b.Address1 + " " + b.Address2 + " " + b.City + " " + b.ZipPostalCode
								  }).FirstOrDefault();
				customerAPIResponses.ErrorMessageTitle = "Suceess!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.RegisteredSuccessfully", languageId, dbcontext);
				customerAPIResponses.Status = true;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
				customerAPIResponses.ResponseObj = vendorInfo;
				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(availableFromTiming.StoreId, "API.ErrorMesaage.ServerError", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;

			}
		}


		[Route("~/api/v1/GetMerchantCategorys")]
		[HttpPost]
		public virtual CustomerAPIResponses GetMerchantCategorys(MerchantCategorysModel merchantCategorysModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(merchantCategorysModel.StoreId, merchantCategorysModel.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (merchantCategorysModel == null)
			{
				//KP Comments//Please use a function for response
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (string.IsNullOrEmpty(merchantCategorysModel.ApiKey) || string.IsNullOrWhiteSpace(merchantCategorysModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (merchantCategorysModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (merchantCategorysModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			if (merchantCategorysModel.StoreId != 0)
			{
				var storeExist = dbcontext.Store.Any(x => x.Id == merchantCategorysModel.StoreId);
				if (!storeExist)
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}
			if (merchantCategorysModel.CustomerId != 0)
			{
				var customerExist = dbcontext.Customer.Any(x => x.Id == merchantCategorysModel.CustomerId && x.Active && !x.Deleted);
				if (!customerExist)
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}
			try
			{
				if (!string.IsNullOrEmpty(merchantCategorysModel.ApiKey) || !string.IsNullOrWhiteSpace(merchantCategorysModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == merchantCategorysModel.ApiKey && x.StoreId == merchantCategorysModel.StoreId).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				List<int> CooknRestlist = new List<int>();
				using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
				{
					string result = db.Query<string>("NB_GetMerchantCategorys", new
					{
						merchantCategorysModel.StoreId,
						merchantCategorysModel.RestType,
						latLongPos = merchantCategorysModel.LatPos + " " + merchantCategorysModel.LongPos
					}, commandType: CommandType.StoredProcedure).FirstOrDefault();
					try
					{
						if (result != null)
						{
							foreach (string item in result.Split(','))
							{
								int addressid = Convert.ToInt32(item.Trim());
								CooknRestlist.Add(addressid);
							}
						}
					}
					catch (Exception) { }

				}

				if (CooknRestlist.Any())
				{
					var merchantCategorys = dbcontext.MerchantCategory.Where(x => CooknRestlist.Contains(x.Id)).OrderBy(x => x.Id).Distinct().ToList();

					// store Url
					string storeBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == merchantCategorysModel.StoreId).FirstOrDefault().Value;

					var store = dbcontext.Store.Where(x => x.Id == merchantCategorysModel.StoreId).FirstOrDefault();
					//string storeBaseUrl = store?.Url + "images/thumbs/";

					List<MerchantCategoryModel> merchantCategoryList = new List<MerchantCategoryModel>();

					foreach (var merchantCategory in merchantCategorys)
					{
						var merchantCategoryModel = new MerchantCategoryModel
						{
							Id = merchantCategory.Id,
							Name = merchantCategory.Name,
							ShortDescription = merchantCategory.ShortDescription,
							PictureId = merchantCategory.PictureId,
							Published = merchantCategory.Published,
							DisplayOrder = merchantCategory.DisplayOrder,
							StoreId = merchantCategory.StoreId,
							Deleted = merchantCategory.Deleted,
							StoreName = store?.Name
						};


						int pictureSize = 100;
						var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.vendorthumbpicturesize") && x.StoreId == merchantCategory.StoreId).FirstOrDefault();
						if (setting != null)
						{
							pictureSize = Convert.ToInt32(setting.Value);
						}

						if (merchantCategory.PictureId != 0)
						{

							var picture = dbcontext.Picture.Where(x => x.Id == merchantCategory.PictureId).FirstOrDefault();
							string lastPart = Helper.GetFileExtensionFromMimeType(picture.MimeType);
							string thumbFileName = !string.IsNullOrEmpty(picture.SeoFilename)
							? $"{picture.Id:0000000}_{picture.SeoFilename}_{pictureSize}.{lastPart}" + "? " + DateTime.Now
							: $"{picture.Id:0000000}_{pictureSize}.{lastPart}" + "?" + DateTime.Now;


							merchantCategoryModel.PictureThumbnailUrl = storeBaseUrl + thumbFileName;
						}
						else
						{
							merchantCategoryModel.PictureThumbnailUrl = storeBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
						}

						merchantCategoryList.Add(merchantCategoryModel);
					}

					customerAPIResponses.ErrorMessageTitle = "Success!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.Available", languageId, dbcontext);
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = merchantCategoryList;
					return customerAPIResponses;
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.NoMerchantCategorysAvailableforthisStore", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.ServerError", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

		}

		[Route("~/api/v1/GetMerchantsByMerchantCategoryId")]
		[HttpPost]
		public virtual CustomerAPIResponses GetMerchantsByMerchantCategoryId(MerchantCategorysModel merchantCategorysModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(merchantCategorysModel.StoreId, merchantCategorysModel.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (merchantCategorysModel == null)
			{
				//KP Comments//Please use a function for response
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (string.IsNullOrEmpty(merchantCategorysModel.ApiKey) || string.IsNullOrWhiteSpace(merchantCategorysModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (merchantCategorysModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (merchantCategorysModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			try
			{
				if (!string.IsNullOrEmpty(merchantCategorysModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == merchantCategorysModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				var vendors = (from v in dbcontext.Vendor.AsEnumerable()
							   join mcm in dbcontext.MerchantCategoryMapping.AsEnumerable() on v.Id equals mcm.VendorId
							   where v.StoreId == merchantCategorysModel.StoreId && mcm.MerchantCategoryId == merchantCategorysModel.MerchantCategoryId
							   && v.Active && !v.Deleted
							   select new CooknRestV2
							   {
								   RestnCookId = v.Id,
								   CooknRestName = v.Name,
								   PictureId = v.PictureId,
								   IsOpen = v.IsOpen == null ? false : v.IsOpen.Value,
								   RestnCookAddress = v.AddressId.ToString(),
								   Distance = "0 Km",
								   OpenTime = v.Opentime,
								   CloseTime = v.Closetime

							   }).ToList();

				if (!vendors.Any())
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.SorryNoDataAvailable", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}

				if (merchantCategorysModel.StoreId != 0)
				{
					var storeExist = dbcontext.Store.Any(x => x.Id == merchantCategorysModel.StoreId);
					if (!storeExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				if (merchantCategorysModel.CustomerId != 0)
				{
					var customerExist = dbcontext.Customer.Any(x => x.Id == merchantCategorysModel.CustomerId && x.Active && !x.Deleted);
					if (!customerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				var isApproved = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("ratingreviewsettings.merchant.isapprovalrequired") && x.StoreId == merchantCategorysModel.StoreId).Any() ? Convert.ToBoolean(dbcontext.Setting.Where(x => x.Name.Contains("ratingreviewsettings.merchant.isapprovalrequired") && x.StoreId == merchantCategorysModel.StoreId).FirstOrDefault().Value) : false;

				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == merchantCategorysModel.StoreId).FirstOrDefault().Value;

				List<CooknRestV2> TopRatedChefnRestaurantlist = new List<CooknRestV2>();
				foreach (var item in vendors)
				{
					item.CooknRestName = item.CooknRestName.ToUpper().ToString();
					if (!string.IsNullOrEmpty(item.RestnCookAddress))
					{
						var address = Convert.ToInt32(item.RestnCookAddress);
						var Addreess2 = dbcontext.Address.Where(x => x.Id == address).FirstOrDefault();
						if (Addreess2 != null)
						{
							item.RestnCookAddress = Addreess2.Address1 + " " + Addreess2.Address2;
						}

					}
					item.Rating = isApproved ? (!(from rating in dbcontext.RatingReviews.Where(x => x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == merchantCategorysModel.StoreId
															 && x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved && x.IsLiked)
												  where rating.EntityId == item.RestnCookId
												  select new
												  {
													  rating
												  }).AsEnumerable().Any() ? 0 :
							(from rating in dbcontext.RatingReviews.Where(x => x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == merchantCategorysModel.StoreId
														   && x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved && x.IsLiked)
							 where rating.EntityId == item.RestnCookId
							 select new
							 {
								 rating
							 }).AsEnumerable().ToList().Count(x => x.rating.IsLiked)) : (!(from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == merchantCategorysModel.StoreId
														   && x.IsNotInterested == false && x.IsSkipped == false && x.IsLiked)
																						   where rating.EntityId == item.RestnCookId
																						   select new
																						   {
																							   rating
																						   }).AsEnumerable().Any() ? 0 :
							(from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == merchantCategorysModel.StoreId
														   && x.IsNotInterested == false && x.IsSkipped == false && x.IsLiked)
							 where rating.EntityId == item.RestnCookId
							 select new
							 {
								 rating
							 }).AsEnumerable().ToList().Count(x => x.rating.IsLiked));
					item.RatingCount = isApproved ? (from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == merchantCategorysModel.StoreId
								 && x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved)
													 where rating.EntityId == item.RestnCookId
													 select new
													 {
														 rating.Rating
													 }).AsEnumerable().Count() : (from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == merchantCategorysModel.StoreId
													&& x.IsNotInterested == false && x.IsSkipped == false)
																				  where rating.EntityId == item.RestnCookId
																				  select new
																				  {
																					  rating.Rating
																				  }).AsEnumerable().Count();

					var restDetail = dbcontext.Vendor.Where(x => x.Id == item.RestnCookId).FirstOrDefault();
					string[] venLtLngStrArr = restDetail.Geolocation.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');

					int pictureSize = 100;
					var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.vendorthumbpicturesize")).FirstOrDefault();
					if (setting != null)
					{
						pictureSize = Convert.ToInt32(setting.Value);
					}
					if (item.PictureId != 0)
					{

						var picture = dbcontext.Picture.Where(x => x.Id == item.PictureId).FirstOrDefault();
						string lastPart = Helper.GetFileExtensionFromMimeType(picture.MimeType);
						string thumbFileName = !string.IsNullOrEmpty(picture.SeoFilename)
						? $"{picture.Id:0000000}_{picture.SeoFilename}_{pictureSize}.{lastPart}" + "? " + DateTime.Now
						: $"{picture.Id:0000000}_{pictureSize}.{lastPart}" + "?" + DateTime.Now;


						item.CooknRestImageURL = ChefBaseUrl + thumbFileName;
					}
					else
					{
						item.CooknRestImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
					}
					item.IsRestAvailable = false;
					if (item.CooknRestName.ToLower().Contains("live"))
					{
						var isVendorSchedule = dbcontext.Vendor.Where(x => x.Id == item.RestnCookId).FirstOrDefault().AvailableType;
						if (isVendorSchedule != null && isVendorSchedule.Value != 0)
						{
							if (isVendorSchedule == (int)AvailableTypeEnum.AllDay)
							{
								var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.AllDay).ToList();
								if (AllDaySchedulesTimimgs.Any())
								{
									//var CurrentTime = DateTime.Now.TOS;
									var CurrentTime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).TimeOfDay;
									foreach (var __item in AllDaySchedulesTimimgs)
									{
										if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
										{
											item.IsRestAvailable = true;
										}
										DateTime fromTime = DateTime.Today.Add(__item.ScheduleFromTime);
										string displayFromTime = fromTime.ToString("hh:mm tt");
										DateTime toTime = DateTime.Today.Add(__item.ScheduleToTime);
										string displayToTime = toTime.ToString("hh:mm tt");
										string fromToTime = displayFromTime + " - " + displayToTime;
										item.OpenCloseTime.Add(fromToTime);

									}
								}
								else
								{
									item.IsRestAvailable = true;
								}

							}
							else if (isVendorSchedule == (int)AvailableTypeEnum.Custom)
							{
								var dayofWeek = (int)Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).DayOfWeek;
								var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.Custom && x.AvailableOn == dayofWeek).ToList();
								if (AllDaySchedulesTimimgs.Any())
								{
									//var CurrentTime = DateTime.Now.TOS;
									var CurrentTime = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext).TimeOfDay;
									foreach (var __item in AllDaySchedulesTimimgs)
									{
										if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
										{
											item.IsRestAvailable = true;
										}
										DateTime fromTime = DateTime.Today.Add(__item.ScheduleFromTime);
										string displayFromTime = fromTime.ToString("hh:mm tt");
										DateTime toTime = DateTime.Today.Add(__item.ScheduleToTime);
										string displayToTime = toTime.ToString("hh:mm tt");
										string fromToTime = displayFromTime + " - " + displayToTime;
										item.OpenCloseTime.Add(fromToTime);

									}
								}
								else
								{
									item.IsRestAvailable = true;
								}
							}
							else
							{
								item.IsRestAvailable = true;
							}
						}
						else
						{
							item.IsRestAvailable = true;
						}
					}
					else
					{
						item.IsRestAvailable = true;
					}
					if (item.Rating != 0)
					{
						item.Rating = Convert.ToDecimal((item.Rating / item.RatingCount));
						item.Rating = Math.Round(item.Rating * 5, 2);
					}
					TopRatedChefnRestaurantlist.Add(item);
				}

				if (TopRatedChefnRestaurantlist.Any())
				{
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = TopRatedChefnRestaurantlist.Distinct().ToList();
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.SorryNoDataAvailable", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v2.1/GetMerchantsByMerchantCategoryId")]
		[HttpPost]
		public virtual CustomerAPIResponses GetMerchantsByMerchantCategoryIdV2_1(MerchantCategorysModel merchantCategorysModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(merchantCategorysModel.StoreId, merchantCategorysModel.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (merchantCategorysModel == null)
			{
				//KP Comments//Please use a function for response
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (string.IsNullOrEmpty(merchantCategorysModel.ApiKey) || string.IsNullOrWhiteSpace(merchantCategorysModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (merchantCategorysModel.CustomerId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (merchantCategorysModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}

			try
			{
				if (!string.IsNullOrEmpty(merchantCategorysModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == merchantCategorysModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				var vendors = (from v in dbcontext.Vendor.AsEnumerable()
							   join mcm in dbcontext.MerchantCategoryMapping.AsEnumerable() on v.Id equals mcm.VendorId
							   where v.StoreId == merchantCategorysModel.StoreId && mcm.MerchantCategoryId == merchantCategorysModel.MerchantCategoryId
							   && v.Active && !v.Deleted
							   select new CooknRestV2
							   {
								   RestnCookId = v.Id,
								   CooknRestName = v.Name,
								   PictureId = v.PictureId,
								   IsOpen = v.IsOpen == null ? false : v.IsOpen.Value,
								   RestnCookAddress = v.AddressId.ToString(),
								   Distance = "0 Km",
								   OpenTime = v.Opentime,
								   CloseTime = v.Closetime

							   }).ToList();

				if (!vendors.Any())
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.SorryNoDataAvailable", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}

				if (merchantCategorysModel.StoreId != 0)
				{
					var storeExist = dbcontext.Store.Any(x => x.Id == merchantCategorysModel.StoreId);
					if (!storeExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMessage.StoreNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				if (merchantCategorysModel.CustomerId != 0)
				{
					var customerExist = dbcontext.Customer.Any(x => x.Id == merchantCategorysModel.CustomerId && x.Active && !x.Deleted);
					if (!customerExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				var isApproved = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("ratingreviewsettings.merchant.isapprovalrequired") && x.StoreId == merchantCategorysModel.StoreId).Any() ? Convert.ToBoolean(dbcontext.Setting.Where(x => x.Name.Contains("ratingreviewsettings.merchant.isapprovalrequired") && x.StoreId == merchantCategorysModel.StoreId).FirstOrDefault().Value) : false;

				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == merchantCategorysModel.StoreId).FirstOrDefault().Value;

				List<CooknRestV2> TopRatedChefnRestaurantlist = new List<CooknRestV2>();
				foreach (var item in vendors)
				{
					item.CooknRestName = item.CooknRestName.ToUpper().ToString();
					if (!string.IsNullOrEmpty(item.RestnCookAddress))
					{
						var address = Convert.ToInt32(item.RestnCookAddress);
						var Addreess2 = dbcontext.Address.Where(x => x.Id == address).FirstOrDefault();
						if (Addreess2 != null)
						{
							item.RestnCookAddress = Addreess2.Address1 + " " + Addreess2.Address2;
						}

					}
					item.Rating = isApproved ? (!(from rating in dbcontext.RatingReviews.Where(x => x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == merchantCategorysModel.StoreId
															 && x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved && x.IsLiked)
												  where rating.EntityId == item.RestnCookId
												  select new
												  {
													  rating
												  }).AsEnumerable().Any() ? 0 :
							(from rating in dbcontext.RatingReviews.Where(x => x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == merchantCategorysModel.StoreId
														   && x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved && x.IsLiked)
							 where rating.EntityId == item.RestnCookId
							 select new
							 {
								 rating
							 }).AsEnumerable().ToList().Count(x => x.rating.IsLiked)) : (!(from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == merchantCategorysModel.StoreId
														   && x.IsNotInterested == false && x.IsSkipped == false && x.IsLiked)
																						   where rating.EntityId == item.RestnCookId
																						   select new
																						   {
																							   rating
																						   }).AsEnumerable().Any() ? 0 :
							(from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == merchantCategorysModel.StoreId
														   && x.IsNotInterested == false && x.IsSkipped == false && x.IsLiked)
							 where rating.EntityId == item.RestnCookId
							 select new
							 {
								 rating
							 }).AsEnumerable().ToList().Count(x => x.rating.IsLiked));
					item.RatingCount = isApproved ? (from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == merchantCategorysModel.StoreId
								 && x.IsNotInterested == false && x.IsSkipped == false && x.IsApproved)
													 where rating.EntityId == item.RestnCookId
													 select new
													 {
														 rating.Rating
													 }).AsEnumerable().Count() : (from rating in dbcontext.RatingReviews.Where(x => x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Merchant && x.StoreId == merchantCategorysModel.StoreId
													&& x.IsNotInterested == false && x.IsSkipped == false)
																				  where rating.EntityId == item.RestnCookId
																				  select new
																				  {
																					  rating.Rating
																				  }).AsEnumerable().Count();

					var restDetail = dbcontext.Vendor.Where(x => x.Id == item.RestnCookId).FirstOrDefault();
					string[] venLtLngStrArr = restDetail.Geolocation.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');

					int pictureSize = 100;
					var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.vendorthumbpicturesize")).FirstOrDefault();
					if (setting != null)
					{
						pictureSize = Convert.ToInt32(setting.Value);
					}
					if (item.PictureId != 0)
					{

						var picture = dbcontext.Picture.Where(x => x.Id == item.PictureId).FirstOrDefault();
						string lastPart = Helper.GetFileExtensionFromMimeType(picture.MimeType);
						string thumbFileName = !string.IsNullOrEmpty(picture.SeoFilename)
						? $"{picture.Id:0000000}_{picture.SeoFilename}_{pictureSize}.{lastPart}" + "? " + DateTime.Now
						: $"{picture.Id:0000000}_{pictureSize}.{lastPart}" + "?" + DateTime.Now;


						item.CooknRestImageURL = ChefBaseUrl + thumbFileName;
					}
					else
					{
						item.CooknRestImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
					}
					item.IsRestAvailable = false;
					if (item.CooknRestName.ToLower().Contains("live"))
					{
						var isVendorSchedule = dbcontext.Vendor.Where(x => x.Id == item.RestnCookId).FirstOrDefault().AvailableType;
						if (isVendorSchedule != null && isVendorSchedule.Value != 0)
						{
							if (isVendorSchedule == (int)AvailableTypeEnum.AllDay)
							{
								var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.AllDay).ToList();
								if (AllDaySchedulesTimimgs.Any())
								{
									//var CurrentTime = DateTime.Now.TOS;
									var CurrentTime = Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, merchantCategorysModel.CustomerId).TimeOfDay;
									foreach (var __item in AllDaySchedulesTimimgs)
									{
										if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
										{
											item.IsRestAvailable = true;
										}
										DateTime fromTime = DateTime.Today.Add(__item.ScheduleFromTime);
										string displayFromTime = fromTime.ToString("hh:mm tt");
										DateTime toTime = DateTime.Today.Add(__item.ScheduleToTime);
										string displayToTime = toTime.ToString("hh:mm tt");
										string fromToTime = displayFromTime + " - " + displayToTime;
										item.OpenCloseTime.Add(fromToTime);

									}
								}
								else
								{
									item.IsRestAvailable = true;
								}

							}
							else if (isVendorSchedule == (int)AvailableTypeEnum.Custom)
							{
								var dayofWeek = (int)Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, merchantCategorysModel.CustomerId).DayOfWeek;
								var AllDaySchedulesTimimgs = dbcontext.VendorScheduleMapping.Where(x => x.VendorId == item.RestnCookId && x.AvailableType == (int)AvailableTypeEnum.Custom && x.AvailableOn == dayofWeek).ToList();
								if (AllDaySchedulesTimimgs.Any())
								{
									//var CurrentTime = DateTime.Now.TOS;
									var CurrentTime = Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, merchantCategorysModel.CustomerId).TimeOfDay;
									foreach (var __item in AllDaySchedulesTimimgs)
									{
										if (__item.ScheduleFromTime <= CurrentTime && __item.ScheduleToTime >= CurrentTime)
										{
											item.IsRestAvailable = true;
										}
										DateTime fromTime = DateTime.Today.Add(__item.ScheduleFromTime);
										string displayFromTime = fromTime.ToString("hh:mm tt");
										DateTime toTime = DateTime.Today.Add(__item.ScheduleToTime);
										string displayToTime = toTime.ToString("hh:mm tt");
										string fromToTime = displayFromTime + " - " + displayToTime;
										item.OpenCloseTime.Add(fromToTime);

									}
								}
								else
								{
									item.IsRestAvailable = true;
								}
							}
							else
							{
								item.IsRestAvailable = true;
							}
						}
						else
						{
							item.IsRestAvailable = true;
						}
					}
					else
					{
						item.IsRestAvailable = true;
					}
					if (item.Rating != 0)
					{
						item.Rating = Convert.ToDecimal((item.Rating / item.RatingCount));
						item.Rating = Math.Round(item.Rating * 5, 2);
					}
					TopRatedChefnRestaurantlist.Add(item);
				}

				if (TopRatedChefnRestaurantlist.Any())
				{
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = TopRatedChefnRestaurantlist.Distinct().ToList();
				}
				else
				{
					customerAPIResponses.ErrorMessageTitle = "Error!!";
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(merchantCategorysModel.StoreId, "API.ErrorMesaage.SorryNoDataAvailable", languageId, dbcontext);
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v1/SearchItems")]
		[HttpPost]
		public ItemsAPIResponses SearchItems([FromBody] SearchItemsModel requestObj)
		{
			int languageId = LanguageHelper.GetIdByLangCode(requestObj.StoreId, requestObj.UniqueSeoCode, dbcontext);

			ItemsAPIResponses responseObj = new ItemsAPIResponses();
			if (requestObj == null)
			{
				return Helper.GetAPIErrorResponses(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext));
			}
			if (string.IsNullOrEmpty(requestObj.APIKey))
			{
				return Helper.GetAPIErrorResponses(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext));
			}
			if (requestObj.CustomerId == 0)
			{
				return Helper.GetAPIErrorResponses(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.ErrorMesaage.CustomerId", languageId, dbcontext));
			}
			if (requestObj.RestnCookId == 0)
			{
				return Helper.GetAPIErrorResponses(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.ErrorMesaage.RestaurantIdNotDefined", languageId, dbcontext));
			}
			try
			{
				if (!string.IsNullOrEmpty(requestObj.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == requestObj.APIKey && x.StoreId == requestObj.StoreId).Any();
					if (!keyExist)
					{
						return Helper.GetAPIErrorResponses(LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.ErrorMesaage.InvalidAuthentication", languageId, dbcontext), (int)HttpStatusCode.Unauthorized);
					}
				}

				string currencySymbol = Helper.GetStoreCurrency(requestObj.StoreId, dbcontext);
				string baseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == requestObj.StoreId).FirstOrDefault()?.Value;
				var itemsQuery = from p in dbcontext.Product
								 join sm in dbcontext.StoreMapping
								 on p.Id equals sm.EntityId
								 where p.VendorId == requestObj.RestnCookId &&
								 p.Published && !p.Deleted &&
								 sm.EntityName == "Product" &&
								 sm.StoreId == requestObj.StoreId &&
								 (
									 string.IsNullOrEmpty(requestObj.Keywords) ||
									 p.Name.Contains(requestObj.Keywords) ||
									 p.ShortDescription.Contains(requestObj.Keywords)
								 )
								 select new
								 {
									 ItemId = p.Id,
									 ItemName = LanguageHelper.GetLocalizedValueProduct(p.Id, "Name", languageId, p.Name, dbcontext),
									 Description = LanguageHelper.GetLocalizedValueProduct(p.Id, "ShortDescription", languageId, p.ShortDescription ?? "", dbcontext),
									 Quantity = dbcontext.ShoppingCartItem.Where(x => x.ProductId == p.Id && x.CustomerId == requestObj.CustomerId && x.ShoppingCartTypeId == 1).Sum(x => x.Quantity),
									 Available = p.Published,
									 Price = p.Price,
									 ImageUrl = Helper.GetProductPictureUrl(p, 75, baseUrl, dbcontext),
									 ProductProductAttributes = Helper.GetProductAttributes(p, currencySymbol, dbcontext)
								 };

				var totalRecords = itemsQuery.Count();
				if (requestObj.PageSize == 0)
					requestObj.PageSize = (int.MaxValue - 1);
				if (requestObj.PageIndex > 0)
					requestObj.PageIndex = requestObj.PageIndex - 1;

				var pagedItems = itemsQuery.Skip((requestObj.PageIndex) * requestObj.PageSize).Take(requestObj.PageSize).ToList();

				if (pagedItems.Any())
				{
					responseObj.Status = true;
					responseObj.StatusCode = (int)HttpStatusCode.OK;
					responseObj.TotalRecords = totalRecords;
					responseObj.ResponseObj = pagedItems.Distinct().ToList();
				}
				else
				{
					responseObj.Status = false;
					responseObj.StatusCode = (int)HttpStatusCode.NoContent;
					responseObj.ResponseObj = null;
					responseObj.ErrorMessage = LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.ErrorMesaageChefNotProvidingAnyProductForSellYet", languageId, dbcontext);
					responseObj.ErrorMessageTitle = "Error!!";
				}

				return responseObj;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				return Helper.GetAPIErrorResponses(ex.Message, 400);
			}
		}

		[Route("~/api/v1/GetProductDetailByProductId")]
		[HttpPost]
		public CustomerAPIResponses GetProductDetailByProductId([FromBody] CooknRestProductDetailsByID productInfo)
		{
			int languageId = LanguageHelper.GetIdByLangCode(productInfo.StoreId, productInfo.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (productInfo == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(productInfo.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (string.IsNullOrEmpty(productInfo.APIKey) || string.IsNullOrWhiteSpace(productInfo.APIKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(productInfo.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (productInfo.CustId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(productInfo.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (productInfo.RestnCookId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(productInfo.StoreId, "API.ErrorMesaage.RestaurantAndCookIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (productInfo.ProductId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(productInfo.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (productInfo.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(productInfo.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(productInfo.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == productInfo.APIKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(productInfo.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}
				string CurrencySymbol = Helper.GetStoreCurrency(productInfo.StoreId, dbcontext);
				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == productInfo.StoreId).FirstOrDefault().Value;
				var storeName = Helper.GetStoreName(productInfo.StoreId, dbcontext);
				var productDetailById = (from a in dbcontext.Product.AsEnumerable()
										 where a.Id == productInfo.ProductId
										 && !a.Deleted && a.Published
										 select new ProductDetailResponseModel
										 {
											 Id = a.Id,
											 Name = LanguageHelper.GetLocalizedValueProduct(a.Id, "Name", languageId, a.Name, dbcontext),
											 Description = LanguageHelper.GetLocalizedValueProduct(a.Id, "ShortDescription", languageId, a.ShortDescription ?? "", dbcontext),
											 FullDescription = LanguageHelper.GetLocalizedValueProduct(a.Id, "Description", languageId, a.FullDescription ?? "", dbcontext),
											 Quantity = a.StockQuantity,
											 TrackInventory = a.ManageInventoryMethodId == 1 || a.ManageInventoryMethodId == 2 ? true : false,
											 MerchantProductDescription = string.Format(LanguageHelper.GetResourseValueByName(productInfo.StoreId, "NB.API.ErrorMessage.SoldBy", languageId, dbcontext), " " + storeName),
											 Price = dbcontext.Product.AsEnumerable().Where(x => x.Id == a.Id && x.TaxCategoryId == 0).FirstOrDefault() != null
													? dbcontext.Product.AsEnumerable().Where(x => x.Id == a.Id && x.TaxCategoryId == 0).FirstOrDefault().Price.ToString() : (from priceCategoryTax in dbcontext.Product.AsEnumerable()
																																											 join categoryTax in dbcontext.TaxRate.AsEnumerable()
																																								  on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
																																											 where categoryTax.StoreId == productInfo.StoreId &&
																																												 priceCategoryTax.Id == a.Id

																																											 select new
																																											 {
																																												 PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
																																											 }).FirstOrDefault().PriceCalculated.ToString(),
											 OldPrice = Convert.ToString(Helper.ConvertdecimaltoUptotwoPlaces(a.OldPrice)),
											 Rating = (!(from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item)

														 select new
														 {
															 rating.Rating
														 }).Any()) ? 0 :
										 (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item)

										  select new
										  {
											  rating.Rating
										  }).ToList().Sum(x => x.Rating),
											 RatingCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item)

															select new
															{
																rating.Rating
															}).Count(),
											 ReviewCount = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item && !string.IsNullOrEmpty(x.ReviewText))

															select new
															{
																rating.ReviewText
															}).Count(),
											 CustomerImages = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item && !string.IsNullOrEmpty(x.ReviewText))
															   join customerImage in dbcontext.GenericAttribute.AsEnumerable().Where(x => x.Key == "ProfilePic")
															   on rating.CustomerId equals customerImage.EntityId
															   select new CustomerImage
															   {
																   Image = customerImage.Value
															   }).ToList(),

											 Comments = (from rating in dbcontext.RatingReviews.AsEnumerable().Where(x => x.EntityId == a.Id && x.IsApproved && x.CustomerId == productInfo.CustId && x.ReviewType == (int)NHKCustomerApplication.Utilities.ReviewType.Item)
														 select new ProductCommentsModels
														 {
															 Id = rating.Id,
															 CustomerName = dbcontext.GenericAttribute.AsEnumerable().Where(c => c.EntityId == rating.CustomerId && c.Key.Contains("FirstName"))?.FirstOrDefault()?.Value + " " +
dbcontext.GenericAttribute.AsEnumerable().Where(c => c.EntityId == rating.CustomerId && c.Key.Contains("LastName"))?.FirstOrDefault()?.Value,
															 Comment = rating.ReviewText,
															 Rating = Convert.ToString(rating.Rating),
															 DateTime = Helper.ConvertToUserTime(rating.CreatedOnUtc, DateTimeKind.Utc, dbcontext),
															 CustomerImage = dbcontext.GenericAttribute.AsEnumerable().Where(c => c.EntityId == rating.CustomerId && c.Key.Contains("ProfilePic")).FirstOrDefault() != null ? dbcontext.GenericAttribute.Where(c => c.EntityId == rating.CustomerId && c.Key.Contains("ProfilePic")).FirstOrDefault().Value : " "
														 }).OrderByDescending(x => x.Id).ToList(),
											 Preferences = (from ProductTag in dbcontext.ProductProductTagMapping.AsEnumerable().Where(x => x.ProductId == a.Id)
															join ProductTagss in dbcontext.ProductTag.AsEnumerable()
															on ProductTag.ProductTagId equals ProductTagss.Id
															join ProductTagImage in dbcontext.ProductTagImageMapping.AsEnumerable()
															on ProductTagss.Id equals ProductTagImage.TagId
															join ProductTagImageDetails in dbcontext.Picture.AsEnumerable()
															on ProductTagImage.PictureId equals ProductTagImageDetails.Id
															select new PreferencesV2
															{
																Name = ProductTagss.Name,
																Image = ProductTagImageDetails.Id.ToString(),


															}
											 ).Distinct().ToList(),
											 ImageUrl = (from product in dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == a.Id)
														 join picture in dbcontext.Picture.AsEnumerable()
														 on product.PictureId equals picture.Id
														 select new CooknChefProductsImagesV2
														 {
															 ProductImageURL = picture.Id.ToString()
														 }).ToList(),
											 Specifications = (from Product in dbcontext.Product.AsEnumerable()
															   join psm in dbcontext.ProductSpecificationAttributeMapping.AsEnumerable()
															   on Product.Id equals psm.ProductId
															   join sao1 in dbcontext.SpecificationAttributeOption.AsEnumerable()
															   on psm.SpecificationAttributeOptionId equals sao1.Id
															   join sa1 in dbcontext.SpecificationAttribute.AsEnumerable()
															   on sao1.SpecificationAttributeId equals sa1.Id
															   where psm.ProductId == a.Id && sao1.SpecificationAttributeId == sa1.Id
															   group sa1 by sao1.SpecificationAttributeId into productSpecificationsAttributes
															   select new SpecificationModel
															   {
																   Id = productSpecificationsAttributes.Key,
																   Name = productSpecificationsAttributes.Select(x => x.Name).FirstOrDefault(),
																   ProductSpecificationAttribute = (from Product in dbcontext.Product.AsEnumerable()
																									join sam in dbcontext.ProductSpecificationAttributeMapping.AsEnumerable()
																									on Product.Id equals sam.ProductId
																									join sao in dbcontext.SpecificationAttributeOption.AsEnumerable()
																									on sam.SpecificationAttributeOptionId equals sao.Id
																									where Product.Id == a.Id && sao.SpecificationAttributeId == productSpecificationsAttributes.Key
																									select new productSpecificationAttribute
																									{
																										Id = sao.Id,
																										Name = sao.Name
																									}).Distinct().ToList()
															   }).Distinct().ToList(),

											 Cuisine = (from productcuisine in dbcontext.ProductProductCuisineMapping.AsEnumerable()
														join product in dbcontext.ProductCuisine.AsEnumerable()
														on productcuisine.CuisineId equals product.Id
														where productcuisine.ProductId == a.Id
														select new
														{
															product.Name
														}).FirstOrDefault() != null ? (from productcuisine in dbcontext.ProductProductCuisineMapping.AsEnumerable()
																					   join product in dbcontext.ProductCuisine.AsEnumerable()
																					   on productcuisine.CuisineId equals product.Id
																					   where productcuisine.ProductId == a.Id
																					   select new
																					   {
																						   product.Name
																					   }).FirstOrDefault().Name : "",
											 DishPrepareTime = a.PrepareTime != 0 ? a.PrepareTime + " min" : "0 min",
											 ProductProductAttributes = (from _a in dbcontext.Product.AsEnumerable().Where(x => x.Id == a.Id)
																		 join b in dbcontext.ProductProductAttributeMapping.AsEnumerable()
																		 on _a.Id equals b.ProductId
																		 join c in dbcontext.ProductAttribute.AsEnumerable()
																		 on b.ProductAttributeId equals c.Id
																		 select new CooknRestProductProductAttributesResponse
																		 {
																			 AttributeName = c.Name,
																			 IsRequired = b.IsRequired,
																			 AttributeTypeId = b.AttributeControlTypeId,
																			 ProductId = _a.Id,
																			 ProductAttributeId = c.Id,
																			 ProductAttributeMappingId = b.Id,
																			 ProductAttribute = (from d in dbcontext.ProductAttributeValue.AsEnumerable()
																								 where d.ProductAttributeMappingId == b.Id
																								 select new ProductAttributes
																								 {
																									 Name = d.Name,
																									 Price = Convert.ToString(Helper.ConvertdecimaltoUptotwoPlaces(d.PriceAdjustment)),
																									 ProductAttributeValueId = d.Id,
																									 IsPreSelected = d.IsPreSelected,
																									 Currency = CurrencySymbol
																								 }).ToList()
																		 }).ToList(),
											 IsProductAttributesExist = (from _a in dbcontext.Product.Where(x => x.Id == a.Id)
																		 join b in dbcontext.ProductProductAttributeMapping.AsEnumerable()
																		 on _a.Id equals b.ProductId
																		 join c in dbcontext.ProductAttribute.AsEnumerable()
																		 on b.ProductAttributeId equals c.Id
																		 select new CooknRestProductProductAttributesResponse
																		 {
																			 AttributeName = c.Name,
																			 IsRequired = b.IsRequired,
																			 AttributeTypeId = b.AttributeControlTypeId,
																			 ProductId = _a.Id,
																			 ProductAttributeId = c.Id,
																			 ProductAttributeMappingId = b.Id,
																			 ProductAttribute = (from d in dbcontext.ProductAttributeValue.AsEnumerable()
																								 where d.ProductAttributeMappingId == b.Id
																								 select new ProductAttributes
																								 {
																									 Name = d.Name,
																									 Price = d.PriceAdjustment.ToString(),
																									 ProductAttributeValueId = d.Id,
																									 IsPreSelected = d.IsPreSelected,
																									 Currency = CurrencySymbol
																								 }).ToList()
																		 }).Any()
										 }).AsEnumerable().ToList();
				if (productDetailById != null)
				{
					foreach (var item in productDetailById)
					{
						foreach (var attribute in item.ProductProductAttributes)
						{
							if (attribute.AttributeTypeId == (int)ProductAttributeEnum.Checkboxes)
							{
								attribute.AttributeMultiSelection = true;
							}
							else
							{
								attribute.AttributeMultiSelection = false;
							}

						}
						item.Comments = item.Comments.Skip((productInfo.PageIndex) * productInfo.PageSize).Take(productInfo.PageSize).ToList();
						item.Price = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.Price));
						foreach (var pref in item.Preferences)
						{

							int pictureId = Convert.ToInt32(pref.Image);
							string lastPart = Helper.GetFileExtensionFromMimeType(pref.ImageType);
							var prefdetail = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();
							var fileName = !string.IsNullOrEmpty(prefdetail.SeoFilename)
										? $"{pictureId:0000000}_{prefdetail.SeoFilename}_{75}.{lastPart}" + "? " + DateTime.Now
										: $"{pictureId:0000000}_{75}.{lastPart}" + "? " + DateTime.Now;
							pref.Image = ChefBaseUrl + fileName;
						}
						int pictureSize = 75;
						var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
						if (setting != null)
						{
							pictureSize = Convert.ToInt32(setting.Value);
						}
						if (item.ImageUrl.Any())
						{
							foreach (var image in item.ImageUrl)
							{
								if (!string.IsNullOrEmpty(image.ProductImageURL))
								{
									int pictureId = Convert.ToInt32(image.ProductImageURL);
									var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();

									string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
									string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
									? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}" + "? " + DateTime.Now
									: $"{pictureId:0000000}_{pictureSize}.{lastPart}" + "? " + DateTime.Now;
									image.ProductImageURL = ChefBaseUrl + thumbFileName;
								}
								else
								{
									image.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
								}
							}
						}
						else
						{
							CooknChefProductsImagesV2 cooknChefProductsImagesV2 = new CooknChefProductsImagesV2();
							cooknChefProductsImagesV2.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
							item.ImageUrl.Add(cooknChefProductsImagesV2);
						}
						if (item.Rating != 0)
						{
							item.Rating = Convert.ToDouble(item.Rating / item.RatingCount);
							item.Rating = Math.Round(item.Rating, 2);
						}
					}
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = productDetailById;
				}
				else
				{
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
					customerAPIResponses.ResponseObj = null;
					customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(productInfo.StoreId, "API.ErrorMesaage.Datanotavailable", languageId, dbcontext);
					customerAPIResponses.ErrorMessageTitle = "Error!!";
				}

				var productSpecificationAttribute = (from sam in dbcontext.ProductSpecificationAttributeMapping.AsEnumerable()
													 join sao in dbcontext.SpecificationAttributeOption.AsEnumerable()
													 on sam.SpecificationAttributeOptionId equals sao.Id
													 where sam.ProductId == 123 && sao.SpecificationAttributeId == 14
													 select new productSpecificationAttribute
													 {
														 Id = sao.Id,
														 Name = sao.Name
													 }).ToList().GroupBy(sam => sam.Id);
				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		[Route("~/api/v1/GetSubcategoryOrProductByCategoryId")]
		[HttpPost]
		public CustomerAPIResponses GetSubcategoryOrProductByCategoryId([FromBody] CooknRestProductByCategoryID customer)
		{
			int languageId = LanguageHelper.GetIdByLangCode(customer.StoreId, customer.UniqueSeoCode, dbcontext);

			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
			if (customer == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (string.IsNullOrEmpty(customer.APIKey) || string.IsNullOrWhiteSpace(customer.APIKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (customer.CustId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext); LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (customer.RestnCookId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.RestaurantIdNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			else if (customer.CategoryId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaage.CategoryIdNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(customer.APIKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == customer.APIKey && x.StoreId == customer.StoreId).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				string CurrencySymbol = Helper.GetStoreCurrency(customer.StoreId, dbcontext);
				string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == customer.StoreId).FirstOrDefault()?.Value;
				var GetRestaurantnCookById = (from a in dbcontext.Category.AsEnumerable().Where(x => x.Deleted == false && x.Published == true && x.ParentCategoryId == customer.CategoryId)
											  join b in dbcontext.StoreMapping.AsEnumerable().Where(x => x.VendorId == customer.RestnCookId && x.EntityName == "Category")
											  on a.Id equals b.EntityId
											  select new CooknRestProductsV2
											  {
												  CategoryId = a?.Id ?? 0,
												  CategoryName = LanguageHelper.GetLocalizedValueCategory(a.Id, "Name", languageId, a.Name, dbcontext),
											  }).OrderBy(x => x.CategoryId).ToList();
				if (GetRestaurantnCookById.Any())
				{
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
					customerAPIResponses.ResponseObj = GetRestaurantnCookById.Distinct().ToList();

				}
				else
				{

					var categoryItems = (from c in dbcontext.Product.AsEnumerable()
										 join d in dbcontext.ProductCategoryMapping.AsEnumerable()
										 on c.Id equals d.ProductId
										 where c.VendorId == customer.RestnCookId
										 && d.CategoryId == customer.CategoryId && c.Deleted == false && c.Published == true
										 && c.Price != 0
										 select new CooknChefCatProductsV2
										 {
											 ItemId = c.Id,
											 ItemName = LanguageHelper.GetLocalizedValueProduct(c.Id, "Name", languageId, c.Name, dbcontext),
											 Quantity = dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.ProductId == c.Id && x.CustomerId == customer.CustId).Any() ? dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.ProductId == c.Id && x.CustomerId == customer.CustId).Sum(x => x.Quantity) : 0,
											 Description = LanguageHelper.GetLocalizedValueProduct(c.Id, "ShortDescription", languageId, c.ShortDescription ?? "", dbcontext),
											 Available = c.Published,
											 Cuisine = (from productcuisine in dbcontext.ProductProductCuisineMapping.AsEnumerable()
														join product in dbcontext.ProductCuisine.AsEnumerable()
														on productcuisine.CuisineId equals product.Id
														where productcuisine.ProductId == c.Id

														select new
														{
															product.Name
														}).FirstOrDefault() != null ? (from productcuisine in dbcontext.ProductProductCuisineMapping.AsEnumerable()
																					   join product in dbcontext.ProductCuisine.AsEnumerable()
																					   on productcuisine.CuisineId equals product.Id
																					   where productcuisine.ProductId == c.Id
																					   select new
																					   {
																						   product.Name
																					   }).FirstOrDefault().Name : "",
											 ImageUrl = (from product in dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == c.Id)
														 join picture in dbcontext.Picture.AsEnumerable()
														 on product.PictureId equals picture.Id
														 select new CooknChefProductsImagesV2
														 {
															 ProductImageURL = picture.Id.ToString(),
															 MimeType = picture.MimeType,
															 FileSeo = picture.SeoFilename

														 }).ToList(),
											 Price = dbcontext.Product.AsEnumerable().Where(x => x.Id == c.Id && x.TaxCategoryId == 0).FirstOrDefault() != null
											  ? (dbcontext.Product.AsEnumerable().Where(x => x.Id == c.Id && x.TaxCategoryId == 0).FirstOrDefault()?.Price ?? 0).ToString()
											  : "" + (from priceCategoryTax in dbcontext.Product.AsEnumerable()
													  join categoryTax in dbcontext.TaxRate.AsEnumerable()
											   on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
													  where categoryTax.StoreId == customer.StoreId &&
													  priceCategoryTax.Id == c.Id

													  select new
													  {
														  PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
													  }).FirstOrDefault()?.PriceCalculated != null ? (from priceCategoryTax in dbcontext.Product.AsEnumerable()
																									  join categoryTax in dbcontext.TaxRate.AsEnumerable()
																													  on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
																									  where categoryTax.StoreId == customer.StoreId &&
																									  priceCategoryTax.Id == c.Id

																									  select new
																									  {
																										  PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
																									  }).FirstOrDefault()?.PriceCalculated.ToString() : "0.0",

											 ProductProductAttributes = (from d in dbcontext.Product.AsEnumerable().Where(x => x.Id == c.Id)
																		 join e in dbcontext.ProductProductAttributeMapping.AsEnumerable()
																		 on d.Id equals e.ProductId
																		 join f in dbcontext.ProductAttribute.AsEnumerable()
																		 on e.ProductAttributeId equals f.Id
																		 select new CooknRestProductProductAttributesResponse
																		 {
																			 AttributeName = f.Name,
																			 IsRequired = e.IsRequired,
																			 AttributeTypeId = e.AttributeControlTypeId,
																			 ProductId = d.Id,
																			 ProductAttributeId = f.Id,
																			 ProductAttributeMappingId = e.Id,
																			 ProductAttribute = (from _d in dbcontext.ProductAttributeValue.AsEnumerable()
																								 where _d.ProductAttributeMappingId == e.Id
																								 select new ProductAttributes
																								 {
																									 Name = _d.Name,
																									 Price = _d.PriceAdjustment.ToString(),
																									 ProductAttributeValueId = _d.Id,
																									 IsPreSelected = _d.IsPreSelected,
																									 Currency = CurrencySymbol
																								 }).ToList()
																		 }).ToList(),
											 IsProductAttributesExist = (from d in dbcontext.Product.AsEnumerable().Where(x => x.Id == c.Id)
																		 join e in dbcontext.ProductProductAttributeMapping.AsEnumerable()
																		 on d.Id equals e.ProductId
																		 join f in dbcontext.ProductAttribute.AsEnumerable()
																		 on e.ProductAttributeId equals f.Id
																		 select new CooknRestProductProductAttributesResponse
																		 {
																			 AttributeName = f.Name,
																			 IsRequired = e.IsRequired,
																			 AttributeTypeId = e.AttributeControlTypeId,
																			 ProductId = d.Id,
																			 ProductAttributeId = f.Id,
																			 ProductAttributeMappingId = e.Id,
																			 ProductAttribute = (from _d in dbcontext.ProductAttributeValue.AsEnumerable()
																								 where _d.ProductAttributeMappingId == e.Id
																								 select new ProductAttributes
																								 {
																									 Name = _d.Name,
																									 Price = _d.PriceAdjustment.ToString(),
																									 ProductAttributeValueId = _d.Id,
																									 IsPreSelected = _d.IsPreSelected,
																									 Currency = CurrencySymbol
																								 }).ToList()
																		 }).Any(),
										 }).OrderBy(x => x.ItemId).ToList();

					if (categoryItems.Any())
					{
						List<CooknRestProductsV2> list = new List<CooknRestProductsV2>();
						foreach (var category in categoryItems)
						{
							category.Price = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(category.Price));
							foreach (var pref in category.ProductProductAttributes)
							{
								foreach (var ______item in pref.ProductAttribute)
								{
									______item.Price = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(______item.Price));
								}
							}

							int pictureSize = 75;
							var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
							if (setting != null)
							{
								pictureSize = Convert.ToInt32(setting.Value);
							}
							if (category.ImageUrl.Any())
								foreach (var pref in category.ImageUrl)
								{
									if (!string.IsNullOrEmpty(pref.ProductImageURL))
									{
										int pictureId = Convert.ToInt32(pref.ProductImageURL);
										if (pictureId != 0)
										{
											string lastPart = Helper.GetFileExtensionFromMimeType(pref.MimeType);
											string thumbFileName = !string.IsNullOrEmpty(pref.FileSeo)
											? $"{pictureId:0000000}_{pref.FileSeo}.{lastPart}" + "? " + DateTime.Now
											: $"{pictureId:0000000}_{pictureSize}.{lastPart}" + "? " + DateTime.Now;
											pref.ProductImageURL = ChefBaseUrl + thumbFileName;
										}
										else
										{
											pref.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
										}
									}
									else
									{
										pref.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
									}

								}
							else
							{
								CooknChefProductsImagesV2 cooknChefProductsImagesV2 = new CooknChefProductsImagesV2();
								cooknChefProductsImagesV2.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
								category.ImageUrl.Add(cooknChefProductsImagesV2);
							}
						}

						var item = new CooknRestProductsV2
						{
							CategoryItems = categoryItems
						};
						list.Add(item);

						customerAPIResponses.Status = true;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
						customerAPIResponses.ResponseObj = list;
					}
					else
					{
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
						customerAPIResponses.ResponseObj = null;
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(customer.StoreId, "API.ErrorMesaageChefNotProvidingAnyProductForSellYet", languageId, dbcontext);
						customerAPIResponses.ErrorMessageTitle = "Error!!";
					}
				}

				return customerAPIResponses;
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}

		/// <summary>
		/// Get merchant reviews
		/// </summary>
		/// <param name="reviewRequestModel"></param>
		/// <returns>CustomerAPIResponses</returns>
		[Route("~/api/v1/GetMerchantReviews")]
		[HttpPost]
		public CustomerAPIResponses GetMerchantReviews([FromBody] ReviewRequestModel reviewRequestModel)
		{
			int languageId = LanguageHelper.GetIdByLangCode(reviewRequestModel.StoreId, reviewRequestModel.UniqueSeoCode, dbcontext);
			CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();

			if (reviewRequestModel == null)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(reviewRequestModel.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (string.IsNullOrEmpty(reviewRequestModel.ApiKey))
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(reviewRequestModel.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (reviewRequestModel.StoreId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(reviewRequestModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (reviewRequestModel.EntityId == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(reviewRequestModel.StoreId, "API.Reviews.ErrorMesaage.Entity.Missing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			if (reviewRequestModel.ReviewType == 0)
			{
				customerAPIResponses.ErrorMessageTitle = "Error!!";
				customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(reviewRequestModel.StoreId, "API.Reviews.ErrorMesaage.Reviewtype.Missing", languageId, dbcontext);
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
			try
			{
				if (!string.IsNullOrEmpty(reviewRequestModel.ApiKey))
				{
					var keyExist = dbcontext.VersionInfo.AsEnumerable().Where(x => x.Apikey == reviewRequestModel.ApiKey).Any();
					if (!keyExist)
					{
						customerAPIResponses.ErrorMessageTitle = "Error!!";
						customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(reviewRequestModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
						customerAPIResponses.Status = false;
						customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
						customerAPIResponses.ResponseObj = null;
						return customerAPIResponses;
					}
				}

				var reviewsResponse = _commonService.GetReviewsByEntityId(reviewRequestModel, languageId, dbcontext);
				if (reviewsResponse != null && reviewsResponse.ValidData)
				{
					customerAPIResponses.ErrorMessage = null;
					customerAPIResponses.Status = true;
					customerAPIResponses.StatusCode = 200;
					customerAPIResponses.ResponseObj = reviewsResponse;
					return customerAPIResponses;
				}
				else
				{
					customerAPIResponses.ErrorMessage = reviewsResponse?.ResultData;
					customerAPIResponses.Status = false;
					customerAPIResponses.StatusCode = 204;
					customerAPIResponses.ResponseObj = null;
					return customerAPIResponses;
				}
			}
			catch (Exception ex)
			{
				Helper.SentryLogs(ex);
				customerAPIResponses.ErrorMessage = ex.Message;
				customerAPIResponses.Status = false;
				customerAPIResponses.StatusCode = 400;
				customerAPIResponses.ResponseObj = null;
				return customerAPIResponses;
			}
		}
	}
	public class SemiNumericComparer : IComparer<string>
	{
		public int Compare(string s1, string s2)
		{
			if (IsNumeric(s1) && IsNumeric(s2))
			{
				if (Convert.ToInt32(s1) > Convert.ToInt32(s2)) return 1;
				if (Convert.ToInt32(s1) < Convert.ToInt32(s2)) return -1;
				if (Convert.ToInt32(s1) == Convert.ToInt32(s2)) return 0;
			}

			if (IsNumeric(s1) && !IsNumeric(s2))
				return -1;

			if (!IsNumeric(s1) && IsNumeric(s2))
				return 1;

			return string.Compare(s1, s2, true);
		}

		public static bool IsNumeric(object value)
		{
			try
			{
				decimal i = Convert.ToDecimal(value.ToString());
				return true;
			}
			catch (FormatException ex)
			{
				Helper.SentryLogs(ex);
				return false;
			}
		}
	}
}