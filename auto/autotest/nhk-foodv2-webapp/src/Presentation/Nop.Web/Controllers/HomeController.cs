﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Core;
using Nop.Web.Areas.Admin.Models;
using Nop.Web.Models.Home;
using Nop.Data;
using System.Linq;
using Nop.Services.Common;
using System;
using System.Globalization;
using Nop.Services.Orders;
using Nop.Services.Vendors;
using Nop.Core.Domain.Customers;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain;
using Nop.Web.Framework.Themes;
using Nop.Core.Domain.NB;

namespace Nop.Web.Controllers
{
    public partial class HomeController : BasePublicController
    {
        #region Fields
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IVendorService _vendorService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly ICommonService _commonService;
        private readonly IThemeContext _themeContext;
        #endregion

        #region Ctor
        public HomeController(
            IWorkContext workContext,
            IStoreContext storeContext,
            IGenericAttributeService genericAttributeService,
            IVendorService vendorService,
            ILocalizationService localizationService,
            IWorkflowMessageService workflowMessageService,
            LocalizationSettings localizationSettings,
            StoreInformationSettings storeInformationSettings,
            ICommonService commonService,
            IThemeContext themeContext)
        {
            _workContext = workContext;
            _storeContext = storeContext;
            _genericAttributeService = genericAttributeService;
            _vendorService = vendorService;
            _localizationService = localizationService;
            _workflowMessageService = workflowMessageService;
            _localizationSettings = localizationSettings;
            _storeInformationSettings = storeInformationSettings;
            _commonService = commonService;
            _themeContext = themeContext;
        }
        #endregion

        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult Index()
        {
            ClearEnteredDeliveryAddress();
            int currStoreId = _workContext.GetCurrentStoreId;
            var store = _storeContext.CurrentStoreById(currStoreId);

            StoreItemModel model = new StoreItemModel();
            model.Name = store.Name;
            model.CompanyName = store.CompanyName;
            model.CompanyAddress = store.CompanyAddress;
            model.CompanyPhoneNumber = store.CompanyPhoneNumber;
            model.IsDisplayDeliveryOrTakeaway = store.IsDisplayDeliveryOrTakeaway;
            model.IsAggrigator = store.IsAggrigator;
            model.IsDelivery = store.IsDelivery;
            model.IsTakeaway = store.IsTakeaway;
            model.IsDinning = store.IsDinning;
            model.CountryId = store.CountryId;


            if (_workContext.StoreInformationSettings.CheckStoreFront)
            {
                if (_workContext.StoreInformationSettings.IsStoreFront)
                {
                    var vendorId = _vendorService.GetAllVendors(storeId: currStoreId).FirstOrDefault()?.Id ?? 0;
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                            "CustomerCurrentMerchantId", vendorId.ToString(), currStoreId);

                    if (vendorId == 0)
                    {
                        if (_workContext.CurrentCustomer.IsRegistered())
                            return Redirect("~/customer/info");
                        else
                            return Redirect("~/login");
                    }
                }
                return View(model);
            }
            else
            {

                if (store.IsAggrigator == false)
                {
                    int vendorId = _workContext.CurrentMerchantId;
                    //Add CurrentVendore    
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        "CustomerCurrentMerchantId", vendorId.ToString(), currStoreId);

                    var vendor = _vendorService.GetAllVendors(storeId: currStoreId).FirstOrDefault();
                    if (vendor == null)
                    {
                        if (_workContext.CurrentCustomer.IsRegistered())
                            return Redirect("~/customer/info");
                        else
                            return Redirect("~/login");
                    }
                    return Redirect("~/homemerchant");
                }
                else
                    return View(model);
            }
        }

        public IActionResult GetMerchantWithGeo(string id, int ordtype, string enteredDeliveryAddress, bool getOfferMerchant = false)
        {
            _workContext.EnteredDeliveryAddress = enteredDeliveryAddress;
            var orderDetails = _workContext.CurrentOrderDetails;
            orderDetails.EnteredLocation = enteredDeliveryAddress;
            _workContext.CurrentOrderDetails = orderDetails;
            return ViewComponent("HomepageMerchants", new { latlong = id, ordertype = ordtype, getOfferMerchant = getOfferMerchant });
        }

        [HttpPost]
        public void UpdateSearchCoordinates(string latlong)
        {
            //save searched latlng in generic attribute to use further for distance calulation on merchant page.
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, NopNbDomainDefaults.SearchedLocationLatLongAttribute, latlong, _workContext.GetCurrentStoreId);
        }

        #region Custom Code by KP
        public void ClearEnteredDeliveryAddress()
        {
            _workContext.EnteredDeliveryAddress = string.Empty;
            //Custom code by KP 23Apr19
            var orderDetails = _workContext.CurrentOrderDetails;
            orderDetails.EnteredLocation = string.Empty;
            _workContext.CurrentOrderDetails = orderDetails;
        }
        //Custom code by KP 23Apr19
        public IActionResult SetOrderType(int orderType)
        {
            var orderDetails = _workContext.CurrentOrderDetails;
            orderDetails.OrderType = orderType;
            _workContext.CurrentOrderDetails = orderDetails;
            return Json(new { success = true });
        }
        //Custom Code By Mahendra 25-05-2019
        public IActionResult SetOrderTypeWithTime(int orderType, int orderTime)
        {
            var orderDetails = _workContext.CurrentOrderDetails;
            orderDetails.OrderType = orderType;
            orderDetails.OrderTime = orderTime;
            _workContext.CurrentOrderDetails = orderDetails;
            return Json(new { success = true });
        }
        public IActionResult SetOrderScheduleDateTime(string scheduleDate, string scheduleTime)
        {
            var orderDetails = _workContext.CurrentOrderDetails;
            DateTime dateTimetoConvert = DateTime.ParseExact(scheduleTime,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanFrom = dateTimetoConvert.TimeOfDay;
            orderDetails.ScheduleDate = DateTime.Parse(scheduleDate);
            orderDetails.ScheduleTime = spanFrom;
            _workContext.CurrentOrderDetails = orderDetails;
            return Json(new { success = true });
        }

        public IActionResult SetOrderComments(string orderComments)
        {
            var orderDetails = _workContext.CurrentOrderDetails;
            orderDetails.OrderNote = orderComments;
            _workContext.CurrentOrderDetails = orderDetails;
            return Json(new { success = true });
        }

        public IActionResult GetMerchantCategorysWithGeo(string latlong, int ordType, bool skipLocationSearch)
        {
            var orderDetails = _workContext.CurrentOrderDetails;
            return ViewComponent("HomepageMerchantCategorys", new { latlong = latlong, ordertype = ordType, skipLocationSearch });
        }

        [HttpPost]
        public virtual IActionResult SendAppLinkByEmail(string emailId)
        {
            try
            {
                var isCorrectEmail = false;
                if (!string.IsNullOrEmpty(emailId))
                {
                    string[] emailArray = emailId.Split('.');
                    var count = emailArray.Count();
                    isCorrectEmail = _localizationService.ValidateEmailExtension("." + emailArray[count - 1]);
                }

                if (isCorrectEmail)
                {
                    _workflowMessageService.SendAppLinkByEmail(emailId, _workContext.CurrentCustomer, _localizationSettings.DefaultAdminLanguageId);

                    return Json(new { Result = true, Message = _localizationService.GetResource("NB.Email.ForAppLink.Sent.Successfully") });
                }
                else
                {
                    return Json(new { Result = false, Message = _localizationService.GetResource("NB.Email.ForAppLink.Error") });
                }
            }
            catch (Exception exc)
            {
                return Json(new { Result = false, Message = exc.Message });
            }
        }
        #endregion

        #region Nivoslider pictures

        /// <summary>
        /// Get nivoslider view with pictures which is mapped along with geofancy location
        /// </summary>
        /// <param name="latlong"></param>
        /// <returns></returns>
        public virtual string GetSliderPicturesWithGeo(string latlong)
        {
            if (string.IsNullOrEmpty(latlong))
                return null;

            try
            {
                var geoPicturesSettingIds = _commonService.GetAllSliderPictureSettingIdsByLongLat(latlong, _workContext.GetCurrentStoreId);
                if (geoPicturesSettingIds == null || !geoPicturesSettingIds.Any())
                    return null;
                var model = new SliderPictureModel()
                {
                    GetPicturesByGeofacing = true,
                    GeofaciningSettingIds = geoPicturesSettingIds.ToList()
                };
                var viewPath = "~/Themes/" + _themeContext.WorkingThemeName.ToString() + "/Views/Home/_SliderPictures.cshtml";
                return RenderPartialViewToString(viewPath, model);
            }
            catch (Exception)
            {
                return null;
            }
        }

        #endregion
    }
}