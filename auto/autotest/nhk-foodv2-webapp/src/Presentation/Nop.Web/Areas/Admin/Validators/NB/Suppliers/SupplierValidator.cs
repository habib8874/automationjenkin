﻿using FluentValidation;
using Nop.Core.Domain.NB.Suppliers;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Services.NB.Suppliers;
using Nop.Web.Areas.Admin.Models.NB.Suppliers;
using Nop.Web.Framework.Validators;

namespace Nop.Web.Areas.Admin.Validators.NB.Suppliers
{
    public class SupplierValidator : BaseNopValidator<SupplierModel>
    {
        public SupplierValidator(ILocalizationService localizationService, IDbContext dbContext, ISupplierService supplierService)
        {
            RuleFor(x => x.Geofancing).Must((x, context) =>
            {
                if (string.IsNullOrEmpty(x.Geofancing))
                    return true;

                return supplierService.CheckGeofancingFormat(x.Geofancing);


            }).WithMessage(localizationService.GetResource("Admin.Suppliers.Fields.Geofancing.IncorrectFormat"));
            RuleFor(x => x.SupplierName).NotEmpty().WithMessage(localizationService.GetResource("Admin.Suppliers.Fields.SupplierName.Required"));
            RuleFor(x => x.Description).NotEmpty().WithMessage(localizationService.GetResource("Admin.Suppliers.Fields.Description.Required"));
          
            SetDatabaseValidationRules<Supplier>(dbContext);
        }
    }

}
