﻿namespace Nestorbird.Plugin.Api.ResponseModel
{
    public class BaseResponse<T>
    {
        public BaseResponse()
        {

        }

        public bool Success { get; set; }

        public string Message { get; set; }

        public T Data { get; set; }
    }
}
