using System.Collections.Generic;
using Nop.Core.Domain.Catalog;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Product tag service interface
    /// </summary>
    public partial interface IProductCuisineMappingService 
    {
        
        void DeleteProductCuisineMapping(ProductCuisine ProductCuisineMapping);

        IList<ProductCuisine> GetAllProductCuisineMapping();

        ProductCuisine GetProductCuisineMappingById(int ProductCuisineMappingId);

        void DeleteAllMappedProductCuisines(int productId); //by Mohini

        int InsertProductCuisineMapping(ProductCuisine ProductCuisineMapping);

        IList<ProductCuisine> GetProductCuisineMappingByStoreId(int StoreId);

        void UpdateProductCuisineMapping(ProductCuisine ProductCuisineMapping);
        int InsertProductCuisine_Mapping(ProductCuisineMapping ProductCuisineMapping);
        IList<ProductCuisineMapping> GetProductCuisine_MappingByProduct(int id);
        ProductCuisine GetProductCuisineMappingByName(string name);
        bool DeleteProductCuisine_Mapping(int Id);
    }
}
