﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core.Domain.NB.Package;

namespace Nop.Services.NB.Package
{
     public partial interface INBCustomerPackageService
    {

        #region packages

        /// <summary>
        /// Deletes a NBCustomerPackageMapping
        /// </summary>
        /// <param name="customerPackageMapping">customerPackageMapping</param>
        void DeleteCustomerPackageMapping(NBCustomerPackageMapping customerPackageMapping);

        /// <summary>
        /// Gets a NBCustomerPackageMapping
        /// </summary>
        /// <param name="customerId">The customerId identifier</param>
        /// <returns>NBCustomerPackageMapping</returns>
        NBCustomerPackageMapping GetPackageByCustomerId(int customerId);

        /// <summary>
        /// Inserts a NBCustomerPackageMapping
        /// </summary>
        /// <param name="customerPackageMapping">customerPackageMapping</param>
        void InsertCustomerPackageMapping(NBCustomerPackageMapping customerPackageMapping);

        /// <summary>
        /// Updates the NBCustomerPackageMapping
        /// </summary>
        /// <param name="package">Package</param>
        void UpdateCustomerPackageMapping(NBCustomerPackageMapping customerPackageMapping);


        #endregion

    }
}
