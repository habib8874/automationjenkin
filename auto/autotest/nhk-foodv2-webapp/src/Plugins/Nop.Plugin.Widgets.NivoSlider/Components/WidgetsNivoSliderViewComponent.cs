﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Plugin.Widgets.NivoSlider.Infrastructure.Cache;
using Nop.Plugin.Widgets.NivoSlider.Models;
using Nop.Services.Configuration;
using Nop.Services.Media;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Widgets.NivoSlider.Components
{
    [ViewComponent(Name = "WidgetsNivoSlider")]
    public class WidgetsNivoSliderViewComponent : NopViewComponent
    {
        private readonly IStoreContext _storeContext;
        private readonly IStaticCacheManager _cacheManager;
        private readonly ISettingService _settingService;
        private readonly IPictureService _pictureService;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;

        public WidgetsNivoSliderViewComponent(IStoreContext storeContext,
            IStaticCacheManager cacheManager,
            ISettingService settingService,
            IPictureService pictureService,
            IWebHelper webHelper,
            IWorkContext workContext)
        {
            _storeContext = storeContext;
            _cacheManager = cacheManager;
            _settingService = settingService;
            _pictureService = pictureService;
            _webHelper = webHelper;
            _workContext = workContext;
        }

        public IViewComponentResult Invoke(string widgetZone, object additionalData, bool getPicturesByGeofacing = false, IList<int> geofaciningSettingIds = null)
        {
            var nivoSliderSettings = _settingService.LoadSetting<NivoSliderSettings>(_storeContext.CurrentStore.Id);
            var model = new PublicInfoModel();

            if (getPicturesByGeofacing)
            {
                var geofancingPictureIds = new List<int>();
                var geoFancingSettings = _settingService.GetAllSettings().Where(s => geofaciningSettingIds.Contains(s.Id));
                if (geoFancingSettings != null && geofaciningSettingIds.Any())
                {
                    foreach (var setting in geoFancingSettings)
                    {
                        //get pictureid setting
                        var resultString = Regex.Match(setting.Name, @"\d+").Value;
                        var pictureNumber = Int32.Parse(resultString);
                        var pictureId = _settingService.GetSettingByKey("nivoslidersettings.picture" + pictureNumber + "id", 0, _workContext.GetCurrentStoreId, false);
                        if (pictureId > 0)
                            geofancingPictureIds.Add(pictureId);
                    }
                }

                model = new PublicInfoModel
                {
                    Picture1Url = geofancingPictureIds.Contains(nivoSliderSettings.Picture1Id) ? GetPictureUrl(nivoSliderSettings.Picture1Id) : string.Empty,
                    Text1 = nivoSliderSettings.Text1,
                    Link1 = nivoSliderSettings.Link1,
                    AltText1 = nivoSliderSettings.AltText1,

                    Picture2Url = geofancingPictureIds.Contains(nivoSliderSettings.Picture2Id) ? GetPictureUrl(nivoSliderSettings.Picture2Id) : string.Empty,
                    Text2 = nivoSliderSettings.Text2,
                    Link2 = nivoSliderSettings.Link2,
                    AltText2 = nivoSliderSettings.AltText2,

                    Picture3Url = geofancingPictureIds.Contains(nivoSliderSettings.Picture3Id) ? GetPictureUrl(nivoSliderSettings.Picture3Id) : string.Empty,
                    Text3 = nivoSliderSettings.Text3,
                    Link3 = nivoSliderSettings.Link3,
                    AltText3 = nivoSliderSettings.AltText3,

                    Picture4Url = geofancingPictureIds.Contains(nivoSliderSettings.Picture4Id) ? GetPictureUrl(nivoSliderSettings.Picture4Id) : string.Empty,
                    Text4 = nivoSliderSettings.Text4,
                    Link4 = nivoSliderSettings.Link4,
                    AltText4 = nivoSliderSettings.AltText4,

                    Picture5Url = geofancingPictureIds.Contains(nivoSliderSettings.Picture5Id) ? GetPictureUrl(nivoSliderSettings.Picture5Id) : string.Empty,
                    Text5 = nivoSliderSettings.Text5,
                    Link5 = nivoSliderSettings.Link5,
                    AltText5 = nivoSliderSettings.AltText5
                };
            }
            else
            {
                model = new PublicInfoModel
                {
                    Picture1Url = GetPictureUrl(nivoSliderSettings.Picture1Id),
                    Text1 = nivoSliderSettings.Text1,
                    Link1 = nivoSliderSettings.Link1,
                    AltText1 = nivoSliderSettings.AltText1,

                    Picture2Url = GetPictureUrl(nivoSliderSettings.Picture2Id),
                    Text2 = nivoSliderSettings.Text2,
                    Link2 = nivoSliderSettings.Link2,
                    AltText2 = nivoSliderSettings.AltText2,

                    Picture3Url = GetPictureUrl(nivoSliderSettings.Picture3Id),
                    Text3 = nivoSliderSettings.Text3,
                    Link3 = nivoSliderSettings.Link3,
                    AltText3 = nivoSliderSettings.AltText3,

                    Picture4Url = GetPictureUrl(nivoSliderSettings.Picture4Id),
                    Text4 = nivoSliderSettings.Text4,
                    Link4 = nivoSliderSettings.Link4,
                    AltText4 = nivoSliderSettings.AltText4,

                    Picture5Url = GetPictureUrl(nivoSliderSettings.Picture5Id),
                    Text5 = nivoSliderSettings.Text5,
                    Link5 = nivoSliderSettings.Link5,
                    AltText5 = nivoSliderSettings.AltText5
                };
            }

            if (string.IsNullOrEmpty(model.Picture1Url) && string.IsNullOrEmpty(model.Picture2Url) &&
                string.IsNullOrEmpty(model.Picture3Url) && string.IsNullOrEmpty(model.Picture4Url) &&
                string.IsNullOrEmpty(model.Picture5Url))
                //no pictures uploaded
                return Content("");

            return View("~/Plugins/Widgets.NivoSlider/Views/PublicInfo.cshtml", model);
        }

        protected string GetPictureUrl(int pictureId)
        {
            var cacheKey = string.Format(ModelCacheEventConsumer.PICTURE_URL_MODEL_KEY,
                pictureId, _webHelper.IsCurrentConnectionSecured() ? Uri.UriSchemeHttps : Uri.UriSchemeHttp);

            return _cacheManager.Get(cacheKey, () =>
            {
                //little hack here. nulls aren't cacheable so set it to ""
                var url = _pictureService.GetPictureUrl(pictureId, showDefaultPicture: false) ?? "";
                return url;
            });
        }
    }
}
