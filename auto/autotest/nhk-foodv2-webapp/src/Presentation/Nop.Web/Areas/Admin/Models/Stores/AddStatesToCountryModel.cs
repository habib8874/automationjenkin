﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Stores
{
    /// <summary>
    /// Represents a state model to add to the country
    /// </summary>
    public partial class AddStatesToCountryModel : BaseNopModel
    {
        #region Ctor

        public AddStatesToCountryModel()
        {
            SelectedProductIds = new List<int>();
        }
        #endregion

        #region Properties

        public int CategoryId { get; set; }
        public int StoreId { get; set; }

        public IList<int> SelectedProductIds { get; set; }

        #endregion
    }
}