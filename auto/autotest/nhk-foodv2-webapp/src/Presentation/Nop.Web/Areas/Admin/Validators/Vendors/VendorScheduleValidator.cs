﻿using FluentValidation;
using Nop.Web.Areas.Admin.Models.Vendors;
using Nop.Core.Domain.Vendors;
using Nop.Core.Infrastructure;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Areas.Admin.Validators.Common;
using Nop.Web.Framework.Validators;

namespace Nop.Web.Areas.Admin.Validators.Vendors
{
    public partial class VendorScheduleValidator : BaseNopValidator<VendorModel.VendorScheduleModel>
    {
        public VendorScheduleValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            var addressValidator = (AddressValidator)EngineContext.Current.ResolveUnregistered(typeof(AddressValidator));

            RuleFor(x => x.ScheduleName).NotEmpty().WithMessage(localizationService.GetResource("Admin.Vendors.Fields.Name.ScheduleName"));

            RuleFor(x => x.ScheduleFromTime).NotEmpty().WithMessage(localizationService.GetResource("Admin.Vendors.Fields.Name.ScheduleFromTime"));
            RuleFor(x => x.ScheduleToTime).NotEmpty().WithMessage(localizationService.GetResource("Admin.Vendors.Fields.Name.ScheduleToTime"));

            SetDatabaseValidationRules<Vendor>(dbContext);
        }
    }
}