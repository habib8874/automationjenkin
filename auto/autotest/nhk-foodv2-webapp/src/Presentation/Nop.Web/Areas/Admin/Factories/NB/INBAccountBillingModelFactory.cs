﻿using Nop.Web.Areas.Admin.Models.NB.AccountsBilling;

namespace Nop.Web.Areas.Admin.Factories.NB
{
    public partial interface INBAccountBillingModelFactory
    {
        AccountTransactionListModel PrepareAccountTransactionListModel(AccountTransactionSearchModel searchModel);

        AccountBillingModel PrepareAccountBillingModel(StripeAccountDetailResponseModel stripeAccountDetailModel);

        AccountPackagePaymentUpdateModel PrepareAccountPackagePaymentUpdateModel(int customerId);

        bool UpdateCustomerPackagePlan(AccountPackagePaymentUpdateModel model);

        AccountPackagePaymentUpdateModel PreparePackagePaymentInfoModel(int packageId, int packagePlanId, int customerId);
    }
}
