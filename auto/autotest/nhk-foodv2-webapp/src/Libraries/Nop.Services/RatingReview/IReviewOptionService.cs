﻿using System.Collections.Generic;
using Nop.Core.Domain.RatingReview;

namespace Nop.Services.RatingReview
{
    /// <summary>
    /// ReviewOption service interface
    /// </summary>
    public interface IReviewOptionService
    {
        /// <summary>
        /// Gets a reviewOption
        /// </summary>
        /// <param name="reviewOptionId">The reviewOption identifier</param>
        /// <returns>ReviewOption</returns>
        ReviewOption GetReviewOptionById(int reviewOptionId);

        /// <summary>
        /// Gets all ReviewOption
        /// </summary>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <returns>ReviewOptions</returns>
        IList<ReviewOption> GetAllReviewOption();

        /// <summary>
        /// Inserts a reviewOption
        /// </summary>
        /// <param name="reviewOption">ReviewOption</param>
        void InsertReviewOption(ReviewOption reviewOption);

        /// <summary>
        /// Updates the reviewOption
        /// </summary>
        /// <param name="reviewOption">ReviewOption</param>
        void UpdateReviewOption(ReviewOption reviewOption);

        /// <summary>
        /// Deletes a reviewOption
        /// </summary>
        /// <param name="reviewOption">ReviewOption</param>
        void DeleteReviewOption(ReviewOption reviewOption);
    }
}
