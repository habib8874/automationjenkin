using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Vendors;
using Nop.Core.Domain.VendorsGallery;
using Nop.Core.Infrastructure;
using Nop.Services.Configuration;
using Nop.Services.Events;

namespace Nop.Services.VendorsGallery
{
    /// <summary>
    /// Vendor service
    /// </summary>
    public partial class VendorGalleryService : IVendorGalleryService
    {
        #region Fields

        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IRepository<VendorGallery> _vendorGalleryRepository;
        private readonly IRepository<VendorNote> _vendorNoteRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="vendorRepository">Vendor repository</param>
        /// <param name="vendorNoteRepository">Vendor note repository</param>
        /// <param name="eventPublisher">Event published</param>
        public VendorGalleryService(IRepository<Vendor> vendorRepository,
            IRepository<VendorNote> vendorNoteRepository,
            IEventPublisher eventPublisher,
            IRepository<VendorGallery> vendorGalleryRepository)
        {
            _vendorRepository = vendorRepository;
            _vendorNoteRepository = vendorNoteRepository;
            _eventPublisher = eventPublisher;
            _vendorGalleryRepository = vendorGalleryRepository;
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Gets a vendor by vendor identifier
        /// </summary>
        /// <param name="vendorId">Vendor identifier</param>
        /// <returns>Vendor</returns>
        public virtual VendorGallery GetVendorGalleryById(int vendorId)
        {
            if (vendorId == 0)
                return null;

            return _vendorGalleryRepository.GetById(vendorId);
        }

        /// <summary>
        /// Delete a vendor
        /// </summary>
        /// <param name="vendor">Vendor</param>
        public virtual void DeleteVendorGallery(VendorGallery vendor)
        {
            if (vendor == null)
                throw new ArgumentNullException(nameof(vendor));

            vendor.Deleted = true;
            UpdateVendorGallery(vendor);

            //event notification
            _eventPublisher.EntityDeleted(vendor);
        }

        /// <summary>
        /// Gets all vendors
        /// </summary>
        /// <param name="name">Vendor name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Vendors</returns>
        public virtual IPagedList<VendorGallery> GetAllVendorGallery(string name = "", int storeId = 0, int vendorId = 0,int GalleryTypeId=0,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {

            //var storeContext = EngineContext.Current.Resolve<IStoreContext>();
            //var settingService = EngineContext.Current.Resolve<ISettingService>();
            //int currentStoreId = storeContext.CurrentStore.Id;

            //var webHelper = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Core.IWebHelper>();
            //var storeUrl = webHelper.GetStoreLocation();

            
            var query = _vendorGalleryRepository.Table;
            if (!string.IsNullOrWhiteSpace(name))
                query = query.Where(v => v.GalleryCaption.Contains(name));
                query = query.Where(v => v.Deleted==false);
            query = query.Where(v => !v.Deleted);
            if(storeId>0)
            {
                query = query.Where(x => x.StoreId ==storeId);
            }
            if (vendorId > 0)
            {
                query = query.Where(x => x.VendorId == vendorId);
            }
            if (GalleryTypeId > 0)
            {
                query = query.Where(x => x.GalleryType == GalleryTypeId);
            }
            var vendors = new PagedList<VendorGallery>(query.ToList(), pageIndex, pageSize);
            return vendors;
        }


        /// <summary>
        /// Gets all vendors by store
        /// </summary>
        /// <param name="name">Vendor name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Vendors</returns>
        public virtual IPagedList<Vendor> GetAllVendorsByStore(List<int> storeids,string name = "",
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {

            //var storeContext = EngineContext.Current.Resolve<IStoreContext>();
            //var settingService = EngineContext.Current.Resolve<ISettingService>();
            //int currentStoreId = storeContext.CurrentStore.Id;

            //var webHelper = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Core.IWebHelper>();
            //var storeUrl = webHelper.GetStoreLocation();


            var query = _vendorRepository.Table;

            if (!string.IsNullOrWhiteSpace(name))
                query = query.Where(v => v.Name.Contains(name));
            if (!showHidden)
                query = query.Where(v => v.Active);
            query = query.Where(v => !v.Deleted);
            query = query.Where(x => storeids.Contains(x.StoreId));
            query = query.OrderBy(v => v.DisplayOrder).ThenBy(v => v.Name);


            var vendors = new PagedList<Vendor>(query, pageIndex, pageSize);
            return vendors;
        }

        /// <summary>
        /// Inserts a vendor Gallery
        /// </summary>
        /// <param name="vendor">Vendor</param>
        public virtual void InsertVendorGallery(VendorGallery vendorGallery)
        {
            if (vendorGallery == null)
                throw new ArgumentNullException(nameof(vendorGallery));
            try
            {
                _vendorGalleryRepository.Insert(vendorGallery);
                _eventPublisher.EntityInserted(vendorGallery);
            }
            catch (Exception ex)
            {

                throw;
            }
            

            //event notification
           
        }

        /// <summary>
        /// Updates the vendor
        /// </summary>
        /// <param name="vendor">Vendor</param>
        public virtual void UpdateVendorGallery(VendorGallery vendorGallery)
        {
            if (vendorGallery == null)
                throw new ArgumentNullException(nameof(vendorGallery));

            _vendorGalleryRepository.Update(vendorGallery);

            //event notification
            _eventPublisher.EntityUpdated(vendorGallery);
        }

        /// <summary>
        /// Gets a vendor note note
        /// </summary>
        /// <param name="vendorNoteId">The vendor note identifier</param>
        /// <returns>Vendor note</returns>
        public virtual VendorNote GetVendorNoteById(int vendorNoteId)
        {
            if (vendorNoteId == 0)
                return null;

            return _vendorNoteRepository.GetById(vendorNoteId);
        }

        /// <summary>
        /// Deletes a vendor note
        /// </summary>
        /// <param name="vendorNote">The vendor note</param>
        public virtual void DeleteVendorNote(VendorNote vendorNote)
        {
            if (vendorNote == null)
                throw new ArgumentNullException(nameof(vendorNote));

            _vendorNoteRepository.Delete(vendorNote);

            //event notification
            _eventPublisher.EntityDeleted(vendorNote);
        }

        #endregion
    }
}