namespace Nop.Core.Domain.RatingReview
{
    /// <summary>
    /// Represents an ReviewType
    /// </summary>
    public enum ReviewType
    {
        Agent= 1,
        Merchant = 2,
        Item = 3,
    }
}
