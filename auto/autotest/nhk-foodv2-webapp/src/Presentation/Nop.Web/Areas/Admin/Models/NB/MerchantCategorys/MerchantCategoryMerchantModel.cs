﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.MerchantCategorys
{
    /// <summary>
    /// Represents a Merchant Category Merchant model
    /// </summary>
    public partial class MerchantCategoryMerchantModel : BaseNopEntityModel
    {
        #region Properties

        public int MerchantCategoryId { get; set; }

        public int VendorId { get; set; }

        [NopResourceDisplayName("NB.Admin.MerchantCategoryMerchant.Fields.Product")]
        public string VendorName { get; set; }

        [NopResourceDisplayName("NB.Admin.MerchantCategoryMerchant.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        #endregion
    }
}