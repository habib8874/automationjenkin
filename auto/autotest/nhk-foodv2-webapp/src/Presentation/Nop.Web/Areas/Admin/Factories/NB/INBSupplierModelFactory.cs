﻿using Nop.Core.Domain.NB.Suppliers;
using Nop.Web.Areas.Admin.Models.NB.Suppliers;
using DataSourceRequest = Nop.Web.Areas.Admin.Models.NB.Suppliers.DataSourceRequest;

namespace Nop.Web.Areas.Admin.Factories
{
    public interface INbSupplierModelFactory
    {


        /// <summary>
        /// Prepare paged supplier list model
        /// </summary>
        /// <param name="searchModel">Supplier search model</param>
        SupplierListModel PrepareSupplierListModel(SupplierSearchModel searchModel);

        /// <summary>
        /// Prepare supplier model
        /// </summary>
        /// <param name="model">Supplier model</param>
        /// <param name="supplier">Supplier</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Supplier model</returns>
        SupplierModel PrepareSupplierModel(SupplierModel model, Supplier supplier, bool excludeProperties = false);

        /// <summary>
        /// Prepare supplier search model
        /// </summary>
        /// <param name="searchModel">Supplier search model</param>
        /// <returns>Supplier search model</returns>
        SupplierSearchModel PrepareSupplierSearchModel(SupplierSearchModel searchModel);

        /// <summary>
        /// Prepare Product bulk Edit 
        /// </summary>
        /// <param name="searchModel">ProductBulkEdit search model</param>
        /// <returns>Supplier search model</returns>
        SupplierProductListModel PrepareProductBulkEditListModel(DataSourceRequest searchModel);


        /// <summary>
        /// Prepare supplierproduct search model
        /// </summary>
        /// <param name="searchModel">SupplierProduct search model</param>
        /// <returns>supplierproduct search model</returns>
        SupplierProductSearchModel PrepareSupplierProductSearchModel(SupplierProductSearchModel searchModel, Supplier supplier);


        /// <summary>
        /// Prepare supplier to product search model
        /// </summary>
        /// <param name="searchModel">SupplierProduct search model</param>
        /// <returns>supplier to product search model</returns>
        AddProductToSupplierSearchModel PrepareAddProductToSupplierSearchModel(AddProductToSupplierSearchModel searchModel);


        /// <summary>
        /// Prepare paged supplierproduct list model
        /// </summary>
        /// <param name="searchModel">supplier search model</param>
        /// <returns>supplierproduct list model</returns>
        AddProductToSupplierListModel PrepareAddProductToSupplierListModel(AddProductToSupplierSearchModel searchModel);


    }
}