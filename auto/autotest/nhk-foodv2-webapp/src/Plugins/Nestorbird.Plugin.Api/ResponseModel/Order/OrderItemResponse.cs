﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nestorbird.Plugin.Api.ResponseModel.Order
{
    public class OrderItemResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Qty { get; set; }

        public string UnitTotalPrice { get; set; }

        public string TotalPrice { get; set; }

        public string Image { get; set; }
    }
}
