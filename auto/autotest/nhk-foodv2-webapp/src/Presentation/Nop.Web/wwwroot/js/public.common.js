﻿/*
** nopCommerce custom js functions
*/



function OpenWindow(query, w, h, scroll) {
  var l = (screen.width - w) / 2;
  var t = (screen.height - h) / 2;

  winprops = 'resizable=0, height=' + h + ',width=' + w + ',top=' + t + ',left=' + l + 'w';
  if (scroll) winprops += ',scrollbars=1';
  var f = window.open(query, "_blank", winprops);
}

function setLocation(url) {
  window.location.href = url;
}

function displayAjaxLoading(display) {
  if (display) {
    $('.ajax-loading-block-window').show();
  }
  else {
    $('.ajax-loading-block-window').hide('slow');
  }
}

function displayPopupNotification(message, messagetype, modal) {
  //types: success, error, warning
  var container;
  if (messagetype == 'success') {
    //success
    container = $('#dialog-notifications-success');
  }
  else if (messagetype == 'error') {
    //error
    container = $('#dialog-notifications-error');
  }
  else if (messagetype == 'warning') {
    //warning
    container = $('#dialog-notifications-warning');
  }
  else {
    //other
    container = $('#dialog-notifications-success');
  }

  //we do not encode displayed message
  var htmlcode = '';
  if ((typeof message) == 'string') {
    htmlcode = '<p>' + message + '</p>';
  } else {
    for (var i = 0; i < message.length; i++) {
      htmlcode = htmlcode + '<p>' + message[i] + '</p>';
    }
  }

  container.html(htmlcode);

  var isModal = (modal ? true : false);
  container.dialog({
    modal: isModal,
    width: 350
  });
}
function displayJoinedPopupNotifications(notes) {
  if (Object.keys(notes).length === 0) return;

  var container = $('#dialog-notifications-success');
  var htmlcode = document.createElement('div');

  for (var note in notes) {
    if (notes.hasOwnProperty(note)) {
      var messages = notes[note];

      for (var i = 0; i < messages.length; ++i) {
        var elem = document.createElement("div");
        elem.innerHTML = messages[i];
        elem.classList.add('popup-notification');
        elem.classList.add(note);

        htmlcode.append(elem);
      }
    }
  }

  container.html(htmlcode);
  container.dialog({
    width: 350,
    modal: true
  });
}
function displayPopupContentFromUrl(url, title, modal, width) {
  var isModal = (modal ? true : false);
  var targetWidth = (width ? width : 550);
  var maxHeight = $(window).height() - 20;

  $('<div></div>').load(url)
    .dialog({
      modal: isModal,
      width: targetWidth,
      maxHeight: maxHeight,
      title: title,
      close: function (event, ui) {
        $(this).dialog('destroy').remove();
      }
    });
}

function displayBarNotification(message, messagetype, timeout) {
  var notificationTimeout;

  var messages = typeof message === 'string' ? [message] : message;
  if (messages.length === 0)
    return;

  //types: success, error, warning
  var cssclass = ['success', 'error', 'warning'].indexOf(messagetype) !== -1 ? messagetype : 'success';

  //remove previous CSS classes and notifications
  $('#bar-notification')
    .removeClass('success')
    .removeClass('error')
    .removeClass('warning');
  $('.bar-notification').remove();

  //add new notifications
  var htmlcode = document.createElement('div');
  htmlcode.classList.add('bar-notification', cssclass);

  //add close button for notification
  var close = document.createElement('span');
  close.classList.add('close');
  close.setAttribute('title', document.getElementById('bar-notification').dataset.close);

  for (var i = 0; i < messages.length; i++) {
    var content = document.createElement('p');
    content.classList.add('content');
    content.innerHTML = messages[i];

    htmlcode.append(content);
  }

  htmlcode.append(close);

  $('#bar-notification')
    .append(htmlcode);

  $(htmlcode)
    .fadeIn('slow')
    .on('mouseenter', function () {
      clearTimeout(notificationTimeout);
    });

  //callback for notification removing
  var removeNoteItem = function () {
    htmlcode.remove();
  };

  $(close).on('click', function () {
    $(htmlcode).fadeOut('slow', removeNoteItem);
  });

  //timeout (if set)
  if (timeout > 0) {
    notificationTimeout = setTimeout(function () {
      $(htmlcode).fadeOut('slow', removeNoteItem);
    }, timeout);
  }
}

function htmlEncode(value) {
  return $('<div/>').text(value).html();
}

function htmlDecode(value) {
  return $('<div/>').html(value).text();
}


// CSRF (XSRF) security
function addAntiForgeryToken(data) {
  //if the object is undefined, create a new one.
  if (!data) {
    data = {};
  }
  //add token
  var tokenInput = $('input[name=__RequestVerificationToken]');
  if (tokenInput.length) {
    data.__RequestVerificationToken = tokenInput.val();
  }
  return data;
};

function validateEmail(txtEmail) {
  var a = document.getElementById(txtEmail).value;
  var filter = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{1,4}$/;
  if (filter.test(a)) {
    return true;
  }
  else {
    return false;
  }
}

function addToWhishList(url, id) {
  AjaxCart.addproducttocart_catalog(url)
  var $el = $("." + id);
  var pId = id.split('-')[1];
  $el.addClass('active');
  $(".wishlistlink_" + pId).attr("onclick", "deleteFromWishList(" + pId + ",'" + url + "')");
}

function deleteFromWishList(id, addWishListLink) {
  AjaxCart.deleteproductfromcart_shoppingcart(id, addWishListLink)
}

function addItemFromWishListToCart(url, id, vendorId) {
  if (vendorId > 0) {
    $.ajax({
      cache: false,
      url: '/HomeMerchant/SetMerchant?merchantid=' + vendorId,
      data: {},
      type: 'post',
      success: function (response) {        
        AjaxCart.addItemFromWishListToCart(url);
      },
      error: function () {
        alert('An error occurred');
      }
    });
  }
  return false;
}

//Add to favorite merchant 
function addRemoveMerchantToFavorite(merchantId, isMyacountPage) {
  displayAjaxLoading(true);
  $.ajax({
    type: "POST",
    cache: false,
    url: "/Vendor/AddRemoveMerchantToFavorite",
    data: { merchantId, isMyacountPage },
    success: function (response) {
      if (response) {
        var element = $(".heart-" + merchantId);
        if (element.length != 0) {
          if (element.hasClass("active")) {
            element.removeClass('active');
          } else {
            element.addClass('active');
          }
        }

        //myaccount favorite merchants
        if (isMyacountPage) {
          if (response.myaccountFavoriteMerchants != '') {
            var favMerchant = $(".favoriteMerchantBlock");
            if (favMerchant.length > 0) {
              //update merchant list
              favMerchant.html(response.myaccountFavoriteMerchants);
            }
          }
        }

        if (response.message) {
          //display notification
          if (response.success == true) {
            //success
            displayBarNotification(response.message, 'success', 3500);
          }
          else {
            //error
            displayPopupNotification(response.message, 'error', true);
          }
        }

      }
    },
    error: function () {
      alert('Failed to add the merchant. Please refresh the page and try one more time.');
    },
    complete: function () { displayAjaxLoading(false) },
  });
}