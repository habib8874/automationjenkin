﻿using NHKCustomerApplication.Models;
using NHKCustomerApplication.ViewModels;
using NHKCustomerApplication.Utilities;

namespace NHKCustomerApplication.Services
{
	public interface IOrderServices
	{
		/// <summary>
		/// Get AgentOrderGeoLocation entity
		/// </summary>
		/// <param name="agentId"></param>
		/// <param name="orderId"></param>
		/// <returns>AgentOrderGeoLocation</returns>
		AgentOrderGeoLocation GetAgentOrderGeoLocation(int agentId, int orderId);

		/// <summary>
		/// Prepare LiveTrackingModel
		/// </summary>
		/// <param name="orderDetailmodel"></param>
		/// <param name="storeId"></param>
		/// <param name="languageId"></param>
		/// <returns>LiveTrackingModel</returns>
		LiveTrackingModel PrepareLiveTrackingModel(OrderTrackingModelV1 orderDetailmodel, int storeId, int languageId);

		/// <summary>
		/// Get order status mini description
		/// </summary>
		/// <param name="orderStatus"></param>
		/// <param name="orderReviewModel"></param>
		/// <param name="languageId"></param>
		/// <param name="vendor"></param>
		/// <param name="agent"></param>
		/// <returns></returns>
		string GetStatusMiniDescription(int orderStatusId, int storeId, int languageId, Vendor vendor = null, int agentCustomerId = 0);

		/// <summary>
		/// Get rating values
		/// </summary>
		/// <param name="storeId"></param>
		/// <param name="entityId"></param>
		/// <param name="reviewType"></param>
		/// <returns></returns>
		decimal GetAverageRating(int storeId, int entityId, int reviewType, out int reviewCount);
	}
}
