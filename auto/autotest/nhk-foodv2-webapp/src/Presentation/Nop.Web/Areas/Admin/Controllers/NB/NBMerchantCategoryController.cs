﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.NB.MerchantCategorys;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.NB.MerchantCategorys;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.NB.MerchantCategorys;
using Nop.Web.Areas.Admin.Models.Vendors;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.Extensions;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers.NB
{
    public partial class NBMerchantCategoryController : BaseAdminController
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IWorkContext _workContext;
        private readonly IMerchantCategoryService _merchantCategoryService;
        private readonly IPictureService _pictureService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly IStoreService _storeService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly ILocalizedModelFactory _localizedModelFactory;
        private readonly IVendorService _vendorService;

        #endregion

        #region Ctor

        public NBMerchantCategoryController(ILocalizationService localizationService,
            INotificationService notificationService,
            IPermissionService permissionService,
            IWorkContext workContext,
            IMerchantCategoryService merchantCategoryService,
            IPictureService pictureService,
            IBaseAdminModelFactory baseAdminModelFactory,
            IStoreService storeService,
            ILocalizedEntityService localizedEntityService,
            ILocalizedModelFactory localizedModelFactory,
            IVendorService vendorService)
        {
            _localizationService = localizationService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _workContext = workContext;
            _merchantCategoryService = merchantCategoryService;
            _pictureService = pictureService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _storeService = storeService;
            _localizedEntityService = localizedEntityService;
            _localizedModelFactory = localizedModelFactory;
            _vendorService = vendorService;
        }

        #endregion

        #region Methods
        #region Merchant Category

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchantCategorys))
                return AccessDeniedView();

            return View(new MerchantCategorySearchModel());
        }

        /// <summary>
        /// Functionality to get MerchantCategorys list
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual IActionResult MerchantCategorysList(MerchantCategorySearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchantCategorys))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = PrepareMerchantCategoryListModel(searchModel);

            return Json(model);
        }

        /// <summary>
        /// Functionality to create a new MerchantCategory
        /// </summary>
        /// <returns></returns>
        public virtual IActionResult CreateMerchantCategory()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchantCategorys))
                return AccessDeniedView();

            //prepare model
            var model = new MerchantCategoryModel();

            //define localized model configuration action
            Action<MerchantCategoryLocalizedModel, int> localizedModelConfiguration = null;
            model.Locales = _localizedModelFactory.PrepareLocalizedModels(localizedModelConfiguration);

            //prepare available stores
            _baseAdminModelFactory.PrepareStores(model.AvailableStores, false);

            return View(model);
        }

        /// <summary>
        /// Functionality to save a newly created MerchantCategory
        /// </summary>
        /// <param name="model"></param>
        /// <param name="continueEditing"></param>
        /// <returns></returns>
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult CreateMerchantCategory(MerchantCategoryModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchantCategorys))
                return AccessDeniedView();


            if (!ModelState.IsValid)
            {
                _notificationService.ErrorNotification(_localizationService.GetResource("NB.Admin.MerchantCategory.Empty.Error"));
                return RedirectToAction("CreateMerchantCategory");
            }
            var merchantCategory = new MerchantCategory
            {
                Name = model.Name,
                ShortDescription = model.ShortDescription ?? "",
                PictureId = model.PictureId,
                Published = model.Published,
                Deleted = model.Deleted,
                DisplayOrder = model.DisplayOrder,
                StoreId = model.StoreId,
                CreatedOnUtc = DateTime.UtcNow
            };
            _merchantCategoryService.InsertMerchantCategory(merchantCategory);

            //locales
            UpdateLocales(merchantCategory, model);

            _notificationService.SuccessNotification(_localizationService.GetResource("NB.Admin.MerchantCategory.Added.Succesfully"));
            if (!continueEditing)
            {
                return RedirectToAction("List");
            }

            return RedirectToAction("EditMerchantCategory", new { id = merchantCategory.Id });
        }

        /// <summary>
        /// Functionality to edit a MerchantCategory
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual IActionResult EditMerchantCategory(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchantCategorys))
                return AccessDeniedView();

            var merchantCategory = _merchantCategoryService.GetMerchantCategoryById(id);
            if (merchantCategory == null)
            {
                throw new Exception("No merchantcategory found with the specified id");
            }

            Action<MerchantCategoryLocalizedModel, int> localizedModelConfiguration = null;

            var model = new MerchantCategoryModel
            {
                Id = merchantCategory.Id,
                Name = merchantCategory.Name,
                ShortDescription = merchantCategory.ShortDescription ?? "",
                PictureId = merchantCategory.PictureId,
                Published = merchantCategory.Published,
                DisplayOrder = merchantCategory.DisplayOrder,
                StoreId = merchantCategory.StoreId
            };

            //prepare nested search model
            PrepareMerchantCategoryMerchantSearchModel(model.MerchantCategoryMerchantSearchModel, merchantCategory);

            //define localized model configuration action
            localizedModelConfiguration = (locale, languageId) =>
            {
                locale.Name = _localizationService.GetLocalized(merchantCategory, entity => entity.Name, languageId, false, false);
                locale.ShortDescription = _localizationService.GetLocalized(merchantCategory, entity => entity.ShortDescription, languageId, false, false);
            };

            model.Locales = _localizedModelFactory.PrepareLocalizedModels(localizedModelConfiguration);

            //prepare available stores
            _baseAdminModelFactory.PrepareStores(model.AvailableStores, false);

            return View(model);
        }

        /// <summary>
        /// Functionality to edit a MerchantCategory
        /// </summary>
        /// <param name="model"></param>
        /// <param name="continueEditing"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult EditMerchantCategory(MerchantCategoryModel model, bool continueEditing, IFormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchantCategorys))
                return AccessDeniedView();

            var merchantCategory = _merchantCategoryService.GetMerchantCategoryById(model.Id);

            if (merchantCategory == null)
            {
                throw new Exception("No merchantcategory found with the specified id");
            }

            if (!ModelState.IsValid)
            {
                _notificationService.ErrorNotification(_localizationService.GetResource("NB.Admin.MerchantCategory.Empty.Error"));
                return RedirectToAction("EditMerchantCategory", new { id = model.Id });
            }

            merchantCategory.Name = model.Name;
            merchantCategory.ShortDescription = model.ShortDescription ?? "";
            merchantCategory.PictureId = model.PictureId;
            merchantCategory.Published = model.Published;
            merchantCategory.DisplayOrder = model.DisplayOrder;
            merchantCategory.StoreId = model.StoreId;
            merchantCategory.UpdatedOnUtc = DateTime.UtcNow;

            _merchantCategoryService.UpdateMerchantCategory(merchantCategory);

            //locales
            UpdateLocales(merchantCategory, model);

            _notificationService.SuccessNotification(_localizationService.GetResource("NB.Admin.MerchantCategory.Updated.Succesfully"));
            if (!continueEditing)
            {
                return RedirectToAction("List");
            }

            return RedirectToAction("EditMerchantCategory", new { id = model.Id });
        }

        /// <summary>
        /// Functionality to delete the MerchantCategory
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual IActionResult DeleteMerchantCategory(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchantCategorys))
                return AccessDeniedView();

            var merchantCategory = _merchantCategoryService.GetMerchantCategoryById(id);
            merchantCategory.Deleted = true;
            _merchantCategoryService.UpdateMerchantCategory(merchantCategory);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult DeleteSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchantCategorys))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                var merchantCategories = new List<MerchantCategory>();
                foreach (var id in selectedIds)
                {
                    var category = _merchantCategoryService.GetMerchantCategoryById(id);
                    if (category != null)
                    {
                        category.Deleted = true;
                        _merchantCategoryService.UpdateMerchantCategory(category);
                    }

                }
            }

            return Json(new { Result = true });
        }

        /// <summary>
        /// Functionality to prepare MerchantCategory list model
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        public virtual MerchantCategoryListModel PrepareMerchantCategoryListModel(MerchantCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var subAdminAccosiatedvendorIds = new List<int>();
            //a subadmin should have access only to his accosiated vendors products
            if (_workContext.SubAdmin.IsSubAdminRole)
                subAdminAccosiatedvendorIds = _workContext.SubAdmin.AssociatedVenorIds?.ToList();

            //get merchantCategorys
            var merchantCategorys = _merchantCategoryService.GetAllMerchantCategory(_workContext.IsAdminRole ? 0 : _workContext.GetCurrentStoreId, searchName: searchModel.Name, subAdminAccosiatedvendorIds).ToPagedList(searchModel);

            var allStores = _storeService.GetAllStores();

            //prepare grid model
            var model = new MerchantCategoryListModel().PrepareToGrid(searchModel, merchantCategorys, () =>
            {
                return merchantCategorys.Select(merchantCategory =>
                {
                    //fill in model values from the entity
                    var merchantCategorysModel = new MerchantCategoryModel
                    {
                        Id = merchantCategory.Id,
                        Name = merchantCategory.Name,
                        DisplayOrder = merchantCategory.DisplayOrder,
                        Published = merchantCategory.Published
                    };

                    var store = allStores.Where(x => x.Id == merchantCategory.StoreId).FirstOrDefault();
                    merchantCategorysModel.StoreName = store == null ? string.Empty : store.Name;

                    if (merchantCategory.PictureId > 0)
                    {
                        //picture
                        var defaultProductPicture = _pictureService.GetPictureById(merchantCategory.PictureId);
                        var url = _pictureService.GetPictureUrl(defaultProductPicture, 75, true);
                        merchantCategorysModel.PictureThumbnailUrl = url;
                    }
                    if (string.IsNullOrEmpty(merchantCategorysModel.PictureThumbnailUrl))
                    {
                        merchantCategorysModel.PictureThumbnailUrl = "/images/thumbs/default-image_100.png";
                    }

                    return merchantCategorysModel;
                });
            });

            return model;
        }

        protected virtual void UpdateLocales(MerchantCategory merchantCategory, MerchantCategoryModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(merchantCategory,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(merchantCategory,
                    x => x.ShortDescription,
                    localized.ShortDescription,
                    localized.LanguageId);
            }
        }

        /// <summary>
        /// Prepare category product search model
        /// </summary>
        /// <param name="searchModel">Category product search model</param>
        /// <param name="category">Category</param>
        /// <returns>Category product search model</returns>
        protected virtual MerchantCategoryMerchantSearchModel PrepareMerchantCategoryMerchantSearchModel(MerchantCategoryMerchantSearchModel searchModel, MerchantCategory merchantCategory)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (merchantCategory == null)
                throw new ArgumentNullException(nameof(merchantCategory));

            searchModel.MerchantCategoryId = merchantCategory.Id;
            searchModel.StoreId = merchantCategory.StoreId;

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        [HttpPost]
        public virtual IActionResult MerchantCategoryMerchantList(MerchantCategoryMerchantSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchantCategorys))
                return AccessDeniedDataTablesJson();

            //try to get a category with the specified id
            var merchantCategory = _merchantCategoryService.GetMerchantCategoryById(searchModel.MerchantCategoryId)
                ?? throw new ArgumentException("No merchantcategory found with the specified id");

            //prepare model
            var model = PrepareMerchantCategoryMerchantListModel(searchModel, merchantCategory);

            return Json(model);
        }

        /// <summary>
        /// Prepare paged Merchant Category merchant list model
        /// </summary>
        /// <param name="searchModel">Merchant Category merchant search model</param>
        /// <param name="merchantCategory">MerchantCategory</param>
        /// <returns>Merchant Category merchant list model</returns>
        public virtual MerchantCategoryMerchantListModel PrepareMerchantCategoryMerchantListModel(MerchantCategoryMerchantSearchModel searchModel, MerchantCategory merchantCategory)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (merchantCategory == null)
                throw new ArgumentNullException(nameof(merchantCategory));

            //get merchantCategoryMerchants
            var merchantCategoryMerchants = _merchantCategoryService.GetMerchantCategoryMerchantsByMerchantCategoryId(merchantCategory.Id).ToPagedList(searchModel);

            //prepare grid model
            var model = new MerchantCategoryMerchantListModel().PrepareToGrid(searchModel, merchantCategoryMerchants, () =>
            {
                return merchantCategoryMerchants.Select(merchantCategoryMerchant =>
                {
                    //fill in model values from the entity
                    var merchantCategoryMerchantModel = new MerchantCategoryMerchantModel
                    {
                        Id = merchantCategoryMerchant.Id,
                        MerchantCategoryId = merchantCategoryMerchant.MerchantCategoryId,
                        VendorId = merchantCategoryMerchant.VendorId,
                        DisplayOrder = merchantCategory.DisplayOrder
                    };

                    //fill in additional values (not existing in the entity)
                    merchantCategoryMerchantModel.VendorName = _vendorService.GetVendorById(merchantCategoryMerchant.VendorId)?.Name;

                    return merchantCategoryMerchantModel;
                });
            });

            return model;
        }

        public virtual IActionResult MerchantCategoryMerchantDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchantCategorys))
                return AccessDeniedView();

            //try to get a merchant category mapping with the specified id
            var merchantCategoryMapping = _merchantCategoryService.GetMerchantCategoryMappingById(id)
                ?? throw new ArgumentException("No merchant category mapping found with the specified id", nameof(id));

            _merchantCategoryService.DeleteMerchantCategoryMapping(merchantCategoryMapping);

            return new NullJsonResult();
        }

        public virtual IActionResult MerchantCategoryMerchantAddPopup(int merchantCategoryId, int storeId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchantCategorys))
                return AccessDeniedView();

            //prepare model
            var model = PrepareAddMerchantToMerchantCategorySearchModel(new AddMerchantToMerchantCategorySearchModel());
            model.StoreId = storeId;

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult MerchantAddPopupList(AddMerchantToMerchantCategorySearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchantCategorys))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = PrepareAddMerchantToMerchantCategoryListModel(searchModel);

            return Json(model);
        }

        /// <summary>
        /// Prepare paged Merchant list model to add to the Merchant category
        /// </summary>
        /// <param name="searchModel">Merchant search model to add to the Merchant category</param>
        /// <returns>Merchant list model to add to the Merchant category</returns>
        public virtual AddMerchantToMerchantCategoryListModel PrepareAddMerchantToMerchantCategoryListModel(AddMerchantToMerchantCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get vendors
            var stores = new List<int>
            {
                searchModel.StoreId
            };

            var subAdminVendorIds = new List<int>();
            if (_workContext.SubAdmin.IsSubAdminRole)
                subAdminVendorIds = _workContext.SubAdmin.AssociatedVenorIds?.ToList();
            var vendors = _vendorService.GetAllVendorsByStore(stores, subAdminVendorIds, searchModel.SearchMerchantName, searchModel.Page - 1, searchModel.PageSize, true);

            //prepare grid model
            var model = new AddMerchantToMerchantCategoryListModel().PrepareToGrid(searchModel, vendors, () =>
            {
                //fill in model values from the entity
                return vendors.Select(vendor =>
                {
                    var vendorModel = vendor.ToModel<VendorModel>();
                    return vendorModel;
                });
            });

            return model;
        }


        /// <summary>
        /// Prepare Merchant search model to add to the Merchant category
        /// </summary>
        /// <param name="searchModel">Merchant search model to add to the Merchant category</param>
        /// <returns>Merchant search model to add to the Merchant category</returns>
        public virtual AddMerchantToMerchantCategorySearchModel PrepareAddMerchantToMerchantCategorySearchModel(AddMerchantToMerchantCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();

            return searchModel;
        }


        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult MerchantCategoryMerchantAddPopup(AddMerchantToMerchantCategoryModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMerchantCategorys))
                return AccessDeniedView();

            //get selected merchants
            var selectedMerchants = _vendorService.GetVendorsByIds(model.SelectedMerchantIds.ToArray());
            if (selectedMerchants.Any())
            {
                var existingMerchantToMerchantCategories = _merchantCategoryService.GetMerchantCategoryMerchantsByMerchantCategoryId(model.MerchantCategoryId);
                foreach (var merchant in selectedMerchants)
                {
                    //whether merchant category with such parameters already exists
                    if (_merchantCategoryService.FindMerchantCategoryMapping(existingMerchantToMerchantCategories, merchant.Id, model.MerchantCategoryId) != null)
                        continue;

                    //insert the new merchant category mapping
                    _merchantCategoryService.InsertMerchantCategoryMapping(new MerchantCategoryMapping
                    {
                        MerchantCategoryId = model.MerchantCategoryId,
                        VendorId = merchant.Id
                    });
                }
            }

            ViewBag.RefreshPage = true;

            return View(new AddMerchantToMerchantCategorySearchModel());
        }
        #endregion
        #endregion
    }
}