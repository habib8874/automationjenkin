﻿using System;
using System.Linq;
using Nop.Core.Data;
using Nop.Core.Domain.NB.Package;

namespace Nop.Services.NB.Package
{
    public partial class NBCustomerPackageService : INBCustomerPackageService
    {
        #region Fields

        private readonly IRepository<NBCustomerPackageMapping> _customerPackageMappingRepositry;

        #endregion

        #region Ctor
        public NBCustomerPackageService(IRepository<NBCustomerPackageMapping> customerPackageMappingRepositry)
        {
            _customerPackageMappingRepositry = customerPackageMappingRepositry;
        }

        #endregion


        #region Methods

        #region Packages

        /// <summary>
        /// Deletes a NBCustomerPackageMapping
        /// </summary>
        /// <param name="customerPackageMapping">customerPackageMapping</param>
        public virtual void DeleteCustomerPackageMapping(NBCustomerPackageMapping customerPackageMapping)
        {
            if (customerPackageMapping == null)
                throw new ArgumentNullException(nameof(customerPackageMapping));

            _customerPackageMappingRepositry.Update(customerPackageMapping);

        }

        /// <summary>
        /// Gets a NBCustomerPackageMapping
        /// </summary>
        /// <param name="customerId">The customerId identifier</param>
        /// <returns>NBCustomerPackageMapping</returns>
        public virtual NBCustomerPackageMapping GetPackageByCustomerId(int customerId)
        {
            if (customerId == 0)
                return null;

            var query = (from c in _customerPackageMappingRepositry.Table
                         where c.CustomerId == customerId
                         orderby c.Id
                         select c).FirstOrDefault();

            return query;
        }

        /// <summary>
        /// Inserts a NBCustomerPackageMapping
        /// </summary>
        /// <param name="customerPackageMapping">customerPackageMapping</param>
        public virtual void InsertCustomerPackageMapping(NBCustomerPackageMapping customerPackageMapping)
        {
            if (customerPackageMapping == null)
                throw new ArgumentNullException(nameof(customerPackageMapping));

            _customerPackageMappingRepositry.Insert(customerPackageMapping);

        }

        /// <summary>
        /// Updates the package
        /// </summary>
        /// <param name="package">Package</param>
        public virtual void UpdateCustomerPackageMapping(NBCustomerPackageMapping customerPackageMapping)
        {
            if (customerPackageMapping == null)
                throw new ArgumentNullException(nameof(customerPackageMapping));

            _customerPackageMappingRepositry.Update(customerPackageMapping);

        }

        #endregion

        #endregion
    }
}
