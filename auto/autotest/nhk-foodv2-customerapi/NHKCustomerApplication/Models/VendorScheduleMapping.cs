﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class VendorScheduleMapping
    {
        public int Id { get; set; }
        public int VendorId { get; set; }
        public string ScheduleName { get; set; }
        public TimeSpan ScheduleFromTime { get; set; }
        public TimeSpan ScheduleToTime { get; set; }
        public bool Deleted { get; set; }
        public int DisplayOrder { get; set; }
        public int AvailableType { get; set; }
        public int AvailableOn { get; set; }

        public virtual Vendor Vendor { get; set; }
    }
}
