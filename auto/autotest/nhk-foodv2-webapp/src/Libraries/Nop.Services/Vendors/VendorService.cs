﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Vendors;
using Nop.Core.Html;
using Nop.Services.Events;
using Nop.Core.Infrastructure;
using Nop.Services.Configuration;
using System.Data;
using Nop.Data;
using Nop.Core.Domain.NB.Discount;
using Nop.Services.Common;

namespace Nop.Services.Vendors
{
    /// <summary>
    /// Vendor service
    /// </summary>
    public partial class VendorService : IVendorService
    {
        #region Fields

        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IRepository<VendorNote> _vendorNoteRepository;
        private readonly IRepository<VendorSchedule> _vendorScheduleRepository;
        private readonly IDbContext _dbContext;
        private readonly IDataProvider _dataProvider;
        private readonly IRepository<VendorSliderPicture> _vendorSliderPictureRepository;
        private readonly IRepository<DiscountVendorMapping> _discountVendorMappingrepository;
        private readonly ICommonService _commonService;
      

        #endregion

        #region Ctor

        public VendorService(IEventPublisher eventPublisher,
            IRepository<Vendor> vendorRepository,
            ICommonService commonService,
            
            IRepository<VendorNote> vendorNoteRepository,
            IRepository<VendorSchedule> vendorScheduleRepository,
            IDbContext dbContext, 
            IDataProvider dataProvider,
            IRepository<VendorSliderPicture> vendorSliderPictureRepository,
            IRepository<DiscountVendorMapping> discountVendorMappingrepository)
        {
            _eventPublisher = eventPublisher;
            _vendorRepository = vendorRepository;
            _vendorNoteRepository = vendorNoteRepository;
            _vendorScheduleRepository = vendorScheduleRepository;
            _dbContext = dbContext;
            _dataProvider = dataProvider;
            _vendorSliderPictureRepository = vendorSliderPictureRepository;
            _discountVendorMappingrepository = discountVendorMappingrepository;
            _commonService = commonService;
           
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a vendor by vendor identifier
        /// </summary>
        /// <param name="vendorId">Vendor identifier</param>
        /// <returns>Vendor</returns>
        public virtual Vendor GetVendorById(int vendorId)
        {
            if (vendorId == 0)
                return null;

            return _vendorRepository.GetById(vendorId);
        }

        /// <summary>
        /// Delete a vendor
        /// </summary>
        /// <param name="vendor">Vendor</param>
        public virtual void DeleteVendor(Vendor vendor)
        {
            if (vendor == null)
                throw new ArgumentNullException(nameof(vendor));

            vendor.Deleted = true;
            UpdateVendor(vendor);
           

            //event notification
            _eventPublisher.EntityDeleted(vendor);
        }

        /// <summary>
        /// Gets all vendors
        /// </summary>
        /// <param name="name">Vendor name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Vendors</returns>
        public virtual IPagedList<Vendor> GetAllVendors(string name = "", int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, int storeId = 0, bool getOfferMerchant= false)
        {
            var query = _vendorRepository.Table;
            if (!string.IsNullOrWhiteSpace(name))
                query = query.Where(v => v.Name.Contains(name));
            if (!showHidden)
                query = query.Where(v => v.Active);

            if (storeId > 0)
                query = query.Where(x => x.StoreId == storeId);

            query = query.Where(v => !v.Deleted);
            query = query.OrderBy(v => v.DisplayOrder).ThenBy(v => v.Name);

            if (getOfferMerchant)
                query = (from vendor in query
                         join vendorDiscount in _discountVendorMappingrepository.Table
                         on vendor.Id equals vendorDiscount.VendorId
                         select vendor);


            var vendors = new PagedList<Vendor>(query, pageIndex, pageSize);
            return vendors;
        }

        /// <summary>
        /// Gets vendors
        /// </summary>
        /// <param name="vendorIds">Vendor identifiers</param>
        /// <returns>Vendors</returns>
        public virtual IList<Vendor> GetVendorsByIds(int[] vendorIds)
        {
            var query = _vendorRepository.Table;
            if (vendorIds != null)
                query = query.Where(v => vendorIds.Contains(v.Id));

            return query.ToList();
        }

        /// <summary>
        /// Inserts a vendor
        /// </summary>
        /// <param name="vendor">Vendor</param>
        public virtual void InsertVendor(Vendor vendor)
        {
            if (vendor == null)
                throw new ArgumentNullException(nameof(vendor));

            _vendorRepository.Insert(vendor);
           _commonService.ClearCache(vendor.StoreId,string.Format( "API.Cache.Merchant-{0}", vendor.StoreId));
            

            //event notification
            _eventPublisher.EntityInserted(vendor);
        }

        /// <summary>
        /// Updates the vendor
        /// </summary>
        /// <param name="vendor">Vendor</param>
        public virtual void UpdateVendor(Vendor vendor)
        {
            if (vendor == null)
                throw new ArgumentNullException(nameof(vendor));

            _vendorRepository.Update(vendor);
            _commonService.ClearCache(vendor.StoreId, string.Format("API.Cache.Merchant-{0}", vendor.StoreId));

            //event notification
            _eventPublisher.EntityUpdated(vendor);
        }

        /// <summary>
        /// Gets a vendor note
        /// </summary>
        /// <param name="vendorNoteId">The vendor note identifier</param>
        /// <returns>Vendor note</returns>
        public virtual VendorNote GetVendorNoteById(int vendorNoteId)
        {
            if (vendorNoteId == 0)
                return null;

            return _vendorNoteRepository.GetById(vendorNoteId);
        }

        /// <summary>
        /// Deletes a vendor note
        /// </summary>
        /// <param name="vendorNote">The vendor note</param>
        public virtual void DeleteVendorNote(VendorNote vendorNote)
        {
            if (vendorNote == null)
                throw new ArgumentNullException(nameof(vendorNote));

            _vendorNoteRepository.Delete(vendorNote);

            //event notification
            _eventPublisher.EntityDeleted(vendorNote);
        }

        /// <summary>
        /// Formats the vendor note text
        /// </summary>
        /// <param name="vendorNote">Vendor note</param>
        /// <returns>Formatted text</returns>
        public virtual string FormatVendorNoteText(VendorNote vendorNote)
        {
            if (vendorNote == null)
                throw new ArgumentNullException(nameof(vendorNote));

            var text = vendorNote.Note;

            if (string.IsNullOrEmpty(text))
                return string.Empty;

            text = HtmlHelper.FormatText(text, false, true, false, false, false, false);

            return text;
        }

        #endregion

        #region Custom code from v4.0
        /// <summary>
        /// Gets all vendors by store
        /// </summary>
        /// <param name="name">Vendor name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Vendors</returns>
        public virtual IPagedList<Vendor> GetAllVendorsByStore(List<int> storeids, IList<int> vendorIds = null, string name = "",
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {

            //var storeContext = EngineContext.Current.Resolve<IStoreContext>();
            //var settingService = EngineContext.Current.Resolve<ISettingService>();
            //int currentStoreId = storeContext.CurrentStore.Id;

            //var webHelper = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Core.IWebHelper>();
            //var storeUrl = webHelper.GetStoreLocation();


            var query = _vendorRepository.Table;

            if (!string.IsNullOrWhiteSpace(name))
                query = query.Where(v => v.Name.Contains(name));
            if (!showHidden)
                query = query.Where(v => v.Active);
            query = query.Where(v => !v.Deleted);
            query = query.Where(x => storeids.Contains(x.StoreId));
            //filter by sub admin
            if (vendorIds != null && vendorIds.Any())
            {
                query = query.Where(v => vendorIds.Contains(v.Id));
            }
            query = query.OrderBy(v => v.DisplayOrder).ThenBy(v => v.Name);


            var vendors = new PagedList<Vendor>(query, pageIndex, pageSize);
            return vendors;
        }

        // <summary>
        /// Gets a schedule  by vendor identifier
        /// </summary>
        /// <param name="vendorId">The product identifier</param>
        /// <returns>Product pictures</returns>
        public virtual IList<VendorSchedule> GetVendorScheduleByScheduleId(int vendorId, int type)
        {
            var query = from pp in _vendorScheduleRepository.Table
                        where pp.VendorId == vendorId
                        && pp.AvailableType == type
                        orderby pp.DisplayOrder, pp.Id
                        select pp;
            var vendorSchedules = query.ToList();
            return vendorSchedules;
        }
        /// <summary>
        /// InsertVendorSchedule
        /// </summary>
        /// <param name="vendorSchedule">Vendor identifier</param>
        /// <returns>bool</returns>
        public bool InsertVendorSchedule(VendorSchedule vendorSchedule)
        {
            _vendorScheduleRepository.Insert(vendorSchedule);
            return true;
        }
        /// <summary>
        /// InsertVendorSchedule
        /// </summary>
        /// <param name="vendorSchedule">Vendor identifier</param>
        /// <returns>bool</returns>
        public bool UpdateVendorSchedule(VendorSchedule vendorSchedule)
        {
            _vendorScheduleRepository.Update(vendorSchedule);
            return true;
        }
        /// <summary>
        /// InsertVendorSchedule
        /// </summary>
        /// <param name="vendorSchedule">Vendor identifier</param>
        /// <returns>bool</returns>
        public VendorSchedule GetVendorScheduleById(int id)
        {
            var vendorSchedule = _vendorScheduleRepository.Table.Where(x => x.Id == id).FirstOrDefault();
            return vendorSchedule;
        }
        /// <summary>
        /// InsertVendorSchedule
        /// </summary>
        /// <param name="vendorSchedule">Vendor identifier</param>
        /// <returns>bool</returns>
        public bool DeleteVendorScheduleById(VendorSchedule vendorSchedule)
        {
            _vendorScheduleRepository.Delete(vendorSchedule);
            return true;
        }
        #endregion

        #region Custom code
        public virtual IList<Vendor> GetVendorsByStore(int storeid)
        {
            var query = _vendorRepository.Table.Where(x=>x.Active && x.IsOpen && !x.Deleted && x.StoreId== storeid);
            return query.ToList();
        }

        /// <summary>
        /// Check geofancing value
        /// </summary>
        /// <param name="geofancing"></param>
        /// <returns>bool</returns>
        public virtual bool CheckGeofancingFormat(string geofancing)
        {
            try
            {
                var pGeofancing = _dataProvider.GetParameter();
                pGeofancing.ParameterName = "Geofancing";
                pGeofancing.Value = geofancing;
                pGeofancing.DbType = DbType.String;

                _dbContext.ExecuteSqlCommand("EXEC Check_Geofancing @Geofancing", parameters: pGeofancing);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region Vendor slider pictures

        /// <summary>
        /// Deletes a Vendor slider picture
        /// </summary>
        /// <param name="vendorSliderPicture">Product picture</param>
        public virtual void DeleteVendorSliderPicture(VendorSliderPicture vendorSliderPicture)
        {
            if (vendorSliderPicture == null)
                throw new ArgumentNullException(nameof(vendorSliderPicture));

            _vendorSliderPictureRepository.Delete(vendorSliderPicture);
            var storeId = (from v in _vendorRepository.Table
                           where vendorSliderPicture.VendorId==v.Id
                           select v.StoreId).FirstOrDefault();

            _commonService.ClearCache(storeId, string.Format("API.Cache.VendorBanner-{0}", storeId));

            //event notification
            _eventPublisher.EntityDeleted(vendorSliderPicture);
        }

        /// <summary>
        /// Gets a Vendor slider pictures by vendor identifier
        /// </summary>
        /// <param name="vendorId">The vendor identifier</param>
        /// <returns>Vendor slider pictures</returns>
        public virtual IList<VendorSliderPicture> GetVendorSliderPicturesByVendorId(int vendorId)
        {
            var query = from pp in _vendorSliderPictureRepository.Table
                        where pp.VendorId == vendorId
                        orderby pp.DisplayOrder, pp.Id
                        select pp;
            var vendorSliderPictures = query.ToList();
            return vendorSliderPictures;
        }

        /// <summary>
        /// Gets a Vendor slider picture
        /// </summary>
        /// <param name="vendorSliderPictureId">Product picture identifier</param>
        /// <returns>Vendor slider picture</returns>
        public virtual VendorSliderPicture GetVendorSliderPictureById(int vendorSliderPictureId)
        {
            if (vendorSliderPictureId == 0)
                return null;

            return _vendorSliderPictureRepository.GetById(vendorSliderPictureId);
        }

        /// <summary>
        /// Inserts a Vendor slider picture
        /// </summary>
        /// <param name="vendorSliderPicture">Vendor slider picture</param>
        public virtual void InsertVendorSliderPicture(VendorSliderPicture vendorSliderPicture)
        {
            if (vendorSliderPicture == null)
                throw new ArgumentNullException(nameof(vendorSliderPicture));

            _vendorSliderPictureRepository.Insert(vendorSliderPicture);

            var storeId = (from v in _vendorRepository.Table
                           where vendorSliderPicture.VendorId == v.Id
                           select v.StoreId).FirstOrDefault();

            _commonService.ClearCache(storeId, string.Format("API.Cache.VendorBanner-{0}", storeId));

            //event notification
            _eventPublisher.EntityInserted(vendorSliderPicture);
        }

        /// <summary>
        /// Updates a Vendor slider picture
        /// </summary>
        /// <param name="vendorSliderPicture">Vendor slider picture</param>
        public virtual void UpdateVendorSliderPicture(VendorSliderPicture vendorSliderPicture)
        {
            if (vendorSliderPicture == null)
                throw new ArgumentNullException(nameof(vendorSliderPicture));

            _vendorSliderPictureRepository.Update(vendorSliderPicture);

            var storeId = (from v in _vendorRepository.Table
                           where vendorSliderPicture.VendorId == v.Id
                           select v.StoreId).FirstOrDefault();

            _commonService.ClearCache(storeId,string.Format("API.Cache.VendorBanner-{0}", storeId));
            //event notification
            _eventPublisher.EntityUpdated(vendorSliderPicture);
        }

        #endregion
    }
}