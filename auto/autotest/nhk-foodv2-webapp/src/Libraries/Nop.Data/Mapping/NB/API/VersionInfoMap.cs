﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB.API;

namespace Nop.Data.Mapping.NB.API
{
    /// <summary>
    /// Represents an address mapping configuration
    /// </summary>
    public partial class VersionInfoMap : NopEntityTypeConfiguration<VersionInfo>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<VersionInfo> builder)
        {
            builder.ToTable(nameof(VersionInfo));
            builder.HasKey(vs => vs.Id);

            base.Configure(builder);
        }

        #endregion
    }
}
