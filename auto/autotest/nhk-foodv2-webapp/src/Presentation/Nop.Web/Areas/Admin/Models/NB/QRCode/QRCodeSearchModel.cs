﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB
{
    /// <summary>
    /// Represents a QRCode search model
    /// </summary>
    public partial class QRCodeSearchModel : BaseSearchModel
    {
        public QRCodeSearchModel()
        {
            AvailableVendors = new List<SelectListItem>();            
        }

        #region Properties

        public int Id { get; set; }

        public IList<SelectListItem> AvailableVendors { get; set; }

        [NopResourceDisplayName("NB.Admin.Catalog.QRCode.List.SearchVendorId")]
        public int SearchVendorId { get; set; }

        #endregion
    }
}