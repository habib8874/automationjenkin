﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Services.Localization;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.Schedule
{
    public class ScheduleValidatorModel : BaseNopValidator<ScheduleListModel>
    {
        public ScheduleValidatorModel(ILocalizationService localizationService)
        {
            RuleFor(x => x.SearchStoreId).NotEmpty().WithMessage(localizationService.GetResource("Admin.Schedule.Validator.Store"));
            RuleFor(x => x.SearchVendorId).NotEmpty().WithMessage(localizationService.GetResource("Admin.Schedule.Validator.Vendor"));
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Schedule.Validator.Name"));
            RuleFor(x => x.FromStringTime).NotEmpty().WithMessage(localizationService.GetResource("Admin.Schedule.Validator.FromStringTime"));
            RuleFor(x => x.ToStringTime).NotEmpty().WithMessage(localizationService.GetResource("Admin.Schedule.Validator.ToStringTime"));
           // RuleFor(x => x.To).NotEmpty().WithMessage(localizationService.GetResource("Forum.TextCannotBeEmpty"));
            //RuleFor(x => x.TableNumber).NotEmpty().WithMessage(localizationService.GetResource("Forum.TextCannotBeEmpty"));
            //RuleFor(x => x.MobileNumber).NotEmpty().WithMessage(localizationService.GetResource("Booking.CustomerPhone.TextCannotBeEmpty"));
            //RuleFor(x => x.CustomerName).NotEmpty().WithMessage(localizationService.GetResource("Booking.CustomerName.TextCannotBeEmpty"));
            //RuleFor(x => x.Email)
            //     .NotEmpty()
            //     .WithMessage(localizationService.GetResource("Address.Fields.Email.Required"));
            //RuleFor(x => x.Email)
            //    .EmailAddress()
            //    .WithMessage(localizationService.GetResource("Common.WrongEmail"));


        }
    }
}