﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB
{
    /// <summary>
    /// Represents a QRCode list model
    /// </summary>
    public partial class QRCodeListModel : BasePagedListModel<QRCodeModel>
    {
    }
}