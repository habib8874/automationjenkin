﻿using FluentValidation;
using Nop.Core.Domain;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.Customer;

namespace Nop.Web.Validators.Customer
{
    public partial class PasswordRecoveryValidator : BaseNopValidator<PasswordRecoveryModel>
    {
        public PasswordRecoveryValidator(ILocalizationService localizationService, StoreInformationSettings storeSettings)
        {
            if (storeSettings.EnableOTPAuthentication)
            {
                RuleFor(x => x.EmailOrPhone).NotEmpty().WithMessage(localizationService.GetResource("Account.PasswordRecovery.EmailOrPhone.Required"));
            }
            else
            {
                RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Account.PasswordRecovery.Email.Required"));
                RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Common.WrongEmail"));
            }
        }
    }
}