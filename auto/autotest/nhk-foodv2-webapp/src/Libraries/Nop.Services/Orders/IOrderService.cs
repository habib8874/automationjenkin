﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Orders;

namespace Nop.Services.Orders
{
    /// <summary>
    /// Order service interface
    /// </summary>
    public partial interface IOrderService
    {
        #region Orders

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="orderId">The order identifier</param>
        /// <returns>Order</returns>
        Order GetOrderById(int orderId);

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="customOrderNumber">The custom order number</param>
        /// <returns>Order</returns>
        Order GetOrderByCustomOrderNumber(string customOrderNumber);

        /// <summary>
        /// Get orders by identifiers
        /// </summary>
        /// <param name="orderIds">Order identifiers</param>
        /// <returns>Order</returns>
        IList<Order> GetOrdersByIds(int[] orderIds);

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="orderGuid">The order identifier</param>
        /// <returns>Order</returns>
        Order GetOrderByGuid(Guid orderGuid);

        /// <summary>
        /// Deletes an order
        /// </summary>
        /// <param name="order">The order</param>
        void DeleteOrder(Order order);

        /// <summary>
        /// Search orders
        /// </summary>
        /// <param name="storeId">Store identifier; null to load all orders</param>
        /// <param name="vendorId">Vendor identifier; null to load all orders</param>
        /// <param name="customerId">Customer identifier; null to load all orders</param>
        /// <param name="productId">Product identifier which was purchased in an order; 0 to load all orders</param>
        /// <param name="affiliateId">Affiliate identifier; 0 to load all orders</param>
        /// <param name="billingCountryId">Billing country identifier; 0 to load all orders</param>
        /// <param name="warehouseId">Warehouse identifier, only orders with products from a specified warehouse will be loaded; 0 to load all orders</param>
        /// <param name="paymentMethodSystemName">Payment method system name; null to load all records</param>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="osIds">Order status identifiers; null to load all orders</param>
        /// <param name="psIds">Payment status identifiers; null to load all orders</param>
        /// <param name="ssIds">Shipping status identifiers; null to load all orders</param>
        /// <param name="billingPhone">Billing phone. Leave empty to load all records.</param>
        /// <param name="billingEmail">Billing email. Leave empty to load all records.</param>
        /// <param name="billingLastName">Billing last name. Leave empty to load all records.</param>
        /// <param name="orderNotes">Search in order notes. Leave empty to load all records.</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="getOnlyTotalCount">A value in indicating whether you want to load only total number of records. Set to "true" if you don't want to load data from database</param>
        /// <returns>Orders</returns>
        IPagedList<Order> SearchOrders(int storeId = 0,
            int vendorId = 0, IList<int> vendorIds = null, int customerId = 0,
            int productId = 0, int affiliateId = 0, int warehouseId = 0,
            int billingCountryId = 0, string paymentMethodSystemName = null,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            List<int> osIds = null, List<int> psIds = null, List<int> ssIds = null,
            string billingPhone = null, string billingEmail = null, string billingLastName = "",
            string orderNotes = null, int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false, int agentId = 0, string billingFullName = "", bool isForAgentEarning = false, bool isDeliverySlot = false, List<int> slotIds = null);

        /// <summary>
        /// Inserts an order
        /// </summary>
        /// <param name="order">Order</param>
        void InsertOrder(Order order);

        /// <summary>
        /// Updates the order
        /// </summary>
        /// <param name="order">The order</param>
        void UpdateOrder(Order order);

        /// <summary>
        /// Get an order by authorization transaction ID and payment method system name
        /// </summary>
        /// <param name="authorizationTransactionId">Authorization transaction ID</param>
        /// <param name="paymentMethodSystemName">Payment method system name</param>
        /// <returns>Order</returns>
        Order GetOrderByAuthorizationTransactionIdAndPaymentMethod(string authorizationTransactionId, string paymentMethodSystemName);

        /// <summary>
        /// Parse tax rates
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="taxRatesStr"></param>
        /// <returns>Rates</returns>
        SortedDictionary<decimal, decimal> ParseTaxRates(Order order, string taxRatesStr);

        /// <summary>
        /// Gets a value indicating whether an order has items to be added to a shipment
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether an order has items to be added to a shipment</returns>
        bool HasItemsToAddToShipment(Order order);

        /// <summary>
        /// Gets a value indicating whether an order has items to ship
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether an order has items to ship</returns>
        bool HasItemsToShip(Order order);

        /// <summary>
        /// Gets a value indicating whether an order has items to deliver
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether an order has items to deliver</returns>
        bool HasItemsToDeliver(Order order);

        #endregion

        #region Orders items

        /// <summary>
        /// Gets an order item
        /// </summary>
        /// <param name="orderItemId">Order item identifier</param>
        /// <returns>Order item</returns>
        OrderItem GetOrderItemById(int orderItemId);

        /// <summary>
        /// Gets an order item
        /// </summary>
        /// <param name="orderItemGuid">Order item identifier</param>
        /// <returns>Order item</returns>
        OrderItem GetOrderItemByGuid(Guid orderItemGuid);

        /// <summary>
        /// Gets all downloadable order items
        /// </summary>
        /// <param name="customerId">Customer identifier; null to load all records</param>
        /// <returns>Order items</returns>
        IList<OrderItem> GetDownloadableOrderItems(int customerId);

        /// <summary>
        /// Delete an order item
        /// </summary>
        /// <param name="orderItem">The order item</param>
        void DeleteOrderItem(OrderItem orderItem);

        /// <summary>
        /// Gets a total number of items in all shipments
        /// </summary>
        /// <param name="orderItem">Order item</param>
        /// <returns>Total number of items in all shipments</returns>
        int GetTotalNumberOfItemsInAllShipment(OrderItem orderItem);

        /// <summary>
        /// Gets a total number of already items which can be added to new shipments
        /// </summary>
        /// <param name="orderItem">Order item</param>
        /// <returns>Total number of already delivered items which can be added to new shipments</returns>
        int GetTotalNumberOfItemsCanBeAddedToShipment(OrderItem orderItem);

        /// <summary>
        /// Gets a total number of not yet shipped items (but added to shipments)
        /// </summary>
        /// <param name="orderItem">Order item</param>
        /// <returns>Total number of not yet shipped items (but added to shipments)</returns>
        int GetTotalNumberOfNotYetShippedItems(OrderItem orderItem);

        /// <summary>
        /// Gets a total number of already shipped items
        /// </summary>
        /// <param name="orderItem">Order item</param>
        /// <returns>Total number of already shipped items</returns>
        int GetTotalNumberOfShippedItems(OrderItem orderItem);

        /// <summary>
        /// Gets a total number of already delivered items
        /// </summary>
        /// <param name="orderItem">Order  item</param>
        /// <returns>Total number of already delivered items</returns>
        int GetTotalNumberOfDeliveredItems(OrderItem orderItem);

        /// <summary>
        /// Gets all order items by orderid
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns>Order items</returns>
        IList<OrderItem> GetOrderItemsByOrderId(int orderId);

        #endregion

        #region Order notes

        /// <summary>
        /// Gets an order note
        /// </summary>
        /// <param name="orderNoteId">The order note identifier</param>
        /// <returns>Order note</returns>
        OrderNote GetOrderNoteById(int orderNoteId);

        /// <summary>
        /// Deletes an order note
        /// </summary>
        /// <param name="orderNote">The order note</param>
        void DeleteOrderNote(OrderNote orderNote);

        /// <summary>
        /// Formats the order note text
        /// </summary>
        /// <param name="orderNote">Order note</param>
        /// <returns>Formatted text</returns>
        string FormatOrderNoteText(OrderNote orderNote);

        #endregion

        #region Recurring payments

        /// <summary>
        /// Deletes a recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        void DeleteRecurringPayment(RecurringPayment recurringPayment);

        /// <summary>
        /// Gets a recurring payment
        /// </summary>
        /// <param name="recurringPaymentId">The recurring payment identifier</param>
        /// <returns>Recurring payment</returns>
        RecurringPayment GetRecurringPaymentById(int recurringPaymentId);

        /// <summary>
        /// Inserts a recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        void InsertRecurringPayment(RecurringPayment recurringPayment);

        /// <summary>
        /// Updates the recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        void UpdateRecurringPayment(RecurringPayment recurringPayment);

        /// <summary>
        /// Search recurring payments
        /// </summary>
        /// <param name="storeId">The store identifier; 0 to load all records</param>
        /// <param name="customerId">The customer identifier; 0 to load all records</param>
        /// <param name="initialOrderId">The initial order identifier; 0 to load all records</param>
        /// <param name="initialOrderStatus">Initial order status identifier; null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Recurring payments</returns>
        IPagedList<RecurringPayment> SearchRecurringPayments(int storeId = 0,
            int customerId = 0, int initialOrderId = 0, OrderStatus? initialOrderStatus = null,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        #endregion

        #region Custom code from v4.0

        #region OrderVendorDetails

        /// <summary>
        /// Gets an order vendor details
        /// </summary>
        /// <param name="orderId">The order identifier</param>
        /// <returns>OrderVendorDetails</returns>
        OrderVendorDetails GetOrderVendorDetails(int orderId);

        /// <summary>
        /// get vendors and customer details of order
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        Tuple<OrderVendorDetails, OrderCustomerDetails> GetOrderVendorAndCustomerDetails(Order order);

        OrderCustomerDetails GetOrderCustomerDetails(int orderId);

        /// <summary>
        /// Get Order Quantity and Product Name
        /// </summary>
        /// <param name="orderId">The order identifier</param>
        /// <returns>Order</returns>
        List<PushOrderItems> GetOrdersDetailsNameandQty(int orderId);
        #endregion

        List<Order> PendingOrderList(int storeId = 0,
         int vendorId = 0, IList<int> vendorIds = null);

        void InsertOrderDetails(OrderDetails orderDetails);

        /// <summary>
        /// Updates the orderDetails
        /// </summary>
        /// <param name="orderDetails">The order details</param>
        void UpdateOrderDetails(OrderDetails orderDetails);

        OrderDetails GetOrderDetails(int orderId = 0, int customerId = 0, int storeId = 0, bool? isOrderComplete = null);
        #endregion

        #region Agent Earning
        /// <summary>
        /// get agent earning
        /// </summary>
        /// <param name="id"></param>
        /// <param name="agentId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        AgentEarning GetAgentEarning(int id = 0, int agentId = 0, int orderId = 0);

        /// <summary>
        /// insert or update agent earning
        /// </summary>
        /// <param name="agentEarning"></param>
        void InsertUpdateAgentEarning(AgentEarning agentEarning);

        /// <summary>
        /// get agent earning amount details
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="vendorId"></param>
        /// <param name="customerId"></param>
        /// <param name="createdFromUtc"></param>
        /// <param name="createdToUtc"></param>
        /// <param name="osIds"></param>
        /// <param name="billingEmail"></param>
        /// <param name="agentId"></param>
        /// <returns></returns>
        Tuple<decimal, decimal, decimal, decimal> GetAgentEarningDetails(int storeId = 0,
            int vendorId = 0, IList<int> vendorIds = null, int customerId = 0,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            List<int> osIds = null, string billingEmail = null, int agentId = 0);
        #endregion

        #region Merchant Earning
        /// <summary>
        /// get merchant earning
        /// </summary>
        /// <param name="id"></param>
        /// <param name="merchantId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        MerchantEarning GetMerchantEarning(int id = 0, int merchantId = 0, int orderId = 0);

        /// <summary>
        /// insert or update merchant earning
        /// </summary>
        /// <param name="merchantEarning"></param>
        void InsertUpdateMerchantEarning(MerchantEarning merchantEarning);

        #endregion

        #region Dispatch management

        /// <summary>
        /// Get un assigned order id
        /// </summary>
        /// <returns>ids</returns>
        IList<int> GetUnassignedOrderIds(int storeId = 0, int vendorId = 0, IList<int> vendorIds = null);

        /// <summary>
        /// Get un assigned order id
        /// </summary>
        /// <returns>ids</returns>
        IList<int> GetAssignedOrderIds(int storeId = 0, int vendorId = 0, IList<int> vendorIds = null);

        #endregion

        #region Order Tracking

        /// <summary>
        /// Value idicates wether to show order tracking for order or not
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>True/False</returns>
        bool ShowOrderTracking(Order order);

        #endregion

        #region SignalR Invokers

        /// <summary>
        /// Invoke SignalR when order status changed 
        /// </summary>
        /// <param name="order"></param>
        void SendOrderStatusSignalR(Order order);

        /// <summary>
        /// Invoke SignalR when Agent asigned to order.
        /// </summary>
        /// <param name="order"></param>
        void SendAgentAssignedSignalR(Order order);

        #endregion

        #region Customer/Merchant Notification & SMS

        /// <summary>
        /// Send SMS to customer
        /// </summary>
        /// <param name="order"></param>
        /// <param name="orderstatus"></param>
        void SendMessage(Order order, int orderstatus);

        /// <summary>
        /// Send Mobile notification to customer
        /// </summary>
        /// <param name="order"></param>
        void SendNotificationtoCustomer(Order order);

        /// <summary>
        /// Send new order placed Mobile notification to merchant
        /// </summary>
        /// <param name="order"></param>
        void OrderPlacedNotificationtoMerchant(Order order);

        #endregion
    }
}