﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nestorbird.Plugin.Api.ResponseModel.Products
{
    public class ProductAttributeResponse: ListItemResponse
    {
        public ProductAttributeResponse()
        {
            ProductAttributeValues = new List<ProductAttributeValueResponse>();
        }

        public IList<ProductAttributeValueResponse> ProductAttributeValues { get; set; }
    }

    public class ProductAttributeValueResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Price { get; set; }

        public string ImageUrl { get; set; }
    }
}
