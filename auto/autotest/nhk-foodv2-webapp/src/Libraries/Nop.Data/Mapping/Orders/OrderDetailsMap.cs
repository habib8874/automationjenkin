﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Orders;

namespace Nop.Data.Mapping.Orders
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class OrderDetailsMap : NopEntityTypeConfiguration<OrderDetails>
    {
        public override void Configure(EntityTypeBuilder<OrderDetails> builder)
        {
            builder.ToTable(nameof(OrderDetails));
            builder.HasKey(sci => sci.Id);

            //builder.Ignore(sci => sci.StoreId);
            //builder.Ignore(sci => sci.CustomerId);
            //builder.Ignore(sci => sci.OrderType);

            base.Configure(builder);
        }
    }
}
