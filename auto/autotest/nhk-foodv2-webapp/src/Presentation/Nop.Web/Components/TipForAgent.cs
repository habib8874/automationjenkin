﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;

namespace Nop.Web.Components
{
    public class TipForAgentViewComponent : NopViewComponent
    {
        private readonly ICommonModelFactory _commonModelFactory;

        public TipForAgentViewComponent(ICommonModelFactory commonModelFactory)
        {
            _commonModelFactory = commonModelFactory;
        }

        public IViewComponentResult Invoke()
        {
            var model = _commonModelFactory.PrepareTipModel();
            return View(model);
        }
    }
}
