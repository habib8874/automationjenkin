﻿using System.Collections.Generic;
using Nop.Web.Models.NB;

namespace Nop.Web.Factories.NB
{
    public partial interface INBCustomerPackageModelFactory
    {

        /// <summary>
        /// Get PackagePlan by Package identifier
        /// </summary>
        /// <param name="packageId">Package identifier</param>
        /// <param name="addSelectPackagePlan">Whether to add "Select Package" item to list of packages</param>
        /// <returns>List of identifiers and names of PackagePlan</returns>
        IList<PackagePlanModel> GetPackagesPlanBypackageId(string packageId, bool addSelectStateItem);

    }
}
