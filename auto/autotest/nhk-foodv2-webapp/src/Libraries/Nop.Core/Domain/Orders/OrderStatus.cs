﻿namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents an order status enumeration
    /// </summary>
    public enum OrderStatus
    {
        //Custom code from v4.0
        /// <summary>
        /// Pending
        /// </summary>
        //Pending = 10,
        Received = 10,

        /// <summary>
        /// Processing
        /// </summary>
        //Processing = 20,
        Confirmed = 20,

        /// <summary>
        /// Rider Assigned
        /// </summary>
        RiderAssigned = 25,

        /// <summary>
        /// Complete
        /// </summary>
        //Complete = 30,
        Prepared = 30,

        /// <summary>
        /// Picked
        /// </summary>
        OrderPickedUp = 3,

        /// <summary>
        /// Delivered
        /// </summary>
        OrderDelivered = 4,

        /// <summary>
        /// Cancelled
        /// </summary>
        Cancelled = 40
    }
}
