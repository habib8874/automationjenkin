﻿using System;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.NB
{
    /// <summary>
    /// Represents a DeliveryDays model
    /// </summary>
    public partial class DeliveryDaysModel : BaseNopEntityModel
    {
        public DeliveryDaysModel() 
        {
           
        }

        public string WeekDay { get; set; }
        public string WeekDateValue { get; set; }
        public DateTime WeekDate { get; set; }
    }
}