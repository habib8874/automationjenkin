terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 2.0"
    }
  }
}

provider "cloudflare" { 
  email   = "systemadmin@nestorbird.com"
  api_key = var.api_key
}

variable "api_key" {
    default = "f4fc9313821b6782e99d2fbb92f17de33cf32"
    description = "global api key"
}

variable "zone_id" {
    default = "15e2f841370bb9eac37932ed2fc10047"
    description = "You may get zone ID  from 'select domain -> overview -> zone id'"
}

variable "ip" {
    default = "122.186.73.186"
    description = "ip to be mapped"
}
variable "subdomain" {
    type = list
    default = ["fuuddev"]
}


# Add a record to the domain
resource "cloudflare_record" "test" {
  zone_id = var.zone_id
  name    = var.subdomain[count.index]
  value   = var.ip
  type    = "A"
  ttl     = 1
  count = 1
  proxied = var.subdomain[count.index] == var.subdomain[0] ? true : false

}
