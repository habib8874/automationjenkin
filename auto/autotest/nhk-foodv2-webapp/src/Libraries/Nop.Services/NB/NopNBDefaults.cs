﻿namespace Nop.Services.Catalog
{
    /// <summary>
    /// Represents default values related to NB services
    /// </summary>
    public static partial class NopNBDefaults
    {
        #region Favorite merchants

        /// <summary>
        /// Gets a key for favorite merchant list
        /// </summary>
        /// <remarks>
        /// {0} : customer id
        /// {1} : storeId
        /// {2} : pageIndex
        /// {3} : pageSize
        /// </remarks>
        public static string FavoriteMerchantsCacheKey => "Nop.Nb.favorite.merchants-{0}-{1}-{2}-{3}";

        /// <summary>
        /// Gets a key for favorite merchant list by customer id
        /// </summary>
        /// <remarks>
        /// {0} : customer id
        /// </remarks>
        public static string FavoriteMerchantsByCustomerIdCacheKey => "Nop.Nb.favorite.merchants-{0}";

        /// <summary>
        /// Gets a key pattern to clear cache
        /// </summary>
        public static string FavoriteMerchantsPrefixCacheKey => "Nop.Nb.favorite.merchants-";

        /// <summary>
        /// Gets a key for entity reviews list
        /// </summary>
        /// <remarks>
        /// {0} : review type id
        /// {1} : entityId
        /// {2} : storeId
        /// </remarks>
        public static string EntityReviewsCacheKey => "Nop.Nb.entity.review-{0}-{1}-{2}";

        /// <summary>
        /// Gets a entity reviews key pattern to clear cache
        /// </summary>
        /// <remarks>
        public static string EntityReviewsPrefixCacheKey => "Nop.Nb.entity.review-";

        #endregion

        #region User sub admin

        /// <summary>
        /// Gets a key for user sub admin
        /// </summary>
        /// <remarks>
        /// {0} : customer id
        /// </remarks>
        public static string UserSubAdminCacheKey => "Nop.Nb.Admin.User.SubAdmin-{0}";

        /// <summary>
        /// Gets a user sub admin key pattern to clear cache
        /// </summary>
        /// <remarks>
        public static string UserSubAdminPrefixCacheKey => "Nop.Nb.Admin.User.SubAdmin-";

        #endregion

        #region NB Packages

        /// <summary>
        /// Gets a key for caching
        /// </summary>
        /// <remarks>
        /// {0} : Package ID
        /// </remarks>
        public static string PackagesByIdCacheKey => "Nop.package.id-{0}";

        /// <summary>
        /// Gets a key for caching
        /// </summary>
        /// <remarks>
        /// {0} : Package Plan ID
        /// </remarks>
        public static string PackagePlanByIdCacheKey => "Nop.package.plan.id-{0}";

        #endregion
    }
}
