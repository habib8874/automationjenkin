﻿using System.Linq;
using Nop.Core.Data;
using Nop.Core.Domain.NB.API;

namespace Nop.Services.NB.API
{
    public partial class VersionInfoServices : IVersionInfoServices
    {
        #region Fields

        private readonly IRepository<VersionInfo> _versionInfoRepository;

        #endregion

        #region Ctor

        public VersionInfoServices(IRepository<VersionInfo> versionInfoRepository)
        {
            _versionInfoRepository = versionInfoRepository;
        }

        #endregion

        /// <summary>
        /// value idicates whether provided API key is valid or not
        /// </summary>
        /// <param name="key"></param>
        /// <returns>True/False</returns>
        public bool IsValidAPIkey(string key)
        {
            if (string.IsNullOrEmpty(key))
                return false;

            return _versionInfoRepository.Table.Where(v => v.Apikey == key).Any();
        }
    }
}
