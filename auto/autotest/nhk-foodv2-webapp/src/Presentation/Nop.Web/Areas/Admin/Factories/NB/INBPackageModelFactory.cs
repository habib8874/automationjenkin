﻿using Nop.Core.Domain.NB.Package;
using Nop.Web.Areas.Admin.Models.NB.Package;

namespace Nop.Web.Areas.Admin.Factories.NB
{
    public partial interface INBPackageModelFactory
    {

        /// <summary>
        /// Prepare Package search model
        /// </summary>
        /// <param name="searchModel">Package search model</param>
        /// <returns>Package search model</returns>
        PackageSearchModel PreparePackageSearchModel(PackageSearchModel searchModel);

        /// <summary>
        /// Prepare paged Package list model
        /// </summary>
        /// <param name="searchModel">Package search model</param>
        /// <returns>Package list model</returns>
        PackageListModel PreparePackageListModel(PackageSearchModel searchModel);

        /// <summary>
        /// Prepare package model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="package"></param>
        /// <returns></returns>
        PackageModel PreparePackageModel(PackageModel model, NBPackage package);

        /// <summary>
        /// get package plans list
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        PackagePlanListModel PreparePackagePlanListModel(PackagePlanSearchModel searchModel);

    }
}
