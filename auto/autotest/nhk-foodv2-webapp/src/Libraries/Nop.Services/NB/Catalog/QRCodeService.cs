﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.NB.Catalog.QRCode;
using Nop.Services.Events;

namespace Nop.Services.NB.Catalog
{
    /// <summary>
    /// QRCode service
    /// </summary>
    public partial class QRCodeService : IQRCodeService
    {

        #region Fields

        private readonly IRepository<QRCode> _qRCodeRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="qRCodeRepository"></param>
        /// <param name="eventPublisher"></param>
        public QRCodeService(IRepository<QRCode> qRCodeRepository,
            IEventPublisher eventPublisher)
        {
            _qRCodeRepository = qRCodeRepository;
            _eventPublisher = eventPublisher;
        }

        #endregion

        #region QRCode

        /// <summary>
        /// Inserts a QRCode
        /// </summary>
        /// <param name="QRCode">QRCode</param>
        public virtual void InsertQRCode(QRCode qRCode)
        {
            if (qRCode == null)
                throw new ArgumentNullException("QRCode");

            _qRCodeRepository.Insert(qRCode);

            //event notification
            _eventPublisher.EntityInserted(qRCode);
        }

        /// <summary>
        /// Update the QRCode 
        /// </summary>
        /// <param name="QRCode">QRCode</param>
        public virtual void UpdateQRCode(QRCode qRCode)
        {
            if (qRCode == null)
                throw new ArgumentNullException("qRCode");

            _qRCodeRepository.Update(qRCode);

            //event notification
            _eventPublisher.EntityUpdated(qRCode);
        }

        /// <summary>
        /// Delete a qRCode
        /// </summary>
        /// <param name="qRCode">qRCode</param>
        public virtual void DeleteQRCode(QRCode qRCode)
        {
            if (qRCode == null)
                throw new ArgumentNullException("qRCode");

            _qRCodeRepository.Delete(qRCode);

            //event notification
            _eventPublisher.EntityDeleted(qRCode);
        }

        /// <summary>
        /// GetAllQRCode
        /// </summary>
        /// <param name="vendorId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="getOnlyTotalCount"></param>
        /// <returns></returns>
        public virtual IPagedList<QRCode> GetAllQRCode(int vendorId = 0,
          int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false)
         {
            var query = _qRCodeRepository.Table;
           
            if (vendorId > 0)
                query = query.Where(c => vendorId == c.MerchantId);
            
            var qRCode = new PagedList<QRCode>(query, pageIndex, pageSize, getOnlyTotalCount);
            return qRCode;
        }

        /// <summary>
        /// Gets a QRCode
        /// </summary>
        /// <param name="qRCodeId">QRCode identifier</param>
        /// <returns>A QRCode</returns>
        public virtual QRCode GetQRCodeById(int qRCodeId)
        {
            if (qRCodeId == 0)
                return null;

            return _qRCodeRepository.GetById(qRCodeId);
        }

        /// <summary>
        /// GetQRCodeByIds
        /// </summary>
        /// <param name="qRCodeIds"></param>
        /// <returns></returns>
        public virtual IList<QRCode> GetQRCodeByIds(int[] qRCodeIds)
        {
            if (qRCodeIds == null || qRCodeIds.Length == 0)
                return new List<QRCode>();

            var query = from p in _qRCodeRepository.Table
                        where qRCodeIds.Contains(p.Id) 
                        select p;
            var qRCodes = query.ToList();
            //sort by passed identifiers
            var sortedQRCodes = new List<QRCode>();
            foreach (var id in qRCodeIds)
            {
                var qRCode = qRCodes.Find(x => x.Id == id);
                if (qRCode != null)
                    sortedQRCodes.Add(qRCode);
            }

            return sortedQRCodes;
        }

        #endregion
    }
}
