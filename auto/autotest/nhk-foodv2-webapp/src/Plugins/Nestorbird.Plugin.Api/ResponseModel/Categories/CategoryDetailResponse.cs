﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nestorbird.Plugin.Api.ResponseModel.Categories
{
    public class CategoryDetailResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int CategoryTemplateId { get; set; }

        public bool ShowOnHomepage { get; set; }

        public int ParentCategoryId { get; set; }

        public int DisplayOrder { get; set; }

        public bool Published { get; set; }

        public string ImageBase64 { get; set; }

        public IList<int> ProductIds { get; set; }
    }
}
