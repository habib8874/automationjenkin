﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB.MerchantCategorys
{
    /// <summary>
    /// Represents a MerchantCategory list model
    /// </summary>
    public partial class MerchantCategoryListModel : BasePagedListModel<MerchantCategoryModel>
    {
    }
}