﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;


namespace Nop.Web.Areas.Admin.Models.NB.Suppliers
{
    public partial class SupplierSearchModel : BaseSearchModel
    {
        #region Properties

        [NopResourceDisplayName("Admin.Supplier.List.SupplierName")]
        public string SupplierName { get; set; }

        [NopResourceDisplayName("Admin.Supplier.List.IsSupplier")]
        public bool IsSupplier { get; set; }

        [NopResourceDisplayName("Admin.Supplier.List.StoreId")]
        public int StoreId { get; set; }

    }


        public partial class DataSourceRequest : BaseSearchModel
        {
            /// <summary>
            /// Ctor
            /// </summary>
            public DataSourceRequest()
            {
            
            }
        [NopResourceDisplayName("Admin.Supplier.Products.List.SearchSupplier")]
        public int SearchSupplierId { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Products.List.SearchStore")]
        public int SearchStoreId { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Products.List.SearchVendor")]
        public int SearchVendorId { get; set; }

        #endregion

    }
}

