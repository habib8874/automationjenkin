﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Data;

namespace Nop.Web.Areas.Admin.Controllers
{
    public class UserPushTokenController : Controller
    {
        private readonly IDbContext _dbContext;

        public UserPushTokenController(IDbContext dbContext)
        {
            this._dbContext = dbContext;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void InsertUserPushToken(int userId, string userToken)
        {
            string strsql = "insert into UserPushIdDetails (UserId,Token,CrDt) values(@userId,@userToken,@Dt)";            
            //var listven = _dbContext.SqlQuery<int>(strsql).ToList();
            var statusCount = _dbContext.ExecuteSqlCommand(strsql,
                parameters: new[]{
                new SqlParameter("@userId",userId),
                new SqlParameter("@userToken", userToken),
                new SqlParameter("@Dt",DateTime.Now) }
                );
        }

    }
}