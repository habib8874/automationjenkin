﻿using System.Linq;
using FluentValidation;
using Nop.Data;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Web.Areas.Admin.Models.NB.AccountsBilling;
using Nop.Web.Framework.Validators;

namespace Nop.Web.Areas.Admin.Validators.Packages
{
    public partial class AccountsBillingValidator : BaseNopValidator<AccountBillingModel>
    {
        public AccountsBillingValidator(ILocalizationService localizationService,
        IStateProvinceService stateProvinceService
        )
        {
            //ensure that valid email address is entered with
            RuleFor(x => x.EmailAddress)
                .NotEmpty()
                .EmailAddress()
                .WithMessage(localizationService.GetResource("Admin.Common.WrongEmail"));

            //form fields
            RuleFor(x => x.CountryId)
                .NotEqual(0)
                .WithMessage(localizationService.GetResource("Account.Fields.Country.Required"));

            RuleFor(x => x.StateProvinceId).Must((x, context) =>
            {
                //does selected country have states?
                var hasStates = stateProvinceService.GetStateProvincesByCountryId(x.CountryId).Any();
                if (hasStates)
                {
                    //if yes, then ensure that a state is selected
                    if (x.StateProvinceId == 0)
                        return false;
                }

                return true;
            }).WithMessage(localizationService.GetResource("Account.Fields.StateProvince.Required"));

            RuleFor(x => x.AddressLine1)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Admin.Customers.Customers.Fields.StreetAddress.Required"));

            RuleFor(x => x.FirstName)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Admin.Customers.Customers.Fields.FirstName.Required"));

            RuleFor(x => x.LastName)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Admin.Customers.Customers.Fields.LastName.Required"));

            RuleFor(x => x.ZipCode)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Admin.Customers.Customers.Fields.ZipPostalCode.Required"));

            RuleFor(x => x.PhoneNumber)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Admin.Customers.Customers.Fields.Phone.Required"));

            RuleFor(x => x.AccountDocumentModel.IdentityProofFront)
             .NotEmpty()
             .WithMessage(localizationService.GetResource("Admin.AccountDocumentModel.Field.IdentityProofFront.Required"));

            RuleFor(x => x.AccountDocumentModel.IdentityProofBack)
            .NotEmpty()
             .WithMessage(localizationService.GetResource("Admin.AccountDocumentModel.Field.IdentityProofBack.Required"));
        }
    }
}
