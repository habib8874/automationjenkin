﻿using Nop.Web.Areas.Admin.Models.Directory;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Stores
{
    /// <summary>
    /// Represents a states list model to add to the country
    /// </summary>
    public partial class AddStatesToCountryListModel : BasePagedListModel<StateProvinceModel>
    {
    }
}