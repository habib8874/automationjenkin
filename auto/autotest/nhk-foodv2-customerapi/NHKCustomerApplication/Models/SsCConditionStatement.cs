﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsCConditionStatement
    {
        public int Id { get; set; }
        public int ConditionType { get; set; }
        public int ConditionProperty { get; set; }
        public int OperatorType { get; set; }
        public string Value { get; set; }
        public int ConditionGroupId { get; set; }

        public virtual SsCConditionGroup ConditionGroup { get; set; }
    }
}
