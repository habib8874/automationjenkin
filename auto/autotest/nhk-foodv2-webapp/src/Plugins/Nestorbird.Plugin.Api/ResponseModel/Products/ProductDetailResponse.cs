﻿using System.Collections.Generic;

namespace Nestorbird.Plugin.Api.ResponseModel.Products
{
    public class ProductDetailResponse
    {
        public ProductDetailResponse()
        {
            CategoryIds = new List<int>();
            CrossSellProductIds = new List<int>();
            Images = new List<ProductImageModel>();
            ProductAttributes = new List<ProductAttributeDetailResponse>();
            SpecificationAttributes = new List<SpecificationAttributeDetailResponse>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string ShortDescription { get; set; }

        public string FullDescription { get; set; }

        public IList<int> CategoryIds { get; set; }

        public decimal Price { get; set; }

        public decimal OldPrice { get; set; }

        public int DisplayOrder { get; set; }

        public bool Published { get; set; }

        public int? DiscountId { get; set; }

        public int TaxCategoryStateId { get; set; }

        public int TaxCategoryId { get; set; }

        public IList<ProductImageModel> Images { get; set; }

        public IList<ProductAttributeDetailResponse> ProductAttributes { get; set; }

        public IList<SpecificationAttributeDetailResponse> SpecificationAttributes { get; set; }

        public IList<int> CrossSellProductIds { get; set; }
    }

    public class ProductImageModel
    {
        public string ImageUrl { get; set; }

        public int ImageId { get; set; }
    }

    public class SpecificationAttributeDetailResponse
    {
        public int Id { get; set; }

        public int? SpecificationAttributeId { get; set; }

        public string Name { get; set; }

        public string Text { get; set; }
    }

    public class ProductAttributeDetailResponse
    {
        public ProductAttributeDetailResponse()
        {
            Values = new List<ProductAttributeValueDetailResponse>();
        }

        public int Id { get; set; }

        public int AttributeId { get; set; }

        public string Name { get; set; }

        public IList<ProductAttributeValueDetailResponse> Values { get; set; }
    }

    public class ProductAttributeValueDetailResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ImageUrl { get; set; }
    }
}
