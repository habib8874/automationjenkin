﻿namespace NHKCustomerApplication.Caching
{
    public static class CacheKeys
    {
        public static string CategoryListKey { get { return "API.Cache.Category-{0}-{1}-{2}-{3}-{4}-{5}"; } }
        public static string MerchantListKey { get { return "API.Cache.Merchant-{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}-{8}-{9}"; } }
        public static string ProductListKey { get { return "API.Cache.Product-{0}-{1}-{2}-{3}-{4}-{5}"; } }
        public static string ProductListV1Key { get { return "API.Cache.Product-{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}"; } }
        public static string AuthenticateKey { get { return "API.Cache.Authentication-{0}-{1}"; } }
        public static string BusinessCategoryListKey { get{ return "API.Cache.Category-{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}"; } }
        public static string StoreBannerListKey { get{ return "API.Cache.StoreBanner-{0}-{1}-{2}-{3}"; } }
        public static string VendorBannerListKey { get{ return "API.Cache.VendorBanner-{0}-{1}-{2}"; } }
        public static string VersionKey {get { return "API.Cache.Version-{0}-{1}-{2}-{3}-{4}"; } }
        public static string OrderHistroyListKey { get { return "API.Cache.Order-{0}-{1}-{2}-{3}-{4}-{5}"; } }
        public static string ProductFilterListKey { get { return "API.Cache.ProductFilter-{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}-{8}-{9}-{10}-{11}-{12}-{13}-{14}-{15}-{16}-{17}-{18}-{19}-{20}-{21}-{22}"; } }


    }
}
