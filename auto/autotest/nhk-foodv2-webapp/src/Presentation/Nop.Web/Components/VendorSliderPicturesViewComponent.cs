﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Services.Media;
using Nop.Services.Vendors;
using Nop.Web.Framework.Components;

namespace Nop.Web.Components
{
    public class VendorSliderPicturesViewComponent : NopViewComponent
    {
        private readonly IVendorService _vendorService;
        private readonly IWorkContext _workContext;
        private readonly IPictureService _pictureService;

        public VendorSliderPicturesViewComponent(IVendorService vendorService,
            IWorkContext workContext,
            IPictureService pictureService)
        {
            _vendorService = vendorService;
            _workContext = workContext;
            _pictureService = pictureService;
        }

        //public IViewComponentResult Invoke(string merchantid)
        public IViewComponentResult Invoke()
        {
            var merchantid = _workContext.CurrentMerchant?.Id ?? 0;
            if (merchantid <= 0)
                return Content("");

            var model = new List<string>();
            var vendorSliderPictures = _vendorService.GetVendorSliderPicturesByVendorId(merchantid);
            foreach (var vendorSliderPicture in vendorSliderPictures)
            {
                var picture = _pictureService.GetPictureById(vendorSliderPicture.PictureId)
                              ?? throw new Exception("Picture cannot be loaded");

                model.Add(_pictureService.GetPictureUrl(picture));
            }

            return View(model);
        }
    }
}
