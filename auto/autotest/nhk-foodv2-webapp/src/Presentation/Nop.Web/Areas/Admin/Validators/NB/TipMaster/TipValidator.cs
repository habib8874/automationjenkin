﻿using FluentValidation;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Areas.Admin.Models.NB;
using Nop.Web.Framework.Validators;

namespace Nop.Web.Areas.Admin.Validators.NB.TipMaster
{
    public partial class TipValidator : BaseNopValidator<TipModel>
    {
        public TipValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.TipAmount).NotEmpty().WithMessage(localizationService.GetResource("NB.Admin.TipMaster.Fields.TipAmount.Required"));
        }
    }
}