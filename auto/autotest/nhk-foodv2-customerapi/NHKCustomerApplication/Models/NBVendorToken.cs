﻿namespace NHKCustomerApplication.Models
{
    public partial class NBVendorToken
    {
        public int Id { get; set; }
        public int CustomerId { get; set;}
        public string Token { get; set; }
        public bool Deleted { get; set; }
    }
}
