﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.RatingReview;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Orders;
using Nop.Services.RatingReview;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Web.Models.Home;
using Nop.Web.Models.RatingReview;
using Nop.Web.Models.ShoppingCart;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Web.Components
{
    public class OffersViewComponent : NopViewComponent
    {
        private readonly IDiscountService _discountService;
        private readonly IStoreService _storeService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;

        public OffersViewComponent(IDiscountService discountService,
            IStoreService storeService, IStoreMappingService storeMappingService,
            IWorkContext workContext, IStoreContext storeContext)
        {
            this._discountService = discountService;
            this._storeService = storeService;
            this._storeMappingService = storeMappingService;
            this._workContext = workContext;
            this._storeContext = storeContext;
        }

        public IViewComponentResult Invoke(int reviewType, int productId = 0)
        {
            var model = new List<OffersModel>();
            int storeId = _workContext.GetCurrentStoreId;
            var discountsAll = _discountService.GetAllDiscounts(showHidden: false);

            foreach (var disc in discountsAll)
            {
                var mappedStores = _storeMappingService.GetStoresIdsWithAccess("Discount", disc.Id);
                if (mappedStores.Contains(storeId))
                {
                    model.Add(new OffersModel { Name = disc.Name, IsPercentage = disc.UsePercentage });
                }
            }

            if (model.Count == 0)
                return Content("");

            return View(model);
        }
    }
}
