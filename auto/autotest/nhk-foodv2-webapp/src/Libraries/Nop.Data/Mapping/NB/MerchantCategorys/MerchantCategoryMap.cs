﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB.MerchantCategorys;

namespace Nop.Data.Mapping.NB.MerchantCategorys
{
    /// <summary>
    /// Represents a Merchant Category configuration
    /// </summary>
    public partial class MerchantCategoryMap : NopEntityTypeConfiguration<MerchantCategory>
    {
        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<MerchantCategory> builder)
        {
            builder.ToTable(NopMappingDefaults.MerchantCategoryTable);
            builder.HasKey(pc => pc.Id);
            base.Configure(builder);
        }
    }
}
