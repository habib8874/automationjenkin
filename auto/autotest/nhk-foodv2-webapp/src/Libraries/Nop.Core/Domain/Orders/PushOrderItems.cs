﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
   public class PushOrderItems
    {
        public string ItemName { get; set; }
        public string ItemQty { get; set; }
    }
}
