﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB
{
    /// <summary>
    /// Represents a customer wallet list model
    /// </summary>
    public partial class CustomerWalletListModel : BasePagedListModel<CustomerWalletModel>
    {
    }
}