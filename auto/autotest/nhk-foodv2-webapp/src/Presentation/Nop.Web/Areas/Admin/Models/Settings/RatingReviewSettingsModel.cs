﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using System.Collections.Generic;
using Nop.Core.Domain.RatingReview;

namespace Nop.Web.Areas.Admin.Models.Settings
{
    public partial class RatingReviewSettingsModel : BaseNopEntityModel
    {
        #region Code By Deeksha

        public RatingReviewSettingsModel()
        {
            AvailableStores = new List<SelectListItem>();
            AvailableVendor = new List<SelectListItem>();
            AvailableAgnetReviewOptions = new List<ReviewOption>();
            AvailableMerchantReviewOptions = new List<ReviewOption>();
        }


        public IList<int> SelectedStoreIds { get; set; }

        public IList<SelectListItem> AvailableStores { get; set; }
        public IList<SelectListItem> AvailableVendor { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Fields.SearchStoreId")]
        public int SearchStoreId { get; set; }
        public bool StoreEnables { get; set; }
        public bool VendorsEnables { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Fields.SearchVendorId")]
        public int SearchVendorId { get; set; }


        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Fields.Value")]
        public string Value { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Fields.StoreName")]
        public string Store { get; set; }
        public int StoreId { get; set; }
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.DislikeOption")]
        public bool DislikeOption { get; set; }
        public bool DislikeOption_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.IsActive")]
        public bool AgentIsActive { get; set; }
        public bool AgentIsActive_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Agent.TookTooLong")]
        public bool TookTooLong { get; set; }
        public bool TookTooLong_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Agent.Unprofessional")]
        public bool Unprofessional { get; set; }
        public bool Unprofessional_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Agent.ItemDamaged")]
        public bool ItemDamaged { get; set; }
        public bool ItemDamaged_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Agent.DeliveryManWearingMask")]
        public bool DeliveryManWearingMask { get; set; }
        public bool DeliveryManWearingMask_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Agent.IgnoredDeliveryNote")]
        public bool IgnoredDeliveryNote { get; set; }
        public bool IgnoredDeliveryNote_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.AdditionalNotes")]
        public bool AgentAdditionalNotes { get; set; }
        public bool AgentAdditionalNotes_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.IsApprovalRequired")]
        public bool AgentRequiresAdminApproval { get; set; }
        public bool AgentRequiresAdminApproval_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.IsActive")]
        public bool MerchantIsActive { get; set; }
        public bool MerchantIsActive_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Merchant.Quality")]
        public bool Quality { get; set; }
        public bool Quality_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Merchant.Packaging")]
        public bool Packaging { get; set; }
        public bool Packaging_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Merchant.ColdFood")]
        public bool ColdFood { get; set; }
        public bool ColdFood_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Merchant.Delivery")]
        public bool Delivery { get; set; }
        public bool Delivery_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.Merchant.Price")]
        public bool Price { get; set; }
        public bool Price_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.AdditionalNotes")]
        public bool MerchantAdditionalNotes { get; set; }
        public bool MerchantAdditionalNotes_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.IsApprovalRequired")]
        public bool MerchantRequiresAdminApproval { get; set; }
        public bool MerchantRequiresAdminApproval_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.IsActive")]
        public bool ItemIsActive { get; set; }
        public bool ItemIsActive_OverrideForStore { get; set; }


        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.AdditionalNotes")]
        public bool ItemAdditionalNotes { get; set; }
        public bool ItemAdditionalNotes_OverrideForStore { get; set; }
        
        [NopResourceDisplayName("Admin.Configuration.Settings.RatingReviewSettings.IsApprovalRequired")]
        public bool ItemRequiresAdminApproval { get; set; }
        public bool ItemRequiresAdminApproval_OverrideForStore { get; set; }

        public IList<ReviewOption> AvailableAgnetReviewOptions { get; set; }
        public IList<ReviewOption> AvailableMerchantReviewOptions { get; set; }

        public string AgentActiveSettingIds { get; set; }
        public string MerchantActiveSettingIds { get; set; }

        #endregion
    }
}