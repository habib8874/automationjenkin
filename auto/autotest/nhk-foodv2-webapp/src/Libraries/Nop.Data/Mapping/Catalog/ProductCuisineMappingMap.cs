using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class ProductCuisineMappingMap : NopEntityTypeConfiguration<ProductCuisineMapping>
    {
        public override void Configure(EntityTypeBuilder<ProductCuisineMapping> builder)
        {
            builder.ToTable("Product_ProductCuisine_Mapping");
            builder.HasKey(pt => pt.Id);
            builder.Property(pt => pt.CuisineId).IsRequired();
            builder.Property(pt => pt.ProductId).IsRequired();

            base.Configure(builder);
        }
    }
}