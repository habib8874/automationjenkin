﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.NB;

namespace Nop.Services.NB
{
    /// <summary>
    /// Order service interface
    /// </summary>
    public partial interface IWalletDetailsService
    {
        /// <summary>
        /// Gets an WalletDetails
        /// </summary>
        /// <param name="walletId">The WalletDetails identifier</param>
        /// <returns>Order</returns>
        WalletDetails GetWalletById(int walletId);

        /// <summary>
        /// GetWalletsByCustomerId by identifiers
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <returns>WalletDetails</returns>
        IList<WalletDetails> GetWalletsByCustomerId(int customerId);

        /// <summary>
        /// Deletes an wallet
        /// </summary>
        /// <param name="wallet">The wallet</param>
        void DeleteWallet(WalletDetails wallet);

        /// <summary>
        /// SearchWallets
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>WalletDetails</returns>
        IPagedList<WalletDetails> SearchWallets(int customerId = 0, int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false);
        Decimal GetWalletBalance(int customerId);
        /// <summary>
        /// Inserts an WalletDetails
        /// </summary>
        /// <param name="WalletDetails">WalletDetails</param>
        void InsertWallet(WalletDetails wallet);

        /// <summary>
        /// Updates the WalletDetails
        /// </summary>
        /// <param name="WalletDetails">The WalletDetails</param>
        void UpdateWallet(WalletDetails wallet);      
   
    }
}