﻿using Nop.Core.Domain.Vendors;

namespace Nop.Core.Domain.NB.MerchantCategorys
{
    /// <summary>
    /// Represents a Merchant Category mapping class
    /// </summary>
    public partial class MerchantCategoryMapping : BaseEntity
    {
        /// <summary>
        /// Gets or sets the MerchantCategory identifier
        /// </summary>
        public int MerchantCategoryId { get; set; }

        /// <summary>
        /// Gets or sets the vendor tag identifier
        /// </summary>
        public int VendorId { get; set; }

        /// <summary>
        /// Gets or sets the MerchantCategory
        /// </summary>
        public virtual MerchantCategory MerchantCategory { get; set; }

        /// <summary>
        /// Gets or sets the vendor tag
        /// </summary>
        public virtual Vendor Vendor { get; set; }
    }
}