﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Payments.WalletPay.Models
{
    public class PaymentInfoModel : BaseNopModel
    {
        public string DescriptionText { get; set; }
        public decimal OrderTotal { get; set; }
        public string WalletBallance { get; set; }
        public bool ValidAmountInWallet { get; set; }
    }
}