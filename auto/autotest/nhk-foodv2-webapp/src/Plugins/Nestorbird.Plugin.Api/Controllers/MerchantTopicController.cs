﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Services.Localization;
using Nop.Services.Topics;

namespace Nestorbird.Plugin.Api.Controllers
{
    [Route("merchantapi/topic")]
    public class MerchantTopicController : BaseAPIController
    {
        #region Fields

        private readonly ITopicService _topicService;

        #endregion

        #region Ctor

        public MerchantTopicController(ILanguageService languageService,
            ITopicService topicService) : base(languageService)
        {
            _topicService = topicService;
        }

        #endregion

        #region Methods

        [HttpGet]
        [Route("getcontent/{systemName}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public IActionResult GetContent(string systemName)
        {
            if (string.IsNullOrEmpty(systemName))
                return Error("Please provide system name");

            var topicContent = _topicService.GetTopicBySystemName(systemName);
            if (topicContent == null)
                return Error("This system name is not exist");

            return Success(topicContent.Body);
        }

        #endregion
    }
}
