﻿using NHKCustomerApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NHKCustomerApplication.ViewModels
{
    /// <summary>
    /// Represents a request for tax calculation
    /// </summary>
    public partial class CalculateTaxRequest
    {
        /// <summary>
        /// Gets or sets a customer
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets a product
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// Gets or sets an address
        /// </summary>
        public TaxAddress Address { get; set; }

        /// <summary>
        /// Gets or sets a tax category identifier
        /// </summary>
        public int TaxCategoryId { get; set; }

        /// <summary>
        /// Gets or sets a price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets a current store identifier
        /// </summary>
        public int CurrentStoreId { get; set; }

        /// <summary>
        /// Represents information about address for tax calculation
        /// </summary>
        [Serializable]
        public class TaxAddress
        {
            /// <summary>
            /// Gets or sets an indentifier of appropriate "Address" entity (if available). Otherwise, 0
            /// </summary>
            public int AddressId { get; set; }

            /// <summary>
            /// Gets or sets the country identifier
            /// </summary>
            public int? CountryId { get; set; }

            /// <summary>
            /// Gets or sets the state/province identifier
            /// </summary>
            public int? StateProvinceId { get; set; }

            /// <summary>
            /// Gets or sets the county
            /// </summary>
            public string County { get; set; }

            /// <summary>
            /// Gets or sets the city
            /// </summary>
            public string City { get; set; }

            /// <summary>
            /// Gets or sets the address 1
            /// </summary>
            public string Address1 { get; set; }

            /// <summary>
            /// Gets or sets the address 2
            /// </summary>
            public string Address2 { get; set; }

            /// <summary>
            /// Gets or sets the zip/postal code
            /// </summary>
            public string ZipPostalCode { get; set; }
        }
    }

    /// <summary>
    /// Represents a tax rate for caching
    /// </summary>
    public partial class TaxRateForCaching
    {
        public int Id { get; set; }
        public int StoreId { get; set; }
        public int TaxCategoryId { get; set; }
        public int CountryId { get; set; }
        public int StateProvinceId { get; set; }
        public string Zip { get; set; }
        public decimal Percentage { get; set; }
    }
    public partial class PickupPoint
    {
        /// <summary>
        /// Gets or sets an identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets a name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a system name of the pickup point provider
        /// </summary>
        public string ProviderSystemName { get; set; }

        /// <summary>
        /// Gets or sets an address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets a city
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets a county
        /// </summary>
        public string County { get; set; }

        /// <summary>
        /// Gets or sets a state abbreviation
        /// </summary>
        public string StateAbbreviation { get; set; }

        /// <summary>
        /// Gets or sets a two-letter ISO country code
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Gets or sets a zip postal code
        /// </summary>
        public string ZipPostalCode { get; set; }

        /// <summary>
        /// Gets or sets a latitude
        /// </summary>
        public decimal? Latitude { get; set; }

        /// <summary>
        /// Gets or sets a longitude
        /// </summary>
        public decimal? Longitude { get; set; }

        /// <summary>
        /// Gets or sets a fee for the pickup
        /// </summary>
        public decimal PickupFee { get; set; }

        /// <summary>
        /// Gets or sets an opening hours
        /// </summary>
        public string OpeningHours { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }
    }

    /// <summary>
    /// Represents a result of tax calculation
    /// </summary>
    public partial class CalculateTaxResult
    {
        public CalculateTaxResult()
        {
            Errors = new List<string>();
        }

        /// <summary>
        /// Gets or sets a tax rate
        /// </summary>
        public decimal TaxRate { get; set; }

        /// <summary>
        /// Gets or sets errors
        /// </summary>
        public IList<string> Errors { get; set; }

        /// <summary>
        /// Gets a value indicating whether request has been completed successfully
        /// </summary>
        public bool Success => !Errors.Any();

        /// <summary>
        /// Add error
        /// </summary>
        /// <param name="error">Error</param>
        public void AddError(string error)
        {
            Errors.Add(error);
        }
    }
}
