﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Core.Domain.Customers;

namespace Nop.Web.Models.Customer
{
    public partial class RegisterModel : BaseNopModel
    {
        public RegisterModel()
        {
            AvailableTimeZones = new List<SelectListItem>();
            AvailableCountries = new List<SelectListItem>();
            AvailableStates = new List<SelectListItem>();
            CustomerAttributes = new List<CustomerAttributeModel>();
            GdprConsents = new List<GdprConsentModel>();
            AvailableISD = new List<SelectListItem>();//Custom code from v4.0

            //package module
            AvailablePackages = new List<SelectListItem>();//Custom code from v4.0
            AvailablePackagesPlan = new List<SelectListItem>();//Custom code from v4.0

            AvailableRiderCountries = new List<SelectListItem>();
            AvailableRiderStates = new List<SelectListItem>();
            AvailableRiderCities = new List<SelectListItem>();
        }
        
        [DataType(DataType.EmailAddress)]
        [NopResourceDisplayName("Account.Fields.Email")]
        public string Email { get; set; }
        
        public bool EnteringEmailTwice { get; set; }
        [DataType(DataType.EmailAddress)]
        [NopResourceDisplayName("Account.Fields.ConfirmEmail")]
        public string ConfirmEmail { get; set; }

        public bool UsernamesEnabled { get; set; }
        [NopResourceDisplayName("Account.Fields.Username")]
        public string Username { get; set; }

        public bool CheckUsernameAvailabilityEnabled { get; set; }

        [DataType(DataType.Password)]
        [NoTrim]
        [NopResourceDisplayName("Account.Fields.Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [NoTrim]
        [NopResourceDisplayName("Account.Fields.ConfirmPassword")]
        public string ConfirmPassword { get; set; }

        //form fields & properties
        public bool GenderEnabled { get; set; }
        [NopResourceDisplayName("Account.Fields.Gender")]
        public string Gender { get; set; }

        [NopResourceDisplayName("Account.Fields.FirstName")]
        public string FirstName { get; set; }
        [NopResourceDisplayName("Account.Fields.LastName")]
        public string LastName { get; set; }

        public bool DateOfBirthEnabled { get; set; }
        [NopResourceDisplayName("Account.Fields.DateOfBirth")]
        public int? DateOfBirthDay { get; set; }
        [NopResourceDisplayName("Account.Fields.DateOfBirth")]
        public int? DateOfBirthMonth { get; set; }
        [NopResourceDisplayName("Account.Fields.DateOfBirth")]
        public int? DateOfBirthYear { get; set; }
        public bool DateOfBirthRequired { get; set; }
        public DateTime? ParseDateOfBirth()
        {
            if (!DateOfBirthYear.HasValue || !DateOfBirthMonth.HasValue || !DateOfBirthDay.HasValue)
                return null;

            DateTime? dateOfBirth = null;
            try
            {
                dateOfBirth = new DateTime(DateOfBirthYear.Value, DateOfBirthMonth.Value, DateOfBirthDay.Value);
            }
            catch { }
            return dateOfBirth;
        }

        public bool CompanyEnabled { get; set; }
        public bool CompanyRequired { get; set; }
        [NopResourceDisplayName("Account.Fields.Company")]
        public string Company { get; set; }

        public bool StreetAddressEnabled { get; set; }
        public bool StreetAddressRequired { get; set; }
        [NopResourceDisplayName("Account.Fields.StreetAddress")]
        public string StreetAddress { get; set; }

        public bool StreetAddress2Enabled { get; set; }
        public bool StreetAddress2Required { get; set; }
        [NopResourceDisplayName("Account.Fields.StreetAddress2")]
        public string StreetAddress2 { get; set; }

        public bool ZipPostalCodeEnabled { get; set; }
        public bool ZipPostalCodeRequired { get; set; }
        [NopResourceDisplayName("Account.Fields.ZipPostalCode")]
        public string ZipPostalCode { get; set; }

        public bool CityEnabled { get; set; }
        public bool CityRequired { get; set; }
        [NopResourceDisplayName("Account.Fields.City")]
        public string City { get; set; }

        public bool CountyEnabled { get; set; }
        public bool CountyRequired { get; set; }
        [NopResourceDisplayName("Account.Fields.County")]
        public string County { get; set; }

        public bool CountryEnabled { get; set; }
        public bool CountryRequired { get; set; }
        [NopResourceDisplayName("Account.Fields.Country")]
        public int CountryId { get; set; }
        public IList<SelectListItem> AvailableCountries { get; set; }

        public bool StateProvinceEnabled { get; set; }
        public bool StateProvinceRequired { get; set; }
        [NopResourceDisplayName("Account.Fields.StateProvince")]
        public int StateProvinceId { get; set; }
        public IList<SelectListItem> AvailableStates { get; set; }

        public bool PhoneEnabled { get; set; }
        public bool PhoneRequired { get; set; }
        [DataType(DataType.PhoneNumber)]
        [NopResourceDisplayName("Account.Fields.Phone")]
        public string Phone { get; set; }

        public bool FaxEnabled { get; set; }
        public bool FaxRequired { get; set; }
        [DataType(DataType.PhoneNumber)]
        [NopResourceDisplayName("Account.Fields.Fax")]
        public string Fax { get; set; }
        
        public bool NewsletterEnabled { get; set; }
        [NopResourceDisplayName("Account.Fields.Newsletter")]
        public bool Newsletter { get; set; }
        
        public bool AcceptPrivacyPolicyEnabled { get; set; }
        public bool AcceptPrivacyPolicyPopup { get; set; }

        //time zone
        [NopResourceDisplayName("Account.Fields.TimeZone")]
        public string TimeZoneId { get; set; }
        public bool AllowCustomersToSetTimeZone { get; set; }
        public IList<SelectListItem> AvailableTimeZones { get; set; }

        //EU VAT
        [NopResourceDisplayName("Account.Fields.VatNumber")]
        public string VatNumber { get; set; }
        public bool DisplayVatNumber { get; set; }

        public bool HoneypotEnabled { get; set; }
        public bool DisplayCaptcha { get; set; }

        public IList<CustomerAttributeModel> CustomerAttributes { get; set; }

        public IList<GdprConsentModel> GdprConsents { get; set; }

        //Custom code from v4.0
        public int ISDId { get; set; }
        public IList<SelectListItem> AvailableISD { get; set; }

        public RegistrationCustomerType RegisterationType { get; set; }

        [NopResourceDisplayName("NB.Account.Fields.BusinessName")]
        public string RestaurantName { get; set; }

        public bool RestaurantRegisterAllow { get; set; }

        [NoTrim]
        [NopResourceDisplayName("Account.Fields.OTP")]
        public string NewRegisterOTP { get; set; }

        public string CompanySale { get; set; }

        [NopResourceDisplayName("NB.Account.Fields.VenueName")]
        public string VenueName { get; set; }

        [NopResourceDisplayName("NB.Account.Fields.VenueURL")]
        public string VenueURL { get; set; }

        public bool StoreRegisterAllow { get; set; }

        //field for package and package plan

        [NopResourceDisplayName("NB.Account.Fields.SelectPackage")]
        public int PackageId { get; set; }
        public IList<SelectListItem> AvailablePackages { get; set; }


        [NopResourceDisplayName("NB.Account.Fields.SelectPackagePlan")]
        public int PackagePlanId { get; set; }
        public IList<SelectListItem> AvailablePackagesPlan { get; set; }

        public bool IsStoreRegisterAllowPackage { get; set; }

        [NopResourceDisplayName("NB.Account.Fields.RiderCountryId")]
        public int RiderCountryId { get; set; }
        public IList<SelectListItem> AvailableRiderCountries { get; set; }
        public bool RiderCountryEnabled { get; set; }

        [NopResourceDisplayName("NB.Account.Fields.RiderStateProvinceId")]
        public int RiderStateProvinceId { get; set; }
        public IList<SelectListItem> AvailableRiderStates { get; set; }
        public bool RiderStateProvinceEnabled { get; set; }

        [NopResourceDisplayName("NB.Account.Fields.RiderCityId")]
        public int RiderCityId { get; set; }
        public IList<SelectListItem> AvailableRiderCities { get; set; }
        public bool RiderCitiesEnabled { get; set; }

        [NopResourceDisplayName("NB.Account.Fields.VehicleNumber")]
        public string VehicleNumber { get; set; }
        public bool IsAgentRider { get; set; }

        [NopResourceDisplayName("NB.Account.Rider.Fields.Document.IdProof")]
        [UIHint("Document")]
        public int IdProofDocId { get; set; }

        [NopResourceDisplayName("NB.Account.Rider.Fields.Document.RCBook")]
        [UIHint("Document")]
        public int RcBookDocId { get; set; }
    }
}