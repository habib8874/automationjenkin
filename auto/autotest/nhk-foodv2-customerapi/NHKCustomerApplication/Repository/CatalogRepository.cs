﻿using Dapper;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModel;
using NHKCustomerApplication.ViewModels;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace NHKCustomerApplication.Repository
{
    public class CatalogRepository : ICatalogRespository
    {
        #region "Fields"
        private const string CATEGORYLISTING_V1 = "NB_CategoryListing";
        private const string BUSINESSCATEGORY = "NB_GetMerchantCategorys";
        private const string BUSINESSCATEGORYLIST = "NB_GetMerchantCategorysV1";
        private const string BUSINESSCATEGORYLIST2 = "NB_GetMerchantCategorysV2";
        private string connectionString = string.Empty;

        #endregion

        #region "constructor"

        public CatalogRepository()
        {
            connectionString = Helper.GetConnectionString();
        }
        #endregion

        /// <summary>
        ///Brings category Lisitng of merchant from Sp [CATEGORYLISTING_V1]
        /// </summary>
        /// <param name="categorys,languageId"></param>
        /// <returns></returns>
        public MerchantCategoryResponceModel GetCategoryByVendor(Categorys categorys, int languageId)
        {
            using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
            {
                try
                {
                    var result = db.Query<CatList>(CATEGORYLISTING_V1, new
                    {
                        categorys.ApiKey,
                        categorys.StoreId,
                        categorys.RestncookId,
                        categorys.PageIndex,
                        categorys.PageSize,
                        categorys.Keywords,
                        categorys.ParentCategoryId
                    }, commandType: CommandType.StoredProcedure);
                    return new MerchantCategoryResponceModel { ValidData = true, ResultData = "", catLists = result.ToList() };
                }
                catch (SqlException e)
                {
                    return new MerchantCategoryResponceModel { ValidData = false, ResultData = e.Message };
                }
            }
        }
        /// <summary>
        ///Brings  merchantId in string format from Sp [BUSINESSCATEGORY]
        /// </summary>
        /// <param name="merchantCategorysModelV2,languageId"></param>
        /// <returns></returns>
        public CustomerResponseModel GetMerchantCategories(MerchantCategorysModelV2 merchantCategorysModelV2, int languageId)
        {
            using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
            {
                try
                {
                    string result = db.Query<string>(BUSINESSCATEGORY, new
                    {
                        merchantCategorysModelV2.StoreId,
                        merchantCategorysModelV2.RestType,
                        latLongPos = merchantCategorysModelV2.LatPos + " " + merchantCategorysModelV2.LongPos
                    }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return new CustomerResponseModel { ValidData = true, ResultData = result };
                }
                catch (SqlException e)
                {
                    return new CustomerResponseModel { ValidData = false, ResultData = e.Message };
                }
            }
        }

        /// <summary>
        ///Brings Bussiness Category of merchant from Sp [BUSINESSCATEGORYLIST]
        /// </summary>
        /// <param name="merchantCategorysModelV2"></param>
        /// <returns></returns>
        public MerchantCategoryListResponceModelV1 GetMerchntCategoriesListById(MerchantCategorysModelV2 merchantCategorysModelV2)
        {
            using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
            {
                try
                {
                    var result2 = db.Query<MerchantCategoryListResponceModel>(BUSINESSCATEGORYLIST2, new
                    {
                        merchantCategorysModelV2.ApiKey,
                        merchantCategorysModelV2.StoreId,
                        merchantCategorysModelV2.RestType,
                        latlonpos = merchantCategorysModelV2.LatPos + " " + merchantCategorysModelV2.LongPos,
                        merchantCategorysModelV2.PageIndex,
                        merchantCategorysModelV2.PageSize

                    }, commandType: CommandType.StoredProcedure);
                    return new MerchantCategoryListResponceModelV1 { ValidData = true, ResultData = "", Categoriess = result2.ToList() };
                }
                catch (SqlException e)
                {
                    return new MerchantCategoryListResponceModelV1 { ValidData = false, ResultData = e.Message };

                }
            }
        }
     }
}
