﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB.Package;

namespace Nop.Data.Mapping.NB.Package
{
    /// <summary>
    /// Represents a Package
    /// </summary>
    public partial class NBPackageMap : NopEntityTypeConfiguration<NBPackage>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<NBPackage> builder)
        {
            builder.ToTable(NopMappingDefaults.PackageTable);
            builder.HasKey(p => p.Id);
            base.Configure(builder);
        }

        #endregion
    }
}