using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.RatingReview;

namespace Nop.Data.Mapping.RatingReview
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class RatingReviewsMap : NopEntityTypeConfiguration<RatingReviews>
    {
        /// <summary>
        /// Ctor
        /// </summary>
        public override void Configure(EntityTypeBuilder<RatingReviews> builder)
        {
            builder.ToTable("RatingReviews");
            builder.HasKey(pr => pr.Id);

            builder.HasOne(pr => pr.Customer)
                .WithMany()
                .HasForeignKey(pr => pr.CustomerId).IsRequired();

            builder.HasOne(pr => pr.Store)
                .WithMany()
                .HasForeignKey(pr => pr.StoreId).IsRequired();

            base.Configure(builder);
        }
    }
}