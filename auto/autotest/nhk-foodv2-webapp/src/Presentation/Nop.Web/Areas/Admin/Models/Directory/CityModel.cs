﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Directory
{
    /// <summary>
    /// Represents a state and province model
    /// </summary>
    public partial class CityModel : BaseNopEntityModel, ILocalizedModel<CityLocalizedModel>
    {
        #region Ctor

        public CityModel()
        {
            Locales = new List<CityLocalizedModel>();
            CitySearchModel = new CitySearchModel();
        }

        #endregion

        #region Properties

        public int StateId { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Countries.States.Fields.Cities")]
        public string Cities { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Countries.States.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Countries.States.Fields.Code")]
        public string Code { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Countries.States.Fields.Published")]
        public bool Published { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Countries.States.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        public IList<CityLocalizedModel> Locales { get; set; }
        public CitySearchModel CitySearchModel{ get; set; }
        #endregion
    }

    public partial class CityLocalizedModel : ILocalizedLocaleModel
    {
        public int LanguageId { get; set; }
        
        [NopResourceDisplayName("Admin.Configuration.Countries.States.Fields.Name")]
        public string Name { get; set; }
    }
}