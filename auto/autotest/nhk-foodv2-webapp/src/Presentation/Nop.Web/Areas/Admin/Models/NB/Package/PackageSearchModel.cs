﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB.Package
{
    /// <summary>
    /// Represents a package search model
    /// </summary>
    public partial class PackageSearchModel : BaseSearchModel
    {
        public int StoreId { get; set; }
    }
}