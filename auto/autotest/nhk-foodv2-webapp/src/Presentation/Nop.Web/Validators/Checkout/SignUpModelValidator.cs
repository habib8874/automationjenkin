﻿using System;
using System.Linq;
using FluentValidation;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.Checkout;

namespace Nop.Web.Validators.Checkout
{
    public partial class SignUpModelValidator : BaseNopValidator<SignUpModel>
    {
        public SignUpModelValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Email.Required"));
            RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Common.WrongEmail"));
            //RuleFor(x => x.Email).Must((x, context) =>
            //{
            //    string email = x.Email;
            //    string[] emailArray = email.Split('.');
            //    var count = emailArray.Count();
            //    bool result = localizationService.ValidateEmailExtension("." + emailArray[count - 1]);
            //    return result;
            //}).WithMessage(localizationService.GetResource("Account.Fields.Email.Required.ValidExt"));
            RuleFor(x => x.FirstName).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.FirstName.Required"));
            RuleFor(x => x.LastName).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.LastName.Required"));

            RuleFor(x => x.Password).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Password.Required"));
            RuleFor(x => x.Phone).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Customer.Phone.Required"));
            RuleFor(x => x.Phone).Length(10, 12).WithMessage(string.Format(localizationService.GetResource("Account.Fields.Customer.Phone.LengthValidation"), 10));

        }
    }
}