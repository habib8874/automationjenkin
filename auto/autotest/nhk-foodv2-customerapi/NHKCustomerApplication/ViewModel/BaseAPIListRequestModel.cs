﻿namespace NHKCustomerApplication.ViewModel
{
    public class BaseAPIListRequestModel: BaseAPIRequestModel
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
