﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Orders;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Order
{
    public partial class CustomerWalletListModel : BaseNopModel
    {
        public CustomerWalletListModel()
        {
            Wallets = new List<WalletDetailsModel>();
        }
        public IList<WalletDetailsModel> Wallets { get; set; }
        public string BalanceAmount { get; set; }

        #region Nested classes

        public partial class WalletDetailsModel : BaseNopEntityModel
        {
            public int CustomerId { get; set; }
            public int? OrderId { get; set; }
            public Decimal Amount { get; set; }
            public string DisplayAmount { get; set; }
            public string Note { get; set; }
            public bool Credited { get; set; }
            public DateTime TransactionOnUtc { get; set; }
            public int TransactionBy { get; set; }
            public bool FromWeb { get; set; }
        }
        #endregion
    }
}