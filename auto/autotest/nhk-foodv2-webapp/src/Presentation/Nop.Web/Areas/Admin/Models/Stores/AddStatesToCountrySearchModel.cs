﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Stores
{
    /// <summary>
    /// Represents a states search model to add to the country
    /// </summary>
    public partial class AddStatesToCountrySearchModel : BaseSearchModel
    {

        #region Properties

        public int SearchStoreId { get; set; }
        public int SearchCountryId { get; set; }

        #endregion
    }
}