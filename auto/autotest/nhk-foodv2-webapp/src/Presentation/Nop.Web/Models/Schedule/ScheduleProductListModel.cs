﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.Schedule
{
    public partial class ScheduleProductDataListModel : BasePagedListModel<ScheduleProductListModel>
    {
    }
    public class ScheduleProductListSearchModel : BaseSearchModel
    {
        public int scheduleId { get; set; }
    }
    public class ScheduleProductListModel : BaseNopModel
    {
        public int ProductId { get; set; }
        public string  Product { get; set; }
        public int  ScheduleId { get; set; }
        public bool IsFeaturedProduct { get; set; }
        public int DisplayOrder { get; set; }
        public int Id { get; set; }
    }
}
