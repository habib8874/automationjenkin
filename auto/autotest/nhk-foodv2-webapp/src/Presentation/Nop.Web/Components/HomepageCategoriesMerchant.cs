﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;

namespace Nop.Web.Components
{
    public class HomepageCategoriesMerchantViewComponent : NopViewComponent
    {
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly IWorkContext _workContext;

        public HomepageCategoriesMerchantViewComponent(ICatalogModelFactory catalogModelFactory, IWorkContext workContext)
        {
            this._catalogModelFactory = catalogModelFactory;
            this._workContext = workContext;
        }

        //public IViewComponentResult Invoke(string merchantid)
        public IViewComponentResult Invoke()
        {
            string merchantid = (_workContext.CurrentMerchant?.Id??0).ToString();  // Add currentvendor
            var model = _catalogModelFactory.PrepareHomepageCategoryMerchantModels(merchantid);
            if (!model.Any())
                return Content("");

            return View(model);
        }
    }
}
