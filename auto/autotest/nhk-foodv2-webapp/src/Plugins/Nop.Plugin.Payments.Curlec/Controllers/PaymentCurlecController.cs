﻿using System;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Payments.Curlec.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.Payments.Curlec.Controllers
{
    public class PaymentCurlecController : BasePaymentController
    {
        #region Fields

        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly IPaymentPluginManager _paymentPluginManager;
        private readonly IPermissionService _permissionService;
        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly IWebHelper _webHelper;
        private readonly CurlecPaymentSettings _curlecPaymentSettings;

        #endregion

        #region Ctor

        public PaymentCurlecController(IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IPaymentPluginManager paymentPluginManager,
            IPermissionService permissionService,
            ILocalizationService localizationService,
            INotificationService notificationService,
            ISettingService settingService,
            IStoreContext storeContext,
            IWebHelper webHelper,
            CurlecPaymentSettings curlecPaymentSettings)
        {
            _orderProcessingService = orderProcessingService;
            _orderService = orderService;
            _paymentPluginManager = paymentPluginManager;
            _permissionService = permissionService;
            _localizationService = localizationService;
            _notificationService = notificationService;
            _settingService = settingService;
            _storeContext = storeContext;
            _webHelper = webHelper;
            _curlecPaymentSettings = curlecPaymentSettings;
        }

        #endregion

        #region Methods

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var curlecPaymentSettings = _settingService.LoadSetting<CurlecPaymentSettings>(storeScope);

            var model = new ConfigurationModel
            {
                MerchantID = curlecPaymentSettings.MerchantID,
                EmployeeID = curlecPaymentSettings.EmployeeID,
                MerchantURL = curlecPaymentSettings.MerchantURL,
                MerchantCallbackURL = curlecPaymentSettings.MerchantCallbackURL,
                CollectionCallbackURL = curlecPaymentSettings.CollectionCallbackURL,
                EnrollmentCallbackURL = curlecPaymentSettings.EnrollmentCallbackURL,
                ActiveStoreScopeConfiguration = storeScope
            };

            if (storeScope <= 0)
                return View("~/Plugins/Payments.Curlec/Views/Configure.cshtml", model);

            model.MerchantID_OverrideForStore = _settingService.SettingExists(curlecPaymentSettings, x => x.MerchantID, storeScope);
            model.EmployeeID_OverrideForStore = _settingService.SettingExists(curlecPaymentSettings, x => x.EmployeeID, storeScope);
            model.MerchantURL_OverrideForStore = _settingService.SettingExists(curlecPaymentSettings, x => x.MerchantURL, storeScope);
            model.MerchantCallbackURL_OverrideForStore = _settingService.SettingExists(curlecPaymentSettings, x => x.MerchantCallbackURL, storeScope);
            model.CollectionCallbackURL_OverrideForStore = _settingService.SettingExists(curlecPaymentSettings, x => x.CollectionCallbackURL, storeScope);
            model.EnrollmentCallbackURL_OverrideForStore = _settingService.SettingExists(curlecPaymentSettings, x => x.EnrollmentCallbackURL, storeScope);

            return View("~/Plugins/Payments.Curlec/Views/Configure.cshtml", model);
        }

        [HttpPost]
        [AuthorizeAdmin]
        [AdminAntiForgery]
        [Area(AreaNames.Admin)]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var curlecPaymentSettings = _settingService.LoadSetting<CurlecPaymentSettings>(storeScope);

            //save settings
            curlecPaymentSettings.MerchantID = model.MerchantID;
            curlecPaymentSettings.EmployeeID = model.EmployeeID;
            curlecPaymentSettings.MerchantURL = model.MerchantURL;
            curlecPaymentSettings.MerchantCallbackURL = model.MerchantCallbackURL;
            curlecPaymentSettings.CollectionCallbackURL = model.CollectionCallbackURL;
            curlecPaymentSettings.EnrollmentCallbackURL = model.EnrollmentCallbackURL;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSettingOverridablePerStore(curlecPaymentSettings, x => x.MerchantID, model.MerchantID_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(curlecPaymentSettings, x => x.EmployeeID, model.EmployeeID_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(curlecPaymentSettings, x => x.MerchantURL, model.MerchantURL_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(curlecPaymentSettings, x => x.MerchantCallbackURL, model.MerchantCallbackURL_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(curlecPaymentSettings, x => x.CollectionCallbackURL, model.CollectionCallbackURL_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(curlecPaymentSettings, x => x.EnrollmentCallbackURL, model.EnrollmentCallbackURL_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }


        public IActionResult CurlecResponse()
        {
            var sellerOrderNo = _webHelper.QueryString<string>("fpx_sellerOrderNo");
            var debitAuthCode = _webHelper.QueryString<string>("fpx_debitAuthCode");

            var order = _orderService.GetOrderById(Convert.ToInt32(sellerOrderNo));

            if (debitAuthCode == "00")
            {
                var curlecMethod = _webHelper.QueryString<string>("curlec_method");
                var fpxTxnId = _webHelper.QueryString<string>("fpx_fpxTxnId");
                var sellerExOrderNo = _webHelper.QueryString<string>("fpx_sellerExOrderNo");
                var fpxTxnTime = _webHelper.QueryString<string>("fpx_fpxTxnTime");
                var sellerId = _webHelper.QueryString<string>("fpx_sellerId");
                var txnCurrency = _webHelper.QueryString<string>("fpx_txnCurrency");
                var txnAmount = _webHelper.QueryString<string>("fpx_txnAmount");
                //var buyerName = _webHelper.QueryString<string>("fpx_buyerName");
                var buyerBankId = _webHelper.QueryString<string>("fpx_buyerBankId");
                var type = _webHelper.QueryString<string>("fpx_type");

                var sb = new StringBuilder();
                sb.AppendLine("Curlec Response:");
                sb.AppendLine("curlec_method: " + curlecMethod);
                sb.AppendLine("fpx_fpxTxnId: " + fpxTxnId);
                sb.AppendLine("fpx_sellerExOrderNo: " + sellerExOrderNo);
                sb.AppendLine("fpx_fpxTxnTime: " + sellerId);
                sb.AppendLine("fpx_sellerOrderNo: " + sellerOrderNo);
                sb.AppendLine("fpx_sellerId: " + sellerId);
                sb.AppendLine("fpx_txnCurrency: " + txnCurrency);
                sb.AppendLine("fpx_txnAmount: " + txnAmount);
                //sb.AppendLine("fpx_buyerName: " + buyerName);
                sb.AppendLine("fpx_buyerBankId: " + buyerBankId);
                sb.AppendLine("fpx_debitAuthCode: " + debitAuthCode);
                sb.AppendLine("fpx_type: " + type);

                order.OrderNotes.Add(new OrderNote
                {
                    Note = sb.ToString(),
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });

                _orderService.UpdateOrder(order);

                //mark order as paid
                order.AuthorizationTransactionId = fpxTxnId;
                order.OrderStatusId = (int)OrderStatus.Received;
                _orderService.UpdateOrder(order);
                _orderProcessingService.MarkOrderAsPaid(order);

                return RedirectToRoute("CheckoutCompleted", new { orderId = order.Id });
            }

            //order note
            order.OrderNotes.Add(new OrderNote
            {
                Note = "There is some issue with the payment gateway, please contact them on their support.",
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });

            _orderService.UpdateOrder(order);

            return RedirectToAction("Index", "Home", new { area = string.Empty });
        }

        #endregion
    }
}