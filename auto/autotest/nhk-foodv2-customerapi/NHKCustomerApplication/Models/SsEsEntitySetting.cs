﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsEsEntitySetting
    {
        public int Id { get; set; }
        public int EntityType { get; set; }
        public int EntityId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
