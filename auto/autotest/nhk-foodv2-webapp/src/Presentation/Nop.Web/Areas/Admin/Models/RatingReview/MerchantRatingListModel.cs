﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.RatingReview
{
    public partial class MerchantRatingListSearchModel : BaseSearchModel
    {
        public MerchantRatingListSearchModel()
        {
            AvailableVendors = new List<SelectListItem>();
            AvailableStores = new List<SelectListItem>();
            AvailableApprovedOptions = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Admin.RatingReview.List.CreatedOnFrom")]
        [UIHint("DateNullable")]
        public DateTime? CreatedOnFrom { get; set; }

        [NopResourceDisplayName("Admin.RatingReview.List.CreatedOnTo")]
        [UIHint("DateNullable")]
        public DateTime? CreatedOnTo { get; set; }

        [NopResourceDisplayName("Admin.RatingReview.List.SearchText")]
        public string SearchText { get; set; }

        [NopResourceDisplayName("Admin.RatingReview.List.SearchStore")]
        public int SearchStoreId { get; set; }

        [NopResourceDisplayName("Admin.RatingReview.List.SearchVendor")]
        public int SearchVendorId { get; set; }

        [NopResourceDisplayName("Admin.RatingReview.List.SearchProduct")]
        public int SearchProductId { get; set; }

        [NopResourceDisplayName("Admin.RatingReview.List.SearchApproved")]
        public int SearchApprovedId { get; set; }

        //vendor
        public bool IsLoggedInAsVendor { get; set; }

        public IList<SelectListItem> AvailableStores { get; set; }
        public IList<SelectListItem> AvailableApprovedOptions { get; set; }

        public IList<SelectListItem> AvailableVendors { get; set; }
        public bool IsCurrentVendor { get; set; }
        [NopResourceDisplayName("Admin.RatingReviews.List.SearchMerchant")]
        public int SearchMerchantId { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.List.MerchantName")]
        public string MerchantName { get; set; }

        [NopResourceDisplayName("Admin.RatingReviews.List.ReviewOptions")]
        public string ReviewOptions { get; set; }
        public int AdminUserType { get; set; }

        public bool HideStoresList { get; set; }
    }
    public partial class MerchantRatingListModel : BasePagedListModel<MerchantRatingModel>
    {
    }
}