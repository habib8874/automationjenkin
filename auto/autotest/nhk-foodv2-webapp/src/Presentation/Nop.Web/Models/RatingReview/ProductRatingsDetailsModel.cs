﻿using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Models.RatingReview
{
    public partial class ProductRatingsDetailsModel : BaseNopEntityModel
    {
        public ProductRatingsDetailsModel()
        {
            RatingsList = new List<ProductRatingsModel>();
        }
        public string ProductName { get; set; }
        public string ShortDescription { get; set; }
        public string ProductPictureUrl { get; set; }
        public decimal TotalRatingPercent { get; set; }
        public int TotalReviews { get; set; }
        public bool DisableBuyButton { get; set; }
        public IList<ProductRatingsModel> RatingsList { get; set; }
    }

    #region Nested classes
    public partial class ProductRatingsModel 
    {
        public string CustomerPictureUrl { get; set; }
        public string CustomerName { get; set; }
        public string Review { get; set; }

        public decimal RatingPercent { get; set; }
    }
    #endregion
}