﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Stores;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Directory;
using Nop.Web.Areas.Admin.Models.Stores;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.DataTables;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the store model factory implementation
    /// </summary>
    public partial class StoreModelFactory : IStoreModelFactory
    {
        #region Fields

        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedModelFactory _localizedModelFactory;
        private readonly IStoreService _storeService;
        private readonly IStoreModelFactory _storeModelFactory;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public StoreModelFactory(IBaseAdminModelFactory baseAdminModelFactory,
            ILocalizationService localizationService,
            ILocalizedModelFactory localizedModelFactory,
            IStoreService storeService,
            IStateProvinceService stateProvinceService,
            IWorkContext workContext)
        {
            _baseAdminModelFactory = baseAdminModelFactory;
            _localizationService = localizationService;
            _localizedModelFactory = localizedModelFactory;
            _storeService = storeService;
            _stateProvinceService = stateProvinceService;
            _workContext = workContext;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare store search model
        /// </summary>
        /// <param name="searchModel">Store search model</param>
        /// <returns>Store search model</returns>
        public virtual StoreSearchModel PrepareStoreSearchModel(StoreSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged store list model
        /// </summary>
        /// <param name="searchModel">Store search model</param>
        /// <returns>Store list model</returns>
        public virtual StoreListModel PrepareStoreListModel(StoreSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get stores
            var stores = _storeService.GetAllStores(loadCacheableCopy: false).ToPagedList(searchModel);

            //sub admin segaragetion
            if (_workContext.SubAdmin.IsSubAdminRole)
                stores = stores.Where(s => s.Id == _workContext.GetCurrentStoreId).ToList().ToPagedList(searchModel);

            //prepare list model
            var model = new StoreListModel().PrepareToGrid(searchModel, stores, () =>
            {
                //fill in model values from the entity
                return stores.Select(store => store.ToModel<StoreModel>());
            });

            return model;
        }

        /// <summary>
        /// Prepare store model
        /// </summary>
        /// <param name="model">Store model</param>
        /// <param name="store">Store</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Store model</returns>
        public virtual StoreModel PrepareStoreModel(StoreModel model, Store store, bool excludeProperties = false)
        {
            Action<StoreLocalizedModel, int> localizedModelConfiguration = null;

            if (store != null)
            {
                //fill in model values from the entity
                model = model ?? store.ToModel<StoreModel>();

                //define localized model configuration action
                localizedModelConfiguration = (locale, languageId) =>
                {
                    locale.Name = _localizationService.GetLocalized(store, entity => entity.Name, languageId, false, false);
                };

                model.CountryStateSearchModel.StoreId = model.Id;
                model.CountryStateSearchModel.CountryId = model.CountryId;
            }
            model.CountryStateSearchModel.SetGridPageSize();

            //prepare available languages
            _baseAdminModelFactory.PrepareLanguages(model.AvailableLanguages, defaultItemText: _localizationService.GetResource("Admin.Configuration.Stores.Fields.DefaultLanguage.DefaultItemText"));
            //prepare available time zones
            _baseAdminModelFactory.PrepareTimeZones(model.AvailableTimeZones, false);
            //prepare localized models
            if (!excludeProperties)
                model.Locales = _localizedModelFactory.PrepareLocalizedModels(localizedModelConfiguration);

            return model;
        }

        #endregion

        #region Country state mapping
        public virtual CountryStateListModel PrepareCountryStateListModel(CountryStateSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get states
            var states = _stateProvinceService.GetStoreCountryStateMappingsByCountryId(searchModel.CountryId, searchModel.StoreId, true).ToPagedList(searchModel);

            //prepare grid model
            var model = new CountryStateListModel().PrepareToGrid(searchModel, states, () =>
            {
                return states.Select(s =>
                {
                    //fill in model values from the entity
                    var state = new CountryStateModel();
                    state.Id = s.Id;
                    state.StoreId = s.StoreId;
                    state.CountryId = s.CountryId;
                    state.CountryName = s.CountryName;
                    state.StateId = s.StateId;
                    state.StateName = s.StateName;
                    state.IsActive = s.IsActive.HasValue ? s.IsActive.Value : false;
                    return state;
                });
            });

            return model;
        }

        public virtual AddStatesToCountryListModel PrepareAddStatesToCountryListModel(AddStatesToCountrySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var states = _stateProvinceService.GetStateProvincesByCountryId(searchModel.SearchCountryId, showHidden: true).ToPagedList(searchModel);

            //prepare list model
            var model = new AddStatesToCountryListModel().PrepareToGrid(searchModel, states, () =>
            {
                //fill in model values from the entity
                return states.Select(state => state.ToModel<StateProvinceModel>());
            });

            return model;
        }
        #endregion
    }
}