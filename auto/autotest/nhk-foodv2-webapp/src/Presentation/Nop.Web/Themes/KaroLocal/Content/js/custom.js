$(function () {

  $('.threeItemSlider').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.fourItemSlider').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
    ]
  });

  $('.fiveItemSlider').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 5,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.oneItemSlider').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1
  });

/*var hoverOrClick = function () {
  $(this).siblings(".notmobile").slideToggle();
}
$(".shopByCategoryDrop").click(hoverOrClick).mouseenter(hoverOrClick);
$(".top-menu.notmobile").mouseleave(function () {
    $(this).slideUp();
}); */

  $(document).ready(function () { 
    $(".shopByCategoryDrop").mouseenter(function () {
      $(this).siblings(".categoryDropMenu").slideDown("slow");
    });
    $(".categoryDropMenu").mouseleave(function () {
      $(this).slideUp("slow");
    });
    //$(".header").mouseenter(function () {
    //  $(".categoryDropMenu").slideUp("slow");
    //});  
  });


  $(document).ready(function () { 
  var id = null;
  function myMove() {
    var elem = document.getElementById("myAnimation");
    var pos = 0;
    clearInterval(id);
    id = setInterval(frame, 10);
    function frame() {
      if (pos == 350) {
        clearInterval(id);
      } else {
        pos++;
        elem.style.top = pos + 'px';
        elem.style.left = pos + 'px';
      }
    }
  }
  });
     
     


  $(".header-menu .notmobile").wrap("<div class='categoryDropMenu'></div>")
  //$(".header-menu .notmobile").after("<div class='categoryDisMenu'> </div>")
  if ($(".header-menu > .notmobile > li").length > 11) {
    $(".header-menu > .notmobile > li").eq("10").nextAll("li").hide();
    $(".header-menu > .notmobile > li").eq("10").after("<li class='viewAll'><a href='#'>View all</a></li>");
  }
  $(".header-menu > .notmobile > li > a").each(function () {
    if ($(this).siblings(".sublist")) {
      $(this).parent("li").addClass("active");
      $(this).siblings(".sublist").show();
      return false;
    }
  });
  
  $(".header-menu > .notmobile > li > a").each(function () {
    if ($(this).siblings(".sublist")) {
      $(this).parent("li").addClass("active");
      $(this).siblings(".sublist").show();
      $("body .categoryDisMenu").load($(this).attr("href") + " .category-description");
      $(".first-level > li > a").each(function () {
        if ($(this).siblings(".sublist")) {
          $(this).parent("li").addClass("active");
          $(this).siblings(".sublist").show();
        }
        return false;
      });
    }
    return false;
  });

  $(".header-menu .notmobile > li > a").on("mouseenter", function () {
    $(this).parent("li").siblings("li").removeClass("active");
    $(this).parent("li").siblings("li").find(".sublist").hide();
    $(this).parent("li").addClass("active");
    $(this).siblings(".sublist").show();
    $(this).siblings(".sublist").find(".sublist").show();
    $(this).siblings(".sublist").find(".sublist").parent("li").siblings("li").removeClass("active");
    $(this).siblings(".sublist").find(".sublist").parent("li").addClass("active");
    $("body .categoryDisMenu").load($(this).attr("href") + " .category-description");
  })
  $(".header-menu .notmobile .first-level > li > a").on("mouseenter", function () {
    $(this).parent("li").siblings("li").removeClass("active");
    $(this).parent("li").siblings("li").find(".sublist").hide();
    $(this).parent("li").addClass("active");
    $(this).siblings(".sublist").show();
    $(this).siblings(".sublist").find(".sublist").show();
    $(this).siblings(".sublist").find(".sublist").parent("li").siblings("li").removeClass("active");
    $(this).siblings(".sublist").find(".sublist").parent("li").addClass("active");
  })

  $("body").on("click", ".header-menu > ul > li.viewAll", function () {
    $(this).nextAll("li").show();
    $(this).remove();
  })



  $("#billing-address-select option").each(function () {
    var adressV = $(this).attr("value");
    var adressH = $(this).html();
    if (adressV == "") {
      $(".addressLists").append('<div class="newAddressBox" value="' + adressV + '" >' + adressH + '<button class="newAddressBtn">Add Address</button></div>');
    } else {
      $(".addressLists").append('<div class="addressB" value="' + adressV + '" >' + adressH + '<button class="addressBtnT">Delivery here</button></div>');
    }

  });

  $(".addressLists .addressB .addressBtnT").click(function () {
    $("#billing-new-address-form").hide();
    var cchbv = $(this).parent(".addressB").attr("value");
    $("#billing-address-select option").each(function () {
      if ($(this).attr("value") == cchbv) {
        $(this).attr('selected', true);
      }
    });
  });
  $(".newAddressBtn").click(function () {
    $("#billing-new-address-form").show();
  });













  if ($("body .cart-detail").length > 0) {

    if ($(".mini-shopping-cart").html().trim().length === 0) {
      $(".cart-filled").hide();
      $(".headerCart").hide();
      $(".cart-empty").show();
    } else {
      $(".cart-empty").hide();
      $(".cart-filled").show();
      $(".item-count").html($(".topBucketCount").html());
      $(".headerCart").remove();
      $(".checkoutBtn").html($(".totalAndCharge .buttons").html())
      $(".cartItemTotal .cartItemTotalRow:first-child .cartItemTotValRight").html($(".totalAndCharge .totalsRow:first-child span").html());
      $(".cartItemTotalMain .cartTotalVal").html($(".totalAndCharge .totalsRow:first-child span").html());
      $(".cartProduct").remove();
      $(".mini-shopping-cart .item").each(function () {
        const ctImg = $(this).find(".picture").html();
        const proName = $(this).find(".name").html();
        const proQt = $(this).find(".qtyCartP").html();
        const flyMlt = $(this).find(".flyMlt").html();
        const price = $(this).find(".price").html();

        $(".cart-detail .cart-filled").prepend(`<div class="cartProduct">
                                      <div class="cartProductDetails">
                                          <div class="cartProImg">${ctImg}</div>
                                          <div class="cartProName">
                                              <span>Fresho</span>${proName}
                                          </div>
                                          <div class="cartProInDe">
                                              <span class="minusCartP"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-dash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                          <path fill-rule="evenodd" d="M3.5 8a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.5-.5z"></path>
                                      </svg></span>
                                              <span class="qtyCartP">${proQt}</span>
                                              <span class="plusCartP"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z"></path>
                                              <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z"></path>
                                          </svg></span>
                                          </div>
                                      </div>
                                      <div class="cartProductQty">
                                          <div class="cartMltqt">${flyMlt}</div>
                                          <div class="cartMltqtRight">${price} <span class="cartProductRemove"><i class="fa fa-trash-o"></i></span></div>
                                      </div>
                                  </div>`)
      });
    }
  } else {
    $(".wishlistCart .headerCart").show();
  }

  $(".productWishlist input").click(function () {
    $(this).toggleClass("active")
  })

  if ($("body .ico-login").length === 0) {
    $(".userLoginCheckout").show();
  } else {
    $(".notLoginUser").show();
  }
  $(".changeAddresBtn").click(function () {
    $(".addressAreaFilled").hide();
    $(".addAddressArea").show();
  });




  $(window).on("scroll touchmove", function () {
    $('.header.stickyHeader, .header-cont').toggleClass('sticky', $(document).scrollTop() > 0);
    $('.checkOutBlock').toggleClass('sticky', $(document).scrollTop() > 0);

  });





  $(document).on("click", ".checkOutSection .checkOutC1", function () {
    if ($(".checkOutSection .catOpt").length === 0) {
      $(this).parent(".checkOutSection").prepend('<div class="catOpt"></div>');
    }
    $("body").addClass("deviceCartActive");
    $(this).parent(".checkOutSection").addClass("catOpenDevic");
    $(this).parent(".checkOutSection").removeClass("checkMiniCartMobileOpen");
  });
  $(document).on("click", ".catOpt", function () {
    $('body').removeClass("deviceCartActive");
    $(".checkOutSection").removeClass("catOpenDevic");
    $(".checkOutSection").addClass("checkMiniCartMobileOpen");
    $('.tooltripInfo').removeClass('active');
    $('.customToolTripBox').slideUp();
    $(this).remove();
  });
  if ($(".checkOutSection .catOpt").length === 0) {
    $(this).parent(".checkOutSection").prepend('<div class="catOpt"></div>');
  }
  $(document).on("click", ".catOpt", function () {
    $('body').removeClass("deviceCartActive");
    $(".checkOutSection").removeClass("catOpenDevic");
    $(".checkOutSection").addClass("checkMiniCartMobileOpen");
    $('.tooltripInfo').removeClass('active');
    $('.customToolTripBox').slideUp();
    $(this).remove();
  });
})
function miniCartMobileC() {
  if (window.innerWidth < 992) {
    $(".checkOutSection").addClass("checkMiniCartMobileOpen");
  } else {
    $(".checkOutSection").removeClass("checkMiniCartMobileOpen");
  }
}
miniCartMobileC();
  //override global ajax loader
  function displayAjaxLoading(display) {
    if (display) {
      $('.ajax-loading-block-window').show();
      $('.ajax-overlay-window').show();
    }
    else {
      $('.ajax-loading-block-window').fadeOut();
      $('.ajax-overlay-window').fadeOut();
    }
  }

 

