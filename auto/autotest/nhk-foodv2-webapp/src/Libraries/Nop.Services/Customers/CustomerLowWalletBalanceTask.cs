﻿using System;
using Nop.Core.Domain.Customers;
using Nop.Services.Tasks;

namespace Nop.Services.Customers
{
   public partial class CustomerLowWalletBalanceTask: IScheduleTask
    {
        #region Fields

        private readonly ICustomerService _customerService;

        #endregion

        #region Ctor

        public CustomerLowWalletBalanceTask(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Executes a task
        /// </summary>
        public void Execute()
        {
            _customerService.SendCustomerLowBalanceNotification();
        }

        #endregion
    }
}
