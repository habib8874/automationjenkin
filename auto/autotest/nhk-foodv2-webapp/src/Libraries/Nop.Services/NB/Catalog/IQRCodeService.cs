﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.NB.Catalog.QRCode;

namespace Nop.Services.NB.Catalog
{
    /// <summary>
    /// QRCode service interface
    /// </summary>
    public partial interface IQRCodeService
    {

        #region QRCode

        /// <summary>
        /// Inserts a QRCode
        /// </summary>
        /// <param name="QRCode">QRCode</param>
        void InsertQRCode(QRCode qRCode);

        /// <summary>
        /// Update the QRCode 
        /// </summary>
        /// <param name="QRCode">QRCode</param>
        void UpdateQRCode(QRCode qRCode);

        /// <summary>
        /// Delete a qRCode
        /// </summary>
        /// <param name="qRCode">qRCode</param>
        void DeleteQRCode(QRCode qRCode);

        /// <summary>
        /// GetAllQRCode
        /// </summary>
        /// <param name="vendorId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="getOnlyTotalCount"></param>
        /// <returns></returns>
        IPagedList<QRCode> GetAllQRCode(int vendorId = 0,
          int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false);

        /// <summary>
        /// Gets a QRCode
        /// </summary>
        /// <param name="qRCodeId">QRCode identifier</param>
        /// <returns>A QRCode</returns>
        QRCode GetQRCodeById(int qRCodeId);

        /// <summary>
        /// GetQRCodeByIds
        /// </summary>
        /// <param name="qRCodeIds"></param>
        /// <returns></returns>
        IList<QRCode> GetQRCodeByIds(int[] qRCodeIds);

        #endregion
    }
}