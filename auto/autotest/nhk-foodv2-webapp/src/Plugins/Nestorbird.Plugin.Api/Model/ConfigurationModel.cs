﻿using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nestorbird.Plugin.Api.Model
{
    public class ConfigurationModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugin.Api.Fields.MerchantAppWebKey")]
        public string MerchantAppWebKey { get; set; }
        public bool MerchantAppWebKey_OverrideForStore { get; set; }
    }
}
