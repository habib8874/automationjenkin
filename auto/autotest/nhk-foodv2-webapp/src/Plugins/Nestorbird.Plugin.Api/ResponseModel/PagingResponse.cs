﻿namespace Nestorbird.Plugin.Api.ResponseModel
{
    public class PagingResponse
    {
        public int TotalCount { get; set; }

        public int TotalPages { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }
    }
}
