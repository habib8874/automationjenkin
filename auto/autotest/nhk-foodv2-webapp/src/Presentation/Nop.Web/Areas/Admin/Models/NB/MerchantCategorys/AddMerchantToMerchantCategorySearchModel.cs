﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.MerchantCategorys
{
    /// <summary>
    /// Represents a merchant search model to add to the Merchant Category
    /// </summary>
    public partial class AddMerchantToMerchantCategorySearchModel : BaseSearchModel
    {
        #region Properties

        [NopResourceDisplayName("NB.Admin.MerchantCategoryMerchant.Merchants.List.SearchMerchantName")]
        public string SearchMerchantName { get; set; }

        public int StoreId { get; set; }

        #endregion
    }
}