﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsPrRibbonPicture
    {
        public SsPrRibbonPicture()
        {
            SsPrCategoryPageRibbon = new HashSet<SsPrCategoryPageRibbon>();
            SsPrProductPageRibbon = new HashSet<SsPrProductPageRibbon>();
        }

        public int Id { get; set; }
        public int PictureId { get; set; }

        public virtual ICollection<SsPrCategoryPageRibbon> SsPrCategoryPageRibbon { get; set; }
        public virtual ICollection<SsPrProductPageRibbon> SsPrProductPageRibbon { get; set; }
    }
}
