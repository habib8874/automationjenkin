﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.BookingTable
{
    public partial class BookingTableMaster:BaseEntity
    {
        public int StoreId { get; set; }
        public int VendorId { get; set; }
        public int TableNo { get; set; }
        public int Seats { get; set; }
        public bool Status { get; set; }
        public decimal Availability { get; set; }
        public DateTime CreatedAt { get; set; }
        public TimeSpan TableAvailableFrom { get; set; }
        public TimeSpan TableAvailableTo { get; set; }
        public int? BookingDetailsId { get; set; }
    }
}
