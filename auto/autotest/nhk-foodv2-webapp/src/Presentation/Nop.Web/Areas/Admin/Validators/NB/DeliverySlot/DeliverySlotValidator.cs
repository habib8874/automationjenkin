﻿using FluentValidation;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Areas.Admin.Models.NB;
using Nop.Web.Framework.Validators;

namespace Nop.Web.Areas.Admin.Validators.NB
{
    public partial class DeliverySlotValidator : BaseNopValidator<DeliverySlotModel>
    {
        public DeliverySlotValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("NB.Admin.DeliverySlot.Fields.Name.Required"));
        }
    }
}