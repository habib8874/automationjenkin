﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Data;
using Nop.Services.Stores;
using Nop.Core.Infrastructure;
using Nop.Core.Data;
using Nop.Core.Domain.CustomEntities;
using Nop.Core.Data.Extensions;
using Nop.Core.Domain.Vendors;
using Nop.Services.Common;
using Nop.Core;
using Nop.Services.Vendors;

namespace Nop.Web.Components
{
    public class HomepageMerchantCategorysViewComponent : NopViewComponent
    {
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly IStoreService _storeService;
        private readonly ICommonService _commonService;
        private readonly IWorkContext _workContext;

        public HomepageMerchantCategorysViewComponent(ICatalogModelFactory catalogModelFactory, 
            IStoreService storeService,
            ICommonService commonService,
            IWorkContext workContext)
        {
            _catalogModelFactory = catalogModelFactory;
            _storeService = storeService;
            _commonService = commonService;
            _workContext = workContext;
        }

        public IViewComponentResult Invoke(string latlong, int orderType, bool skipLocationSearch)
        {            
            var listMerchantCategoryIds = _commonService.GetMerchantCategorysIdsByLongLat(_workContext.GetCurrentStoreId, orderType, (latlong != null ? latlong : "")  , skipLocationSearch);
            var model = _catalogModelFactory.PrepareMerchantCategorys(listMerchantCategoryIds);
            if (!model.Any())
                return Content("");
            return View(model);
        }
    }
}