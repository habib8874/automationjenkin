﻿namespace Nestorbird.Plugin.Api.RequestModel.Language
{
    public class LocalizedResourceRequest
    {
        public string Key { get; set; }
    }
}
