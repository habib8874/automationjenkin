﻿using NHKCustomerApplication.Models;
using NHKCustomerApplication.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace NHKCustomerApplication.Utilities
{
    public static class LanguageHelper
    {
        public static string GetDefaultLangCode(int storeId,int customerId, ecuadordevContext db)
        {
            var defaultLangCode = db.GenericAttribute.FirstOrDefault(x=>x.Key=="DefaultCustLanguageCode"
                            && x.KeyGroup=="Customer"
                            && x.EntityId== customerId
                          && x.StoreId == storeId)?.Value ?? "";

            if (string.IsNullOrEmpty(defaultLangCode))
            {
                defaultLangCode =db.Store.Join(db.Language,s=>s.DefaultLanguageId,l=>l.Id,(s,l)=> new { Code = l.UniqueSeoCode }).FirstOrDefault()?.Code ?? "";
            }
            return defaultLangCode;

        }

        public static int GetIdByLangCode(int storeId, string langCode, ecuadordevContext db)
        {
            int languageId = 0;
            if (string.IsNullOrEmpty(langCode))
            {
                languageId= db.Store.FirstOrDefault(x => x.Id == storeId)?.DefaultLanguageId ?? 0;
                
                if (languageId > 0)
                    return languageId;
            }
            
            langCode = !string.IsNullOrEmpty(langCode) ? langCode : "en";
            languageId = db.Language.Where(x => (x.UniqueSeoCode.ToUpper() == langCode.ToUpper())
                                                       && (db.StoreMapping.Any(y => y.StoreId == storeId && y.EntityName == "Language" && y.EntityId == x.Id) || !x.LimitedToStores)
                                                     ).FirstOrDefault()?.Id??0;

            if (languageId==0)
            {
                languageId = db.Store.FirstOrDefault(x => x.Id == storeId)?.DefaultLanguageId ?? 0;
            }
            return languageId;

        }

        public static List<Language> GetAllLanguages(int storeId,string code, ecuadordevContext db)
        {
            var languages= db.Language.Where(x => (string.IsNullOrEmpty(code) || x.UniqueSeoCode.ToUpper() == code.ToUpper())
                                                      && (db.StoreMapping.Any(y => y.StoreId == storeId && y.EntityName == "Language" && y.EntityId==x.Id) || !x.LimitedToStores)
                                                     ).ToList();
            return languages;
        }

        public static List<LanguagesModel> GetAllLanguagesForList(int storeId, ecuadordevContext db)
        {
            var languages = db.Language.Where(x => x.Published && (!x.LimitedToStores || db.StoreMapping.Any(y => y.StoreId == storeId && y.EntityName == "Language" && y.EntityId == x.Id))).OrderBy(x => x.DisplayOrder)
                  .Select(l => new LanguagesModel { Id = l.Id, UniqueSeoCode = l.UniqueSeoCode, Name = l.Name }).ToList();
            return languages;
        }

        public static string GetLocalizedValueMessageTemplate(int entityId, string localeKey, int languageId, string defaultValue, ecuadordevContext db)
        {
            return GetLocalizedValue(entityId, localeKey, "MessageTemplate", languageId, defaultValue, db);
        }

        public static string GetLocalizedValueProduct(int entityId, string localeKey, int languageId, string defaultValue, ecuadordevContext db)
        {
            return GetLocalizedValue(entityId, localeKey, "Product", languageId, defaultValue, db);
        }

        public static string GetLocalizedValueCategory(int entityId, string localeKey, int languageId, string defaultValue, ecuadordevContext db)
        {
            return GetLocalizedValue(entityId, localeKey, "Category", languageId, defaultValue, db);
        }
        public static string GetLocalizedValueMerchantReview(int entityId, string localeKey, int languageId, string defaultValue, ecuadordevContext db)
        {
            return GetLocalizedValue(entityId, localeKey, "ReviewOption", languageId, defaultValue, db);
        }
        public static string GetLocalizedValueAgentReview(int entityId, string localeKey, int languageId, string defaultValue, ecuadordevContext db)
        {
            return GetLocalizedValue(entityId, localeKey, "ReviewOption", languageId, defaultValue, db);
        }

        public static string GetLocalizedValue(int entityId, string localeKey, string localeKeyGroup, int languageId, string defaultValue, ecuadordevContext db)
        {
            var localizedValue = db.LocalizedProperty.Join(db.Language, s => s.LanguageId, l => l.Id, (s, l) => new { LanguageId = l.Id, LocalizedData = s }).Where(x => x.LanguageId == languageId &&
                        x.LocalizedData.LocaleKey == localeKey &&
                        x.LocalizedData.LocaleKeyGroup == localeKeyGroup &&
                        x.LocalizedData.EntityId == entityId
            ).FirstOrDefault()?.LocalizedData?.LocaleValue ?? defaultValue;
            return localizedValue;
        }

        public static string GetResourseValueByName(int storeId, string resourceName, int languageId, ecuadordevContext db)
        {
            var localizedValue = db.LocaleStringResource.Where(x=>(x.StoreId==storeId || x.StoreId==0) &&
            (x.ResourceName??"").ToLower()== (resourceName??"").ToLower() &&
            x.LanguageId==languageId).OrderByDescending(x=>x.StoreId).FirstOrDefault()?.ResourceValue?? resourceName;
            return localizedValue;
        }
        public static TopicDetailsModel GetTopicContent(string url, int languageId, int storeId, ecuadordevContext db)
        {
            string topicSeName = !string.IsNullOrEmpty(url) ? (url.Split('/').LastOrDefault()) : string.Empty;

            var dataTopic = (from t in db.Topic
                             join sn in db.UrlRecord
                             on t.Id equals sn.EntityId
                             where sn.EntityName == "Topic" &&
                             sn.Slug != null &&
                             sn.Slug.ToLower() == topicSeName.ToLower() &&
                             (db.StoreMapping.Any(y => y.StoreId == storeId && y.EntityId == t.Id && y.EntityName == "Topic") || !t.LimitedToStores)
                             select new TopicDetailsModel
                             {
                                 Heading = GetLocalizedValue(t.Id, "Title", "Topic", languageId, t.Title, db),
                                 BodyContent = GetLocalizedValue(t.Id, "Body", "Topic", languageId, t.Body, db)
                             }).FirstOrDefault();

            return dataTopic;
        }
        public static TopicDetailsModel GetTopicContent(string helpUrl, string privacyUrl, string supportchatUrl, string termsUrl, string topicType, int languageId, int storeId, ecuadordevContext db)
        {
            if (topicType == "HELP")
                return GetTopicContent(helpUrl, languageId, storeId, db);
            else if (topicType == "PRIVACY")
                return GetTopicContent(privacyUrl, languageId, storeId, db);
            else if (topicType == "TERMS")
                return GetTopicContent(termsUrl, languageId, storeId, db);
            else if (topicType == "CONTACTUS")
                return GetTopicContent(supportchatUrl, languageId, storeId, db);

            return new TopicDetailsModel();
        }
    }
}