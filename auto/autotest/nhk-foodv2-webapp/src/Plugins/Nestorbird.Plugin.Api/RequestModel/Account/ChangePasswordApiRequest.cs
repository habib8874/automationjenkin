﻿namespace Nestorbird.Plugin.Api.RequestModel.Account
{
    public class ChangePasswordApiRequest
    {
        public int CustomerId { get; set; }

        public string OldPassword { get; set; }

        public string NewPassword { get; set; }
    }
}
