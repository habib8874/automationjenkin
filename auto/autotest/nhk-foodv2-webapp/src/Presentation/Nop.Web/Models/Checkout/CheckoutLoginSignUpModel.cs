﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Shipping;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Validators.Checkout;

namespace Nop.Web.Models.Checkout
{
    public partial class CheckoutLoginSignUpModel : BaseNopModel
    {
        public CheckoutLoginSignUpModel()
        {
            AvailableISD = new List<SelectListItem>();
            SignUpModel = new SignUpModel();
        }
        public int ISDId { get; set; }
        public IList<SelectListItem> AvailableISD { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneOrEmail { get; set; }
        public string Password { get; set; }
        public string OTP { get; set; }
        public bool ShowOTP { get; set; }
        public bool IsEmail { get; set; }
        public bool IsLogedIn { get; set; }
        public bool IsValid { get; set; }
        public string Message{ get; set; }

        public SignUpModel SignUpModel { get; set; }

    }

    //[Validator(typeof(SignUpModelValidator))]
    public partial class SignUpModel : BaseNopModel
    {
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [NoTrim]
        public string Password { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        public bool IsValid { get; set; }
        public bool IsOTP { get; set; }
        public string OTP { get; set; }
        public int CustomerId { get; set; }
        public string Message { get; set; }
    }
}