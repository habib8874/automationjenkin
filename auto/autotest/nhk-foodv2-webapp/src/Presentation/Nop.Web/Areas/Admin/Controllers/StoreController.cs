﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Stores;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Services.Customers;
using System.Collections.Generic;
using Nop.Core.Data;
using Nop.Services.Directory;
using Nop.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc;
using Nop.Core.Domain.Directory;
using Nop.Core;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class StoreController : BaseAdminController
    {
        #region Fields

        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IStoreModelFactory _storeModelFactory;
        private readonly IStoreService _storeService;
        private readonly ICustomerService _customerService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IDbContext _dbContext;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public StoreController(ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService,
            INotificationService notificationService,
            IPermissionService permissionService,
            ISettingService settingService,
            IStoreModelFactory storeModelFactory,
            IStoreService storeService,
            ICustomerService customerService,
            ICountryService countryService,
            IStateProvinceService stateProvinceService,
            IDbContext dbContext,
            IWorkContext workContext)
        {
            _customerActivityService = customerActivityService;
            _localizationService = localizationService;
            _localizedEntityService = localizedEntityService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _settingService = settingService;
            _storeModelFactory = storeModelFactory;
            _storeService = storeService;
            _customerService = customerService;
            _countryService = countryService;
            _stateProvinceService = stateProvinceService;
            _dbContext = dbContext;
            _workContext = workContext;
        }

        #endregion

        #region Utilities

        protected virtual void UpdateAttributeLocales(Store store, StoreModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(store,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);
            }
        }
        #region Custom code from v4.0
        protected virtual void PrepareStoreOwnerIdModel(StoreModel model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            model.AvailableStoreOwnerId.Add(new SelectListItem
            {
                Text = "---",
                Value = "0"
            });


            var customers = _customerService.GetStoreOwners(model.Id, model.StoreOwnerId);
            foreach (var customer in customers)
            {
                model.AvailableStoreOwnerId.Add(new SelectListItem
                {
                    Text = customer.Email + "(" + customer.Username + ")",
                    Value = customer.Id.ToString()
                });
            }

        }

        protected virtual void PrepareCountryIdModel(StoreModel model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            model.AvailableCountryId.Add(new SelectListItem
            {
                Text = "---",
                Value = "0"
            });
            //var Cust = Nop.Web.Areas.Admin.Models.Customers.CustomerListModel ;


            var countries = _countryService.GetAllCountry();

            foreach (var country in countries)
            {
                model.AvailableCountryId.Add(new SelectListItem
                {
                    Text = country.Name,
                    Value = country.Id.ToString()
                });
            }

        }
        #endregion
        #endregion

        #region Methods

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();

            //prepare model
            var model = _storeModelFactory.PrepareStoreSearchModel(new StoreSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(StoreSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _storeModelFactory.PrepareStoreListModel(searchModel);

            return Json(model);
        }

        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();

            //prepare model
            var model = _storeModelFactory.PrepareStoreModel(new StoreModel(), null);
            //StoreOwnerId
            PrepareStoreOwnerIdModel(model);
            //Country
            PrepareCountryIdModel(model);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Create(StoreModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var store = model.ToEntity<Store>();

                //ensure we have "/" at the end
                if (!store.Url.EndsWith("/"))
                    store.Url += "/";

                _storeService.InsertStore(store);

                //activity log
                _customerActivityService.InsertActivity("AddNewStore",
                    string.Format(_localizationService.GetResource("ActivityLog.AddNewStore"), store.Id), store);

                //locales
                UpdateAttributeLocales(store, model);
                //Update StoreOwner Id from store Mah 02-07-2019

                // Add setting for store timezone
                _settingService.SetSetting("datetimesettings.storetimezoneid", model.TimeZoneId, model.Id);
                var resultSql = _dbContext.ExecuteSqlCommand("update Customer set RegisteredInStoreId=" + store.Id + " where Id=" + model.StoreOwnerId + "");

                // Store in setting table field for package plan 
                _settingService.SetSetting("setting.storesetup.IsRegisterAsMerchant", model.IsRegisterAsMerchant, store.Id);
                //Store in setting table field for Resturant plan 
                _settingService.SetSetting("setting.storesetup.RestaurantRegisterAllow", model.RestaurantRegisterAllow, store.Id);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Configuration.Stores.Added"));

                return continueEditing ? RedirectToAction("Edit", new { id = store.Id }) : RedirectToAction("List");
            }

            //prepare model
            model = _storeModelFactory.PrepareStoreModel(model, null, true);
            //storeownerid
            PrepareStoreOwnerIdModel(model);
            //Country
            PrepareCountryIdModel(model);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();

            //try to get a store with the specified id
            var store = _storeService.GetStoreById(id, false);
            if (store == null)
                return RedirectToAction("List");

            //sub admin? restrict from accessing other store.
            if (_workContext.SubAdmin.IsSubAdminRole)
                if (store.Id != _workContext.GetCurrentStoreId)
                    return RedirectToAction("List");

            //prepare model
            var model = _storeModelFactory.PrepareStoreModel(null, store);
            //storeownerid
            PrepareStoreOwnerIdModel(model);
            //PrepareModel For Update
            //Country Added by mah
            PrepareCountryIdModel(model);
            model.TimeZoneId = _settingService.GetSetting("datetimesettings.storetimezoneid", model.Id)?.Value;

            // Fetching field for package plan 
            model.IsRegisterAsMerchant = _settingService.GetSettingByKey("setting.storesetup.IsRegisterAsMerchant", false, store.Id);
            model.RestaurantRegisterAllow = _settingService.GetSettingByKey("setting.storesetup.RestaurantRegisterAllow", false, store.Id);
            

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public virtual IActionResult Edit(StoreModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();

            //try to get a store with the specified id
            var store = _storeService.GetStoreById(model.Id, false);
            if (store == null)
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                store = model.ToEntity(store);

                //ensure we have "/" at the end
                if (!store.Url.EndsWith("/"))
                    store.Url += "/";

                _storeService.UpdateStore(store);

                //activity log
                _customerActivityService.InsertActivity("EditStore",
                    string.Format(_localizationService.GetResource("ActivityLog.EditStore"), store.Id), store);

                //locales
                UpdateAttributeLocales(store, model);
                //Update StoreOwner Id from store Mah 02-07-2019
                _settingService.SetSetting("datetimesettings.storetimezoneid", model.TimeZoneId, model.Id);
                var resultSql = _dbContext.ExecuteSqlCommand("update Customer set RegisteredInStoreId=" + store.Id + " where Id=" + model.StoreOwnerId + "");


                // Store in setting table field for package plan 
                _settingService.SetSetting("setting.storesetup.IsRegisterAsMerchant", model.IsRegisterAsMerchant, store.Id);
                //Store in setting table field for Resturant plan 
                _settingService.SetSetting("setting.storesetup.RestaurantRegisterAllow", model.RestaurantRegisterAllow, store.Id);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Configuration.Stores.Updated"));

                return continueEditing ? RedirectToAction("Edit", new { id = store.Id }) : RedirectToAction("List");
            }

            //prepare model
            model = _storeModelFactory.PrepareStoreModel(model, store, true);
            //storeownerid
            PrepareStoreOwnerIdModel(model);
            //Country
            PrepareCountryIdModel(model);
            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();

            //try to get a store with the specified id
            var store = _storeService.GetStoreById(id, false);
            if (store == null)
                return RedirectToAction("List");

            try
            {
                _storeService.DeleteStore(store);

                //activity log
                _customerActivityService.InsertActivity("DeleteStore",
                    string.Format(_localizationService.GetResource("ActivityLog.DeleteStore"), store.Id), store);

                //when we delete a store we should also ensure that all "per store" settings will also be deleted
                var settingsToDelete = _settingService
                    .GetAllSettings()
                    .Where(s => s.StoreId == id)
                    .ToList();
                _settingService.DeleteSettings(settingsToDelete);

                //when we had two stores and now have only one store, we also should delete all "per store" settings
                var allStores = _storeService.GetAllStores(false);
                if (allStores.Count == 1)
                {
                    settingsToDelete = _settingService
                        .GetAllSettings()
                        .Where(s => s.StoreId == allStores[0].Id)
                        .ToList();
                    _settingService.DeleteSettings(settingsToDelete);
                }

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Configuration.Stores.Deleted"));

                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                _notificationService.ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = store.Id });
            }
        }

        #endregion

        #region CountryStateMapping

        [HttpPost]
        public virtual IActionResult CountryStateList(CountryStateSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedDataTablesJson();

            //try to get a country with the specified id
            var country = _countryService.GetCountryById(searchModel.CountryId)
                ?? throw new ArgumentException("No country found with the specified id");

            //try to get a store with the specified id
            var store = _storeService.GetStoreById(searchModel.StoreId)
                ?? throw new ArgumentException("No store found with the specified id");

            //prepare model
            var model = _storeModelFactory.PrepareCountryStateListModel(searchModel);

            return Json(model);
        }

        public virtual IActionResult CountryStateUpdate(CountryStateModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();

            //try to get a country state with the specified id
            var state = _stateProvinceService.GetStoreCountryStateMappingById(model.Id)
                ?? throw new ArgumentException("No country state mapping found with the specified id");

            //fill entity from country state
            state.IsActive = model.IsActive;
            _stateProvinceService.UpdateStoreCountryStateMapping(state);

            return new NullJsonResult();
        }

        public virtual IActionResult CountryStateAddPopup(int countryId, int storeId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();

            //prepare model
            var model = new AddStatesToCountrySearchModel();
            model.SearchCountryId = countryId;
            model.SearchStoreId = storeId;
            model.SetPopupGridPageSize();

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult CountryStateAddPopupList(AddStatesToCountrySearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _storeModelFactory.PrepareAddStatesToCountryListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult CountryStateAddPopup(AddStatesToCountryModel model, int countryId, int storeId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();

            var existingStates = _stateProvinceService.GetStoreCountryStateMappingsByCountryId(countryId, storeId, true);
            var country = _countryService.GetCountryById(countryId);
            if (country != null)
            {
                foreach (var stateId in model.SelectedProductIds)
                {
                    var state = _stateProvinceService.GetStateProvinceById(stateId);
                    if (state != null)
                    {
                        if (existingStates.Any(x => x.StateId == state.Id))
                            continue;

                        _stateProvinceService.InsertStoreCountryStateMapping(new StoreCountryStateMapping
                        {
                            StoreId = storeId,
                            CountryId = country.Id,
                            CountryName = country.Name,
                            StateName = state.Name,
                            StateId = state.Id,
                            IsActive = true,
                        });
                    }
                }
            }


            ViewBag.RefreshPage = true;
            var searchModel = new AddStatesToCountrySearchModel();
            searchModel.SearchCountryId = countryId;
            searchModel.SearchStoreId = storeId;
            searchModel.SetPopupGridPageSize();
            return View(searchModel);
        }

        #endregion
    }
}