﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Stores;

namespace Nop.Data.Mapping.Stores
{
    /// <summary>
    /// Represents a store mapping configuration
    /// </summary>
    public partial class StoreMap : NopEntityTypeConfiguration<Store>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<Store> builder)
        {
            builder.ToTable(nameof(Store));
            builder.HasKey(store => store.Id);

            builder.Property(store => store.Name).HasMaxLength(400).IsRequired();
            builder.Property(store => store.Url).HasMaxLength(400).IsRequired();
            builder.Property(store => store.Hosts).HasMaxLength(1000);
            builder.Property(store => store.CompanyName).HasMaxLength(1000);
            builder.Property(store => store.CompanyAddress).HasMaxLength(1000);
            builder.Property(store => store.CompanyPhoneNumber).HasMaxLength(1000);
            builder.Property(store => store.CompanyVat).HasMaxLength(1000);                       

            //Custom code from v4.0
            //builder.Ignore(item => item.StoreOwnerId);
            //builder.Ignore(item => item.IsDisplayDeliveryOrTakeaway);
            //builder.Ignore(item => item.IsAggrigator);
            //builder.Ignore(item => item.IsDinning);
            //builder.Ignore(item => item.IsTakeaway);
            //builder.Ignore(item => item.IsDelivery);
            //builder.Ignore(item => item.CountryId);

            base.Configure(builder);
        }

        #endregion
    }
}