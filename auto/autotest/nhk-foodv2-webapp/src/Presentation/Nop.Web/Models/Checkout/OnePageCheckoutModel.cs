﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Checkout
{
    public partial class OnePageCheckoutModel : BaseNopModel
    {
        public bool ShippingRequired { get; set; }
        public bool DisableBillingAddressCheckoutStep { get; set; }

        public string WalletAmount { get; set; }

        public CheckoutBillingAddressModel BillingAddress { get; set; }
        public CheckoutLoginSignUpModel LoginSignUpModel { get; set; }//Custom code from v4.0

        public bool DisplayDeliveryScheduleButton { get; set; }
    }
}