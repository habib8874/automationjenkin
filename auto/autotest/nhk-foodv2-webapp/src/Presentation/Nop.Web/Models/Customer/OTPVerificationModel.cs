﻿using System;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Customer
{
    public partial class OTPVerificationModel : BaseNopModel
    {
        public int CustomerId{ get; set; }
        public string OTP { get; set; }
        public string Result { get; set; }
        public int Timer { get; set; }
        public string Date { get; set; }
        public DateTime CurrentDate { get; set; }

    }
}