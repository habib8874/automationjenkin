﻿using System;

namespace Nop.Core.Domain.NB.Package
{
    /// <summary>
    /// Represents a NBCustomerPackageMapping
    /// </summary>
    public partial class NBCustomerPackageMapping : BaseEntity
    {
        /// <summary>
        /// Gets or sets the CustomerId 
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the PackageId 
        /// </summary>
        public int PackageId { get; set; }

        /// <summary>
        /// Gets or sets the PackagePlanId 
        /// </summary>
        public int PackagePlanId { get; set; }

        public string CardholderName { get; set; }

        /// <summary>
        /// Gets or sets the CreditCardType 
        /// </summary>
        public string CreditCardType { get; set; }

        /// <summary>
        /// Gets or sets the CardNumber 
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Gets or sets the ExpireMonth 
        /// </summary>
        public string ExpireMonth { get; set; }

        /// <summary>
        /// Gets or sets the ExpireYear 
        /// </summary>
        public string ExpireYear { get; set; }

        /// <summary>
        /// Gets or sets the CardCode 
        /// </summary>
        public string CardCode { get; set; }

        /// <summary>
        /// Gets or sets the PackageAmount 
        /// </summary>
        public decimal PackageAmount { get; set; }

        /// <summary>
        /// Gets or sets the StripeId 
        /// </summary>
        public string StripeId { get; set; }

        /// <summary>
        /// Gets or sets the StripeCustomerId 
        /// </summary>
        public string StripeCustomerId { get; set; }

        /// <summary>
        /// Gets or sets the CreatedOnUtc 
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

    }
}
