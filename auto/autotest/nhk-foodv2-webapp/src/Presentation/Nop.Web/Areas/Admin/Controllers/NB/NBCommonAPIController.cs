﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Nop.Core.Domain.Orders;
using Nop.Services.Localization;
using Nop.Services.NB.API;
using Nop.Services.NB.SignalR;

namespace Nop.Web.Areas.Admin.Controllers.NB
{
    [Route("api/[controller]")]
    [ApiController]
    public class NBCommonAPIController : ControllerBase
    {
        #region Fields

        private readonly IHubContext<SignalRHub> _signalRHubContext;
        private readonly ILocalizationService _localizationService;
        private readonly IVersionInfoServices _versionInfoServices;

        #endregion

        #region Ctor

        public NBCommonAPIController(IHubContext<SignalRHub> signalRHubContext,
            ILocalizationService localizationService,
            IVersionInfoServices versionInfoServices)
        {
            _signalRHubContext = signalRHubContext;
            _localizationService = localizationService;
            _versionInfoServices = versionInfoServices;
        }

        #endregion

        #region Utitlies

        public class CommonAPIResponses
        {
            public int StatusCode { get; set; }
            public string ErrorMessageTitle { get; set; }
            public string ErrorMessage { get; set; }
            public bool Status { get; set; }
            public object ResponseObj { get; set; }

        }

        public class OrderStatusModel
        {
            public string ApiKey { get; set; }
            public int OrderId { get; set; }
            public int OrderStatusId { get; set; }
        }

        public class OrderPlacedModel
        {
            public string ApiKey { get; set; }
            public int OrderId { get; set; }
        }

        #endregion

        #region Dispatch Management

        [Route("~/api/v1/InvokeOrderStatusChangeSignalR")]
        [HttpPost]
        public CommonAPIResponses InvokeOrderStatusChangeSignalR([FromBody] OrderStatusModel orderStatusModel)
        {
            CommonAPIResponses customerAPIResponses = new CommonAPIResponses();
            if (orderStatusModel == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = _localizationService.GetResource("NB.Admin.API.Dispatch.Model.Missing");
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (orderStatusModel.OrderId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = _localizationService.GetResource("NB.Admin.API.Dispatch.OrdersId.Missing");
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (orderStatusModel.OrderStatusId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = _localizationService.GetResource("NB.Admin.API.Dispatch.OrderStatus.Missing");
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            try
            {
                //check Authentication
                if (!_versionInfoServices.IsValidAPIkey(orderStatusModel.ApiKey))
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = _localizationService.GetResource("NB.Admin.API.Web.Common.Error.InvalidAuthentication");
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }

                var responseMessgae = string.Empty;

                #region Admin side 

                //For Dispatch management
                if (orderStatusModel.OrderStatusId == (int)OrderStatus.Confirmed)
                {
                    _signalRHubContext.Clients.All.SendAsync("orderConfirmedRequest", orderStatusModel.OrderId);
                    responseMessgae += _localizationService.GetResource("NB.Admin.API.Dispatch.OrderConfirm.SignalR.Success");
                }
                else if (orderStatusModel.OrderStatusId == (int)OrderStatus.OrderDelivered)
                {
                    _signalRHubContext.Clients.All.SendAsync("orderDeliveredRequest", orderStatusModel.OrderId);
                    responseMessgae += _localizationService.GetResource("NB.Admin.API.Dispatch.OrderDelivered.SignalR.Success");
                }
                //Date: 24-5-21, OrderStatus 9 = Rider Reject Order.(Confirmed with App developer). So when rider rejects the order, that order again need to show in pending order list 
                else if (orderStatusModel.OrderStatusId == 9)
                {
                    //on rider assign invoke both 
                    _signalRHubContext.Clients.All.SendAsync("orderConfirmedRequest", orderStatusModel.OrderId);
                    _signalRHubContext.Clients.All.SendAsync("orderDeliveredRequest", orderStatusModel.OrderId);
                    responseMessgae += "Both SignalR invoked";
                }

                #endregion

                #region Public side

                //For Order Live Tracking 
                _signalRHubContext.Clients.All.SendAsync("updateTrackingProgress", orderStatusModel.OrderId);
                responseMessgae += " And " + _localizationService.GetResource("NB.Admin.API.OrderTracking.updateTrackingProgress.SignalR.Success");

                #endregion

                customerAPIResponses.Status = true;
                customerAPIResponses.ResponseObj = responseMessgae;
                return customerAPIResponses;
            }
            catch (Exception ex)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = 400;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
        }

        #endregion

        #region Order Placed Notification

        [Route("~/api/v1/InvokeOrderPlacedNotifySignalR")]
        [HttpPost]
        public CommonAPIResponses InvokeOrderPlacedNotifySignalR([FromBody] OrderPlacedModel model)
        {
            CommonAPIResponses customerAPIResponses = new CommonAPIResponses();
            if (model == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = _localizationService.GetResource("NB.Admin.API.Dispatch.Model.Missing");
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (model.OrderId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = _localizationService.GetResource("NB.Admin.API.Dispatch.OrdersId.Missing");
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            try
            {
                //check Authentication
                if (!_versionInfoServices.IsValidAPIkey(model.ApiKey))
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = _localizationService.GetResource("NB.Admin.API.Web.Common.Error.InvalidAuthentication");
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }

                //invoke order placed notify signalR
                _signalRHubContext.Clients.All.SendAsync("orderPlacedNotifyRequest", model.OrderId);
                customerAPIResponses.ResponseObj = _localizationService.GetResource("NB.Admin.API.OrderPlaced.Notify.SignalR.Success");
                customerAPIResponses.Status = true;
                return customerAPIResponses;
            }
            catch (Exception ex)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = 400;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
        }

        #endregion
    }
}
