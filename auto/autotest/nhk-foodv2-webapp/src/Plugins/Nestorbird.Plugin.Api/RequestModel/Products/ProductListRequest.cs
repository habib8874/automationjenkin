﻿using System.Collections.Generic;

namespace Nestorbird.Plugin.Api.RequestModel.Products
{
    public class ProductListRequest : PagingRequest
    {
        public ProductListRequest()
        {
            CategoryIds = new List<int>();
        }

        public int VendorId { get; set; }

        public IList<int> CategoryIds { get; set; }
    }
}
