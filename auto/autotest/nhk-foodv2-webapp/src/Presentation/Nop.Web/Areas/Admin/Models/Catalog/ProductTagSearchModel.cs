﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a product tag search model
    /// </summary>
    public partial class ProductTagSearchModel : BaseSearchModel
    {
        public ProductTagSearchModel()
        {
            AvailableStores = new List<SelectListItem>();
           // AvailableVendors = new List<SelectListItem>();
        }
        public IList<SelectListItem> AvailableStores { get; set; }
        public int storeId { get; set; }

        //public IList<SelectListItem> AvailableVendors { get; set; }
        //public int SearchVendor { get; set; }
    }
}