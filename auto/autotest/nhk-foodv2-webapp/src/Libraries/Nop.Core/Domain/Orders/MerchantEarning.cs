﻿using System;

namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents an Merchant Earning
    /// </summary>
    public partial class MerchantEarning : BaseEntity
    {
        
        public int MerchantId { get; set; }

        public int OrderId { get; set; }
        public decimal PayableAmount { get; set; }
        public decimal PaidAmount { get; set; }
    }
}