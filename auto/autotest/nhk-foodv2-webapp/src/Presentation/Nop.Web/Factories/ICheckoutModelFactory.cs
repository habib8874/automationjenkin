﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Orders;
using Nop.Services.Payments;
using Nop.Web.Areas.Admin.Models.Common;
using Nop.Web.Models.Checkout;
using Nop.Web.Models.OnePageCheckoutModelv2;

namespace Nop.Web.Factories
{
    public partial interface ICheckoutModelFactory
    {
        /// <summary>
        /// Prepare billing address model
        /// </summary>
        /// <param name="cart">Cart</param>
        /// <param name="selectedCountryId">Selected country identifier</param>
        /// <param name="prePopulateNewAddressWithCustomerFields">Pre populate new address with customer fields</param>
        /// <param name="overrideAttributesXml">Override attributes xml</param>
        /// <returns>Billing address model</returns>
        CheckoutBillingAddressModel PrepareBillingAddressModel(IList<ShoppingCartItem> cart,
            int? selectedCountryId = null,
            bool prePopulateNewAddressWithCustomerFields = false,
            string overrideAttributesXml = "");

        /// <summary>
        /// Prepare shipping address model
        /// </summary>
        /// <param name="selectedCountryId">Selected country identifier</param>
        /// <param name="prePopulateNewAddressWithCustomerFields">Pre populate new address with customer fields</param>
        /// <param name="overrideAttributesXml">Override attributes xml</param>
        /// <returns>Shipping address model</returns>
        CheckoutShippingAddressModel PrepareShippingAddressModel(int? selectedCountryId = null,
            bool prePopulateNewAddressWithCustomerFields = false, string overrideAttributesXml = "");

        /// <summary>
        /// Prepare shipping method model
        /// </summary>
        /// <param name="cart">Cart</param>
        /// <param name="shippingAddress">Shipping address</param>
        /// <returns>Shipping method model</returns>
        CheckoutShippingMethodModel PrepareShippingMethodModel(IList<ShoppingCartItem> cart, Address shippingAddress);

        /// <summary>
        /// Prepare payment method model
        /// </summary>
        /// <param name="cart">Cart</param>
        /// <param name="filterByCountryId">Filter by country identifier</param>
        /// <returns>Payment method model</returns>
        CheckoutPaymentMethodModel PreparePaymentMethodModel(IList<ShoppingCartItem> cart, int filterByCountryId);

        /// <summary>
        /// Prepare payment info model
        /// </summary>
        /// <param name="paymentMethod">Payment method</param>
        /// <returns>Payment info model</returns>
        CheckoutPaymentInfoModel PreparePaymentInfoModel(IPaymentMethod paymentMethod);

        /// <summary>
        /// Prepare confirm order model
        /// </summary>
        /// <param name="cart">Cart</param>
        /// <returns>Confirm order model</returns>
        CheckoutConfirmModel PrepareConfirmOrderModel(IList<ShoppingCartItem> cart);

        /// <summary>
        /// Prepare checkout completed model
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Checkout completed model</returns>
        CheckoutCompletedModel PrepareCheckoutCompletedModel(Order order);

        /// <summary>
        /// Prepare checkout progress model
        /// </summary>
        /// <param name="step">Step</param>
        /// <returns>Checkout progress model</returns>
        CheckoutProgressModel PrepareCheckoutProgressModel(CheckoutProgressStep step);

        /// <summary>
        /// Prepare one page checkout model
        /// </summary>
        /// <param name="cart">Cart</param>
        /// <returns>One page checkout model</returns>
        OnePageCheckoutModel PrepareOnePageCheckoutModel(IList<ShoppingCartItem> cart);

        #region Custom code from v4.0
        OnePageCheckoutModelv2 PrepareBillingAddressModelV2(OnePageCheckoutModelv2 model, IList<ShoppingCartItem> cart,
           int? selectedCountryId = null,
           bool prePopulateNewAddressWithCustomerFields = false,
           string overrideAttributesXml = "");

        OnePageCheckoutModelv2 PrepareShippingAddressModelV2(OnePageCheckoutModelv2 model, int? selectedCountryId = null,
            bool prePopulateNewAddressWithCustomerFields = false, string overrideAttributesXml = "");

        OnePageCheckoutModelv2 PrepareShippingMethodModelV2(OnePageCheckoutModelv2 model, IList<ShoppingCartItem> cart, Address shippingAddress);

        OnePageCheckoutModelv2 PreparePaymentMethodModelV2(OnePageCheckoutModelv2 model, IList<ShoppingCartItem> cart, int filterByCountryId);

        OnePageCheckoutModelv2 PreparePaymentInfoModelV2(OnePageCheckoutModelv2 model, IPaymentMethod paymentMethod);
        OnePageCheckoutModelv2 PrepareConfirmOrderModelV2(OnePageCheckoutModelv2 model, IList<ShoppingCartItem> cart);
        Nop.Web.Models.Common.AddressModel PrepareSelectedBillingAddressModel(Address address);

        #endregion

        /// <summary>
        /// PrepareWalletPaymentMethodModel
        /// </summary>
        /// <returns></returns>
        CheckoutPaymentMethodModel PrepareWalletPaymentMethodModel();

        /// <summary>
        /// PrepareWalletConfirmOrderModel
        /// </summary>
        /// <returns></returns>
        CheckoutConfirmModel PrepareWalletConfirmOrderModel();

        /// <summary>
        /// PrepareOnePageWalletCheckoutModel
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        OnePageCheckoutModel PrepareOnePageWalletCheckoutModel(Decimal amount);
    }
}
