﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Web.Framework.Components;
using Nop.Web.Models.Order;

namespace Nop.Web.Components
{
    public class TrackLatestOrderBlock : NopViewComponent
    {
        private readonly IWorkContext _workContext;
        private readonly IOrderService _orderService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;

        public TrackLatestOrderBlock(IWorkContext workContext,
            IOrderService orderService,
            IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService)
        {
            _workContext = workContext;
            _orderService = orderService;
            _dateTimeHelper = dateTimeHelper;
            _localizationService = localizationService;
        }

        public IViewComponentResult Invoke()
        {
            //get current customers last order
            var order = _orderService.SearchOrders(storeId: _workContext.GetCurrentStoreId,
            customerId: _workContext.CurrentCustomer.Id, pageSize: 1).FirstOrDefault();

            //verify order
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Content("");

            //check order is trackable
            if (!_orderService.ShowOrderTracking(order))
                return Content("");

            //prepare model
            var model = new OrderDetailsModel()
            {
                Id = order.Id,
                CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc),
                OrderStatus = _localizationService.GetLocalizedEnum(order.OrderStatus),
                CustomOrderNumber = order.CustomOrderNumber,
            };

            return View(model);
        }
    }
}
