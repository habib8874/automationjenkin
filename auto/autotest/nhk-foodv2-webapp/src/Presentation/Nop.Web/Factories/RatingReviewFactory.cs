﻿using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.RatingReview;
using Nop.Core.Domain.Topics;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.RatingReview;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Topics;
using Nop.Services.Vendors;
using Nop.Web.Infrastructure.Cache;
using Nop.Web.Models.RatingReview;
using Nop.Services.Common;
using Nop.Services.Catalog;

namespace Nop.Web.Factories
{
    /// <summary>
    /// Represents the rating review model factory
    /// </summary>
    public partial class RatingReviewFactory : IRatingReviewFactory
    {
        #region Fields

        private readonly IRatingReviewService _ratingReviewService;
        private readonly ICustomerService _customerService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IAclService _aclService;
        private readonly ISettingService _settingService;
        private readonly IVendorService _vendorService;
        private readonly IPictureService _pictureService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IProductService _productService;
        private readonly IReviewOptionService _reviewOptionService;
        private readonly ILocalizationService _localizationService;

        #endregion

        #region Ctor

        public RatingReviewFactory(IRatingReviewService ratingReviewService,
            ICustomerService customerService,
            IWorkContext workContext,
            IStoreContext storeContext,
            IStaticCacheManager cacheManager,
            IStoreMappingService storeMappingService,
            IAclService aclService,
            ISettingService settingService,
            IVendorService vendorService,
            IPictureService pictureService,
            IGenericAttributeService genericAttributeService,
            IProductService productService,
            IReviewOptionService reviewOptionService,
            ILocalizationService localizationService)
        {
            _ratingReviewService = ratingReviewService;
            _customerService = customerService;
            _workContext = workContext;
            _storeContext = storeContext;
            _cacheManager = cacheManager;
            _storeMappingService = storeMappingService;
            _aclService = aclService;
            _settingService = settingService;
            _vendorService = vendorService;
            _pictureService = pictureService;
            _genericAttributeService = genericAttributeService;
            _productService = productService;
            _reviewOptionService = reviewOptionService;
            _localizationService = localizationService;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Prepare the rating review model for agent
        /// </summary>
        /// <returns>RatingReview Model</returns>
        protected virtual RatingReviewModel PrepareRatingReviewAgentModel(Order order)
        {
            var model = new RatingReviewModel();
            //var reviewOptions = _settingService.GetSettingByKey<string>("ratingreviewsettings.agent.dislikeoptions", string.Empty, _workContext.GetCurrentStoreId, true);

            #region Review options

            var reviewOptions = _settingService.GetSettingByKey<string>("ratingreviewsettings.agent.activeoptionsettingids", string.Empty, _workContext.GetCurrentStoreId, true);
            if (!string.IsNullOrEmpty(reviewOptions))
            {
                var reviewOptionids = reviewOptions
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                foreach (var id in reviewOptionids)
                {
                    var reviewOption = _reviewOptionService.GetReviewOptionById(id);
                    if (reviewOption != null)
                    {
                        model.NBReviewOptions.Add(new NBReviewOptionModel
                        {
                            Id = reviewOption.Id,
                            Name = _localizationService.GetLocalized(reviewOption, entity => entity.Name, _workContext.WorkingLanguage.Id, false, false) ?? reviewOption.Name,
                            EntityType = reviewOption.EntityType,
                            Active = reviewOption.Active
                        });
                    }
                }
            }

            #endregion

            var showAdditionalNotes = _settingService.GetSettingByKey<bool>("ratingreviewsettings.agent.showadditionalnotes", true, _workContext.GetCurrentStoreId, true);
            model.ReviewOptions = reviewOptions;
            model.ShowAdditionalNotes = showAdditionalNotes;
            model.OrderId = order.Id;

            //get agent details
            var agent = _customerService.GetAssignedAgentByOrderId(order.Id);
            if (agent != null)
            {
                model.EntityId = agent.AgentId;
                var agentDetails = _customerService.GetCustomerById(agent.AgentId);
                if (agentDetails != null)
                {
                    model.ReviewForName = agentDetails.GetFullName();
                    model.ProfilePicture = _pictureService.GetPictureUrl(_genericAttributeService.GetAttribute<int>(agentDetails, NopCustomerDefaults.AvatarPictureIdAttribute), 100, false);
                }
            }
            return model;
        }

        /// <summary>
        /// Prepare the rating review model for merchant
        /// </summary>
        /// <returns>RatingReview Model</returns>
        protected virtual RatingReviewModel PrepareRatingReviewMerchantModel(Order order)
        {
            var model = new RatingReviewModel();
            //var reviewOptions = _settingService.GetSettingByKey<string>("ratingreviewsettings.merchant.dislikeoptions", string.Empty, _workContext.GetCurrentStoreId, true);

            #region Review options

            var reviewOptions = _settingService.GetSettingByKey<string>("ratingreviewsettings.merchant.activeoptionsettingids", string.Empty, _workContext.GetCurrentStoreId, true);
            if (!string.IsNullOrEmpty(reviewOptions))
            {
                var reviewOptionids = reviewOptions
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                foreach (var id in reviewOptionids)
                {
                    var reviewOption = _reviewOptionService.GetReviewOptionById(id);
                    if (reviewOption != null)
                    {
                        model.NBReviewOptions.Add(new NBReviewOptionModel
                        {
                            Id = reviewOption.Id,
                            Name = _localizationService.GetLocalized(reviewOption, entity => entity.Name, _workContext.WorkingLanguage.Id, false, false) ?? reviewOption.Name,
                            EntityType = reviewOption.EntityType,
                            Active = reviewOption.Active
                        });
                    }
                }
            }

            #endregion

            var showAdditionalNotes = _settingService.GetSettingByKey<bool>("ratingreviewsettings.merchant.showadditionalnotes", true, _workContext.GetCurrentStoreId, true);
            model.ReviewOptions = reviewOptions;
            model.ShowAdditionalNotes = showAdditionalNotes;
            model.OrderId = order.Id;

            //get merchant details
            try
            {
                var merchant = _vendorService.GetVendorById(order.OrderItems.FirstOrDefault().Product.VendorId);
                if (merchant != null)
                {
                    model.EntityId = merchant.Id;
                    model.ReviewForName = merchant.Name;
                    if (merchant.PictureId > 0)
                    {
                        model.ProfilePicture = _pictureService.GetPictureUrl(merchant.PictureId, 100, true);
                    }
                }
            }
            catch (Exception ex) { }
            return model;
        }

        /// <summary>
        /// Prepare the rating review model for item
        /// </summary>
        /// <returns>RatingReview Model</returns>
        protected virtual RatingReviewModel PrepareRatingReviewItemModel(Order order, int directItemId = 0)
        {
            var model = new RatingReviewModel();
            var showAdditionalNotes = _settingService.GetSettingByKey<bool>("ratingreviewsettings.item.showadditionalnotes", true, _workContext.GetCurrentStoreId, true);
            model.ShowAdditionalNotes = showAdditionalNotes;
            model.OrderId = order.Id;
            //get items details
            try
            {
                foreach (var item in order.OrderItems)
                {
                    //get existing item review for current order
                    var ratings = _ratingReviewService.GetAllRatingReviews(_workContext.GetCurrentStoreId)
                                                      ?.Where(x => x.OrderId == order.Id && x.ReviewType == (int)ReviewType.Item);

                    //check if rating table already has this item review
                    if (ratings != null && ratings.Any(x => x.EntityId == item.ProductId))
                        continue;

                    //direct item review?
                    if (directItemId > 0)
                    {
                        //continue loop untill we found product for which direct review requested. 
                        if (item.Product.Id != directItemId)
                            continue;
                    }

                    var _item = new RatingReviewModel.ItemsModel();
                    _item.ProductId = item.Product.Id;
                    _item.Name = item.Product.Name;
                    var picture = _pictureService.GetPicturesByProductId(item.Product.Id, 100).FirstOrDefault();
                    if (picture != null)
                    {
                        _item.ProductPicture = _pictureService.GetPictureUrl(picture.Id, 100, true);
                    }
                    if (!model.Items.Any(x => x.ProductId == item.ProductId))
                    {
                        model.Items.Add(_item);
                        model.EntityId = item.ProductId;
                    }

                    //One item review at a time
                    if (model.Items.Count == 1)
                        break;
                }
            }
            catch (Exception ex) { }
            return model;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get the rating review model by review type
        /// </summary>
        /// <param name="reviewType">reviewType name</param>
        /// <returns>RatingReview model</returns>
        public virtual RatingReviewModel PrepareRatingReviewModel(int reviewType, Order order, int directItemId = 0)
        {
            int storeId = _workContext.GetCurrentStoreId;
            var model = new RatingReviewModel();

            var isActiveAgent = _settingService.GetSettingByKey<bool>("ratingreviewsettings.agent.isactive", true, storeId, true);
            var isActiveMerchant = _settingService.GetSettingByKey<bool>("ratingreviewsettings.merchant.isactive", true, storeId, true);
            var isActiveItem = _settingService.GetSettingByKey<bool>("ratingreviewsettings.item.isactive", true, storeId, true);
            if (reviewType == (int)ReviewType.Agent && !isActiveAgent)
                reviewType = (int)ReviewType.Merchant;
            else if (reviewType == (int)ReviewType.Merchant && !isActiveMerchant)
                reviewType = (int)ReviewType.Item;
            else if (reviewType == (int)ReviewType.Item && !isActiveItem)
                return null;

            if (reviewType == (int)ReviewType.Agent)
            {
                model = PrepareRatingReviewAgentModel(order);
            }
            else if (reviewType == (int)ReviewType.Merchant)
            {
                model = PrepareRatingReviewMerchantModel(order);
            }
            else if (reviewType == (int)ReviewType.Item)
            {
                model = PrepareRatingReviewItemModel(order, directItemId);
            }
            model.ReviewType = reviewType;

            return model;
        }

        /// <summary>
        /// prepare product rating details model
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public virtual ProductRatingsDetailsModel PrepareProductRatingsDetailsModel(int productId)
        {
            var model = new ProductRatingsDetailsModel();
            var product = _productService.GetProductById(productId);
            if (product == null)
                return model;

            model.Id = product.Id;
            model.ProductName = product.Name;
            model.ShortDescription = product.ShortDescription;
            model.DisableBuyButton = product.DisableBuyButton;
            var picture = _pictureService.GetPicturesByProductId(product.Id, 200).FirstOrDefault();
            if (picture != null)
            {
                model.ProductPictureUrl = _pictureService.GetPictureUrl(picture.Id, 200, true);
            }
            //get product ratings
            var ratings = _ratingReviewService.GetItemRatings(_workContext.GetCurrentStoreId, product.Id);
            foreach (var item in ratings)
            {
                var ratingModel = new ProductRatingsModel();
                var customer = _customerService.GetCustomerById(item.CustomerId);
                if (customer != null)
                {
                    ratingModel.CustomerName = _customerService.GetCustomerFullName(customer);
                    var avatarPictureId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AvatarPictureIdAttribute);
                    ratingModel.CustomerPictureUrl = _pictureService.GetPictureUrl(avatarPictureId);
                }
                ratingModel.RatingPercent = (item.Rating);
                ratingModel.Review = item.ReviewText;
                model.RatingsList.Add(ratingModel);
            }
            model.TotalReviews = ratings.Count;
            model.TotalRatingPercent = ratings.Count > 0 ? (((ratings.Sum(x => x.Rating)) / ratings.Count)) : 0;
            return model;
        }
        #endregion
    }
}
