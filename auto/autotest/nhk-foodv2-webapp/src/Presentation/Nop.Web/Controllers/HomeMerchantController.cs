﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.BookingTable;
using Nop.Core.Domain.Configuration;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Data;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Areas.Admin.Models.BookingTable;
using Nop.Web.Framework.Mvc;
//using static Nop.Web.Areas.Admin.Controllers.BookingTableTransactionController;

namespace Nop.Web.Controllers
{
    public class HomeMerchantController : Controller
    {
        #region Fields
        private readonly IRepository<BookingTableMasterDetail> _bookingTableMasterDetailrepository;
        private readonly IRepository<Picture> _picturerepository;
        private readonly IOrderService _orderService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly IStoreService _storeService;
        private readonly IRepository<BookingTableMaster> _bookingrepository;
        private readonly IRepository<BookingTableTransaction> _bookingTransactionrepository;
        private readonly ILocalizationService _localizationService;
        private readonly IDbContext _dbContext;
        private readonly IStoreContext _storeContext;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IVendorService _vendorService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IRepository<Setting> _settings;

        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly ITokenizer _tokenizer;
        private readonly IRepository<MessageTemplate> _messageTemplate;
        private readonly IEmailAccountService _emailAccountService;
        private readonly IEmailSender _emailSender;
        private readonly IPictureService _pictureService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly ISettingService _settingService;

        #endregion

        #region Ctor
        public HomeMerchantController(IOrderService orderService, IWorkContext workContext, ICustomerService customerService,
            IStoreService storeService,
            ILocalizationService localizationService,
            IRepository<BookingTableMaster> repository,
            IRepository<BookingTableTransaction> bookingTransactionrepository,
            IDbContext dbContext,
            IStoreContext storeContext, IGenericAttributeService genericAttributeService,
            IRepository<BookingTableMasterDetail> bookingTableMasterDetailrepository,
            IRepository<Picture> picturerepository, IVendorService vendorService,
            IPriceFormatter priceFormatter,
            IRepository<Setting> settings,
            IMessageTokenProvider messageTokenProvider,
            IRepository<MessageTemplate> messageTemplate,
            ITokenizer tokenizer,
            IEmailAccountService emailAccountService,
            IEmailSender emailSender,
            IPictureService pictureService,
            IShoppingCartService shoppingCartService,
            ISettingService settingService
            )
        {
            _picturerepository = picturerepository;
            _bookingTableMasterDetailrepository = bookingTableMasterDetailrepository;
            _orderService = orderService;
            _workContext = workContext;
            _customerService = customerService;
            _dbContext = dbContext;
            _storeService = storeService;
            _localizationService = localizationService;
            _bookingrepository = repository;
            _bookingTransactionrepository = bookingTransactionrepository;
            _storeContext = storeContext;
            _genericAttributeService = genericAttributeService;
            _vendorService = vendorService;
            _priceFormatter = priceFormatter;
            _settings = settings;
            _messageTokenProvider = messageTokenProvider;
            _tokenizer = tokenizer;
            _messageTemplate = messageTemplate;
            _emailAccountService = emailAccountService;
            _emailSender = emailSender;
            _pictureService = pictureService;
            _shoppingCartService = shoppingCartService;
            _settingService = settingService;
        }

        #endregion

        #region Methods

        public IActionResult Index()
        {
            var isd = _workContext.CurrentStoreISDCode;
            ////custom by kp (20-06-2019)
            if (_workContext.CurrentMerchant == null)
            {
                return RedirectToRoute("HomePage");

            }//End
            if (TempData["Success"] != null)
            {
                ViewBag.Success = TempData["Success"].ToString();
                TempData["Success"] = null;
            }

            if (_workContext.CurrentMerchant != null)
            {
                int vendorId = _workContext.CurrentMerchant.Id;
                var vendor = _vendorService.GetVendorById(vendorId);
                if (vendor.PictureId == 0)
                {
                    ViewBag.VendorPicture = "/images/thumbs/default-image_100.png";
                }
                else
                {
                    var vendorPicture = _picturerepository.GetById(vendor.PictureId);
                    ViewBag.VendorPicture = _pictureService.GetPictureUrl(vendorPicture, 400);
                }
                var bokingMaster = _bookingrepository.Table.Where(x => x.VendorId == vendorId).FirstOrDefault();
                if (bokingMaster != null)
                {
                    var bookingMasterDetails = _bookingTableMasterDetailrepository.Table.Where(x => x.Id == bokingMaster.BookingDetailsId).FirstOrDefault();
                    if (bookingMasterDetails != null)
                    {
                        var picture = _picturerepository.GetById(bookingMasterDetails.ImageId)?.PictureBinary;
                        if (picture != null)
                        {
                            var base64 = Convert.ToBase64String(picture.BinaryData);
                            ViewBag.Picture = String.Format("data:image/gif;base64,{0}", base64);
                        }
                    }
                }
            }

            return View();
        }
        protected virtual string GetFileExtensionFromMimeType(string mimeType)
        {
            if (mimeType == null)
                return null;

            //TODO use FileExtensionContentTypeProvider to get file extension

            var parts = mimeType.Split('/');
            var lastPart = parts[parts.Length - 1];
            switch (lastPart)
            {
                case "pjpeg":
                    lastPart = "jpg";
                    break;
                case "x-png":
                    lastPart = "png";
                    break;
                case "x-icon":
                    lastPart = "ico";
                    break;
            }
            return lastPart;
        }
        public bool SetMerchant(int merchantid)
        {
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    "CustomerCurrentMerchantId", merchantid, _workContext.GetCurrentStoreId);
            int currStoreId = _workContext.GetCurrentStoreId;
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart && sci.StoreId == _workContext.GetCurrentStoreId).ToList();
            if (cart.Count > 0 && cart.FirstOrDefault().Product.VendorId != merchantid)
            {
                var allIdsToRemove = new List<int>();
                allIdsToRemove = cart.Select(x => x.Id).ToList();

                foreach (var sci in cart)
                {
                    var remove = allIdsToRemove.Contains(sci.Id);
                    if (remove)
                        _shoppingCartService.DeleteShoppingCartItem(sci, ensureOnlyActiveCheckoutAttributes: true);
                }
            }
            return true;
        }

        public IActionResult OrderDetailsForAgent(int OrderId, string PubKey = null)
        {
            OrderInfoNotification orderInfoNotification = new OrderInfoNotification();
            try
            {
                var orderVendor = _orderService.GetOrderVendorDetails(OrderId);
                var orderCustomer = _orderService.GetOrderCustomerDetails(OrderId);
                orderInfoNotification.Status = true;
                orderInfoNotification.OrderId = OrderId.ToString();
                //pushOrderNotification.AgentId = ag.AgentId.ToString();
                orderInfoNotification.CustomerName = ""; //order.Customer.GetFullName();
                orderInfoNotification.MerchantName = "";
                orderInfoNotification.PickupLocation = "";
                orderInfoNotification.PickupLat = "0";
                orderInfoNotification.PickupLong = "0";
                orderInfoNotification.PickupMobileNo = "0";
                if (orderVendor != null)
                {
                    orderInfoNotification.MerchantName = orderVendor.MerchantName;
                    orderInfoNotification.PickupLocation = orderVendor.PickupLocation;
                    orderInfoNotification.PickupLat = orderVendor.PickupLat;
                    orderInfoNotification.PickupLong = orderVendor.PickupLong;
                    orderInfoNotification.PickupMobileNo = orderVendor.PickupMobileNo;
                }
                orderInfoNotification.DeliveryLocation = "0";
                orderInfoNotification.DeliveryLat = "0";
                orderInfoNotification.DeliveryLong = "0";
                orderInfoNotification.DeliveryMobileNo = "0";
                if (orderCustomer != null)
                {
                    orderInfoNotification.CustomerName = orderCustomer.CustomerName;
                    orderInfoNotification.DeliveryLocation = orderCustomer.CustomerLocation;
                    string[] geoLoc = getGeoLoc(orderCustomer.CustomerLocation).Split(',');
                    orderInfoNotification.DeliveryLat = geoLoc[0].ToString();
                    orderInfoNotification.DeliveryLong = geoLoc[1].ToString();
                    orderInfoNotification.DeliveryMobileNo = orderCustomer.CustomerMobileNo;
                }
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
                orderInfoNotification.Status = false;
                Json(orderInfoNotification);
            }

            return Json(orderInfoNotification);
        }

        public JsonResult BookingRecievedList(string PubKey = null)
        {

            List<BookingRecievedListModel> bookingRecievedList = new List<BookingRecievedListModel>();
            if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Administrators").Any())
            {
                var bookingTransactions = _bookingTransactionrepository.Table.ToList();
                foreach (var item in bookingTransactions)
                {
                    BookingRecievedListModel bookingRecievedObj = new BookingRecievedListModel();
                    bookingRecievedObj.BookingId = item.Id;
                    bookingRecievedObj.BookingDate = item.BookingDate.ToShortDateString();
                    bookingRecievedObj.CustomerName = item.CustomerName;
                    bookingRecievedObj.TableNumber = item.TableNumber;
                    bookingRecievedObj.CreatedDate = item.CreatedDt.ToShortDateString();
                    DateTime fromtime = DateTime.Today.Add(item.BookingTimeFrom);
                    bookingRecievedObj.BookingTimeFrom = fromtime.ToString("h:mm tt");
                    DateTime totime = DateTime.Today.Add(item.BookingTimeTo);
                    bookingRecievedObj.BookingTimeTo = totime.ToString("h:mm tt");
                    bookingRecievedList.Add(bookingRecievedObj);
                }
            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "StoreOwner").Any())
            {
                var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).FirstOrDefault();
                var bookingTransactions = _bookingTransactionrepository.Table.Where(x => x.StoreId == stores.Id).ToList();
                foreach (var item in bookingTransactions)
                {
                    BookingRecievedListModel bookingRecievedObj = new BookingRecievedListModel();
                    bookingRecievedObj.BookingId = item.Id;
                    bookingRecievedObj.BookingDate = item.BookingDate.ToShortDateString();
                    bookingRecievedObj.CustomerName = item.CustomerName;
                    bookingRecievedObj.TableNumber = item.TableNumber;
                    bookingRecievedObj.CreatedDate = item.CreatedDt.ToShortDateString();
                    DateTime fromtime = DateTime.Today.Add(item.BookingTimeFrom);
                    bookingRecievedObj.BookingTimeFrom = fromtime.ToString("h:mm tt");
                    DateTime totime = DateTime.Today.Add(item.BookingTimeTo);
                    bookingRecievedObj.BookingTimeTo = totime.ToString("h:mm tt");
                    bookingRecievedList.Add(bookingRecievedObj);
                }

            }
            else if (_workContext.CurrentCustomer.CustomerRoles.Where(x => x.SystemName == "Vendors").Any())
            {
                var vendorId = _customerService.GetAllCustomers().FirstOrDefault(x => x.Id == _workContext.CurrentCustomer.Id).VendorId;
                var bookingTransactions = _bookingTransactionrepository.Table.Where(x => x.VendorId == vendorId).ToList();
                foreach (var item in bookingTransactions)
                {
                    BookingRecievedListModel bookingRecievedObj = new BookingRecievedListModel();
                    bookingRecievedObj.BookingId = item.Id;
                    bookingRecievedObj.BookingDate = item.BookingDate.ToShortDateString();
                    bookingRecievedObj.CustomerName = item.CustomerName;
                    bookingRecievedObj.TableNumber = item.TableNumber;
                    bookingRecievedObj.CreatedDate = item.CreatedDt.ToShortDateString();
                    DateTime fromtime = DateTime.Today.Add(item.BookingTimeFrom);
                    bookingRecievedObj.BookingTimeFrom = fromtime.ToString("h:mm tt");
                    DateTime totime = DateTime.Today.Add(item.BookingTimeTo);
                    bookingRecievedObj.BookingTimeTo = totime.ToString("h:mm tt");
                    bookingRecievedList.Add(bookingRecievedObj);
                }
            }
            else
            {

            }
            bookingRecievedList = bookingRecievedList.Where(x => x.CreatedDate == DateTime.UtcNow.ToShortDateString()).OrderByDescending(x => x.BookingId).ToList();
            return Json(bookingRecievedList);

        }

        [NonAction]
        public string getGeoLoc(string CustAddress)
        {
            string latstr = "0,0";
            string url = "https://maps.google.com/maps/api/geocode/xml?address=" + CustAddress + "&key=" + _storeContext.GoogleMapAccountKey;
            WebRequest request = WebRequest.Create(url);
            using (WebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    DataSet dsResult = new DataSet();
                    dsResult.ReadXml(reader);

                    foreach (DataRow row in dsResult.Tables["result"].Rows)
                    {
                        string geometry_id = dsResult.Tables["geometry"].Select("result_id = " + row["result_id"].ToString())[0]["geometry_id"].ToString();
                        DataRow location = dsResult.Tables["location"].Select("geometry_id = " + geometry_id)[0];
                        latstr = location["lat"] + "," + location["lng"];
                        break;
                    }

                }
            }
            return latstr.ToString();
        }

        public virtual BookingTableTransactionListSearchModel GetAvailableTimings(string date, int merchantid)
        {
            var model = new BookingTableTransactionListSearchModel();
            model.DateOfBirthDay = date;
            var timingsList = _storeService.GetAllAvailableTiming();

            int CurrStoreId = _workContext.GetCurrentStoreId;
            int CurrVendorId = 0;
            var store = _storeContext.CurrentStoreById(CurrStoreId);
            if (store.IsAggrigator == false)
            {
                //Added By Mah
                CurrVendorId = _workContext.CurrentMerchantId;
            }
            else
            {
                if (_workContext.CurrentMerchant != null)
                    CurrVendorId = _workContext.CurrentMerchant.Id;
            }

            //var storeId = _workContext.GetCurrentStoreId; //_workContext.CurrentCustomer.RegisteredInStoreId;
            //var storeId = 1;
            var booked = _bookingrepository.Table.Where(x => x.StoreId == CurrStoreId && x.VendorId == CurrVendorId).ToList();
            DateTime dateTime = DateTime.Now;
            TimeSpan fromTime = dateTime.TimeOfDay;
            TimeSpan toTime = dateTime.TimeOfDay;
            foreach (var item in booked)
            {
                fromTime = item.TableAvailableFrom;
                toTime = item.TableAvailableTo;
            }
            foreach (var s in timingsList)
            {
                DateTime dateTimetoConvert = DateTime.ParseExact(s,
                                    "h:mm tt", CultureInfo.InvariantCulture);
                TimeSpan span = dateTimetoConvert.TimeOfDay;
                if (span >= fromTime && span <= toTime)
                    model.AvailableTimings.Add(new SelectListItem { Text = s, Value = s.ToString() });
            }
            return model;
        }
        public virtual IList<SelectListItem> GetAvailableToTimings(string From)
        {
            IList<SelectListItem> AvailableTimings = new List<SelectListItem>();
            var timingsList = _storeService.GetAllAvailableTiming();
            //var storeId = _workContext.GetCurrentStoreId;  //_workContext.CurrentCustomer.RegisteredInStoreId;
            //var storeId = 1;
            int CurrStoreId = _workContext.GetCurrentStoreId;
            int CurrVendorId = 0;
            var store = _storeContext.CurrentStoreById(CurrStoreId);
            if (store.IsAggrigator == false)
            {
                //Added By Mah
                var listven1 = _workContext.GetCurrentStoreId;
                CurrVendorId = Convert.ToInt32(listven1);
            }
            else
            {
                if (_workContext.CurrentMerchant != null)
                    CurrVendorId = _workContext.CurrentMerchant.Id;
            }

            var booked = _bookingrepository.Table.Where(x => x.StoreId == CurrStoreId && x.VendorId == CurrVendorId).ToList();
            DateTime dateTime = DateTime.Now;
            TimeSpan fromTime = dateTime.TimeOfDay;
            TimeSpan toTime = dateTime.TimeOfDay;
            foreach (var item in booked)
            {
                fromTime = item.TableAvailableFrom;
                toTime = item.TableAvailableTo;
            }
            foreach (var s in timingsList)
            {
                DateTime dateTimetoConvert = DateTime.ParseExact(s,
                                    "h:mm tt", CultureInfo.InvariantCulture);
                DateTime ConvertFromTiming = DateTime.ParseExact(From,
                                    "h:mm tt", CultureInfo.InvariantCulture);
                TimeSpan span = dateTimetoConvert.TimeOfDay;
                TimeSpan spanFrom = ConvertFromTiming.TimeOfDay;
                if (span > spanFrom && span <= toTime)
                    AvailableTimings.Add(new SelectListItem { Text = s, Value = s.ToString() });
            }
            return AvailableTimings;
        }

        public virtual IList<SelectListItem> GetAvailableTables(string From, string To, string date)
        {
            //For Get Vendor Id Added By Mahendra
            //var webHelper = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Core.IWebHelper>();
            //var storeUrl = webHelper.GetThisPageUrl(true);
            //string path = HttpContext.Current.Request.Url.AbsoluteUri;
            //string path1 = HttpContext.Current.Request.RawUrl;
            //string path2 = HttpContext.Current.Request.Url.OriginalString;
            //string path3 = HttpContext.Current.Request.Url.ToString();

            int CurrStoreId = _workContext.GetCurrentStoreId;
            int CurrVendorId = 0;
            var store = _storeContext.CurrentStoreById(CurrStoreId);
            if (store.IsAggrigator == false)
            {
                //Added By Mah
                CurrVendorId = _workContext.CurrentMerchantId;
            }
            else
            {
                if (_workContext.CurrentMerchant != null)
                    CurrVendorId = _workContext.CurrentMerchant.Id;
            }
            //

            var model = new BookingTableTransactionListModel();
            // model.DateOfBirthDay = date;
            IList<SelectListItem> AvailableTableforBookings = new List<SelectListItem>();
            //var storeId = _workContext.GetCurrentStoreId; //_workContext.CurrentCustomer.RegisteredInStoreId;
            //var storeId = 1;
            var TableAvailableforbooking = _bookingrepository.Table.Where(x => x.StoreId == CurrStoreId && x.VendorId == CurrVendorId && x.Status).ToList();
            DateTime dateTimeDate = DateTime.Parse(date);
            DateTime dateTimetoConvert = DateTime.ParseExact(From,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanFrom = dateTimetoConvert.TimeOfDay;
            DateTime dateTimetoConvertTo = DateTime.ParseExact(To,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanTo = dateTimetoConvertTo.TimeOfDay;
            //spanFrom.Subtract(new TimeSpan(0, 30, 0));
            //spanFrom=spanFrom.Add(new TimeSpan(0, 30, 0));
            //spanTo = spanTo.Subtract(new TimeSpan(0, 30, 0));

            DateTime dateTime = DateTime.Now;
            TimeSpan fromTime = dateTime.TimeOfDay;
            TimeSpan toTime = dateTime.TimeOfDay;
            var BookedTables = _bookingTransactionrepository.Table.Where(x => x.StoreId == CurrStoreId && x.VendorId == CurrVendorId && x.BookingDate == dateTimeDate
             && ((x.BookingTimeTo >= spanFrom && x.BookingTimeFrom <= spanFrom)) && x.Status == (int)BookingTableStatus.Booked).ToList();
            var BookedTablesto = _bookingTransactionrepository.Table.Where(x => x.StoreId == CurrStoreId && x.VendorId == CurrVendorId && x.BookingDate == dateTimeDate
            && ((x.BookingTimeTo >= spanTo && x.BookingTimeFrom <= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
            var BookedTables3 = _bookingTransactionrepository.Table.Where(x => x.StoreId == CurrStoreId && x.VendorId == CurrVendorId && x.BookingDate == dateTimeDate
            && ((x.BookingTimeFrom >= spanFrom && x.BookingTimeFrom <= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
            //BookedTables = _bookingTransactionrepository.Table.Where(x => x.StoreId == storeId && x.VendorId == vendorId && x.BookingDate == dateTimeDate
            //&& ((x.BookingTimeTo >= spanTo && x.BookingTimeFrom <= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
            var BookedTablesto3 = _bookingTransactionrepository.Table.Where(x => x.StoreId == CurrStoreId && x.VendorId == CurrVendorId && x.BookingDate == dateTimeDate
            && ((x.BookingTimeTo <= spanFrom && x.BookingTimeTo >= spanTo)) && x.Status == (int)BookingTableStatus.Booked).ToList();
            var lstBooksTable = BookedTables.Select(x => x.TableNumber).Union(BookedTablesto.Select(x => x.TableNumber))
                .Union(BookedTables3.Select(x => x.TableNumber)).Union(BookedTablesto3.Select(x => x.TableNumber));

            //var lstBooksTable = BookedTables.Select(x => x.TableNumber).ToList();

            foreach (var item in TableAvailableforbooking)
            {
                fromTime = item.TableAvailableFrom;
                toTime = item.TableAvailableTo;
            }

            var TableAvailableAccordingToSelectedTiming = TableAvailableforbooking.Where(x => !lstBooksTable.Contains(x.TableNo)).ToList();

            foreach (var item in TableAvailableAccordingToSelectedTiming)
            {

                AvailableTableforBookings.Add(new SelectListItem { Text = item.TableNo.ToString(), Value = item.TableNo.ToString() });
            }

            return AvailableTableforBookings;
        }
        public virtual IActionResult SubmitTableDetails(BookingTableTransactionListSearchModel bookingTableTransactionModel)
        {
            int CurrStoreId = _workContext.GetCurrentStoreId;
            int CurrVendorId = 0;
            var store = _storeContext.CurrentStoreById(CurrStoreId);
            if (store.IsAggrigator == false)
            {
                //Added By Mah
                var listven1 = _workContext.CurrentMerchantId;
                CurrVendorId = listven1;
            }
            else
            {
                if (_workContext.CurrentMerchant != null)
                    CurrVendorId = _workContext.CurrentMerchant.Id;
            }

            IList<SelectListItem> AvailableTableforBookings = new List<SelectListItem>();
            BookingTableTransaction bookingTableTransaction = new BookingTableTransaction();
            bookingTableTransaction.StoreId = CurrStoreId; //_workContext.GetCurrentStoreId;  //_workContext.CurrentCustomer.RegisteredInStoreId;
            bookingTableTransaction.VendorId = CurrVendorId; //_workContext.CurrentCustomer.VendorId;
            DateTime dateTimetoConvert = DateTime.ParseExact(bookingTableTransactionModel.From,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanFrom = dateTimetoConvert.TimeOfDay;
            DateTime dateTimetoConvertTo = DateTime.ParseExact(bookingTableTransactionModel.To,
                                    "h:mm tt", CultureInfo.InvariantCulture);
            TimeSpan spanTo = dateTimetoConvertTo.TimeOfDay;
            bookingTableTransaction.BookingTimeFrom = spanFrom;
            bookingTableTransaction.BookingTimeTo = spanTo;
            bookingTableTransaction.CreatedDt = DateTime.UtcNow;
            bookingTableTransaction.BookingDate = DateTime.Parse(bookingTableTransactionModel.DateOfBirthDay);
            bookingTableTransaction.CustomerName = bookingTableTransactionModel.CustomerName;
            bookingTableTransaction.Email = bookingTableTransactionModel.Email;
            bookingTableTransaction.MobileNumber = bookingTableTransactionModel.MobileNumber;
            bookingTableTransaction.TableNumber = bookingTableTransactionModel.TableNumber;
            bookingTableTransaction.Status = (int)BookingTableStatus.Booked;
            bookingTableTransaction.SeatingCapacity = bookingTableTransactionModel.SeatingCapacity;
            bookingTableTransaction.SpecialNotes = bookingTableTransactionModel.SpecialNotes;
            _bookingTransactionrepository.Insert(bookingTableTransaction);
            TempData["Success"] = "Your request submitted successfully";
            //int CurrStoreId = _workContext.GetCurrentStoreId;
            //var store = _storeContext.CurrentStoreById(CurrStoreId);
            //For Email 13-05-2019
            var emailId = _settings.Table.Where(x => x.Name.Contains("emailaccountsettings.defaultemailaccountid")).FirstOrDefault();
            if (emailId != null)
            {
                int Email = Convert.ToInt32(emailId.Value);
                var emailAccount = _emailAccountService.GetEmailAccountById(Email);
                var tokens = new List<Token>();
                _messageTokenProvider.AddStoreTokens(tokens, _storeContext.CurrentStore, emailAccount);
                if (store != null)
                {
                    _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
                }
                var vendorBody = _vendorService.GetVendorById(bookingTableTransaction.VendorId);
                if (vendorBody != null)
                {
                    _messageTokenProvider.AddVendorTokens(tokens, vendorBody);
                }

                var welcomeMessage = _messageTemplate.Table.Where(x => x.Name.ToString().Contains("Customer.Email.BookTable")).FirstOrDefault();

                var subject = _tokenizer.Replace(welcomeMessage.Subject, tokens, false);
                subject = subject.Replace("%Booking.Status%", "Booked");
                var body = _tokenizer.Replace(welcomeMessage.Body, tokens, true);
                body = body.Replace("%Booking.Status%", "Booked");
                body = body.Replace("%Customer.FullName%", bookingTableTransaction.CustomerName);
                _emailSender.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, bookingTableTransaction.Email, null);
            }
            //End For Email 13-05-2019

            if (store.IsAggrigator == false)
            {
                //Added By Mah
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    "CustomerCurrentMerchantId", CurrVendorId.ToString(), _workContext.GetCurrentStoreId);
                return Redirect("~/homemerchant");
                //return Redirect("~/homemerchant?merchantid=" + CurrVendorId.ToString());
            }
            else
                return Redirect("index");
        }


        public JsonResult OrderDetails(string OrderId)
        {
            OrderInfoNotification orderInfoNotification = new OrderInfoNotification();
            try
            {
                Guid OrderGUId = new Guid(OrderId);

                var order = _orderService.GetOrderByGuid(OrderGUId);
                if (order == null)
                {
                    orderInfoNotification.Status = false;
                    Json(orderInfoNotification);
                }
                var orderVendor = _orderService.GetOrderVendorDetails(order.Id);
                var orderCustomer = _orderService.GetOrderCustomerDetails(order.Id);
                orderInfoNotification.Status = true;
                orderInfoNotification.OrderId = order.Id.ToString();
                //pushOrderNotification.AgentId = ag.AgentId.ToString();
                orderInfoNotification.CustomerName = ""; //order.Customer.GetFullName();
                orderInfoNotification.MerchantName = "";
                orderInfoNotification.PickupLocation = "";
                orderInfoNotification.PickupLat = "0";
                orderInfoNotification.PickupLong = "0";
                orderInfoNotification.PickupMobileNo = "0";
                if (orderVendor != null)
                {
                    orderInfoNotification.MerchantName = orderVendor.MerchantName != null ? orderVendor.MerchantName : "";
                    orderInfoNotification.PickupLocation = orderVendor.PickupLocation != null ? orderVendor.PickupLocation : "";
                    orderInfoNotification.PickupLat = orderVendor.PickupLat != null ? orderVendor.PickupLat : "0";
                    orderInfoNotification.PickupLong = orderVendor.PickupLong != null ? orderVendor.PickupLong : "0";
                    orderInfoNotification.PickupMobileNo = orderVendor.PickupMobileNo != null ? orderVendor.PickupMobileNo : "0";
                }
                orderInfoNotification.DeliveryLocation = "0";
                orderInfoNotification.DeliveryLat = "0";
                orderInfoNotification.DeliveryLong = "0";
                orderInfoNotification.DeliveryMobileNo = "0";
                if (orderCustomer != null)
                {
                    orderInfoNotification.CustomerName = orderCustomer.CustomerName != null ? orderCustomer.CustomerName : "";
                    orderInfoNotification.DeliveryLocation = orderCustomer.CustomerLocation != null ? orderCustomer.CustomerLocation : "";
                    if (orderCustomer.CustomerLocation != null)
                    {
                        string[] geoLoc = getGeoLoc(orderCustomer.CustomerLocation).Split(',');
                        orderInfoNotification.DeliveryLat = geoLoc[0].ToString() != null ? geoLoc[0].ToString() : "0";
                        orderInfoNotification.DeliveryLong = geoLoc[1].ToString() != null ? geoLoc[1].ToString() : "0";
                        ;
                    }
                    orderInfoNotification.DeliveryMobileNo = orderCustomer.CustomerMobileNo != null ? orderCustomer.CustomerMobileNo : "";
                }
                return Json(orderInfoNotification);
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
                orderInfoNotification.Status = false;
                return Json(orderInfoNotification);
            }

        }
        public JsonResult OrderStatusDetails(string OrderId)
        {
            List<OrderStatusModel> listOrderStatusModel = new List<OrderStatusModel>();
            try
            {
                Guid OrderGUId = new Guid(OrderId);
                var order = _orderService.GetOrderByGuid(OrderGUId);
                var orderNotes = order.OrderNotes.GroupBy(x => x.Note).Select(x => x.FirstOrDefault()).ToList().OrderByDescending(x => x.Id);

                if (orderNotes.Any())
                {
                    foreach (var item in orderNotes)
                    {
                        if (item.Note.Equals("Order placed"))
                        {
                            OrderStatusModel orderStatusModel = new OrderStatusModel();
                            orderStatusModel.OrderStatus = "Order Recieved";
                            orderStatusModel.OrderStatusTime = item.CreatedOnUtc.ToString("hh:mm tt");
                            orderStatusModel.OrderDate = order.CreatedOnUtc.ToString("dd/MM/yyyy");
                            listOrderStatusModel.Add(orderStatusModel);
                        }
                        if (item.Note.Contains("Order status has been edited"))
                        {
                            OrderStatusModel orderStatusModel = new OrderStatusModel();
                            orderStatusModel.OrderStatus = item.Note.Replace("Order status has been edited. New status:", "");
                            orderStatusModel.OrderStatusTime = item.CreatedOnUtc.ToString("hh:mm tt");
                            orderStatusModel.OrderDate = order.CreatedOnUtc.ToString("dd/MM/yyyy");
                            listOrderStatusModel.Add(orderStatusModel);
                        }
                    }
                }
                //var orderNotes= _orderService.GetOrderNoteById
                return Json(listOrderStatusModel);
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
                return Json(strEx);
            }

        }

        public JsonResult OrderProductsDetails(string OrderId)
        {
            OrderModel orderModel = new OrderModel();
            try
            {
                Guid OrderGUId = new Guid(OrderId);
                var order = _orderService.GetOrderByGuid(OrderGUId);
                var OrderItems = order.OrderItems.ToList().OrderByDescending(x => x.Id);
                orderModel.CustomerName = order.ShippingAddress.FirstName + " " + order.ShippingAddress.LastName;
                orderModel.OrderDate = order.CreatedOnUtc.ToString("dd MMM yyyy");
                var vendorId = OrderItems.FirstOrDefault().Product.VendorId;
                orderModel.Merchant = _vendorService.GetVendorById(vendorId).Name;
                orderModel.OrderNumber = order.CustomOrderNumber;
                orderModel.OrderTotal = _priceFormatter.FormatPrice(order.OrderTotal);

                if (OrderItems.Any())
                {
                    foreach (var item in OrderItems)
                    {
                        OrderProductModel orderProductModel = new OrderProductModel();
                        orderProductModel.Price = _priceFormatter.FormatPrice(item.PriceInclTax);
                        orderProductModel.Product = item.Product.Name;
                        orderProductModel.ProductQty = item.Quantity.ToString();
                        orderModel.OrderProducts.Add(orderProductModel);
                        // orderProductModel.Add(orderStatusModel);
                    }
                }
                //var orderNotes= _orderService.GetOrderNoteById

                return Json(orderModel);
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
                return Json(strEx);
            }

        }

        public virtual BookingTableTransactionListSearchModel GetAvailableScheduleTimings()
        {
            var model = new BookingTableTransactionListSearchModel();
            //model.DateOfBirthDay = date;
            //var timingsList = _storeService.GetAllAvailableTiming();



            List<string> timeIntervals = new List<string>();
            TimeSpan startTime = new TimeSpan(0, 0, 0);
            DateTime startDate = new DateTime(DateTime.MinValue.Ticks); // Date to be used to get shortTime format.

            var timeObj = _vendorService.GetVendorById(_workContext.CurrentMerchant.Id);


            if (timeObj.LunchFrom != null && timeObj.LunchTo != null)
            {
                TimeSpan lunchFrom = (TimeSpan)timeObj.LunchFrom;
                TimeSpan lunchTo = (TimeSpan)timeObj.LunchTo;

                while (lunchFrom < lunchTo)
                {
                    TimeSpan timeToBeAdded = new TimeSpan(0, 15, 0);
                    TimeSpan t = lunchFrom.Add(timeToBeAdded);
                    DateTime result = startDate + t;
                    timeIntervals.Add(result.ToShortTimeString());
                    lunchFrom = t;
                }
            }

            if (timeObj.DinnerFrom != null && timeObj.DinnerTo != null)
            {
                TimeSpan dinnerFrom = (TimeSpan)timeObj.DinnerFrom;
                TimeSpan dinnerTo = (TimeSpan)timeObj.DinnerTo;

                while (dinnerFrom < dinnerTo)
                {
                    TimeSpan timeToBeAdded = new TimeSpan(0, 15, 0);
                    TimeSpan t = dinnerFrom.Add(timeToBeAdded);
                    DateTime result = startDate + t;
                    timeIntervals.Add(result.ToShortTimeString());
                    dinnerFrom = t;
                }
            }

            if ((timeObj.DinnerFrom == null || timeObj.DinnerTo == null) && (timeObj.LunchFrom == null || timeObj.LunchTo == null) && (timeObj.Opentime != null || timeObj.Closetime != null))
            {
                DateTime dateTimetoConvert = DateTime.ParseExact(timeObj.Opentime,
                                    "h:mm tt", CultureInfo.InvariantCulture);
                TimeSpan dinnerFrom = dateTimetoConvert.TimeOfDay;

                DateTime dateTimetoConvertClose = DateTime.ParseExact(timeObj.Closetime,
                                    "h:mm tt", CultureInfo.InvariantCulture);
                TimeSpan dinnerTo = dateTimetoConvertClose.TimeOfDay;


                while (dinnerFrom < dinnerTo)
                {
                    TimeSpan timeToBeAdded = new TimeSpan(0, 15, 0);
                    TimeSpan t = dinnerFrom.Add(timeToBeAdded);
                    DateTime result = startDate + t;
                    timeIntervals.Add(result.ToShortTimeString());
                    dinnerFrom = t;
                }
            }

            var timingsList = timeIntervals;


            DateTime dateTime = DateTime.Now;
            TimeSpan fromTime = dateTime.TimeOfDay;
            TimeSpan toTime = dateTime.TimeOfDay;

            foreach (var s in timingsList)
            {
                DateTime dateTimetoConvert = DateTime.ParseExact(s,
                                    "h:mm tt", CultureInfo.InvariantCulture);
                TimeSpan span = dateTimetoConvert.TimeOfDay;
                //if (span >= fromTime && span <= toTime)
                model.AvailableTimings.Add(new SelectListItem { Text = s, Value = s.ToString() });
            }
            return model;
        }

        public JsonResult CheckMerchantTiming(string localTime)
        {
            var date = DateTime.Now;
            var result = false;
            var merchant = _workContext.CurrentMerchant;

            if (merchant == null || (string.IsNullOrEmpty(merchant.Opentime) && string.IsNullOrEmpty(merchant.Closetime)))
                result = true;

            if (((string.IsNullOrEmpty(merchant.Opentime)) || Convert.ToDateTime(date.ToShortDateString() + " " + localTime) >= Convert.ToDateTime(date.ToShortDateString() + " " + merchant.Opentime)) && ((string.IsNullOrEmpty(merchant.Closetime)) || Convert.ToDateTime(date.ToShortDateString() + " " + localTime) <= Convert.ToDateTime(date.ToShortDateString() + " " + merchant.Closetime)))
                result = true;

            var orderDetails = _workContext.CurrentOrderDetails;
            if (orderDetails.ScheduleDate.HasValue && DateTime.Now <= orderDetails.ScheduleDate.Value)
                result = true;

            return Json(result);
        }

        public virtual IActionResult DisplayPopup(int vendorId)
        {
            var currentVendorId = _workContext.CurrentMerchantId;
            var popup = currentVendorId == vendorId;
            return Json(popup);
        }
        #endregion

    }


    public class OrderInfoNotification
    {
        public bool Status { get; set; }
        public string OrderId { get; set; }
        //public string AgentId { get; set; }
        public string CustomerName { get; set; }
        public string MerchantName { get; set; }
        public string PickupLocation { get; set; }
        public string PickupLat { get; set; }
        public string PickupLong { get; set; }
        public string PickupMobileNo { get; set; }
        public string DeliveryLocation { get; set; }
        public string DeliveryLat { get; set; }
        public string DeliveryLong { get; set; }
        public string DeliveryMobileNo { get; set; }
    }
    public class BookingModel
    {
        public string From { get; set; }
        public string To { get; set; }
    }
    public class BookingRecievedListModel
    {
        public int BookingId { get; set; }
        public string CustomerName { get; set; }
        public int TableNumber { get; set; }
        public string BookingDate { get; set; }
        public string CreatedDate { get; set; }
        public string BookingTimeFrom { get; set; }
        public string BookingTimeTo { get; set; }
    }

    public class OrderModel
    {
        public OrderModel()
        {
            OrderProducts = new List<OrderProductModel>();
        }
        public string OrderDate { get; set; }
        public string CustomerName { get; set; }
        public string Merchant { get; set; }
        public string OrderNumber { get; set; }
        public string OrderTotal { get; set; }
        public List<OrderProductModel> OrderProducts { get; set; }

    }
    public class OrderProductModel
    {
        public string Product { get; set; }
        public string Price { get; set; }

        public string ProductQty { get; set; }
    }

    public class OrderStatusModel
    {
        public string OrderDate { get; set; }
        public string OrderStatus { get; set; }
        public string OrderStatusTime { get; set; }
    }
    public enum BookingTableStatus
    {
        Cancelled = 3,
        Booked = 1,
        Complete = 2
    }

}