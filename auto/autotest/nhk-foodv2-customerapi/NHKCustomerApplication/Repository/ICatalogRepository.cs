﻿using NHKCustomerApplication.ViewModel;
using NHKCustomerApplication.ViewModels;

namespace NHKCustomerApplication.Repository
{
    public interface ICatalogRespository
    {
        MerchantCategoryResponceModel GetCategoryByVendor(Categorys categorys, int languageId);
        CustomerResponseModel GetMerchantCategories(MerchantCategorysModelV2 merchantCategorysModelV2, int languageId);
        MerchantCategoryListResponceModelV1 GetMerchntCategoriesListById(MerchantCategorysModelV2 merchantCategorysModelV2);
    }
}
