﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Stores;

namespace Nop.Core.Domain.RatingReview
{
    /// <summary>
    /// Represents a product review
    /// </summary>
    public partial class RatingReviews : BaseEntity
    {

        /// <summary>
        /// Gets or sets the customer identifier
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the EntityId identifier
        /// </summary>
        public int EntityId { get; set; }

        /// <summary>
        /// Gets or sets the OrderId identifier
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Gets or sets the ReviewType
        /// </summary>
        public int ReviewType { get; set; }

        /// <summary>
        /// Gets or sets the store identifier
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the content is approved
        /// </summary>
        public bool IsApproved { get; set; }

        /// <summary>
        /// Gets or sets the title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the review text
        /// </summary>
        public string ReviewText { get; set; }

        /// <summary>
        /// Gets or sets the reply text
        /// </summary>
        public string ReplyText { get; set; }

        /// <summary>
        /// Review rating
        /// </summary>
        public int Rating { get; set; }

        /// <summary>
        /// Gets or sets the Review Options
        /// </summary>
        public string ReviewOptions { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it is ilked
        /// </summary>
        public bool IsLiked { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether customer is not isterested
        /// </summary>
        public bool IsNotInterested { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the review is skipped
        /// </summary>
        public bool IsSkipped { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the disliked review options
        /// </summary>
        public string DislikedReviewOptions { get; set; }

        /// <summary>
        /// Gets or sets the customer
        /// </summary>
        public virtual Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets the store
        /// </summary>
        public virtual Store Store { get; set; }

    }
}
