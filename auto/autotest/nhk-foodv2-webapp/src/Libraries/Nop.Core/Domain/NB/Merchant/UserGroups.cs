﻿namespace Nop.Core.Domain.NB.Merchant
{
    /// <summary>
    /// Represents an user groups enumeration
    /// </summary>
    public enum UserGroups
    {
        /// <summary>
        /// Pending
        /// </summary>
        Business = 10,

        /// <summary>
        /// Processing
        /// </summary>
        Merchant = 20,
    }
}
