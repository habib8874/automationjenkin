﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.NB.Address
{
    public enum LocationAddressType
    {
        Home = 1,
        Office =2,
        Other = 3
    }
}
