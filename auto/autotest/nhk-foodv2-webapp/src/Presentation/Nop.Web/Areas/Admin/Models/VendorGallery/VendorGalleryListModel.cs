﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Areas.Admin.Models.VendorGallery
{
    public partial class VendorGalleryListModel : BasePagedListModel<VendorGalleryDataModel>
    {
    }
    public partial class VendorGalleryListSearchModel : BaseSearchModel
    {
        public VendorGalleryListSearchModel()
        {
            AvailableStores = new List<SelectListItem>();
            AvailableVendors = new List<SelectListItem>();
            AvailableGalleryType = new List<SelectListItem>();

        }
        public int Id { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.List.SearchName")]
        public string SearchName { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.List.SearchStore")]
        public int SearchStoreId { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.List.SearchVendor")]
        public int SearchVendorId { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.GalleryType")]
        public int GalleryTypeId { get; set; }
        public IList<SelectListItem> AvailableStores{ get; set; }
        public IList<SelectListItem> AvailableVendors { get; set; }
        public IList<SelectListItem> AvailableGalleryType{ get; set; }

        [NopResourceDisplayName("Admin.VendorGallery.Fields.Store")]
        public string Store { get; set; }
        public int StoreId { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.Vendor")]
        public string Vendor { get; set; }
        public string PictureThumbnailUrl { get; set; }
        public string VideoThumbnailUrl { get; set; }
        public int VendorId { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.GalleryType")]
        public string GalleryType { get; set; }
        [UIHint("Picture")]
        [NopResourceDisplayName("Admin.VendorGallery.Fields.Picture")]
        public int PictureId { get; set; }

        [NopResourceDisplayName("Admin.VendorGallery.Fields.GalleryURL")]
        public string GalleryURL { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.GalleryCaption")]
        public string GalleryCaption { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.Active")]
        public bool Active { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.DisplayOrder")]
        public Nullable<int> DisplayOrder { get; set; }


    }
    public partial class VendorGalleryDataModel : BaseSearchModel
    {
        public int Id { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.List.SearchName")]
        public string SearchName { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.List.SearchStore")]
        public int SearchStoreId { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.List.SearchVendor")]
        public int SearchVendorId { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.GalleryType")]
        public int GalleryTypeId { get; set; }

        [NopResourceDisplayName("Admin.VendorGallery.Fields.Store")]
        public string Store { get; set; }
        public int StoreId { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.Vendor")]
        public string Vendor { get; set; }
        public string PictureThumbnailUrl { get; set; }
        public string VideoThumbnailUrl { get; set; }
        public int VendorId { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.GalleryType")]
        public string GalleryType { get; set; }
        [UIHint("Picture")]
        [NopResourceDisplayName("Admin.VendorGallery.Fields.Picture")]
        public int PictureId { get; set; }

        [NopResourceDisplayName("Admin.VendorGallery.Fields.GalleryURL")]
        public string GalleryURL { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.GalleryCaption")]
        public string GalleryCaption { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.Active")]
        public bool Active { get; set; }
        [NopResourceDisplayName("Admin.VendorGallery.Fields.DisplayOrder")]
        public Nullable<int> DisplayOrder { get; set; }


    }
}