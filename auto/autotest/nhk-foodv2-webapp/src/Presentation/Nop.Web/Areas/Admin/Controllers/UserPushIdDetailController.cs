﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Areas.Admin.Models.UserPushIdDetails;
using Nop.Core.Domain.UserPushIdDetails;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Services.UserPushIdDetails;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Services.Customers;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Controllers
{
    public class UserPushIdDetailController : Controller
    {
        #region Fields

        private readonly IUserPushIdDetailService _userPushIdDetailService;
        
        #endregion

        #region Ctor

        public UserPushIdDetailController(IUserPushIdDetailService userPushIdDetailService)
        {
            this._userPushIdDetailService = userPushIdDetailService;            
        }

        #endregion
        public IActionResult Index()
        {
            return View();
        }

        //[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [HttpPost]
        public void Create(int UserId,string Token)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
            //    return AccessDeniedView();

            UserPushIdDetail userPushIdDetail = new UserPushIdDetail();
            userPushIdDetail.UserId = UserId;
            userPushIdDetail.Token = Token;
            userPushIdDetail.CrDt = DateTime.Now;

            _userPushIdDetailService.InsertUserPushIdDetails(userPushIdDetail);

            //SuccessNotification(_localizationService.GetResource("Admin.Configuration.Stores.Added"));
            //return continueEditing ? RedirectToAction("Edit", new { id = store.Id }) : RedirectToAction("List");
            //If we got this far, something failed, redisplay form
                        
        }
    }
}