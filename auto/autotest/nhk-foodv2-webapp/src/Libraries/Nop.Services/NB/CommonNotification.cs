﻿using System.Collections.Generic;

namespace Nop.Services.NB
{
    public partial class CommonNotification
    {
        #region MyRegion
        //GeoLookupService location Model
        public class GeocodedWaypoint
        {
            public string geocoder_status { get; set; }
            public string place_id { get; set; }
            public List<string> types { get; set; }
        }

        public class Northeast
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class Southwest
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class Bounds
        {
            public Northeast northeast { get; set; }
            public Southwest southwest { get; set; }
        }

        public class Distance
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        public class Duration
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        public class EndLocation
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class StartLocation
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class Distance2
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        public class Duration2
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        public class EndLocation2
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class Polyline
        {
            public string points { get; set; }
        }

        public class StartLocation2
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class Distance3
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        public class Duration3
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        public class EndLocation3
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class Polyline2
        {
            public string points { get; set; }
        }

        public class StartLocation3
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class Step2
        {
            public Distance3 distance { get; set; }
            public Duration3 duration { get; set; }
            public EndLocation3 end_location { get; set; }
            public string html_instructions { get; set; }
            public Polyline2 polyline { get; set; }
            public StartLocation3 start_location { get; set; }
            public string travel_mode { get; set; }
        }

        public class Step
        {
            public Distance2 distance { get; set; }
            public Duration2 duration { get; set; }
            public EndLocation2 end_location { get; set; }
            public string html_instructions { get; set; }
            public Polyline polyline { get; set; }
            public StartLocation2 start_location { get; set; }
            public List<Step2> steps { get; set; }
            public string travel_mode { get; set; }
        }

        public class Leg
        {
            public Distance distance { get; set; }
            public Duration duration { get; set; }
            public string end_address { get; set; }
            public EndLocation end_location { get; set; }
            public string start_address { get; set; }
            public StartLocation start_location { get; set; }
            public List<Step> steps { get; set; }
            public List<object> traffic_speed_entry { get; set; }
            public List<object> via_waypoint { get; set; }
        }

        public class OverviewPolyline
        {
            public string points { get; set; }
        }

        public class Route
        {
            public Bounds bounds { get; set; }
            public string copyrights { get; set; }
            public List<Leg> legs { get; set; }
            public OverviewPolyline overview_polyline { get; set; }
            public string summary { get; set; }
            public List<string> warnings { get; set; }
            public List<object> waypoint_order { get; set; }
        }

        public class GoogleTimeAPIModel
        {
            public List<GeocodedWaypoint> geocoded_waypoints { get; set; }
            public List<Route> routes { get; set; }
            public string status { get; set; }
        }

        #endregion

        public class FCSResultModel
        {
            public bool Result { get; set; }
            public string ResultMsg { get; set; }
        }
        public class Result
        {
            public string error { get; set; }
            public string message_id { get; set; }
        }

        public class FCSResultObjModel
        {
            public long multicast_id { get; set; }
            public int success { get; set; }
            public int failure { get; set; }
            public int canonical_ids { get; set; }
            public List<Result> results { get; set; }

        }

        public class FCSNotificationModel
        {
            public string ApiKey { get; set; }
            //public string WebAddr { get; set; }
            //public string WebKey { get; set; }
            public string To { get; set; }
            public string Heading { get; set; }
            public string OrderId { get; set; }
            public string CustomerId { get; set; }
            public string StoreId { get; set; }
            public string UniqueSeoCode { get; set; }
            public string Status { get; set; }
            public string DeliveredTime { get; set; }
        }
    }
}
