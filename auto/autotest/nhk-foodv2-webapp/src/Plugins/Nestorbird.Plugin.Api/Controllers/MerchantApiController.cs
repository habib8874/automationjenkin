﻿using Microsoft.AspNetCore.Mvc;
using Nestorbird.Plugin.Api.Model;
using Nop.Core;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nestorbird.Plugin.Api.Controllers
{
    [AuthorizeAdmin]
    [Area(AreaNames.Admin)]
    public class MerchantApiController: BasePluginController
    {
        #region Fields

        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingService;
        private readonly INotificationService _notificationService;
        private readonly ILocalizationService _localizationService;

        #endregion

        #region Ctor

        public MerchantApiController(IStoreContext storeContext,
            ISettingService settingService,
            INotificationService notificationService,
            ILocalizationService localizationService)
        {
            _storeContext = storeContext;
            _settingService = settingService;
            _notificationService = notificationService;
            _localizationService = localizationService;
        }

        #endregion

        public IActionResult Configure()
        {
            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var merchantAPISettings = _settingService.LoadSetting<MerchantAPISettings>(storeScope);

            var model = new ConfigurationModel
            {
                MerchantAppWebKey = merchantAPISettings.MerchantAppWebKey,
                ActiveStoreScopeConfiguration = storeScope
            };

            if (storeScope > 0)
            {
                model.MerchantAppWebKey_OverrideForStore = _settingService.SettingExists(merchantAPISettings, x => x.MerchantAppWebKey, storeScope);
            }
            return View("~/Plugins/Nestorbird.Plugin.Api/Views/Configure.cshtml", model);
        }

        [HttpPost]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var merchantAPISettings = _settingService.LoadSetting<MerchantAPISettings>(storeScope);

            //save settings
            merchantAPISettings.MerchantAppWebKey = model.MerchantAppWebKey;
                        
            _settingService.SaveSettingOverridablePerStore(merchantAPISettings, x => x.MerchantAppWebKey, model.MerchantAppWebKey_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));
            return Configure();
        }
    }
}
