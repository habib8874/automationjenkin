﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core.Domain.NB.Package;

namespace Nop.Services.NB.Package
{
     public partial interface INBPackageService
    {

        #region packages

        /// <summary>
        /// Deletes a package
        /// </summary>
        /// <param name="package">Package</param>
        void DeletePackage(NBPackage package);

        /// <summary>
        /// Gets a package
        /// </summary>
        /// <param name="packageId">The package identifier</param>
        /// <returns>Package</returns>
        NBPackage GetPackageById(int packageId);

        /// <summary>
        /// Gets all packages
        /// </summary>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <returns>Packages</returns>
        IList<NBPackage> GetAllPackages(int storeId, string name);

        /// <summary>
        /// Inserts a package
        /// </summary>
        /// <param name="package">Package</param>
        void InsertPackage(NBPackage package);

        /// <summary>
        /// Updates the package
        /// </summary>
        /// <param name="package">Package</param>
        void UpdatePackage(NBPackage package);

        #endregion

        #region Package plans
        /// <summary>
        /// Deletes a packagePlan
        /// </summary>
        /// <param name="packagePlan">PackagePlan</param>
        void DeletePackagePlan(NBPackagePlan packagePlan);

        /// <summary>
        /// Gets a packagePlan
        /// </summary>
        /// <param name="packagePlanId">The packagePlan identifier</param>
        /// <returns>PackagePlan</returns>
        NBPackagePlan GetPackagePlanById(int packagePlanId);

        /// <summary>
        /// Gets packagePlans
        /// </summary>
        /// <param name="packageId">get records by packageId</param>
        /// <param name="productId">get records by productId</param>
        /// <returns>PackagePlans</returns>
        IList<NBPackagePlan> GetPackagePlans(int packageId = 0, int productId = 0);

        /// <summary>
        /// Inserts a packagePlan
        /// </summary>
        /// <param name="packagePlan">PackagePlan</param>
        void InsertPackagePlan(NBPackagePlan packagePlan);

        /// <summary>
        /// Updates the packagePlan
        /// </summary>
        /// <param name="packagePlan">PackagePlan</param>
        void UpdatePackagePlan(NBPackagePlan packagePlan);

        /// <summary>
        /// Gets a packagePlan
        /// </summary>
        /// <param name="productId">The productId identifier</param>
        /// <returns>PackagePlan</returns>
        NBPackagePlan GetPackagePlanByProductId(int productId);

        #endregion
    }
}
