﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Vendors;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Common;
using Nop.Web.Areas.Admin.Models.Vendors;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.DataTables;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the vendor model factory implementation
    /// </summary>
    public partial class VendorModelFactory : IVendorModelFactory
    {
        #region Fields

        private readonly IAddressAttributeModelFactory _addressAttributeModelFactory;
        private readonly IAddressService _addressService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ICustomerService _customerService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedModelFactory _localizedModelFactory;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IVendorAttributeParser _vendorAttributeParser;
        private readonly IVendorAttributeService _vendorAttributeService;
        private readonly IVendorService _vendorService;
        private readonly VendorSettings _vendorSettings;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;
        private readonly IPictureService _pictureService;
        private readonly IDiscountService _discountService;
        private readonly IDiscountSupportedModelFactory _discountSupportedModelFactory;

        #endregion

        #region Ctor
        public VendorModelFactory(IAddressAttributeModelFactory addressAttributeModelFactory,
            IAddressService addressService,
            IBaseAdminModelFactory baseAdminModelFactory,
            ICustomerService customerService,
            IDateTimeHelper dateTimeHelper,
            IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            ILocalizedModelFactory localizedModelFactory,
            IUrlRecordService urlRecordService,
            IVendorAttributeParser vendorAttributeParser,
            IVendorAttributeService vendorAttributeService,
            IVendorService vendorService,
            VendorSettings vendorSettings,
            IStoreService storeService,
            IWorkContext workContext,
            IPictureService pictureService,
            IDiscountService discountService,
            IDiscountSupportedModelFactory discountSupportedModelFactory)
        {
            _addressAttributeModelFactory = addressAttributeModelFactory;
            _addressService = addressService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _customerService = customerService;
            _dateTimeHelper = dateTimeHelper;
            _genericAttributeService = genericAttributeService;
            _localizationService = localizationService;
            _localizedModelFactory = localizedModelFactory;
            _urlRecordService = urlRecordService;
            _vendorAttributeParser = vendorAttributeParser;
            _vendorAttributeService = vendorAttributeService;
            _vendorService = vendorService;
            _vendorSettings = vendorSettings;
            _storeService = storeService;
            _workContext = workContext;
            _pictureService = pictureService;
            _discountService = discountService;
            _discountSupportedModelFactory = discountSupportedModelFactory;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Prepare vendor associated customer models
        /// </summary>
        /// <param name="models">List of vendor associated customer models</param>
        /// <param name="vendor">Vendor</param>
        protected virtual void PrepareAssociatedCustomerModels(IList<VendorAssociatedCustomerModel> models, Vendor vendor)
        {
            if (models == null)
                throw new ArgumentNullException(nameof(models));

            if (vendor == null)
                throw new ArgumentNullException(nameof(vendor));

            var associatedCustomers = _customerService.GetAllCustomers(vendorId: vendor.Id);
            foreach (var customer in associatedCustomers)
            {
                models.Add(new VendorAssociatedCustomerModel
                {
                    Id = customer.Id,
                    Email = customer.Email
                });
            }
        }

        /// <summary>
        /// Prepare vendor attribute models
        /// </summary>
        /// <param name="models">List of vendor attribute models</param>
        /// <param name="vendor">Vendor</param>
        protected virtual void PrepareVendorAttributeModels(IList<VendorModel.VendorAttributeModel> models, Vendor vendor)
        {
            if (models == null)
                throw new ArgumentNullException(nameof(models));

            //get available vendor attributes
            var vendorAttributes = _vendorAttributeService.GetAllVendorAttributes();
            foreach (var attribute in vendorAttributes)
            {
                var attributeModel = new VendorModel.VendorAttributeModel
                {
                    Id = attribute.Id,
                    Name = attribute.Name,
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _vendorAttributeService.GetVendorAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var attributeValueModel = new VendorModel.VendorAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.Name,
                            IsPreSelected = attributeValue.IsPreSelected
                        };
                        attributeModel.Values.Add(attributeValueModel);
                    }
                }

                //set already selected attributes
                if (vendor != null)
                {
                    var selectedVendorAttributes = _genericAttributeService.GetAttribute<string>(vendor, NopVendorDefaults.VendorAttributes);
                    switch (attribute.AttributeControlType)
                    {
                        case AttributeControlType.DropdownList:
                        case AttributeControlType.RadioList:
                        case AttributeControlType.Checkboxes:
                            {
                                if (!string.IsNullOrEmpty(selectedVendorAttributes))
                                {
                                    //clear default selection
                                    foreach (var item in attributeModel.Values)
                                        item.IsPreSelected = false;

                                    //select new values
                                    var selectedValues = _vendorAttributeParser.ParseVendorAttributeValues(selectedVendorAttributes);
                                    foreach (var attributeValue in selectedValues)
                                        foreach (var item in attributeModel.Values)
                                            if (attributeValue.Id == item.Id)
                                                item.IsPreSelected = true;
                                }
                            }
                            break;
                        case AttributeControlType.ReadonlyCheckboxes:
                            {
                                //do nothing
                                //values are already pre-set
                            }
                            break;
                        case AttributeControlType.TextBox:
                        case AttributeControlType.MultilineTextbox:
                            {
                                if (!string.IsNullOrEmpty(selectedVendorAttributes))
                                {
                                    var enteredText = _vendorAttributeParser.ParseValues(selectedVendorAttributes, attribute.Id);
                                    if (enteredText.Any())
                                        attributeModel.DefaultValue = enteredText[0];
                                }
                            }
                            break;
                        case AttributeControlType.Datepicker:
                        case AttributeControlType.ColorSquares:
                        case AttributeControlType.ImageSquares:
                        case AttributeControlType.FileUpload:
                        default:
                            //not supported attribute control types
                            break;
                    }
                }

                models.Add(attributeModel);
            }
        }

        /// <summary>
        /// Prepare address model
        /// </summary>
        /// <param name="model">Address model</param>
        /// <param name="address">Address</param>
        protected virtual void PrepareAddressModel(AddressModel model, Address address)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            //set some of address fields as enabled and required
            model.CountryEnabled = true;
            model.StateProvinceEnabled = true;
            model.CountyEnabled = true;
            model.CityEnabled = true;
            model.StreetAddressEnabled = true;
            model.StreetAddress2Enabled = true;
            model.ZipPostalCodeEnabled = true;
            model.PhoneEnabled = true;
            model.FaxEnabled = true;

            //prepare available countries
            _baseAdminModelFactory.PrepareCountries(model.AvailableCountries);

            //prepare available states
            _baseAdminModelFactory.PrepareStatesAndProvinces(model.AvailableStates, model.CountryId);

            //prepare custom address attributes
            _addressAttributeModelFactory.PrepareCustomAddressAttributes(model.CustomAddressAttributes, address);
        }

        /// <summary>
        /// Prepare vendor note search model
        /// </summary>
        /// <param name="searchModel">Vendor note search model</param>
        /// <param name="vendor">Vendor</param>
        /// <returns>Vendor note search model</returns>
        protected virtual VendorNoteSearchModel PrepareVendorNoteSearchModel(VendorNoteSearchModel searchModel, Vendor vendor)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (vendor == null)
                throw new ArgumentNullException(nameof(vendor));

            searchModel.VendorId = vendor.Id;

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare vendor slider picture search model
        /// </summary>
        /// <param name="searchModel">Vendor slider picture search model</param>
        /// <param name="vendor">Vendor</param>
        /// <returns>Vendor slider picture search model</returns>
        public virtual VendorSliderPictureSearchModel PrepareVendorSliderPictureSearchModel(VendorSliderPictureSearchModel searchModel, Vendor vendor)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (vendor == null)
                throw new ArgumentNullException(nameof(vendor));

            searchModel.VendorId = vendor.Id;

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare vendor search model
        /// </summary>
        /// <param name="searchModel">Vendor search model</param>
        /// <returns>Vendor search model</returns>
        public virtual VendorSearchModel PrepareVendorSearchModel(VendorSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged vendor list model
        /// </summary>
        /// <param name="searchModel">Vendor search model</param>
        /// <returns>Vendor list model</returns>
        public virtual VendorListModel PrepareVendorListModel(VendorSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (_workContext.IsAdminRole)
            {
                //get vendors
                var vendors = _vendorService.GetAllVendors(showHidden: true,
                    name: searchModel.SearchName,
                    pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

                //prepare list model
                var model = new VendorListModel().PrepareToGrid(searchModel, vendors, () =>
                {
                    //fill in model values from the entity
                    return vendors.Select(vendor =>
                        {
                            var vendorModel = vendor.ToModel<VendorModel>();
                            vendorModel.SeName = _urlRecordService.GetSeName(vendor, 0, true, false);

                            return vendorModel;
                        });
                });

                return model;
            }
            else if (_workContext.IsStoreOwnerRole)
            {
                //get vendors
                var stores = _storeService.GetAllStores().Where(x => x.StoreOwnerId == _workContext.CurrentCustomer.Id).Select(x => x.Id).ToList();

                var vendors = _vendorService.GetAllVendorsByStore(stores, null, searchModel.SearchName, searchModel.Page - 1, searchModel.PageSize, true);

                //prepare list model
                var model = new VendorListModel().PrepareToGrid(searchModel, vendors, () =>
                {
                    //fill in model values from the entity
                    return vendors.Select(vendor =>
                    {
                        var vendorModel = vendor.ToModel<VendorModel>();
                        vendorModel.SeName = _urlRecordService.GetSeName(vendor, 0, true, false);

                        return vendorModel;
                    });
                });

                return model;
            }
            else if (_workContext.IsVendorRole)
            {
                int[] ids = new int[1];
                ids[0] = _workContext.CurrentVendor.Id;

                var vendors = _vendorService.GetVendorsByIds(ids).ToPagedList(searchModel);

                //prepare list model
                var model = new VendorListModel().PrepareToGrid(searchModel, vendors, () =>
                {
                    //fill in model values from the entity
                    return vendors.Select(vendor =>
                    {
                        var vendorModel = vendor.ToModel<VendorModel>();
                        vendorModel.SeName = _urlRecordService.GetSeName(vendor, 0, true, false);

                        return vendorModel;
                    });
                });

                return model;
            }
            else if (_workContext.SubAdmin.IsSubAdminRole)
            {
                //get vendors
                var vendors = _vendorService.GetAllVendors(showHidden: true, storeId: _workContext.GetCurrentStoreId,
                    name: searchModel.SearchName,
                    pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

                var subAdminAccosiatedvendorIds = new List<int>();
                //a subadmin should have access only to his accosiated vendors products
                if (_workContext.SubAdmin.IsSubAdminRole)
                {
                    subAdminAccosiatedvendorIds = _workContext.SubAdmin.AssociatedVenorIds?.ToList();
                    vendors = vendors.Where(v => subAdminAccosiatedvendorIds.Contains(v.Id)).ToList().ToPagedList(searchModel);
                }

                //prepare list model
                var model = new VendorListModel().PrepareToGrid(searchModel, vendors, () =>
                {
                    //fill in model values from the entity
                    return vendors.Select(vendor =>
                    {
                        var vendorModel = vendor.ToModel<VendorModel>();
                        vendorModel.SeName = _urlRecordService.GetSeName(vendor, 0, true, false);

                        return vendorModel;
                    });
                });

                return model;
            }

            return new VendorListModel();
        }

        /// <summary>
        /// Prepare vendor model
        /// </summary>
        /// <param name="model">Vendor model</param>
        /// <param name="vendor">Vendor</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Vendor model</returns>
        public virtual VendorModel PrepareVendorModel(VendorModel model, Vendor vendor, bool excludeProperties = false)
        {
            Action<VendorLocalizedModel, int> localizedModelConfiguration = null;

            if (vendor != null)
            {
                //fill in model values from the entity
                if (model == null)
                {
                    model = vendor.ToModel<VendorModel>();
                    model.SeName = _urlRecordService.GetSeName(vendor, 0, true, false);
                }

                //define localized model configuration action
                localizedModelConfiguration = (locale, languageId) =>
                {
                    locale.Name = _localizationService.GetLocalized(vendor, entity => entity.Name, languageId, false, false);
                    locale.Description = _localizationService.GetLocalized(vendor, entity => entity.Description, languageId, false, false);
                    locale.MetaKeywords = _localizationService.GetLocalized(vendor, entity => entity.MetaKeywords, languageId, false, false);
                    locale.MetaDescription = _localizationService.GetLocalized(vendor, entity => entity.MetaDescription, languageId, false, false);
                    locale.MetaTitle = _localizationService.GetLocalized(vendor, entity => entity.MetaTitle, languageId, false, false);
                    locale.SeName = _urlRecordService.GetSeName(vendor, languageId, false, false);
                };


                if (vendor.AvailableType == (int)AvailableTypeEnum.AllDay)
                {
                    model.IsAllDay = true;
                }
                if (vendor.AvailableType == (int)AvailableTypeEnum.Custom)
                {
                    model.IsCustom = true;
                }
                //Mah                
                int customerId = _workContext.CurrentCustomer.Id;
                List<int> rollIds = _workContext.CurrentCustomer.CustomerRoles.Select(x => x.Id).ToList();
                var storeName = _storeService.GetAllStores().FirstOrDefault(x => x.StoreOwnerId == customerId);

                //precheck Registered Role as a default role while creating a new customer through admin
                if (storeName != null)
                {
                    model.StoreId = storeName.Id;
                }
                //Mah

                //prepare associated customers
                PrepareAssociatedCustomerModels(model.AssociatedCustomers, vendor);

                //prepare nested search models
                PrepareVendorNoteSearchModel(model.VendorNoteSearchModel, vendor);

                //prepare vendor slider picture model
                PrepareVendorSliderPictureSearchModel(model.VendorSliderPictureSearchModel, vendor);
            }
            //prepare available time zones
            _baseAdminModelFactory.PrepareTimeZones(model.AvailableTimeZones, false);
            //set default values for the new model
            if (vendor == null)
            {
                model.PageSize = 6;
                model.Active = true;
                model.AllowCustomersToSelectPageSize = true;
                model.PageSizeOptions = _vendorSettings.DefaultVendorPageSizeOptions;
            }

            //prepare localized models
            if (!excludeProperties)
                model.Locales = _localizedModelFactory.PrepareLocalizedModels(localizedModelConfiguration);

            #region Merchant Distance fee
            if (vendor == null)
            {
                model.FixedOrDynamic = "f";
                model.KmOrMilesForDynamic = "k";
                model.PriceForDynamic = decimal.Zero;
                model.DistanceForDynamic = 0;
                model.PricePerUnitDistanceForDynamic = decimal.Zero;
            }
            #endregion

            //prepare model vendor attributes
            PrepareVendorAttributeModels(model.VendorAttributes, vendor);

            //prepare address model
            var address = _addressService.GetAddressById(vendor?.AddressId ?? 0);
            if (!excludeProperties && address != null)
                model.Address = address.ToModel(model.Address);
            PrepareAddressModel(model.Address, address);

            //check is vendor or not
            model.IsVendor = _workContext.IsVendorRole;


            //prepare model discounts
            var availableDiscounts = _discountService.GetAllDiscounts(DiscountType.AssignedToVendor, showHidden: true);
            _discountSupportedModelFactory.PrepareNBModelDiscounts(model, vendor, availableDiscounts, excludeProperties);

            if (model.SelectedDiscountIds.Count > 0)
            {
                model.DiscountId = model.SelectedDiscountIds.FirstOrDefault();
            }
            return model;
        }

        /// <summary>
        /// Prepare paged vendor note list model
        /// </summary>
        /// <param name="searchModel">Vendor note search model</param>
        /// <param name="vendor">Vendor</param>
        /// <returns>Vendor note list model</returns>
        public virtual VendorNoteListModel PrepareVendorNoteListModel(VendorNoteSearchModel searchModel, Vendor vendor)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (vendor == null)
                throw new ArgumentNullException(nameof(vendor));

            //get vendor notes
            var vendorNotes = vendor.VendorNotes.OrderByDescending(note => note.CreatedOnUtc).ToList().ToPagedList(searchModel);

            //prepare list model
            var model = new VendorNoteListModel().PrepareToGrid(searchModel, vendorNotes, () =>
            {
                //fill in model values from the entity
                return vendorNotes.Select(note =>
                {
                    //fill in model values from the entity        
                    var vendorNoteModel = note.ToModel<VendorNoteModel>();

                    //convert dates to the user time
                    vendorNoteModel.CreatedOn = _dateTimeHelper.ConvertToUserTime(note.CreatedOnUtc, DateTimeKind.Utc);

                    //fill in additional values (not existing in the entity)
                    vendorNoteModel.Note = _vendorService.FormatVendorNoteText(note);

                    return vendorNoteModel;
                });
            });

            return model;
        }

        /// <summary>
        /// Prepare paged vendor slider picture list model
        /// </summary>
        /// <param name="searchModel">Vendor slider picture search model</param>
        /// <param name="vendor">Vendor</param>
        /// <returns>Vendor slider picture list model</returns>
        public virtual VendorSliderPictureListModel PrepareVendorSliderPictureListModel(VendorSliderPictureSearchModel searchModel, Vendor vendor)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (vendor == null)
                throw new ArgumentNullException(nameof(vendor));

            //get vendor slider pictures
            var vendorSliderPictures = _vendorService.GetVendorSliderPicturesByVendorId(vendor.Id).ToPagedList(searchModel);

            //prepare grid model
            var model = new VendorSliderPictureListModel().PrepareToGrid(searchModel, vendorSliderPictures, () =>
            {
                return vendorSliderPictures.Select(p =>
                {
                    //fill in model values from the entity
                    //var productPictureModel = p.ToModel<VendorSliderPictureModel>();
                    var productPictureModel = new VendorSliderPictureModel()
                    {
                        Id = p.Id,
                        PictureId = p.PictureId,
                        VendorId = p.VendorId,
                        DisplayOrder = p.DisplayOrder
                    };

                    //fill in additional values (not existing in the entity)
                    var picture = _pictureService.GetPictureById(p.PictureId)
                                  ?? throw new Exception("Picture cannot be loaded");

                    productPictureModel.PictureUrl = _pictureService.GetPictureUrl(picture);
                    productPictureModel.OverrideAltAttribute = picture.AltAttribute;
                    productPictureModel.OverrideTitleAttribute = picture.TitleAttribute;

                    return productPictureModel;
                });
            });

            return model;
        }

        #endregion

    }
}