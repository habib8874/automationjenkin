﻿using NHKCustomerApplication.ViewModels;
using System.Collections.Generic;

namespace NHKCustomerApplication.Services
{
    public interface ICatalogServices
    {
        List<MerchantCategoryModel> GetMerchantCategory(MerchantCategorysModelV2 merchantCategorysModelV2, int languageId);
        List<CooknCategory> GetCategorys(Categorys categorys, int languageId);
    }
}
