﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.NB.Merchant;

namespace Nop.Services.NB.Merchant
{
    /// <summary>
    /// FavoriteMerchant service interface
    /// </summary>
    public interface IFavoriteMerchantServices
    {
        /// <summary>
        /// Gets all favorite merchants(vendor)
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Favorite merchants</returns>
        IPagedList<FavoriteMerchant> GetAllFavoriteMerchants(int customerId,
            int storeId = 0,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// Inserts a favorite merchant(vendor)
        /// </summary>
        /// <param name="FavoriteMerchant">favorite merchant(vendor)</param>
        void InsertFavoriteMerchant(FavoriteMerchant favoriteMerchant);

        /// <summary>
        /// Delete a favorite merchant(vendor)
        /// </summary>
        /// <param name="FavoriteMerchant">favorite merchant(vendor)</param>
        void DeleteFavoriteMerchant(FavoriteMerchant favoriteMerchant);

        /// <summary>
        /// Gets all favorite merchants(vendor)s by customer identifier
        /// </summary>
        /// <param name="customerId">customer identifier</param>
        /// <returns>Favorite merchants(vendors)</returns>
        IList<FavoriteMerchant> GetAllFavoriteMerchantssByCustomerId(int customerId);

        /// <summary>
        /// value idicates whether merchant already in favorite merchant table for this customer
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="customerId"></param>
        /// <returns>True/False</returns>
        bool IsMerchantInFavorite(int merchantId, int customerId);
    }
}
