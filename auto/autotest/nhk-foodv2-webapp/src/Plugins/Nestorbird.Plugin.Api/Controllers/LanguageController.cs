﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nestorbird.Plugin.Api.RequestModel.Language;
using Nestorbird.Plugin.Api.ResponseModel.Language;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Localization;

namespace Nestorbird.Plugin.Api.Controllers
{
    [Route("merchantapi/language")]
    public class MerchantLanguageController : BaseAPIController
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IStoreContext _storeContext;
        private readonly ICustomerService _customerService;

        #endregion

        #region Ctor

        public MerchantLanguageController(ILanguageService languageService,
            ILocalizationService localizationService,
            IGenericAttributeService genericAttributeService,
            IStoreContext storeContext,
            ICustomerService customerService) : base(languageService)
        {
            _localizationService = localizationService;
            _genericAttributeService = genericAttributeService;
            _storeContext = storeContext;
            _customerService = customerService;
        }

        #endregion

        #region Methods

        [HttpGet]
        [Route("getlanguages")]
        [ProducesResponseType(typeof(IList<LanguageResponse>), StatusCodes.Status200OK)]
        public IActionResult GetLanguages()
        {
            var langauges = _languageService.GetAllLanguages();
            var listOfLanguages = langauges.Select(x =>
            {
                return new LanguageResponse
                {
                    LanguageId = x.Id,
                    Name = x.Name
                };
            }).ToList();

            return Success(listOfLanguages);
        }

        [HttpPost]
        [Route("getlocalizedresources")]
        [ProducesResponseType(typeof(IList<LocalizedResourcesResponse>), StatusCodes.Status200OK)]
        public IActionResult GetLocalizedResources([FromBody] IList<LocalizedResourceRequest> request)
        {
            if (request == null)
                return Error("Please provide resource lists");

            var localizedResourcesResponses = new List<LocalizedResourcesResponse>();
            foreach (var item in request)
            {
                var value = _localizationService.GetResource(item.Key, CurrentLanguageId);
                localizedResourcesResponses.Add(new LocalizedResourcesResponse { Key = item.Key, Value = value });
            }

            return Success(localizedResourcesResponses);
        }

        [HttpPost]
        [Route("changelanguage")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public IActionResult ChangeLanguage(int customerId, int languageId)
        {
            if (customerId <= 0)
                return Error("Please provide customer id");

            if (languageId <= 0)
                return Error("Please provide language id");

            var customer = _customerService.GetCustomerById(customerId);
            _genericAttributeService.SaveAttribute(customer,
                            NopCustomerDefaults.LanguageIdAttribute, languageId, _storeContext.CurrentStore.Id);

            return Success("", "Language changed succcessfully");
        }
        

        #endregion
    }
}
