﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.NB.Merchant;
using Nop.Services.Catalog;

namespace Nop.Services.NB.Merchant
{
    /// <summary>
    /// Favorite merchant service
    /// </summary>
    public partial class FavoriteMerchantServices : IFavoriteMerchantServices
    {
        #region Fields

        private readonly IRepository<FavoriteMerchant> _favoriteMerchantRepository;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        public FavoriteMerchantServices(IRepository<FavoriteMerchant> favoriteMerchantRepository,
            ICacheManager cacheManager)
        {
            _favoriteMerchantRepository = favoriteMerchantRepository;
            _cacheManager = cacheManager;
        }

        #endregion

        /// <summary>
        /// Gets all favorite merchants(vendor)
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Favorite merchants</returns>
        public IPagedList<FavoriteMerchant> GetAllFavoriteMerchants(int customerId,
            int storeId = 0,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var key = string.Format(NopNBDefaults.FavoriteMerchantsCacheKey, customerId, storeId, pageIndex, pageSize);
            return _cacheManager.Get(key, () =>
            {
                var query = _favoriteMerchantRepository.Table;
                if (storeId > 0)
                    query = query.Where(q => q.Vendor.StoreId == storeId);
                if (customerId > 0)
                    query = query.Where(q => q.CustomerId == customerId);

                var favoriteMerchants = new PagedList<FavoriteMerchant>(query, pageIndex, pageSize);
                return favoriteMerchants;
            });
        }

        /// <summary>
        /// Inserts a favorite merchant(vendor)
        /// </summary>
        /// <param name="FavoriteMerchant">favorite merchant(vendor)</param>
        public void InsertFavoriteMerchant(FavoriteMerchant favoriteMerchant)
        {
            if (favoriteMerchant == null)
                throw new ArgumentNullException(nameof(favoriteMerchant));

            //insert
            _favoriteMerchantRepository.Insert(favoriteMerchant);

            //clear cache
            _cacheManager.RemoveByPrefix(NopNBDefaults.FavoriteMerchantsPrefixCacheKey);
        }

        /// <summary>
        /// Delete a favorite merchant(vendor)
        /// </summary>
        /// <param name="FavoriteMerchant">favorite merchant(vendor)</param>
        public void DeleteFavoriteMerchant(FavoriteMerchant favoriteMerchant)
        {
            if (favoriteMerchant == null)
                throw new ArgumentNullException(nameof(favoriteMerchant));

            //delete
            _favoriteMerchantRepository.Delete(favoriteMerchant);

            //clear cache
            _cacheManager.RemoveByPrefix(NopNBDefaults.FavoriteMerchantsPrefixCacheKey);
        }

        /// <summary>
        /// Gets all favorite merchants(vendor)s by customer identifier
        /// </summary>
        /// <param name="customerId">customer identifier</param>
        /// <returns>Favorite merchants(vendors)</returns>
        public IList<FavoriteMerchant> GetAllFavoriteMerchantssByCustomerId(int customerId)
        {
            if (customerId == 0)
                return null;

            var key = string.Format(NopNBDefaults.FavoriteMerchantsByCustomerIdCacheKey, customerId);
            return _cacheManager.Get(key, () =>
            {
                var query = from c in _favoriteMerchantRepository.Table
                            where c.CustomerId == customerId
                            orderby c.Id
                            select c;

                var records = query;
                return records.ToList();
            });
        }

        /// <summary>
        /// value idicates whether merchant already in favorite merchant table for this customer
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="customerId"></param>
        /// <returns>True/False</returns>
        public bool IsMerchantInFavorite(int merchantId, int customerId)
        {
            if (customerId == 0 || merchantId == 0)
                return false;

            return _favoriteMerchantRepository.Table.Where(f => f.CustomerId == customerId && f.VendorId == merchantId).Any();
        }
    }
}
