﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.CustomEntities;
using Nop.Core.Domain.NB;

namespace Nop.Services.Common
{
    /// <summary>
    /// ICommonService
    /// </summary>
    public partial interface ICommonService
    {
        IList<CLS_StoreCountryStateMapping> GetStoreCountryStateMapping(int storeId);
        IList<CLS_ISDCode> GetISDCode(int storeId, bool getAll = false);
        IList<int> GetVendorByLongLat(string latlong);
        IList<int> GetAllVendorByLongLat(string latlong, int restType);
        IList<int> GetAutoAssignAgent(int storeId, int queryType);
        int CheckGeoArea(string param1, string param2);
        CLS_AgentDetailsForNotification GetAgentDetailsForNotification(int agentId);

        IList<int> GetMerchantCategorysIdsByLongLat(int currStoreId, int orderTypestring, string latlong, bool skipLocationSearch);

        IList<DeliverySlot> GetDeliverySlots(DateTime deliveryDateTime, string productIds, int merchantId, int currStoreId);

        /// <summary>
        /// Get setting ids by lat long.
        /// </summary>
        /// <param name="latlong"></param>
        /// <param name="storeId"></param>
        /// <returns></returns>
        IList<int> GetAllSliderPictureSettingIdsByLongLat(string latlong, int storeId);
        ResultModel ClearCache(int storeId ,string cacheKeyPrefix);
        AgentResultModel ClearCacheAgent(int storeId, string cacheKeyPrefix);

        #region Common API services

        /// <summary>
        /// Insert store host url in setting table, So it can be accessible from Customer/Agent App 
        /// and we can make API call to which we have developed in CommonAPI controller
        /// </summary>
        void AddUpdateStoreBaseUrlInSetting();

        #endregion
    }
}