﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB.Catalog.QRCode;

namespace Nop.Data.Mapping.NB.Catalog.QRCodeMap
{
    /// <summary>
    /// Represents a QR code configuration
    /// </summary>
    public partial class QRCodeMap : NopEntityTypeConfiguration<QRCode>
    {
        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<QRCode> builder)
        {
            builder.ToTable(NopMappingDefaults.QRCodeTable);
            builder.HasKey(pc => pc.Id);

            builder.HasOne(qrCode => qrCode.Vendor)
                .WithMany()
                .HasForeignKey(qrCode => qrCode.MerchantId);

            base.Configure(builder);
        }
    }
}