﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Reports
{
    /// <summary>
    /// Represents an AgentEarning list model
    /// </summary>
    public partial class AgentEarningListModel : BasePagedListModel<AgentEarningModel>
    {
    }
}