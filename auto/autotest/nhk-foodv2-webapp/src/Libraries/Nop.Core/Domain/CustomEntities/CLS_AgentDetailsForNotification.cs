﻿namespace Nop.Core.Domain.CustomEntities
{
    public partial class CLS_AgentDetailsForNotification
    {
        public string KW1 { get; set; }
        public string KW2 { get; set; }
        public string AW2 { get; set; }
        public string AW3 { get; set; }
        public string KA3 { get; set; }
        public string KA1 { get; set; }
        public string DeviceToken { get; set; }
        public string MobileNo { get; set; }
        public string StoreName { get; set; }
    }
}
