﻿using System.Collections.Generic;
using Nestorbird.Plugin.Api.RequestModel.Orders;
using Nestorbird.Plugin.Api.ResponseModel.Order;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;

namespace Nestorbird.Plugin.Api.Factories
{
    public interface IOrderModelFactory
    {
        /// <summary>
        /// Get orders
        /// </summary>
        /// <param name="request">request</param>
        /// <returns></returns>
        OrderListResponse GetOrders(OrderListRequest request, Language currentLanguage);

        /// <summary>
        /// Get order notes
        /// </summary>
        /// <param name="order">order</param>
        /// <returns>order notes</returns>
        IList<OrderNoteResponse> GetOrderNotes(Order order);

        /// <summary>
        /// Get order details
        /// </summary>
        /// <param name="order">order</param>
        /// <param name="currentLanguage">current language</param>
        /// <returns></returns>
        OrderDetailResponse GetOrderDetails(Order order, Language currentLanguage);

        /// <summary>
        /// Get notifications
        /// </summary>
        /// <param name="vendorId">vendor id</param>
        /// <returns></returns>
        IList<NotificationResponse> GetNotifications(int vendorId);
    }
}