﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Models.RatingReview
{
    public partial class NBReviewOptionModel : BaseNopEntityModel
    {
        public string Name { get; set; }
        public int EntityType { get; set; }
        public bool Active { get; set; }
    }
}
