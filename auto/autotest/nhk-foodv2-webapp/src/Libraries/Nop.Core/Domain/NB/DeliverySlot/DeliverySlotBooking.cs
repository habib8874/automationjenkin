﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Localization;

namespace Nop.Core.Domain.NB
{
    /// <summary>
    /// Represents a DeliverySlotBooking
    /// </summary>
    public partial class DeliverySlotBooking : BaseEntity
    {

        public int SlotId { get; set; }
        public int OrderId { get; set; }
        public int StoreId { get; set; }
        public int CustomerId { get; set; }

        public DateTime DeliveryDateUtc { get; set; }

        public DateTime DeliveryStartTime { get; set; }

        public DateTime DeliveryEndTime { get; set; }
        public bool Deleted { get; set; }

        public DateTime CreatedOnUtc { get; set; }
    }
}
