﻿using NHKCustomerApplication.Models;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NHKCustomerApplication.ViewModels
{
	public class CustomerLoginV2_1 : CustomerLogin
	{
		public string TimeZoneId { get; set; }
	}

	public class UploadPhotoModel
	{
		public bool Success { get; set; }
		public string FileName { get; set; }
	}
	public class CustomerModel
	{
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string MobileNo { get; set; }
		public string Pswd { get; set; }
		public string ProfilePic { get; set; }
		//public string Address { get; set; }
		//public int City { get; set; }
		//public int Country { get; set; }
		//public int State { get; set; }
		//public int Pin { get; set; }
		//public int Gender { get; set; }
		public string Imei1 { get; set; }
		public string Imei2 { get; set; }
		public string DeviceNo { get; set; }
		public string LatPos { get; set; }
		public string LongPos { get; set; }
		public string ApiKey { get; set; }
		public int StoreId { get; set; }
		public string DeviceToken { get; set; }
		public string DeviceType { get; set; }
		public string UniqueSeoCode { get; set; }
	}

	public class GuestUserModel
	{
		public string DeviceNo { get; set; }
		public string ApiKey { get; set; }
		public int StoreId { get; set; }
		public string DeviceToken { get; set; }
		public string DeviceType { get; set; }
		public string UniqueSeoCode { get; set; }
	}
	public class ExternalAuthModel
	{
		public string AccountId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string MobileNo { get; set; }
		public string DeviceToken { get; set; }
		public string LoginType { get; set; }
		public string Imei1 { get; set; }
		public string Imei2 { get; set; }
		public string DeviceNo { get; set; }
		public string LatPos { get; set; }
		public string LongPos { get; set; }
		public string UniqueSeoCode { get; set; }
		public string ApiKey { get; set; }
		public int StoreId { get; set; }
		public bool IsGuestId { get; set; }
		public int GuestUserId { get; set; }
		public string DeviceType { get; set; }
	}
	public class CustomerDetails
	{
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public int MobileStd { get; set; }
		public string MobileNo { get; set; }
		public string ProfilePic { get; set; }
		public int StoreId { get; set; }
	}
	public class RegistationverificationCustomer
	{
		public int CustomerId { get; set; }
		public string OTP { get; set; }
		public string ApiKey { get; set; }
		public int StoreId { get; set; }
		public string UniqueSeoCode { get; set; }
		public bool isGuestId { get; set; }
		public int GuestUserId { get; set; }
	}

	public class CustomerLoginV3
	{
		public string Email { get; set; }
		public string Phone { get; set; }
		public string ApiKey { get; set; }
		public int StoreId { get; set; }
		public string UniqueSeoCode { get; set; }
		public string DeviceToken { get; set; }
		public string DeviceType { get; set; }
	
	}

	public class ResendOTPModel
	{
		public int CustomerId { get; set; }
		public string ApiKey { get; set; }
		public string Imei1 { get; set; }
		public string Imei2 { get; set; }
		public string DeviceNo { get; set; }
		public string LatPos { get; set; }
		public string LongPos { get; set; }
		public int StoreId { get; set; }
		public string DeviceToken { get; set; }
		public string DeviceType { get; set; }
		public string UniqueSeoCode { get; set; }

	}
	public class CustomerSignUpResponseModel
	{
		public int UserID { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string MobileNo { get; set; }
		public string Gender { get; set; }
		public string UserStatus { get; set; }
		public bool IsFCMToken { get; set; }
		public string DeviceToken { get; set; }
		public string Email { get; set; }
		public string OTP { get; set; }
	}

	public class CustomerLoginResponseModelV3
	{
		public int UserID { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string MobileNo { get; set; }
		public string Gender { get; set; }
		public bool UserStatus { get; set; }
		public bool IsFCMToken { get; set; }
		public string DeviceToken { get; set; }
		public string Email { get; set; }
		public string OTP { get; set; }

	}
	public class CustomerDetailsModel
	{
		public int CustomerId { get; set; }
		public string ApiKey { get; set; }
		public int StoreId { get; set; }
		public string UniqueSeoCode { get; set; }
	}
	public class CustomerLogin
	{
		public string Email { get; set; }
		public string Phone { get; set; }
		public string Pswd { get; set; }
		public string Imei1 { get; set; }
		public string Imei2 { get; set; }
		public string DeviceNo { get; set; }
		public string LatPos { get; set; }
		public string LongPos { get; set; }
		public string ApiKey { get; set; }
		public string DeviceToken { get; set; }
		public int StoreId { get; set; }
		public string DeviceType { get; set; }
		public string UniqueSeoCode { get; set; }
		public int GuestUserId { get; set; }
	}
	public class CustomerForgetPassword
	{
		public string Email { get; set; }
		public string Phone { get; set; }
		public string ApiKey { get; set; }
		public string Imei1 { get; set; }
		public string Imei2 { get; set; }
		public string DeviceNo { get; set; }
		public string LatPos { get; set; }
		public string LongPos { get; set; }
		public string DeviceToken { get; set; }
		public int StoreId { get; set; }
		public string DeviceType { get; set; }
		public string UniqueSeoCode { get; set; }

	}

	public class CustomerResetPassword
	{
		public string ApiKey { get; set; }
		[DefaultValue(false)]
		public bool IsForgetPassword { get; set; }
		public string OldPassword { get; set; }
		public string NewPassword { get; set; }
		public int CustomerId { get; set; }
		public int StoreId { get; set; }
		public string UniqueSeoCode { get; set; }
	}

	public class CustomerCreateNewPassword
	{
		public string ApiKey { get; set; }
		public string OTP { get; set; }
		public string NewPassword { get; set; }
		public int CustomerId { get; set; }
		public int StoreId { get; set; }
		public string UniqueSeoCode { get; set; }
	}

	public class ApplyCouponModel
	{
		public int CustomerId { get; set; }
		public string ApiKey { get; set; }
		public int StoreId { get; set; }
		public string CouponCode { get; set; }
		public int OrderId { get; set; }
		public decimal OrderTotal { get; set; }
	}
	public class CustomerAddressDetailsModel
	{
		public int CustId { get; set; }
		public string ApiKey { get; set; }
		public int AddressId { get; set; }
		public string UniqueSeoCode { get; set; }
		public int StoreId { get; set; }
	}
	public class CheckoutPaymentModel
	{
		public int CustomerId { get; set; }
		public string ApiKey { get; set; }
		public string PaymentToken { get; set; }
		public long Total { get; set; }
		public int OrderId { get; set; }
		public bool IsTip { get; set; }
		public long TipAmount { get; set; }
		public string Comments { get; set; }
		public string CouponCode { get; set; }
	}
	public class CheckoutPaymentModelV2
	{
		public int CustomerId { get; set; }
		public string ApiKey { get; set; }
		public int OrderId { get; set; }
		public string PaymentId { get; set; }
		public string PaymentMessage { get; set; }
		public bool IsSuccess { get; set; }
		public int PaymentMethodId { get; set; }
		public string PaymentToken { get; set; }
		public bool IsNew { get; set; }
		public string CardId { get; set; }
		public string UniqueSeoCode { get; set; }
		public int StoreId { get; set; }


	}

	public class CheckoutPaymentModelV3 : CheckoutPaymentModelV2
	{
		public bool IsWalletPay { get; set; }
		public decimal WalletAmount { get; set; }


	}
	public class WalletRequestModel : BaseAPIRequestModel
	{
		public int CustomerId { get; set; }

		public int? OrderId { get; set; }
		public decimal Amount { get; set; }
		public string Note { get; set; }
		public bool Credited { get; set; }
		public string TransactionOnUtc { get; set; }

        public int PaymentMethodId { get; set; }

       


	}
    public class WalletMiniModel:BaseAPIRequestModel
    {
        public int CustomerId { get; set; }
    }
    public class ProcessPaymentRequest
    {
		/// <summary>
		/// Gets or sets a store identifier
		/// </summary>
		public int StoreId { get; set; }

		/// <summary>
		/// Gets or sets a customer identifier
		/// </summary>
		public int CustomerId { get; set; }

		/// <summary>
		/// Gets or sets an order unique identifier. Used when order is not saved yet (payment gateways that do not redirect a customer to a third-party URL)
		/// </summary>
		public Guid OrderGuid { get; set; }
		/// <summary>
		/// Gets or sets a datetime when "OrderGuid" property was generated (used for security purposes)
		/// </summary>
		public DateTime? OrderGuidGeneratedOnUtc { get; set; }

		/// <summary>
		/// Gets or sets an order total
		/// </summary>
		public decimal OrderTotal { get; set; }

		/// <summary>
		/// /// <summary>
		/// Gets or sets a payment method identifier
		/// </summary>
		/// </summary>
		public string PaymentMethodSystemName { get; set; }

		#region Payment method specific properties 

		/// <summary>
		/// Gets or sets a credit card type (Visa, Master Card, etc...). We leave it empty if not used by a payment gateway
		/// </summary>
		public string CreditCardType { get; set; }

		/// <summary>
		/// Gets or sets a credit card owner name
		/// </summary>
		public string CreditCardName { get; set; }

		/// <summary>
		/// Gets or sets a credit card number
		/// </summary>
		public string CreditCardNumber { get; set; }

		/// <summary>
		/// Gets or sets a credit card expire year
		/// </summary>
		public int CreditCardExpireYear { get; set; }

		/// <summary>
		/// Gets or sets a credit card expire month
		/// </summary>
		public int CreditCardExpireMonth { get; set; }

		/// <summary>
		/// Gets or sets a credit card CVV2 (Card Verification Value)
		/// </summary>
		public string CreditCardCvv2 { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether to store credit card or not
		/// </summary>
        public bool StoreCreditCard { get; set; }

        #endregion
    }
    public class UpdateProduct
	{
		public int OrderId { get; set; }
		public int StoreId { get; set; }
		public string ApiKey { get; set; }
		public string UniqueSeoCode { get; set; }
	}
	public class PaymentMethodModel
	{
		public int PaymentMethodId { get; set; }
		public string PaymentMethodName { get; set; }
		public bool isShow { get; set; }
	}
	public class CustomerAddressModel
	{
		public int Id { get; set; }
		public int LabelId { get; set; }
		public string Label { get; set; }
		public string City { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string ZipPostalCode { get; set; }
		public string APIKey { get; set; }
		public int CustId { get; set; }

	}
	public class CustomerAddressModelV2
	{
		public int Id { get; set; }
		public int LabelId { get; set; }
		public string Label { get; set; }
		public string? City { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string ZipPostalCode { get; set; }
		public string ApiKey { get; set; }
		public int CustId { get; set; }
		public string LatPos { get; set; }
		public string LongPos { get; set; }
		public string UniqueSeoCode { get; set; }
		public int StoreId { get; set; }
		public string? Landmarktext { get; set; }
		public string? FlatNo { get; set; }
		public bool SetAsDefault { get; set; }

	}

	public class CustomerAddressViewModel
	{
		public int Id { get; set; }
		public string Label { get; set; }
		public int LabelId { get; set; }
		public string City { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string PhoneNumber { get; set; }
		public string AddressLine1 { get; set; }
		public string AddressLine2 { get; set; }
		public string ZipCode { get; set; }
		public decimal LatPos { get; set; }
		public decimal LongPos { get; set; }
		public string Landmarktext { get; set; }
		public string FlatNo { get; set; }
		public bool SetAsDefault { get; set; }

	}

	public class PasswordModel
	{
		public int CustomerId { get; set; }
		public bool Active { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public string PasswordSalt { get; set; }
	}

	public class CustomerResponseModel
	{
		public bool ValidData { get; set; }
		public string ResultData { get; set; }
	}
	public class CustomerLoginResponseModel : CustomerResponseModel
	{
		public int UserID { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string MobileNo { get; set; }
		public string UserStatus { get; set; }
		public string Email { get; set; }
	}
	public class CustomerRestProductResponseModel : CustomerResponseModel
	{
		public List<RestProductModel> Products { get; set; }
	}

	public class CustomerRestProductResponseModels : CustomerResponseModel
	{
		public List<RestProductResponseModel> Products { get; set; }
	}

	public class CustomerRestProductFilterResponseModel : CustomerResponseModel
	{
		public List<RestProductFilterModel> Products { get; set; }

	}

	public class RestProductFilterModel
	{
		public int Id { get; set; }
		public bool Available { get; set; }
		public string Cuisine { get; set; }
		public int ProductTypeId { get; set; }
		public string CustomeURL { get; set; }
		public string Name { get; set; }
		public string ShortDescription { get; set; }
		public string FullDescription { get; set; }
		public bool DisableBuyButton { get; set; }
		public bool DisableWishlistButton { get; set; }
		public decimal? Price { get; set; }
		public decimal? OldPrice { get; set; }
		public decimal? ProductCost { get; set; }
		public DateTime CreatedOnUtc { get; set; }
		public bool Deleted { get; set; }
		public bool Published { get; set; }
		public int? Quantity { get; set; }
		public bool CallForPrice { get; set; }
		public bool IsWishList { get; set; }
		public int? WishListId { get; set; }
		public string ChefBaseUrl { get; set; }
		public int? PictureSize { get; set; }
		public string ProductPics { get; set; }
		public string Rating { get; set; }

	}
	public class RestProductModelV2
	{
		public int CategoryId { get; set; }
		public string CategoryName { get; set; }
	}
	public class RestProductModel
	{
		public int ItemId { get; set; }
		public string itemName { get; set; }
		public string ShortDescription { get; set; }
		public int CategoryId { get; set; }
		public int ParentCategoryId { get; set; }
		public string CategoryName { get; set; }
		public string CategoryDescription { get; set; }
		public string CategoryPictureUrl { get; set; }
		public bool IsMagicMenu { get; set; }
		public string ParentCategoryName { get; set; }
		public bool Available { get; set; }
		public int? Quantity { get; set; }
		public bool IsWishList { get; set; }
		public int? WishListId { get; set; }
		public bool IsProductAttributesExist { get; set; }
		public decimal? ProductPrice { get; set; }
		public string ProductPreferences { get; set; }
		public string ProductPics { get; set; }
		public string CustomeURL { get; set; }
		public string Rating { get; set; }
		public string Cuisine { get; set; }
		public string ChefBaseUrl { get; set; }
		public int? PictureSize { get; set; }
		public string CurrencyCode { get; set; }
		public decimal? OldPrice { get; set; }
	}

	public class LoginOtpTimerSecond
	{
		public int CustomerID { get; set; }
		public string Second { get; set; }
	}

	public class ResendOtpTimerSecond
	{
		public int CustomerID { get; set; }
		public string Minutes { get; set; }
	}

	#region Favorite merchants

	public class AddRemoveFavoriteMerchant : BaseAPIRequestModel
	{
		public int CustomerId { get; set; }
		public int MerchantId { get; set; }
	}

	public class FavoriteMerchantResponce
	{
		public bool IsRemoved { get; set; }
		public string Message { get; set; }
		public bool Success { get; set; }
	}

	public class CustomerFavoriteMerchant : BaseAPIRequestModel
	{
		public int CustomerId { get; set; }
		public int PageIndex { get; set; }
		public int PageSize { get; set; }
	}

	public class CustomerFavoriteMerchantResponce : CustomerResponseModel
	{
		public CustomerFavoriteMerchantResponce()
		{
			CooknRests = new List<CooknRestV2>();
		}
		public int TotalRecords { get; set; }
		public List<CooknRestV2> CooknRests { get; set; }
	}

	#endregion
}