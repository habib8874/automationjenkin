﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Core;
using System.Collections.Generic;
using Nop.Web.Models.Catalog;

namespace Nop.Web.Components
{
    public class HomepageProductsMerchantByCategoryViewComponent : NopViewComponent
    {
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly IAclService _aclService;
        private readonly IProductModelFactory _productModelFactory;
        private readonly IProductService _productService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IWorkContext _workContext;

        public HomepageProductsMerchantByCategoryViewComponent(ICatalogModelFactory catalogModelFactory, IAclService aclService,
            IProductModelFactory productModelFactory,
            IProductService productService,
            IStoreMappingService storeMappingService, IWorkContext workContext)
        {
            this._catalogModelFactory = catalogModelFactory;
            this._aclService = aclService;
            this._productModelFactory = productModelFactory;
            this._productService = productService;
            this._storeMappingService = storeMappingService;
            this._workContext = workContext;
        }

        public IViewComponentResult Invoke(IList<ProductOverviewModelMerchant> products)
        {
            string merchantid = _workContext.CurrentMerchant.Id.ToString(); 
            var categories = _catalogModelFactory.PrepareHomepageCategoryMerchantModels(merchantid);

            var model = new CategoryProductsMerchantModel();
            model.Products = products;
            model.Categories = categories;

            return View(model);
        }
    }
}
