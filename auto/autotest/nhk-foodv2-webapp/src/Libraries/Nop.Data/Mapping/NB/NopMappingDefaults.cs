﻿
namespace Nop.Data.Mapping
{
    /// <summary>
    /// Represents default values related to data mapping
    /// </summary>
    public static partial class NopMappingDefaults
    {
        public static string Tip => "NB_TipMaster"; 
        public static string MerchantCategoryTable => "NB_MerchantCategory"; 
        public static string MerchantCategoryMappingTable => "NB_MerchantCategory_Merchant_Mapping";
        public static string VendorCustomerRoleMappingTable => "NB_Vendor_CustomerRole_Mapping";
        public static string NBVendorToken => "NB_VendorToken";
        public static string DeliverySlotTable => "NB_DeliverySlot";
        public static string DeliverySlotBookingTable => "NB_DeliverySlotBooking";
        public static string ReviewOptionTable => "NB_ReviewOption";
        public static string ProductDeliverySlotMappingTable => "NB_Product_NB_DeliverySlot_Mapping";
        public static string FavoriteMerchantTable => "NB_FavoriteMerchant";
        public static string AgentOrderGeoLocationTable => "AgentOrderGeoLocation";

        // Package  Task 

        /// <summary>
        /// Gets a name of the Package table
        /// </summary>
        public static string PackageTable => "NB_Package";

        /// <summary>
        /// Gets a name of the Package Plan table
        /// </summary>
        public static string PackagePlanTable => "NB_PackagePlan";

        public static string CustomerPackageMappingTable => "NB_Customer_NBPackage_Mapping";

        public static string QRCodeTable => "NB_QRCode";

    }
}