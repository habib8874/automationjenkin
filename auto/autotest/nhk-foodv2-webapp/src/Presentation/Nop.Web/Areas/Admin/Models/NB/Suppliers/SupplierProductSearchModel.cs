﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB.Suppliers
{
    public partial class SupplierProductSearchModel:BaseSearchModel
    {
        public SupplierProductSearchModel()
        {
          
        }
        #region Properties
        [NopResourceDisplayName("Admin.Supplier.Products.List.SupplierId")]
        public int SupplierId { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Products.List.ProductName")]
        public string ProductName { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Products.List.Price")]
        public decimal Price { get; set; }

        [NopResourceDisplayName("Admin.Supplier.Products.List.ProductId")]

        public int ProductId { get; set; }



        #endregion
    }
}
