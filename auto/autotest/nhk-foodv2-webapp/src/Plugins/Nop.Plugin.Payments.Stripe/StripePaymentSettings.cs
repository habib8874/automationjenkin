using Nop.Core.Configuration;
using Nop.Plugin.Payments.Stripe;

namespace Nop.Plugin.Payments.Stripe
{
    /// <summary>
    /// Represents settings of Stripe payment plugin
    /// </summary>
    public class StripePaymentSettings : ISettings
    {
        /// <summary>
        /// Gets or sets a value indicating whether to use sandbox (testing environment)
        /// </summary>
        public bool UseSandbox { get; set; }

        /// <summary>
        /// Gets or sets payment transaction mode
        /// </summary>
        public TransactMode TransactMode { get; set; }

        /// <summary>
        /// Gets or sets PublishableKey
        /// </summary>
        public string PublishableKey { get; set; }

        /// <summary>
        /// Gets or sets SecretKey
        /// </summary>
        public string SecretKey{ get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to "additional fee" is specified as percentage. true - percentage, false - fixed value.
        /// </summary>
        public bool AdditionalFeePercentage { get; set; }

        /// <summary>
        /// Gets or sets an additional fee
        /// </summary>
        public decimal AdditionalFee { get; set; }
    }
}
