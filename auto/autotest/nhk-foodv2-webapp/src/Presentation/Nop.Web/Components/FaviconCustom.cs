﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain;
using Nop.Core.Domain.Common;
using Nop.Services.Media;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;

namespace Nop.Web.Components
{
    public class FaviconCustomViewComponent : NopViewComponent
    {
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly IPictureService _pictureService;

        public FaviconCustomViewComponent(StoreInformationSettings storeInformationSettings,
            IPictureService pictureService)
        {
            _storeInformationSettings = storeInformationSettings;
            _pictureService = pictureService;
        }

        public IViewComponentResult Invoke()
        {
            string picturePath = string.Empty;
            var faviconPictureId = _storeInformationSettings.FaviconPictureId;
            if (faviconPictureId==0)
                return Content("");

            var picture = _pictureService.GetPictureById(faviconPictureId);
            if (picture != null)
                picturePath = _pictureService.GetPictureUrl(picture);
            ViewBag.Favicon = picturePath;
            return View();
        }
    }
}
