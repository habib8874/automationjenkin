﻿using Nop.Core.Domain.Vendors;

namespace Nop.Core.Domain.NB.Discount
{
    /// <summary>
    /// Represents a discount-vendor mapping class
    /// </summary>
    public partial class DiscountVendorMapping : BaseEntity
    {
        /// <summary>
        /// Gets or sets the discount identifier
        /// </summary>
        public int DiscountId { get; set; }

        /// <summary>
        /// Gets or sets the product identifier
        /// </summary>
        public int VendorId { get; set; }

        /// <summary>
        /// Gets or sets the discount
        /// </summary>
        public virtual Discounts.Discount Discount { get; set; }

        /// <summary>
        /// Gets or sets the vendor
        /// </summary>
        public virtual Vendor Vendor { get; set; }
    }
}
