﻿using FluentValidation;
using Nop.Services.Localization;
using Nop.Web.Areas.Admin.Models.BookingTable;
using Nop.Web.Framework.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Validators.BookingTable
{
    public partial class ManageBookingTransactionValidator : BaseNopValidator<BookingTableTransactionModel>
    {
        public ManageBookingTransactionValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.DateOfBirthDay).NotEmpty().WithMessage(localizationService.GetResource("Forum.TextCannotBeEmpty"));
            RuleFor(x => x.From).NotEmpty().WithMessage(localizationService.GetResource("Forum.TextCannotBeEmpty"));
            RuleFor(x => x.To).NotEmpty().WithMessage(localizationService.GetResource("Forum.TextCannotBeEmpty"));
            RuleFor(x => x.TableNumber).NotEmpty().WithMessage(localizationService.GetResource("Forum.TextCannotBeEmpty"));
            RuleFor(x => x.MobileNumber).NotEmpty().WithMessage(localizationService.GetResource("Forum.TextCannotBeEmpty"));
            RuleFor(x => x.CustomerName).NotEmpty().WithMessage(localizationService.GetResource("Booking.CustomerName.TextCannotBeEmpty"));
            RuleFor(x => x.Email)
                 .NotEmpty()
                 .WithMessage(localizationService.GetResource("Address.Fields.Email.Required"));
            RuleFor(x => x.Email)
                .EmailAddress()
                .WithMessage(localizationService.GetResource("Common.WrongEmail"));

        }
    }
}
