﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Plugin.Payments.WalletPay.Models;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.NB;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Plugins;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.Payments.WalletPay
{
    /// <summary>
    /// WalletPay payment processor
    /// </summary>
    public class WalletPayPaymentProcessor : BasePlugin, IPaymentMethod
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IPaymentService _paymentService;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly WalletPayPaymentSettings _walletPayPaymentSettings;
        private readonly IWorkContext _workContext;
        private readonly IWalletDetailsService _walletService;
        private readonly ICustomerService _customerService;

        #endregion

        #region Ctor

        public WalletPayPaymentProcessor(ILocalizationService localizationService,
            IPaymentService paymentService,
            ISettingService settingService,
            IWebHelper webHelper,
            IWorkContext workContext,
            IWalletDetailsService walletService,
            ICustomerService customerService,
            WalletPayPaymentSettings walletPayPaymentSettings)
        {
            _customerService = customerService;
            _walletService = walletService;
            _workContext = workContext;
            _localizationService = localizationService;
            _paymentService = paymentService;
            _settingService = settingService;
            _webHelper = webHelper;
            _walletPayPaymentSettings = walletPayPaymentSettings;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);
            if (customer == null)
                throw new Exception("Customer cannot be loaded");
            var result = new ProcessPaymentResult();
            //get price
            var totalAmt = processPaymentRequest.OrderTotal;
            var walletBalance = _walletService.GetWalletBalance(processPaymentRequest.CustomerId);
            if (totalAmt > walletBalance)
            {
                result.AddError(_localizationService.GetResource("Plugins.Payment.WalletPay.LowBalance"));
            }
            else
            {
                //WalletDetails wallet = new WalletDetails
                //{
                //    Amount = processPaymentRequest.OrderTotal,
                //    Credited = false,
                //    CustomerId = processPaymentRequest.CustomerId,
                //    FromWeb = true,
                //    OrderId = processPaymentRequest.InitialOrderId,
                //    TransactionBy = processPaymentRequest.CustomerId,
                //    TransactionOnUtc = DateTime.UtcNow
                //};
                //_walletService.InsertWallet(wallet);
                result.NewPaymentStatus = PaymentStatus.Paid;
            }
            return result;
        }

        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            //nothing
        }

        /// <summary>
        /// Returns a value indicating whether payment method should be hidden during checkout
        /// </summary>
        /// <param name="cart">Shoping cart</param>
        /// <returns>true - hide; false - display.</returns>
        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            //you can put any logic here
            //for example, hide this payment method if all products in the cart are downloadable
            //or hide this payment method if current customer is from certain country
            var enablewallet = _settingService.GetSetting("storesettings.store.walletenabled", _workContext.GetCurrentStoreId)?.Value != "False";
            var url = _webHelper.GetThisPageUrl(false);
            return !enablewallet || cart.Count == 0 || url.IndexOf("onepagewalletcheckout") > -1;
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <param name="cart">Shoping cart</param>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            var result = _paymentService.CalculateAdditionalFee(cart,
                _walletPayPaymentSettings.AdditionalFee, _walletPayPaymentSettings.AdditionalFeePercentage);
            return result;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();
            result.AddError("Capture method not supported");
            return result;
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();
            result.AddError("Refund method not supported");
            return result;
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();
            result.AddError("Void method not supported");
            return result;
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.AddError("Recurring payment not supported");
            return result;
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            var result = new CancelRecurringPaymentResult();
            result.AddError("Recurring payment not supported");
            return result;
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            //it's not a redirection payment method. So we always return false
            return false;
        }

        public IList<string> ValidatePaymentForm(IFormCollection form)
        {
            var warnings = new List<string>();

            //validate
            var userBalance = _walletService.GetWalletBalance(_workContext.CurrentCustomer.Id);
            var validator = new PaymentInfoValidator(_localizationService, userBalance);
            var model = new PaymentInfoModel
            {
                OrderTotal = Convert.ToDecimal(form["OrderTotal"])
            };
            var validationResult = validator.Validate(model);
            if (!validationResult.IsValid)
                warnings.AddRange(validationResult.Errors.Select(error => error.ErrorMessage));

            return warnings;
            //return new List<string>();
        }

        public ProcessPaymentRequest GetPaymentInfo(IFormCollection form)
        {
            return new ProcessPaymentRequest
            {
               // OrderTotal = Convert.ToDecimal(form["OrderTotal"]),
            };
        }

        public string GetPublicViewComponentName()
        {
            return WalletPayDefaults.PAYMENT_INFO_VIEW_COMPONENT_NAME;
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/PaymentWalletPay/Configure";
        }

        /// <summary>
        /// Install the plugin
        /// </summary>
        public override void Install()
        {
            //settings
            _settingService.SaveSetting(new WalletPayPaymentSettings()
            {
                DescriptionText = "<P>Pay using wallet balance, You can edit this text from admin panel.</p>"
            });

            //locales
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.WalletPay.LowBalance", "Low Balance");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.WalletPay.DescriptionText", "Description");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.WalletPay.AdditionalFee", "Additional fee");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.WalletPay.AdditionalFee.Hint", "The additional fee.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.WalletPay.AdditionalFeePercentage", "Additional fee.Use percentage");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.WalletPay.AdditionalFeePercentage.Hint", "Determines whether to apply a percentage additional fee to the order total. If not enabled, a fixed value is used.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.WalletPay.DescriptionText.Hint", "Enter info that will be shown to customers during checkout");
            base.Install();
        }

        /// <summary>
        /// Uninstall the plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<WalletPayPaymentSettings>();

            //locales
            //_localizationService.DeletePluginLocaleResources("Plugins.Payment.WalletPay");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.WalletPay.LowBalance");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.WalletPay.DescriptionText");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.WalletPay.AdditionalFee");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.WalletPay.AdditionalFee.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.WalletPay.AdditionalFeePercentage");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.WalletPay.AdditionalFeePercentage.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.WalletPay.DescriptionText.Hint");

            base.Uninstall();
        }

        #endregion

        #region Properies

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture => false;

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund => false;

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund => false;

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid => false;

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType => RecurringPaymentType.NotSupported;

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType => PaymentMethodType.Standard;

        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo => false;

        /// <summary>
        /// Gets a payment method description that will be displayed on checkout pages in the public store
        /// </summary>
        public string PaymentMethodDescription => _localizationService.GetResource("Plugins.Payment.WalletPay.PaymentMethodDescription");

        #endregion

    }
    public partial class PaymentInfoValidator : BaseNopValidator<PaymentInfoModel>
    {
        public PaymentInfoValidator(ILocalizationService localizationService, decimal userBalance)
        {
            //useful links:
            //http://fluentvalidation.codeplex.com/wikipage?title=Custom&referringTitle=Documentation&ANCHOR#CustomValidator
            //http://benjii.me/2010/11/credit-card-validator-attribute-for-asp-net-mvc-3/

            RuleFor(x => x.OrderTotal).Must((x, context) =>
            {
                if (x.OrderTotal > userBalance)
                    return false;
                return true;
            }).WithMessage(localizationService.GetResource("Plugins.Payment.WalletPay.LowBalance"));
        }
    }
}
