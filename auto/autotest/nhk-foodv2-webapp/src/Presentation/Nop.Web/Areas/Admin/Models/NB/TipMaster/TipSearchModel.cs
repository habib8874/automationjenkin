﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB
{
    /// <summary>
    /// Represents a question search model
    /// </summary>
    public partial class TipSearchModel : BaseSearchModel
    {
        #region Properties
        public int Id { get; set; }

        public int StoreId { get; set; }
        #endregion
    }
}