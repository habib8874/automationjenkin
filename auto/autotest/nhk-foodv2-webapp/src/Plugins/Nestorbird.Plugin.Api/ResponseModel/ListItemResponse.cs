﻿namespace Nestorbird.Plugin.Api.ResponseModel
{
    public class ListItemResponse
    {
        public string Text { get; set; }

        public string Value { get; set; }
    }
}
