﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nestorbird.Plugin.Api.RequestModel.Products
{
    public class AddSpecificationAttributeRequest
    {
        public AddSpecificationAttributeRequest()
        {
            ShowOnProductPage = true;
        }

        public int ProductId { get; set; }

        public int SpecificationAttributeId { get; set; }

        public int AttributeTypeId { get; set; }

        public int OptionId { get; set; }

        public string CustomValue { get; set; }

        public bool ShowOnProductPage { get; set; }
    }
}
