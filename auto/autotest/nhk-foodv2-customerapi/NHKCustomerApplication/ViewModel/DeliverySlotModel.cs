﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.ViewModels
{
    /// <summary>
    /// Represents a DeliveryDays model
    /// </summary>
    public partial class DeliveryDaysModel
    {
        public DeliveryDaysModel()
        {

        }

        public string WeekDay { get; set; }
        public string WeekDateValue { get; set; }
        public DateTime WeekDate { get; set; }
    }

    /// <summary>
    /// Represents a DeliverySlot model
    /// </summary>
    public partial class DeliverySlotModel
    {
        public DeliverySlotModel()
        {

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDefaultSlot { get; set; }

        public int Duration { get; set; }
        public DateTime StartDateUtc { get; set; }
        public DateTime EndDateUtc { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int NumberOfOrders { get; set; }
        public decimal SurChargeFee { get; set; }
        public int DisplayOrder { get; set; }
        public int StoreId { get; set; }
        public int MerchantId { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the entity is published
        /// </summary>
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }

        public string StartDateString { get; set; }
    }

    /// <summary>
    /// Represents a Delivery model
    /// </summary>
    public partial class DeliveryModel
    {
        public DeliveryModel()
        {
            DeliverySlotList = new List<DeliverySlotModel>();
            DeliveryDaysList = new List<DeliveryDaysModel>();
        }

        public IList<DeliverySlotModel> DeliverySlotList { get; set; }
        public IList<DeliveryDaysModel> DeliveryDaysList { get; set; }

        public DateTime ActiveSlotDate { get; set; }

        public int SlotId { get; set; }

        public string SlotDate { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }
    }

    public class RequestDeliverySlotModel
    {
        public string APIKey { get; set; }
        public int StoreId { get; set; }
        public int CustomerId { get; set; }
        public int RestnCookId { get; set; }
        public string DeliveryDate { get; set; }
        public string UniqueSeoCode { get; set; }
        public int languageId { get; set; }
    }

    public class SaveDeliverySlotModel : RequestDeliverySlotModel
    {
        public int SlotId { get; set; }

        public string SlotDate { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }
    }
}