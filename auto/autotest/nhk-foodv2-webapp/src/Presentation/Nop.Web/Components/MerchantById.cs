﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Services.Stores;
using Nop.Core;
using System;

namespace Nop.Web.Components
{
    public class MerchantByIdViewComponent : NopViewComponent
    {
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;

        public MerchantByIdViewComponent(ICatalogModelFactory catalogModelFactory,IStoreService storeService,IWorkContext workContext)
        {
            this._catalogModelFactory = catalogModelFactory;
            this._storeService = storeService;
            this._workContext = workContext;
        }

        //public IViewComponentResult Invoke(string merchantid)
        public IViewComponentResult Invoke()
        {
            //New logic Added By Mah 22-03-2019 string merchantid

            string merchantid = _workContext.CurrentMerchant.Id.ToString(); //Add new logic to get current vendor

            var model = _catalogModelFactory.PrepareMerchantAllModels().Where(x => x.Id.ToString() == merchantid).ToList();            
            if (!model.Any())
                return Content("");
            //ViewBag.ExpectedDelTime = model.FirstOrDefault().ExpDeliveryTime;
            //ViewData["ExpDeliveryTime"]= model.FirstOrDefault().ExpDeliveryTime;
            TempData["ExpTime"]= model.FirstOrDefault().ExpDeliveryTime;
            var storeModel = _storeService.GetStoreById(model.FirstOrDefault().StoreId,false).IsDisplayDeliveryOrTakeaway;
            foreach (var item in model.FirstOrDefault().AvailableSchedules)
            {
                DateTime time = DateTime.Today.Add(item.ScheduleFromTimeTS);
                item.ScheduleFromTime = time.ToString("hh:mm tt");
                DateTime time2 = DateTime.Today.Add(item.ScheduleToTimeTS);
                item.ScheduleToTime = time2.ToString("hh:mm tt");
            }
            if (storeModel == null)
                TempData["IsDelTakeOption"] = false;
            else
            {
                TempData["IsDelTakeOption"] = storeModel;                
            }
                
            return View(model);            
        }
    }
}
